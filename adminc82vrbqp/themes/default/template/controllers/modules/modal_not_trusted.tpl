<div class="untrusted-content-action">

	<div class="modal-body">
		<div class="row">
			<div class="col-sm-2" style="text-align: center;">
				<img id="untrusted-module-logo" class="" src="" alt="" style="max-width:96px;">
			</div>
			<div class="col-sm-10">
				<table class="table">
					<tr>
						<td>{l s='Module'}</td>
						<td><strong><span class="module-display-name-placeholder"></span></strong></td>
					</tr>
					<tr>
						<td>{l s='Author'}</td>
						<td><strong><span class="author-name-placeholder"></span></strong></td>
					</tr>
				</table>
			</div>

			<div class="col-sm-12" style="text-align: center; padding-top: 12px;">
				<a id="proceed-install-anyway" href="#" class="btn btn-warning">{l s='Proceed with the installation'}</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">{l s='Back to modules list'}</button>
			</div>
		</div>
	</div>
</div>

<div class="untrusted-content-more-info" style="display:none;">

	<div class="modal-body">
	</div>

	<div class="modal-footer">
		<a id="untrusted-show-action" class="btn btn-default" href="#">{l s='Back'}</a>
	</div>

</div>
