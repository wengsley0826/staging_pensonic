{$withGDEX = $order->checkIsGDEXAllowed()}
{if $withGDEX > 0}
  <a href="#" class="add_gdex_shipping_link btn btn-default">
	  <i class="icon-plus"></i> {l s='Add'}
  </a>
{/if}
<div class="table-responsive">
	<table class="table" id="shipping_table_gdex">
		<thead>
			<tr>
				<th>
					<span class="title_box ">{l s='Date'}</span>
				</th>
				<th>
					<span class="title_box ">{l s='Weight'}</span>
				</th>
				<th>
					<span class="title_box ">{l s='Height'}</span>
				</th>
				<th>
					<span class="title_box ">{l s='Width'}</span>
				</th>
				<th>
					<span class="title_box ">{l s='Length'}</span>
				</th>
				<th>
					<span class="title_box ">{l s='Pcs'}</span>
				</th>
        <th>
					<span class="title_box ">{l s='Update'}</span>
				</th>
				<th>
					<span class="title_box ">{l s='Status'}</span>
				</th>
				<th>
					<span class="title_box ">{l s='Tracking number'}</span>
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$order->getCustomShipping() item=line}
			<tr>
				<td>{dateFormat date=$line.date_add full=true}</td>
				<td class="weight">
					<span class="gdex_shipping_label">{$line.weight|string_format:"%.3f"}</span>
					<span class="gdex_shipping_edit" style="display:none;">
						<input type="text" name="weight" value="{$line.weight|htmlentities}" />
					</span>
					{Configuration::get('PS_WEIGHT_UNIT')}
				</td>
				<td class="height">
					<span class="gdex_shipping_label">{$line.height|string_format:"%.3f"}</span>
					<span class="gdex_shipping_edit" style="display:none;">
						<input type="text" name="height" value="{$line.height|htmlentities}" />
					</span>
					{Configuration::get('PS_DIMENSION_UNIT')}
				</td>
				<td class="width">
					<span class="gdex_shipping_label">{$line.width|string_format:"%.3f"}</span>
					<span class="gdex_shipping_edit" style="display:none;">
						<input type="text" name="width" value="{$line.width|htmlentities}" />
					</span>
					{Configuration::get('PS_DIMENSION_UNIT')}
				</td>
				<td class="length">
					<span class="gdex_shipping_label">{$line.length|string_format:"%.3f"}</span>
					<span class="gdex_shipping_edit" style="display:none;">
						<input type="text" name="length" value="{$line.length|htmlentities}" />
					</span>
					{Configuration::get('PS_DIMENSION_UNIT')}
					</td>
				<td>
					<span class="gdex_shipping_label">{$line.pieces}</span>
					<span class="gdex_shipping_edit" style="display:none;">
						<input type="text" name="pieces" value="{$line.pieces|htmlentities}" />
					</span>
				</td>
        <td>
          <span class="gdex_shipping_label">{if $line.update_order_status}Y{else}N{/if}</span>
        </td>
				<td>
					<span class="gdex_shipping_label">{$line.status}</span>
					<span class="gdex_shipping_edit" style="display:none;">
						<input type="text" name="status" value="{$line.status|htmlentities}" />
					</span>
				</td>
				<td><a href="../index.php?controller=pdf-vendor-gdex&id_order={$order->id}&gdexnumber={$line.tracking_number}" target="_blank" rel="{$line.tracking_number}" >{$line.tracking_number}</a></td>				
			</tr>
      {/foreach}
      {if $withGDEX > 0}
      <tr class="blankRow" style="display:none;">
				<td></td>
				<td class="weight">
					<span class="gdex_shipping_edit">
						<input type="text" id="weight" name="weight" value="{$order->getOrderTotalWeight()}" />
					</span>
					{Configuration::get('PS_WEIGHT_UNIT')}
				</td>
				<td class="height">
					<span class="gdex_shipping_edit">
						<input type="text" id="height" name="height" value="" />
					</span>
					{Configuration::get('PS_DIMENSION_UNIT')}
				</td>
				<td class="width">
					<span class="gdex_shipping_edit">
						<input type="text" id="width" name="width" value="" />
					</span>
					{Configuration::get('PS_DIMENSION_UNIT')}
				</td>
				<td class="length">
					<span class="gdex_shipping_edit">
						<input type="text" id="length" name="length" value="" />
					</span>
					{Configuration::get('PS_DIMENSION_UNIT')}
					</td>
				<td>
					<span class="gdex_shipping_edit">
						<input type="text" id="pieces" name="pieces" value="1" />
					</span>
				</td>
        <td>
					<span class="gdex_shipping_edit">
            <input type="checkbox" id="update_order_status" name="update_order_status" checked="checked" />
					</span>
				</td>
				<td class="actions" colspan="2">				
					<input type="hidden" name="id_order_shipping" value="0" />
					<input type="hidden" id="ship_order_id" name="id_order" value="{$order->id}" />
					<span class="gdex_shipping_edit">
						<button type="submit" class="addgdexshipping btn btn-default" name="addGDEXShipping">
							<i class="icon-plus"></i> {l s='Add'}
						</button>
					</span>
					<a href="#" class="cancel_gdex_shipping_link_0 btn btn-default" >
						<i class="icon-remove"></i>{l s='Cancel'}
					</a>				
				</td>
			</tr>
      {/if}
    </tbody>
	</table>
</div>
