<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 14:07:42
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1498564422615bebae54f3d5-93533649%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '03f8a510fde19c0aa1f8ee4b9893f9972c4925dc' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/product.tpl',
      1 => 1602254037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1498564422615bebae54f3d5-93533649',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ajax_loyalty' => 0,
    'link' => 0,
    'display_credits' => 0,
    'credits' => 0,
    'total_credits' => 0,
    'no_pts_discounted' => 0,
    'minimum' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615bebae559622_15901391',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615bebae559622_15901391')) {function content_615bebae559622_15901391($_smarty_tpl) {?>
<?php if (!isset($_smarty_tpl->tpl_vars['ajax_loyalty']->value)) {?>
<!-- MODULE allinone_rewards -->
<script type="text/javascript">
//<![CDATA[
	var url_allinone_loyalty="<?php echo strtr($_smarty_tpl->tpl_vars['link']->value->getModuleLink('allinone_rewards','loyalty',array(),true), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
//]]>
</script>
<p id="loyalty" class="align_justify"></p>
<br class="clear" />
<!-- END : MODULE allinone_rewards -->
<?php } else { ?>
	<?php if ($_smarty_tpl->tpl_vars['display_credits']->value) {?>
		<?php echo smartyTranslate(array('s'=>'Buying this product you will collect ','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <b><span id="loyalty_credits"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['credits']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span></b> <?php echo smartyTranslate(array('s'=>' with our loyalty program.','mod'=>'allinone_rewards'),$_smarty_tpl);?>

		<?php echo smartyTranslate(array('s'=>'Your cart will total','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <b><span id="total_loyalty_credits"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['total_credits']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span></b> <?php echo smartyTranslate(array('s'=>'that can be converted into a voucher for a future purchase.','mod'=>'allinone_rewards'),$_smarty_tpl);?>

	<?php } else { ?>
		<?php if (isset($_smarty_tpl->tpl_vars['no_pts_discounted']->value)&&$_smarty_tpl->tpl_vars['no_pts_discounted']->value==1) {?>
			<?php echo smartyTranslate(array('s'=>'No reward credits for this product because there\'s already a discount.','mod'=>'allinone_rewards'),$_smarty_tpl);?>

		<?php } else { ?>
			<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>floatval($_smarty_tpl->tpl_vars['minimum']->value)),$_smarty_tpl);?>
<?php $_tmp9=ob_get_clean();?><?php echo smartyTranslate(array('s'=>'Your basket must contain at least %s of products in order to get loyalty rewards.','sprintf'=>$_tmp9,'mod'=>'allinone_rewards'),$_smarty_tpl);?>

		<?php }?>
	<?php }?>
<?php }?><?php }} ?>
