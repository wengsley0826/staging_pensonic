<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 13:10:41
         compiled from "/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/order-steps.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1733656134615bde51ba43b2-45555047%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e8ce2c0a709aa1b440315115ae5cc9fdbd3c707' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/order-steps.tpl',
      1 => 1632587444,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1733656134615bde51ba43b2-45555047',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'back' => 0,
    'multi_shipping' => 0,
    'opc' => 0,
    'current_step' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615bde51bd3e60_75670040',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615bde51bd3e60_75670040')) {function content_615bde51bd3e60_75670040($_smarty_tpl) {?><?php $_smarty_tpl->_capture_stack[0][] = array("url_back", null, null); ob_start(); ?>	<?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?><?php if (!isset($_smarty_tpl->tpl_vars['multi_shipping']->value)) {?>	<?php $_smarty_tpl->tpl_vars['multi_shipping'] = new Smarty_variable('0', null, 0);?><?php }?><?php if (!$_smarty_tpl->tpl_vars['opc']->value&&((!isset($_smarty_tpl->tpl_vars['back']->value)||empty($_smarty_tpl->tpl_vars['back']->value))||(isset($_smarty_tpl->tpl_vars['back']->value)&&preg_match("/[&?]step=/",$_smarty_tpl->tpl_vars['back']->value)))) {?>	<!-- Steps -->	<ul class="step clearfix" id="order_step">		<li class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='summary') {?>step_current <?php } elseif ($_smarty_tpl->tpl_vars['current_step']->value=='login') {?>step_done_last step_done<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='address'||$_smarty_tpl->tpl_vars['current_step']->value=='login') {?>step_done<?php } else { ?>step_todo<?php }?><?php }?> first">			<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='address'||$_smarty_tpl->tpl_vars['current_step']->value=='login') {?>			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true);?>
">				<div>1</div>			</a>			<small><?php echo smartyTranslate(array('s'=>'Summary'),$_smarty_tpl);?>
</small>			<?php } else { ?>				<span><div>1</div><small><?php echo smartyTranslate(array('s'=>'Summary'),$_smarty_tpl);?>
</small></span>			<?php }?>        		</li>		<li class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='login') {?>step_current<?php } elseif ($_smarty_tpl->tpl_vars['current_step']->value=='address') {?>step_done step_done_last<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='address') {?>step_done<?php } else { ?>step_todo<?php }?><?php }?> second">			<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='address') {?>			<a href="<?php ob_start();?><?php if ($_smarty_tpl->tpl_vars['multi_shipping']->value) {?><?php echo "&multi-shipping=";?><?php echo (string)$_smarty_tpl->tpl_vars['multi_shipping']->value;?><?php }?><?php $_tmp8=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true,null,((string)Smarty::$_smarty_vars['capture']['url_back'])."&step=1".$_tmp8), ENT_QUOTES, 'UTF-8', true);?>
">				<div>2</div>			</a>			 <small><?php echo smartyTranslate(array('s'=>'Login'),$_smarty_tpl);?>
</small>			<?php } else { ?>				<span><div>2</div><small><?php echo smartyTranslate(array('s'=>'Login'),$_smarty_tpl);?>
</small></span>			<?php }?>		</li>		<li class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='address') {?>step_current<?php } elseif ($_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>step_done step_done_last<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>step_done<?php } else { ?>step_todo<?php }?><?php }?> third">			<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>			<a href="<?php ob_start();?><?php if ($_smarty_tpl->tpl_vars['multi_shipping']->value) {?><?php echo "&multi-shipping=";?><?php echo (string)$_smarty_tpl->tpl_vars['multi_shipping']->value;?><?php }?><?php $_tmp9=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true,null,((string)Smarty::$_smarty_vars['capture']['url_back'])."&step=1".$_tmp9), ENT_QUOTES, 'UTF-8', true);?>
">				<div>3</div>			</a>			<small><?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>
</small>			<?php } else { ?>				<span><div>3</div><small><?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>
</small></span>			<?php }?>		</li>		<li class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>step_current<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment') {?>step_done step_done_last<?php } else { ?>step_todo<?php }?><?php }?> four">			<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment') {?>			<a href="<?php ob_start();?><?php if ($_smarty_tpl->tpl_vars['multi_shipping']->value) {?><?php echo "&multi-shipping=";?><?php echo (string)$_smarty_tpl->tpl_vars['multi_shipping']->value;?><?php }?><?php $_tmp10=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true,null,((string)Smarty::$_smarty_vars['capture']['url_back'])."&step=2".$_tmp10), ENT_QUOTES, 'UTF-8', true);?>
">				<div>4</div>			</a>			<small><?php echo smartyTranslate(array('s'=>'Shipping'),$_smarty_tpl);?>
</small>			<?php } else { ?>				<span><div>4</div><small><?php echo smartyTranslate(array('s'=>'Shipping'),$_smarty_tpl);?>
</small></span>			<?php }?>		</li>		<li id="step_end" class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment') {?>step_current<?php } else { ?>step_todo<?php }?> last">			<span><div>5</div><small><?php echo smartyTranslate(array('s'=>'Payment'),$_smarty_tpl);?>
</small></span>		</li>	</ul>	<!-- /Steps -->	<h6 id="steps-summary" class="steps-heading <?php if ($_smarty_tpl->tpl_vars['current_step']->value=='summary') {?>steps-heading-current<?php } else { ?>step-heading-todo<?php }?>">Checkout</h6>	<h6 id="steps-login" class="steps-heading <?php if ($_smarty_tpl->tpl_vars['current_step']->value=='login') {?>steps-heading-current<?php } else { ?>step-heading-todo<?php }?>">AUTHENTICATION</h6>    <h6 id="steps-address" class="steps-heading <?php if ($_smarty_tpl->tpl_vars['current_step']->value=='address') {?>steps-heading-current<?php } else { ?>step-heading-todo<?php }?>">address</h6>    <h6 id="steps-shipping" class="steps-heading <?php if ($_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>steps-heading-current<?php } else { ?>step-heading-todo<?php }?>">shipping</h6>    <h6 id="steps-payment" class="steps-heading <?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment') {?>steps-heading-current<?php } else { ?>step-heading-todo<?php }?>">Summary</h6><?php }?><?php }} ?>
