<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:38:58
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/admincustomer-sponsorship.tpl" */ ?>
<?php /*%%SmartyHeaderCode:462020338615c0f2226e558-75516998%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1a723f81f5d50be8f4480e359dfd4641efb23464' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/admincustomer-sponsorship.tpl',
      1 => 1602254037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '462020338615c0f2226e558-75516998',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'msg' => 0,
    'sponsorship_code' => 0,
    'sponsorship_custom_code' => 0,
    'sponsorship_link' => 0,
    'sponsorship_templates' => 0,
    'template' => 0,
    'sponsorship_template_id' => 0,
    'sponsor' => 0,
    'discount_gc' => 0,
    'currencies' => 0,
    'default_currency' => 0,
    'currency' => 0,
    'friends' => 0,
    'stats' => 0,
    'sponsored' => 0,
    'channel' => 0,
    'module_template_dir' => 0,
    'sponsor_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0f222c9019_15005499',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0f222c9019_15005499')) {function content_615c0f222c9019_15005499($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/var/www/html/pensonicstore.claritascloud.com/tools/smarty/plugins/modifier.escape.php';
?>
<!-- MODULE allinone_rewards -->
<style>
	tr.inactive td {
		text-decoration: line-through;
	}
	tr.inactive td.nostrike {
		text-decoration: none;
	}
</style>
<div class="<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>col-lg-12<?php } else { ?>clear<?php }?>" id="admincustomer_sponsorship">
<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
	<div class="panel">
		<div class="panel-heading"><?php echo smartyTranslate(array('s'=>'Sponsorship program','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</div>
		<?php if ($_smarty_tpl->tpl_vars['msg']->value) {?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['msg']->value, 'string', 'UTF-8');?>
<?php }?>
<?php } else { ?>
		<h2><?php echo smartyTranslate(array('s'=>'Sponsorship program','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h2>
		<?php if ($_smarty_tpl->tpl_vars['msg']->value) {?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['msg']->value, 'string', 'UTF-8');?>
<br><?php }?>
<?php }?>
		<form id='sponsor' method='post'>
			<input type="hidden" name="action" />
			<input type="hidden" id="id_sponsorship_to_update" name="id_sponsorship_to_update" />

			<span><?php echo smartyTranslate(array('s'=>'Sponsorship code :','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsorship_code']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
			<span style="padding-left: 40px"><?php echo smartyTranslate(array('s'=>'You can customize this code','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <input type="text" name="sponsorship_custom_code" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsorship_custom_code']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" size="12" maxlength="20" style="width: auto; display: inline-block"/></span>
			<input class="button" name="submitSponsorCustomCode" id="submitSponsorCustomCode" value="<?php echo smartyTranslate(array('s'=>'Save settings','mod'=>'allinone_rewards'),$_smarty_tpl);?>
" type="submit" />
			&nbsp;<?php echo smartyTranslate(array('s'=>'(length between 5 and 20 characters, and only digits or letters)','mod'=>'allinone_rewards'),$_smarty_tpl);?>

			<br/><span><?php echo smartyTranslate(array('s'=>'Sponsorship link :','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsorship_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span><br/><br/>

			<?php echo smartyTranslate(array('s'=>'Template used for "Sponsorship program"','mod'=>'allinone_rewards'),$_smarty_tpl);?>
&nbsp;
			<select class="change_template" name="sponsorship_template" style="display: inline; width: auto;">
				<option value='0'><?php echo smartyTranslate(array('s'=>'Default template','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</option>
				<?php  $_smarty_tpl->tpl_vars['template'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['template']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sponsorship_templates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['template']->key => $_smarty_tpl->tpl_vars['template']->value) {
$_smarty_tpl->tpl_vars['template']->_loop = true;
?>
					<option <?php if ($_smarty_tpl->tpl_vars['template']->value['id_template']==$_smarty_tpl->tpl_vars['sponsorship_template_id']->value) {?>selected<?php }?> value='<?php echo intval($_smarty_tpl->tpl_vars['template']->value['id_template']);?>
'><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['template']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
				<?php } ?>
			</select><br/><br/>
		<?php if ($_smarty_tpl->tpl_vars['sponsor']->value) {?>
			<?php echo smartyTranslate(array('s'=>'Sponsor','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <a href="?tab=AdminCustomers&id_customer=<?php echo intval($_smarty_tpl->tpl_vars['sponsor']->value->id);?>
&viewcustomer&token=<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getAdminToken'][0][0]->getAdminTokenLiteSmarty(array('tab'=>'AdminCustomers'),$_smarty_tpl);?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsor']->value->firstname, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsor']->value->lastname, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a>
		<?php } else { ?>
			<?php echo smartyTranslate(array('s'=>'Choose a sponsor','mod'=>'allinone_rewards'),$_smarty_tpl);?>
&nbsp;
			<input type="hidden" size="30" name="new_sponsor" id="new_sponsor"/>
			<input type="text" size="30" id="search_sponsor" style="display: inline; width: 150px;"/>
			<?php if ($_smarty_tpl->tpl_vars['discount_gc']->value) {?>
				&nbsp;&nbsp;&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Generate the welcome voucher ?','mod'=>'allinone_rewards'),$_smarty_tpl);?>
&nbsp;<input checked type="checkbox" value="1" name="generate_voucher" style="display: inline; width: auto;">&nbsp;
				<select name="generate_currency" style="display: inline !important; width: auto;">
				<?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value) {
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
					<option <?php if ($_smarty_tpl->tpl_vars['default_currency']->value==$_smarty_tpl->tpl_vars['currency']->value['id_currency']) {?>selected<?php }?> value="<?php echo intval($_smarty_tpl->tpl_vars['currency']->value['id_currency']);?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
				<?php } ?>
				</select>
			<?php }?>
			&nbsp;<input class="button" name="submitSponsor" id="submitSponsor" value="<?php echo smartyTranslate(array('s'=>'Save settings','mod'=>'allinone_rewards'),$_smarty_tpl);?>
" type="submit" />
		<?php }?>
			<br/><br/>
		<?php if (count($_smarty_tpl->tpl_vars['friends']->value)) {?>
		<table cellspacing='0' cellpadding='0' class='table'>
			<thead>
				<tr style="background-color: #EEEEEE">
					<th class="borderright" colspan='3' style='text-align: center'><?php echo smartyTranslate(array('s'=>'Total rewards','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th colspan='5' style='text-align: center'><?php echo smartyTranslate(array('s'=>'Sponsored friends (Level 1)','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				</tr>
				<tr style="background-color: #EEEEEE">
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Rewards for orders','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Rewards for registrations','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th class="borderright" style='text-align: center'><?php echo smartyTranslate(array('s'=>'Indirect rewards','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Pending','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Registered','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'With orders','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Orders','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Total','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				</tr>
			</thead>
			<tr>
				<td align='center'><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['stats']->value['direct_rewards_orders']),$_smarty_tpl);?>
</td>
				<td align='center'><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['stats']->value['direct_rewards_registrations']),$_smarty_tpl);?>
</td>
				<td class="borderright" align='center'><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['stats']->value['indirect_rewards']),$_smarty_tpl);?>
</td>
				<td align='center'><?php echo intval($_smarty_tpl->tpl_vars['stats']->value['nb_pending']);?>
</td>
				<td align='center'><?php echo intval($_smarty_tpl->tpl_vars['stats']->value['nb_registered']);?>
</td>
				<td align='center'><?php echo intval($_smarty_tpl->tpl_vars['stats']->value['nb_buyers']);?>
</td>
				<td align='center'><?php echo intval($_smarty_tpl->tpl_vars['stats']->value['nb_orders']);?>
</td>
				<td align='center'><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['stats']->value['total_orders']),$_smarty_tpl);?>
</td>
			</tr>
		</table>
		<div class='clear' style="margin-top: 20px">&nbsp;</div>
		<table cellspacing='0' cellpadding='0' class='tablesorter tablesorter-ice' id='sponsorships_list'>
			<thead>
				<tr style="background-color: #EEEEEE">
					<th class="filter-select"><?php echo smartyTranslate(array('s'=>'Levels','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th class="filter-select"><?php echo smartyTranslate(array('s'=>'Channels','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Name of the friends','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th class="filter-select"><?php echo smartyTranslate(array('s'=>'Number of orders','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Total orders','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Total rewards','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'End date','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th class='filter-false sorter-false'><?php echo smartyTranslate(array('s'=>'Action','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['sponsored'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sponsored']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['friends']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sponsored']->key => $_smarty_tpl->tpl_vars['sponsored']->value) {
$_smarty_tpl->tpl_vars['sponsored']->_loop = true;
?>
				<?php ob_start();?><?php echo smartyTranslate(array('s'=>'Email invitation','mod'=>'allinone_rewards'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["channel"] = new Smarty_variable($_tmp2, null, 0);?>
				<?php if (($_smarty_tpl->tpl_vars['sponsored']->value['channel']==2)) {?>
					<?php ob_start();?><?php echo smartyTranslate(array('s'=>'Sponsorship link','mod'=>'allinone_rewards'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["channel"] = new Smarty_variable($_tmp3, null, 0);?>
				<?php } elseif (($_smarty_tpl->tpl_vars['sponsored']->value['channel']==3)) {?>
					<?php ob_start();?><?php echo smartyTranslate(array('s'=>'Facebook','mod'=>'allinone_rewards'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["channel"] = new Smarty_variable($_tmp4, null, 0);?>
				<?php } elseif (($_smarty_tpl->tpl_vars['sponsored']->value['channel']==4)) {?>
					<?php ob_start();?><?php echo smartyTranslate(array('s'=>'Twitter','mod'=>'allinone_rewards'),$_smarty_tpl);?>
<?php $_tmp5=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["channel"] = new Smarty_variable($_tmp5, null, 0);?>
				<?php } elseif (($_smarty_tpl->tpl_vars['sponsored']->value['channel']==5)) {?>
					<?php ob_start();?><?php echo smartyTranslate(array('s'=>'Google +1','mod'=>'allinone_rewards'),$_smarty_tpl);?>
<?php $_tmp6=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["channel"] = new Smarty_variable($_tmp6, null, 0);?>
				<?php }?>
				<tr <?php if (!$_smarty_tpl->tpl_vars['sponsored']->value['active']) {?>class="inactive"<?php }?>>
					<td align='center'><?php echo intval($_smarty_tpl->tpl_vars['sponsored']->value['level_sponsorship']);?>
</td>
					<td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['channel']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
					<td><a href="?tab=AdminCustomers&id_customer=<?php echo intval($_smarty_tpl->tpl_vars['sponsored']->value['id_sponsored']);?>
&viewcustomer&token=<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getAdminToken'][0][0]->getAdminTokenLiteSmarty(array('tab'=>'AdminCustomers'),$_smarty_tpl);?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['lastname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['firstname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></td>
					<td align='center'><?php echo intval($_smarty_tpl->tpl_vars['sponsored']->value['nb_orders']);?>
</td>
					<td align='right'><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['sponsored']->value['total_orders']),$_smarty_tpl);?>
</td>
					<td align='right'><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['sponsored']->value['total_rewards']),$_smarty_tpl);?>
</td>
					<td class="nostrike" style="text-align: center"><?php if ($_smarty_tpl->tpl_vars['sponsored']->value['level_sponsorship']==1) {?><input type="text" name="date_end_<?php echo intval($_smarty_tpl->tpl_vars['sponsored']->value['id_sponsorship']);?>
" style="width: 140px" class="datetimepicker" value="<?php if ($_smarty_tpl->tpl_vars['sponsored']->value['date_end']!=0) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['date_end'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"><?php }?></td>
					<td class="nostrike" style="text-align: center"><?php if ($_smarty_tpl->tpl_vars['sponsored']->value['level_sponsorship']==1) {?><input class="button" name="submitSponsorshipEndDate" type="submit" value="<?php echo smartyTranslate(array('s'=>'Save settings','mod'=>'allinone_rewards'),$_smarty_tpl);?>
" onClick="$('#id_sponsorship_to_update').val(<?php echo intval($_smarty_tpl->tpl_vars['sponsored']->value['id_sponsorship']);?>
)"><?php }?></td>
				</tr>
			<?php } ?>
			<tbody>
		</table>
		<div class="pager">
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/first.png" class="first"/>
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/prev.png" class="prev"/>
	    	<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/next.png" class="next"/>
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/last.png" class="last"/>
	    	<select class="pagesize" style="width: auto; display: inline">
	      		<option value='10'>10</option>
	      		<option value='20'>20</option>
	      		<option value='50'>50</option>
	      		<option value='100'>100</option>
	      		<option value='500'>500</option>
	    	</select>
		</div>
		<script>
			$('.datetimepicker').datetimepicker({
				prevText: '',
				nextText: '',
				dateFormat: 'yy-mm-dd',
				// Define a custom regional settings in order to use PrestaShop translation tools
				currentText: '<?php echo smartyTranslate(array('s'=>'Now','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
				closeText: '<?php echo smartyTranslate(array('s'=>'Done','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
				ampm: false,
				amNames: ['AM', 'A'],
				pmNames: ['PM', 'P'],
				timeFormat: 'hh:mm:ss tt',
				timeSuffix: '',
				timeOnlyTitle: "<?php echo smartyTranslate(array('s'=>'Choose Time','mod'=>'allinone_rewards'),$_smarty_tpl);?>
",
				timeText: '<?php echo smartyTranslate(array('s'=>'Time','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
				hourText: '<?php echo smartyTranslate(array('s'=>'Hour','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
				minuteText: '<?php echo smartyTranslate(array('s'=>'Minute','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
				secondText: '<?php echo smartyTranslate(array('s'=>'Second','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
				showSecond: true
			});
		</script>
		<?php } else { ?>
			<?php echo smartyTranslate(array('s'=>'This customer has not sponsored any friends yet.','mod'=>'allinone_rewards'),$_smarty_tpl);?>

		<?php }?>
		</form>
<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
	</div>
<?php }?>
</div>
<script>
	var idText="<?php echo smartyTranslate(array('s'=>'ID','mod'=>'allinone_rewards'),$_smarty_tpl);?>
";
	var firstnameText="<?php echo smartyTranslate(array('s'=>'Firstname','mod'=>'allinone_rewards'),$_smarty_tpl);?>
";
	var lastnameText="<?php echo smartyTranslate(array('s'=>'Lastname','mod'=>'allinone_rewards'),$_smarty_tpl);?>
";
	var emailText="<?php echo smartyTranslate(array('s'=>'Email','mod'=>'allinone_rewards'),$_smarty_tpl);?>
";
	var sponsor_url = "<?php echo strtr($_smarty_tpl->tpl_vars['sponsor_url']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
	initAutocomplete(<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>true<?php } else { ?>false<?php }?>);
</script>
<!-- END : MODULE allinone_rewards --><?php }} ?>
