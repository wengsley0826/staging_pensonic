<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:38:23
         compiled from "/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/modules/blockcms/blockcms.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1731991928615c0eff37f7a5-67278677%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1c47a0fac6ced3672bf29b087ce67125795ff86c' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/modules/blockcms/blockcms.tpl',
      1 => 1601890741,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1731991928615c0eff37f7a5-67278677',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'block' => 0,
    'cms_titles' => 0,
    'cms_key' => 0,
    'cms_title' => 0,
    'cms_page' => 0,
    'link' => 0,
    'show_price_drop' => 0,
    'PS_CATALOG_MODE' => 0,
    'show_new_products' => 0,
    'show_best_sales' => 0,
    'display_stores_footer' => 0,
    'show_contact' => 0,
    'contact_url' => 0,
    'cmslinks' => 0,
    'cmslink' => 0,
    'show_sitemap' => 0,
    'show_review' => 0,
    'review_count' => 0,
    'review_rate' => 0,
    'show_price_drop2' => 0,
    'show_new_products2' => 0,
    'show_best_sales2' => 0,
    'display_stores_footer2' => 0,
    'show_contact2' => 0,
    'cmslinks2' => 0,
    'show_sitemap2' => 0,
    'show_review2' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0eff3ec9f2_85017594',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0eff3ec9f2_85017594')) {function content_615c0eff3ec9f2_85017594($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['block']->value==1) {?>
<!-- Block CMS module -->
<?php  $_smarty_tpl->tpl_vars['cms_title'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cms_title']->_loop = false;
 $_smarty_tpl->tpl_vars['cms_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cms_titles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cms_title']->key => $_smarty_tpl->tpl_vars['cms_title']->value) {
$_smarty_tpl->tpl_vars['cms_title']->_loop = true;
 $_smarty_tpl->tpl_vars['cms_key']->value = $_smarty_tpl->tpl_vars['cms_title']->key;
?>
<section id="informations_block_left_<?php echo $_smarty_tpl->tpl_vars['cms_key']->value;?>
" class="block informations_block_left">
  <p class="title_block"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cms_title']->value['category_link'], ENT_QUOTES, 'UTF-8', true);?>
"> <?php if (!empty($_smarty_tpl->tpl_vars['cms_title']->value['name'])) {?><?php echo $_smarty_tpl->tpl_vars['cms_title']->value['name'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['cms_title']->value['category_name'];?>
<?php }?> </a> </p>
  <div class="block_content list-block">
    <ul>
      <?php  $_smarty_tpl->tpl_vars['cms_page'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cms_page']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cms_title']->value['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cms_page']->key => $_smarty_tpl->tpl_vars['cms_page']->value) {
$_smarty_tpl->tpl_vars['cms_page']->_loop = true;
?>						<?php if (isset($_smarty_tpl->tpl_vars['cms_page']->value['link'])) {?>
      <li class="bullet"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cms_page']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cms_page']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cms_page']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
 </a> </li>
      <?php }?>					<?php } ?>					<?php  $_smarty_tpl->tpl_vars['cms_page'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cms_page']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cms_title']->value['cms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cms_page']->key => $_smarty_tpl->tpl_vars['cms_page']->value) {
$_smarty_tpl->tpl_vars['cms_page']->_loop = true;
?>						<?php if (isset($_smarty_tpl->tpl_vars['cms_page']->value['link'])) {?>
      <li> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cms_page']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cms_page']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cms_page']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
 </a> </li>
      <?php }?>					<?php } ?>					<?php if ($_smarty_tpl->tpl_vars['cms_title']->value['display_store']) {?>
      <li> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('stores'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Our stores','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Our stores','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
      <?php }?>
    </ul>
  </div>
</section>
<?php } ?>
<!-- /Block CMS module -->
<?php } else { ?>
<!-- Block CMS module footer -->
<section class="footer-block corporate col-xs-12 col-sm-3" id="block_various_links_footer">
  <h4><?php echo smartyTranslate(array('s'=>'About Pensonic','mod'=>'blockcms'),$_smarty_tpl);?>
</h4>
  <ul>
    <?php if (isset($_smarty_tpl->tpl_vars['show_price_drop']->value)&&$_smarty_tpl->tpl_vars['show_price_drop']->value&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('prices-drop'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Specials','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Specials','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['show_new_products']->value)&&$_smarty_tpl->tpl_vars['show_new_products']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('new-products'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'New products','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'New products','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['show_best_sales']->value)&&$_smarty_tpl->tpl_vars['show_best_sales']->value&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('best-sales'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Top sellers','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Top sellers','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['display_stores_footer']->value)&&$_smarty_tpl->tpl_vars['display_stores_footer']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('stores'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Our stores','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Our stores','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['show_contact']->value)&&$_smarty_tpl->tpl_vars['show_contact']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink($_smarty_tpl->tpl_vars['contact_url']->value,true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Contact us','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Contact us','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php  $_smarty_tpl->tpl_vars['cmslink'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cmslink']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cmslinks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cmslink']->key => $_smarty_tpl->tpl_vars['cmslink']->value) {
$_smarty_tpl->tpl_vars['cmslink']->_loop = true;
?>				<?php if ($_smarty_tpl->tpl_vars['cmslink']->value['meta_title']!='') {?>					<?php if (strtolower($_smarty_tpl->tpl_vars['cmslink']->value['meta_title'])=='faq') {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'General FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'General FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#order-faq" title="<?php echo smartyTranslate(array('s'=>'Order FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Order FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#delivery-faq" title="<?php echo smartyTranslate(array('s'=>'Delivery FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Delivery FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#payment-faq" title="<?php echo smartyTranslate(array('s'=>'Payment and Pricing FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Payment and Pricing FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#account-faq" title="<?php echo smartyTranslate(array('s'=>'Account FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Account FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#return-faq" title="<?php echo smartyTranslate(array('s'=>'Return and Cancellation FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Return and Cancellation FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#refund-faq" title="<?php echo smartyTranslate(array('s'=>'Refund FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Refund FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#membership-faq" title="<?php echo smartyTranslate(array('s'=>'Membership FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Membership FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php } else { ?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
 </a> </li>
    <?php }?>				<?php }?>			<?php } ?>			<?php if (isset($_smarty_tpl->tpl_vars['show_sitemap']->value)&&$_smarty_tpl->tpl_vars['show_sitemap']->value) {?>
    <li> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('sitemap'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Sitemap','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Sitemap','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>
  </ul>
  <?php if (isset($_smarty_tpl->tpl_vars['show_review']->value)&&$_smarty_tpl->tpl_vars['show_review']->value) {?> <br />
  <div class="y-badges"> <span class="y-badges-review"><?php echo $_smarty_tpl->tpl_vars['review_count']->value;?>
</span>
    <div class="y-badge-stars"> <?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=0.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=0.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=1.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=1.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=2.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=2.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=3.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=3.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=4.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=4.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?> </div>
    <span class="y-badges-title">Certified reviews</span> </div>
  <?php }?> </section>
<!-- Block CMS module footer -->
<section class="footer-block customer-service col-xs-12 col-sm-2" id="block_various_links_footer">
  <h4><?php echo smartyTranslate(array('s'=>'FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
</h4>
  <ul>
    <?php if (isset($_smarty_tpl->tpl_vars['show_price_drop2']->value)&&$_smarty_tpl->tpl_vars['show_price_drop2']->value&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('prices-drop'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Specials','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Specials','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['show_new_products2']->value)&&$_smarty_tpl->tpl_vars['show_new_products2']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('new-products'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'New products','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'New products','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['show_best_sales2']->value)&&$_smarty_tpl->tpl_vars['show_best_sales2']->value&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('best-sales'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Top sellers','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Top sellers','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['display_stores_footer2']->value)&&$_smarty_tpl->tpl_vars['display_stores_footer2']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('stores'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Our stores','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Our stores','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php if (isset($_smarty_tpl->tpl_vars['show_contact2']->value)&&$_smarty_tpl->tpl_vars['show_contact2']->value) {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink($_smarty_tpl->tpl_vars['contact_url']->value,true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Contact us','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Contact us','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>			<?php  $_smarty_tpl->tpl_vars['cmslink'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cmslink']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cmslinks2']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cmslink']->key => $_smarty_tpl->tpl_vars['cmslink']->value) {
$_smarty_tpl->tpl_vars['cmslink']->_loop = true;
?>				<?php if ($_smarty_tpl->tpl_vars['cmslink']->value['meta_title']!='') {?>					<?php if (strtolower($_smarty_tpl->tpl_vars['cmslink']->value['meta_title'])=='faq') {?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'General FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'General FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#order-faq" title="<?php echo smartyTranslate(array('s'=>'Order FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Order FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#delivery-faq" title="<?php echo smartyTranslate(array('s'=>'Delivery FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Delivery FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#payment-faq" title="<?php echo smartyTranslate(array('s'=>'Payment and Pricing FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Payment and Pricing FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#account-faq" title="<?php echo smartyTranslate(array('s'=>'Account FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Account FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#return-faq" title="<?php echo smartyTranslate(array('s'=>'Return and Cancellation FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Return and Cancellation FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#refund-faq" title="<?php echo smartyTranslate(array('s'=>'Refund FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Refund FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
#membership-faq" title="<?php echo smartyTranslate(array('s'=>'Membership FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Membership FAQ','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php } else { ?>
    <li class="item"> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cmslink']->value['meta_title'], ENT_QUOTES, 'UTF-8', true);?>
 </a> </li>
    <?php }?>				<?php }?>			<?php } ?>			<?php if (isset($_smarty_tpl->tpl_vars['show_sitemap2']->value)&&$_smarty_tpl->tpl_vars['show_sitemap2']->value) {?>
    <li> <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('sitemap'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Sitemap','mod'=>'blockcms'),$_smarty_tpl);?>
"> <?php echo smartyTranslate(array('s'=>'Sitemap','mod'=>'blockcms'),$_smarty_tpl);?>
 </a> </li>
    <?php }?>
  </ul>
  <?php if (isset($_smarty_tpl->tpl_vars['show_review2']->value)&&$_smarty_tpl->tpl_vars['show_review2']->value) {?> <br />
  <div class="y-badges"> <span class="y-badges-review"><?php echo $_smarty_tpl->tpl_vars['review_count']->value;?>
</span>
    <div class="y-badge-stars"> <?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=0.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=0.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=1.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=1.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=2.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=2.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=3.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=3.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?>				<?php if ($_smarty_tpl->tpl_vars['review_rate']->value>=4.8) {?><i class="icon-star"></i><?php } elseif ($_smarty_tpl->tpl_vars['review_rate']->value>=4.3) {?><i class="icon-star-half-empty"></i><?php } else { ?><i class="icon-star-empty"></i><?php }?> </div>
    <span class="y-badges-title">Certified reviews</span> </div>
  <?php }?> </section>
<!-- /Block CMS module footer -->
<?php }?> <?php }} ?>
