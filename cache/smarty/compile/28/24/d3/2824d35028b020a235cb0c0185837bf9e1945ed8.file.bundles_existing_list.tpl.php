<?php /* Smarty version Smarty-3.1.19, created on 2021-09-13 03:10:51
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_existing_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1569579251613e50bb183941-55302617%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2824d35028b020a235cb0c0185837bf9e1945ed8' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_existing_list.tpl',
      1 => 1619764012,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1569579251613e50bb183941-55302617',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bundles' => 0,
    'bundle' => 0,
    'bundle_product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_613e50bb193541_10970875',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_613e50bb193541_10970875')) {function content_613e50bb193541_10970875($_smarty_tpl) {?>

<?php  $_smarty_tpl->tpl_vars['bundle'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['bundle']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bundles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['bundle']->key => $_smarty_tpl->tpl_vars['bundle']->value) {
$_smarty_tpl->tpl_vars['bundle']->_loop = true;
?>
	<div class="pbp-bundle" data-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
		<span class="id-bundle">
			ID: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

		</span>
		<div class="pbp-products">
			<?php  $_smarty_tpl->tpl_vars['bundle_product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['bundle_product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bundle']->value->products; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['bundle_product']->key => $_smarty_tpl->tpl_vars['bundle_product']->value) {
$_smarty_tpl->tpl_vars['bundle_product']->_loop = true;
?>
				<span class="pbp-product">
					<b><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle_product']->value->product->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</b>
					<?php echo smartyTranslate(array('s'=>'Discount','mod'=>'productbundlespro'),$_smarty_tpl);?>
 :
					<?php if ($_smarty_tpl->tpl_vars['bundle_product']->value->discount_type=='money') {?>
						<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['bundle_product']->value->discount_amount),$_smarty_tpl);?>

					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['bundle_product']->value->discount_type=='percentage') {?>
						<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle_product']->value->discount_amount, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
%
					<?php }?>
				</span>
			<?php } ?>
		</div>
	</div>
<?php } ?>
<?php }} ?>
