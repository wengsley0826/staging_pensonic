<?php /* Smarty version Smarty-3.1.19, created on 2021-09-13 03:10:38
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_productlist.tpl" */ ?>
<?php /*%%SmartyHeaderCode:553447597613e50ae6f5419-13529251%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b1b6ab6d6384761c66b41c24f2aef71b775332b' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_productlist.tpl',
      1 => 1619764013,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '553447597613e50ae6f5419-13529251',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_613e50ae764d84_17866819',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_613e50ae764d84_17866819')) {function content_613e50ae764d84_17866819($_smarty_tpl) {?>

<table id="productsTable" class="table tableDnD">
	<thead>
	<tr class="nodrag nodrop">
		<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'ID','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Name','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Discount Type','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Discount','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Tax','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Action','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Position','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
	</tr>
	</thead>

	<tbody>

	<?php if (count($_smarty_tpl->tpl_vars['products']->value)>0) {?>
		<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
			<tr data-id_pbp_product="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->id_pbp_product, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
				data-id_product="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->id_product, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
				data-name="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->product->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
				data-discount_type="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->discount_type, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
				data-discount_amount="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->discount_amount, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
				data-data-discount_tax="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->discount_tax, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
			>
				<td class="id_product"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->id_product, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td class="name"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->product->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td class="discount_type"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->discount_type, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td class="discount_amount"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->discount_amount, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td class="discount_tax"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->discount_tax, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td>
					<a href="#edit" data-id_product="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->id_product, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="pbp-bundle-product-edit"><i class="icon icon-pencil"></i></a>
					<a href="#delete" data-id_product="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->id_product, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="pbp-bundle-product-delete"><i class="icon icon-trash"></i></a>
				</td>
				<td class="dragHandle pointer">
					<div class="dragGroup">
						&nbsp;
					</div>
				</td>
			</tr>
		<?php } ?>
	<?php } else { ?>
		<tr class="no-products">
			<td colspan="8">
				<div style="height:200px; padding-top:60px;" class="text-center">
					<i class="icon-warning-sign"></i> <?php echo smartyTranslate(array('s'=>'No Products to show','mod'=>'productbundlespro'),$_smarty_tpl);?>

				</div>
			</td>
		</tr>
	<?php }?>
	<tr class="cloneable hidden" style="cursor:pointer">
		<td class="id_product"></td>
		<td class="name"></td>
		<td class="discount_type"></td>
		<td class="discount_amount"></td>
		<td class="discount_tax"></td>
		<td>
			<a href="#edit" class="pbp-bundle-product-edit"><i class="icon icon-pencil"></i></a>
			<a href="#delete" class="pbp-bundle-product-delete"><i class="icon icon-trash"></i></a>
		</td>
	</tr>
	</tbody>
</table><?php }} ?>
