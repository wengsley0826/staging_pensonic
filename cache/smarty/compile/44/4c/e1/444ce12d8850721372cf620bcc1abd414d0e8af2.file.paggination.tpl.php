<?php /* Smarty version Smarty-3.1.19, created on 2021-10-01 20:09:41
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/hook/paggination.tpl" */ ?>
<?php /*%%SmartyHeaderCode:714651076156fa85c16a56-13837036%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '444ce12d8850721372cf620bcc1abd414d0e8af2' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/hook/paggination.tpl',
      1 => 1603849280,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '714651076156fa85c16a56-13837036',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'total_pages' => 0,
    'list_id' => 0,
    'page' => 0,
    'search_ajax_link' => 0,
    'p' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6156fa85c2d3a6_90404163',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6156fa85c2d3a6_90404163')) {function content_6156fa85c2d3a6_90404163($_smarty_tpl) {?>
<!-- Block search module paggnation -->
<?php if (isset($_smarty_tpl->tpl_vars['total_pages']->value)&&$_smarty_tpl->tpl_vars['total_pages']->value) {?>
    <ul class="pagination_search pagination_search_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_id']->value, ENT_QUOTES, 'UTF-8', true);?>
 pull-right" <?php if ($_smarty_tpl->tpl_vars['total_pages']->value=='1') {?>style="display: none;" <?php }?>>
        <li <?php if ($_smarty_tpl->tpl_vars['page']->value<=1) {?>class="disabled"<?php }?>>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_ajax_link']->value, ENT_QUOTES, 'UTF-8', true);?>
&page=1" class="pagination-link" data-page="1" data-list-id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
">
                <i class="fa fa-angle-double-left"></i>
            </a>
        </li>
        <li <?php if ($_smarty_tpl->tpl_vars['page']->value<=1) {?>class="disabled"<?php }?>>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_ajax_link']->value, ENT_QUOTES, 'UTF-8', true);?>
?page=<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" class="pagination-link" data-page="<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" data-list-id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
">
                <i class="fa fa-angle-left"></i>
            </a>
        </li>
        <?php $_smarty_tpl->tpl_vars['p'] = new Smarty_variable(0, null, 0);?>
        <?php while ($_smarty_tpl->tpl_vars['p']->value++<$_smarty_tpl->tpl_vars['total_pages']->value) {?>
            <?php if ($_smarty_tpl->tpl_vars['p']->value<$_smarty_tpl->tpl_vars['page']->value-2) {?>
                <li class="disabled">
                    <a href="javascript:void(0);">&hellip;</a>
                </li>
                <?php $_smarty_tpl->tpl_vars['p'] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-3, null, 0);?>
            <?php } elseif ($_smarty_tpl->tpl_vars['p']->value>$_smarty_tpl->tpl_vars['page']->value+2) {?>
                <li class="disabled">
                    <a href="javascript:void(0);">&hellip;</a>
                </li>
                <?php $_smarty_tpl->tpl_vars['p'] = new Smarty_variable($_smarty_tpl->tpl_vars['total_pages']->value, null, 0);?>
            <?php } else { ?>
                <li <?php if ($_smarty_tpl->tpl_vars['p']->value==$_smarty_tpl->tpl_vars['page']->value) {?>class="active"<?php }?>>
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_ajax_link']->value, ENT_QUOTES, 'UTF-8', true);?>
&page=<?php echo intval($_smarty_tpl->tpl_vars['p']->value);?>
" class="pagination-link <?php if ($_smarty_tpl->tpl_vars['p']->value==$_smarty_tpl->tpl_vars['page']->value) {?>active<?php }?>" data-page="<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
" data-list-id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value;?>
</a>
                </li>
            <?php }?>
        <?php }?>
        <li <?php if ($_smarty_tpl->tpl_vars['page']->value>=$_smarty_tpl->tpl_vars['total_pages']->value) {?>class="disabled"<?php }?>>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_ajax_link']->value, ENT_QUOTES, 'UTF-8', true);?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" class="pagination-link" data-page="<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" data-list-id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
">
                <i class="fa fa-angle-right"></i>
            </a>
        </li>
        <li <?php if ($_smarty_tpl->tpl_vars['page']->value>=$_smarty_tpl->tpl_vars['total_pages']->value) {?>class="disabled"<?php }?>>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_ajax_link']->value, ENT_QUOTES, 'UTF-8', true);?>
&page=<?php echo $_smarty_tpl->tpl_vars['total_pages']->value;?>
" class="pagination-link" data-page="<?php echo $_smarty_tpl->tpl_vars['total_pages']->value;?>
" data-list-id="<?php echo $_smarty_tpl->tpl_vars['list_id']->value;?>
">
                <i class="fa fa-angle-double-right"></i>
            </a>
        </li>
    </ul>
<?php }?><?php }} ?>
