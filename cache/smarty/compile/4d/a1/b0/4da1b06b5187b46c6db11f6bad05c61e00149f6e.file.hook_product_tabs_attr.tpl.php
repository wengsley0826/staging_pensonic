<?php /* Smarty version Smarty-3.1.19, created on 2021-09-13 03:05:18
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/front/hook_product_tabs_attr.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1782641391613e4f6eebaea1-32637598%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4da1b06b5187b46c6db11f6bad05c61e00149f6e' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/front/hook_product_tabs_attr.tpl',
      1 => 1619764011,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1782641391613e4f6eebaea1-32637598',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'pbp_general' => 0,
    'tabs_collection' => 0,
    'tab' => 0,
    'i' => 0,
    'counter' => 0,
    'bundle' => 0,
    'parent_product' => 0,
    'link' => 0,
    'attribute_group' => 0,
    'bundle_product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_613e4f6eee98f2_69547114',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_613e4f6eee98f2_69547114')) {function content_613e4f6eee98f2_69547114($_smarty_tpl) {?>

<div class="row">
	<div id="pbp-product-tabs" class="col-xs-12 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_location'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 pbp_display_attr">
		<?php if (count($_smarty_tpl->tpl_vars['tabs_collection']->value)>1) {?>
			<div class="tab-links">
				<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(1, null, 0);?>
				<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs_collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
					<a data-tab_id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="<?php if ($_smarty_tpl->tpl_vars['i']->value==1) {?>active<?php }?>"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a>
					<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
				<?php } ?>
			</div>
		<?php }?>

		<?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable(1, null, 0);?>
		<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs_collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
			<div class="tab-content-wrapper">
				<div class="pbp-tab-content pbp-tab-content-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="<?php if ($_smarty_tpl->tpl_vars['counter']->value>1) {?>display:none;<?php }?>">
					<?php  $_smarty_tpl->tpl_vars['bundle'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['bundle']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value->bundles; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['bundle']->key => $_smarty_tpl->tpl_vars['bundle']->value) {
$_smarty_tpl->tpl_vars['bundle']->_loop = true;
?>
						<div class="bundle col-xs-12 col-sm-12 col-md-6" data-id_bundle="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
							<div class="content">
								<span class="bundle-title">
									<?php echo smartyTranslate(array('s'=>'Buy it with ','mod'=>'productbundlespro'),$_smarty_tpl);?>

									<span class="product-title">
										<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->products[0]->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

									</span>
								</span>

								<div class="parent">								
									<img itemprop="image" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['parent_product']->value->link_rewrite,$_smarty_tpl->tpl_vars['parent_product']->value->cover_image,'medium_default'), ENT_QUOTES, 'UTF-8', true);?>
" />
									<span name="parent-product-name" class="parent-product-name">
										<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['parent_product']->value->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

									</span>
									
									<?php if (count($_smarty_tpl->tpl_vars['parent_product']->value->attribute_groups)>0) {?>
										<select class="parent_product_ipa">
											<?php  $_smarty_tpl->tpl_vars['attribute_group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attribute_group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['parent_product']->value->attribute_groups; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attribute_group']->key => $_smarty_tpl->tpl_vars['attribute_group']->value) {
$_smarty_tpl->tpl_vars['attribute_group']->_loop = true;
?>
												<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['attribute_group']->value['id_product_attribute'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
														<?php if ($_smarty_tpl->tpl_vars['attribute_group']->value['default']==1) {?>selected="selected"<?php }?>>
															<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['attribute_group']->value['label'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

												</option>
											<?php } ?>
										</select>
									<?php }?>									
								</div>

								<div class="bundle-products">
									<?php if (!empty($_smarty_tpl->tpl_vars['bundle']->value->products)) {?>
										<?php  $_smarty_tpl->tpl_vars['bundle_product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['bundle_product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bundle']->value->products; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['bundle_product']->key => $_smarty_tpl->tpl_vars['bundle_product']->value) {
$_smarty_tpl->tpl_vars['bundle_product']->_loop = true;
?>
											<div class="bundle-product" data-id_product="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle_product']->value->id_product, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
												<div class="image">
													<a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle_product']->value->url, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
														<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['bundle_product']->value->link_rewrite,$_smarty_tpl->tpl_vars['bundle_product']->value->id_image,'medium_default'), ENT_QUOTES, 'UTF-8', true);?>
" class="bundle_product_thumb"/>
													</a>	
												</div>

												<div class="info">
													<?php if ($_smarty_tpl->tpl_vars['bundle_product']->value->discount_type=='percentage') {?>
														<span class="pbp-discount">
															<?php echo smartyTranslate(array('s'=>'SAVE:','mod'=>'productbundlespro'),$_smarty_tpl);?>

															<span class="pbp-discount-amount"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>floatval($_smarty_tpl->tpl_vars['bundle_product']->value->product->discount_saving)),$_smarty_tpl);?>
 (<?php echo sprintf("%.0f",$_smarty_tpl->tpl_vars['bundle_product']->value->discount_amount);?>
%)</span>
														</span>
														<span class="pbp-discount">
															<?php echo smartyTranslate(array('s'=>'Offer Price:','mod'=>'productbundlespro'),$_smarty_tpl);?>

															<span class="pbp-offer-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>floatval($_smarty_tpl->tpl_vars['bundle_product']->value->product->discount_price)),$_smarty_tpl);?>
</span>
														</span>
													<?php } else { ?>	
														<span class="pbp-discount">
															<?php echo smartyTranslate(array('s'=>'SAVE:','mod'=>'productbundlespro'),$_smarty_tpl);?>

															<span class="pbp-discount-amount"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>floatval($_smarty_tpl->tpl_vars['bundle_product']->value->product->discount_saving)),$_smarty_tpl);?>
</span>
														</span>
														<span class="pbp-discount">
															<?php echo smartyTranslate(array('s'=>'Offer Price:','mod'=>'productbundlespro'),$_smarty_tpl);?>

															<span class="pbp-offer-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>floatval($_smarty_tpl->tpl_vars['bundle_product']->value->product->discount_price)),$_smarty_tpl);?>
</span>
														</span>														
													<?php }?>
													<?php if (count($_smarty_tpl->tpl_vars['bundle_product']->value->attribute_groups)>0) {?>
														<select class="bundle_product_ipa">
															<?php  $_smarty_tpl->tpl_vars['attribute_group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attribute_group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bundle_product']->value->attribute_groups; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attribute_group']->key => $_smarty_tpl->tpl_vars['attribute_group']->value) {
$_smarty_tpl->tpl_vars['attribute_group']->_loop = true;
?>
																<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['attribute_group']->value['id_product_attribute'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
																		<?php if ($_smarty_tpl->tpl_vars['attribute_group']->value['default']==1) {?>selected="selected"<?php }?>
																><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['attribute_group']->value['label'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
															<?php } ?>
														</select>
													<?php }?>
												</div>
											</div>
										<?php } ?>
									<?php }?>
								</div>

								<div class="box-info-product">
									<button type="submit" name="Submit" class="btn_add_bundle_cart exclusive">
										<span>
											<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'productbundlespro'),$_smarty_tpl);?>

											<?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_display_bundle_total']==1) {?>
												-
											<?php }?>
											<div class="pbp_bundle_total" style="display: inline">
												<?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_display_bundle_total']==1) {?>
													<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['bundle']->value->bundle_price_discounted),$_smarty_tpl);?>

												<?php }?>
											</div>
										</span>
									</button>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
		<?php } ?>
	</div>
</div>


<script>
	$(document).ready(function() {
		pbp_front_ajax_url = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['link']->value->getModuleLink('productbundlespro','ajax',array()));?>
";
		var pbp_front_product_controller = new PBPFrontProductController('#pbp-product-tabs');
	});
</script>

<?php }} ?>
