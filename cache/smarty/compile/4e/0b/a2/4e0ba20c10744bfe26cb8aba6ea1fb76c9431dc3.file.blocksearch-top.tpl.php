<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:38:23
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/hook/blocksearch-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:291818617615c0eff333559-95733194%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e0ba20c10744bfe26cb8aba6ea1fb76c9431dc3' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/hook/blocksearch-top.tpl',
      1 => 1603849279,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '291818617615c0eff333559-95733194',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id_lang' => 0,
    'page_search' => 0,
    'YBC_BLOCKSEARCH_PLACEHOLDER' => 0,
    'search_query' => 0,
    'button_color' => 0,
    'button_hover' => 0,
    'main_color' => 0,
    'text_color' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0eff3476e2_53380856',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0eff3476e2_53380856')) {function content_615c0eff3476e2_53380856($_smarty_tpl) {?>
<!-- Block search module TOP -->
<script type="text/javascript">
var id_lang =<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
;
</script>
<div id="search_block_top" class="col-sm-12 col-md-6 col-lg-5 clearfix	pull-right">
	<form id="ybc_searchbox" method="post" action="<?php if (isset($_smarty_tpl->tpl_vars['page_search']->value)&&$_smarty_tpl->tpl_vars['page_search']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page_search']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" >
        <input class="search_query form-control ybc_search_query" type="text" id="search_query_top" name="search_query" placeholder="<?php if (isset($_smarty_tpl->tpl_vars['YBC_BLOCKSEARCH_PLACEHOLDER']->value)&&$_smarty_tpl->tpl_vars['YBC_BLOCKSEARCH_PLACEHOLDER']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_BLOCKSEARCH_PLACEHOLDER']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Enter product name ...','mod'=>'ybc_blocksearch'),$_smarty_tpl);?>
<?php }?>" value="<?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
" />
		<button type="submit"  class="btn btn-default button-search" >
			<span><?php echo smartyTranslate(array('s'=>'Search','mod'=>'ybc_blocksearch'),$_smarty_tpl);?>
</span>
		</button>
	</form>
	<div class="search_block_results" id="search_block_results">
	</div>
</div>

<style type="text/css">
	<?php if (isset($_smarty_tpl->tpl_vars['button_color']->value)&&$_smarty_tpl->tpl_vars['button_color']->value) {?>
		#search_block_top .btn.button-search{
			background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_color']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}
		#search_block_top .btn.button-search{
			color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_color']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}

	<?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['button_hover']->value)&&$_smarty_tpl->tpl_vars['button_hover']->value) {?>
		#search_block_top .btn.button-search:hover{
			background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['button_hover']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}
	<?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['main_color']->value)&&$_smarty_tpl->tpl_vars['main_color']->value) {?>
		/*span.title_group{
			color:;
			border-color: ;
		}*/
		.title_group span.total,
		.search_block_results .box_button_see{
			background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['main_color']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}
		.wrap_header_tap ul li.active a{
			background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['main_color']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}
	<?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['text_color']->value)&&$_smarty_tpl->tpl_vars['text_color']->value) {?>
		#search_block_top .btn.button-search::before{
			color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text_color']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}
		.title_group span.total{
			color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text_color']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}
		a.ets_link_see{
			color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text_color']->value, ENT_QUOTES, 'UTF-8', true);?>
 !important;
		}
		.wrap_header_tap ul li.active a{
			color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text_color']->value, ENT_QUOTES, 'UTF-8', true);?>
 !important;
		}
		.wrap_header_tap ul li.active a span{
			color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text_color']->value, ENT_QUOTES, 'UTF-8', true);?>
;
		}
	<?php }?>

</style>
<!-- /Block search module TOP -->
<?php }} ?>
