<?php /* Smarty version Smarty-3.1.19, created on 2021-09-13 03:10:42
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_existing.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1200702096613e50b2269217-80860119%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5084639f585e4eae13cead9efebb7d3e004f2b10' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_existing.tpl',
      1 => 1619764012,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1200702096613e50b2269217-80860119',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tabs_collection' => 0,
    'bundle' => 0,
    'tab' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_613e50b22a20d5_87024389',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_613e50b22a20d5_87024389')) {function content_613e50b22a20d5_87024389($_smarty_tpl) {?>

<div id="pbp-bundle_existing" class="panel">
	<h3>Add Existing Bundle</h3>

	<div class="row">

		
		<div class="col-xs-6">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label class="control-label">
							<span class="label-tooltip" title="Choose a tab">Choose a tab</span>
						</label>

						<select id="id_tab" name="id_tab">
							<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs_collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
								<?php if (isset($_smarty_tpl->tpl_vars['bundle']->value)&&$_smarty_tpl->tpl_vars['bundle']->value->id_tab==$_smarty_tpl->tpl_vars['tab']->value->id_tab) {?>
									<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id_tab, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" selected><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
								<?php } else { ?>
									<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id_tab, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
								<?php }?>
							<?php } ?>
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">
							<span class="label-tooltip" title="type in the first few characters of a product name to select a bundle from"><?php echo smartyTranslate(array('s'=>'Type in product name','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span>
						</label>

						<div class="input-group">
							<input id="product_search" type="text" name="product_search" value="">
							<span class="input-group-addon">
								<i class="icon-search"></i>
							</span>
						</div>

						
						<div id="product-search-results">
							<input name="id_product" type="hidden">

							<table id="search-results-table" class="table">
								<thead>
								<tr class="nodrag nodrop">
									<th style="width:10%"><span class="title_box"><?php echo smartyTranslate(array('s'=>'ID','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
									<th style="width:30%"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Ref','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
									<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Name','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
								</tr>
								</thead>
								<tbody>
								<tr class="cloneable hidden" style="cursor:pointer">
									<td class="id_product" data-bind="id_product"></td>
									<td class="name" data-bind="name"></td>
								</tr>
								</tbody>
							</table>
						</div>
						



					</div>

				</div>
			</div>
		</div>
		

		
		<div class="col-xs-6">
			<div class="row">
				<div class="col-xs-12">
					<div id="pbp-bundles-existing-list"></div>
				</div>
			</div>
		</div>
		

	</div>


	<div class="row">
		<div class="panel-footer">
			<a id="pbp-edit-product-cancel" href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>

			<button type="submit" id="pbp-existing-bundle-add" class="btn btn-default pull-right">
				<i class="process-icon-check icon-check"></i> Add
			</button>
		</div>

	</div><?php }} ?>
