<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:38:23
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/phgoogletagmanager/views/templates/hook/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1576173235615c0eff016ec0-91653849%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5813c9d3a3930002dfc710ba05c72e9a37609217' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/phgoogletagmanager/views/templates/hook/header.tpl',
      1 => 1594691417,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1576173235615c0eff016ec0-91653849',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'valuta' => 0,
    'meta_title' => 0,
    'ph_analytics_uacode' => 0,
    'ph_allowLinker' => 0,
    'ph_autoLinkDomains' => 0,
    'ph_fbpixel_activ' => 0,
    'ph_fbpixel_code' => 0,
    'ph_user_id_custom_dimension_nr' => 0,
    'ph_ecomm_prodid_custom_dimension_nr' => 0,
    'ph_ecomm_pagetype_custom_dimension_nr' => 0,
    'ph_ecomm_totalvalue_custom_dimension_nr' => 0,
    'ph_customer_id_dimension_nr' => 0,
    'FBuser' => 0,
    'trackuid' => 0,
    'uid' => 0,
    'cid' => 0,
    'product' => 0,
    'tgmm_v' => 0,
    'ph_hotjar_activ' => 0,
    'ph_hotjar_code' => 0,
    'ph_inspectlet_activ' => 0,
    'ph_inspectlet_code' => 0,
    'ph_pinterest_activ' => 0,
    'ph_pinterest_code' => 0,
    'ph_GTS_activ' => 0,
    'ph_adwords_activ' => 0,
    'ph_remarketing_activ' => 0,
    'ph_GTS_Store_ID' => 0,
    'ph_GTS_Locale' => 0,
    'ph_GTS_Shopping_ID' => 0,
    'ph_GTS_Shopping_Account_ID' => 0,
    'ph_GTS_Shopping_Country' => 0,
    'ph_GTS_Shopping_Language' => 0,
    'ph_GCR_BADGE_activ' => 0,
    'ph_GCR_OPTIN_activ' => 0,
    'ph_GCR_ID' => 0,
    'action' => 0,
    'ph_GCR_est_delivery_days' => 0,
    'ph_GCR_est_delivery_date' => 0,
    'ph_crazyegg_activ' => 0,
    'ph_crazyegg_code' => 0,
    'ph_shop_name' => 0,
    'impressions' => 0,
    'products' => 0,
    'product_ids' => 0,
    'remarketing' => 0,
    'currentCateg' => 0,
    'parentCateg' => 0,
    'adwords' => 0,
    'info' => 0,
    'AdwConvId' => 0,
    'AdwConvLg' => 0,
    'AdwConvLb' => 0,
    'product_name' => 0,
    'gtm' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0eff095d45_35678984',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0eff095d45_35678984')) {function content_615c0eff095d45_35678984($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/var/www/html/pensonicstore.claritascloud.com/tools/smarty/plugins/modifier.replace.php';
?>
<script type="text/javascript" data-keepinline="true">
	window.dataLayer = window.dataLayer || [];
</script>
<script type="text/javascript" data-keepinline="true">
    	var currencyCode = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['valuta']->value);?>
";

    	var ph_page_name = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
    	var ph_analytics_uacode = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_analytics_uacode']->value);?>
";
    	var ph_no_track_backoffice = false;
    	var ph_allowLinker = <?php if (isset($_smarty_tpl->tpl_vars['ph_allowLinker']->value)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_allowLinker']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>false<?php }?>;
    	var ph_autoLinkDomains = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_autoLinkDomains']->value, ENT_QUOTES, 'UTF-8', true);?>
";
    	var ph_fbpixel_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_fbpixel_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_fbpixel_code = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_fbpixel_code']->value);?>
";
    	var ph_User_ID_Custom_Dimension_Nr = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_user_id_custom_dimension_nr']->value);?>
;
    	var ph_ecomm_prodid_custom_dimension_nr = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_ecomm_prodid_custom_dimension_nr']->value);?>
;
    	var ph_ecomm_pagetype_custom_dimension_nr = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_ecomm_pagetype_custom_dimension_nr']->value);?>
;
    	var ph_ecomm_totalvalue_custom_dimension_nr = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_ecomm_totalvalue_custom_dimension_nr']->value);?>
;
    	var ph_customer_id_dimension_nr = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_customer_id_dimension_nr']->value);?>
;
    	var FBuser = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['FBuser']->value);?>
";
		<?php if (true==$_smarty_tpl->tpl_vars['trackuid']->value) {?>
			var ph_UID = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['uid']->value);?>
";
			var ph_CID = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['cid']->value);?>
";
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['product']->value['embedded_attributes']['price'])&&$_smarty_tpl->tpl_vars['product']->value['embedded_attributes']['price']) {?>
			var ph_product_price = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['product']->value['embedded_attributes']['price']);?>
;
		<?php }?>
    	var ph_tgmm_v = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['tgmm_v']->value);?>
";

    	var ph_hotjar_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_hotjar_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_hotjar_code = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_hotjar_code']->value);?>
";
    	
    	var ph_inspectlet_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_inspectlet_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_inspectlet_code = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_inspectlet_code']->value);?>
";

    	var ph_pinterest_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_pinterest_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_pinterest_code = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_pinterest_code']->value);?>
";

    	var ph_GTS_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_GTS_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_adwords_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_adwords_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_remarketing_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_remarketing_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;

    	var ph_GTS_Store_ID = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_GTS_Store_ID']->value);?>
";
    	var ph_GTS_Localee = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_GTS_Locale']->value);?>
";
    	var ph_GTS_Shopping_ID = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_GTS_Shopping_ID']->value);?>
";
    	var ph_GTS_Shopping_Account_ID = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_GTS_Shopping_Account_ID']->value);?>
";
    	var ph_GTS_Shopping_Country = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_GTS_Shopping_Country']->value);?>
";
    	var ph_GTS_Shopping_Language = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_GTS_Shopping_Language']->value);?>
";

    	var ph_GCR_BADGE_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_GCR_BADGE_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_GCR_OPTIN_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_GCR_OPTIN_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_GCR_ID = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_GCR_ID']->value, ENT_QUOTES, 'UTF-8', true);?>
";
		<?php if (empty($_smarty_tpl->tpl_vars['action']->value)!=true&&($_smarty_tpl->tpl_vars['action']->value['action']=="purchase"||$_smarty_tpl->tpl_vars['action']->value['action']=="purchase_already_sent")) {?>
	    	var ph_GCR_orderid = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value['id'], ENT_QUOTES, 'UTF-8', true);?>
";
	    	var ph_GCR_email = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value['email'], ENT_QUOTES, 'UTF-8', true);?>
";
	    	var ph_GCR_delivery_country = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value['delivery_country'], ENT_QUOTES, 'UTF-8', true);?>
";
	    	var ph_GCR_est_delivery_days = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_GCR_est_delivery_days']->value, ENT_QUOTES, 'UTF-8', true);?>
";
	    	var ph_GCR_est_delivery_date = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_GCR_est_delivery_date']->value, ENT_QUOTES, 'UTF-8', true);?>
";
		<?php } else { ?>
	    	var ph_GCR_orderid = "";
	    	var ph_GCR_email = "";
	    	var ph_GCR_delivery_country = "";
	    	var ph_GCR_est_delivery_days = "";
	    	var ph_GCR_est_delivery_date = "";
		<?php }?>

    	var ph_crazyegg_activ = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ph_crazyegg_activ']->value, ENT_QUOTES, 'UTF-8', true);?>
;
    	var ph_crazyegg_code = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_crazyegg_code']->value);?>
";


    	var ph_shop_name = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['ph_shop_name']->value);?>
";

		var removeFromCartClick = function (e) {
				var mybtn = (this).closest('dt');
				var qtity = $(mybtn).find('.quantity').text();
				var mydataid = $(mybtn).attr('data-id') ;
				var product_id = mydataid.split('_')[3] ;
				var attribute_id = mydataid.split('_')[4] ;
				//console.log("remove from cart: " + product_id + "-" + attribute_id + " x " + qtity);
				window.dataLayer.push({
				'event': 'removeFromCart',
				'ecommerce': {
					'remove': {
						'products': [{
							'id': product_id + "-" + attribute_id,
							'quantity': qtity
						}]
					}
				}
			});
		}
		var removeFromCartClick_ps171 = function (e) {
				var mybtn = (this).closest('.product-line-grid');
				var qtity = $(mybtn).find('.js-cart-line-product-quantity').val();
				var product_id = $(this).attr('data-id-product');
				var attribute_id = $(this).attr('data-id-product-attribute');
				console.log("remove from cart 1.7: " + product_id + "-" + attribute_id + " x " + qtity);
				window.dataLayer.push({
				'event': 'removeFromCart',
				'ecommerce': {
					'remove': {
						'products': [{
							'id': product_id + "-" + attribute_id,
							'quantity': qtity
						}]
					}
				}
			});
		}
    	window.addEventListener('load', function() {
			$(document).on('click', '.ajax_cart_block_remove_link',
				removeFromCartClick
			);
			$(document).on('mousedown', 'BODY#cart .cart-items a.remove-from-cart',
				removeFromCartClick_ps171
			);
		});
    	
		if (typeof(ph_product_price) == 'number'){
			var productPrice = ph_product_price;
		}
		if(typeof(productPrice) == 'undefined'){
			var productPrice = 0;
			var productPriceFloat = 0;
		}else{
			if(isFloat(productPrice)){
				var productPriceFloat = productPrice;
			} else {
				var productPriceFloat = productPrice.toFixed(2);
			}
		}
		if (typeof(sharing_name) == 'undefined' ){
			var sharing_name = ph_page_name;
		}

		
		<?php if (empty($_smarty_tpl->tpl_vars['action']->value)!=true||empty($_smarty_tpl->tpl_vars['impressions']->value)!=true) {?>

			<?php if (empty($_smarty_tpl->tpl_vars['impressions']->value)==true) {?>
				<?php if ($_smarty_tpl->tpl_vars['action']->value['action']=="purchase") {?>
				/*PURCHASE*/

				var id = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['id']);?>
";
				var order_ref = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['order_ref']);?>
";
				var revenue = "<?php echo floatval($_smarty_tpl->tpl_vars['action']->value['revenue']);?>
";
				var tax = "<?php echo floatval($_smarty_tpl->tpl_vars['action']->value['tax']);?>
";
				var shipping = "<?php echo floatval($_smarty_tpl->tpl_vars['action']->value['shipping']);?>
";
				var products = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['products']->value));?>
;
				<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
					<?php ob_start();?><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['product']->value['id']);?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->createLocalArrayVariable('product_ids', null, 0);
$_smarty_tpl->tpl_vars['product_ids']->value[] = $_tmp1;?>
				<?php } ?>
				var product_ids = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['product_ids']->value));?>
;
				var currencyCode = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['valuta']->value);?>
";
				

				window.dataLayer.push({
						"page": "order-confirmation",
						"ecommerce": {
								"purchase": {
										"actionField": {
												"id": id,
												"revenue": revenue,
												"tax": tax,
												"shipping": shipping
										},
										"products": products
								}
						}
				});
				


				<?php } elseif ($_smarty_tpl->tpl_vars['action']->value['action']=="checkout") {?>
				/*CHECKOUT*/
				var step = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['step']);?>
";
				var option = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['option']);?>
";
				var products = '';
				<?php if (empty($_smarty_tpl->tpl_vars['products']->value)!=true) {?>
				products = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['products']->value));?>

				<?php }?>

				

					window.dataLayer.push({
							"page": "checkout-step-" + step,
							"ecommerce": {
									"checkout": {
											"actionField": {
													"step": step,
													"option": option
											},
											"products": products
									}
							}
					});

				

				<?php } else { ?>

					<?php if ($_smarty_tpl->tpl_vars['action']->value['page']=='product') {?>
					/*VIEW  PRODUCT DETAIL*/
					var filtered_keys = function(obj, filter) {
					  var key, keys = [];
					  for (key in obj) {
					    if (obj.hasOwnProperty(key) && filter.test(key)) {
					      keys.push(key);
					    }
					  }
					  return keys;
					}
			    	
					if (typeof(id_product) == "undefined"){
						if(typeof(prestashop) == "undefined" ){
								<?php if (isset($_smarty_tpl->tpl_vars['products']->value[0]['id_product'])) {?>
									id_product = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['products']->value[0]['id_product']);?>
";
								<?php } else { ?>
									id_product = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['remarketing']->value['id_product']);?>
";
								<?php }?>

						} else{

							var findproductid = filtered_keys( prestashop.page.body_classes , /^product-id-\d+$/ );
							id_product = findproductid[0].replace('product-id-','');
						}
					}

					var action = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['action']);?>
"
					var list = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['actionFieldValue']);?>
";
					var products = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['products']->value));?>
;
					var currencyCode = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['valuta']->value);?>
";
						
						window.dataLayer.push({
								"page": "viewProduct",
								"ecommerce": {
										'detail': {
												"actionField": {
														"list": list
												},
												"products": products
										}
								}
						});
						



					<?php }?>

				<?php }?>

			<?php } else { ?>

				/*IMPRESSIONS PRODUCTS*/
				var impressions = [];
				var maxProducts = 35;
				<?php if (empty($_smarty_tpl->tpl_vars['products']->value)!=true) {?>
					var impressions =  <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['products']->value));?>
;
					
				<?php }?>;

				

				while(impressions.length){
					var impressions_part = impressions.splice(0,maxProducts);
					window.dataLayer.push({
							"page": "viewCategory",
							"currentCateg": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['currentCateg']->value);?>
",
							"parentCateg": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['parentCateg']->value);?>
",

							"ecommerce": {
									"currencyCode": currencyCode,
									/*"impressions": impressions_part*/
							}
					});
				}

				/*CLICK PRODUCT ON CATEGORY*/
				var clickCallBack = function (e) {
						var url = $(this).attr('href');
						window.dataLayer.push({
								'event': 'productClick',
								'ecommerce': {
										'click': {
												'actionField': {'list': 'Category Listing'},      // Optional list property.*/
												'products': [{
														'name': this.text.trim()
														//'id': productObj.id,
														//'price': $,
														//'brand': productObj.brand,
														//'category': productObj.cat,
														//'variant': productObj.variant,
														//'position': productObj.position
												}]
										}
								},
								'eventCallback': function () {
									document.location = url
								}
						});
					/*	$(document).on('click', '.product-name', clickCallBack); */
						/*e.preventDefault();*/
				};
		    	window.addEventListener('load', function() {
					$(document).on('click', '.product-name , BODY#category .products article a', clickCallBack);
				});

				

			<?php }?>

		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['action']->value)) {?>
		/* for all pages*/
		var action = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['action']);?>
"
		var list = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['actionFieldValue']);?>
";
		var products = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['products']->value));?>
;
		var currencyCode = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['valuta']->value);?>
";
			
			window.addEventListener('load', function() {

				$(document).on('click', '.ajax_add_to_cart_button , #add_to_cart > button > span , .add-to-cart', function (e) {
					var products = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['products']->value));?>
;

					if(products.length>1){/* Fix For PS1.7  */
					// if(typeof(products) == "undefined" ){
					  // if(!(typeof(products[0]) == "undefined" )){
					      var products_quickview = $(this).closest('.quickview');
					      if(products_quickview.size()>0){
					          var products_quickview = $(this).closest('.quickview');
					          //if(typeof(ph_merchant_center_id) == "undefined")
					          var products_name = products_quickview.find('H1').text();
					          var products_id = products_quickview.find('INPUT[name=id_product]').attr('value') ;    /*reference*/
					          var products_id_product =  products_quickview.find('INPUT[name=id_product]').attr('value');
					          var products_reference = products_quickview.find('INPUT[name=id_product]').attr('value') ;
					          var products_price = products_quickview.find('.product-price span').attr('content') ;
					          var products_quantity = products_quickview.find('.qty input').attr('value'); ;
					          var products_ean13 = '' ;
					          var products_upc = '' ;
					          var products_category = '' ;
					          var products = [{"reference":products_reference,"ean13":products_ean13,"upc":products_upc,"id":products_id,"id_product":products_id_product,"name":products_name,"price":products_price,"quantity":products_quantity,"category":products_category,"list":"QuickView"}];
					      }
					  // }
					}

					window.dataLayer.push({
							'event': 'addToCart',
							'ecommerce': {
									'currencyCode': currencyCode,
									'add': {
											'products': products
									}
							}
					});
				});
			});
			
		<?php }?>

		

		<?php if (empty($_smarty_tpl->tpl_vars['adwords']->value)!=true) {?>
		if (typeof(id) == "undefined"){
			<?php if (isset($_smarty_tpl->tpl_vars['action']->value['id'])) {?>id = <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['id']);?>
;<?php }?>
		}
		if (typeof(order_ref) == "undefined"){
			<?php if (isset($_smarty_tpl->tpl_vars['action']->value['order_ref'])) {?>order_ref = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['action']->value['order_ref']);?>
";<?php }?>
		}
		if (typeof(id) != "undefined"){
			window.dataLayer.push({
					"google_conversion_id": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['adwords']->value['conversion_id']);?>
",
					"google_conversion_language": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['adwords']->value['conversion_language']);?>
",
					"google_conversion_format": "3",
					"google_conversion_color": "ffffff",
					"google_conversion_label": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['adwords']->value['conversion_label']);?>
",
					<?php if (isset($_smarty_tpl->tpl_vars['adwords']->value['conversion_value'])) {?>"google_conversion_value": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['valuta']->value);?>
<?php echo floatval(smarty_modifier_replace($_smarty_tpl->tpl_vars['adwords']->value['conversion_value'],'\'',''));?>
",<?php }?>
					"google_conversion_only": false,
					"currency_code": currencyCode,
					"order_id": id,
					"order_reference": order_ref
			});
		}else{
			window.dataLayer.push({
					"google_conversion_id": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['adwords']->value['conversion_id']);?>
",
					"google_conversion_language": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['adwords']->value['conversion_language']);?>
",
					"google_conversion_format": "3",
					"google_conversion_color": "ffffff",
					"google_conversion_label": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['adwords']->value['conversion_label']);?>
",
					<?php if (isset($_smarty_tpl->tpl_vars['adwords']->value['conversion_value'])) {?>"google_conversion_value": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['valuta']->value);?>
<?php echo floatval(smarty_modifier_replace($_smarty_tpl->tpl_vars['adwords']->value['conversion_value'],'\'',''));?>
",<?php }?>
					"google_conversion_only": false,
					"currency_code": currencyCode,
			});
		}
		<?php }?>

		
		<?php if (empty($_smarty_tpl->tpl_vars['remarketing']->value)!=true) {?>
		
			
			var ph_fb_prodid = [];
			var ph_fb_prodid_attribute = '<?php if (isset($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'])) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>';

			<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['page_type'])&&$_smarty_tpl->tpl_vars['remarketing']->value['page_type']=='cart') {?>
				if(typeof(prestashop) == "undefined" ){
					for(myindex = 0, len = products.length ; myindex < len ; ++ myindex ){
						// ph_fb_prodid.push( prestashop.cart.products[myindex].id_product+'v'+prestashop.cart.products[myindex].id_product_attribute );
						ph_fb_prodid.push( products[myindex].id_product );
					};
				}else{
					for(myindex = 0, len = prestashop.cart.products.length ; myindex < len ; ++ myindex ){
						// ph_fb_prodid.push( prestashop.cart.products[myindex].id_product+'v'+prestashop.cart.products[myindex].id_product_attribute );
						ph_fb_prodid.push( prestashop.cart.products[myindex].id_product );
					};
				}
				// console.log( ph_fb_prodid );
			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['page_type'])&&$_smarty_tpl->tpl_vars['remarketing']->value['page_type']=='product') {?>
				if(typeof(prestashop) == "undefined" ){
					ph_fb_prodid = id_product;
				} else {
					var findproductidprodpage = filtered_keys( prestashop.page.body_classes , /^product-id-\d+$/ );
					ph_fb_id_product_prodpage = findproductidprodpage[0].replace('product-id-','');
					ph_fb_prodid.push( ph_fb_id_product_prodpage );
				}

			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['page_type'])&&$_smarty_tpl->tpl_vars['remarketing']->value['page_type']=='purchase') {?>
				<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
					
					ph_fb_prodid.push( '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');
				<?php } ?>
			<?php }?>


			var google_tag_params = {
				<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['id_product'])) {?>
					ecomm_prodid: <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['remarketing']->value['id_product']);?>
,
				<?php } else { ?>
				<?php }?>
					ecomm_pagetype: "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['remarketing']->value['page_type']);?>
",
				<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['total'])) {?>
					ecomm_totalvalue: "<?php echo floatval($_smarty_tpl->tpl_vars['remarketing']->value['total']);?>
",
				<?php } else { ?>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['content_type'])) {?>
					content_type: "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['remarketing']->value['content_type']);?>
",
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['ecomm_category'])) {?>
					ecomm_category: "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['remarketing']->value['ecomm_category']);?>
",
				<?php }?>
			};
			<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['info'])) {?>
				<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['info']['category_name'])) {?>
					google_tag_params["category"] = "<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['info']['category_name'])) {?><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['remarketing']->value['info']['category_name']);?>
<?php }?>";
				<?php }?>

				<?php if (isset($_smarty_tpl->tpl_vars['info']->value['product_name'])) {?>
					google_tag_params["product"] = "<?php if (isset($_smarty_tpl->tpl_vars['remarketing']->value['info']['product_name'])) {?><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['remarketing']->value['info']['product_name']);?>
<?php }?>";
				<?php }?>
				window.dataLayer.push({
					"google_tag_params": google_tag_params
				});
			<?php } else { ?>
				window.dataLayer.push({
					"google_tag_params": google_tag_params
				});
			<?php }?>
		<?php }?>
		
		<?php if (isset($_smarty_tpl->tpl_vars['trackuid']->value)||isset($_smarty_tpl->tpl_vars['AdwConvId']->value)) {?>
			window.dataLayer.push({
				<?php if (true==$_smarty_tpl->tpl_vars['trackuid']->value) {?>"UID_Cookie": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['uid']->value);?>
",<?php }?>
				<?php if (true==$_smarty_tpl->tpl_vars['trackuid']->value) {?>"CID_Cookie": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['cid']->value);?>
",<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['AdwConvId']->value)) {?>"AdwConvId": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['AdwConvId']->value);?>
",<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['AdwConvLg']->value)) {?>"AdwConvLg": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['AdwConvLg']->value);?>
",<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['AdwConvLb']->value)) {?>"AdwConvLb": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['AdwConvLb']->value);?>
",<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['product_name']->value)) {?>"product_name": "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product_name']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
",<?php }?>
			});
		<?php }?>
		function isFloat(n) {
			return n === +n && n !== (n|0);
		}
		function isInteger(n) {
		    return n === +n && n === (n|0);
		}
		function createCookie(name,value,days,path) {
		    if (days) {
		        var date = new Date();
		        date.setTime(date.getTime()+(days*24*60*60*1000));
		        var expires = "; expires="+date.toGMTString();
		    }
		    else var expires = "";
		    document.cookie = name+"="+value+expires+"; path="+path;
		}
		function eraseCookie(cookie_name,path) {
			createCookie(cookie_name,"",0,path);
		}
		function getCookie(name) {
		  var value = "; " + document.cookie;
		  var parts = value.split("; " + name + "=");
		  if (parts.length == 2) return parts.pop().split(";").shift();
		}

</script>
<script data-keepinline="true">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['gtm']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');</script>

<?php }} ?>
