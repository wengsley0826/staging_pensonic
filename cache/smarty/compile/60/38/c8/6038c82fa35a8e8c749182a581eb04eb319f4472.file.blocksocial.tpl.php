<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:38:23
         compiled from "/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/modules/blocksocial/blocksocial.tpl" */ ?>
<?php /*%%SmartyHeaderCode:699178462615c0eff453ca7-92162170%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6038c82fa35a8e8c749182a581eb04eb319f4472' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/modules/blocksocial/blocksocial.tpl',
      1 => 1602662443,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '699178462615c0eff453ca7-92162170',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'facebook_url' => 0,
    'facebook2_url' => 0,
    'twitter_url' => 0,
    'rss_url' => 0,
    'youtube_url' => 0,
    'youtube2_url' => 0,
    'google_plus_url' => 0,
    'pinterest_url' => 0,
    'vimeo_url' => 0,
    'instagram_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0eff46b8e1_19886841',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0eff46b8e1_19886841')) {function content_615c0eff46b8e1_19886841($_smarty_tpl) {?><section id="social-media" class="footer-social-media col-xs-12 col-sm-3">  <div class="footer-download-apps">    <h4>Downloads Pensonic Apps</h4>    <ul>      <li>        <a href="https://play.google.com/store/apps/details?id=com.pensonic.DCRM">          <img src="../../../img/google-play-store.jpg" alt="Google Play Store">
          </a>      </li>      <li>        <a href="https://apps.apple.com/us/app/pensonic-your-enjoyment/id1439098419">          <img src="../../../img/apple-app-store.jpg" alt="App Store">
          </a>      </li>      <li>        <a href="https://appgallery.huawei.com/#/app/C101386843">          <img src="../../../img/huawei-app-gallery.png" alt="Huawei App Gallery" />
          </a>      </li>    </ul>  </div>
  <div class="footer-socialmedia-link" style="padding: 30px 0;">
    <h4>Social</h4>
    <ul>

      <?php if (isset($_smarty_tpl->tpl_vars['facebook_url']->value)&&$_smarty_tpl->tpl_vars['facebook_url']->value!='') {?>

      <li class="facebook">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facebook_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Facebook','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['facebook2_url']->value)&&$_smarty_tpl->tpl_vars['facebook2_url']->value!='') {?>

      <li class="facebook facebook2">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facebook2_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Facebook','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['twitter_url']->value)&&$_smarty_tpl->tpl_vars['twitter_url']->value!='') {?>

      <li class="twitter">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['twitter_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Twitter','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['rss_url']->value)&&$_smarty_tpl->tpl_vars['rss_url']->value!='') {?>

      <li class="rss">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rss_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'RSS','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['youtube_url']->value)&&$_smarty_tpl->tpl_vars['youtube_url']->value!='') {?>

      <li class="youtube">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['youtube_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Youtube','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['youtube2_url']->value)&&$_smarty_tpl->tpl_vars['youtube2_url']->value!='') {?>

      <li class="youtube youtube2">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['youtube2_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Youtube','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['google_plus_url']->value)&&$_smarty_tpl->tpl_vars['google_plus_url']->value!='') {?>

      <li class="google-plus">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['google_plus_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="publisher">

          <span><?php echo smartyTranslate(array('s'=>'Google Plus','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['pinterest_url']->value)&&$_smarty_tpl->tpl_vars['pinterest_url']->value!='') {?>

      <li class="pinterest">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pinterest_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Pinterest','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['vimeo_url']->value)&&$_smarty_tpl->tpl_vars['vimeo_url']->value!='') {?>

      <li class="vimeo">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vimeo_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Vimeo','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

      <?php if (isset($_smarty_tpl->tpl_vars['instagram_url']->value)&&$_smarty_tpl->tpl_vars['instagram_url']->value!='') {?>

      <li class="instagram">

        <a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instagram_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">

          <span><?php echo smartyTranslate(array('s'=>'Instagram','mod'=>'blocksocial'),$_smarty_tpl);?>
</span>

        </a>

      </li>

      <?php }?>

    </ul>
  </div>
</section>
<?php }} ?>
