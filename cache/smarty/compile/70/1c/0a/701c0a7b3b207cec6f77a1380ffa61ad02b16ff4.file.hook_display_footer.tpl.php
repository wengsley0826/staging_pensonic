<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 14:07:41
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/front/hook_display_footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1592326108615bebad1be815-47965314%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '701c0a7b3b207cec6f77a1380ffa61ad02b16ff4' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/front/hook_display_footer.tpl',
      1 => 1619764011,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1592326108615bebad1be815-47965314',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'pbp_product_options' => 0,
    'pbp_general' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615bebad1c3761_76801150',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615bebad1c3761_76801150')) {function content_615bebad1c3761_76801150($_smarty_tpl) {?>

<script>
	$(document).ready(function() {
		var pbp_front_ajax_url = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['link']->value->getModuleLink('productbundlespro','ajax',array()));?>
";
		var url = pbp_front_ajax_url + '?route=pbpproductcontroller&action=hookrendertabs&rand=' + new Date().getTime();

		var pbp_product_options = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['pbp_product_options']->value);?>
;
		var pbp_general = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['pbp_general']->value);?>
;

		if (typeof(pbp_product_options) !== 'undefined') {
			if (pbp_product_options.disabled_addtocart == 1)
				$("#add_to_cart").hide();
		}

		$.get(
			url,
			{
				id_product : id_product
			},
		function (data) { // Loads content into the 'data' variable.

			switch (pbp_general.pbp_location) {
				case 'description-after' :
					$('.pb-center-column').append(data);
					break;
				default:
					$('.primary_block').after(data);
					break;
			}
		});
	});
</script>

<?php if (!empty($_smarty_tpl->tpl_vars['pbp_product_options']->value->disabled_addtocart)) {?>
	<style>
		#add_to_cart, #aw_add_to_cart {
			display: none !important;
		}
	</style>
<?php }?><?php }} ?>
