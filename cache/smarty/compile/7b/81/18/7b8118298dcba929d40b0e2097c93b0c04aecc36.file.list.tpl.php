<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 15:42:02
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/hook/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1125521119615c01caa39426-95193891%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b8118298dcba929d40b0e2097c93b0c04aecc36' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/hook/list.tpl',
      1 => 1599917686,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1125521119615c01caa39426-95193891',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'theme_url' => 0,
    'secslider_secure_key' => 0,
    'eid' => 0,
    'grouping_list' => 0,
    'grouping' => 0,
    'filter' => 0,
    'link' => 0,
    'slides' => 0,
    'slide' => 0,
    'image_baseurl' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c01caa77765_66336616',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c01caa77765_66336616')) {function content_615c01caa77765_66336616($_smarty_tpl) {?><script>
 	theme_url='<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
';
  secslider_secure_key = '<?php echo $_smarty_tpl->tpl_vars['secslider_secure_key']->value;?>
';
</script>
<div class="panel"><h3><i class="icon-list-ul"></i> <?php echo smartyTranslate(array('s'=>'Section list','mod'=>'claritassectionslider'),$_smarty_tpl);?>

	<span class="panel-heading-action">
    <span style="display:inline-block;vertical-align:top;padding-right: 10px;">
      <select id="grouping_filter" name="grouping_filter" onchange="return SliderFilterChange(this, <?php echo $_smarty_tpl->tpl_vars['eid']->value;?>
);">
        <?php  $_smarty_tpl->tpl_vars['grouping'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['grouping']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['grouping_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['grouping']->key => $_smarty_tpl->tpl_vars['grouping']->value) {
$_smarty_tpl->tpl_vars['grouping']->_loop = true;
?>
        <option value="<?php echo $_smarty_tpl->tpl_vars['grouping']->value['grouping'];?>
" <?php if ($_smarty_tpl->tpl_vars['filter']->value==$_smarty_tpl->tpl_vars['grouping']->value['grouping']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['grouping']->value['grouping'];?>
</option>
        <?php } ?>
      </select>
    </span>
    <span style="display:inline-block;">
		  <a id="desc-product-new" class="list-toolbar-btn" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminModules');?>
&configure=claritassectionslider&addSlide=1">
			  <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="<?php echo smartyTranslate(array('s'=>'Add new','mod'=>'claritassectionslider'),$_smarty_tpl);?>
" data-html="true">
				  <i class="process-icon-new "></i>
			  </span>
		  </a>
    </span>
	</span>
	</h3>
	<div id="slidesContent">
		<div id="slides" class="slider-unstyled">
		  <?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value) {
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
				<div id="slides_<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
" class="panel">
					<div class="row">
						<div class="col-lg-1">
							<span><i class="icon-arrows "></i></span>
						</div>
						<div class="col-md-3">
              <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']!='') {?>
							  <img src="<?php echo $_smarty_tpl->tpl_vars['image_baseurl']->value;?>
<?php echo $_smarty_tpl->tpl_vars['slide']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['slide']->value['title'];?>
" class="img-thumbnail" />
              <?php }?>
						</div>
						<div class="col-md-8">
							<h4 class="pull-left">
								#<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
 - <?php echo $_smarty_tpl->tpl_vars['slide']->value['title'];?>

								<?php if ($_smarty_tpl->tpl_vars['slide']->value['is_shared']) {?>
									<div>
										<span class="label color_field pull-left" style="background-color:#108510;color:white;margin-top:5px;">
											<?php echo smartyTranslate(array('s'=>'Shared slide','mod'=>'claritassectionslider'),$_smarty_tpl);?>

										</span>
									</div>
								<?php }?>
							</h4>
							<div class="btn-group-action pull-right">
								<?php echo $_smarty_tpl->tpl_vars['slide']->value['status'];?>


								<a class="btn btn-default"
									href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminModules');?>
&configure=claritassectionslider&id_slide=<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
">
									<i class="icon-edit"></i>
									<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'claritassectionslider'),$_smarty_tpl);?>

								</a>
								<a class="btn btn-default"
									href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminModules');?>
&configure=claritassectionslider&delete_id_slide=<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
">
									<i class="icon-trash"></i>
									<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'claritassectionslider'),$_smarty_tpl);?>

								</a>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php }} ?>
