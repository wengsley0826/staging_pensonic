<?php /* Smarty version Smarty-3.1.19, created on 2021-10-01 20:09:41
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/front/search16.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18637947266156fa85afd730-69188672%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8dbb83b21c278f1f9e7febed9474c6e1550070cc' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/front/search16.tpl',
      1 => 1603849279,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18637947266156fa85afd730-69188672',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title_page' => 0,
    'keyWord' => 0,
    'no_type' => 0,
    'status' => 0,
    'data' => 0,
    'id_list' => 0,
    'item' => 0,
    'html' => 0,
    'ik' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6156fa85b16b50_86085408',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6156fa85b16b50_86085408')) {function content_6156fa85b16b50_86085408($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php if (isset($_smarty_tpl->tpl_vars['title_page']->value)&&$_smarty_tpl->tpl_vars['title_page']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_page']->value, ENT_QUOTES, 'utf-8', true);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Search result','mod'=>'ybc_blocksearch'),$_smarty_tpl);?>
<?php }?><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<h1 class="page-heading product-listing">
    <?php if (isset($_smarty_tpl->tpl_vars['title_page']->value)&&$_smarty_tpl->tpl_vars['title_page']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_page']->value, ENT_QUOTES, 'utf-8', true);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Search results for','mod'=>'ybc_blocksearch'),$_smarty_tpl);?>
<?php }?> <span class="key_search"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyWord']->value, ENT_QUOTES, 'utf-8', true);?>
</span>
</h1>
<div id="content-wrapper">
    <div class="wrap_search">
        <?php if (isset($_smarty_tpl->tpl_vars['no_type']->value)&&$_smarty_tpl->tpl_vars['no_type']->value) {?>
            <div class="status">
                <h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['status']->value, ENT_QUOTES, 'utf-8', true);?>
</h4>
            </div>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['data']->value)&&$_smarty_tpl->tpl_vars['data']->value) {?>
            <input type="hidden" name="key_word" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyWord']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
            <div class="wrap_header_tap">
                <ul>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                        <li <?php if (isset($_smarty_tpl->tpl_vars['id_list']->value)&&$_smarty_tpl->tpl_vars['id_list']->value&&$_smarty_tpl->tpl_vars['id_list']->value==$_smarty_tpl->tpl_vars['item']->value['id']) {?>class="active"<?php }?>>
                            <a class="ajax_tab ajax_tab_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id'], ENT_QUOTES, 'UTF-8', true);?>
 <?php if (isset($_smarty_tpl->tpl_vars['id_list']->value)&&$_smarty_tpl->tpl_vars['id_list']->value&&$_smarty_tpl->tpl_vars['id_list']->value==$_smarty_tpl->tpl_vars['item']->value['id']) {?>active<?php }?>" href="#" data-tab-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id'], ENT_QUOTES, 'UTF-8', true);?>
" >
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['heading'], ENT_QUOTES, 'UTF-8', true);?>

                                <?php if (isset($_smarty_tpl->tpl_vars['item']->value['total'])&&$_smarty_tpl->tpl_vars['item']->value['total']) {?><span>(<?php echo intval($_smarty_tpl->tpl_vars['item']->value['total']);?>
)</span><?php }?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="content_tab">
                <?php if (isset($_smarty_tpl->tpl_vars['html']->value)&&$_smarty_tpl->tpl_vars['html']->value) {?>
                    <?php echo $_smarty_tpl->tpl_vars['html']->value;?>

                <?php } else { ?>
                    <?php if (isset($_smarty_tpl->tpl_vars['data']->value)&&$_smarty_tpl->tpl_vars['data']->value) {?>
                        <?php $_smarty_tpl->tpl_vars['ik'] = new Smarty_variable(0, null, 0);?>
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                            <?php $_smarty_tpl->tpl_vars['ik'] = new Smarty_variable($_smarty_tpl->tpl_vars['ik']->value+1, null, 0);?>
                            <?php if ($_smarty_tpl->tpl_vars['ik']->value==1) {?>
                                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayDataContent','id_list'=>$_smarty_tpl->tpl_vars['item']->value['id'],'keyWord'=>$_smarty_tpl->tpl_vars['keyWord']->value),$_smarty_tpl);?>

                            <?php }?>
                        <?php } ?>
                    <?php }?>
                <?php }?>
            </div>
        <?php }?>
    </div>
</div><?php }} ?>
