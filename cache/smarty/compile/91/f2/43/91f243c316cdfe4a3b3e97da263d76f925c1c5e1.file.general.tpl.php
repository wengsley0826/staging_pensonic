<?php /* Smarty version Smarty-3.1.19, created on 2021-10-01 19:24:34
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/general.tpl" */ ?>
<?php /*%%SmartyHeaderCode:505440326156eff2dc44a1-35679400%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '91f243c316cdfe4a3b3e97da263d76f925c1c5e1' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/general.tpl',
      1 => 1619764013,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '505440326156eff2dc44a1-35679400',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'pbp_product_option' => 0,
    'module_tab_url' => 0,
    'module_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6156eff2dfba76_91116194',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6156eff2dfba76_91116194')) {function content_6156eff2dfba76_91116194($_smarty_tpl) {?>

<div id="pbp-general" class="panel">
    <h3>General Options</h3>

    <div class="row">
		<div class="col-md-3">
			<?php if (!empty($_smarty_tpl->tpl_vars['pbp_product_option']->value->disabled_addtocart)) {?>
				<input type="checkbox" name="pbp-disabled_addtocart" id="pbp-disabled_addtocart" style="margin-right:10px;" value="1" checked>
			<?php } else { ?>
				<input type="checkbox" name="pbp-disabled_addtocart" id="pbp-disabled_addtocart" style="margin-right:10px;" value="1">
			<?php }?>
			<label class="control-label">
				Disable parent product Add to Basket?
			</label>
		</div>
    </div>

	<div class="panel-footer">
		<button type="button" value="1" id="pbp-general-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>

<script>
	module_tab_url = '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['module_tab_url']->value);?>
';
	module_url = '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['module_url']->value);?>
';
	$(document).ready(function () {
		if (typeof(pbp_admin_tab_general_controller) === 'undefined')
			pbp_admin_tab_general_controller = new PBPAdminTabGeneralController("#pbp-general");
	});
</script><?php }} ?>
