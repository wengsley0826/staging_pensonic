<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 13:10:41
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/shopping-cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:343970561615bde51901504-94407151%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9afad6c9a338be932a6fc59444f1de9f6701eaba' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/shopping-cart.tpl',
      1 => 1602254037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '343970561615bde51901504-94407151',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'display_credits' => 0,
    'credits' => 0,
    'guest_checkout' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615bde51907fb6_28324587',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615bde51907fb6_28324587')) {function content_615bde51907fb6_28324587($_smarty_tpl) {?>
<!-- MODULE allinone_rewards -->
<p id="loyalty">
	<?php if ($_smarty_tpl->tpl_vars['display_credits']->value>0) {?>
		<?php echo smartyTranslate(array('s'=>'By checking out this shopping cart you will collect ','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['credits']->value, ENT_QUOTES, 'UTF-8', true);?>
</b>
		<?php echo smartyTranslate(array('s'=>'that can be converted into a voucher for a future purchase.','mod'=>'allinone_rewards'),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['guest_checkout']->value)&&$_smarty_tpl->tpl_vars['guest_checkout']->value) {?><sup>*</sup><?php }?><br />
		<?php if (isset($_smarty_tpl->tpl_vars['guest_checkout']->value)&&$_smarty_tpl->tpl_vars['guest_checkout']->value) {?><sup>*</sup> <?php echo smartyTranslate(array('s'=>'Not available for Instant checkout order','mod'=>'allinone_rewards'),$_smarty_tpl);?>
<?php }?>
	<?php } else { ?>
		<?php echo smartyTranslate(array('s'=>'Add some products to your shopping cart to collect some loyalty credits.','mod'=>'allinone_rewards'),$_smarty_tpl);?>

	<?php }?>
</p>
<!-- END : MODULE allinone_rewards --><?php }} ?>
