<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 18:10:14
         compiled from "/var/www/html/pensonicstore.claritascloud.com/adminc82vrbqp/themes/default/template/controllers/modules/filters.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1436349274615c24860e9df6-54951501%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d41812516b7dbbbf6752ee8fef64fbc6b074060' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/adminc82vrbqp/themes/default/template/controllers/modules/filters.tpl',
      1 => 1594690975,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1436349274615c24860e9df6-54951501',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'showEnabledModules' => 0,
    'showCountryModules' => 0,
    'nameCountryDefault' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c24860f3fb8_96973716',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c24860f3fb8_96973716')) {function content_615c24860f3fb8_96973716($_smarty_tpl) {?><!--start filter module-->
<form method="post" class="form-inline">
<div class="row">
	<div class="col-lg-8">
		<div class="form-group">
			<label><?php echo smartyTranslate(array('s'=>'Filter by'),$_smarty_tpl);?>
</label>
			<select name="module_install" id="module_install_filter" class="form-control " style="display:none;">
				
				<option value="installed" selected="selected"><?php echo smartyTranslate(array('s'=>'Installed Modules'),$_smarty_tpl);?>
</option>			
			</select>
		</div>

		<div class="form-group">
			<select name="module_status" id="module_status_filter" class="form-control <?php if (isset($_smarty_tpl->tpl_vars['showEnabledModules']->value)&&$_smarty_tpl->tpl_vars['showEnabledModules']->value&&$_smarty_tpl->tpl_vars['showEnabledModules']->value!='enabledDisabled') {?>active<?php }?>">
				<option value="enabledDisabled" <?php if ($_smarty_tpl->tpl_vars['showEnabledModules']->value=='enabledDisabled') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Enabled & Disabled'),$_smarty_tpl);?>
</option>
				<option value="enabled" <?php if ($_smarty_tpl->tpl_vars['showEnabledModules']->value=='enabled') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Enabled Modules'),$_smarty_tpl);?>
</option>
				<option value="disabled" <?php if ($_smarty_tpl->tpl_vars['showEnabledModules']->value=='disabled') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Disabled Modules'),$_smarty_tpl);?>
</option>
			</select>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="form-group">
			<select class="filter" name="module_type" id="module_type_filter" style="display:none">
				<option value="allModules" selected="selected"><?php echo smartyTranslate(array('s'=>'All'),$_smarty_tpl);?>
</option>
			</select>
		</div>
	</div>
</div>
	<!-- <span>
		<select class="filter fixed-width-lg" name="country_module_value" id="country_module_value_filter">
			<option value="0" ><?php echo smartyTranslate(array('s'=>'All countries'),$_smarty_tpl);?>
</option>
			<option value="1" <?php if ($_smarty_tpl->tpl_vars['showCountryModules']->value==1) {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Current country:'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['nameCountryDefault']->value;?>
</option>
		</select>
	</span> -->
	<!-- <span class="pull-right">
		<button class="btn btn-default " type="submit" name="resetFilterModules">
			<i class="icon-eraser"></i>
			<?php echo smartyTranslate(array('s'=>'Reset'),$_smarty_tpl);?>
 
		</button>
		<button class="btn btn-default " name="filterModules" id="filterModulesButton" type="submit">
			<i class="icon-filter"></i> 
			<?php echo smartyTranslate(array('s'=>'Filter'),$_smarty_tpl);?>

		</button>
	</span> -->
</form>
<!--end filter module-->
<?php }} ?>
