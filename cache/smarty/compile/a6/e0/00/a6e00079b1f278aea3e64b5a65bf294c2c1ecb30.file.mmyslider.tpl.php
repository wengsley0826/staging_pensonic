<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 11:36:24
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/front/mmyslider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:906286876615bc838cb11b4-58698571%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6e00079b1f278aea3e64b5a65bf294c2c1ecb30' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/front/mmyslider.tpl',
      1 => 1614571105,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '906286876615bc838cb11b4-58698571',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sectionslider_slides' => 0,
    'slide' => 0,
    'product' => 0,
    'link' => 0,
    'slide_img_pos' => 0,
    'currency' => 0,
    'restricted_country_mode' => 0,
    'PS_CATALOG_MODE' => 0,
    'static_token' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615bc838d3b234_36964778',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615bc838d3b234_36964778')) {function content_615bc838d3b234_36964778($_smarty_tpl) {?>	<?php if (isset($_smarty_tpl->tpl_vars['sectionslider_slides']->value)) {?>
		<div id="section-slider">
			<?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sectionslider_slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value) {
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
				<?php if (((isset($_smarty_tpl->tpl_vars['slide']->value['products'])&&count($_smarty_tpl->tpl_vars['slide']->value['products'])>0)||$_smarty_tpl->tpl_vars['slide']->value['section_type']==7)) {?>
				<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==8) {?>
				<div class="row" style="height: 540px;overflow:hidden;">
					<h1 style="color: #36a89f!important; font-weight: bold; font-size: 40px; font-family: 'Lato', sans-serif;"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h1>
					
					<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
					
						<div class="col-lg-2 col-md-2 col-xs-4" style="padding-left: 5px; padding-right: 5px;text-align:center"><a href="/brands/<?php echo $_smarty_tpl->tpl_vars['product']->value['link'];?>
" target="_blank" rel="noopener"><img class="img-responsive" style="border:3px solid #FAFAFA;padding:20px" onerror="this.parent.parent.style.display='none'" src="<?php if (isset($_smarty_tpl->tpl_vars['product']->value['image'])) {?><?php echo $_smarty_tpl->tpl_vars['product']->value['image'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink("/img/m/");?>
<?php echo $_smarty_tpl->tpl_vars['product']->value['id_manufacturer'];?>
-large_default.jpg<?php }?>" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name']);?>
" width="300"/></a>
	
						<h5 itemprop="name" class="product-title">
							<a class="product-name" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
								<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>

							</a>
						</h5>
						</div>
					<?php } ?>
				</div>
				<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['layout_type']=="grid") {?>
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sectionslider_views']->value)."/templates/front/mmygrid.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('slide'=>$_smarty_tpl->tpl_vars['slide']->value), 0);?>

				<?php } else { ?>
				<div class="section_container <?php if ((isset($_smarty_tpl->tpl_vars['slide']->value['addon_class'])&&$_smarty_tpl->tpl_vars['slide']->value['addon_class']!='')) {?><?php echo $_smarty_tpl->tpl_vars['slide']->value['addon_class'];?>
<?php }?>">
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['show_title']) {?>
						<?php if ($_smarty_tpl->tpl_vars['slide']->value['hdr_image']!='') {?>
							<div class="section_title_img" style="<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']!=7) {?>border-color:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;<?php } else { ?>border:none;<?php }?>background:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;">
								<img src="" data-src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/header/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['hdr_image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
							</div>
						<?php } else { ?>
							<div class="section_title"><span><?php echo $_smarty_tpl->tpl_vars['slide']->value['title'];?>
</span></div>
						<?php }?>
					<?php }?>
					<div id="section-slider-wrapper" class="<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==10) {?>voucher-wrapper<?php }?>" style="<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']!=7) {?>border-color:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;<?php } else { ?>border:none;<?php }?>" >
						<?php if ($_smarty_tpl->tpl_vars['slide']->value['image']!=''&&$_smarty_tpl->tpl_vars['slide']->value['section_type']!=10) {?>
							<div class="section_img<?php if ((isset($_smarty_tpl->tpl_vars['slide']->value['mobile_image'])&&$_smarty_tpl->tpl_vars['slide']->value['mobile_image']!='')) {?>mobile_hidden<?php }?>" style="float:<?php echo $_smarty_tpl->tpl_vars['slide_img_pos']->value;?>
;" >
							<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5) {?>
								<a href="https://<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" target='_story'>
							<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
								<a href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php } else { ?>
								<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php }?>

							<?php if ((isset($_smarty_tpl->tpl_vars['slide']->value['mobile_image'])&&$_smarty_tpl->tpl_vars['slide']->value['mobile_image']!='')) {?>
								<picture>
									<source media="(max-width: 580px)" srcset="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/m/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['mobile_image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
">
									<source media="(min-width: 581px)" srcset="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
">
									<img
										src="" data-src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
"
										<?php if (isset($_smarty_tpl->tpl_vars['slide']->value['size'])&&$_smarty_tpl->tpl_vars['slide']->value['size']) {?><?php echo $_smarty_tpl->tpl_vars['slide']->value['size'];?>
<?php } else { ?>width="100%" height="100%"<?php }?> 
										alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" 
									/>
								</picture>
								
							<?php } else { ?>
								<img 
									src="" data-src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
"
									<?php if (isset($_smarty_tpl->tpl_vars['slide']->value['size'])&&$_smarty_tpl->tpl_vars['slide']->value['size']) {?> <?php echo $_smarty_tpl->tpl_vars['slide']->value['size'];?>
<?php } else { ?> width="100%" height="100%"<?php }?> 
									alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" 
								/>
							<?php }?>
							</a>
							</div>
						<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['section_type']==10) {?>
							<div class="section_voucher" style="float:<?php echo $_smarty_tpl->tpl_vars['slide_img_pos']->value;?>
;" >
								<div class="voucher_desc"><?php echo $_smarty_tpl->tpl_vars['slide']->value['description'];?>
</div>
								<!-- add voucher -->
								<a class="voucher_collect" href="#" codeid="<?php echo $_smarty_tpl->tpl_vars['slide']->value['section_value'];?>
" onclick="saveVouchers(<?php echo $_smarty_tpl->tpl_vars['slide']->value['section_value'];?>
); return false;" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
									<?php echo smartyTranslate(array('s'=>"Collect"),$_smarty_tpl);?>

								</a>
							</div>
						<?php }?>
						<div class="section_products <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']==''&&$_smarty_tpl->tpl_vars['slide']->value['section_type']!=10) {?>section_full<?php }?>">
							<ul class="clearfix row section_slide<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
 <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']=='') {?>section5<?php }?> <?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==7) {?>section_content<?php }?> <?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5) {?>type5<?php }?>" >
							<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5) {?>
							<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
								<li>
									<div class="product-container" >
										<div class="left-block2">
											<div class="product-image-container">
												<a class="product_img_link2" href="<?php echo $_smarty_tpl->tpl_vars['product']->value['url'];?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" target='_story'>								
													<img class="replace-2x img-responsive" src="" data-src="<?php echo $_smarty_tpl->tpl_vars['product']->value['thumbnail'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">												
												<a class="product-name" href="<?php echo $_smarty_tpl->tpl_vars['product']->value['url'];?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" target='_story'>
													<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>

												</a>
											</h5>
											<div class="clear"></div>
										</div>
									</div>
								</li>			
							<?php } ?>
							<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['section_type']==9) {?>
							
							<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
								<li>
									<div class="product-container" >
										<div class="left-block2">
											<div class="product-image-container">
												<a class="product_img_link2 nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo $_smarty_tpl->tpl_vars['product']->value['url'];?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" target='_story'>								
													<img class="replace-2x img-responsive" src="" data-src="<?php echo $_smarty_tpl->tpl_vars['product']->value['imgLink'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">												
												<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo $_smarty_tpl->tpl_vars['product']->value['url'];?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" target='_story'>
													<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>

												</a>
											</h5>
											<div class="clear"></div>
										</div>
									</div>
								</li>			
							<?php } ?>
							<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['section_type']==6) {?>
							
							<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
								<li>
									<div class="product-container" itemscope="" itemtype="http://schema.org/Product">
										<div class="left-block">
											<div class="product-image-container">
													<a class="product_img_link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
													
													<img class="replace-2x img-responsive" src="" data-src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['imgLink'], ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block right-block-review">
											<h5 itemprop="name" class="product-title">
												<a class="product-name" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
													<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>

												</a>
											</h5>
											<div class="clear"></div>
											<div class="reviews_list_stars">
												<span class="star_content clearfix">

													<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["i"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['name'] = "i";
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] = (int) 0;
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] = is_array($_loop=5) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] = ((int) 1) == 0 ? 1 : (int) 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total']);
?>
														<?php if ($_smarty_tpl->tpl_vars['product']->value['grade']<=$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']) {?>
															<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar2.png" class="list-img-star-category" alt="0"/>
														<?php } else { ?>
															<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar1.png" class="list-img-star-category" alt="0"/>
														<?php }?>
													<?php endfor; endif; ?>

													<span style="vertical-align:top;line-height:16px;">(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['grade'], ENT_QUOTES, 'UTF-8', true);?>
)</span>
													<meta itemprop="worstRating" content = "0" />
													<meta itemprop="ratingValue" content = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['grade'], ENT_QUOTES, 'UTF-8', true);?>
" />
													<meta itemprop="bestRating" content = "5" />
												</span>

											</div>
											<div class='line-height-default review-content-height'>
												<?php echo $_smarty_tpl->tpl_vars['product']->value['content'];?>

											</div>
											<div class='line-height-default'>
											<br>
												<strong itemprop="author"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['customer_name'], ENT_QUOTES, 'UTF-8', true);?>
 </strong>
											</div>
									</div>
								</li>			
							<?php } ?>
							<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['section_type']==7) {?>
								<div class="section-content col-xs-12 col-sm-12"><?php echo $_smarty_tpl->tpl_vars['slide']->value['description'];?>
</div>
							<?php } else { ?>
							<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
								<li>
									<div class="product-container" itemscope="" itemtype="http://schema.org/Product" style="display: inline-block;vertical-align: top;width: auto;">
										<div class="left-block">
											<div class="product-image-container">
												<?php if ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
													<a class="product_img_link" href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
												<?php } else { ?>
													<a class="product_img_link" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
												<?php }?>
													
													<img class="replace-2x img-responsive" src="" data-src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">
												<?php if ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
													<a class="product-name" href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
												<?php } else { ?>
													<a class="product-name" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
												<?php }?>
													<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>

												</a>
											</h5>
											<div class="clear"></div>
											<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
												<span class="price product-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value["price"]),$_smarty_tpl);?>
</span>
												<meta itemprop="priceCurrency" content="<?php echo $_smarty_tpl->tpl_vars['currency']->value->iso_code;?>
">
												<meta itemprop="price" content="<?php echo $_smarty_tpl->tpl_vars['product']->value["price"];?>
">
												<?php if (isset($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']>0) {?>
													<span class="old-price product-price">
														<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['product']->value['price_without_reduction']),$_smarty_tpl);?>

													</span>
													<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'id_product'=>$_smarty_tpl->tpl_vars['product']->value['id_product'],'type'=>"old_price"),$_smarty_tpl);?>

													<?php if ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='percentage') {?>
														<span class="price-percent-reduction">-<?php echo $_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100;?>
%</span>
													<?php } elseif ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='amount'&&round($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100.0/$_smarty_tpl->tpl_vars['product']->value['price_without_reduction'],0)>=5) {?>
														<span class="price-percent-reduction amount_reduction">-<?php echo round($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100.0/$_smarty_tpl->tpl_vars['product']->value['price_without_reduction'],0);?>
%</span>
													<?php }?>
												<?php }?>
											</div>
											<div class="button-container" style="border-top: 1px solid #eaeaea;"> 
												<?php if ($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']==0&&$_smarty_tpl->tpl_vars['product']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['product']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>							
													<?php if ((!isset($_smarty_tpl->tpl_vars['product']->value['customization_required'])||!$_smarty_tpl->tpl_vars['product']->value['customization_required'])&&($_smarty_tpl->tpl_vars['product']->value['allow_oosp']||$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>								
														<?php $_smarty_tpl->_capture_stack[0][] = array('default', null, null); ob_start(); ?>add=1&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>

															<?php if (isset($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'])&&$_smarty_tpl->tpl_vars['product']->value['id_product_attribute']) {?>&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
<?php }?>
															<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {?>&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
<?php }?>
														<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?> 
														<a class="button ajax_add_to_cart_button btn btn-default col-xs-6" 
															href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',true,null,Smarty::$_smarty_vars['capture']['default'],false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
" 
															data-id-product-attribute="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
" data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" 
															data-minimal_quantity="<?php if (isset($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity'])&&$_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']>=1) {?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']);?>
<?php } else { ?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['minimal_quantity']);?>
<?php }?>">
															
															<i class="icon-shopping-cart"></i>
															<span><?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
</span> 
														</a> 
													<?php } else { ?> 
														<span class="button ajax_add_to_cart_button btn btn-default col-xs-6" style="color:#ccc;"><?php echo smartyTranslate(array('s'=>'Out of Stock'),$_smarty_tpl);?>
</span>
													<?php }?>	
												<?php } else { ?>					
													<span class="col-xs-6">&nbsp;</span>
												<?php }?>
												<a class="button detail_button btn-default col-xs-6" style="text-align:right;" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url"> 
													<i class="icon-list"></i>
													<span><?php echo smartyTranslate(array('s'=>'Details'),$_smarty_tpl);?>
</span> 
												</a>	
											</div>	
										</div>
									</div>
								</li>			
							<?php } ?>
							<?php }?>
							</ul>
						</div>
                        <div class="clearfix"></div>
					</div>
 				</div>
				<?php }?>
				<?php }?>
			<?php } ?>			
		</div>
		<div class="clearfix"></div>	
	<?php }?>
<?php }} ?>
