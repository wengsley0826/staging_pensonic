<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 18:11:17
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/phgoogletagmanager/views/templates/admin/order_refund.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1526953395615c24c5dcfc48-26443485%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'add1d96e0bcad7424a1179f44473cf5746c660c8' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/phgoogletagmanager/views/templates/admin/order_refund.tpl',
      1 => 1594691417,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1526953395615c24c5dcfc48-26443485',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'refund_js_script' => 0,
    'ph_id_googletagmanager' => 0,
    'ph_analytics_uacode' => 0,
    'uid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c24c5dd9dc2_31803372',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c24c5dd9dc2_31803372')) {function content_615c24c5dd9dc2_31803372($_smarty_tpl) {?>
<?php if (isset($_smarty_tpl->tpl_vars['refund_js_script']->value)) {?>
	<script>
	var ph_no_track_backoffice = true;
	var ph_gtm_id = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['ph_id_googletagmanager']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
	var ph_analytics_uacode = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['ph_analytics_uacode']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
	
	  function init_ph_googleTagManger(){
	    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer',ph_gtm_id);
	  }
	  function refundByOrderId (OrderId,order_id_customer,order_id_guest_userid) {
	      dataLayer = [];
	      dataLayer.push({
	        "UID_Cookie": order_id_guest_userid,
	        "CID_Cookie": order_id_customer
	      });
	      dataLayer.push({
	        'ecommerce': {
	          'refund': {
	            'actionField': {'id': OrderId}         // Transaction ID. Required for purchases and refunds.
	          }
	        }
	      });
	      //alert(dataLayer);
	      //console.log(dataLayer);
	      init_ph_googleTagManger();
	      //console.log('send, event, Ecommerce, refundByOrderId '+OrderId)
	  }
	  function refundByProduct (OrderId) {
	      dataLayer = [];
	      dataLayer.push({
	        'ecommerce': {
	          'refund': {
	            'actionField': {'id': OrderId}         // Transaction ID. Required for purchases and refunds.
	          }
	        }
	      });
	      init_ph_googleTagManger();
	  }
	

	// console.log("|BEGIN|$refund_js_script:<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['refund_js_script']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
|END|");
	<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['refund_js_script']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

	// Refund an entire transaction by providing the transaction ID. This example assumes the details
	// of the completed refund are available when the page loads:
	
	<?php if (isset($_smarty_tpl->tpl_vars['uid']->value)) {?>
		dataLayer.push({
			"UID_Cookie": "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['uid']->value);?>
"
		});
	<?php }?>	
	dataLayer.push({
	  'ecommerce': {
	    'refund': {
	      'actionField': {'id': "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['refund_js_script']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"}         // Transaction ID. Required for purchases and refunds.
	    }
	  }
	});
	
	</script>
<?php }?><?php }} ?>
