<?php /* Smarty version Smarty-3.1.19, created on 2021-10-03 21:23:52
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/config/tabs_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10528939686159aee80dfc78-23327470%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af6f1fea5864229bf54520d8dafe86976795b2c2' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/config/tabs_list.tpl',
      1 => 1619764012,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10528939686159aee80dfc78-23327470',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tabs' => 0,
    'tab' => 0,
    'module_config_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6159aee812b203_16676392',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6159aee812b203_16676392')) {function content_6159aee812b203_16676392($_smarty_tpl) {?>

<div id="pbp-tabs" class="panel">
	<h3><?php echo smartyTranslate(array('s'=>'Bundle Tabs','mod'=>'productbundlespro'),$_smarty_tpl);?>
</h3>

	<table id="fieldsTable" class="table">
		<thead>
		<tr>
			<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Name','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
			<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Action','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		</tr>
		</thead>

		<tbody>
			<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
				<tr>
					<td>
						<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

					</td>
					<td>
						<a href="#edit" class="pbp-edit" data-id_tab="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id_tab, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="padding:6px 8px"><i class="icon icon-pencil"></i></a>
						<a href="#delete" class="pbp-delete" data-id_tab="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id_tab, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="padding:6px 8px"><i class="icon icon-trash"></i></a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

	<a href="#add-tab" id="add-tab" class="btn btn-default" style="margin-top:20px;">
		<i class="icon-plus-sign"></i> <?php echo smartyTranslate(array('s'=>'Add a new Tab','mod'=>'productbundlespro'),$_smarty_tpl);?>

	</a>
</div>
<script>
	module_config_url = '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['module_config_url']->value);?>
';
	console.log(module_config_url);
	$(document).ready(function () {
		pbp_tabs = new PBPAdminConfigTabsController("#pbp-tabs");
	});
</script><?php }} ?>
