<?php /* Smarty version Smarty-3.1.19, created on 2021-10-01 19:24:34
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/adminproductsextra.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20768828706156eff295be94-21003619%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b08e4063ea35fc71e1637ce13d573f20292b8610' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/adminproductsextra.tpl',
      1 => 1602254037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20768828706156eff295be94-21003619',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product_rewards' => 0,
    'product_reward' => 0,
    'currency' => 0,
    'product_rewards_url' => 0,
    'virtual_value' => 0,
    'virtual_name' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6156eff29e4178_00062350',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6156eff29e4178_00062350')) {function content_6156eff29e4178_00062350($_smarty_tpl) {?>
<!-- MODULE allinone_rewards -->
<div class="panel">
	<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','<')) {?>
	<h4><?php echo smartyTranslate(array('s'=>'Loyalty rewards','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h4>
	<?php } else { ?>
	<h3><?php echo smartyTranslate(array('s'=>'Loyalty rewards','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h3>
	<?php }?>
	<table class="table" id="reward_product_list" <?php if (version_compare(@constant('_PS_VERSION_'),'1.6','<')) {?>style="width: 100%"<?php }?>>
		<thead>
			<tr>
				<th><?php echo smartyTranslate(array('s'=>'Reward value','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				<th><?php echo smartyTranslate(array('s'=>'Reward date from','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				<th><?php echo smartyTranslate(array('s'=>'Reward date to','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				<th><?php echo smartyTranslate(array('s'=>'Action','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
			</tr>
		</thead>
		<tbody>
<?php if (count($_smarty_tpl->tpl_vars['product_rewards']->value)>0) {?>
	<?php  $_smarty_tpl->tpl_vars['product_reward'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product_reward']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_rewards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product_reward']->key => $_smarty_tpl->tpl_vars['product_reward']->value) {
$_smarty_tpl->tpl_vars['product_reward']->_loop = true;
?>
			<tr id="<?php echo intval($_smarty_tpl->tpl_vars['product_reward']->value['id_reward_product']);?>
">
				<td><span class="reward_value"><?php echo floatval($_smarty_tpl->tpl_vars['product_reward']->value['value']);?>
</span> <span class="reward_type"><?php if ($_smarty_tpl->tpl_vars['product_reward']->value['type']==0) {?>%<?php } else { ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
<?php }?></span></td>
				<td class="reward_from"><?php if ($_smarty_tpl->tpl_vars['product_reward']->value['date_from']!=0) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_reward']->value['date_from'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></td>
				<td class="reward_to"><?php if ($_smarty_tpl->tpl_vars['product_reward']->value['date_to']!=0) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_reward']->value['date_to'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></td>
				<td>
					<img style="cursor: pointer" class="edit_reward" src="../img/admin/edit.gif">
					<img style="cursor: pointer" class="delete_reward" src="../img/admin/delete.gif">
				</td>
			</tr>
	<?php } ?>
<?php }?>
			<tr id="0" style="display: none">
				<td colspan="4" align="center"><?php echo smartyTranslate(array('s'=>'No reward is defined for that product','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</td>
			</tr>
		</tbody>
	</table>
	<div class="panel" style="margin-top: 50px">
		<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','<')) {?>
		<h4 id="new_reward"><?php echo smartyTranslate(array('s'=>'New reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h4>
		<h4 id="update_reward" style="display: none"><?php echo smartyTranslate(array('s'=>'Update reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h4>
		<?php } else { ?>
		<h3 id="new_reward"><?php echo smartyTranslate(array('s'=>'New reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h3>
		<h3 id="update_reward" style="display: none"><?php echo smartyTranslate(array('s'=>'Update reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h3>
		<?php }?>
		<div class="form-group">
			<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Value','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</label>
			<div class="input-group <?php if (version_compare(@constant('_PS_VERSION_'),'1.6','<')) {?>margin-form<?php }?>">
				<input type="hidden" name="reward_product_id" id="reward_product_id">
				<input type="text" name="reward_product_value" id="reward_product_value" style="width: 80px; margin-right: 5px">
				<select id="reward_product_type" name="reward_product_type" style="width: 160px">
					<option value="0">% <?php echo smartyTranslate(array('s'=>'of its own price','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</option>
					<option value="1"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
</option>
				</select>
				&nbsp;<span id="virtualvalue"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Dates','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</label>
			<div class="input-group col-lg-10 <?php if (version_compare(@constant('_PS_VERSION_'),'1.6','<')) {?>margin-form<?php }?>">
				<div <?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>class="row"<?php }?>>
					<div class="col-lg-5" <?php if (version_compare(@constant('_PS_VERSION_'),'1.6','<')) {?>style="float: left; padding-right: 5px"<?php }?>>
						<div class="input-group">
							<span class="input-group-addon"><?php echo smartyTranslate(array('s'=>'from','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</span>
							<input type="text" id="reward_product_from" style="text-align: center" name="reward_product_from" class="datetimepicker">
							<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="input-group">
							<span class="input-group-addon"><?php echo smartyTranslate(array('s'=>'to','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</span>
							<input type="text" id="reward_product_to" style="text-align: center" name="reward_product_to" class="datetimepicker">
							<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer <?php if (version_compare(@constant('_PS_VERSION_'),'1.6','<')) {?>margin-form<?php }?>">
			<button class="button btn btn-default" id="cancelRewardProduct" type="button"><i class="process-icon-save"></i> <?php echo smartyTranslate(array('s'=>'Cancel','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</button>
			<button class="button btn btn-default pull-right" id="submitRewardProduct" type="button"><i class="process-icon-save"></i> <?php echo smartyTranslate(array('s'=>'Save','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</button>
		</div>
	</div>
	<script>
		var product_rewards_url = "<?php echo strtr($_smarty_tpl->tpl_vars['product_rewards_url']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
		var delete_reward_label = "<?php echo smartyTranslate(array('s'=>'Do you really want to delete this reward ?','mod'=>'allinone_rewards'),$_smarty_tpl);?>
";
		var delete_reward_title = "<?php echo smartyTranslate(array('s'=>'Delete reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
";
		var currency_sign = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
';

		var virtual_value = <?php echo floatval($_smarty_tpl->tpl_vars['virtual_value']->value);?>
;
		var virtual_name = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['virtual_name']->value, ENT_QUOTES, 'UTF-8', true);?>
";

		manageEmptyRow();

		$('.datetimepicker').datetimepicker({
			prevText: '',
			nextText: '',
			dateFormat: 'yy-mm-dd',
			// Define a custom regional settings in order to use PrestaShop translation tools
			currentText: '<?php echo smartyTranslate(array('s'=>'Now','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
			closeText: '<?php echo smartyTranslate(array('s'=>'Done','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
			ampm: false,
			amNames: ['AM', 'A'],
			pmNames: ['PM', 'P'],
			timeFormat: 'hh:mm:ss tt',
			timeSuffix: '',
			timeOnlyTitle: "<?php echo smartyTranslate(array('s'=>'Choose Time','mod'=>'allinone_rewards'),$_smarty_tpl);?>
",
			timeText: '<?php echo smartyTranslate(array('s'=>'Time','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
			hourText: '<?php echo smartyTranslate(array('s'=>'Hour','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
			minuteText: '<?php echo smartyTranslate(array('s'=>'Minute','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
			secondText: '<?php echo smartyTranslate(array('s'=>'Second','mod'=>'allinone_rewards'),$_smarty_tpl);?>
',
			showSecond: true
		});
	</script>
</div>
<!-- END : MODULE allinone_rewards --><?php }} ?>
