<?php /* Smarty version Smarty-3.1.19, created on 2021-10-01 20:09:41
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/hook/product-list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3296092266156fa85c30412-59532974%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bd76dcca5ce4092801fd65dddfe1c43c010a8d0a' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/ybc_blocksearch/views/templates/hook/product-list.tpl',
      1 => 1603849280,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3296092266156fa85c30412-59532974',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'datas' => 0,
    'wrapper' => 0,
    'sort_lists' => 0,
    'search_ajax_link' => 0,
    'id_list' => 0,
    'sort_by' => 0,
    'option' => 0,
    'paggination' => 0,
    'id' => 0,
    'class' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6156fa85c450b5_68958494',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6156fa85c450b5_68958494')) {function content_6156fa85c450b5_68958494($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['datas']->value)&&$_smarty_tpl->tpl_vars['datas']->value) {?>
    <?php $_smarty_tpl->tpl_vars["products"] = new Smarty_variable($_smarty_tpl->tpl_vars['datas']->value, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["page_name"] = new Smarty_variable("search", null, 0);?>
    <?php if ($_smarty_tpl->tpl_vars['wrapper']->value) {?>
        <div class="wrap_content_product wrap_content featured-products">
        <?php if (isset($_smarty_tpl->tpl_vars['sort_lists']->value)&&$_smarty_tpl->tpl_vars['sort_lists']->value) {?>
            <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_ajax_link']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post">
                <label for="homecat_sort_by_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_list']->value, ENT_QUOTES, 'utf-8', true);?>
"><?php echo smartyTranslate(array('s'=>'Sort by','mod'=>'ybc_blocksearch'),$_smarty_tpl);?>
</label>
                <select name="ybc_search_sort_by" id="ybc_search_sort_by_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_list']->value, ENT_QUOTES, 'utf-8', true);?>
" class="ybc_search_sort_by" data-id-list="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_list']->value, ENT_QUOTES, 'utf-8', true);?>
">
                    <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sort_lists']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                        <option <?php if ($_smarty_tpl->tpl_vars['sort_by']->value==$_smarty_tpl->tpl_vars['option']->value['id_option']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['id_option'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                    <?php } ?>
                </select>
            </form>
        <?php }?>

        <div class="content_tab_product"><?php }?>
            <?php if (isset($_smarty_tpl->tpl_vars['paggination']->value)&&$_smarty_tpl->tpl_vars['paggination']->value) {?>
                <div class="view_number_show">
                    <?php echo $_smarty_tpl->getSubTemplate ("./showing_title.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                </div>
            <?php }?>
		<?php ob_start();?><?php if (isset($_smarty_tpl->tpl_vars['id']->value)&&$_smarty_tpl->tpl_vars['id']->value) {?><?php echo " ";?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?><?php $_tmp9=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('class'=>"product_list grid row",'id'=>$_tmp9), 0);?>

	   </div>
       <?php if ($_smarty_tpl->tpl_vars['wrapper']->value) {?>
        <?php if (isset($_smarty_tpl->tpl_vars['paggination']->value)&&$_smarty_tpl->tpl_vars['paggination']->value) {?>
            <?php echo $_smarty_tpl->tpl_vars['paggination']->value;?>

        <?php }?>
        </div>
    <?php }?>
<?php } elseif ($_smarty_tpl->tpl_vars['wrapper']->value) {?>
	<div class="search_product_list_wrapper <?php if (isset($_smarty_tpl->tpl_vars['class']->value)&&$_smarty_tpl->tpl_vars['class']->value) {?>search-wrapper-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['class']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
		<div class="col-sm-12 col-xs-12"><div class="clearfix"></div><span class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'No products available','mod'=>'ybc_blocksearch'),$_smarty_tpl);?>
</span></div>
	</div>
<?php }?>
<?php }} ?>
