<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:38:23
         compiled from "/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/modules/blockcontactinfos/blockcontactinfos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1097396033615c0eff35f4d8-69645389%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c334256940f3064ec4ee29b30f34dbe61deb1a1e' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/themes/default-bootstrap/modules/blockcontactinfos/blockcontactinfos.tpl',
      1 => 1601999767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1097396033615c0eff35f4d8-69645389',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'blockcontactinfos_company' => 0,
    'blockcontactinfos_address' => 0,
    'blockcontactinfos_phone' => 0,
    'blockcontactinfos_email' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0eff368289_49883557',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0eff368289_49883557')) {function content_615c0eff368289_49883557($_smarty_tpl) {?><?php if (!is_callable('smarty_function_mailto')) include '/var/www/html/pensonicstore.claritascloud.com/tools/smarty/plugins/function.mailto.php';
?><!-- MODULE Block contact infos -->
<section id="block_contact_infos" class="footer-address col-xs-12 col-sm-4">
  <div>
    <h4><?php echo smartyTranslate(array('s'=>'Address','mod'=>'blockcontactinfos'),$_smarty_tpl);?>
</h4>
    <ul class="company-add-info">      <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_company']->value!=''||$_smarty_tpl->tpl_vars['blockcontactinfos_address']->value!='') {?>        <li>          <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_company']->value!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_company']->value, ENT_QUOTES, 'UTF-8', true);?>
<br /><?php }?>                      <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_address']->value!='') {?><?php echo $_smarty_tpl->tpl_vars['blockcontactinfos_address']->value;?>
<?php }?>        </li>
        <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_phone']->value!='') {?>
        <li><?php echo smartyTranslate(array('s'=>' ','mod'=>'blockcontactinfos'),$_smarty_tpl);?>
 <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_phone']->value, ENT_QUOTES, 'UTF-8', true);?>
</span></li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_email']->value!='') {?>
        <li><?php echo smartyTranslate(array('s'=>' ','mod'=>'blockcontactinfos'),$_smarty_tpl);?>
 <span><?php echo smarty_function_mailto(array('address'=>htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_email']->value, ENT_QUOTES, 'UTF-8', true),'encode'=>"hex"),$_smarty_tpl);?>
</span></li>
        <?php }?>
      <?php }?>
    </ul>
  </div>
</section>
<!-- /MODULE Block contact infos -->
<?php }} ?>
