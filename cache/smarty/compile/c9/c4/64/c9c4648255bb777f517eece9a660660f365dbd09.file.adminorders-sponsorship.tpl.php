<?php /* Smarty version Smarty-3.1.19, created on 2021-09-30 18:34:43
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/adminorders-sponsorship.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2112726923615592c3055bb9-22482188%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c9c4648255bb777f517eece9a660660f365dbd09' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/adminorders-sponsorship.tpl',
      1 => 1602254037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2112726923615592c3055bb9-22482188',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'rewards' => 0,
    'reward' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615592c30675b7_09153843',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615592c30675b7_09153843')) {function content_615592c30675b7_09153843($_smarty_tpl) {?>
<?php if ((count($_smarty_tpl->tpl_vars['rewards']->value))) {?>
<!-- MODULE allinone_rewards -->
	<div class="<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>col-lg-7<?php } else { ?>clear<?php }?>" id="adminorders_sponsorship">
	<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
		<div class="panel" style="overflow: auto">
			<div class="panel-heading"><?php echo smartyTranslate(array('s'=>'Sponsorship rewards for this order','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</div>
	<?php } else { ?>
			<br>
			<fieldset>
				<legend><?php echo smartyTranslate(array('s'=>'Sponsorship rewards for this order','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</legend>
	<?php }?>

				<table style="width: 100%">
					<tr style="font-weight: bold">
						<td><?php echo smartyTranslate(array('s'=>'Level','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</td>
						<td><?php echo smartyTranslate(array('s'=>'Name','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</td>
						<td><?php echo smartyTranslate(array('s'=>'Reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</td>
						<td><?php echo smartyTranslate(array('s'=>'Status','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</td>
					</tr>
	<?php  $_smarty_tpl->tpl_vars['reward'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['reward']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['rewards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['reward']->key => $_smarty_tpl->tpl_vars['reward']->value) {
$_smarty_tpl->tpl_vars['reward']->_loop = true;
?>
					<tr>
						<td><?php echo intval($_smarty_tpl->tpl_vars['reward']->value['level_sponsorship']);?>
</td>
						<td><a href="?tab=AdminCustomers&id_customer=<?php echo intval($_smarty_tpl->tpl_vars['reward']->value['id_customer']);?>
&viewcustomer&token=<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getAdminToken'][0][0]->getAdminTokenLiteSmarty(array('tab'=>'AdminCustomers'),$_smarty_tpl);?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['reward']->value['firstname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['reward']->value['lastname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></td>
						<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['reward']->value['credits']),$_smarty_tpl);?>
</td>
						<td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['reward']->value['state'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
					</tr>
	<?php } ?>
				</table>
<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
		</div>
<?php } else { ?>
			</fieldset>
<?php }?>
	</div>
<!-- END : MODULE allinone_rewards -->
<?php }?><?php }} ?>
