<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 18:11:17
         compiled from "/var/www/html/pensonicstore.claritascloud.com/adminc82vrbqp/themes/default/template/controllers/modules/modal_not_trusted.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1598455200615c24c5df8697-94317619%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd4c5de47e80927519efe5317e4ad3bf9fcc3c884' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/adminc82vrbqp/themes/default/template/controllers/modules/modal_not_trusted.tpl',
      1 => 1594690976,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1598455200615c24c5df8697-94317619',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c24c5dfa8b2_56255992',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c24c5dfa8b2_56255992')) {function content_615c24c5dfa8b2_56255992($_smarty_tpl) {?><div class="untrusted-content-action">

	<div class="modal-body">
		<div class="row">
			<div class="col-sm-2" style="text-align: center;">
				<img id="untrusted-module-logo" class="" src="" alt="" style="max-width:96px;">
			</div>
			<div class="col-sm-10">
				<table class="table">
					<tr>
						<td><?php echo smartyTranslate(array('s'=>'Module'),$_smarty_tpl);?>
</td>
						<td><strong><span class="module-display-name-placeholder"></span></strong></td>
					</tr>
					<tr>
						<td><?php echo smartyTranslate(array('s'=>'Author'),$_smarty_tpl);?>
</td>
						<td><strong><span class="author-name-placeholder"></span></strong></td>
					</tr>
				</table>
			</div>

			<div class="col-sm-12" style="text-align: center; padding-top: 12px;">
				<a id="proceed-install-anyway" href="#" class="btn btn-warning"><?php echo smartyTranslate(array('s'=>'Proceed with the installation'),$_smarty_tpl);?>
</a>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo smartyTranslate(array('s'=>'Back to modules list'),$_smarty_tpl);?>
</button>
			</div>
		</div>
	</div>
</div>

<div class="untrusted-content-more-info" style="display:none;">

	<div class="modal-body">
	</div>

	<div class="modal-footer">
		<a id="untrusted-show-action" class="btn btn-default" href="#"><?php echo smartyTranslate(array('s'=>'Back'),$_smarty_tpl);?>
</a>
	</div>

</div>
<?php }} ?>
