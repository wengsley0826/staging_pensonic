<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 13:07:59
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/authentication.tpl" */ ?>
<?php /*%%SmartyHeaderCode:948251521615bddafad0b51-25495210%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd7a9910184280b559799bfcbcde63f3e81573c77' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/authentication.tpl',
      1 => 1602254037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '948251521615bddafad0b51-25495210',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'module_template_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615bddafaddfc7_07769121',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615bddafaddfc7_07769121')) {function content_615bddafaddfc7_07769121($_smarty_tpl) {?>
<!-- MODULE allinone_rewards -->
<fieldset class="account_creation aior_sponsor">
	<h3 class="page-subheading"><?php echo smartyTranslate(array('s'=>'Sponsorship program','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h3>
	<p class="text form-group">
<?php if (!isset($_POST['sponsorship_invisible'])) {?>
		<label for="sponsorship"><?php echo smartyTranslate(array('s'=>'Code or E-mail address of your sponsor','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</label>
		<input type="text" size="52" maxlength="128" class="form-control text" id="sponsorship" name="sponsorship" value="<?php if (isset($_POST['sponsorship'])) {?><?php echo mb_convert_encoding(htmlspecialchars($_POST['sponsorship'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>" />
		<script type="text/javascript">
			/* cant be done in sponsorship.js, because that tpl is loaded in ajax (use live jquery function ?)*/
			// this variable is necesary because on 1.5.2 it crashs if directly in the code
			var error_sponsor = "<?php echo smartyTranslate(array('s'=>'This sponsor does not exist','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl);?>
";
			$(document).ready(function(){
				$('#sponsorship').focus(function(){
					<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
						$('#sponsorship').parents('p').removeClass('form-error');
						$('#sponsorship').parents('p').removeClass('form-ok');
					<?php } else { ?>
						$('#sponsorship').removeClass('sponsorship_nok');
						$('#sponsorship_result').remove();
					<?php }?>
				});
				$('#sponsorship').blur(function(event){
					if (jQuery.trim($('#sponsorship').val()) != '') {
						$.ajax({
							type	: "POST",
							async	: false,
							cache	: false,
							url		: '<?php echo strtr($_smarty_tpl->tpl_vars['link']->value->getModuleLink('allinone_rewards','sponsorship',array(),true), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
							dataType: 'json',
							data 	: "popup=1&checksponsor=1&sponsorship="+$('#sponsorship').val()+"&customer_email="+$('#email').val(),
							success: function(obj)	{
								if (obj && obj.result == 1) {
									<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
										$('#sponsorship').parents('p').addClass('form-ok');
									<?php } else { ?>
										$('#sponsorship').after('&nbsp;<img id="sponsorship_result" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/valid.png" align="absmiddle" class="icon" />');
									<?php }?>
								} else {
									<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
										$('#sponsorship').parents('p').removeClass('form-ok');
										$('#sponsorship').parents('p').addClass('form-error');
									<?php } else { ?>
										$('#sponsorship').addClass('sponsorship_nok');
										$('#sponsorship').after('&nbsp;<img id="sponsorship_result" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
img/invalid.png" title="'+error_sponsor+'" align="absmiddle" class="icon" />');
									<?php }?>
									event.stopPropagation();
								}
							}
						});
					}
				});
			});
		</script>
<?php } else { ?>
		<label style="width: 100%; text-align: center"><?php echo smartyTranslate(array('s'=>'You have been sponsored','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</label>
<?php }?>
	</p>
</fieldset>
<!-- END : MODULE allinone_rewards --><?php }} ?>
