<?php /* Smarty version Smarty-3.1.19, created on 2021-09-13 02:30:22
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/config/tab_add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:450009313613e473eb7f644-12551088%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd7e6b74ae26b81a4c9b55e3602aab57a6d032acc' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/config/tab_add.tpl',
      1 => 1619764012,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '450009313613e473eb7f644-12551088',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id_tab' => 0,
    'languages' => 0,
    'language' => 0,
    'id_lang_default' => 0,
    'title_localised_array' => 0,
    'language_dropdown' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_613e473eb8bee9_24195010',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_613e473eb8bee9_24195010')) {function content_613e473eb8bee9_24195010($_smarty_tpl) {?>

<input type="hidden" name="id_tab" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['id_tab']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">

<div id="panel1" class="panel">
	<h3>Add / Edit Tab</h3>

	<div class="alert alert-danger" style="display: none"></div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-2" for="">
					<span class="label-tooltip" title="tile of the tab as displayed on your product page">Title</span>
				</label>

				<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
					<div class="translatable-field lang-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['id_lang'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="<?php if ($_smarty_tpl->tpl_vars['language']->value['id_lang']==$_smarty_tpl->tpl_vars['id_lang_default']->value) {?>display: block;<?php } else { ?>display:none;<?php }?>">
						<div class="col-lg-7">
							<input type="text" id="title_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['id_lang'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="form-control updateCurrentText " name="title_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['id_lang'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" value="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['title_localised_array']->value[mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['id_lang'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')];?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_tmp1)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['title_localised_array']->value[$_smarty_tpl->tpl_vars['language']->value['id_lang']], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>" required="required">
						</div>

						<div class="col-lg-2">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
								<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

								<span class="caret"></span>
							</button>


							<ul class="dropdown-menu">
								<?php  $_smarty_tpl->tpl_vars['language_dropdown'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language_dropdown']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language_dropdown']->key => $_smarty_tpl->tpl_vars['language_dropdown']->value) {
$_smarty_tpl->tpl_vars['language_dropdown']->_loop = true;
?>
									<li>
										<a href="javascript:hideOtherLanguage(<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language_dropdown']->value['id_lang'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
);"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['language_dropdown']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a>
									</li>
								<?php } ?>
							</ul>

						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="panel-footer">
		<a href="#close" id="pbp-popup-close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="pbp-tab-popup-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> <?php echo smartyTranslate(array('s'=>'Save','mod'=>'productbundlespro'),$_smarty_tpl);?>

		</button>
	</div>
</div><?php }} ?>
