<?php /* Smarty version Smarty-3.1.19, created on 2021-10-03 21:23:52
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/config/general.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17161487446159aee801c926-73618544%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e209e08bc5b90cf3961be2c13b10964859c0770f' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/config/general.tpl',
      1 => 1619764012,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17161487446159aee801c926-73618544',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'pbp_general' => 0,
    'module_config_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6159aee8044da5_07149804',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6159aee8044da5_07149804')) {function content_6159aee8044da5_07149804($_smarty_tpl) {?>

<div id="pbp-general" class="panel">
	<div class="panel-heading">
		<i class="icon-cogs"></i> General Settings
	</div>

	<div class="form-wrapper row">

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display in Product Quick View?
			</label>

			<div class="col-lg-10">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="pbp_quickview_enabled" id="pbp_quickview_enabled_on" value="1" <?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_quickview_enabled']=="1") {?>checked<?php }?>>
					<label for="pbp_quickview_enabled_on">Yes</label>
					<input type="radio" name="pbp_quickview_enabled" id="pbp_quickview_enabled_off" value="0" <?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_quickview_enabled']!="1") {?>checked<?php }?>>
					<label for="pbp_quickview_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display Attributes of parent product in bundle?
			</label>

			<div class="col-lg-10">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="pbp_parent_attr_display" id="pbp_parent_attr_display_on" value="1" <?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_parent_attr_display']=="1") {?>checked<?php }?>>
					<label for="pbp_parent_attr_display_on">Yes</label>
					<input type="radio" name="pbp_parent_attr_display" id="pbp_parent_attr_display_off" value="0" <?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_parent_attr_display']!="1") {?>checked<?php }?>>
					<label for="pbp_parent_attr_display_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display bundle total in bundles?
			</label>

			<div class="col-lg-10">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="pbp_display_bundle_total" id="pbp_display_bundle_total_on" value="1" <?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_display_bundle_total']=="1") {?>checked<?php }?>>
					<label for="pbp_display_bundle_total_on">Yes</label>
					<input type="radio" name="pbp_display_bundle_total" id="pbp_display_bundle_total_off" value="0" <?php if ($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_display_bundle_total']!="1") {?>checked<?php }?>>
					<label for="pbp_display_bundle_total_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>


		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display Location
			</label>

			<div class="col-lg-10">
				<select name="pbp_location" id="pbp_location">
					<option value="tabs-before">Above Product Tabs</option>
					<option value="description-after">After Product Description</option>
				</select>

				<?php if (!empty($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_location'])) {?>
					<script>
						$(document).ready(function() {
							$("#pbp_location").val('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['pbp_general']->value['pbp_location'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');
						});
					</script>
				<?php } else { ?>
					<script>
						$(document).ready(function () {
							$("#pbp_location").val('tabs-before');
						});
					</script>
				<?php }?>

			</div>
		</div>
	</div>
	<!-- /.form-wrapper -->

	<div class="panel-footer">
		<button type="button" value="1" id="pbp-general-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>

<script>
	module_config_url = '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['module_config_url']->value);?>
';
	$(document).ready(function () {
		pbp_general = new PBPAdminConfigGeneralController("#pbp-general");
	});
</script><?php }} ?>
