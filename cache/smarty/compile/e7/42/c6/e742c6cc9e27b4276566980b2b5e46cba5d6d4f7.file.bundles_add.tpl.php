<?php /* Smarty version Smarty-3.1.19, created on 2021-09-13 03:10:38
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:338565089613e50ae257dd7-71029668%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e742c6cc9e27b4276566980b2b5e46cba5d6d4f7' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles_add.tpl',
      1 => 1619764012,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '338565089613e50ae257dd7-71029668',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id_product' => 0,
    'bundle' => 0,
    'tabs_collection' => 0,
    'tab' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_613e50ae26bc83_42802875',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_613e50ae26bc83_42802875')) {function content_613e50ae26bc83_42802875($_smarty_tpl) {?>

<?php if (!empty($_smarty_tpl->tpl_vars['id_product']->value)) {?>
	<input type="hidden" name="id_product" value="<?php echo intval($_smarty_tpl->tpl_vars['id_product']->value);?>
">
<?php }?>

<div id="panel1" class="panel">
	<h3>Add / Edit Bundle</h3>

	<div class="row">
		<div class="col-lg-12">

			<div class="form-group">
				<label class="control-label col-lg-3">
					<?php echo smartyTranslate(array('s'=>'Bundle Enabled','mod'=>'productbundlespro'),$_smarty_tpl);?>

				</label>

				<div class="col-lg-9 ">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="enabled" id="enabled_on" value="1" <?php if ($_smarty_tpl->tpl_vars['bundle']->value->enabled=="1") {?>checked<?php }?>>
						<label for="enabled_on">Yes</label>
						<input type="radio" name="enabled" id="enabled_off" value="0" <?php if ($_smarty_tpl->tpl_vars['bundle']->value->enabled!="1") {?>checked<?php }?>>
						<label for="enabled_off">No</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Choose a tab">Choose a tab</span>
				</label>

				<div class="col-lg-6">
					<select id="id_tab" name="id_tab">
						<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs_collection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
							<?php if (isset($_smarty_tpl->tpl_vars['bundle']->value)&&$_smarty_tpl->tpl_vars['bundle']->value->id_tab==$_smarty_tpl->tpl_vars['tab']->value->id_tab) {?>
								<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id_tab, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" selected><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
							<?php } else { ?>
								<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->id_tab, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">

				<div class="col-lg-6">
					<a id="pbp-edit-product" href="#pbp-edit-product" class="btn btn-default">
						<i class="icon icon-list"></i>
						<span><?php echo smartyTranslate(array('s'=>'Add Product','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span>
					</a>
				</div>

			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-12">
			<div id="pbp-bundle-products"></div>
		</div>
	</div>


	<div class="panel-footer">
		<a id="pbp-popup-close" href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="pbp-popup-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> <?php echo smartyTranslate(array('s'=>'Save','mod'=>'productbundlespro'),$_smarty_tpl);?>

		</button>
	</div>
</div>


<div id="panel2" class="panel subpanel">
	<h3>Add / Edit Product in bundle</h3>

	<input type="hidden" name="id_pbp_product" id="id_pbp_product" value="" />

	<div class="row">
		<div class="form-group">
			<label class="control-label col-lg-2">
				<span class="label-tooltip" title="">Search</span>
			</label>

			<div class="col-lg-8">
				<div class="input-group col-xs-9">
					<input id="product_search" type="text" name="product_search" value="">
					<span class="input-group-addon">
						<i class="icon-search"></i>
					</span>
				</div>

				
				<div id="product-search-results">
					<input id="id_product" name="id_product" type="hidden">

					<table id="search-results-table" class="table">
						<thead>
						<tr class="nodrag nodrop">
							<th style="width:10%"><span class="title_box"><?php echo smartyTranslate(array('s'=>'ID','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
							<th style="width:30%"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Ref','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
							<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Name','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
						</tr>
						</thead>
						<tbody>
						<tr class="cloneable hidden" style="cursor:pointer">
							<td class="id_product" data-bind="id_product"></td>
							<td class="name" data-bind="name"></td>
						</tr>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-2">
				<span class="label-tooltip" title="">Apply Discount of</span>
			</label>

			<div class="col-lg-8">
				<div class="input-group col-xs-9">
					<input id="discount_amount" type="text" name="discount_amount" value="">
					<select name="discount_type" id="discount_type">
						<option value="percentage">%</option>
						<option value="money">Decimal/Money</option>
					</select>
				</div>
			</div>
		</div>

	<div class="panel-footer">
		<a id="pbp-edit-product-cancel" href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>

		<button type="submit" id="pbp-edit-product-done" class="btn btn-default pull-right">
			<i class="process-icon-check icon-check"></i> Done
		</button>
	</div>

</div>
<?php }} ?>
