<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:26:55
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/hook/sectionslider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1307406800615c0c4feaf7b2-61227518%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f0daed5e5b15bc91cadd7ba529c2851d73c71945' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/hook/sectionslider.tpl',
      1 => 1602675886,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1307406800615c0c4feaf7b2-61227518',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_name' => 0,
    'sectionslider_slides' => 0,
    'slide' => 0,
    'link' => 0,
    'colWidth' => 0,
    'product' => 0,
    'currency' => 0,
    'restricted_country_mode' => 0,
    'PS_CATALOG_MODE' => 0,
    'static_token' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0c4ff31317_69230987',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0c4ff31317_69230987')) {function content_615c0c4ff31317_69230987($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['page_name']->value=='index') {?>
    <?php if (isset($_smarty_tpl->tpl_vars['sectionslider_slides']->value)) {?>
		<div id="section-slider">
			<?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sectionslider_slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value) {
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
				<?php if (((isset($_smarty_tpl->tpl_vars['slide']->value['products'])&&count($_smarty_tpl->tpl_vars['slide']->value['products'])>0)||$_smarty_tpl->tpl_vars['slide']->value['section_type']==7)) {?>
				
				<?php if ($_smarty_tpl->tpl_vars['slide']->value['layout_type']=="grid") {?>
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['sectionslider_views']->value)."/templates/hook/sectiongrid.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('slide'=>$_smarty_tpl->tpl_vars['slide']->value), 0);?>

				<?php } else { ?>
				<div class="section_container <?php if ($_smarty_tpl->tpl_vars['slide']->value['addon_class']!='') {?><?php echo $_smarty_tpl->tpl_vars['slide']->value['addon_class'];?>
<?php }?>">
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['show_title']) {?>
						<?php if ($_smarty_tpl->tpl_vars['slide']->value['hdr_image']!='') {?>
							<div class="section_title_img" style="<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']!=7) {?>border-color:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;<?php } else { ?>border:none;<?php }?>background:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;">
								<img src="" data-src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/header/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['hdr_image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
							</div>
						<?php } else { ?>
							<div class="section_title"><span><?php echo $_smarty_tpl->tpl_vars['slide']->value['title'];?>
</span></div>
						<?php }?>
					<?php }?>
					<div id="section-slider-wrapper" style="<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']!=7) {?>border-color:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;<?php } else { ?>border:none;<?php }?>" >
						<?php if ($_smarty_tpl->tpl_vars['slide']->value['image']!='') {?>
							<div class="section_img mobile_hidden" >
							<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5) {?>
								<a class='nuren-presta-tag' presta-type='sectionslider' href="https://<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" target='_story'>
							<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
								<a class='nuren-presta-tag' presta-type='sectionslider' href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php } else { ?>
								<a class='nuren-presta-tag testtest' presta-type='sectionslider' href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php }?>								<img class='nuren-presta-tag' presta-type='sectionslider' 
									src="" data-src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
"
									<?php if (isset($_smarty_tpl->tpl_vars['slide']->value['size'])&&$_smarty_tpl->tpl_vars['slide']->value['size']) {?><?php echo $_smarty_tpl->tpl_vars['slide']->value['size'];?>
<?php } else { ?>width="100%" height="100%"<?php }?> 
									alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" 
								/>							</a>
							</div>
						<?php }?>
						<div class="section_products <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']=='') {?>section_full<?php }?>">							<?php if ($_smarty_tpl->tpl_vars['slide']->value['item_row_count']>1) {?>								<?php $_smarty_tpl->tpl_vars["colWidth"] = new Smarty_variable((99/($_smarty_tpl->tpl_vars['slide']->value['item_page_count']/$_smarty_tpl->tpl_vars['slide']->value['item_row_count'])), null, 0);?>								<?php $_smarty_tpl->tpl_vars["colWidth"] = new Smarty_variable("calc(".((string)$_smarty_tpl->tpl_vars['colWidth']->value)."% - 16px)", null, 0);?>
							<?php } else { ?>								<?php $_smarty_tpl->tpl_vars["colWidth"] = new Smarty_variable("auto", null, 0);?>							<?php }?>							<ul class="clearfix row section_slide<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
 <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']=='') {?>section5<?php }?> <?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==7) {?>section_content<?php }?> <?php if ($_smarty_tpl->tpl_vars['slide']->value['item_row_count']>1) {?>multirow<?php }?>" data-slidecount="<?php echo $_smarty_tpl->tpl_vars['slide']->value['item_page_count'];?>
" data-rowcount="<?php echo $_smarty_tpl->tpl_vars['slide']->value['item_row_count'];?>
">							<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['productcounter']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['productcounter']['iteration']++;
?>								<?php if ($_smarty_tpl->tpl_vars['slide']->value['item_row_count']>1) {?>
									<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['productcounter']['iteration']%$_smarty_tpl->tpl_vars['slide']->value['item_page_count']==1) {?>
										<li style="width:100%;">
									<?php }?>
									<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['productcounter']['iteration']%($_smarty_tpl->tpl_vars['slide']->value['item_page_count']/$_smarty_tpl->tpl_vars['slide']->value['item_row_count'])==1||$_smarty_tpl->tpl_vars['slide']->value['item_page_count']==$_smarty_tpl->tpl_vars['slide']->value['item_row_count']) {?>
										<div class="row<?php echo ceil($_smarty_tpl->getVariable('smarty')->value['foreach']['productcounter']['iteration']/($_smarty_tpl->tpl_vars['slide']->value['item_page_count']/$_smarty_tpl->tpl_vars['slide']->value['item_row_count']));?>
">
									<?php }?>
								<?php } else { ?>
								<li>								<?php }?>

									<div class="product-container" itemscope="" itemtype="http://schema.org/Product" style="display:inline-block;vertical-align:top;width: <?php echo $_smarty_tpl->tpl_vars['colWidth']->value;?>
">
										<div class="left-block">
											<div class="product-image-container">
												<?php if ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
													<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
												<?php } else { ?>
													<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
												<?php }?>
													
                          <img class="replace-2x img-responsive" src="" data-src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">
												<?php if ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
													<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
												<?php } else { ?>
													<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
												<?php }?>
													<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>

												</a>
											</h5>
											<div class="clear"></div>
											<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price" style="height:30px;">
												<span class="price product-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value["price"]),$_smarty_tpl);?>
</span>
												<meta itemprop="priceCurrency" content="<?php echo $_smarty_tpl->tpl_vars['currency']->value->iso_code;?>
">
												<meta itemprop="price" content="<?php echo $_smarty_tpl->tpl_vars['product']->value["price"];?>
">
												<?php if (isset($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']>0) {?>
													<span class="old-price product-price">
														<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['product']->value['price_without_reduction']),$_smarty_tpl);?>

													</span>
													<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'id_product'=>$_smarty_tpl->tpl_vars['product']->value['id_product'],'type'=>"old_price"),$_smarty_tpl);?>

													<?php if ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='percentage') {?>
														<span class="price-percent-reduction">-<?php echo $_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100;?>
%</span>
													<?php } elseif ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='amount'&&round($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100.0/$_smarty_tpl->tpl_vars['product']->value['price_without_reduction'],0)>=5) {?>
														<span class="price-percent-reduction amount_reduction">-<?php echo round($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100.0/$_smarty_tpl->tpl_vars['product']->value['price_without_reduction'],0);?>
%</span>
													<?php }?>
												<?php }?>
											</div>											<div class="button-container" style="border-top: 1px solid #eaeaea;"> 												<?php if ($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']==0&&$_smarty_tpl->tpl_vars['product']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['product']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>																				<?php if ((!isset($_smarty_tpl->tpl_vars['product']->value['customization_required'])||!$_smarty_tpl->tpl_vars['product']->value['customization_required'])&&($_smarty_tpl->tpl_vars['product']->value['allow_oosp']||$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>																						<?php $_smarty_tpl->_capture_stack[0][] = array('default', null, null); ob_start(); ?>add=1&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
															<?php if (isset($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'])&&$_smarty_tpl->tpl_vars['product']->value['id_product_attribute']) {?>&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
<?php }?>															<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {?>&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
<?php }?>														<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?> 														<a class="button ajax_add_to_cart_button btn btn-default col-xs-6" 															href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',true,null,Smarty::$_smarty_vars['capture']['default'],false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
" 															data-id-product-attribute="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
" data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" 															data-minimal_quantity="<?php if (isset($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity'])&&$_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']>=1) {?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']);?>
<?php } else { ?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['minimal_quantity']);?>
<?php }?>">																														<i class="icon-shopping-cart"></i>															<span><?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
</span> 														</a> 													<?php } else { ?> 														<span class="button ajax_add_to_cart_button btn btn-default col-xs-6" style="color:#ccc;"><?php echo smartyTranslate(array('s'=>'Out of Stock'),$_smarty_tpl);?>
</span>													<?php }?>													<?php } else { ?>																		<span class="col-xs-6">&nbsp;</span>												<?php }?>												<a class="button detail_button btn-default col-xs-6" style="text-align:right;" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url"> 													<i class="icon-list"></i>													<span><?php echo smartyTranslate(array('s'=>'Details'),$_smarty_tpl);?>
</span> 												</a>	
											</div>	
											<?php if ($_smarty_tpl->tpl_vars['slide']->value['show_timer']==1) {?>
											<div class="ticker_box">
												<div class="sales_end_label">Time Left</div>
												<div class="sales_end dsCountDown ds-custome" k-data="<?php echo $_smarty_tpl->tpl_vars['slide']->value['end_datetime'];?>
"></div>
											</div>
											<?php } else { ?><?php }?>
										</div>
									</div>
								<?php if ($_smarty_tpl->tpl_vars['slide']->value['item_row_count']>1) {?>
									<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['productcounter']['iteration']%($_smarty_tpl->tpl_vars['slide']->value['item_page_count']/$_smarty_tpl->tpl_vars['slide']->value['item_row_count'])==0||$_smarty_tpl->tpl_vars['slide']->value['item_page_count']==$_smarty_tpl->tpl_vars['slide']->value['item_row_count']) {?>
										</div>
									<?php }?>
									<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['productcounter']['iteration']%($_smarty_tpl->tpl_vars['slide']->value['item_row_count']*($_smarty_tpl->tpl_vars['slide']->value['item_page_count']/$_smarty_tpl->tpl_vars['slide']->value['item_row_count']))==0||$_smarty_tpl->getVariable('smarty')->value['foreach']['productcounter']['iteration']==count($_smarty_tpl->tpl_vars['slide']->value['products'])) {?>
										</li>										<?php }?>									
								<?php } else { ?>
									</li>									<?php }?>							<?php } ?>
							</ul>
						</div>
                        <div class="clearfix"></div>
						<?php if ($_smarty_tpl->tpl_vars['slide']->value['mobile_image']!='') {?>
							<div class="section_img desktop_hidden" >
							<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5) {?>
								<a class='nuren-presta-tag' presta-type='sectionslider' href="https://<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" target='_story'>
							<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
								<a class='nuren-presta-tag' presta-type='sectionslider' href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php } else { ?>
								<a class='nuren-presta-tag testtest' presta-type='sectionslider' href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php }?>
								<picture>
									<source media="(max-width: 580px)" srcset="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/m/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['mobile_image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
">									<img class='nuren-presta-tag' presta-type='sectionslider' 
										src="" data-src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/m/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['mobile_image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
"
										<?php if (isset($_smarty_tpl->tpl_vars['slide']->value['size'])&&$_smarty_tpl->tpl_vars['slide']->value['size']) {?><?php echo $_smarty_tpl->tpl_vars['slide']->value['size'];?>
<?php } else { ?>width="100%" height="100%"<?php }?> 
										alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" 
									/>
								</picture>								</a>
							</div>
						<?php }?>

					</div>
 				</div>
				<?php }?>
				<?php }?>
			<?php } ?>			
		</div>
		<div class="clearfix"></div>
		<script>		
		$(document).ready(function(){
			<?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sectionslider_slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value) {
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['slide']->value['layout_type']=="slide") {?>					var section = $("#section-slider ul.section_slide<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
");
					var visibleCount = 4;					var rowCount = 1;

					if(section.data("rowcount") !== undefined) {
						rowCount = section.data("rowcount");
					}					
					if(rowCount == 1) {
						if(section.data("slidecount") !== undefined) {							visibleCount = section.data("slidecount");
						}
					}
					else {
						visibleCount = 1;
					}

					var enableResponsiveBreakpoints = false;

					
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['item_row_count']==1) {?>
						enableResponsiveBreakpoints = true;
					<?php }?>
					
					if(!section.hasClass("section_content")) {
						$("#section-slider ul.section_slide<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
").flexisel({
						<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5) {?>
							visibleItems: visibleCount - 1,
						<?php } else { ?>
							visibleItems: visibleCount,
						<?php }?>
							flipPage: true,
							animationSpeed: 500,
							animationLoop :true,
							clone: false,
							autoPlay: false,							enableResponsiveBreakpoints:enableResponsiveBreakpoints
						});
					}
			<?php }?>
			<?php } ?>
		});		
		</script>
	<?php }?>
<?php }?><?php }} ?>
