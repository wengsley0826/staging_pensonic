<?php /* Smarty version Smarty-3.1.19, created on 2021-10-01 19:24:35
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9207742236156eff31f76b4-10820019%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f13efeeaae7e95ba94159288cc5ad02d013a2acd' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/productbundlespro/views/templates/admin/producttab/bundles.tpl',
      1 => 1621567095,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9207742236156eff31f76b4-10820019',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product_bundles' => 0,
    'bundle' => 0,
    'module_tab_url' => 0,
    'module_url' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6156eff3209a81_85612855',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6156eff3209a81_85612855')) {function content_6156eff3209a81_85612855($_smarty_tpl) {?>

<div id="pbp-bundles" class="panel product-tab">

	<h3><?php echo smartyTranslate(array('s'=>'Bundles','mod'=>'productbundlespro'),$_smarty_tpl);?>
</h3>

	<div class="alert alert-info">
		Don't forget - you must add the short code
		<strong>{hook h="displayPBP"}</strong> to your themes product.tpl to display the bundles.
	</div>

	<table id="bundlesTable" class="table tableDnD">
		<thead>
		<tr class="nodrag nodrop">
			<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'ID','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
			<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Tab','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
			<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Products','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
			<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Action','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
			<th><span class="title_box"><?php echo smartyTranslate(array('s'=>'Position','mod'=>'productbundlespro'),$_smarty_tpl);?>
</span></th>
		</tr>
		</thead>

		<tbody>
		<?php  $_smarty_tpl->tpl_vars['bundle'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['bundle']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_bundles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['bundle']->key => $_smarty_tpl->tpl_vars['bundle']->value) {
$_smarty_tpl->tpl_vars['bundle']->_loop = true;
?>
			<tr data-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
				<td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->tab->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td><?php echo mb_convert_encoding(htmlspecialchars(count($_smarty_tpl->tpl_vars['bundle']->value->products), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
				<td>
					<a href="#edit" class="pbp-edit-bundle btn" data-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon icon-pencil"></i></a>
					<a href="#copy" class="pbp-copy-bundle btn" data-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon icon-copy"></i></a>
					<a href="#export" class="pbp-export-bundle btn" data-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon icon-truck"></i></a>
					<a href="#delete" class="pbp-delete-bundle btn" data-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['bundle']->value->id_bundle, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon icon-trash"></i></a>
				</td>
				<td class="dragHandle pointer">
					<div class="dragGroup">
						&nbsp;
					</div>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>

	<a href="#add_bundle" id="pbp-add-bundle" class="btn btn-default" style="margin-top:20px;">
		<i class="icon-plus-sign"></i> <?php echo smartyTranslate(array('s'=>'Add a bundle','mod'=>'productbundlespro'),$_smarty_tpl);?>

	</a>

	<a href="#add_bundle_existing" id="pbp-add-bundle-existing" class="btn btn-default" style="margin-top:20px;">
		<i class="icon-plus-sign"></i> <?php echo smartyTranslate(array('s'=>'Add a existing bundle from another product','mod'=>'productbundlespro'),$_smarty_tpl);?>

	</a>

</div>

<script>
	module_tab_url = '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['module_tab_url']->value);?>
';
	module_url = '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['module_url']->value);?>
';
	pbp_ajax_url = "<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['link']->value->getModuleLink('productbundlespro','ajax',array()));?>
";

	http_get = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_GET);?>
;
	$(document).ready(function () {
		if (typeof(pbp_admin_tab_bundles_controller) === 'undefined')
			pbp_admin_tab_bundles_controller = new PBPAdminTabBundlesController("#pbp-bundles");
	});
</script><?php }} ?>
