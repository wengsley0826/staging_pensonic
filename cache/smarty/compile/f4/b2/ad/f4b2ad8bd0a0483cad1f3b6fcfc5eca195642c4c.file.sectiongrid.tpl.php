<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:26:56
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/hook/sectiongrid.tpl" */ ?>
<?php /*%%SmartyHeaderCode:129833446615c0c501987d9-79772180%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f4b2ad8bd0a0483cad1f3b6fcfc5eca195642c4c' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/claritassectionslider/views/templates/hook/sectiongrid.tpl',
      1 => 1599917686,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '129833446615c0c501987d9-79772180',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'slide' => 0,
    'link' => 0,
    'product' => 0,
    'currency' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0c50257d66_44109902',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0c50257d66_44109902')) {function content_615c0c50257d66_44109902($_smarty_tpl) {?><div class="section_container section-grid <?php if ($_smarty_tpl->tpl_vars['slide']->value['addon_class']!='') {?><?php echo $_smarty_tpl->tpl_vars['slide']->value['addon_class'];?>
<?php }?>">
	<?php if ($_smarty_tpl->tpl_vars['slide']->value['show_title']) {?>
		<?php if ($_smarty_tpl->tpl_vars['slide']->value['hdr_image']!='') {?>
			<div class="section_title_img" style="<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']!=7) {?>border-color:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;<?php } else { ?>border:none;<?php }?>background:<?php if ($_smarty_tpl->tpl_vars['slide']->value['bgColor']=='') {?>#ececec<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['slide']->value['bgColor'];?>
<?php }?>;">
				<img src="" data-src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."claritassectionslider/images/header/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['hdr_image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
			</div>
		<?php } else { ?>
			<div class="section_title"><span><?php echo $_smarty_tpl->tpl_vars['slide']->value['title'];?>
</span></div>
		<?php }?>
	<?php }?>
	<div class="more-link">
		<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5&&$_smarty_tpl->tpl_vars['slide']->value['url']!='') {?>
			<a class='nuren-presta-tag' presta-type='sectionslider' href="https://<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" target='_story'><?php echo smartyTranslate(array('s'=>'More','mod'=>'claritassectionslider'),$_smarty_tpl);?>
 ></a>
		<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
			<a class='nuren-presta-tag' presta-type='sectionslider' href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'More','mod'=>'claritassectionslider'),$_smarty_tpl);?>
 ></a>
		<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['url']!='') {?>
			<a class='nuren-presta-tag' presta-type='sectionslider' href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'More','mod'=>'claritassectionslider'),$_smarty_tpl);?>
 ></a>
		<?php }?>
	</div>
	<div id="section-slider-wrapper">
		<div class="section_products <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']=='') {?>section_full<?php }?>">
			<ul class="clearfix row section_slide<?php echo $_smarty_tpl->tpl_vars['slide']->value['id_slide'];?>
 <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']=='') {?>section5<?php }?> <?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==7) {?>section_content<?php }?>" >
			<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==7) {?>
				<div class="section-content col-xs-12 col-sm-12"><?php echo $_smarty_tpl->tpl_vars['slide']->value['description'];?>
</div>
			<?php } else { ?>
				<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
				<li class="col-xs-6 col-sm-4 col-md-3">
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['section_type']==5) {?>
						<div class="product-container" >
							<div class="left-block2">
								<div class="product-image-container">
									<a class="product_img_link2 nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo $_smarty_tpl->tpl_vars['product']->value['url'];?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" target='_story'>								
										<img class="replace-2x img-responsive" src="" data-src="<?php echo $_smarty_tpl->tpl_vars['product']->value['thumbnail'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
									</a>
								</div>
							</div>
							<div class="right-block">
								<h5 itemprop="name" class="product-title">												
									<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo $_smarty_tpl->tpl_vars['product']->value['url'];?>
&preview=1" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" target='_story'>
										<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['post_title'], ENT_QUOTES, 'UTF-8', true);?>

									</a>
								</h5>
								<div class="clear"></div>
							</div>
						</div>		
					<?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['section_type']==6) {?>
						<div class="product-container" >
							<div class="left-block">
								<div class="product-image-container">
										<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
										
										<img class="replace-2x img-responsive" src="" data-src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['imgLink'], ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
									</a>
								</div>
							</div>
							<div class="right-block right-block-review">
								<h5 itemprop="name" class="product-title">
									<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
										<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>

									</a>
								</h5>
								<div class="clear"></div>
								<div class="reviews_list_stars">
									<span class="star_content clearfix">
										<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["i"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['name'] = "i";
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] = (int) 0;
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] = is_array($_loop=5) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] = ((int) 1) == 0 ? 1 : (int) 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["i"]['total']);
?>
											<?php if ($_smarty_tpl->tpl_vars['product']->value['grade']<=$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']) {?>
												<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar2.png" class="list-img-star-category" alt="0"/>
											<?php } else { ?>
												<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar1.png" class="list-img-star-category" alt="0"/>
											<?php }?>
										<?php endfor; endif; ?>

										<span style="vertical-align:top;line-height:16px;">(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['grade'], ENT_QUOTES, 'UTF-8', true);?>
)</span>
										<meta itemprop="worstRating" content = "0" />
										<meta itemprop="ratingValue" content = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['grade'], ENT_QUOTES, 'UTF-8', true);?>
" />
										<meta itemprop="bestRating" content = "5" />
									</span>

								</div>
								<div class='line-height-default review-content-height'>
									<?php echo $_smarty_tpl->tpl_vars['product']->value['content'];?>

								</div>
								<div class='line-height-default'>
								<br>
									<strong itemprop="author"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['customer_name'], ENT_QUOTES, 'UTF-8', true);?>
 </strong>
								</div>
						</div>		
					<?php } else { ?>
						<div class="product-container" itemscope="" itemtype="http://schema.org/Product">
							<div class="left-block">
								<div class="product-image-container">
									<?php if ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
										<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
									<?php } else { ?>
										<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">													
									<?php }?>
										
										<img class="replace-2x img-responsive" src="" data-src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image">
									</a>
								</div>
							</div>
							<div class="right-block">
								<h5 itemprop="name" class="product-title">
									<?php if ($_smarty_tpl->tpl_vars['slide']->value['isBundle']) {?>
										<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['cat_rewrite'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
									<?php } else { ?>
										<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
									<?php }?>
										<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value["name"], ENT_QUOTES, 'UTF-8', true);?>

									</a>
								</h5>
								<div class="clear"></div>
								<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
									<span class="price product-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value["price"]),$_smarty_tpl);?>
</span>
									<meta itemprop="priceCurrency" content="<?php echo $_smarty_tpl->tpl_vars['currency']->value->iso_code;?>
">
									<meta itemprop="price" content="<?php echo $_smarty_tpl->tpl_vars['product']->value["price"];?>
">
									<?php if (isset($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']>0) {?>
										<span class="old-price product-price">
											<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['product']->value['price_without_reduction']),$_smarty_tpl);?>

										</span>
										<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'id_product'=>$_smarty_tpl->tpl_vars['product']->value['id_product'],'type'=>"old_price"),$_smarty_tpl);?>

										<?php if ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='percentage') {?>
											<span class="price-percent-reduction">-<?php echo $_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100;?>
%</span>
										<?php } elseif ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='amount'&&round($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100.0/$_smarty_tpl->tpl_vars['product']->value['price_without_reduction'],0)>=5) {?>
											<span class="price-percent-reduction amount_reduction">-<?php echo round($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100.0/$_smarty_tpl->tpl_vars['product']->value['price_without_reduction'],0);?>
%</span>
										<?php }?>
									<?php }?>
								</div>
							</div>
						</div>		
					<?php }?>
				</li>	
				<?php } ?>
			<?php }?>	
			</ul>
		</div>
        <div class="clearfix"></div>
	</div>
</div>
			<?php }} ?>
