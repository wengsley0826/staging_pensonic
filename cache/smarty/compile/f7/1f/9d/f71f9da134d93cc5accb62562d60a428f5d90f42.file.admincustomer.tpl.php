<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:38:58
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/admincustomer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2131080728615c0f2212ba55-59729908%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f71f9da134d93cc5accb62562d60a428f5d90f42' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/allinone_rewards/views/templates/hook/admincustomer.tpl',
      1 => 1602254037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2131080728615c0f2212ba55-59729908',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'msg' => 0,
    'core_templates' => 0,
    'template' => 0,
    'core_template_id' => 0,
    'loyalty_templates' => 0,
    'loyalty_template_id' => 0,
    'rewards' => 0,
    'totals' => 0,
    'rewards_account' => 0,
    'new_reward_value' => 0,
    'sign' => 0,
    'new_reward_state' => 0,
    'rewardStateDefault' => 0,
    'rewardStateValidation' => 0,
    'rewardStateCancel' => 0,
    'new_reward_reason' => 0,
    'rewards_duration' => 0,
    'reward' => 0,
    'states_for_update' => 0,
    'bUpdate' => 0,
    'rewardStateReturnPeriod' => 0,
    'module_template_dir' => 0,
    'payment_authorized' => 0,
    'payments' => 0,
    'payment' => 0,
    'customer' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0f221e8817_72755228',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0f221e8817_72755228')) {function content_615c0f221e8817_72755228($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/var/www/html/pensonicstore.claritascloud.com/tools/smarty/plugins/modifier.escape.php';
?>
<!-- MODULE allinone_rewards -->
<div class="<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>col-lg-12<?php } else { ?>clear<?php }?>" id="admincustomer">
<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
	<div class="panel">
		<div class="panel-heading"><?php echo smartyTranslate(array('s'=>'Rewards account','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</div>
		<?php if ($_smarty_tpl->tpl_vars['msg']->value) {?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['msg']->value, 'string', 'UTF-8');?>
<?php }?>
<?php } else { ?>
		<h2><?php echo smartyTranslate(array('s'=>'Rewards account','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h2>
		<?php if ($_smarty_tpl->tpl_vars['msg']->value) {?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['msg']->value, 'string', 'UTF-8');?>
<br><?php }?>
<?php }?>
		<form id="template_change" method="post">
			<input type="hidden" name="action" />
			<?php echo smartyTranslate(array('s'=>'Template used for "Rewards account"','mod'=>'allinone_rewards'),$_smarty_tpl);?>
&nbsp;
			<select class="change_template" name="core_template" style="display: inline; width: auto;">
				<option value='0'><?php echo smartyTranslate(array('s'=>'Default template','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</option>
				<?php  $_smarty_tpl->tpl_vars['template'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['template']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['core_templates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['template']->key => $_smarty_tpl->tpl_vars['template']->value) {
$_smarty_tpl->tpl_vars['template']->_loop = true;
?>
					<option <?php if ($_smarty_tpl->tpl_vars['template']->value['id_template']==$_smarty_tpl->tpl_vars['core_template_id']->value) {?>selected<?php }?> value='<?php echo intval($_smarty_tpl->tpl_vars['template']->value['id_template']);?>
'><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['template']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
				<?php } ?>
			</select>
			<span style="padding-left: 50px">
				<?php echo smartyTranslate(array('s'=>'Template used for "Loyalty program"','mod'=>'allinone_rewards'),$_smarty_tpl);?>
&nbsp;
				<select class="change_template" name="loyalty_template" style="display: inline; width: auto;">
					<option value='0'><?php echo smartyTranslate(array('s'=>'Default template','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</option>
					<?php  $_smarty_tpl->tpl_vars['template'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['template']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['loyalty_templates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['template']->key => $_smarty_tpl->tpl_vars['template']->value) {
$_smarty_tpl->tpl_vars['template']->_loop = true;
?>
						<option <?php if ($_smarty_tpl->tpl_vars['template']->value['id_template']==$_smarty_tpl->tpl_vars['loyalty_template_id']->value) {?>selected<?php }?> value='<?php echo intval($_smarty_tpl->tpl_vars['template']->value['id_template']);?>
'><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['template']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
					<?php } ?>
				</select>
			</span>
		</form>
		<br/><br/>
<?php if (count($_smarty_tpl->tpl_vars['rewards']->value)>0) {?>
	<?php if ((float)$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getValidationId()]>0) {?>
		<form id="rewards_reminder" method="post">
			<input class="button" name="submitRewardReminder" type="submit" value="<?php echo smartyTranslate(array('s'=>'Send an email with account balance :','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getValidationId()]),$_smarty_tpl);?>
" /> <?php if ($_smarty_tpl->tpl_vars['rewards_account']->value->date_last_remind&&$_smarty_tpl->tpl_vars['rewards_account']->value->date_last_remind!='0000-00-00 00:00:00') {?> (<?php echo smartyTranslate(array('s'=>'last email :','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['rewards_account']->value->date_last_remind,'full'=>1),$_smarty_tpl);?>
)<?php }?>
		</form><br>
	<?php }?>
		<table cellspacing="0" cellpadding="0" class="table">
			<thead>
				<tr style="background-color: #EEEEEE">
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Total rewards','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Already converted','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Paid','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Available','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Awaiting validation','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th style='text-align: center'><?php echo smartyTranslate(array('s'=>'Awaiting payment','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				</tr>
			</thead>
			<tr>
				<td class="center"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['totals']->value['total']),$_smarty_tpl);?>
</td>
				<td class="center"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getConvertId()]),$_smarty_tpl);?>
</td>
				<td class="center"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getPaidId()]),$_smarty_tpl);?>
</td>
				<td class="center"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getValidationId()]),$_smarty_tpl);?>
</td>
				<td class="center"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getDefaultId()]+$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getReturnPeriodId()]),$_smarty_tpl);?>
</td>
				<td class="center"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['totals']->value[RewardsStateModel::getWaitingPaymentId()]),$_smarty_tpl);?>
</td>
			</tr>
		</table>
<?php } else { ?>
	<?php echo smartyTranslate(array('s'=>'This customer has no reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>

<?php }?>
<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
	<form id="rewards_history" method="post">
		<div class="panel-heading" style="margin-top: 30px;"><?php echo smartyTranslate(array('s'=>'Add a new reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</div>
<?php } else { ?>
		<form id="rewards_history" method="post">
		<h3><?php echo smartyTranslate(array('s'=>'Add a new reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h3>
<?php }?>
		<?php echo smartyTranslate(array('s'=>'Value','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <input name="new_reward_value" type="text" size="6" value="<?php echo floatval($_smarty_tpl->tpl_vars['new_reward_value']->value);?>
" style="text-align: right; display: inline; width: auto"/> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
&nbsp;&nbsp;&nbsp;&nbsp;
		<?php echo smartyTranslate(array('s'=>'Status','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <select name="new_reward_state" style="display: inline; width: auto">
			<option <?php if ($_smarty_tpl->tpl_vars['new_reward_state']->value==RewardsStateModel::getDefaultId()) {?>selected<?php }?> value="<?php echo intval(RewardsStateModel::getDefaultId());?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['rewardStateDefault']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
			<option <?php if ($_smarty_tpl->tpl_vars['new_reward_state']->value==RewardsStateModel::getValidationId()) {?>selected<?php }?> value="<?php echo intval(RewardsStateModel::getValidationId());?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['rewardStateValidation']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
			<option <?php if ($_smarty_tpl->tpl_vars['new_reward_state']->value==RewardsStateModel::getCancelId()) {?>selected<?php }?> value="<?php echo intval(RewardsStateModel::getCancelId());?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['rewardStateCancel']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
		</select>&nbsp;&nbsp;&nbsp;&nbsp;
		<?php echo smartyTranslate(array('s'=>'Reason','mod'=>'allinone_rewards'),$_smarty_tpl);?>
 <input name="new_reward_reason" type="text" size="40" maxlength="80" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['new_reward_reason']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="display: inline; width: auto"/>
		<input class="button" name="submitNewReward" type="submit" value="<?php echo smartyTranslate(array('s'=>'Save settings','mod'=>'allinone_rewards'),$_smarty_tpl);?>
"/>
<?php if (count($_smarty_tpl->tpl_vars['rewards']->value)>0) {?>
	<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
		<div class="panel-heading" style="margin-top: 30px;"><?php echo smartyTranslate(array('s'=>'Rewards history','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</div>
	<?php } else { ?>
		<h3><?php echo smartyTranslate(array('s'=>'Rewards history','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h3>
	<?php }?>
		<input type="hidden" id="id_reward_to_update" name="id_reward_to_update" />
		<table cellspacing="0" cellpadding="0" class="tablesorter tablesorter-ice" id="rewards_list">
			<thead>
				<tr style="background-color: #EEEEEE">
					<th><?php echo smartyTranslate(array('s'=>'Event','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Date','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
	<?php if ($_smarty_tpl->tpl_vars['rewards_duration']->value>0) {?>
					<th><?php echo smartyTranslate(array('s'=>'Validity','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
	<?php }?>
					<th><?php echo smartyTranslate(array('s'=>'Total (without shipping)','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Reward','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Status','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th class='filter-false sorter-false'><?php echo smartyTranslate(array('s'=>'Action','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				</tr>
			</thead>
			<tbody>
	<?php  $_smarty_tpl->tpl_vars['reward'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['reward']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['rewards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['reward']->key => $_smarty_tpl->tpl_vars['reward']->value) {
$_smarty_tpl->tpl_vars['reward']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['iteration']++;
?>
		<?php ob_start();?><?php echo intval(in_array($_smarty_tpl->tpl_vars['reward']->value['id_reward_state'],$_smarty_tpl->tpl_vars['states_for_update']->value));?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["bUpdate"] = new Smarty_variable($_tmp1, null, 0);?>
				<tr class="<?php if (($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['iteration']%2)==0) {?>alt_row<?php }?>">
					<td><?php if (($_smarty_tpl->tpl_vars['bUpdate']->value&&$_smarty_tpl->tpl_vars['reward']->value['plugin']=="free")) {?><input name="reward_reason_<?php echo intval($_smarty_tpl->tpl_vars['reward']->value['id_reward']);?>
" type="text" size="30" maxlength="80" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['reward']->value['detail'], ENT_QUOTES, 'UTF-8', true);?>
" /><?php } else { ?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['reward']->value['detail'], 'string', 'UTF-8');?>
<?php }?></td>
					<td style="text-align: center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['reward']->value['date'], ENT_QUOTES, 'UTF-8', true);?>
</td>
		<?php if ($_smarty_tpl->tpl_vars['rewards_duration']->value>0) {?>
					<td style="text-align: center"><?php if ($_smarty_tpl->tpl_vars['reward']->value['id_reward_state']==RewardsStateModel::getValidationId()) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['reward']->value['validity'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>&nbsp;<?php }?></td>
		<?php }?>
					<td align="right" class="price"><?php if ((int)$_smarty_tpl->tpl_vars['reward']->value['id_order']>0) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['reward']->value['total_without_shipping'],'currency'=>$_smarty_tpl->tpl_vars['reward']->value['id_currency']),$_smarty_tpl);?>
<?php } else { ?>-<?php }?></td>
					<td align="right"><?php if ($_smarty_tpl->tpl_vars['bUpdate']->value) {?><input name="reward_value_<?php echo intval($_smarty_tpl->tpl_vars['reward']->value['id_reward']);?>
" type="text" size="6" value="<?php echo sprintf('%.2f',$_smarty_tpl->tpl_vars['reward']->value['credits']);?>
" style="text-align: right; display: inline; width: auto"/> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['reward']->value['credits']),$_smarty_tpl);?>
<?php }?></td>
					<td>
		<?php if ($_smarty_tpl->tpl_vars['bUpdate']->value) {?>
						<select name="reward_state_<?php echo intval($_smarty_tpl->tpl_vars['reward']->value['id_reward']);?>
" style="display: inline; width: auto">
							<option <?php if ($_smarty_tpl->tpl_vars['reward']->value['id_reward_state']==RewardsStateModel::getDefaultId()) {?>selected<?php }?> value="<?php echo intval(RewardsStateModel::getDefaultId());?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['rewardStateDefault']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
							<option <?php if ($_smarty_tpl->tpl_vars['reward']->value['id_reward_state']==RewardsStateModel::getValidationId()) {?>selected<?php }?> value="<?php echo intval(RewardsStateModel::getValidationId());?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['rewardStateValidation']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
							<option <?php if ($_smarty_tpl->tpl_vars['reward']->value['id_reward_state']==RewardsStateModel::getCancelId()) {?>selected<?php }?> value="<?php echo intval(RewardsStateModel::getCancelId());?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['rewardStateCancel']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
			<?php if (($_smarty_tpl->tpl_vars['reward']->value['id_reward_state']==RewardsStateModel::getReturnPeriodId()||((int)$_smarty_tpl->tpl_vars['reward']->value['id_order']>0&&Configuration::get('REWARDS_WAIT_RETURN_PERIOD')&&Configuration::get('PS_ORDER_RETURN')&&(int)Configuration::get('PS_ORDER_RETURN_NB_DAYS')>0))) {?>
							<option <?php if ($_smarty_tpl->tpl_vars['reward']->value['id_reward_state']==RewardsStateModel::getReturnPeriodId()) {?>selected<?php }?> value="<?php echo intval(RewardsStateModel::getReturnPeriodId());?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['rewardStateReturnPeriod']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php echo smartyTranslate(array('s'=>'(Return period)','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</option>
			<?php }?>
						</select>
		<?php } else { ?>
						<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['reward']->value['state'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

		<?php }?>
					</td>
					<td style="text-align: center"><?php if ($_smarty_tpl->tpl_vars['bUpdate']->value) {?><input class="button" name="submitRewardUpdate" type="submit" value="<?php echo smartyTranslate(array('s'=>'Save settings','mod'=>'allinone_rewards'),$_smarty_tpl);?>
" onClick="$('#id_reward_to_update').val(<?php echo intval($_smarty_tpl->tpl_vars['reward']->value['id_reward']);?>
)"><?php }?></td>
				</tr>
	<?php } ?>
			</tbody>
		</table>
		<div class="pager">
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/first.png" class="first"/>
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/prev.png" class="prev"/>
	    	<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/next.png" class="next"/>
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/last.png" class="last"/>
	    	<select class="pagesize" style="width: auto; display: inline">
	      		<option value='10'>10</option>
	      		<option value='20'>20</option>
	      		<option value='50'>50</option>
	      		<option value='100'>100</option>
	      		<option value='500'>500</option>
	    	</select>
		</div>

	<?php if ($_smarty_tpl->tpl_vars['payment_authorized']->value) {?>
		<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
		<div class="panel-heading" style="margin-top: 30px;"><?php echo smartyTranslate(array('s'=>'Payments history','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</div>
		<?php } else { ?>
		<h3><?php echo smartyTranslate(array('s'=>'Payments history','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</h3>
		<?php }?>
		<?php if ((count($_smarty_tpl->tpl_vars['payments']->value))) {?>
		<table cellspacing="0" cellpadding="0" class="tablesorter tablesorter-ice" id="payments_list">
			<thead>
				<tr style="background-color: #EEEEEE">
					<th><?php echo smartyTranslate(array('s'=>'Request date','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Payment date','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Value','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th><?php echo smartyTranslate(array('s'=>'Details','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th class='filter-false sorter-false'><?php echo smartyTranslate(array('s'=>'Invoice','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
					<th class='filter-false sorter-false'><?php echo smartyTranslate(array('s'=>'Action','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</th>
				</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['payment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['payment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['payments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['payment']->key => $_smarty_tpl->tpl_vars['payment']->value) {
$_smarty_tpl->tpl_vars['payment']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['iteration']++;
?>
				<tr class="<?php if (($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['iteration']%2)==0) {?>alt_row<?php }?>">
					<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['payment']->value['date_add'], ENT_QUOTES, 'UTF-8', true);?>
</td>
					<td><?php if ($_smarty_tpl->tpl_vars['payment']->value['paid']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['payment']->value['date_upd'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>-<?php }?></td>
					<td style="text-align: right"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['payment']->value['credits']),$_smarty_tpl);?>
</td>
					<td><?php echo nl2br(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['payment']->value['detail'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
</td>
					<td style="text-align: center"><?php if ($_smarty_tpl->tpl_vars['payment']->value['invoice']) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
uploads/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['payment']->value['invoice'], ENT_QUOTES, 'UTF-8', true);?>
" download="Invoice.pdf"><?php echo smartyTranslate(array('s'=>'View','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</a><?php } else { ?>-<?php }?></td>
					<td style="text-align: center"><?php if (!$_smarty_tpl->tpl_vars['payment']->value['paid']) {?><a href="index.php?tab=AdminCustomers&id_customer=<?php echo intval($_smarty_tpl->tpl_vars['customer']->value->id);?>
&viewcustomer&token=<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getAdminToken'][0][0]->getAdminTokenLiteSmarty(array('tab'=>'AdminCustomers'),$_smarty_tpl);?>
&accept_payment=<?php echo intval($_smarty_tpl->tpl_vars['payment']->value['id_payment']);?>
"><?php echo smartyTranslate(array('s'=>'Mark as paid','mod'=>'allinone_rewards'),$_smarty_tpl);?>
</a><?php } else { ?>-<?php }?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<div class="pager">
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/first.png" class="first"/>
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/prev.png" class="prev"/>
	    	<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/next.png" class="next"/>
	    	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_template_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
js/tablesorter/addons/pager/last.png" class="last"/>
	    	<select class="pagesize" style="width: auto; display: inline">
	      		<option value='10'>10</option>
	      		<option value='20'>20</option>
	      		<option value='50'>50</option>
	      		<option value='100'>100</option>
	      		<option value='500'>500</option>
	    	</select>
		</div>
		<?php } else { ?>
			<?php echo smartyTranslate(array('s'=>'No payment request found','mod'=>'allinone_rewards'),$_smarty_tpl);?>

		<?php }?>
	<?php }?>
<?php }?>

<?php if (version_compare(@constant('_PS_VERSION_'),'1.6','>=')) {?>
	</div>
<?php }?>
	<script>
		var footer_pager = "<?php echo smartyTranslate(array('s'=>'{startRow} to {endRow} of {totalRows} rows','mod'=>'allinone_rewards'),$_smarty_tpl);?>
";
	</script>
	</form>
</div>
<!-- END : MODULE allinone_rewards --><?php }} ?>
