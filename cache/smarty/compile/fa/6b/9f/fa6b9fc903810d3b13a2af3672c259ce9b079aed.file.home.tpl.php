<?php /* Smarty version Smarty-3.1.19, created on 2021-10-05 16:26:56
         compiled from "/var/www/html/pensonicstore.claritascloud.com/modules/ph_testimonials/views/templates/hook/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1296913419615c0c50d21c71-97810318%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa6b9fc903810d3b13a2af3672c259ce9b079aed' => 
    array (
      0 => '/var/www/html/pensonicstore.claritascloud.com/modules/ph_testimonials/views/templates/hook/home.tpl',
      1 => 1594691417,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1296913419615c0c50d21c71-97810318',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'testimonials' => 0,
    'title' => 0,
    'link' => 0,
    'testimonial' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_615c0c50d6cf45_31968203',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_615c0c50d6cf45_31968203')) {function content_615c0c50d6cf45_31968203($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['testimonials']->value) {?>
<div id="ph_testimonials_module">
	<h1 class="page-heading">
		<span class="cat-name">
			<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8', true);?>

		</span>
		<div class="testimonialButton">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('ph_testimonials','list'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>"View All Testimonials",'mod'=>"ph_testimonials"),$_smarty_tpl);?>
">
				<?php echo smartyTranslate(array('s'=>"See All Reviews",'mod'=>"ph_testimonials"),$_smarty_tpl);?>

			</a>
			
		</div>
	</h1>

	<div class="testimonials-carousel-wrapper owl-carousel owl-theme clearfix clearBoth" id="ph_testimonials">
		<?php  $_smarty_tpl->tpl_vars['testimonial'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['testimonial']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['testimonials']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['testimonial']->key => $_smarty_tpl->tpl_vars['testimonial']->value) {
$_smarty_tpl->tpl_vars['testimonial']->_loop = true;
?>
		<div class="testimonials-carousel-item">
			<div class="row">
            
            <ul class="landing-page-testimonial">
            
				<?php if ($_smarty_tpl->tpl_vars['testimonial']->value['image']) {?>
			<li>
            <div class="image-wrapper">
						<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['testimonial']->value['image'], ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['testimonial']->value['author_name'], ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['testimonial']->value['author_info']) {?> (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['testimonial']->value['author_info'], ENT_QUOTES, 'UTF-8', true);?>
)<?php }?>" class=" "/>
				</div>
                </li>
				<?php }?>
                
                <li>
				<div class="content-item <?php if ($_smarty_tpl->tpl_vars['testimonial']->value['image']) {?>col-md-12<?php } else { ?>col-md-12<?php }?>">
					<blockquote><?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['testimonial']->value['content'],150,'...'));?>

				 		
				 	</blockquote>
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('ph_testimonials','list'), ENT_QUOTES, 'UTF-8', true);?>
#ph_testimonials-item-<?php echo intval($_smarty_tpl->tpl_vars['testimonial']->value['id_prestahome_testimonial']);?>
">
				 			<?php echo smartyTranslate(array('s'=>'Read more','mod'=>'ph_testimonials'),$_smarty_tpl);?>

				 		</a> 
					<h4>
						
						 		<span>
								<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['testimonial']->value['author_name'], ENT_QUOTES, 'UTF-8', true);?>

								</span> 
						
						
					</h4>
				</div>
                </li>
                
                </ul>
			</div>
			
		</div><!-- .testimonials-carousel-item -->
		<?php } ?>
	</div><!-- .testimonials-carousel-wrapper -->
</div><!-- .container -->

<script>
<?php if (Configuration::get('PH_TESTIMONIALS_CAROUSEL')) {?>
var testimonialsAutoPlay = <?php if (Configuration::get('PH_TESTIMONIALS_AUTOPLAY')) {?><?php echo intval(Configuration::get('PH_TESTIMONIALS_AUTOPLAY'));?>
<?php } else { ?>false<?php }?>;
var nbTestimonialsCarouselDesktop = <?php echo intval(Configuration::get('PH_TESTIMONIALS_ITEMS'));?>
;
var nbTestimonialsCarouselTablet = 2;
var nbTestimonialsCarouselMobile = 1;
var nbTestimonialsCarouselNavigation = <?php if (Configuration::get('PH_TESTIMONIALS_NAVIGATION')) {?>true<?php } else { ?>false<?php }?>;
var testimonialsCarouselNextText = '<?php echo smartyTranslate(array('s'=>'Next','mod'=>'ph_testimonials','js'=>1),$_smarty_tpl);?>
';
var testimonialsCarouselPrevText = '<?php echo smartyTranslate(array('s'=>'Previous','mod'=>'ph_testimonials','js'=>1),$_smarty_tpl);?>
';

$(function() {
    $(window).on('resize', initPhTestimonials);
    $(window).on('load', initPhTestimonials);
});

function initPhTestimonials()
{
	$("#ph_testimonials").owlCarousel({
        autoPlay 			: testimonialsAutoPlay,
        stopOnHover 		: true,
        navigation			: nbTestimonialsCarouselNavigation,
        pagination			: false,
        transitionStyle		: false,
        items 				: 3,
        itemsDesktop 		: 3,
        itemsDesktopSmall 	: nbTestimonialsCarouselTablet,
        itemsTablet 		: [981,nbTestimonialsCarouselTablet],
        itemsMobile			: [550,nbTestimonialsCarouselMobile],
        navigationText: [
	    	'&laquo; ' + testimonialsCarouselPrevText,
	    	testimonialsCarouselNextText + ' &raquo;'
	    ],

    });
}
</script>
<?php }?>
<?php }?><?php }} ?>
