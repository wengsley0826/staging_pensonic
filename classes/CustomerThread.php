<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CustomerThreadCore extends ObjectModel
{
    public $id;
    public $id_shop;
    public $id_lang;
    public $id_contact;
    public $id_customer;
    public $id_order;
    public $id_product;
    public $status;
    public $email;
    public $firstname;
    public $lastname;
    public $token;
    public $date_add;
    public $date_upd;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'customer_thread',
        'primary' => 'id_customer_thread',
        'fields' => array(
            'id_lang' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_contact' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_shop' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_customer' =>array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_order' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'email' =>        array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 254),
            'lastname' =>     array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 32),
            'firstname' =>    array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 32),
            'token' =>        array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'status' =>    array('type' => self::TYPE_STRING),
            'date_add' =>    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

    protected $webserviceParameters = array(
        'fields' => array(
            'id_lang' => array(
                'xlink_resource' => 'languages'
            ),
            'id_shop' => array(
                'xlink_resource' => 'shops'
            ),
            'id_customer' => array(
                'xlink_resource' => 'customers'
            ),
            'id_order' => array(
                'xlink_resource' => 'orders'
            ),
            'id_product' => array(
                'xlink_resource' => 'products'
            ),
        ),
        'associations' => array(
            'customer_messages' => array(
                'resource' => 'customer_message',
                'id' => array('required' => true)),
        )
    );

    public function getWsCustomerMessages()
    {
        return Db::getInstance()->executeS('
		SELECT `id_customer_message` id
		FROM `'._DB_PREFIX_.'customer_message`
		WHERE `id_customer_thread` = '.(int)$this->id);
    }

    public function delete()
    {
        if (!Validate::isUnsignedId($this->id)) {
            return false;
        }

        $return = true;
        $result = Db::getInstance()->executeS('
			SELECT `id_customer_message`
			FROM `'._DB_PREFIX_.'customer_message`
			WHERE `id_customer_thread` = '.(int)$this->id
        );

        if (count($result)) {
            foreach ($result as $res) {
                $message = new CustomerMessage((int)$res['id_customer_message']);
                if (!Validate::isLoadedObject($message)) {
                    $return = false;
                } else {
                    $return &= $message->delete();
                }
            }
        }
        $return &= parent::delete();
        return $return;
    }

    public static function getCustomerMessages($id_customer, $read = null, $id_order = null)
    {
        $sql = 'SELECT *
			FROM '._DB_PREFIX_.'customer_thread ct
			LEFT JOIN '._DB_PREFIX_.'customer_message cm
				ON ct.id_customer_thread = cm.id_customer_thread
			WHERE id_customer = '.(int)$id_customer;

        if ($read !== null) {
            $sql .= ' AND cm.`read` = '.(int)$read;
        }
        if ($id_order !== null) {
            $sql .= ' AND ct.`id_order` = '.(int)$id_order;
        }

        $sql .= ' ORDER BY cm.date_add DESC';

        return Db::getInstance()->executeS($sql);
    }

    public static function getIdCustomerThreadByEmailAndIdOrder($email, $id_order)
    {
        return Db::getInstance()->getValue('
			SELECT cm.id_customer_thread
			FROM '._DB_PREFIX_.'customer_thread cm
			WHERE cm.email = \''.pSQL($email).'\'
				AND cm.id_shop = '.(int)Context::getContext()->shop->id.'
				AND cm.id_order = '.(int)$id_order
        );
    }

    public static function getContacts()
    {
        return Db::getInstance()->executeS('
			SELECT cl.*, COUNT(*) as total, (
				SELECT id_customer_thread
				FROM '._DB_PREFIX_.'customer_thread ct2
				WHERE status = "open" AND ct.id_contact = ct2.id_contact
				'.Shop::addSqlRestriction().'
				ORDER BY date_upd ASC
				LIMIT 1
			) as id_customer_thread
			FROM '._DB_PREFIX_.'customer_thread ct
			LEFT JOIN '._DB_PREFIX_.'contact_lang cl
				ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.(int)Context::getContext()->language->id.')
			WHERE ct.status = "open"
				AND ct.id_contact IS NOT NULL
				AND cl.id_contact IS NOT NULL
				'.Shop::addSqlRestriction().'
			GROUP BY ct.id_contact HAVING COUNT(*) > 0
		');
    }

    public static function getTotalCustomerThreads($where = null)
    {
        if (is_null($where)) {
            return (int)Db::getInstance()->getValue('
				SELECT COUNT(*)
				FROM '._DB_PREFIX_.'customer_thread
				WHERE 1 '.Shop::addSqlRestriction()
            );
        } else {
            return (int)Db::getInstance()->getValue('
				SELECT COUNT(*)
				FROM '._DB_PREFIX_.'customer_thread
				WHERE '.$where.Shop::addSqlRestriction()
            );
        }
    }

    public static function getMessageCustomerThreads($id_customer_thread)
    {
        return Db::getInstance()->executeS('
			SELECT ct.*, cm.*, cl.name subject, CONCAT(e.firstname, \' \', e.lastname) employee_name,
				CONCAT(c.firstname, \' \', c.lastname) customer_name, c.firstname
			FROM '._DB_PREFIX_.'customer_thread ct
			LEFT JOIN '._DB_PREFIX_.'customer_message cm
				ON (ct.id_customer_thread = cm.id_customer_thread)
			LEFT JOIN '._DB_PREFIX_.'contact_lang cl
				ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.(int)Context::getContext()->language->id.')
			LEFT JOIN '._DB_PREFIX_.'employee e
				ON e.id_employee = cm.id_employee
			LEFT JOIN '._DB_PREFIX_.'customer c
				ON (IFNULL(ct.id_customer, ct.email) = IFNULL(c.id_customer, c.email))
			WHERE ct.id_customer_thread = '.(int)$id_customer_thread.'
			ORDER BY cm.date_add ASC
		');
    }

    public static function getNextThread($id_customer_thread)
    {
        $context = Context::getContext();
        return Db::getInstance()->getValue('
			SELECT id_customer_thread
			FROM '._DB_PREFIX_.'customer_thread ct
			WHERE ct.status = "open"
			AND ct.date_upd = (
				SELECT date_add FROM '._DB_PREFIX_.'customer_message
				WHERE (id_employee IS NULL OR id_employee = 0)
					AND id_customer_thread = '.(int)$id_customer_thread.'
				ORDER BY date_add DESC LIMIT 1
			)
			'.($context->cookie->{'customer_threadFilter_cl!id_contact'} ?
                'AND ct.id_contact = '.(int)$context->cookie->{'customer_threadFilter_cl!id_contact'} : '').'
			'.($context->cookie->{'customer_threadFilter_l!id_lang'} ?
                'AND ct.id_lang = '.(int)$context->cookie->{'customer_threadFilter_l!id_lang'} : '').
            ' ORDER BY ct.date_upd ASC
		');
    }
    
    public static function syncIMAP () {
        if (!($url = Configuration::get('PS_SAV_IMAP_URL'))
            || !($port = Configuration::get('PS_SAV_IMAP_PORT'))
            || !($user = Configuration::get('PS_SAV_IMAP_USER'))
            || !($password = Configuration::get('PS_SAV_IMAP_PWD'))) {
            return array('hasError' => true, 'errors' => array('IMAP configuration is not correct'));
        }

        $conf = Configuration::getMultiple(array(
            'PS_SAV_IMAP_OPT_NORSH', 'PS_SAV_IMAP_OPT_SSL',
            'PS_SAV_IMAP_OPT_VALIDATE-CERT', 'PS_SAV_IMAP_OPT_NOVALIDATE-CERT',
            'PS_SAV_IMAP_OPT_TLS', 'PS_SAV_IMAP_OPT_NOTLS'));

        $conf_str = '';
        if ($conf['PS_SAV_IMAP_OPT_NORSH']) {
            $conf_str .= '/norsh';
        }
        if ($conf['PS_SAV_IMAP_OPT_SSL']) {
            $conf_str .= '/ssl';
        }
        if ($conf['PS_SAV_IMAP_OPT_VALIDATE-CERT']) {
            $conf_str .= '/validate-cert';
        }
        if ($conf['PS_SAV_IMAP_OPT_NOVALIDATE-CERT']) {
            $conf_str .= '/novalidate-cert';
        }
        if ($conf['PS_SAV_IMAP_OPT_TLS']) {
            $conf_str .= '/tls';
        }
        if ($conf['PS_SAV_IMAP_OPT_NOTLS']) {
            $conf_str .= '/notls';
        }

        if (!function_exists('imap_open')) {
            return array('hasError' => true, 'errors' => array('imap is not installed on this server'));
        }

        $mbox = @imap_open('{'.$url.':'.$port.$conf_str.'}', $user, $password);

        //checks if there is no error when connecting imap server
        $errors = imap_errors();
        if (is_array($errors)) {
            $errors = array_unique($errors);
        }
        $str_errors = '';
        $str_error_delete = '';

        if (count($errors) && is_array($errors)) {
            $str_errors = '';
            foreach ($errors as $error) {
                $str_errors .= $error.', ';
            }
            $str_errors = rtrim(trim($str_errors), ',');
        }
        //checks if imap connexion is active
        if (!$mbox) {
            return array('hasError' => true, 'errors' => array('Cannot connect to the mailbox :<br />'.($str_errors)));
        }

        //Returns information about the current mailbox. Returns FALSE on failure.
        $check = imap_check($mbox);
        if (!$check) {
            return array('hasError' => true, 'errors' => array('Fail to get information about the current mailbox'));
        }

        if ($check->Nmsgs == 0) {
            return array('hasError' => true, 'errors' => array('NO message to sync'));
        }

        $result = imap_fetch_overview($mbox, "1:{$check->Nmsgs}", 0);

        foreach ($result as $overview) {
            //check if message exist in database
            if (isset($overview->subject)) {
                $subject = $overview->subject;
            } else {
                $subject = '';
            }

            if(!isset($overview->date))
                continue;

            //Creating an md5 to check if message has been allready processed
            $md5 = md5($overview->date.$overview->from.$subject.$overview->msgno);
            $exist = Db::getInstance()->getValue(
                'SELECT `md5_header`
						 FROM `'._DB_PREFIX_.'customer_message_sync_imap`
						 WHERE `md5_header` = \''.pSQL($md5).'\'');
            if ($exist) {
                if (Configuration::get('PS_SAV_IMAP_DELETE_MSG')) {
                    if (!imap_delete($mbox, $overview->msgno)) {
                        $str_error_delete = ', Fail to delete message';
                    }
                }
            } else {
                //check if subject has id_order
                preg_match('/\#ct([0-9]*)/', $subject, $matches1);
                preg_match('/\#tc([0-9-a-z-A-Z]*)/', $subject, $matches2);
                $match_found = false;
                if (isset($matches1[1]) && isset($matches2[1])) {
                    $match_found = true;
                }

                $new_ct = (Configuration::get('PS_SAV_IMAP_CREATE_THREADS') && !$match_found && (strpos($subject, '[no_sync]') == false));

                if ($match_found || $new_ct) {
                    if ($new_ct) {
                        if (!preg_match('/('.Tools::cleanNonUnicodeSupport('[a-z\p{L}0-9!#$%&\'*+\/=?^`{}|~_-]+[.a-z\p{L}0-9!#$%&\'*+\/=?^`{}|~_-]*@[a-z\p{L}0-9]+[._a-z\p{L}0-9-]*\.[a-z0-9]+').')/', $overview->from, $res)
                            || !Validate::isEmail($from = $res[1])) {
                            continue;
                        }

                        // we want to assign unrecognized mails to the right contact category
                        $contacts = Contact::getContacts(3);
                        if (!$contacts) {
                            continue;
                        }
                        
                        if ($from==Configuration::get('PS_SHOP_EMAIL'))
					        continue;
                            
                        foreach ($contacts as $contact) {
                            if (strpos($overview->to, $contact['email']) !== false) {
                                $id_contact = $contact['id_contact'];
                            }
                        }

                        if (!isset($id_contact)) { // if not use the default contact category
                            $id_contact = $contacts[0]['id_contact'];
                        }

                        $customer = new Customer;
                        $client = $customer->getByEmail($from); //check if we already have a customer with this email
                        $ct = new CustomerThread();
                        if (isset($client->id)) { //if mail is owned by a customer assign to him
                            $ct->id_customer = $client->id;
                            $ct->firstname = $client->firstname;
                            $ct->lastname = $client->lastname;
                        }
                        $ct->email = $from;
                        $ct->id_contact = $id_contact;
                        $ct->id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
                        $ct->id_shop = 1; //new customer threads for unrecognized mails are not shown without shop id
                        $ct->status = 'open';
                        $ct->token = Tools::passwdGen(12);
                        $ct->add();
                    } else {
                        $ct = new CustomerThread((int)$matches1[1]);
                    } //check if order exist in database

                    if (Validate::isLoadedObject($ct) && ((isset($matches2[1]) && $ct->token == $matches2[1]) || $new_ct)) {
                        CustomerThread::$emailContent = "";
                        CustomerThread::$charset = "";
                        CustomerThread::$attachments = [];

                        $mid = $overview->msgno;
                        $mail_structure = imap_fetchstructure ($mbox, $mid);

                        if (!$mail_structure->parts)  // simple
                            CustomerThread::getpart($mbox,$mid,$mail_structure,0);  // pass 0 as part-number
                        else {  // multipart: cycle through each part
                            foreach ($mail_structure->parts as $partno0=>$p)
                                CustomerThread::getpart($mbox,$mid,$p,$partno0+1);
                        }

                        //$message = imap_fetchbody($mbox, $overview->msgno, 1);
                        //$message = quoted_printable_decode($message);
                        //$message = utf8_encode($message);
                        //$message = quoted_printable_decode($message);
                        //$message = nl2br($message);
                        //$message = Tools::substr($message, 0, (int) CustomerMessage::$definition['fields']['message']['size']);
                        $message = CustomerThread::$emailContent;

                        $cm = new CustomerMessage();
                        $cm->id_customer_thread = $ct->id;
                        if (empty($message) || !Validate::isCleanHtml($message)) {
                            $str_errors.= Tools::displayError(sprintf('Invalid Message Content for subject: %1s', $subject));
                        } else {
                            $cm->message = $message;
                            $cm->msg_subject = $subject;
                            $cm->mail_ts = $overview->date;
                            $cm->add();

                            if(CustomerThread::$attachments != null && count(CustomerThread::$attachments) > 0) 
                            {                                
                                $dir = dirname(__FILE__)."/../cs/". $cm->id;

                                if(!file_exists ($dir)) {
                                    mkdir($dir, 0777, true);
                                }

                                foreach(CustomerThread::$attachments as $key=>$attach) {
                                    file_put_contents($dir.'/'.$key, $attach);
                                }
                            }
                        }
                    }
                }
                Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'customer_message_sync_imap` (`md5_header`) VALUES (\''.pSQL($md5).'\')');
            }
        }
        
        imap_expunge($mbox);
        imap_close($mbox);
        if ($str_errors.$str_error_delete) {
            return array('hasError' => true, 'errors' => array($str_errors.$str_error_delete));
        }
        else {
            return array('hasError' => false, 'errors' => '');
        }
    }

    static $emailContent = "";
    static $charset = "";
    static $attachments = [];
    private static function getpart($mbox,$mid,$p,$partno) {
        // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
        //global $htmlmsg,$plainmsg,$charset,$attachments;
       
        // DECODE DATA
        $data = ($partno)?
            imap_fetchbody($mbox,$mid,$partno):  // multipart
            imap_body($mbox,$mid);  // simple
        // Any part may be encoded, even plain text messages, so check everything.
        if ($p->encoding==4)
            $data = quoted_printable_decode($data);
        elseif ($p->encoding==3)
            $data = base64_decode($data);

        // PARAMETERS
        // get all parameters, like charset, filenames of attachments, etc.
        $params = array();
        if ($p->ifparameters)
            foreach ($p->parameters as $x)
                $params[strtolower($x->attribute)] = $x->value;
        if ($p->ifdparameters)
            foreach ($p->dparameters as $x)
                $params[strtolower($x->attribute)] = $x->value;

        // ATTACHMENT
        // Any part with a filename is an attachment,
        // so an attached text file (type 0) is not mistaken as the message.
        if ((isset($params['filename']) && $params['filename'] != "") || (isset($params['name']) && $params['name'] != "")) {
            // filename may be given as 'Filename' or 'Name' or both
            $filename = (isset($params['filename']) && $params['filename'] != "")? $params['filename'] : $params['name'];
            // filename may be encoded, so see imap_mime_header_decode()
            CustomerThread::$attachments[$filename] = $data;  // this is a problem if two files have same name
        }

        // TEXT
        if ($p->type==0 && $data) {
            // Messages may be split in different parts because of inline attachments,
            // so append parts together with blank row.
            if (strtolower($p->subtype)=='plain')
                CustomerThread::$emailContent.= trim($data) ."\n\n";
            else
                CustomerThread::$emailContent.= $data ."<br><br>";
            CustomerThread::$charset = $params['charset'];  // assume all parts are same charset
        }

        // EMBEDDED MESSAGE
        // Many bounce notifications embed the original message as type 2,
        // but AOL uses type 1 (multipart), which is not handled here.
        // There are no PHP functions to parse embedded messages,
        // so this just appends the raw source to the main message.
        elseif ($p->type==2 && $data) {
            CustomerThread::$emailContent.= $data."\n\n";
        }

        // SUBPART RECURSION
        if (isset($p->parts)) {
            foreach ($p->parts as $partno0=>$p2)
                CustomerThread::getpart($mbox,$mid,$p2,$partno.'.'.($partno0+1));  // 1.2, 1.2.1, etc.
        }
    }
}
