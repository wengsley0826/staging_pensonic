<?php
class HTMLTemplateConsignmentCore extends HTMLTemplate
{
    public $orderids;
    public $order;
    
    /**
     * @param Order $order
     * @param $smarty
     * @throws PrestaShopException
     */
    public function __construct($orderids, $smarty, $bulk_mode = false)
    {
        $this->orderids = explode(',', $orderids);
        
        if(count($this->orderids) == 1) {
            $this->order= new Order($this->orderids[0]);
        }
      
        $this->smarty = $smarty;

        // header informations
        //$this->date = Tools::displayDate($order->date_add);
        $id_lang = Context::getContext()->language->id;
        //$this->shop = new Shop((int)$this->order->id_shop);
    }

    /**
     * Returns the template's HTML header
     *
     * @return string HTML header
     */
    public function getHeader()
    {
        $this->assignCommonHeaderData();
        $this->smarty->assign(array('header' => HTMLTemplateDeliverySlip::l('DELIVERY ORDER')));
        return $this->smarty->fetch($this->getTemplate('consignment.header'));
    }
     
    public function getFooter()
    {
        return $this->smarty->fetch($this->getTemplate('consignment.footer'));
    }

    /**
     * Returns the template's HTML content
     *
     * @return string HTML content
     */
    public function getContent()
    {
        $delivery_address = null;
        $addresses = array();
       
        foreach($this->orderids as $id_order) 
        {
            if($id_order > 0) {
                $order = new Order((int)$id_order);
        
                if (isset($order->id_address_delivery) && $order->id_address_delivery) {
                    $t['id_order'] = $order->id;
                    $t['reference'] = $order->reference;
                    $t['addr'] = new Address((int)$order->id_address_delivery);
                    $t['state'] = new State((int)$t['addr']->id_state);
                    $addresses[] = $t;
                }
            }
        }
       
        $this->smarty->assign(array(
            'addresses' => $addresses,
        ));

        return $this->smarty->fetch($this->getTemplate('consignment'));
    }


    /**
     * Returns the template filename when using bulk rendering
     *
     * @return string filename
     */
    public function getBulkFilename()
    {
        return 'consignments.pdf';
    }

    /**
     * Returns the template filename
     *
     * @return string filename
     */
    public function getFilename()
    {
        if($this->order) {
            return "Consignment-".$this->order->reference.'.pdf';
        }
        else {
            return "Consignments.pdf";
        }
    }
}