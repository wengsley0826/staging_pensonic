<?php
class HTMLTemplateVendorGDEXCore extends HTMLTemplate
{
    public $invoiceTotal;
    public $salesTotal;
    public $discountTotal;
    public $shippingTotal;
    public $invoiceTotalExclGST;
    public $gstTotal;
    
	public function __construct($order, $smarty)
	{
		$this->smarty = $smarty;
		
		if (is_array($order)){
			$orderxx = $order[0];
			$this->gdexnumber = $order[1];
			$order = $orderxx;
		}
		
		$this->order = $order;
			
        // header informations
		$this->date = date('d-m-Y');

		$id_lang = Context::getContext()->language->id;
		$this->title = "";
		$this->gstNo = "";
        $this->isRefund = false;
        // footer informations
		$this->shop = new Shop(1);
	}

	/**
	 * Returns the template's HTML content
	 * @return string HTML content
	 */
	public function getContent()
	{
		
		//$invoice_address = $this->shop->getAddress();
		//$formatted_invoice_address = AddressFormat::generateAddress($invoice_address, array(), '<br />', ' ');
		//$customer = new Customer((int)$this->order->id_customer);
		
		$id_order = (int)Tools::getValue('id_order');
		$gdexnumber = Tools::getValue('gdexnumber');
		if ($this->gdexnumber){
			$gdexnumber=$this->gdexnumber['tracking_number'];
			$id_order=$this->order->id; 
		}
		
        $oldDateTo = date('d-m-Y');
        $invoiceDateTo = date('ymd');
        
        //$invoiceNo = Tools::getValue("PS_INVOICE_PREFIX").'-'.$invoiceDateTo;
        $invoiceNo = $this->order->reference;
     
		$data = array(
		);
        
		$country = new Country((int)$this->order->id_address_invoice);
		$invoice_address = new Address((int)$this->order->id_address_invoice);
		$formatted_invoice_address = AddressFormat::generateAddress($invoice_address, array(), '<br />', ' ');
		$formatted_delivery_address = '';

		if ($this->order->id_address_delivery != $this->order->id_address_invoice)
		{
			$delivery_address = new Address((int)$this->order->id_address_delivery);
			$formatted_delivery_address = AddressFormat::generateAddress($delivery_address, array(), '<br />', ' ');
		}
		
		if (!$formatted_delivery_address)
			$formatted_delivery_address=$formatted_invoice_address;
		
		$getTrackingSQL = '
			SELECT weight, height, width, length, pieces
			FROM `'._DB_PREFIX_.'order_shipping`
			WHERE id_order = "'.$id_order.'" AND tracking_number = "'.pSQL($gdexnumber).'"
			';
        $resultTracking = Db::getInstance()->executeS($getTrackingSQL);
        $rowTracking = $resultTracking[0];
		
		$getFlashCard = '
SELECT count(1) AS total_count, sum(IF(product_name LIKE \'%Flash Card (Alphabet%\',1,0)) AS total_flash FROM ps_order_detail 
WHERE  id_order = "'.$id_order.'"
			';
        $resultFlashCard = Db::getInstance()->executeS($getFlashCard);
        $rowFlashCard = $resultFlashCard[0];
		if ($rowFlashCard['total_flash']==$rowFlashCard['total_count']){
			$hasOnlyFlash=true;
		}else
			$hasOnlyFlash=false;

		$customer = new Customer((int)$this->order->id_customer);
		$data = array(
            'weight' => $rowTracking['weight'],
            'height' => $rowTracking['height'],
            'width' => $rowTracking['width'],
            'length' => $rowTracking['length'],
            'pieces' => $rowTracking['pieces'],
            'hasOnlyFlash' => $hasOnlyFlash,
			
            'gdexnumber' => $gdexnumber,
            'shop_address' => $this->getShopAddress(),
            'shop_name' => $this->gstCompany,
			//'sellerinfo' => $this->seller,
            'invoiceTotal' => $this->invoiceTotal,
            'salesTotal' => $this->salesTotal,
            'discountTotal' => $this->discountTotal,
            'shippingTotal' => $this->shippingTotal,
            'invoiceTotalExclGST' => $this->invoiceTotalExclGST,
            'gstTotal' => $this->gstTotal,
            'date_from' => $oldDateFrom,
            'date_to' => $oldDateTo,
            'invoiceNo' => $invoiceNo,
            'generateDate' => date("d-m-Y"),
			'order' => $this->order,
			'delivery_address' => $formatted_delivery_address,
			'invoice_address' => $formatted_invoice_address,
			'customer' => $customer
		);

		if (Tools::getValue('debug'))
			die(json_encode($data));

		$this->smarty->assign($data);
		
		

		return $this->smarty->fetch($this->getTemplateByCountry(null));
	}

	/**
	 * Returns the invoice template associated to the country iso_code
	 * @param string $iso_country
	 */
	protected function getTemplateByCountry($iso_country)
	{
		$file = "vendorGDEXInvoice";

		//// try to fetch the iso template
		//$template = $this->getTemplate($file.'.'.$iso_country);

		//// else use the default one
		//if (!$template)
			$template = $this->getTemplate($file);

		return $template;
	}

	/**
	 * Returns the template filename
	 * @return string filename
	 */
	public function getFilename()
	{
		return 'Pensonic-GDEX.pdf';
	}
    
    public function getBulkFilename()
	{
		return 'vendorInvoices.pdf';
	}
    
    protected function getShopAddress()
	{
		$shop_address = '';
		if (Validate::isLoadedObject($this->shop))
		{
			$shop_address_obj = $this->shop->getAddress();
            $shop_address_obj->company = "";
            $this->gstCompany = "ENLINEA SDN BHD";
			if (isset($shop_address_obj) && $shop_address_obj instanceof Address)
				$shop_address = AddressFormat::generateAddress($shop_address_obj, array(), '<br />', ' ');
			return $shop_address;
		}

		return $shop_address;
	}
    
    public function getHeader()
	{
		return $this->smarty->fetch($this->getTemplate('vendorGDEXheader'));

	}
    
    public function getLFooter()
	{
    	return $this->smarty->fetch($this->getTemplate('vendorlGDEXfooter'));
	}
    
    public function getLFooterMargin() 
    {
        return 35;
    }
    
    public function getFooter()
	{
    	return $this->smarty->fetch($this->getTemplate('vendorGDEXfooter'));
	}
    
}

