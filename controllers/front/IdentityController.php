<?php

class IdentityControllerCore extends FrontController
{
    public $auth = true;
    public $php_self = 'identity';
    public $authRedirection = 'identity';
    public $ssl = true;

    /** @var Customer */
    protected $customer;

    public function init()
    {
        parent::init();
        $this->customer = $this->context->customer;
    }

    /**
     * Start forms process
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        $origin_newsletter = (bool)$this->customer->newsletter;

        if (Tools::isSubmit('submitIdentity')) {
            //$email = trim(Tools::getValue('email'));

            //if (Tools::getValue('months') != '' && Tools::getValue('days') != '' && Tools::getValue('years') != '') {
            //    $this->customer->birthday = (int)Tools::getValue('years').'-'.(int)Tools::getValue('months').'-'.(int)Tools::getValue('days');
            //} elseif (Tools::getValue('months') == '' && Tools::getValue('days') == '' && Tools::getValue('years') == '') {
            //    $this->customer->birthday = null;
            //} else {
            //    $this->errors[] = Tools::displayError('Invalid date of birth.');
            //}

            //if (Tools::getIsset('old_passwd')) {
            //    $old_passwd = trim(Tools::getValue('old_passwd'));
            //}

            //if (!Validate::isEmail($email)) {
            //    $this->errors[] = Tools::displayError('This email address is not valid');
            //} elseif ($this->customer->email != $email && Customer::customerExists($email, true)) {
            //    $this->errors[] = Tools::displayError('An account using this email address has already been registered.');
            //} 
            //elseif (!Tools::getIsset('old_passwd') || (Tools::encrypt($old_passwd) != $this->context->cookie->passwd)) {
            //    $this->errors[] = Tools::displayError('The password you entered is incorrect.');
            //} else
            if (Tools::getValue('passwd') != Tools::getValue('confirmation')) {
                $this->errors[] = Tools::displayError('The password and confirmation do not match.');
            }
            //else {
                $prev_id_default_group = $this->customer->id_default_group;
                $_POST['email'] = $this->customer->email; 
                // Merge all errors of this file and of the Object Model
                $this->errors = array_merge($this->errors, $this->customer->validateController());
            //}

            if (!count($this->errors)) {
                $this->customer->id_default_group = (int)$prev_id_default_group;
                $this->customer->firstname = Tools::ucwords($this->customer->firstname);

                if (Configuration::get('PS_B2B_ENABLE')) {
                    $this->customer->website = Tools::getValue('website'); // force update of website, even if box is empty, this allows user to remove the website
                    $this->customer->company = Tools::getValue('company');
                }

                if (!Tools::getIsset('newsletter')) {
                    $this->customer->newsletter = 0;
                    $this->customer->allowSMS = false;
                    $this->customer->allowEmail = false; 
                    $this->customer->allowCall = false; 
                    $this->customer->allowMail = false; 
                } elseif (!$origin_newsletter && Tools::getIsset('newsletter')) {
                    if ($module_newsletter = Module::getInstanceByName('blocknewsletter')) {
                        /** @var Blocknewsletter $module_newsletter */
                        if ($module_newsletter->active) {
                            $module_newsletter->confirmSubscription($this->customer->email);
                        }
                    }
                }

                if($this->customer->newsletter == 1) {
                    if (Tools::getIsset('allowSMS')) { $this->customer->allowSMS = true; }
                    else { $this->customer->allowSMS = false; }
                    if (Tools::getIsset('allowEmail')) { $this->customer->allowEmail = true; }
                    else { $this->customer->allowEmail = false; }
                    if (Tools::getIsset('allowCall')) { $this->customer->allowCall = true; }
                    else { $this->customer->allowCall = false; }
                    if (Tools::getIsset('allowMail')) { $this->customer->allowMail = true; }
                    else { $this->customer->allowMail = false; }
                }

                if (!Tools::getIsset('optin')) {
                    $this->customer->optin = 0;
                }
                if (Tools::getValue('passwd')) {
                    $this->context->cookie->passwd = $this->customer->passwd;
                }

                $birthyear = Tools::getValue('years');
                $birthmonth = Tools::getValue('months');
                $birthday = Tools::getValue('days');

                $crmbirthyear = $birthyear;
                $crmbirthmonth = $birthmonth;
                $crmbirthday = $birthday;

                if($birthyear == "") {
                    $birthyear = "0000";
                    $crmbirthyear = "1900";
                }

                if($birthmonth == "") {
                    $birthmonth = "00";
                    $crmbirthmonth = "01";
                }

                if($birthday == "") {
                    $birthday = "00";
                    $crmbirthday = "01";
                }

                $this->customer->birthday = $birthyear.'-'.$birthmonth.'-'.$birthday;

                //// update CRM
                $token = Tools::GetCRMAPIToken();

                if($token) {
                    $crmId = $this->customer->crm_id;
                    $updateuser = array();
                    
                    $updateuser["Id"] = $crmId;
                    $updateuser["Email"] = $this->customer->email;
                        
                    $updateuser["Name"] = $this->customer->firstname;
                    $updateuser["Phone"] = $this->customer->mobile;
                    $updateuser["DOB"] = $crmbirthyear."-".$crmbirthmonth."-".$crmbirthday;
                    if (Tools::getValue('passwd')) {
                        $updateuser["Password"] = Tools::getValue('passwd');
                    }
                        //$updateuser["CustomerType"] = $crmuser["data"]["CustomerType"];
                        //$updateuser["Address1"] = $crmuser["data"]["Address1"];
                        //$updateuser["Address2"] = $crmuser["data"]["Address2"];
                        //$updateuser["Postcode"] = $crmuser["data"]["Postcode"];
                        //$updateuser["City"] = $crmuser["data"]["City"];
                        //$updateuser["Country"] = $crmuser["data"]["Country"];

                        //$updateuser["Phone2"] = $crmuser["data"]["Phone2"];
                        //$updateuser["Race"] = $crmuser["data"]["Race"];
                        //$updateuser["State"] = $crmuser["data"]["State"];
                        //$updateuser["Newsletter"] = $crmuser["data"]["Newsletter"];
                        //$updateuser["ContactNo"] = $crmuser["data"]["ContactNo"];
                        //$updateuser["AllowLogin"] = $crmuser["data"]["AllowLogin"];

                        //$updateuser["Status"] = $crmuser["data"]["Status"];
                        ////$updateuser["NavisionId"] = $crmuser["data"]["NavisionId"];
                        //$updateuser["EmailConfirmation"] = $crmuser["data"]["EmailConfirmation"];
                        //$updateuser["FBId"] = $crmuser["data"]["FBId"];
                        //$updateuser["ForcePasswordChange"] = $crmuser["data"]["ForcePasswordChange"];
                        //$updateuser["OneSignalToken"] = $crmuser["data"]["OneSignalToken"];
                        ////$updateuser["PDPA"] = $crmuser["data"]["PDPA"];
     
                    $saveresult = Tools::UpdateCRMUser($updateuser, $token);
                  
                    if($saveresult["success"]) {  
                        if ($this->customer->update()) {
                            $this->context->cookie->customer_lastname = $this->customer->lastname;
                            $this->context->cookie->customer_firstname = $this->customer->firstname;
                            $this->context->smarty->assign('confirmation', 1);
                        } else {
                            $this->errors[] = Tools::displayError('The information cannot be updated.');
                        }
                    }
                }
            }
        } else {
            $_POST = array_map('stripslashes', $this->customer->getFields());
        }

        return $this->customer;
    }
    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        if ($this->customer->birthday) {
            $birthday = explode('-', $this->customer->birthday);
        } else {
            $birthday = array('-', '-', '-');
        }

        /* Generate years, months and days */
        $this->context->smarty->assign(array(
                'years' => Tools::dateYears(),
                'sl_year' => $birthday[0],
                'months' => Tools::dateMonths(),
                'sl_month' => $birthday[1],
                'days' => Tools::dateDays(),
                'sl_day' => $birthday[2],
                'errors' => $this->errors,
                'genders' => Gender::getGenders(),
            ));

        // Call a hook to display more information
        $this->context->smarty->assign(array(
            'HOOK_CUSTOMER_IDENTITY_FORM' => Hook::exec('displayCustomerIdentityForm'),
        ));

        $newsletter = Configuration::get('PS_CUSTOMER_NWSL') || (Module::isInstalled('blocknewsletter') && Module::getInstanceByName('blocknewsletter')->active);
        $this->context->smarty->assign('newsletter', $newsletter);
        $this->context->smarty->assign('optin', (bool)Configuration::get('PS_CUSTOMER_OPTIN'));

        $this->context->smarty->assign('field_required', $this->context->customer->validateFieldsRequiredDatabase());

        $this->setTemplate(_PS_THEME_DIR_.'identity.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_THEME_CSS_DIR_.'identity.css');
        $this->addJS(_PS_JS_DIR_.'validate.js');
    }
}
