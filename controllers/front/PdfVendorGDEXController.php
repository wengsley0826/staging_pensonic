<?php

class PdfVendorGDEXControllerCore extends FrontController
{
	public $php_self = 'pdf-vendor-gdex';
	protected $display_header = false;
	protected $display_footer = false;

    public $content_only = true;

	protected $template;
	public $filename;
    
	public function postProcess()
	{
    print_r("TEST");
    die();
		if (!$this->context->customer->isLogged() && !Tools::getValue('secure_key'))
			Tools::redirect('index.php?controller=authentication&back=pdf-vendor-gdex');

		$id_order = (int)Tools::getValue('id_order');
		
		if (!Tools::getValue('id_orders')){
			if (Validate::isUnsignedId($id_order))
				$order = new Order((int)$id_order);

			if (!isset($order) || !Validate::isLoadedObject($order))
				die(Tools::displayError('The invoice was not found.'));

			if (!OrderState::invoiceAvailable($order->getCurrentState()) && !$order->invoice_number)
				die(Tools::displayError('No invoice is available.'));

			$this->order = $order;
		}else{
			$allOrders=Tools::getValue('id_orders');
			$allOrderArr=explode(",",$allOrders);
			
			if (!(int)Configuration::get("PS_INVOICE")) die(Tools::displayError("Invoices are disabled in this shop."));
			foreach($allOrderArr as $oneOrderId){
				$oneOrder=new Order($oneOrderId);
					
				if (!isset($oneOrder) || !Validate::isLoadedObject($oneOrder)) die(Tools::displayError("Invoice not found"));
					
				$id_owner = AgileSellerManager::getObjectOwnerID("order", $oneOrderId);
				$linked_owner = AgileSellerManager::getLinkedSellerID($this->context->customer->id);
				if ($id_owner != $linked_owner) die(Tools::displayError("You do not have permission to see this invoice"));
				if (!OrderState::invoiceAvailable($oneOrder->getCurrentState()) && !$oneOrder->invoice_number) die(Tools::displayError("No invoice available"));
			}
			
			$this->orders = $allOrderArr;
			
		}

	}

	public function display()
	{	
		if ($this->orders){
			
			$finalOrderArr=array();
			foreach($this->orders as $oneOrder){
				$getTrackingSQL = '
					SELECT tracking_number
					FROM `'._DB_PREFIX_.'order_shipping`
					WHERE id_order = "'.$oneOrder.'" 
					';
				$oneOrderObj=new Order($oneOrder);
				$resultTracking = Db::getInstance()->executeS($getTrackingSQL);
				foreach($resultTracking as $oneTracking){
					$finalOrderArr[]=array($oneOrderObj,$oneTracking);
				}
			}
			$obj = new ArrayObject( $finalOrderArr );
			$it = $obj->getIterator();
			
			$pdf = new PDF($it, PDF::TEMPLATE_VENDOR_GDEX, Context::getContext()->smarty);
			$pdf->render();
			//die('zzz');
			
		}else{
			$pdf = new PDF($this->order, PDF::TEMPLATE_VENDOR_GDEX, Context::getContext()->smarty);
			$pdf->render();
		}
//		die('mmmf');
	}

}
