<?php
    print_r("start<br />");
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    require_once(dirname(__FILE__)."/../config/settings.inc.php");
    
    //get category data
    $conn = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_, _DB_NAME_);
    $sql =  "select cat.id_category, cat.id_parent, cat.level_depth, catlang.name ".
            "from "._DB_PREFIX_."category cat ".
            "inner join "._DB_PREFIX_."category_lang catlang on cat.id_category = catlang.id_category ".
            "where level_depth >=1 and catlang.id_lang = 1 ".
            "order by cat.level_depth;";       
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        $categoryList[] = $row;
    }
    
    $sql = "INSERT INTO "._DB_PREFIX_."category_parent(id_category, fullPath)VALUES";
    foreach($categoryList as &$category) {
        if($category["level_depth"] == 1) {
            $category["fullPath"] = str_replace("'", "\'",$category["name"]);
            $sql .= "(".$category["id_category"].",'".$category["fullPath"]."')";
        }
        else {
            $parentPath = getParentPath($categoryList, "id_category", $category["id_parent"]);
            $category["fullPath"] = str_replace("'", "\'", $parentPath." > ".$category["name"]);        
            $sql .= ",(".$category["id_category"].",'".$category["fullPath"]."')";
        }
    }
    
    $sql.="ON DUPLICATE KEY UPDATE fullPath = VALUES(fullPath)";
   
    if($conn->query($sql) === TRUE) {
        print_r("build categories successful.<br />");
    }
    else {
        print_r("build categories failed.<br />");
    }
   
    $result->close();
        
    $manufacturerList = [];
    $sql2 =  "select m.id_manufacturer ".
            "from "._DB_PREFIX_."manufacturer m ".
            "where active=1";       
    $result2 = $conn->query($sql2);
    while ($row2 = $result2->fetch_assoc()) {
        $manufacturerList[] = $row2;
    }
        
    foreach($manufacturerList as &$m) {
        $hasLogo = 0;
        $manufacturerId = $m['id_manufacturer'];
        
        if(file_exists(dirname(__FILE__)."/../img/m/".$manufacturerId."-large_default.jpg")) {
            $hasLogo = 1;
        }   
        
        $sql2 = "UPDATE "._DB_PREFIX_."manufacturer SET hasLogo=".$hasLogo." WHERE id_manufacturer=". $m['id_manufacturer'];
        $conn->query($sql2);
    }
    
    $result2->close();
    mysqli_close($conn);
    print_r("end<br />");
    
    function getParentPath($array, $key, $value) {
        $result = "";

        if (is_array($array)) {
            foreach ($array as $subarray) {
                if (isset($subarray[$key]) && $subarray[$key] == $value && isset($subarray["fullPath"])) {
                    $result = $subarray["fullPath"];
                    break;
                }
            }
        }
        
        return $result;
    }
?>