<?php
if (!defined('_PS_VERSION_'))
	exit;

class BlockUserInfo extends Module
{
	public function __construct()
	{
		$this->name = 'blockuserinfo';
		$this->tab = 'front_office_features';
		$this->version = '0.4.1';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;
        $this->is_eu_compatible = 1;
        $this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('User info block');
		$this->description = $this->l('Adds a block that displays information about the customer.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.99.99');
	}

	public function install()
	{
		return (parent::install() && $this->registerHook('displayTop') && $this->registerHook('displayNav') && $this->registerHook('displayHeader'));
	}

	/**
	* Returns module content for header
	*
	* @param array $params Parameters
	* @return string Content
	*/
	public function hookDisplayTop($params)
	{
		if (!$this->active)
			return;

		$this->smarty->assign(array(
			'cart' => $this->context->cart,
			'cart_qties' => $this->context->cart->nbProducts(),
			'logged' => $this->context->customer->isLogged(),
			'customerName' => ($this->context->customer->logged ? $this->context->customer->firstname.' '.$this->context->customer->lastname : false),
			'firstName' => ($this->context->customer->logged ? $this->context->customer->firstname : false),
			'lastName' => ($this->context->customer->logged ? $this->context->customer->lastname : false),
			'order_process' => Configuration::get('PS_ORDER_PROCESS_TYPE') ? 'order-opc' : 'order'
		));
		return $this->display(__FILE__, 'blockuserinfo.tpl');
	}

	public function hookDisplayHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'blockuserinfo.css', 'all');
	}

	public function hookDisplayNav($params)
	{
		return $this->display(__FILE__, 'nav.tpl');
	}
    
    protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('AUTH_CAPTCHA_URL')) {
                $this->_postErrors[] = $this->l('Captcha URL is required!');
            }
            if (!Tools::getValue('AUTH_CAPTCHA_KEY')) {
                $this->_postErrors[] = $this->l('Captcha key is required!');
            }
            if (!Tools::getValue('AUTH_TNC_ID')) {
                $this->_postErrors[] = $this->l('User of Website Terms is required!');
            }
            if (!Tools::getValue('AUTH_PRIVACY_ID')) {
                $this->_postErrors[] = $this->l('Privacy Policy is required!');
            }
        }
	}

    protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('AUTH_CAPTCHA_URL', Tools::getValue('AUTH_CAPTCHA_URL'));
            Configuration::updateValue('AUTH_CAPTCHA_KEY', Tools::getValue('AUTH_CAPTCHA_KEY'));
            Configuration::updateValue('AUTH_TNC_ID', Tools::getValue('AUTH_TNC_ID'));
            Configuration::updateValue('AUTH_PRIVACY_ID', Tools::getValue('AUTH_PRIVACY_ID'));
        }
        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}
    
    public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
	}
    
    public function renderForm()
	{
        $cmsList = $this->getCMSList();

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Authentication Details'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Captcha URL'),
                        'name' => 'AUTH_CAPTCHA_URL',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Captcha Key'),
                        'name' => 'AUTH_CAPTCHA_KEY',
                        'required' => true
                    ),
                    array(
                        'type' => 'select',
					    'label' => $this->l('User of Website Terms Content:'),
					    'name' => 'AUTH_TNC_ID',
                        'required' => true,
					    'options' => array(
						    'query' => $cmsList,
						    'id' => 'id_cms',
						    'name' => 'name',
						    'default' => array(
							    'label' => $this->l('No Content'),
							    'value' => 0
						    )
					    )
                    ),
                    array(
                        'type' => 'select',
					    'label' => $this->l('Privacy Policy Content:'),
					    'name' => 'AUTH_PRIVACY_ID',
                        'required' => true,
					    'options' => array(
						    'query' => $cmsList,
						    'id' => 'id_cms',
						    'name' => 'name',
						    'default' => array(
							    'label' => $this->l('No Content'),
							    'value' => 0
						    )
					    )
                    ),
                
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            )
        );
        
        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->id                       = (int) Tools::getValue('id_carrier');
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm(array(
            $fields_form
        ));
	}
    
    protected function getCMSList() {
        $allcms = Db::getInstance()->ExecuteS('SELECT DISTINCT A.id_cms, A.meta_title AS page_title'.
                    ' FROM '._DB_PREFIX_.'cms_lang A'.
                    ' INNER JOIN '._DB_PREFIX_.'cms B ON A.id_cms=B.id_cms '.
                    ' WHERE B.active=1 AND A.id_lang=1'.
                    ' ORDER BY A.meta_title'
		);
        $cms_list = array();
   
        foreach ($allcms as $cms)
        {
            $cms_list[] = array(
                "id_cms" => $cms["id_cms"],
                "name" => $cms["page_title"]
            );
        }
        
        return $cms_list;
    }
    
    public function getConfigFieldsValues()
	{
		return array(
			'AUTH_CAPTCHA_URL' => Tools::getValue('AUTH_CAPTCHA_URL', Configuration::get('AUTH_CAPTCHA_URL')),
            'AUTH_CAPTCHA_KEY' => Tools::getValue('AUTH_CAPTCHA_KEY', Configuration::get('AUTH_CAPTCHA_KEY')),
            'AUTH_TNC_ID' => Tools::getValue('AUTH_TNC_ID', Configuration::get('AUTH_TNC_ID')),
            'AUTH_PRIVACY_ID' => Tools::getValue('AUTH_PRIVACY_ID', Configuration::get('AUTH_PRIVACY_ID'))
		);
	}	
}
