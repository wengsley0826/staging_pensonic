<?php

class blockuserinfoResetPasswordModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        parent::init();
    }
    
    public function initContent()
    {
        parent::initContent();
        
        $email = Tools::getValue('cust_mail');
        $this->context->smarty->assign(array('cust_email' => $email));
        
        $this->setTemplate('resetpassword.tpl');
    }
    
    public function postProcess()
    {   
        if (Tools::isSubmit('submitResetPassword'))
		{
            $email = Tools::getValue('email');
            $old_password = Tools::getValue('old_passwd');
            $passwd = Tools::getValue('passwd');
            $confirmpassword = Tools::getValue('confirmpassword');
            
            if (empty($email)) {
                $this->errors[] = Tools::displayError('An email address required.');
            } elseif (!Validate::isEmail($email)) {
                $this->errors[] = Tools::displayError('Invalid email address.');
            } elseif (empty($old_password)) {
                $this->errors[] = Tools::displayError('<b>Old Password</b> is required.', false);
            } elseif (empty($passwd)) {
                $this->errors[] = Tools::displayError('<b>New Password</b> is required.', false);
            } elseif (!Validate::isPasswd($passwd)) {
                $this->errors[] = Tools::displayError('Invalid <b>New password</b>.', false);
            } elseif (empty($confirmpassword)) {
                $this->errors[] = Tools::displayError('<b>Confirm Password</b> is required.', false);
            } elseif ($confirmpassword != $passwd) {
                $this->errors[] = Tools::displayError('<b>New Password</b> and <b>Confirm Password</b> not match.', false);
            } else {
                $customer = new Customer();
                $authentication = $customer->getByEmail(trim($email), trim($old_password));
            
                if (isset($authentication->active) && !$authentication->active) {
                    $this->errors[] = Tools::displayError('Your account isn\'t available at this time, please contact us');
                } elseif (!$authentication || !$authentication->id) {
                    $this->errors[] = Tools::displayError('Authentication failed.');
                }
                else {
                    $authentication->passwd = Tools::encrypt($passwd);
                    $authentication->force_passwd_reset = 0;
                    
                    if ($authentication->update()) {
                        $this->context->smarty->assign(array('confirmation' => 1, 'customer_email' => $authentication->email));
                    }
                }
            }
        }
        
        return parent::postProcess();
    }
}