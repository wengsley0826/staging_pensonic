{capture name=path}<a href="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" title="{l s='Authentication'}" rel="nofollow">{l s='Authentication'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Update password'}{/capture}
<div class="box">
<h1 class="page-subheading">{l s='Update password'}</h1>

{include file="$tpl_dir./errors.tpl"}
{if isset($confirmation) && $confirmation == 1}
	<p class="alert alert-success">{l s='Your password has been successfully update. Please login with your new password'}</p>
{else}
	<p>{l s='Your account password was reset recently. Please update your password. '}</p>
	<form action="{$link->getModuleLink('blockuserinfo', 'resetpassword')|escape:'html':'UTF-8'}" id="form_forgotpassword" class="std" method="post">
		<fieldset>
			<div class="required form-group" >
				<label for="email" class="required">{l s='Email address'}</label>
				<input class="form-control" type="email" id="email" name="email" readonly="readonly" value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'html':'UTF-8'|stripslashes}{elseif isset($cust_email)}{$cust_email|escape:'html':'UTF-8'|stripslashes}{/if}" />
			</div>
			<div class="required form-group">
				<label for="old_passwd" class="required">
					{l s='Current Password'}
				</label>
				<input class="is_required validate form-control" type="password" data-validate="isPasswd" name="old_passwd" id="old_passwd" />
			</div>
			<div class="required password form-group">
				<label for="passwd" class="required">
					{l s='New Password'}
				</label>
				<input class="is_required validate form-control" type="password" data-validate="isPasswd" name="passwd" id="passwd" />
			</div>
			<div class="required password form-group">
				<label for="confirmation" class="required">
					{l s='Confirmation'}
				</label>
				<input class="is_required validate form-control" type="password" data-validate="isPasswd" name="confirmpassword" id="confirmpassword" />
			</div>
			<p class="submit">
				<input type="hidden" name="submitResetPassword" value="1">
				<button type="submit" class="btn btn-default button button-medium"><span>{l s='Update Password'}<i class="icon-chevron-right right"></i></span></button>
			</p>
		</fieldset>
	</form>
{/if}
</div>
<ul class="clearfix footer_links">
	<li><a class="btn btn-default button button-small" href="{$link->getPageLink('authentication')|escape:'html':'UTF-8'}" title="{l s='Back to Login'}" rel="nofollow"><span><i class="icon-chevron-left"></i>{l s='Back to Login'}</span></a></li>
</ul>
