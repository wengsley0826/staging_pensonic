<?php

class BrandsMix extends Module
{
    protected $_html = '';
    protected $_postErrors = array();

    public function __construct()
    {
        if (!defined('_PS_VERSION_')) {
            exit;
        }
        $this->name = 'brandsmix';
        $this->tab = 'Brands_Mix';
        $this->version = '1.0.0';
        $this->author = 'Claritas';
        $this->displayName = 'Claritas Integration settings';
        $this->bootstrap = true;
        $this->display = 'view';
        $this->is_eu_compatible = 1;
        
        parent::__construct();

        $this->description = $this->l('Claritas integration settings');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

    }

    public function install()
    {
        return parent::install();
    }

    public function uninstall()
    {
        if (!Configuration::deleteByName('BM_KL_HOST') 
            || !Configuration::deleteByName('BM_KL_USER') 
            || !Configuration::deleteByName('BM_KL_PASS')
            || !Configuration::deleteByName('BM_KL_DIR_DSO')
            || !Configuration::deleteByName('BM_KL_DIR_INVOICE')
            || !Configuration::deleteByName('BM_KL_DIR_SOC')
            || !Configuration::deleteByName('BM_KL_STATUS')
            || !Configuration::deleteByName('BM_KL_TO_STATUS')
            || !Configuration::deleteByName('BM_KL_SHIP_STATUS')
            || !Configuration::deleteByName('BM_KL_OOS_STATUS')
            || !Configuration::deleteByName('BM_KL_OOS_REVERSE')
            
            

            || !Configuration::deleteByName('BM_JANIO_URL')
            || !Configuration::deleteByName('BM_JANIO_SECRET')
            || !Configuration::deleteByName('BM_JANIO_TRACKING_PREFIX')
            
            || !Configuration::deleteByName('BM_JANIO_PICKUP_CONTACT_NAME')
            || !Configuration::deleteByName('BM_JANIO_PICKUP_CONTACT_NUMBER')
            || !Configuration::deleteByName('BM_JANIO_PICKUP_COUNTRY')
            || !Configuration::deleteByName('BM_JANIO_PICKUP_POSTAL')
            || !Configuration::deleteByName('BM_JANIO_PICKUP_ADDRESS')
            || !Configuration::deleteByName('BM_JANIO_PICKUP_STATE')
            
            || !parent::uninstall())
            return false;
        
        return true;
    }

    protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('BM_KL_HOST')) {
                $this->_postErrors[] = $this->l('Kerry Logistic FTP Hostname is required!');
            }
            if (!Tools::getValue('BM_KL_USER')) {
                $this->_postErrors[] = $this->l('Kerry Logistic FTP Username is required!');
            }
            if (!Tools::getValue('BM_KL_PASS')) {
                $this->_postErrors[] = $this->l('Kerry Logistic FTP Password is required!');
            }
            if (!Tools::getValue('BM_KL_DIR_DSO')) {
                $this->_postErrors[] = $this->l('Kerry Logistic FTP Directory (DSO) is required!');
            }
            if (!Tools::getValue('BM_KL_DIR_INVOICE')) {
                $this->_postErrors[] = $this->l('Kerry Logistic FTP Directory (Invoice) is required!');
            }
            if (!Tools::getValue('BM_KL_DIR_SOC')) {
                $this->_postErrors[] = $this->l('Kerry Logistic FTP Directory (SOC) is required!');
            }
            if (!Tools::getValue('BM_JANIO_TRACKING_PREFIX')) {
                $this->_postErrors[] = $this->l('Janio Tracking Number Prefix is required!');
            }
            if (!Tools::getValue('BM_KL_STATUS')) {
                $this->_postErrors[] = $this->l('Kerry Logistic status to trigger is required!');
            }
            if (!Tools::getValue('BM_KL_TO_STATUS')) {
                $this->_postErrors[] = $this->l('Kerry Logistic status after triggered is required!');
            }
            if (!Tools::getValue('BM_KL_SHIP_STATUS')) {
                $this->_postErrors[] = $this->l('Kerry Logistic shipped status is required!');
            }
            if (!Tools::getValue('BM_KL_OOS_STATUS')) {
                $this->_postErrors[] = $this->l('Kerry Logistic out of stock status is required!');
            }

            if (!Tools::getValue('BM_JANIO_URL')) {
                $this->_postErrors[] = $this->l('Janio API URL is required!');
            }
            if (!Tools::getValue('BM_JANIO_SECRET')) {
                $this->_postErrors[] = $this->l('Janio API Secret Key is required!');
            }

            if (!Tools::getValue('BM_JANIO_PICKUP_CONTACT_NAME')) {
                $this->_postErrors[] = $this->l('Janio Pickup Contact Name is required!');
            }
            if (!Tools::getValue('BM_JANIO_PICKUP_CONTACT_NUMBER')) {
                $this->_postErrors[] = $this->l('Janio Pickup Contact No. is required!');
            }
            if (!Tools::getValue('BM_JANIO_PICKUP_COUNTRY')) {
                $this->_postErrors[] = $this->l('Janio Pickup Country is required!');
            }
            if (!Tools::getValue('BM_JANIO_PICKUP_POSTAL')) {
                $this->_postErrors[] = $this->l('Janio Pickup Postal Code is required!');
            }
            if (!Tools::getValue('BM_JANIO_PICKUP_ADDRESS')) {
                $this->_postErrors[] = $this->l('Janio Pickup Address is required!');
            }
            if (!Tools::getValue('BM_JANIO_PICKUP_STATE')) {
                $this->_postErrors[] = $this->l('Janio Pickup State is required!');
            }            
        }
	}
    
    protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('BM_KL_HOST', Tools::getValue('BM_KL_HOST'));
            Configuration::updateValue('BM_KL_USER', Tools::getValue('BM_KL_USER'));
            Configuration::updateValue('BM_KL_PASS', Tools::getValue('BM_KL_PASS'));
            Configuration::updateValue('BM_KL_DIR_DSO', Tools::getValue('BM_KL_DIR_DSO'));
            Configuration::updateValue('BM_KL_DIR_INVOICE', Tools::getValue('BM_KL_DIR_INVOICE'));
            Configuration::updateValue('BM_KL_DIR_SOC', Tools::getValue('BM_KL_DIR_SOC'));
            Configuration::updateValue('BM_JANIO_TRACKING_PREFIX', Tools::getValue('BM_JANIO_TRACKING_PREFIX'));

            Configuration::updateValue('BM_KL_STATUS', Tools::getValue('BM_KL_STATUS'));
            Configuration::updateValue('BM_KL_TO_STATUS', Tools::getValue('BM_KL_TO_STATUS'));
            Configuration::updateValue('BM_KL_SHIP_STATUS', Tools::getValue('BM_KL_SHIP_STATUS'));
            Configuration::updateValue('BM_KL_OOS_STATUS', Tools::getValue('BM_KL_OOS_STATUS'));
            Configuration::updateValue('BM_KL_OOS_REVERSE', Tools::getValue('BM_KL_OOS_REVERSE'));
            
            
            Configuration::updateValue('BM_JANIO_URL', Tools::getValue('BM_JANIO_URL'));
            Configuration::updateValue('BM_JANIO_SECRET', Tools::getValue('BM_JANIO_SECRET'));
            Configuration::updateValue('BM_JANIO_PICKUP_CONTACT_NAME', Tools::getValue('BM_JANIO_PICKUP_CONTACT_NAME'));
            Configuration::updateValue('BM_JANIO_PICKUP_CONTACT_NUMBER', Tools::getValue('BM_JANIO_PICKUP_CONTACT_NUMBER'));
            Configuration::updateValue('BM_JANIO_PICKUP_COUNTRY', Tools::getValue('BM_JANIO_PICKUP_COUNTRY'));
            Configuration::updateValue('BM_JANIO_PICKUP_POSTAL', Tools::getValue('BM_JANIO_PICKUP_POSTAL'));
            Configuration::updateValue('BM_JANIO_PICKUP_ADDRESS', Tools::getValue('BM_JANIO_PICKUP_ADDRESS'));
            Configuration::updateValue('BM_JANIO_PICKUP_STATE', Tools::getValue('BM_JANIO_PICKUP_STATE'));
        }
        
        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}
    
    public function getContent()
    {
       if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
    }

    public function renderForm($message = null)
    {
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Kerry Logistic'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('FTP Hostname'),
                    'name' => 'BM_KL_HOST',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('FTP Username'),
                    'name' => 'BM_KL_USER',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('FTP Password'),
                    'name' => 'BM_KL_PASS',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('FTP Directory (DSO)'),
                    'name' => 'BM_KL_DIR_DSO',
                    'required' => true,
                    'desc' => 'Flat file path for eStore to Kerry Logistic'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('FTP Directory (Invoice)'),
                    'name' => 'BM_KL_DIR_INVOICE',
                    'required' => true,
                    'desc' => 'Invoice path for eStore to Kerry Logistic'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('FTP Directory (SOC)'),
                    'name' => 'BM_KL_DIR_SOC',
                    'required' => true,
                    'desc' => 'Flat file path for Kerry Logistic to eStore'
                ),
                array(
                    'type' => 'select',
					'label' => $this->l('Trigger on status:'),
					'name' => 'BM_KL_STATUS',
                    'required' => true,
					'options' => array(
						'query' => $this->getOrderStatuses(),
						'id' => 'id_order_state',
						'name' => 'name',
                        //'default' => array(
                        //    'label' => $this->l('No Status'),
                        //    'value' => 0
                        //)
					)
                ),
                array(
                    'type' => 'select',
					'label' => $this->l('Status after send to KL:'),
					'name' => 'BM_KL_TO_STATUS',
                    'required' => true,
					'options' => array(
						'query' => $this->getOrderStatuses(),
						'id' => 'id_order_state',
						'name' => 'name',
                        //'default' => array(
                        //    'label' => $this->l('No Status'),
                        //    'value' => 0
                        //)
					)
                ),
                array(
                    'type' => 'select',
					'label' => $this->l('Status after KL shipped:'),
					'name' => 'BM_KL_SHIP_STATUS',
                    'required' => true,
					'options' => array(
						'query' => $this->getOrderStatuses(),
						'id' => 'id_order_state',
						'name' => 'name',
                        //'default' => array(
                        //    'label' => $this->l('No Status'),
                        //    'value' => 0
                        //)
					)
                ),
                array(
                    'type' => 'select',
					'label' => $this->l('Status on canceled:'),
					'name' => 'BM_KL_OOS_STATUS',
                    'required' => true,
					'options' => array(
						'query' => $this->getOrderStatuses(),
						'id' => 'id_order_state',
						'name' => 'name',
                        //'default' => array(
                        //    'label' => $this->l('No Status'),
                        //    'value' => 0
                        //)
					)
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Reverse Payment Authorization on canceled:'),
                    'name' => 'BM_KL_OOS_REVERSE',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),                    
            ),
            //'submit' => array(
            //    'title' => $this->l('Save')
            //)
        );
        
        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Janio'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('API URL'),
                    'name' => 'BM_JANIO_URL',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('API Secret Key'),
                    'name' => 'BM_JANIO_SECRET',
                    'required' => true
                ),
                 array(
                    'type' => 'text',
                    'label' => $this->l('Tracking No. Prefix'),
                    'name' => 'BM_JANIO_TRACKING_PREFIX',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Pickup Contact Name'),
                    'name' => 'BM_JANIO_PICKUP_CONTACT_NAME',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Pickup Contact No.'),
                    'name' => 'BM_JANIO_PICKUP_CONTACT_NUMBER',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Pickup Country'),
                    'name' => 'BM_JANIO_PICKUP_COUNTRY',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Pickup Postal Code'),
                    'name' => 'BM_JANIO_PICKUP_POSTAL',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Pickup Address'),
                    'name' => 'BM_JANIO_PICKUP_ADDRESS',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Pickup State'),
                    'name' => 'BM_JANIO_PICKUP_STATE',
                    'required' => true
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );

        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm($fields_form);
    }

    protected function getOrderStatuses() {
        $allstatuses = Db::getInstance()->ExecuteS('SELECT DISTINCT A.id_order_state, B.name AS status_name'.
                    ' FROM '._DB_PREFIX_.'order_state A'.
                    ' INNER JOIN '._DB_PREFIX_.'order_state_lang B ON A.id_order_state = B.id_order_state'.
                    ' WHERE B.id_lang = 1'.
                    ' ORDER BY B.name'
		);
        
        $status_list = array();
        
        foreach ($allstatuses as $status)
        {
            $status_list[] = array(
                "id_order_state" => $status["id_order_state"],
                "name" => $status["status_name"]
            );
        }
        
        return $status_list;
    }
    
    public function getConfigFieldsValues()
	{
    	return array(
			'BM_KL_HOST' => Tools::getValue('BM_KL_HOST', Configuration::get('BM_KL_HOST')),
            'BM_KL_USER' => Tools::getValue('BM_KL_USER', Configuration::get('BM_KL_USER')),
            'BM_KL_PASS' => Tools::getValue('BM_KL_PASS', Configuration::get('BM_KL_PASS')),
            'BM_KL_DIR_DSO' => Tools::getValue('BM_KL_DIR_DSO', Configuration::get('BM_KL_DIR_DSO')),
            'BM_KL_DIR_INVOICE' => Tools::getValue('BM_KL_DIR_INVOICE', Configuration::get('BM_KL_DIR_INVOICE')),
            'BM_KL_DIR_SOC' => Tools::getValue('BM_KL_DIR_SOC', Configuration::get('BM_KL_DIR_SOC')),
            'BM_JANIO_TRACKING_PREFIX' => Tools::getValue('BM_JANIO_TRACKING_PREFIX', Configuration::get('BM_JANIO_TRACKING_PREFIX')),
            'BM_KL_STATUS' => Tools::getValue('BM_KL_STATUS', Configuration::get('BM_KL_STATUS')),
            'BM_KL_TO_STATUS' => Tools::getValue('BM_KL_TO_STATUS', Configuration::get('BM_KL_TO_STATUS')),
            'BM_KL_SHIP_STATUS' => Tools::getValue('BM_KL_SHIP_STATUS', Configuration::get('BM_KL_SHIP_STATUS')),
            'BM_KL_OOS_STATUS' => Tools::getValue('BM_KL_OOS_STATUS', Configuration::get('BM_KL_OOS_STATUS')),
            'BM_KL_OOS_REVERSE' => Tools::getValue('BM_KL_OOS_REVERSE', Configuration::get('BM_KL_OOS_REVERSE')),

            'BM_JANIO_URL' => Tools::getValue('BM_JANIO_URL', Configuration::get('BM_JANIO_URL')),
            'BM_JANIO_SECRET' => Tools::getValue('BM_JANIO_SECRET', Configuration::get('BM_JANIO_SECRET')),
            'BM_JANIO_PICKUP_CONTACT_NAME' => Tools::getValue('BM_JANIO_PICKUP_CONTACT_NAME', Configuration::get('BM_JANIO_PICKUP_CONTACT_NAME')),
            'BM_JANIO_PICKUP_CONTACT_NUMBER' => Tools::getValue('BM_JANIO_PICKUP_CONTACT_NUMBER', Configuration::get('BM_JANIO_PICKUP_CONTACT_NUMBER')),
            'BM_JANIO_PICKUP_COUNTRY' => Tools::getValue('BM_JANIO_PICKUP_COUNTRY', Configuration::get('BM_JANIO_PICKUP_COUNTRY')),
            'BM_JANIO_PICKUP_POSTAL' => Tools::getValue('BM_JANIO_PICKUP_POSTAL', Configuration::get('BM_JANIO_PICKUP_POSTAL')),
            'BM_JANIO_PICKUP_ADDRESS' => Tools::getValue('BM_JANIO_PICKUP_ADDRESS', Configuration::get('BM_JANIO_PICKUP_ADDRESS')),
            'BM_JANIO_PICKUP_STATE' => Tools::getValue('BM_JANIO_PICKUP_STATE', Configuration::get('BM_JANIO_PICKUP_STATE')),
		);
	}
    
    public function hookHeader($params)
	{
		
		
        $aff_info = null;
        
        $utmSource = Tools::getValue('utm_source');
		
		if (Tools::getValue('controller')=='category') {
			$id_category = Tools::getValue('id_category');
			
			if ($id_category){
				$sql='SELECT * FROM pensonic_commission WHERE commission_category='.$id_category;
				$order_info = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
				
				if (isset($order_info[0]) && !$order_info[0]['hideLink']){
					$utmSource=$order_info[0]['affiliate_code'];
				}else if (isset($order_info[0]) && $order_info[0]['hideLink']){
					$affiliateCookie = new Cookie("aff_info");
					if((isset($affiliateCookie) && isset($affiliateCookie->utm_source) 
						&& $affiliateCookie->utm_source==$order_info[0]['affiliate_code']) || Tools::getValue('utm_source')==$order_info[0]['affiliate_code']  ) {
						// is ok
					}else{
						header('Location: https://estore.pensonic.com');
						exit;
					}
				}else{
				}
				
				
				$thisCategory=new Category($id_category);
				$parentCatArr=$thisCategory->getAllParents();
				$hasAffiliate314=false;
				foreach($parentCatArr as $oneCat){
					if ($oneCat->id==314)
						$hasAffiliate314=true;
				}
				
				if ($hasAffiliate314){
					global $smarty;
					$this->smarty->assign('nobots', true);
					$smarty->assign('nobots', true);
					$this->smarty->assign('nofollow', true);
					$smarty->assign('nofollow', true);
				}
				
					global $smarty;
					$smarty->assign('left_column_size',0);
					$smarty->assign('hide_left_column',true);
			}
				
		}
		
        if(isset($utmSource) && trim($utmSource) != "") {
            $affiliateCookie = new Cookie("aff_info", "", time() + 60* 60 * 24 * 30);//expired in 30 days
            $affiliateCookie->utm_source = trim($utmSource);
              
            $sessionId = Tools::getValue('session_id');
            
            if(isset($sessionId) && trim($sessionId) != "") {
                $affiliateCookie->session_id = trim($sessionId);
            }
            
            $aff_info = array();
            $aff_info['utm_source'] = $affiliateCookie->utm_source;
            
            if(isset($affiliateCookie->session_id)) {
                 $aff_info['session_id'] = $affiliateCookie->session_id;
            }
			
			if ($this->context->customer && $this->context->customer->id){ 
				Db::getInstance(_PS_USE_SQL_SLAVE_)->execute('
					INSERT INTO `'._DB_PREFIX_.'source_tracking` (id_customer, source, add_date, type, url) VALUES ("'.$this->context->customer->id.'",
					"'.$utmSource.'",CURRENT_TIMESTAMP,"NEW","'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'");
					');
			}else {
				if (!Context::getContext()->cookie->id_guest || Context::getContext()->cookie->id_guest==0)
					Guest::setNewGuest($this->context->cookie);
				Db::getInstance(_PS_USE_SQL_SLAVE_)->execute('
					INSERT INTO `'._DB_PREFIX_.'source_tracking` (id_guest, source, add_date, type, url) VALUES ("'.Context::getContext()->cookie->id_guest.'",
					"'.$utmSource.'",CURRENT_TIMESTAMP,"NEW","'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'");
					');
			}
			
		
			$sql='SELECT * FROM pensonic_commission WHERE affiliate_code="'.$utmSource.'"';
			$order_info = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
			
			if (isset($order_info[0])){
				if ($order_info[0]['hideLink']){
					$pageURL = 'http';
					if ($_SERVER["HTTPS"] == "on") {
						$pageURL .= "s";
					}
					$pageURL .= "://";
					if ($_SERVER["SERVER_PORT"] != "80") {
						$pageURL .= $_SERVER["SERVER_NAME"] . ":" .
							$_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
					} else {
						$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
					}
					
					$pageURL = strtok($pageURL, '?');
					
					header('Location: '.$pageURL);
					exit;
				}
			}
        
        }
		else {
            $affiliateCookie = new Cookie("aff_info");
    
            if(isset($affiliateCookie) && isset($affiliateCookie->utm_source)) {
                $aff_info = array();
                $aff_info['utm_source'] = $affiliateCookie->utm_source;
				$utmSource=$affiliateCookie->utm_source;
            
                if(isset($affiliateCookie->session_id)) {
                     $aff_info['session_id'] = $affiliateCookie->session_id;
                }
					
				if ($this->context->customer && $this->context->customer->id){
					Db::getInstance(_PS_USE_SQL_SLAVE_)->execute('
						INSERT INTO `'._DB_PREFIX_.'source_tracking` (id_customer, source, add_date, type, url) VALUES ("'.$this->context->customer->id.'",
						"'.$utmSource.'",CURRENT_TIMESTAMP,"CONTINUE","'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'");
						');
						
				}else if (isset(Context::getContext()->cookie->id_guest) ){
					Db::getInstance(_PS_USE_SQL_SLAVE_)->execute('
						INSERT INTO `'._DB_PREFIX_.'source_tracking` (id_guest, source, add_date, type,url) VALUES ("'.Context::getContext()->cookie->id_guest.'",
						"'.$utmSource.'",CURRENT_TIMESTAMP,"CONTINUE","'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'");
						');
				}
            
            
            }
        }
        
        Media::addJsDef(array('aff_info' => $aff_info));
	}
    
    public function hookFooter($params)
	{
        //$aff_info = null;
        //$affiliateCookie = new Cookie("aff_info");
    
        //if(isset($affiliateCookie) && isset($affiliateCookie->utm_source)) {
        //    $aff_info = array();
        //    $aff_info['utm_source'] = $affiliateCookie->utm_source;
            
        //    if(isset($affiliateCookie->session_id)) {
        //         $aff_info['session_id'] = $affiliateCookie->session_id;
        //    }
            
        //}
        
        //Media::addJsDef(array('aff_info' => $aff_info));
    }
    
    public function hookDisplayOrderConfirmation($params) 
    {
        $order_info = null;

        if (isset($params['objOrder'])) {
            $order = $params['objOrder'];
            
            $order_pixel = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT * FROM `'._DB_PREFIX_.'order_pixel` A
				WHERE id_order='.(int)$order->id);
				
			if (isset($order_pixel[0]))
				return;
			
            $dbSource = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT source FROM `'._DB_PREFIX_.'source_tracking` A
				WHERE add_date>=ADDDATE(CURRENT_DATE, INTERVAL -30 DAY) AND type="NEW" AND id_customer='.(int)$this->context->customer->id.
				' ORDER BY add_date DESC LIMIT 1'
				);
			$dbSourceValue='';
			if (isset($dbSource[0])){
				$dbSourceValue=$dbSource[0]['source'];
			}
			
			
            $order_info = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT DISTINCT A.product_id,A.product_name,A.product_quantity,A.product_reference,A.unit_price_tax_incl,
                    C.id_category,C.name AS category_name
				FROM `'._DB_PREFIX_.'order_detail` A
                INNER JOIN `'._DB_PREFIX_.'product` B ON A.product_id = B.id_product
                INNER JOIN `'._DB_PREFIX_.'category_lang` C ON B.id_category_default = C.id_category
				WHERE id_order='.(int)$order->id.' AND C.id_lang='.$this->context->language->id);
                
            $utmSource = "";
            $clickId = "";
			$commission_value=0;
            $affiliateCookie = new Cookie("aff_info");
        
            if( (isset($affiliateCookie) && isset($affiliateCookie->utm_source)) || $dbSourceValue ) {
				if (isset($affiliateCookie) && isset($affiliateCookie->utm_source))
					$utmSource = $affiliateCookie->utm_source;
				else
					$utmSource = $dbSourceValue;
            
                if(isset($affiliateCookie->session_id)) {
                    $clickId = $affiliateCookie->session_id;
                }
				
				if ($utmSource){
						
					$sql='SELECT * FROM `'._DB_PREFIX_.'commission` WHERE affiliate_code="'.$utmSource.'"';
					$commissionResult = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
					
					if (isset($commissionResult[0])){
						$commission_rate=$commissionResult[0]['commission_rate'];
						$commission_max=$commissionResult[0]['commission_max'];
						
						$commission_value = round($order->total_paid * $commission_rate / 100,2);
						if ($commission_value>$commission_max)
							$commission_value=$commission_max;
					} 
					
				}
            } 
			
        
            $sql = 'INSERT INTO '._DB_PREFIX_.'order_pixel ('.
                        'id_order,date_add,order_value,clickId,customer_type,'.
                        'basket,exclude,utm_source,utm_campaign,utm_medium,commission_value)'.
                        "VALUES(".$order->id.", NOW(),".$order->total_paid.",'".$clickId."','',".
                        "'".pSQL(json_encode($order_info))."','0','".$utmSource."','','','".$commission_value."')";
                            
            Db::getInstance()->execute($sql);
        }
        
        Media::addJsDef(array('order_info' => $order_info));
    }
}
