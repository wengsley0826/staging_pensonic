<?php

class AdminOrderTrackingController extends ModuleAdminController
{
    protected $_html = '';
    
    public function __construct() { 
		$this->table = 'orders'; 
		$this->className = 'orders'; 
		$this->bootstrap = true;
	
        parent::__construct();
        
        $this->fields_options = array(
			'general' => array(
				'title' =>	$this->l('Bulk Update Order Shipping'),
				'icon' =>	'icon-cogs',
                'fields' => array(
						'ORDER_TRACKING_FILE' => array(
							'type' => 'file', 
							'title' => $this->l('Upload File'), 
							'desc' => $this->l('<a href=../upload/bulkshipping.csv target=_blank>Sample CSV format</a>.'),
				            'name' => 'file_url'
						)
					),
    			'submit' => array(
					'title' => $this->l('Upload'),
					'name' => 'submit_form'
			    )    
		    )
        );
	}
	
	public function postProcess()
    {
		if (Tools::isSubmit('submit_form'))
		{	
    		//file upload code
			if (isset($_FILES['file_url']) && $_FILES['file_url']['size'] > 0) 
			{	
				$target_dir = _PS_UPLOAD_DIR_."\\".$this->context->employee->id."\\";
				$target_file = $target_dir . basename($_FILES['file_url']["name"]);	
				$uploadOk = 1;
				$uploadFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				
				// Check if file already exists
				if (file_exists($target_file)) {
					$this->errors[] = Tools::displayError("Sorry, file already exists.");
                    return;
					$uploadOk = 0;
				}
				// Allow certain file formats
				if($uploadFileType != "csv") {
					$this->errors[] = Tools::displayError("File is not in CSV format. Only CSV format are allowed.");
                    return;
                    $uploadOk = 0;
				}
                
                if(!file_exists ($target_dir)) {
                    if(!mkdir($target_dir)) {
                        $this->errors[] = Tools::displayError("Error occurred when create directory, please contact administrator.");
                        return;
                    }
                }
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 1)
				{
                    if (move_uploaded_file($_FILES['file_url']["tmp_name"], $target_file)) 
					{
                        $file_location = basename($_FILES['file_url']["name"]);
                        //$this->_html .= $this->displayConfirmation($this->l("The file ". $file_location. " has been uploaded."));	
                        $this->confirmations[] = $this->l('File upload successful.')."<br />";
                        
                        if (($handle = fopen($target_file, "r")) !== FALSE) {
                            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                                $num = count($data);
                                $row++;
                                if($num == 2 && $row > 1) {
                                    $id_order = (int)$data[0];
                                    $order = new Order($data[0]);
                                    
                                    if(!Validate::isLoadedObject($order)) {
                                        $this->errors[] = Tools::displayError("Line $row: Invalid order ".$data[0]);
                                    }
                                    else {
                                        $order->shipping_number = $data[1];
                                        $order->update();

                                        // Update order_carrier
                                        $id_order_carrier = Db::getInstance()->getValue("SELECT MIN(id_order_carrier) FROM ". _DB_PREFIX_ ."order_carrier WHERE id_order=".$order->id);
                                        $order_carrier = new OrderCarrier($id_order_carrier);
                                        
                                        if(Validate::isLoadedObject($order_carrier)) {
                                            $order_carrier->tracking_number = $data[1];
                                            
                                            if ($order_carrier->update())
                                                $this->confirmations[] = "line $row: Order ".$data[0]. " updated (".$data[1]. ")<br />";
                                        }
                                    }                                    
                                }
                            }
                            fclose($handle);
                        }
                        
                        if(unlink($target_file)) {
                        }
					} 
					else 
					{
						$this->errors[] = Tools::displayError("Error occurred when upload your file, please contact administrator.");
                        return;
                    }
				}				
			}
            else {
                $this->errors[] = Tools::displayError("Please upload CSV format file to bulk update Orders Tracking No.");
                return;
            }
        }
        
        parent::postProcess();
	}
}
