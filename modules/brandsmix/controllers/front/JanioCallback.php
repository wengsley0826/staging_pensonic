<?php

// ~/module/brandsmix/JanioCallback
class BrandsMixJanioCallbackModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
    
    }
    
    
    public function initContent()
    {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        
        $payload = file_get_contents('php://input'); 
        $order_id = "";
        $tracking_no = "";
        $status = "";
        $logId = $this->SaveLog($payload, $order_id, $tracking_no, $status);
   
        if($payload != "") {
            $janio_data = json_decode($payload, 1);

            if($janio_data != null && isset($janio_data["data"])) {
                if(isset($janio_data["data"]["order_id"])) { $order_id = $janio_data["data"]["order_id"]; }
                if(isset($janio_data["data"]["tracking_no"])) { $tracking_no = $janio_data["data"]["tracking_no"]; }
                if(isset($janio_data["data"]["status"])) { $status = $janio_data["data"]["status"]; }                
            }

            $this->UpdateLog($logId, $order_id, $tracking_no, $status);
        }
        
        $orderInfo = Db::getInstance()->getRow('SELECT id_order, current_state FROM `'._DB_PREFIX_.'orders` WHERE shipping_number="'.$tracking_no.'"');

        if($orderInfo && isset($orderInfo['id_order']) &&  $orderInfo['id_order'] > 0) 
        {
            $newStatus = $this->MapStatus($status);

            if($newStatus > 0 && $orderInfo['current_state'] != $newStatus) {
                $this->updateOrderStatus($orderInfo['id_order'], 0, $newStatus); 
            }
        }

        die();
    }

    private function updateOrderStatus($id_order, $employeeId, $to_status) 
    {
        $new_history = new OrderHistory();
		$new_history->id_order = (int)$id_order;
        $new_history->id_employee = (int)$employeeId;
		$new_history->changeIdOrderState((int)$to_status, $id_order, true);
		$new_history->addWithemail(true);
    }

    private function SaveLog ($postdata, $order_id, $tracking_no, $status)
    {

        $sql = 'INSERT INTO `'._DB_PREFIX_.'janio_tracking` (`date_add`, `janio_response`, `order_id`, `tracking_no`, `janio_status`) '.
                'VALUES (NOW(), "'.pSQL($postdata).'","'.$order_id.'","'.$tracking_no.'","'.$status.'")';
        $return = Db::getInstance()->execute($sql);
        return Db::getInstance()->Insert_ID();
    }

    private function UpdateLog ($id, $order_id, $tracking_no, $status)
    {

        $sql = 'UPDATE `'._DB_PREFIX_.'janio_tracking` SET '.
                    '`order_id`="'.$order_id.'",'.
                    '`tracking_no`="'.$tracking_no.'",'.
                    '`janio_status`="'.$status.'" '.
                'WHERE id='.$id;
        $return = Db::getInstance()->execute($sql);
    }

    private function MapStatus($janio_status) 
    {
        $order_status = 0;
        switch($janio_status) {
             case "ORDER_INFO_RECEIVED":
             case "ORDER_PICKED_UP":
             case "INTRA_ORIGIN_TRANSFER":
             case "ORDER_RECEIVED_AT_LOCAL_SORTING_CENTER":
             case "ORDER_RECEIVED_BY_AIRLINE":
             case "PENDING_CUSTOMS_CLEARANCE":
             case "ORDER_RECEIVED_AT_DESTINATION_WAREHOUSE":
             case "INTRA_DESTINATION_TRANSFER":
             case "DELIVERY_IN_PROGRESS":
             case "WITH_CUSTOMER_SERVICE":
                $order_status = 0;
                break;
            case "SUCCESS":
                $order_status = 5;
                break;
            case "FAILED_DUE_TO_WRONG_ADDRESS":
            case "FAILED_DUE_TO_CUSTOMER_UNCONTACTABLE":
            case "FAILED_DUE_TO_CUSTOMER_REJECT_ORDER":
            case "RETURNED_TO_LOCAL_SORTING_CENTER":
            case "RETURNED_TO_DESTINATION_WAREHOUSE":
            case "DESTROYED_AT_DESTINATION_WAREHOUSE":
            case "CANCELLED_BY_CUSTOMER":
            case "RETURNED_TO_CLIENT":
                $order_status = 15;
                break;
        
        }

        return $order_status;
    }
}
