<?php

// ~/module/brandsmix/KerryInbound
class BrandsMixKerryInboundModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
    
    }
    
    /*
         step 1 => get order for kerry outbound process
         step 2 => generate invoice for the order
         step 3 => generate outbound batch file
         step 4 => ftp to KL
         step 5 => order status updated to in fulfilment
    */
    public function initContent()
    {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        //$this->sftpFileFromKL();

        //$this->TestShipped(83);
        //die();
        if(isset($_REQUEST["type"])) {
            $type = $_REQUEST["type"];
            switch($type) {
                case "list":
                    $this->sftpListInfo();
                    die();
                break;
                case "pdf":
                    $this->generatePDF($_REQUEST['id_order'], "temp");
                    die();
                case "get":
                    $this->sftpFileFromKL();
                    die();
                case "getprocess":
                    $this->sftpFileFromKL();
                    $this->processInboundFile();
                    die();
                case "processInbound":
                    $this->processInboundFile();
                    die();
                case "janio":
                    $this->createJanioOrder();
                    die();
                case "resendToKL": //// generate the order txt file and send to KL again. assume Invoice already generated
                    $id_orders = $_REQUEST["id_orders"];
                    $this->resendToKL($id_orders);
                    die();
                case "completeKLJanio":
                    //// 5 => switch order status to infulfilment when both kerry file uploaded and janio order created       
                    $to_order_state = Configuration::get("BM_KL_TO_STATUS");

                    $sql = "SELECT A.order_outbound_id, A.id_order FROM ". _DB_PREFIX_ ."order_outbound A ".
                            "INNER JOIN ". _DB_PREFIX_ ."orders B ON A.id_order = B.id_order ".
                            "WHERE A.kerry_status=4 AND B.current_state IN (".Configuration::get("BM_KL_STATUS").")";
                    $ordersToUpdate = Db::getInstance()->executeS($sql);
                    $new_order_outbound_list = [];

                    foreach($ordersToUpdate as $o) 
                    {
                        $new_history = new OrderHistory();
			            $new_history->id_order = (int)$o['id_order'];
			            $new_history->changeIdOrderState((int)$to_order_state, $o['id_order'], true);
			            $new_history->addWithemail(true);

                        $new_order_outbound_list[] = $o['order_outbound_id'];
                    }

                    if(count($new_order_outbound_list) > 0) {
            print_r("Update ORDER_OUTBOUND");
            print_r("<br />"); 
                        Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=5 WHERE order_outbound_id IN (".implode(",", $new_order_outbound_list). ")");
                    }
                    die();
                default:
                break;
            }
        }

        //$batchFileName = "SALEORDER_20200306_1.TXT";
        //$processDate = "20200306";
        //$order_id_list = [3,6,8];

        //$uploadResult = $this->sftpFileToKL($batchFileName, $processDate, $order_id_list);
        
        //die();
        $processDate = date("Ymd");
        if($this->isSkipDate($processDate)) {
            print_r("Skip Kerry Logistics Process due to holiday / weekend");
            die();
        }

        $orders = $this->getOrderToProcess();
        $order_ids = [];
        $order_outbound_list = [];
        $order_id_list = [];
        
        for ($i = 0; $i < count($orders); $i++) {
            if(!in_array($orders[$i]["id_order"], $order_ids)) {
                $order_ids[] = $orders[$i]["id_order"];
            }
        }

print_r("Order to process => ".count($order_ids));
print_r("<br />");
        for ($i = 0; $i < count($order_ids); $i++) {
            $id_order = $order_ids[$i];
print_r("ORDER:".$id_order);
print_r("<br />");
print_r("Insert to ORDER_OUTBOUND");
print_r("<br />");
            //// 1 => order to process
            $sql = "INSERT INTO `". _DB_PREFIX_ ."order_outbound` (`id_order`, `date_add`, `date_upd`, `kerry_status`,`janio_status`)".
                    "VALUES(".$id_order.",NOW(),NOW(),1,0)";
            Db::getInstance()->execute($sql);
            $order_outbound_id = Db::getInstance()->Insert_ID();
print_r("Generate Invoice PDF");
print_r("<br />");      

    
            //// generate PDF
            $this->generatePDF($id_order, $processDate);
 
            //// 2 => generate invoice successful
            if(file_exists($this->getOutboundLocalPath().$processDate."/".Tools::brandsOrderNumber($id_order).".pdf")) {
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=2 WHERE order_outbound_id=".$order_outbound_id);
                $order_outbound_list[] = $order_outbound_id;
                $order_id_list[] = $id_order;
            }
        }

        $continueProcess = false;
print_r("Step 3 =>");
        //// 3 => generate Batch File
        if(count($order_outbound_list) > 0) {
print_r("Generate Batch Flat File");
print_r("<br />"); 
            $batchNo = $this->generateOrderBatchFile($orders, $processDate, $order_id_list);

            $batchFileName = "SALEORDER_".$processDate."_".$batchNo.".TXT";
            if(file_exists($this->getOutboundLocalPath().$batchFileName)) 
            {
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=3,kerry_batchId=".$batchNo." WHERE order_outbound_id IN (".implode(",", $order_outbound_list). ")");
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."KL_outbound` SET status=3 WHERE id_KL_outbound=".$batchNo);
                $continueProcess = true;
            }
            else {
                $continueProcess = false;
            }
        }

print_r("Step 4 =>");        
        //// 4 => ftp to KL
        if($continueProcess) {
print_r("FTP to Kerry Logistic");
print_r("<br />");
            $continueProcess = false;
            $uploadResult = $this->sftpFileToKL($batchFileName, $processDate, $order_id_list);
            
            if($uploadResult) {
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=4 WHERE order_outbound_id IN (".implode(",", $order_outbound_list). ")");
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."KL_outbound` SET status=4 WHERE id_KL_outbound=".$batchNo);
                $continueProcess = true;
            }
            else {
                $continueProcess = false;
            }
        }
//print_r("Create Janio Order");
//print_r("<br />");  
//        //// submit order to Janio
//        $this->createJanioOrder();
//print_r("Update order status to infulfilment");
//print_r("<br />"); 
        //// 5 => switch order status to infulfilment when both kerry file uploaded and janio order created       
        $to_order_state = Configuration::get("BM_KL_TO_STATUS");

        $sql = "SELECT A.order_outbound_id, A.id_order FROM ". _DB_PREFIX_ ."order_outbound A ".
                "INNER JOIN ". _DB_PREFIX_ ."orders B ON A.id_order = B.id_order ".
                "WHERE A.kerry_status=4 AND B.current_state IN (".Configuration::get("BM_KL_STATUS").")";
        $ordersToUpdate = Db::getInstance()->executeS($sql);
        $new_order_outbound_list = [];

        foreach($ordersToUpdate as $o) 
        {
            $new_history = new OrderHistory();
			$new_history->id_order = (int)$o['id_order'];
			$new_history->changeIdOrderState((int)$to_order_state, $o['id_order'], true);
			$new_history->addWithemail(true);

            $new_order_outbound_list[] = $o['order_outbound_id'];
        }

        if(count($new_order_outbound_list) > 0) {
print_r("Update ORDER_OUTBOUND");
print_r("<br />"); 
            Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=5 WHERE order_outbound_id IN (".implode(",", $new_order_outbound_list). ")");
        }
print_r("Job Completed");
print_r("<br />");    
        die();
    }

    private function getOutboundLocalPath() 
    {
        $dir = dirname(__FILE__)."/outbound/";
        return $dir;
    }

    private function getInboundLocalPath() 
    {
        $dir = dirname(__FILE__)."/inbound/";
        return $dir;
    }

    private function resendToKL($id_orders) {
        $processDate = date("Ymd");

        $sql = 
            "select A.id_order, A.id_customer, C.firstname, C.lastname, 
	            D.phone, D.phone_mobile, D.firstname AS delivery_firstname, D.lastname AS delivery_lastname, D.postcode,
                D.address1, D.address2, D.city,  E.name AS state_name, F.name AS country,  REPLACE(IFNULL(K.message,''),'\n','. ') AS delivery_note,
                B.id_order_detail,  G.reference AS product_reference, M.name AS product_name, B.product_quantity * IFNULL(L.quantity,1) AS product_quantity, A.total_paid,  
                IFNULL(G.unity, 'PAC') AS UOM, H.box_size, H.total_weightage
            from ". _DB_PREFIX_ ."orders A
            inner join (
	            select A.id_order, B.box_size, A.total_weightage
	            from (
		            select id_order, SUM(product_weight * product_quantity) AS total_weightage
		            from ". _DB_PREFIX_ ."order_detail
		            group by id_order
	            ) A
	            inner join ". _DB_PREFIX_ ."carrier_box B ON B.min_weightage <= A.total_weightage AND B.max_weightage >= A.total_weightage
            ) H ON A.id_order = H.id_order
            inner join ". _DB_PREFIX_ ."order_detail B ON A.id_order = B.id_order
            inner join ". _DB_PREFIX_ ."customer C ON A.id_customer = C.id_customer
            inner join ". _DB_PREFIX_ ."address D ON A.id_address_delivery = D.id_address
            left join ". _DB_PREFIX_ ."state E ON D.id_state = E.id_state
            inner join ". _DB_PREFIX_ ."country_lang F ON D.id_country = F.id_country AND F.id_lang = 1
            left join ". _DB_PREFIX_ ."pack L ON B.product_id = L.id_product_pack
            left join ". _DB_PREFIX_ ."product G ON IFNULL(L.id_product_item, B.product_id) = G.id_product
            left join ". _DB_PREFIX_ ."product_lang M ON G.id_product = M.id_product AND M.id_lang = 1
            left join (
                select id_order, MAX(kerry_status) AS kerry_status
                from ". _DB_PREFIX_ ."order_outbound
                group by id_order
            ) J ON A.id_order = J.id_order AND J.kerry_status >= 4
            left join (
				select id_order, MIN(id_message), message
				from ". _DB_PREFIX_ ."message 
				where private = 0
				group by id_order
            ) K ON A.id_order = K.id_order
            where A.id_order IN (".$id_orders.")
            order by A.id_order";
        $orders = Db::getInstance()->executeS($sql);

        $order_id_list = [];

        for ($i = 0; $i < count($orders); $i++) {
            $order_id_list[] = $orders[$i]['id_order'];
        }

        $batchNo = $this->generateOrderBatchFile($orders, $processDate, $order_id_list);
        $batchFileName = "SALEORDER_".$processDate."_".$batchNo.".TXT";
        $uploadResult = $this->sftpFileToKL($batchFileName, $processDate, $order_id_list);

        if($uploadResult) {
            Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."KL_outbound` SET status=4 WHERE id_KL_outbound=".$batchNo);    
        }
    }

    public function getOrderToProcess() {
        $sql = 
            "select A.id_order, A.id_customer, C.firstname, C.lastname, 
	            D.phone, D.phone_mobile, D.firstname AS delivery_firstname, D.lastname AS delivery_lastname, D.postcode,
                D.address1, D.address2, D.city,  E.name AS state_name, F.name AS country,  REPLACE(IFNULL(K.message,''),'\n','. ') AS delivery_note,
                B.id_order_detail,  G.reference AS product_reference, M.name AS product_name, B.product_quantity * IFNULL(L.quantity,1) AS product_quantity, A.total_paid,  
                IFNULL(G.unity, 'PAC') AS UOM, H.box_size, H.total_weightage
            from ". _DB_PREFIX_ ."orders A
            inner join (
	            select A.id_order, B.box_size, A.total_weightage
	            from (
		            select id_order, SUM(product_weight * product_quantity) AS total_weightage
		            from ". _DB_PREFIX_ ."order_detail
		            group by id_order
	            ) A
	            inner join ". _DB_PREFIX_ ."carrier_box B ON B.min_weightage <= A.total_weightage AND B.max_weightage >= A.total_weightage
            ) H ON A.id_order = H.id_order
            inner join ". _DB_PREFIX_ ."order_detail B ON A.id_order = B.id_order
            inner join ". _DB_PREFIX_ ."customer C ON A.id_customer = C.id_customer
            inner join ". _DB_PREFIX_ ."address D ON A.id_address_delivery = D.id_address
            left join ". _DB_PREFIX_ ."state E ON D.id_state = E.id_state
            inner join ". _DB_PREFIX_ ."country_lang F ON D.id_country = F.id_country AND F.id_lang = 1
            left join ". _DB_PREFIX_ ."pack L ON B.product_id = L.id_product_pack
            left join ". _DB_PREFIX_ ."product G ON IFNULL(L.id_product_item, B.product_id) = G.id_product
            left join ". _DB_PREFIX_ ."product_lang M ON G.id_product = M.id_product AND M.id_lang = 1
            left join (
                select id_order, MAX(kerry_status) AS kerry_status
                from ". _DB_PREFIX_ ."order_outbound
                group by id_order
            ) J ON A.id_order = J.id_order AND J.kerry_status >= 4
            left join (
				select id_order, MIN(id_message), message
				from ". _DB_PREFIX_ ."message 
				where private = 0
				group by id_order
            ) K ON A.id_order = K.id_order
            where A.current_state IN (".Configuration::get("BM_KL_STATUS").") and J.id_order IS NULL
            order by A.id_order";
        $orders = Db::getInstance()->executeS($sql);

        return $orders;
    }

    public function generatePDF($id_order,$processDate)
    {
        $order = new Order((int)$id_order);
        
        if (!Validate::isLoadedObject($order)) {
            return false;
            //die(Tools::displayError('The order cannot be found within your database.'));
        }

        $order_invoice_list = $order->getInvoicesCollection();
        
        $outboundDir = $this->getOutboundLocalPath();
        if(!file_exists ($outboundDir)) {
            mkdir($outboundDir, 0777, true);
        }

        $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, Context::getContext()->smarty);
        $exportDir = $outboundDir.$processDate."/";

        if(!file_exists ($exportDir)) {
            mkdir($exportDir, 0777, true);
        }

        $pdf->render("F", $exportDir, Tools::brandsOrderNumber($id_order).".pdf");

        return true;
    }

    public function generateOrderBatchFile($orders, $processDate, $order_id_list) 
    {
        if(count($orders) > 0) 
        {
            $curDate = date('Y-m-d H:i:s');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'KL_outbound` (`date_add`, `date_upd`, `status`) VALUES ("'.$curDate.'","'.$curDate.'", 1)');
            $batchNo = Db::getInstance()->Insert_ID();

            $outboundDir = $this->getOutboundLocalPath();
            if(!file_exists ($outboundDir)) {
                mkdir($outboundDir, 0777, true);
            }

            $batchFileName = "SALEORDER_".$processDate."_".$batchNo.".TXT";
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'KL_outbound` SET batchFileName="'.$batchFileName.'" WHERE id_KL_outbound='.$batchNo);
            
            $batchFile = fopen($outboundDir.$batchFileName, "w");

            $itemNo = 1;
            $curOrderId = 0;

            foreach ($orders as $o) 
            {
                if(in_array($o["id_order"],$order_id_list)) 
                {
                    $txt = "1|";
                    $txt .= "|"; //Customer number
                    $txt .= Tools::brandsOrderNumber($o['id_order'])."|";
                    $txt .= $o['id_customer']."|";
                
                    $txt .= "|"; //Shop category
                    $txt .= "|"; //Retail store code
                    $txt .= "|"; //Store name
                    $txt .= "|"; //Specify delivery negotiates
                    $txt .= "6|"; //Service categories
                

                    $txt .= $o['lastname']. " " .$o['firstname']."|";
                    $txt .= $o['phone']."|";
                    $txt .= $o['phone_mobile']."|";
                    $txt .= $o['delivery_lastname']. " " .$o['delivery_firstname']."|";
                    $txt .= $o['postcode']."|";

                    $fullAddress = $o['address1'];
                    if($o['address2'] != "") { $fullAddress .= ", ".$o['address2']; }
                    $fullAddress .= ", ".$o['postcode'];
                    if($o['city'] != "") { $fullAddress .= " " . $o['city']; }
                    if($o['state_name'] != "") { $fullAddress .= ", ".$o['state_name']; }
                    if($o['country'] != "") { $fullAddress .= ", ".$o['country']; }
                
                    $txt .= $fullAddress."|";
                    $txt .= $o['delivery_note']."|";

                    if($o['phone'] != "") { $txt .= $o['phone']."|";}else {$txt .= $o['phone_mobile']."|";}
                    if($o['phone_mobile'] != "") { $txt .= $o['phone_mobile']."|";}else {$txt .= $o['phone']."|";}

                    if($curOrderId != $o['id_order']) {
                        $curOrderId = $o['id_order'];
                        $itemNo = 1;
                    }
                    else {
                        $itemNo = $itemNo + 1;
                    }

                    $txt .= $itemNo."|";
                    $txt .= "Singapore food supplement|"; // Customer name on the delivery order
                    $txt .= "https://mystore.brandsworld.com.sg|"; //Delivery on the customer Web sit
                    $txt .= $o['product_reference']."|";
                    $txt .= $o['product_name']."|";
                    $txt .= $o['product_quantity']."|";
                    $txt .= $batchFileName."|";
                    $txt .= "|"; //Effective duration
                    $txt .= round($o['total_paid'],2)."|";
                    $txt .= "E-commerce - NONE|"; //Second part number
                    $txt .= $o['UOM']."|";
                    $txt .= "0|"; //Collected amount
                    $txt .= "Y|"; //Print invoices
                    $txt .= "|"; //Delivery times
                    $txt .= "|"; //A specified time period
                    $txt .= "|"; //ARS Shipping information
                    $txt .= "|"; //Invoice Number
                    $txt .= "|"; //Invoice date
                    $txt .= "|"; //Random Code
                    $txt .= $o['box_size']."|";
                    $txt .= "\r\n";

                    fwrite($batchFile, $txt);
                }
            }
            
            fclose($batchFile);
        }

        return $batchNo;
    }

    public function sftpFileToKL ($batchFileName, $processDate, $order_id_list) 
    {
        $uploadResult = false;
        $uploadRetry = 0;
        $maxRetry = 5;

        $ftp_server = Configuration::get('BM_KL_HOST');
        $username = Configuration::get('BM_KL_USER');
        $password = Configuration::get('BM_KL_PASS');
        $ftpdirDSO = Configuration::get('BM_KL_DIR_DSO');
        $ftpdirInv = Configuration::get('BM_KL_DIR_INVOICE');
       
        //$ftp_server = "fogcreative.com.my";
        //$username = "brands@fogcreative.com.my";
        //$password = "Brands1234%";
        //$ftpdir = "/modules/brandsmix/controllers/front/outbound/";
        //$conn = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server"); 
        $conn = ssh2_connect($ftp_server) or die("Couldn't connect to $ftp_server"); 

        print_r("<br />Connected");
        // Login
        if (ssh2_auth_password($conn, $username, $password))
        //if (ftp_login($conn, $username, $password))
        {
            // Create SFTP session
            $sftp = ssh2_sftp($conn);
            $sftp_fd = intval($sftp);
            ////ftp_pasv($conn, true);
            print_r("<br />Login success");
            print_r("<br />");

            $outboundDir = $this->getOutboundLocalPath();
            if(!file_exists ($outboundDir)) {
                mkdir($outboundDir, 0777, true);
            }
            
            for($i = 0; $i < count($order_id_list); $i++) 
            {
                $uploadRetry = 0;
                $uploadResult = false;

                $fileName = Tools::brandsOrderNumber($order_id_list[$i]).".pdf";
                print_r("Move ".$fileName. " =>");
                $dir = "ssh2.sftp://$sftp_fd".$ftpdirInv;

                while(!$uploadResult && $uploadRetry < $maxRetry) 
                {
                    $sftpStream = @fopen($dir.$fileName, 'w');
                    if (!$sftpStream) { 
                        print_r("Could not open remote file: $fileName.<br />");  
                        $uploadResult = false;
                        $uploadRetry++; 
                        continue; 
                    }
   
                    $data_to_send = @file_get_contents($outboundDir.$processDate."/".$fileName);
                    if ($data_to_send === false) { 
                        print_r("Could not open local file: $fileName.<br />"); 
                        $uploadResult = false;
                        $uploadRetry++; 
                        fclose($sftpStream); 
                        continue; 
                    }
                    
                    if (@fwrite($sftpStream, $data_to_send) === false) {
                        print_r("Could not send data from file: $fileName.<br />");
                        $uploadRetry++; 
                        fclose($sftpStream); 
                        $uploadResult = false;
                        continue; 
                    }
   
                    fclose($sftpStream);
                    $uploadResult = true;

                    //$uploadResult = ssh2_scp_send($conn, 
                    //    $this->getOutboundLocalPath().$processDate."/".Tools::brandsOrderNumber($order_id_list[$i]).".pdf",
                    //    $ftpdirInv.Tools::brandsOrderNumber($order_id_list[$i]).".pdf");
                    ////$uploadResult = ftp_put($conn, 
                    ////    $ftpdirInv.Tools::brandsOrderNumber($order_id_list[$i]).".pdf",
                    ////    $this->getOutboundLocalPath().$processDate."/".Tools::brandsOrderNumber($order_id_list[$i]).".pdf", 
                    ////    FTP_BINARY);
                    $uploadRetry++;

                    if(!$uploadResult && $uploadRetry < $maxRetry) {
                        sleep(5);
                    }
                }

                if($uploadResult) { print_r("Success"); }
                else { print_r("Failed"); }
                print_r("<br />");
            }

            $uploadRetry = 0;
            $uploadResult = false;

            print_r("Move ".$batchFileName." =>");
            $dir = "ssh2.sftp://$sftp_fd".$ftpdirDSO;
            while(!$uploadResult && $uploadRetry < $maxRetry) 
            {
                $sftpStream = @fopen($dir.$batchFileName, 'w');
                if (!$sftpStream) { 
                    print_r("Could not open remote file: $batchFileName.<br />");  
                    $uploadRetry++; 
                    $uploadResult = false;
                    continue; 
                }
   
                $data_to_send = @file_get_contents($outboundDir.$batchFileName);
                if ($data_to_send === false) { 
                    print_r("Could not open local file: $batchFileName.<br />"); 
                    $uploadRetry++; 
                    fclose($sftpStream); 
                    $uploadResult = false;
                    continue; 
                }
                    
                if (@fwrite($sftpStream, $data_to_send) === false) {
                    print_r("Could not send data from file: $batchFileName.<br />");
                    $uploadRetry++; 
                    fclose($sftpStream); 
                    $uploadResult = false;
                    continue; 
                }
   
                fclose($sftpStream);
                $uploadResult = true;

                //$uploadResult = ssh2_scp_send($conn, 
                //        $this->getOutboundLocalPath().$batchFileName,
                //        $ftpdirDSO.$batchFileName);
                ////$uploadResult = ftp_put($conn, $ftpdirDSO.$batchFileName, $this->getOutboundLocalPath().$batchFileName, FTP_BINARY);
                $uploadRetry++;

                if(!$uploadResult && $uploadRetry < $maxRetry) {
                    sleep(5);
                }
            }

            if($uploadResult) { print_r("Success"); }
                else { print_r("Failed"); }
                print_r("<br />");
        }
        else {
            print_r("<br />Login failed");    
        }
        
        $conn = null; 
        unset($conn);
        print_r("<br />FTP Closed"); 

        return $uploadResult;
    }

    public function sftpFileFromKL () 
    {
        $ftp_server = Configuration::get('BM_KL_HOST');
        $username = Configuration::get('BM_KL_USER');
        $password = Configuration::get('BM_KL_PASS');
        $ftpdirSOC = Configuration::get('BM_KL_DIR_SOC');
        
        //$ftp_server = "mft04.kerrylogistics.com";
        //$username = "sg.cerebos";
        //$password = "lynn27Wino14Huh2B";
        //$ftpdir = "UAT";
        $conn = ssh2_connect($ftp_server) or die("Couldn't connect to $ftp_server"); 

        print_r("Connected");
        // Login
        if (ssh2_auth_password($conn, $username, $password))
        {
            //ftp_pasv($conn, true);
            print_r("<br />Login success");
            print_r("<br />");

            $sftp = ssh2_sftp($conn);
            $sftp_fd = intval($sftp);
            $dir = "ssh2.sftp://$sftp_fd".$ftpdirSOC;
            $files = scandir($dir, 1);
            echo "<br /><br />Scan Dir: $dir<br />";
            echo "Entries:<br />";
            $maxFileExists = 10;
            $currentExistsCount = 0;
            $maxRetry = 5;

            $inboundDir = $this->getInboundLocalPath();

            if(!file_exists ($inboundDir)) {
                mkdir($inboundDir, 0777, true);
            }

            foreach($files as $f) 
            {
                if(is_dir("$dir$f")) {
                    //echo "DIR=>$f<br />";
                }
                else {
                    echo "Read file $f<br />";

                    $uploadRetry = 0;
                    $uploadResult = false;

                    if(!file_exists($inboundDir."backup/".$f)) 
                    {
                        while(!$uploadResult && $uploadRetry < $maxRetry) 
                        {
                            $sftpStream = @fopen($inboundDir.$f, 'w');
                            if (!$sftpStream) { 
                                print_r("Could not open local file: $f.<br />");  
                                $uploadRetry++; 
                                $uploadResult = false;
                                continue; 
                            }
   
                            $data_to_receive = @file_get_contents($dir.$f);
                            if ($data_to_receive === false) { 
                                print_r("Could not open local file: $f.<br />"); 
                                $uploadRetry++; 
                                fclose($sftpStream); 
                                $uploadResult = false;
                                continue; 
                            }
                    
                            if (@fwrite($sftpStream, $data_to_receive) === false) {
                                print_r("Could not receive data from file: $f.<br />");
                                $uploadRetry++; 
                                fclose($sftpStream); 
                                $uploadResult = false;
                                continue; 
                            }
   
                            fclose($sftpStream);
                            $uploadResult = true;
                            $uploadRetry++;

                            if(!$uploadResult && $uploadRetry < $maxRetry) {
                                sleep(5);
                            }
                        }
                    }
                    else {
                        $currentExistsCount++;
                        print_r("$f exists<br />");

                        if($currentExistsCount > $maxFileExists) {
                            print_r("Exit due to exceeded duplicate files count");
                            break;
                        }
                    }

                    //$uploadResult = ssh2_scp_send($conn, 
                    //    $ftpdirDSO.$entry,
                    //    $this->getInboundLocalPath().$entry);
                    //;
                }
            }
        }
        else {
            print_r("<br />Login failed");    
        }
        
        $conn = null; 
        unset($conn);
        print_r("<br />FTP Closed"); 
    }

    public function processInboundFile() 
    {
        $to_process_order_state = Configuration::get("BM_KL_TO_STATUS");
        $shipped_order_state = Configuration::get("BM_KL_SHIP_STATUS");
        $oos_order_state = Configuration::get("BM_KL_OOS_STATUS");
        $inboundDir = $this->getInboundLocalPath();
        $updatedOrder = [];

        if(!file_exists ($inboundDir)) {
            mkdir($inboundDir, 0777, true);
        }

        $inboundBackupDir = $inboundDir."backup/";
        
        if(!file_exists ($inboundBackupDir)) {
            mkdir($inboundBackupDir, 0777, true);
        }

        $files = scandir($inboundDir, 1);

        foreach($files as $f) 
        {
            if(is_dir("$inboundDir$f")) {
            }
            else 
            {
                if (($handle = fopen($inboundDir.$f, "r")) !== FALSE) 
                {
                    $outboundInfo = [];

                    while (($buffer = fgets($handle)) !== FALSE) 
                    {
                        $data = explode("|", $buffer);

                        if($data && count($data) > 3) 
                        {
                            $orderNo = $data[0];

                            if(!array_key_exists($orderNo, $outboundInfo)) {
                                $outboundInfo[$orderNo] = [];
                            }

                            $outboundInfo[$orderNo]["shipStatus"] = $data[3];
                            $outboundInfo[$orderNo]["shipDate"] = $data[1];
                            $outboundInfo[$orderNo]["shipTime"] = $data[2];
                            $outboundInfo[$orderNo]["orderId"] = Tools::storeOrderId($orderNo);

                            Db::getInstance()->execute(
                                'INSERT INTO `'. _DB_PREFIX_ .'KL_inbound` '.
                                '(`batchFileName`,`date_add`,`date_upd`,`orderNo`,`id_order`,`shipStatus`,`shipDate`,`shipTime`,`fullData`)'.
                                'VALUES("'.$f.'",NOW(),NOW(),"'.pSQL($orderNo).'",'.$outboundInfo[$orderNo]["orderId"].','.
                                    '"'.pSQL($data[3]).'","'.pSQL($data[1]).'","'.pSQL($data[2]).'","'.pSQL($buffer).'")');
                        }
                        else {
                            Db::getInstance()->execute(
                                'INSERT INTO `'. _DB_PREFIX_ .'KL_inbound` '.
                                '(`batchFileName`,`date_add`,`date_upd`,`fullData`)'.
                                'VALUES("'.$f.'",NOW(),NOW(),"",NULL,"'.pSQL($buffer).'")');
                        }
                    }

                    fclose($handle);

                    //// update order status if the status in fulfilment
                    foreach($outboundInfo as $key => $value) 
                    {
                        $id_order = $value["orderId"];

                        // check is the order already processed. if processed, skip
                        if(!in_array($id_order, $updatedOrder)) 
                        {
                            $order = new Order($id_order);
    print_r("Order:". $id_order);
    print_r("<br />");
    print_r("Current:".$order->current_state);
    print_r("<br />");
    print_r("Expected:".$to_process_order_state);
    print_r("<br />");
                            //if($order->current_state == $to_process_order_state) 
                            {
                                $updateToState = 0; 
                                $reverseAuthorize = false;
                                $templateVars = array();

                                if($value["shipStatus"] == "1") {
                                    $updateToState = $shipped_order_state;

                                    $shipping_number = $order->shipping_number;

                                    if(!$shipping_number || $shipping_number == "") {
                                        $shipping_number = Configuration::get('BM_JANIO_TRACKING_PREFIX').Tools::brandsOrderNumber($order->id);
                                    }

                                    $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);

                                    $templateVars = array(
                                                        '{followup}' => str_replace('@', $shipping_number, $carrier->url),
                                                        '{shipping_number}' => $shipping_number
                                                    );
                                }
                                else if($value["shipStatus"] == "9" || $value["shipStatus"] == "E" || $value["shipStatus"] == "C") {
                                    $updateToState = $oos_order_state;

                                    $reverseAuthorize = true;
                                }
    print_r("Update to:".$updateToState);
    print_r("<br />");
                                if($updateToState != 0 && $order->current_state != $updateToState && $order->current_state != 5 && $order->current_state != 15) 
                                {
                                    $updateOrder = true;

                                    if($reverseAuthorize) {
                                        $oos_reverse = Configuration::get('BM_KL_OOS_REVERSE');
                                        $paymentMethod = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT module FROM `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order);
                                        if($oos_reverse == "1" && $paymentMethod == "cybersource") 
                                        {
                                            if (!class_exists('cybersource')) {
                                                include(dirname(__FILE__).'/../../../cybersource/cybersource.php');
                                            }

                                            $obj = new cybersource();
                                            $result = $obj->ajaxCall("reverse", $id_order, 0, $updateToState);
                                            $result = json_decode($result, true);
print_r("Reverse result:");
var_dump($result);
print_r("<br />");
                                            if($result['result']) {
                                                $updateOrder = false;
                                            }
                                        }                                        
                                    }
                                    
                                    if($updateOrder) 
                                    {
                                        $new_history = new OrderHistory();
			                            $new_history->id_order = (int)$id_order;
			                            $new_history->changeIdOrderState($updateToState, $id_order, true, true);
			                            $new_history->addWithemail(true, $templateVars);
                                    }

                                    $updatedOrder[] = $id_order;                                    
                                }
                            }
                            
print_r("<br />");
                        }
                    }

                    // move the file to backup folder
                    rename($inboundDir.$f, $inboundBackupDir.$f);
                }
            }
        }
    }

    private function TestShipped($id_order) {
        $to_process_order_state = Configuration::get("BM_KL_TO_STATUS");
        $shipped_order_state = Configuration::get("BM_KL_SHIP_STATUS");

        // check is the order already processed. if processed, skip
        {
            $order = new Order($id_order);
print_r("Order:". $id_order);
print_r("<br />");
print_r("Current:".$order->current_state);
print_r("<br />");
print_r("Expected:".$to_process_order_state);
print_r("<br />");
            //if($order->current_state == $to_process_order_state) 
            {
                $updateToState = 0; 
                $reverseAuthorize = false;
                $templateVars = array();

                
                    $updateToState = $shipped_order_state;

                    $shipping_number = $order->shipping_number;

                    if(!$shipping_number || $shipping_number == "") {
                        $shipping_number = Configuration::get('BM_JANIO_TRACKING_PREFIX').Tools::brandsOrderNumber($order->id);
                    }

                    $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);

                    $templateVars = array(
                                        '{followup}' => str_replace('@', $shipping_number, $carrier->url),
                                        '{shipping_number}' => $shipping_number
                                    );
               
print_r("Update to:".$updateToState);
print_r("<br />");
                if($updateToState != 0 && $order->current_state != $updateToState && $order->current_state != 5 && $order->current_state != 15) 
                {
                    $updateOrder = true;

                    if($reverseAuthorize) {
                        $oos_reverse = Configuration::get('BM_KL_OOS_REVERSE');
                        $paymentMethod = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT module FROM `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order);
                        if($oos_reverse == "1" && $paymentMethod == "cybersource") 
                        {
                            if (!class_exists('cybersource')) {
                                include(dirname(__FILE__).'/../../../cybersource/cybersource.php');
                            }

                            $obj = new cybersource();
                            $result = $obj->ajaxCall("reverse", $id_order, 0, $updateToState);
                            $result = json_decode($result, true);
print_r("Reverse result:");
var_dump($result);
print_r("<br />");
                            if($result['result']) {
                                $updateOrder = false;
                            }
                        }                                        
                    }
                                    
                    if($updateOrder) 
                    {
                        $new_history = new OrderHistory();
			            $new_history->id_order = (int)$id_order;
			            $new_history->changeIdOrderState($updateToState, $id_order, true, true);
			            $new_history->addWithemail(true, $templateVars);
                    }

                    $updatedOrder[] = $id_order;                                    
                }
            }
                            
print_r("<br />");
        }
    }

    public function sftpListInfo () 
    {
        $ftp_server = Configuration::get('BM_KL_HOST');
        $username = Configuration::get('BM_KL_USER');
        $password = Configuration::get('BM_KL_PASS');
        $ftpdirSOC = Configuration::get('BM_KL_DIR_SOC');
        $ftpdirDSO = Configuration::get('BM_KL_DIR_DSO');
        $ftpdirInv = Configuration::get('BM_KL_DIR_INVOICE');
        //$ftp_server = "mft04.kerrylogistics.com";
        //$username = "sg.cerebos";
        //$password = "lynn27Wino14Huh2B";
        //$ftpdir = "UAT";
        $conn = ssh2_connect($ftp_server) or die("Couldn't connect to $ftp_server"); 

        print_r("<br />Connected");
        // Login
        if (ssh2_auth_password($conn, $username, $password))
        {
            //ftp_pasv($conn, true);
            print_r("<br />Login success");
            print_r("<br />");

            $sftp = ssh2_sftp($conn);
            $sftp_fd = intval($sftp);
            $dir = "ssh2.sftp://$sftp_fd".$ftpdirSOC;
            $handle = opendir($dir);
            echo "<br /><br />Directory handle: $handle ($dir)<br />";
            echo "Entries:<br />";
            while (false != ($entry = readdir($handle))){
                if(is_dir("$dir/$entry")) {
                    echo "DIR=>$entry<br />";
                }
                else {
                    echo "FILE=>$entry<br />";
                }
            }

            closedir($handle);

            $dir = "ssh2.sftp://$sftp_fd".$ftpdirDSO;
            $handle = opendir($dir);
            echo "<br /><br />Directory handle: $handle ($dir)<br />";
            echo "Entries:<br />";
            while (false != ($entry = readdir($handle))){
                if(is_dir("$dir/$entry")) {
                    echo "DIR=>$entry<br />";
                }
                else {
                    echo "FILE=>$entry<br />";
                }
            }
            closedir($handle);
            
            $dir = "ssh2.sftp://$sftp_fd".$ftpdirInv;
            $handle = opendir($dir);
            echo "<br /><br />Directory handle: $handle ($dir)<br />";
            echo "Entries:<br />";
            while (false != ($entry = readdir($handle))){
                if(is_dir("$dir/$entry")) {
                    echo "DIR=>$entry<br />";
                }
                else {
                    echo "FILE=>$entry<br />";
                }
            }
            closedir($handle);


            $dir = "ssh2.sftp://$sftp_fd".$ftpdirSOC;
            $files = scandir($dir);
            echo "<br /><br />Scan Dir: $dir<br />";
            echo "Entries:<br />";
            foreach($files as $f) {
                if(is_dir("$dir/$f")) {
                    echo "DIR=>$f<br />";
                }
                else {
                    echo "FILE=>$f<br />";
                }
            }

            $dir = "ssh2.sftp://$sftp_fd".$ftpdirDSO;
            $files = scandir($dir);
            echo "<br /><br />Scan Dir: $dir<br />";
            echo "Entries:<br />";
            foreach($files as $f) {
                if(is_dir("$dir/$f")) {
                    echo "DIR=>$f<br />";
                }
                else {
                    echo "FILE=>$f<br />";
                }
            }
            
            $dir = "ssh2.sftp://$sftp_fd".$ftpdirInv;
            $files = scandir($dir, 1);
            echo "<br /><br />Scan Dir: $dir<br />";
            echo "Entries:<br />";
            foreach($files as $f) {
                if(is_dir("$dir/$f")) {
                    echo "DIR=>$f<br />";
                }
                else {
                    echo "FILE=>$f<br />";
                }
            }
        }
        else {
            print_r("<br />Login failed");    
        }
        
        $conn = null; 
        unset($conn);
        print_r("<br />FTP Closed"); 
    }

    public function isSkipDate($processDate) {
        //// check today is holiday
        $sql = "SELECT COUNT(1) FROM ". _DB_PREFIX_ ."holiday WHERE active=1 AND holiday_date='".$processDate."'";
        $isHoliday = Db::getInstance()->getValue($sql);

        if($isHoliday > 0)
            return true;

        //// check today is weekend
        $dayOfWeek = date('w', strtotime($processDate));

        if($dayOfWeek == 0 || $dayOfWeek == 6) {
            return true;
        }

        return false;
    }

    private $JanioCreateOrderURL = "order/orders/";
    public function createJanioOrder() 
    {
        $processDate = date("Y-m-d");
        if($this->isSkipDate($processDate)) {
            print_r("Skip Janio Process due to holiday / weekend");
            return;
        }

        $sql = "SELECT A.id_order, MAX(B.order_outbound_id) AS order_outbound_id FROM ". _DB_PREFIX_ ."orders A ".
            "INNER JOIN ". _DB_PREFIX_ ."order_outbound B ON A.id_order = B.id_order ".
            "LEFT JOIN ". _DB_PREFIX_ ."order_outbound C ON C.janio_status > 0 AND A.id_order = C.id_order ".
            "WHERE B.kerry_status>=4 AND B.janio_status=0 AND A.shipping_number = '' AND A.current_state IN (".Configuration::get("BM_KL_STATUS").",".Configuration::get("BM_KL_TO_STATUS").") AND C.order_outbound_id IS NULL ".
            "GROUP BY A.id_order";
        $ordersToJanio = Db::getInstance()->executeS($sql);

        $janioConfig = Configuration::getMultiple(array(
            'BM_JANIO_PICKUP_CONTACT_NAME', 'BM_JANIO_PICKUP_CONTACT_NUMBER', 'BM_JANIO_PICKUP_COUNTRY', 
            'BM_JANIO_PICKUP_POSTAL', 'BM_JANIO_PICKUP_ADDRESS', 'BM_JANIO_PICKUP_STATE', 
            'BM_JANIO_SECRET', 'BM_JANIO_URL','BM_JANIO_TRACKING_PREFIX'
        ));

        foreach($ordersToJanio as $o) 
        {
            $sql = 
                "select A.id_order, A.id_customer, C.firstname, C.lastname, C.email,
	                D.phone, D.phone_mobile, D.firstname AS delivery_firstname, D.lastname AS delivery_lastname, D.postcode,
                    D.address1, D.address2, D.city,  E.name AS state_name, F.name AS country,  REPLACE(IFNULL(K.message,''),'\n','. ') AS delivery_note,
                    G.call_prefix,
                    H.box_length, H.box_width, H.box_height, H.volume_weight
                from ". _DB_PREFIX_ ."orders A
                inner join (
	                select A.id_order, B.box_size, A.total_weightage, B.box_length, B.box_width, B.box_height, B.volume_weight
	                from (
		                select id_order, SUM(product_weight * product_quantity) AS total_weightage
		                from ". _DB_PREFIX_ ."order_detail
		                group by id_order
	                ) A
	                inner join ". _DB_PREFIX_ ."carrier_box B ON B.min_weightage <= A.total_weightage AND B.max_weightage >= A.total_weightage
                ) H ON A.id_order = H.id_order
                inner join ". _DB_PREFIX_ ."customer C ON A.id_customer = C.id_customer
                inner join ". _DB_PREFIX_ ."address D ON A.id_address_delivery = D.id_address
                left join ". _DB_PREFIX_ ."state E ON D.id_state = E.id_state
                inner join ". _DB_PREFIX_ ."country_lang F ON D.id_country = F.id_country AND F.id_lang = 1
                inner join ". _DB_PREFIX_ ."country G ON D.id_country = G.id_country
                left join (
					select id_order, MIN(id_message), message
					from ". _DB_PREFIX_ ."message 
					where private = 0
					group by id_order
				) K ON A.id_order = K.id_order
                where A.id_order=".$o["id_order"]."
                order by A.id_order";
            $order = Db::getInstance()->getRow($sql);

            $post_data = $this->JanioOrderRequestParameter($order,$janioConfig);

            $httpHeader = array(
                "content-type: application/json"
            );
print_r($post_data);
print_r("<br />");

            $janioOrderResult = $this->post_data($janioConfig['BM_JANIO_URL'].$this->JanioCreateOrderURL, "POST", $post_data, $httpHeader);
            $r = json_decode($janioOrderResult["result"], true);
print_r("Janio Result=>");
print_r($r);
print_r("<br />");
print_r($janioOrderResult);
print_r("<br />");
            if($janioOrderResult["code"] == "400") {
                Db::getInstance()->execute(
                    "UPDATE `". _DB_PREFIX_ ."order_outbound` SET ".
                        "janio_status=0, ".
                        "janio_request='".pSQL($post_data)."',".
                        "janio_response='".pSQL($janioOrderResult["result"])."' ".
                    "WHERE order_outbound_id=". $o["order_outbound_id"]);
            }
            else if ($r) {
                Db::getInstance()->execute(
                    "UPDATE `". _DB_PREFIX_ ."order_outbound` SET ".
                        "janio_status=1, ".
                        "janio_batchNo='".$r["upload_batch_no"]."',".
                        "janio_tracking='".$r["orders"][0]["tracking_no"]."',".
                        "janio_label='".$r["orders"][0]["order_label_url"]."',".
                        "janio_request='".pSQL($post_data)."',".
                        "janio_response='".pSQL($janioOrderResult["result"])."' ".
                    "WHERE order_outbound_id=". $o["order_outbound_id"]);

                //// update tracking number
                $trackingNo = $janioConfig['BM_JANIO_TRACKING_PREFIX'].Tools::brandsOrderNumber($o["id_order"]);
                $this->updateTrackingNo($o["id_order"], $trackingNo);
            }
            else {
                Db::getInstance()->execute(
                    "UPDATE `". _DB_PREFIX_ ."order_outbound` SET ".
                        "janio_status=0, ".
                        "janio_request='".pSQL($post_data)."',".
                        "janio_response='".pSQL($janioOrderResult["result"])."' ".
                    "WHERE order_outbound_id=". $o["order_outbound_id"]);
            }
        }
    }

    public function updateTrackingNo($id_order, $tracking_number) 
    {
        $id_order_carrier = Db::getInstance()->getValue('SELECT id_order_carrier FROM `'._DB_PREFIX_.'order_carrier` WHERE id_order='. (int)$id_order);
        $order_carrier = new OrderCarrier($id_order_carrier);

        if (Validate::isLoadedObject($order_carrier))
        {
            $order = new Order($id_order); 
            
            $order->shipping_number = $tracking_number;
		    $order->update();

            $order_carrier->tracking_number = $tracking_number;

            if ($order_carrier->update())
		    {
                $customer = new Customer((int)$order->id_customer);
                $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);
                if (!Validate::isLoadedObject($customer))
				    return;
			    if (!Validate::isLoadedObject($carrier))
				    return;

                //$templateVars = array(
                //    '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
                //    '{firstname}' => $customer->firstname,
                //    '{lastname}' => $customer->lastname,
                //    '{id_order}' => $id_order,
                //    '{shipping_number}' => $order->shipping_number,
                //    '{order_name}' => Tools::brandsOrderNumber($id_order)
                //);

                //if (@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Package in transit', (int)$order->id_lang), $templateVars,
                //    $customer->email, $customer->firstname.' '.$customer->lastname, null, null, null, null,
                //    _PS_MAIL_DIR_, true, (int)$order->id_shop))
			    //{
                
				Hook::exec('actionAdminOrdersTrackingNumberUpdate', array('order' => $order, 'customer' => $customer, 'carrier' => $carrier), null, false, true, false, $order->id_shop);
			    //}
            }
        }
    }

    private function post_data($url, $post_method, $post_data = null, $header = null)
    {
        $result = '';

        //create cURL connection
        $curl = curl_init();
  
        //set options
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $post_method,
        ));

        //set data to be posted
        if($post_data != null) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        }
        
        if($header != null) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }
        
        //perform our request
        $result = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err = curl_error($curl);
        //close the connection

        curl_close($curl);

        $curlResult = [];
        $curlResult["result"] = $result;
        $curlResult["code"] = $httpCode;

        return $curlResult;
    }

    private function JanioOrderRequestParameter($o,$janioConfig) 
    {
        $orderRequest = new JanioOrderRequest();
        $orderRequest->secret_key = $janioConfig["BM_JANIO_SECRET"];
        $orderRequest->blocking = true;
        $orderRequest->orders = [];

        $order = new JanioOrder();
        //$order->service_id;
        $order->tracking_no = $janioConfig['BM_JANIO_TRACKING_PREFIX'].Tools::brandsOrderNumber($o["id_order"]);
        $order->shipper_order_id = Tools::brandsOrderNumber($o["id_order"]);

        $order->order_length = $o["box_length"];
        $order->order_width = $o["box_width"];
        $order->order_height = $o["box_height"];
        $order->order_weight = $o["volume_weight"];
        //$order->payment_type;
        //$order->cod_amt_to_collect;

        $fullAddress = $o['address1'];
        if($o['address2'] != "") { $fullAddress .= ", ".$o['address2']; }
        $fullAddress .= ", ".$o['postcode'];
        if($o['city'] != "") { $fullAddress .= " " . $o['city']; }
        if($o['state_name'] != "") { $fullAddress .= ", ".$o['state_name']; }
        if($o['country'] != "") { $fullAddress .= ", ".$o['country']; }

        $order->consignee_name = $o['lastname']. " " .$o['firstname'];
        $order->consignee_address = $fullAddress;
        $order->consignee_postal = $o['postcode'];
        $order->consignee_country = $o['country'];
        $order->consignee_city = $o['city'];
        $order->consignee_state = $o['state_name'];
        $order->consignee_province = "";
        $order->consignee_number = "+".$this->formatPhone(trim($o['phone_mobile'] == "" ? $o['phone'] : $o['phone_mobile'], "+"), $o['call_prefix'], '');
        $order->consignee_email = $o['email'];

        $order->pickup_notes = $o['delivery_note'];
        $order->pickup_country = $janioConfig["BM_JANIO_PICKUP_COUNTRY"];
        $order->pickup_contact_name = $janioConfig["BM_JANIO_PICKUP_CONTACT_NAME"];
        $order->pickup_contact_number = $janioConfig["BM_JANIO_PICKUP_CONTACT_NUMBER"];
        $order->pickup_state = $janioConfig["BM_JANIO_PICKUP_STATE"];
        $order->pickup_city = "";
        $order->pickup_province = "";
        $order->pickup_postal = $janioConfig["BM_JANIO_PICKUP_POSTAL"];
        $order->pickup_address = $janioConfig["BM_JANIO_PICKUP_ADDRESS"];

        $order->items = [];
        $sql = 
            "select A.product_id, D.name AS product_name, A.product_quantity * IFNULL(B.quantity, 1) AS product_quantity, C.reference AS product_reference,
                IFNULL(round(A.total_price_tax_incl * C.price / E.originalTotal,2),A.total_price_tax_incl) AS total_price_tax_incl
            from ". _DB_PREFIX_ ."order_detail A
            left join ". _DB_PREFIX_ ."pack B ON A.product_id = B.id_product_pack
            left join ". _DB_PREFIX_ ."product C ON  IFNULL(B.id_product_item, A.product_id) = C.id_product
            left join ". _DB_PREFIX_ ."product_lang D ON C.id_product = D.id_product AND D.id_lang = 1
            left join (
				select A.id_order_detail, SUM(C.price) AS originalTotal
                from ". _DB_PREFIX_ ."order_detail A
				inner join ". _DB_PREFIX_ ."pack B ON A.product_id = B.id_product_pack
				inner join ". _DB_PREFIX_ ."product C ON  IFNULL(B.id_product_item, A.product_id) = C.id_product
                where A.id_order=".$o["id_order"]."
                group by A.id_order_detail
            ) E ON A.id_order_detail = E.id_order_detail
            where A.id_order=".$o["id_order"];
        $order_detail = Db::getInstance()->executeS($sql);

        foreach($order_detail as $od) 
        {
            $item = new JanioOrderItem();
            $item->item_desc = $od['product_name'];
            //$item->item_category = "Others";
            $item->item_product_id = "";
            $item->item_sku = $od['product_reference'];
            $item->item_quantity = $od['product_quantity'];
            $item->item_price_value = round($od['total_price_tax_incl'],2);
            //$item->item_price_currency = "SGD";

            $order->items[] = $item;
        }

        $orderRequest->orders[] = $order;
        $post_data = json_encode($orderRequest);

        return $post_data;
    }

    private function formatPhone($phone, $prefix, $append) 
    {
	    if (strpos( $phone, $prefix ) !== 0) 
        {
            if(strlen($prefix) > 1) {
				$phone = $this->formatPhone($phone, substr ($prefix,1), $append.$prefix[0]);
            }    
            else {
				$phone = $append.$prefix.$phone;
            }
        }
		else {
			$phone = $append.$phone;
		}
			
        return $phone;
    }
}

class JanioOrderRequest 
{
    public $secret_key;
    public $blocking;
    public $orders;
}

class JanioOrder 
{
    public $service_id = 80;
    public $tracking_no;
    public $incoterm = "DDP";
    public $shipper_order_id;

    public $order_length;
    public $order_width;
    public $order_height;
    public $order_weight;
    public $payment_type = "prepaid";
    public $cod_amt_to_collect;

    public $consignee_name;
    public $consignee_address;
    public $consignee_postal;
    public $consignee_country;
    public $consignee_city;
    public $consignee_state;
    public $consignee_province;
    public $consignee_number;
    public $consignee_email;


    public $pickup_country;
    public $pickup_contact_name;
    public $pickup_contact_number;
    public $pickup_state;
    public $pickup_city;
    public $pickup_province;
    public $pickup_postal;
    public $pickup_address;
    public $pickup_notes;

    public $items;
}

class JanioOrderItem 
{
    public $item_desc;
    public $item_category = "Others";
    public $item_product_id;
    public $item_sku;
    public $item_quantity;
    public $item_price_value;
    public $item_price_currency = "SGD";
}