<?php

// ~/module/brandsmix/KerryInbound
class BrandsMixMYKerryInboundModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        
        if(isset($_REQUEST["type"])) {
            $type = $_REQUEST["type"];
            switch($type) {
                case "getOrderToProcess":
                    $orders = $this->getOrderIdsToProcess();
                    echo json_encode($orders);
                    die();
                case "prepareKLOrder":
                    if(isset($_REQUEST['batchno'])) {
                        $batchNo = $_REQUEST['batchno'];
                        $orderids = $_REQUEST['orderids'];
                        if($batchNo != "") {
                            $fileOrder = $this->prepareKLOrderList($batchNo, $orderids);

                            if($fileOrder === false) {
                                echo "ER";
                            }
                            else {
                                echo json_encode($fileOrder);
                            }
                            //echo "OK";
                        }
                        else {
                            echo "ER";
                        }
                    }
                    else {
                        echo "ER";
                    }
                    die();
                case "submittedKL":
                    $orderIds = $_REQUEST['orderids'];
                    $this->updateKLSubmittedOrderStatus($orderIds);
                    echo "OK";
                    die();
                case "processInbound":
                    $result = $this->processInboundFile();

                    if($result) {
                        echo "OK";
                    }
                    else {
                        echo "ER";
                    }
                    die();

            //    case "pdf":
            //        $this->generatePDF($_REQUEST['id_order'], "temp");
            //        die();
            
            //    case "resendToKL": //// generate the order txt file and send to KL again. assume Invoice already generated
            //        $id_orders = $_REQUEST["id_orders"];
            //        $this->resendToKL($id_orders);
            //        die();
                
            //    case "completeKLJanio":
            //        //// 5 => switch order status to infulfilment when both kerry file uploaded and janio order created       
            //        $to_order_state = Configuration::get("BM_KL_TO_STATUS");

            //        $sql = "SELECT A.order_outbound_id, A.id_order FROM ". _DB_PREFIX_ ."order_outbound A ".
            //                "INNER JOIN ". _DB_PREFIX_ ."orders B ON A.id_order = B.id_order ".
            //                "WHERE A.kerry_status=4 AND B.current_state IN (".Configuration::get("BM_KL_STATUS").")";
            //        $ordersToUpdate = Db::getInstance()->executeS($sql);
            //        $new_order_outbound_list = [];

            //        foreach($ordersToUpdate as $o) 
            //        {
            //            $new_history = new OrderHistory();
            //            $new_history->id_order = (int)$o['id_order'];
            //            $new_history->changeIdOrderState((int)$to_order_state, $o['id_order'], true);
            //            $new_history->addWithemail(true);

            //            $new_order_outbound_list[] = $o['order_outbound_id'];
            //        }

            //        if(count($new_order_outbound_list) > 0) {
            //print_r("Update ORDER_OUTBOUND");
            //print_r("<br />"); 
            //            Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=5 WHERE order_outbound_id IN (".implode(",", $new_order_outbound_list). ")");
            //        }
            //        die();
                default:
                break;
            }
        }

        die();
    }
    
    public function initContent()
    {
        
    }

    private function prepareKLOrderList($batchNo, $orderids) {
        $isDebug = false;

        if(isset($_REQUEST['isdebug'])) {
            $isDebug = true;
        }

        $processDate = date("Ymd");
        if($this->isSkipDate($processDate)) {
            echo "";
        }

        $orders = $this->getOrderToProcess($orderids);
        $order_ids = [];
        $order_outbound_list = [];
        $order_id_list = [];
        
        for ($i = 0; $i < count($orders); $i++) {
            if(!in_array($orders[$i]["id_order"], $order_ids)) {
                $order_ids[] = $orders[$i]["id_order"];
            }
        }
        
$this->DebugMessage($isDebug, "Order to process => ".count($order_ids)."<br />");
        for ($i = 0; $i < count($order_ids); $i++) {
            $id_order = $order_ids[$i];
$this->DebugMessage($isDebug, "ORDER:".$id_order."<br />");
$this->DebugMessage($isDebug, "Insert to ORDER_OUTBOUND<br />");
            //// 1 => order to process
            $sql = "INSERT INTO `". _DB_PREFIX_ ."order_outbound` (`id_order`, `date_add`, `date_upd`, `kerry_status`,`janio_status`)".
                    "VALUES(".$id_order.",NOW(),NOW(),1,0)";
            Db::getInstance()->execute($sql);
            $order_outbound_id = Db::getInstance()->Insert_ID();     
$this->DebugMessage($isDebug, "Generate Invoice PDF<br />");
    
            //// generate PDF
            $this->generatePDF($id_order, $processDate);
 
            //// 2 => generate invoice successful
            if(file_exists($this->getOutboundLocalPath().$processDate."/".Tools::brandsOrderNumber($id_order).".pdf")) {
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=2 WHERE order_outbound_id=".$order_outbound_id);
                $order_outbound_list[] = $order_outbound_id;
                $order_id_list[] = $id_order;
            }
        }

        $continueProcess = false;
        $fileOrder = [];
$this->DebugMessage($isDebug, "Step 3 =>");
        //// 3 => generate Batch File
        if(count($order_outbound_list) > 0) {
$this->DebugMessage($isDebug, "Generate Batch Flat File<br />");
            $fileOrder = $this->generateOrderBatchFile($batchNo, $orders, $processDate, $order_id_list);

            Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=3,kerry_batchId=".$batchNo." WHERE order_outbound_id IN (".implode(",", $order_outbound_list). ")");
            Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."KL_outbound` SET status=3 WHERE id_KL_outbound=".$batchNo);
            $continueProcess = true;
        }
        else {
            $continueProcess = true;
        }

$this->DebugMessage($isDebug, "Step 4 =>"); 
        //// 4 => ftp to KL
        if($continueProcess && count($order_id_list) > 0) {
$this->DebugMessage($isDebug, "FTP to Kerry Logistic<br />");

            $continueProcess = false;
            $uploadResult = $this->sftpFileToKL($processDate, $order_id_list);
            
            if($uploadResult) {
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=4 WHERE order_outbound_id IN (".implode(",", $order_outbound_list). ")");
                Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."KL_outbound` SET status=4 WHERE id_KL_outbound=".$batchNo);
                $continueProcess = true;
            }
            else {
                $continueProcess = false;
            }
        }

        if($continueProcess) 
            return $fileOrder;
        else 
            return false;
    }

    private function DebugMessage($isDebug, $message) {
        if($isDebug) {
            print_r($message);
        }
    }

    private function updateKLSubmittedOrderStatus($orderIds) {
        $to_order_state = Configuration::get("BM_KL_TO_STATUS");
        $from_order_state = Configuration::get("BM_KL_STATUS");

        $sql = "SELECT A.order_outbound_id, A.id_order FROM ". _DB_PREFIX_ ."order_outbound A ".
                "INNER JOIN ". _DB_PREFIX_ ."orders B ON A.id_order = B.id_order ".
                "WHERE A.kerry_status=4 AND B.current_state IN (".$from_order_state.") AND A.id_order IN (".$orderIds.")";
        $ordersToUpdate = Db::getInstance()->executeS($sql);
        $new_order_outbound_list = [];

        foreach($ordersToUpdate as $o) 
        {
            $new_history = new OrderHistory();
			$new_history->id_order = (int)$o['id_order'];
			$new_history->changeIdOrderState((int)$to_order_state, $o['id_order'], true);
			$new_history->addWithemail(true);

            $new_order_outbound_list[] = $o['order_outbound_id'];
        }

        if(count($new_order_outbound_list) > 0) {
            Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."order_outbound` SET kerry_status=5 WHERE order_outbound_id IN (".implode(",", $new_order_outbound_list). ")");
        }
    }

    private function getOutboundLocalPath() 
    {
        $dir = dirname(__FILE__)."/outbound/";
        return $dir;
    }

    //private function getInboundLocalPath() 
    //{
    //    $dir = dirname(__FILE__)."/inbound/";
    //    return $dir;
    //}

    //private function resendToKL($id_orders) {
    //    $processDate = date("Ymd");

    //    $sql = 
    //        "select A.id_order, A.id_customer, C.firstname, C.lastname, 
    //            D.phone, D.phone_mobile, D.firstname AS delivery_firstname, D.lastname AS delivery_lastname, D.postcode,
    //            D.address1, D.address2, D.city,  E.name AS state_name, F.name AS country,  REPLACE(IFNULL(K.message,''),'\n','. ') AS delivery_note,
    //            B.id_order_detail,  G.reference AS product_reference, M.name AS product_name, B.product_quantity * IFNULL(L.quantity,1) AS product_quantity, A.total_paid,  
    //            IFNULL(G.unity, 'PAC') AS UOM, H.box_size, H.total_weightage
    //        from ". _DB_PREFIX_ ."orders A
    //        inner join (
    //            select A.id_order, B.box_size, A.total_weightage
    //            from (
    //                select id_order, SUM(product_weight * product_quantity) AS total_weightage
    //                from ". _DB_PREFIX_ ."order_detail
    //                group by id_order
    //            ) A
    //            inner join ". _DB_PREFIX_ ."carrier_box B ON B.min_weightage <= A.total_weightage AND B.max_weightage >= A.total_weightage
    //        ) H ON A.id_order = H.id_order
    //        inner join ". _DB_PREFIX_ ."order_detail B ON A.id_order = B.id_order
    //        inner join ". _DB_PREFIX_ ."customer C ON A.id_customer = C.id_customer
    //        inner join ". _DB_PREFIX_ ."address D ON A.id_address_delivery = D.id_address
    //        left join ". _DB_PREFIX_ ."state E ON D.id_state = E.id_state
    //        inner join ". _DB_PREFIX_ ."country_lang F ON D.id_country = F.id_country AND F.id_lang = 1
    //        left join ". _DB_PREFIX_ ."pack L ON B.product_id = L.id_product_pack
    //        left join ". _DB_PREFIX_ ."product G ON IFNULL(L.id_product_item, B.product_id) = G.id_product
    //        left join ". _DB_PREFIX_ ."product_lang M ON G.id_product = M.id_product AND M.id_lang = 1
    //        left join (
    //            select id_order, MAX(kerry_status) AS kerry_status
    //            from ". _DB_PREFIX_ ."order_outbound
    //            group by id_order
    //        ) J ON A.id_order = J.id_order AND J.kerry_status >= 4
    //        left join (
    //            select id_order, MIN(id_message), message
    //            from ". _DB_PREFIX_ ."message 
    //            where private = 0
    //            group by id_order
    //        ) K ON A.id_order = K.id_order
    //        where A.id_order IN (".$id_orders.")
    //        order by A.id_order";
    //    $orders = Db::getInstance()->executeS($sql);

    //    $order_id_list = [];

    //    for ($i = 0; $i < count($orders); $i++) {
    //        $order_id_list[] = $orders[$i]['id_order'];
    //    }

    //    $uploadResult = $this->sftpFileToKL($processDate, $order_id_list);

    //    if($uploadResult) {
    //        Db::getInstance()->execute("UPDATE `". _DB_PREFIX_ ."KL_outbound` SET status=4 WHERE id_KL_outbound=".$batchNo);    
    //    }
    //}

    public function getOrderToProcess($orderids) {
        $sql = 
            "select A.id_order, A.id_customer, C.firstname, C.lastname, 
	            D.phone, D.phone_mobile, D.firstname AS delivery_firstname, D.lastname AS delivery_lastname, D.postcode,
                D.address1, D.address2, D.city,  E.name AS state_name, F.name AS country,  REPLACE(IFNULL(K.message,''),'\n','. ') AS delivery_note,
                B.id_order_detail,  G.reference AS product_reference, M.name AS product_name, B.product_quantity * IFNULL(L.quantity,1) AS product_quantity, A.total_paid,  
                IFNULL(G.unity, 'PAC') AS UOM, H.box_size, H.total_weightage
            from ". _DB_PREFIX_ ."orders A
            inner join (
	            select A.id_order, B.box_size, A.total_weightage
	            from (
		            select id_order, SUM(product_weight * product_quantity) AS total_weightage
		            from ". _DB_PREFIX_ ."order_detail
		            group by id_order
	            ) A
	            inner join ". _DB_PREFIX_ ."carrier_box B ON B.min_weightage <= A.total_weightage AND B.max_weightage >= A.total_weightage
            ) H ON A.id_order = H.id_order
            inner join ". _DB_PREFIX_ ."order_detail B ON A.id_order = B.id_order
            inner join ". _DB_PREFIX_ ."customer C ON A.id_customer = C.id_customer
            inner join ". _DB_PREFIX_ ."address D ON A.id_address_delivery = D.id_address
            left join ". _DB_PREFIX_ ."state E ON D.id_state = E.id_state
            inner join ". _DB_PREFIX_ ."country_lang F ON D.id_country = F.id_country AND F.id_lang = 1
            left join ". _DB_PREFIX_ ."pack L ON B.product_id = L.id_product_pack
            left join ". _DB_PREFIX_ ."product G ON IFNULL(L.id_product_item, B.product_id) = G.id_product
            left join ". _DB_PREFIX_ ."product_lang M ON G.id_product = M.id_product AND M.id_lang = 1
            left join (
                select id_order, MAX(kerry_status) AS kerry_status
                from ". _DB_PREFIX_ ."order_outbound
                group by id_order
            ) J ON A.id_order = J.id_order AND J.kerry_status >= 4
            left join (
				select id_order, MIN(id_message), message
				from ". _DB_PREFIX_ ."message 
				where private = 0
				group by id_order
            ) K ON A.id_order = K.id_order
            where A.current_state IN (".Configuration::get("BM_KL_STATUS").") and J.id_order IS NULL
                AND A.id_order IN (". $orderids .")
            order by A.id_order";
        $orders = Db::getInstance()->executeS($sql);

        return $orders;
    }

    public function getOrderIdsToProcess() {
        $sql = 
            "select distinct A.id_order
            from ". _DB_PREFIX_ ."orders A
            left join (
                select id_order, MAX(kerry_status) AS kerry_status
                from ". _DB_PREFIX_ ."order_outbound
                group by id_order
            ) J ON A.id_order = J.id_order AND J.kerry_status >= 4
            where A.current_state IN (".Configuration::get("BM_KL_STATUS").") and J.id_order IS NULL";
        $orders = Db::getInstance()->executeS($sql);

        $orderIds = [];
        foreach ($orders as $o) 
        {
            $orderIds[] = (int)$o["id_order"];
        }

        return $orderIds;
    }

    public function generatePDF($id_order,$processDate)
    {
        $order = new Order((int)$id_order);
        
        if (!Validate::isLoadedObject($order)) {
            return false;
            //die(Tools::displayError('The order cannot be found within your database.'));
        }

        $order_invoice_list = $order->getInvoicesCollection();
        
        $outboundDir = $this->getOutboundLocalPath();
        if(!file_exists ($outboundDir)) {
            mkdir($outboundDir, 0777, true);
        }

        $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, Context::getContext()->smarty);
        $exportDir = $outboundDir.$processDate."/";

        if(!file_exists ($exportDir)) {
            mkdir($exportDir, 0777, true);
        }

        $pdf->render("F", $exportDir, Tools::brandsOrderNumber($id_order).".pdf");

        return true;
    }

    public function generateOrderBatchFile($batchNo, $orders, $processDate, $order_id_list) 
    {
        $fileOrder = [];
        if(count($orders) > 0) 
        {
            $curDate = date('Y-m-d H:i:s');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'KL_outbound` (`id_KL_outbound`,`date_add`, `date_upd`, `status`) '.
                'VALUES ('.$batchNo.',"'.$curDate.'","'.$curDate.'", 1)');
            
            $outboundDir = $this->getOutboundLocalPath();
            if(!file_exists ($outboundDir)) {
                mkdir($outboundDir, 0777, true);
            }

            $batchFileName = "SALEORDER_".$processDate."_".$batchNo.".TXT";
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'KL_outbound` SET batchFileName="'.$batchFileName.'" WHERE id_KL_outbound='.$batchNo);
            
            $itemNo = 1;
            $curOrderId = 0;

            foreach ($orders as $o) 
            {
                if(in_array($o["id_order"],$order_id_list)) 
                {
                    $txt = "1|";
                    $txt .= "|"; //Customer number
                    $txt .= Tools::brandsOrderNumber($o['id_order'])."|";
                    $txt .= $o['id_customer']."|";
                
                    $txt .= "|"; //Shop category
                    $txt .= "|"; //Retail store code
                    $txt .= "|"; //Store name
                    $txt .= "|"; //Specify delivery negotiates
                    $txt .= "6|"; //Service categories
                

                    $txt .= $o['lastname']. " " .$o['firstname']."|";
                    $txt .= $o['phone']."|";
                    $txt .= $o['phone_mobile']."|";
                    $txt .= $o['delivery_lastname']. " " .$o['delivery_firstname']."|";
                    $txt .= $o['postcode']."|";

                    $fullAddress = $o['address1'];
                    if($o['address2'] != "") { $fullAddress .= ", ".$o['address2']; }
                    $fullAddress .= ", ".$o['postcode'];
                    if($o['city'] != "") { $fullAddress .= " " . $o['city']; }
                    if($o['state_name'] != "") { $fullAddress .= ", ".$o['state_name']; }
                    if($o['country'] != "") { $fullAddress .= ", ".$o['country']; }
                
                    $txt .= $fullAddress."|";
                    $txt .= $o['delivery_note']."|";

                    if($o['phone'] != "") { $txt .= $o['phone']."|";}else {$txt .= $o['phone_mobile']."|";}
                    if($o['phone_mobile'] != "") { $txt .= $o['phone_mobile']."|";}else {$txt .= $o['phone']."|";}

                    if($curOrderId != $o['id_order']) {
                        $curOrderId = $o['id_order'];
                        $itemNo = 1;
                    }
                    else {
                        $itemNo = $itemNo + 1;
                    }

                    $txt .= $itemNo."|";
                    $txt .= "Singapore food supplement|"; // Customer name on the delivery order
                    $txt .= "https://phstore.brandsworld.com.sg|"; //Delivery on the customer Web sit
                    $txt .= $o['product_reference']."|";
                    $txt .= $o['product_name']."|";
                    $txt .= $o['product_quantity']."|";
                    $txt .= $batchFileName."|";
                    $txt .= "|"; //Effective duration
                    $txt .= round($o['total_paid'],2)."|";
                    $txt .= "E-commerce - NONE|"; //Second part number
                    $txt .= $o['UOM']."|";
                    $txt .= "0|"; //Collected amount
                    $txt .= "Y|"; //Print invoices
                    $txt .= "|"; //Delivery times
                    $txt .= "|"; //A specified time period
                    $txt .= "|"; //ARS Shipping information
                    $txt .= "|"; //Invoice Number
                    $txt .= "|"; //Invoice date
                    $txt .= "|"; //Random Code
                    $txt .= $o['box_size']."|";
                    $txt .= "\r\n";

                    $fileOrder[] = $txt;
                }
            }
        }

        return $fileOrder;
    }

    public function sftpFileToKL ($processDate, $order_id_list) 
    {
        $uploadResult = false;
        $uploadRetry = 0;
        $maxRetry = 5;

        $ftp_server = Configuration::get('BM_KL_HOST');
        $username = Configuration::get('BM_KL_USER');
        $password = Configuration::get('BM_KL_PASS');
        $ftpdirDSO = Configuration::get('BM_KL_DIR_DSO');
        $ftpdirInv = Configuration::get('BM_KL_DIR_INVOICE');
        
        $conn = ssh2_connect($ftp_server) or die("Couldn't connect to $ftp_server"); 

        //print_r("<br />Connected");
        // Login
        if (ssh2_auth_password($conn, $username, $password))
        {
            // Create SFTP session
            $sftp = ssh2_sftp($conn);
            $sftp_fd = intval($sftp);
            ////ftp_pasv($conn, true);
            //print_r("<br />Login success");
            //print_r("<br />");

            $outboundDir = $this->getOutboundLocalPath();
            if(!file_exists ($outboundDir)) {
                mkdir($outboundDir, 0777, true);
            }
            
            for($i = 0; $i < count($order_id_list); $i++) 
            {
                $uploadRetry = 0;
                $uploadResult = false;

                $fileName = Tools::brandsOrderNumber($order_id_list[$i]).".pdf";
                //print_r("Move ".$fileName. " =>");
                $dir = "ssh2.sftp://$sftp_fd".$ftpdirInv;

                while(!$uploadResult && $uploadRetry < $maxRetry) 
                {
                    $sftpStream = @fopen($dir.$fileName, 'w');
                    if (!$sftpStream) { 
                        //print_r("Could not open remote file: $fileName.<br />");  
                        $uploadResult = false;
                        $uploadRetry++; 
                        continue; 
                    }
   
                    $data_to_send = @file_get_contents($outboundDir.$processDate."/".$fileName);
                    if ($data_to_send === false) { 
                        //print_r("Could not open local file: $fileName.<br />"); 
                        $uploadResult = false;
                        $uploadRetry++; 
                        fclose($sftpStream); 
                        continue; 
                    }
                    
                    if (@fwrite($sftpStream, $data_to_send) === false) {
                        //print_r("Could not send data from file: $fileName.<br />");
                        $uploadRetry++; 
                        fclose($sftpStream); 
                        $uploadResult = false;
                        continue; 
                    }
   
                    fclose($sftpStream);
                    $uploadResult = true;

                    
                    $uploadRetry++;

                    if(!$uploadResult && $uploadRetry < $maxRetry) {
                        sleep(5);
                    }
                }
            }
        }
        
        $conn = null; 
        unset($conn);
        
        return $uploadResult;
    }

    public function processInboundFile() 
    {
        $to_process_order_state = Configuration::get("BM_KL_TO_STATUS");
        $shipped_order_state = Configuration::get("BM_KL_SHIP_STATUS");
        $oos_order_state = Configuration::get("BM_KL_OOS_STATUS");
        
        $id_order = 0;
        if(isset($_REQUEST['orderNo'])) {
            $orderNo = $_REQUEST['orderNo'];
            $id_order = Tools::storeOrderId($orderNo);
        }

        $shipStatus = $_REQUEST['shipStatus'];

        if($id_order > 0) 
        {
            $order = new Order($id_order);
            $updateToState = 0; 
            $reverseAuthorize = false;
            $templateVars = array();

            if($shipStatus == "1") 
            {
                $updateToState = $shipped_order_state;
                $shipping_number = $order->shipping_number;

                if(!$shipping_number || $shipping_number == "") {
                    $shipping_number = Configuration::get('BM_JANIO_TRACKING_PREFIX').Tools::brandsOrderNumber($order->id);
                }

                $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);

                $templateVars = array(
                    '{followup}' => str_replace('@', $shipping_number, $carrier->url),
                    '{shipping_number}' => $shipping_number
                );
            }
            else if($shipStatus == "9" || $shipStatus == "E" || $shipStatus == "C") {
                $updateToState = $oos_order_state;
                $reverseAuthorize = true;
            }
                                
            if($updateToState != 0 && $order->current_state != $updateToState && $order->current_state != 5 && $order->current_state != 15) 
            {
                $updateOrder = true;

                if($reverseAuthorize) 
                {
                    $oos_reverse = Configuration::get('BM_KL_OOS_REVERSE');
                    $paymentMethod = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT module FROM `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order);
                    if($oos_reverse == "1" && $paymentMethod == "cybersource") 
                    {
                        if (!class_exists('cybersource')) {
                            include(dirname(__FILE__).'/../../../cybersource/cybersource.php');
                        }

                        $obj = new cybersource();
                        $result = $obj->ajaxCall("reverse", $id_order, 0, $updateToState);
                        $result = json_decode($result, true);

                        if($result['result']) {
                            $updateOrder = false;
                        }
                    }                                        
                }
                                    
                if($updateOrder) 
                {
                    $new_history = new OrderHistory();
                    $new_history->id_order = (int)$id_order;
                    $new_history->changeIdOrderState($updateToState, $id_order, true, true);
                    $new_history->addWithemail(true, $templateVars);
                }
            }
        }

        return true;
    }

    public function isSkipDate($processDate) {
        //// check today is holiday
        $sql = "SELECT COUNT(1) FROM ". _DB_PREFIX_ ."holiday WHERE active=1 AND holiday_date='".$processDate."'";
        $isHoliday = Db::getInstance()->getValue($sql);

        if($isHoliday > 0)
            return true;

        //// check today is weekend
        $dayOfWeek = date('w', strtotime($processDate));

        if($dayOfWeek == 0 || $dayOfWeek == 6) {
            return true;
        }

        return false;
    }
}