<?php

// ~/module/notificationjob/productstock
class BrandsMixAccountVerificationModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        if (Tools::isSubmit('email')) {
            if (!($email = trim(Tools::getValue('email'))) || !Validate::isEmail($email)) {
                $this->errors[] = Tools::displayError('Invalid email address.');
            } else {
                $customer = new Customer();
                $customer->getByemail($email);
                if (!Validate::isLoadedObject($customer)) {
                    $this->errors[] = Tools::displayError('There is no account registered for this email address.');
                } elseif (!$customer->active) {
                    $this->errors[] = Tools::displayError('Account is inactive Unable resend verification email.');
                } elseif ($customer->verified) {
                    $this->errors[] = Tools::displayError('This account had verified.');
                }else {
                    $mail_params = array(
                        '{email}' => $customer->email,
                        '{lastname}' => $customer->lastname,
                        '{firstname}' => $customer->firstname,
                        '{verifyurl}' => $this->context->link->getBaseLink().'login?verify='.$customer->verifyKey
                    );
                     
                    if (Mail::Send($this->context->language->id, 'account', Mail::l('Your verification!'), $mail_params, $customer->email, $customer->firstname.' '.$customer->lastname)) {
                        $this->context->smarty->assign(array('confirmation' => 2, 'customer_email' => $customer->email));
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred while sending the email.');
                    }
                }
            }
        }
    }
    
    public function initContent()
    {
        parent::initContent();
        $this->setTemplate('verification.tpl');
    }
}