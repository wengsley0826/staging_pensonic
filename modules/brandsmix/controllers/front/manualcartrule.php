<?php

// ~/module/notificationjob/productstock
class BrandsMixManualCartRuleModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        
    }
    
    public function initContent()
    {
    ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        print_r("Start");
        print_r("<br />");
        $sql =  'SELECT id_manual,cart_rule_code FROM `'. _DB_PREFIX_ .'manual_cart_rule` A '.
                'LEFT JOIN `'. _DB_PREFIX_ .'cart_rule` B ON A.cart_rule_code = B.code '.
                'WHERE B.id_cart_rule IS NULL LIMIT 5000';
        $manualcartrule = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        $counter = 1;

        foreach ($manualcartrule as $cr) {
            print_r($counter. " Create Cart Rule - ".$cr['cart_rule_code']);
            print_r("<br />");

            $cartRule = new CartRule();
		    $cartRule->id_customer = 0;
		    $cartRule->date_from = '2019-12-15 00:00:00'; /* remove 1s because of a strict comparison between dates in getCustomerCartRules */
		    $cartRule->date_to = '2020-06-30 23:59:00'; // 30 days expiry date
		    $cartRule->description = "";
		    $cartRule->quantity = 1;
		    $cartRule->quantity_per_user = 1;
		    $cartRule->partial_use = 0;
		    $cartRule->highlight = 0;
		    $cartRule->code = $cr['cart_rule_code'];
		    $cartRule->active = 1;
		    $cartRule->reduction_amount = 38;
		    $cartRule->reduction_tax = 1;
		    $cartRule->reduction_currency = 1;
		    $cartRule->minimum_amount = 98;
		    $cartRule->minimum_amount_tax = 1;
		    $cartRule->minimum_amount_currency = 1;
		    $cartRule->minimum_amount_shipping = 0;
		    $cartRule->cart_rule_restriction = 1;

		    $languages = Language::getLanguages(true);
		    foreach ($languages AS $language)
		    {
			    $cartRule->name[(int)($language['id_lang'])] = "CNY Voucher";
		    }

		    $cartRule->add();

            print_r("Completed Cart Rule - ".$cr['cart_rule_code']);
            print_r("<br />");
            print_r("<br />");
            $counter++;
        }

        die();
    }
}