<?php

class BulkVoucher extends Module
{
    protected $_html = '';
    protected $_postErrors = array();

    public function __construct()
    {
        if (!defined('_PS_VERSION_')) {
            exit;
        }
        $this->name = 'bulkvoucher';
        $this->tab = 'bulkvoucher';
        $this->version = '1.0.0';
        $this->author = 'Claritas';
        $this->displayName = 'Bulk Voucher';
        $this->bootstrap = true;
        $this->display = 'view';
        $this->is_eu_compatible = 1;
        
        parent::__construct();

        $this->description = $this->l('Bulk Voucher Generation');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

    }

    public function install()
    {
        return parent::install();
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('BULK_VOUCHER_PREFIX_CODE')) {
                $this->_postErrors[] = $this->l('Bulk promocode Prefix Code is required!');
            }
            if (!Tools::getValue('BULK_VOUCHER_GENERATE_AMOUNT')) {
                $this->_postErrors[] = $this->l('Promocode quantity is required!');
            }
            
            if (!Tools::getValue('BULK_VOUCHER_CART_RULE_ID')) {
                $this->_postErrors[] = $this->l('Promocode template is required!');
            }
            else if (Tools::getValue('BULK_VOUCHER_CART_RULE_ID') == 0) {
                $this->_postErrors[] = $this->l('Promocode template is required!');
            }
            else {
                $templateId = Tools::getValue('BULK_VOUCHER_CART_RULE_ID');

                $cartRule = new CartRule($templateId);   
                if(!$cartRule) {
                    $this->_postErrors[] = $this->l('Invalid Promocode Template!');
                }
            }
        }
	}
    
    protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('BULK_VOUCHER_PREFIX_CODE', '');
            Configuration::updateValue('BULK_VOUCHER_GENERATE_AMOUNT', 10);
            Configuration::updateValue('BULK_VOUCHER_CART_RULE_ID', 0);

            //// generate voucher here
            $prefix = Tools::getValue('BULK_VOUCHER_PREFIX_CODE');
            $quantity = Tools::getValue('BULK_VOUCHER_GENERATE_AMOUNT');
            $templateId = Tools::getValue('BULK_VOUCHER_CART_RULE_ID');

            $this->bulkGenerateVoucher($prefix, $quantity, $templateId);
        }
        
        $this->_html .= $this->displayConfirmation($this->l('Vouchers generated'));
	}
    
    public function getContent()
    {
       if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
    }

    public function renderForm($message = null)
    {
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Bulk Promocode generation'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Promocode Prefix'),
                    'name' => 'BULK_VOUCHER_PREFIX_CODE',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Promocode generate quantity'),
                    'name' => 'BULK_VOUCHER_GENERATE_AMOUNT',
                    'required' => true
                ),
                array(
                    'type' => 'select',
					'label' => $this->l('Template Promocode'),
					'name' => 'BULK_VOUCHER_CART_RULE_ID',
					'options' => array(
						'query' => $this->getPromocodeList(),
						'id' => 'id_cart_rule',
						'name' => 'name',
						'default' => array(
							'label' => $this->l('No Template'),
							'value' => 0
						)
					)
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );
        

        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm($fields_form);
    }
    
    protected function getPromocodeList() {
        $alltemplates = Db::getInstance()->ExecuteS('SELECT DISTINCT A.id_cart_rule, B.name AS template_name'.
                    ' FROM '._DB_PREFIX_.'cart_rule A'.
                    ' INNER JOIN '._DB_PREFIX_.'cart_rule_lang B ON A.id_cart_rule = B.id_cart_rule'.
                    ' WHERE A.template=1 AND B.id_lang=1'.
                    ' ORDER BY B.name'
		);
        
        $template_list = array();
        
        foreach ($alltemplates as $t)
        {
            $template_list[] = array(
                "id_cart_rule" => $t["id_cart_rule"],
                "name" => $t["template_name"]
            );
        }
        
        return $template_list;
    }
    
    public function getConfigFieldsValues()
	{
		return array(
			'BULK_VOUCHER_PREFIX_CODE' => Tools::getValue('BULK_VOUCHER_PREFIX_CODE', Configuration::get('BULK_VOUCHER_PREFIX_CODE')),
            'BULK_VOUCHER_GENERATE_AMOUNT' => Tools::getValue('BULK_VOUCHER_GENERATE_AMOUNT', Configuration::get('BULK_VOUCHER_GENERATE_AMOUNT')),
            'BULK_VOUCHER_CART_RULE_ID' => Tools::getValue('BULK_VOUCHER_CART_RULE_ID', Configuration::get('BULK_VOUCHER_CART_RULE_ID')),
		);
	}

    private function bulkGenerateVoucher($prefix, $quantity, $templateId) {
        $ccount=1;
        while ($ccount <= $quantity){
		    /* Generate a discount code */
		    $code = NULL;
		    do 
            {
                $code = Tools::passwdGen(5); 
			    $code=str_replace('i','x',$code); 
			    $code=str_replace('l','x',$code); 
			    $code=str_replace('0','x',$code); 
			    $code=$prefix.$code;
		    }
            while (CartRule::cartRuleExists($code) || (strpos($code, 'i') !== false) || (strpos($code, 'l') !== false) || (strpos($code, '0') !== false));

		    /* Voucher creation and affectation to the customer */
		    $cartRule = new CartRule($templateId);
		    $cartRule->id=0;
		    //$cartRule->id_customer = (int)$newCustomer->id;
		    //$cartRule->date_from = "2018-03-26 00:00:00"; /* remove 1s because of a strict comparison between dates in getCustomerCartRules */
		    //$cartRule->date_to = "2018-12-31 23:59:59"; // 30 days expiry date
		    //$cartRule->description = $cartRule->description." - ".$code;
		    //$cartRule->quantity = 1;
		    //$cartRule->quantity_per_user = 1;
		    //$cartRule->highlight = 0;
		    //$cartRule->partial_use = 0;
		    $cartRule->code = $code;
		    $cartRule->active = 1;
            $cartRule->template = 0;
		    //$cartRule->reduction_percent=10;
		    //$cartRule->reduction_cap=30;
		    //$cartRule->reduction_amount = 20;
		    //$cartRule->reduction_tax = 1;
		    //$cartRule->reduction_currency = 1;
		    //$cartRule->reduction_product = -2;
		    //$cartRule->gift_product = 22301;
		    //$cartRule->minimum_amount = 0;
		    //$cartRule->minimum_amount_tax = 1;
		    //$cartRule->minimum_amount_currency = 1;
		    //$cartRule->minimum_amount_shipping = 0;
		    //$cartRule->cart_rule_restriction = 0;

		    //$cartRule->product_restriction = 1;

		    //$languages = Language::getLanguages(true);

    //		foreach ($languages AS $language)
    //		{
    //			$cartRule->name[(int)($language['id_lang'])] = $code;
    //		}

		    $cartRule->add();

		    DB::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart_rule_product_rule_group WHERE id_cart_rule='.$cartRule->id);
		
		    // need copy to copy product selection rule
		    CartRule::copyConditions($templateId ,$cartRule->id);
		    //DB::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart_rule_category WHERE id_cart_rule='.$cartRule->id);
		    //DB::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart_rule_product_rule_group WHERE id_cart_rule='.$cartRule->id);

		    $ccount++;
        }
    }
}
