<?php

class SectionSlide extends ObjectModel
{
	public $title;
	public $description;
	public $url;
	public $legend;
	public $image;
	public $hdr_image;
	public $active;
	public $position;
	public $start_datetime;
	public $end_datetime;
	public $id_shop;
    public $product_count;
    public $item_page_count;
    public $item_row_count;
    public $item_column_count;
    public $section_type;
    public $section_value;
    public $bgColor;
    public $show_title;
    public $addon_class;
    public $mobile_image;
    public $grouping;
	public $layout_type;
	public $show_timer;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'sectionslider_slides',
		'primary' => 'id_sectionslider_slides',
		'multilang' => true,
		'fields' => array(
			'active' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'position' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
			'start_datetime' =>	array('type' => self::TYPE_DATE),
			'end_datetime' =>	array('type' => self::TYPE_DATE),
            'section_type' =>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'section_value' =>	array('type' => self::TYPE_INT),
            'product_count' =>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'item_page_count'=> array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'item_row_count'=>  array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'item_column_count'=> array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'bgColor' =>		array('type' => self::TYPE_STRING, 'size' => 7),
            'show_title' =>		array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'addon_class' =>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 100),
			'show_timer' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			
			// Lang fields
			'description' =>	array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 4000),
			'title' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
			//'legend' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
			'url' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isUrl', 'size' => 255),
			'image' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 255),
            'hdr_image' =>		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 255),
            'mobile_image' =>	array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 255),
            'grouping' =>	    array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 100),
            'layout_type' =>	array('type' => self::TYPE_STRING, 'size' => 20),
		)
	);

	public	function __construct($id_slide = null, $id_lang = null, $id_shop = null, Context $context = null)
	{
		parent::__construct($id_slide, $id_lang, $id_shop);
	}

	public function add($autodate = true, $null_values = false)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$res = parent::add($autodate, $null_values);
		$res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'sectionslider` (`id_shop`, `id_sectionslider_slides`)
			VALUES('.(int)$id_shop.', '.(int)$this->id.')'
		);
		return $res;
	}

	public function delete()
	{
		$res = true;

		$images = $this->image;
		foreach ($images as $image)
		{
			if (preg_match('/sample/', $image) === 0)
				if ($image && file_exists(dirname(__FILE__).'/images/'.$image))
					$res &= @unlink(dirname(__FILE__).'/images/'.$image);
		}

        $hdr_images = $this->hdr_image;
		foreach ($hdr_images as $hdrimage)
		{
			if (preg_match('/sample/', $hdrimage) === 0)
				if ($hdrimage && file_exists(dirname(__FILE__).'/images/header/'.$hdrimage))
					$res &= @unlink(dirname(__FILE__).'/images/header/'.$hdrimage);
		}
        
        $mobile_images = $this->mobile_image;
		foreach ($mobile_images as $mobileimage)
		{
			if (preg_match('/sample/', $mobileimage) === 0)
				if ($mobileimage && file_exists(dirname(__FILE__).'/images/m/'.$mobileimage))
					$res &= @unlink(dirname(__FILE__).'/images/m/'.$mobileimage);
		}
        
		$res &= $this->reOrderPositions();

		$res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'sectionslider`
			WHERE `id_sectionslider_slides` = '.(int)$this->id
		);

		$res &= parent::delete();
		return $res;
	}

	public function reOrderPositions()
	{
		$id_slide = $this->id;
		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$max = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT MAX(hss.`position`) as position
			FROM `'._DB_PREFIX_.'sectionslider_slides` hss, `'._DB_PREFIX_.'sectionslider` hs
			WHERE hss.`id_sectionslider_slides` = hs.`id_sectionslider_slides` AND hs.`id_shop` = '.(int)$id_shop
		);

		if ((int)$max == (int)$id_slide)
			return true;

		$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hss.`position` as position, hss.`id_sectionslider_slides` as id_slide
			FROM `'._DB_PREFIX_.'sectionslider_slides` hss
			LEFT JOIN `'._DB_PREFIX_.'sectionslider` hs ON (hss.`id_sectionslider_slides` = hs.`id_sectionslider_slides`)
			WHERE hs.`id_shop` = '.(int)$id_shop.' AND hss.`position` > '.(int)$this->position
		);

		foreach ($rows as $row)
		{
			$current_slide = new SectionSlide($row['id_slide']);
			--$current_slide->position;
			$current_slide->update();
			unset($current_slide);
		}

		return true;
	}

	public static function getAssociatedIdsShop($id_slide)
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_shop`
			FROM `'._DB_PREFIX_.'sectionslider` hs
			WHERE hs.`id_sectionslider_slides` = '.(int)$id_slide
		);

		if (!is_array($result))
			return false;

		$return = array();

		foreach ($result as $id_shop)
			$return[] = (int)$id_shop['id_shop'];

		return $return;
	}

}
