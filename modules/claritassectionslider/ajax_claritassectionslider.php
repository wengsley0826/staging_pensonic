<?php
include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('claritassectionslider.php');

$section_slider = new claritassectionslider();
$slides = array();

if (!Tools::isSubmit('secure_key') || Tools::getValue('secure_key') != $section_slider->secure_key || !Tools::getValue('action'))
	die(1);

if (Tools::getValue('action') == 'updateSlidesPosition' && Tools::getValue('slides'))
{
	$slides = Tools::getValue('slides');

	foreach ($slides as $position => $id_slide)
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'sectionslider_slides` SET `position` = '.(int)$position.'
			WHERE `id_sectionslider_slides` = '.(int)$id_slide
		);

	$section_slider->clearCache();
}
