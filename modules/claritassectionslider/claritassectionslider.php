<?php
if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'claritassectionslider/SectionSlide.php');

class claritassectionslider extends Module
{
	protected $_html = '';
    protected $default_speed = 500;
	protected $default_pause = 3000;
	protected $default_loop = 1;
    protected $cur_group_filter = null;
	
	public function __construct()
	{
		$this->name = 'claritassectionslider';
		$this->tab = 'front_office_features';
		$this->version = '1.6.1';
		$this->author = 'Claritas';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Section slider for your homepage');
		$this->description = $this->l('Adds section slider to your homepage.');
		$this->ps_versions_compliancy = array('min' => '1.6.0.4', 'max' => '1.6.99.99');
	}

	/**
	 * @see Module::install()
	 */
	public function install()
	{
		/* Adds Module */
		if (parent::install() &&
			$this->registerHook('displayHeader') &&
			$this->registerHook('displayTop') && 
            $this->registerHook('displayBackOfficeHeader')
		)
		{
			/* Sets up Global configuration */
			$res = Configuration::updateValue('SECTIONSLIDE_SPEED', $this->default_speed);
			$res &= Configuration::updateValue('SECTIONSLIDE_PAUSE', $this->default_pause);
			$res &= Configuration::updateValue('SECTIONSLIDE_LOOP', $this->default_loop);

			/* Creates tables */
			$res &= $this->createTables();

			return (bool)$res;
		}

		return false;
	}

	/**
	 * @see Module::uninstall()
	 */
	public function uninstall()
	{
		/* Deletes Module */
		if (parent::uninstall())
		{
			///* Deletes tables */
			//$res = $this->deleteTables();

			///* Unsets configuration */
			//$res &= Configuration::deleteByName('SECTIONSLIDE_SPEED');
			//$res &= Configuration::deleteByName('SECTIONSLIDE_PAUSE');
			//$res &= Configuration::deleteByName('SECTIONSLIDE_LOOP');

			//return (bool)$res;
            return true;
		}

		return false;
	}

	/**
	 * Creates tables
	 */
	protected function createTables()
	{
		/* Slides */
		$res = (bool)Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sectionslider` (
				`id_sectionslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_shop` int(10) unsigned NOT NULL,
				PRIMARY KEY (`id_sectionslider_slides`, `id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');

		/* Slides configuration */
		$res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sectionslider_slides` (
			  `id_sectionslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `position` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
              `start_datetime` datetime NULL,
              `end_datetime` datetime NULL,
			  `section_type` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
              `section_value` INT(10) unsigned NULL,
              `product_count` int(10) unsigned NOT NULL,
              `item_page_count` int(10) unsigned NOT NULL DEFAULT \'4\',
              `bgColor` varchar(7) DEFAULT \'#fff\',
              `show_title` tinyint(1) DEFAULT \'0\',
              `addon_class` varchar(100) DEFAULT NULL,
              `grouping` varchar(100) DEFAULT \'front-page\',
			  `layout_type` varchar(10) DEFAULT \'slide\',
			  `show_timer` tinyint(1) DEFAULT \'1\',
              `item_row_count` int(10) unsigned NOT NULL DEFAULT \'1\',
              `item_column_count` int(10) unsigned NOT NULL DEFAULT \'4\',
              PRIMARY KEY (`id_sectionslider_slides`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');

		/* Slides lang configuration */
		$res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sectionslider_slides_lang` (
			  `id_sectionslider_slides` int(10) unsigned NOT NULL,
			  `id_lang` int(10) unsigned NOT NULL,
			  `title` varchar(255) NOT NULL,
			  `description` text NOT NULL,
			  `legend` varchar(255) NOT NULL,
			  `url` varchar(255) NOT NULL,
			  `image` varchar(255) NOT NULL,
			  `hdr_image` varchar(255) NOT NULL,
              `mobile_image` varchar(255) DEFAULT NULL,
			  PRIMARY KEY (`id_sectionslider_slides`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');

		return $res;
	}

	/**
	 * deletes tables
	 */
	protected function deleteTables()
	{
		$slides = $this->getSlides();
		foreach ($slides as $slide)
		{
			$to_del = new SectionSlide($slide['id_slide']);
			$to_del->delete();
		}

		return Db::getInstance()->execute('
			DROP TABLE IF EXISTS `'._DB_PREFIX_.'sectionslider`, `'._DB_PREFIX_.'sectionslider_slides`, `'._DB_PREFIX_.'sectionslider_slides_lang`;
		');
	}

	public function getContent()
	{
		$this->_html .= $this->headerHTML();

		/* Validate & process */
		if (Tools::isSubmit('submitSlide') || Tools::isSubmit('delete_id_slide') ||
			Tools::isSubmit('submitSlider') ||
			Tools::isSubmit('changeStatus')
		)
		{
			if ($this->_postValidation())
			{
				$this->_postProcess();
				$this->_html .= $this->renderForm();
				$this->_html .= "<div id='sliderList'>".$this->renderList($this->cur_group_filter)."</div>";
			}
			else
				$this->_html .= $this->renderAddForm();

			$this->clearCache();
		}
		elseif (Tools::isSubmit('addSlide') || (Tools::isSubmit('id_slide') && $this->slideExists((int)Tools::getValue('id_slide'))))
		{
			if (Tools::isSubmit('addSlide'))
				$mode = 'add';
			else
				$mode = 'edit';

			if ($mode == 'add')
			{
				if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL)
					$this->_html .= $this->renderAddForm();
				else
					$this->_html .= $this->getShopContextError(null, $mode);
			}
			else
			{
				$associated_shop_ids = SectionSlide::getAssociatedIdsShop((int)Tools::getValue('id_slide'));
				$context_shop_id = (int)Shop::getContextShopID();

				if ($associated_shop_ids === false)
					$this->_html .= $this->getShopAssociationError((int)Tools::getValue('id_slide'));
				else if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL && in_array($context_shop_id, $associated_shop_ids))
				{
					if (count($associated_shop_ids) > 1)
						$this->_html = $this->getSharedSlideWarning();
					$this->_html .= $this->renderAddForm();
				}
				else
				{
					$shops_name_list = array();
					foreach ($associated_shop_ids as $shop_id)
					{
						$associated_shop = new Shop((int)$shop_id);
						$shops_name_list[] = $associated_shop->name;
					}
					$this->_html .= $this->getShopContextError($shops_name_list, $mode);
				}
			}
		}
		else // Default viewport
		{
			$this->_html .= $this->getWarningMultishopHtml().$this->getCurrentShopInfoMsg().$this->renderForm();

			if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL)
				$this->_html .= "<div id='sliderList'>".$this->renderList()."</div>";
		}

		return $this->_html;
	}

	protected function _postValidation()
	{
		$errors = array();

		/* Validation for Slider configuration */
		if (Tools::isSubmit('submitSlider'))
		{
			if (!Validate::isInt(Tools::getValue('SECTIONSLIDE_SPEED')) || !Validate::isInt(Tools::getValue('SECTIONSLIDE_PAUSE')))
				$errors[] = $this->l('Invalid values');
		} 
        /* Validation for status */
		elseif (Tools::isSubmit('changeStatus'))
		{
			if (!Validate::isInt(Tools::getValue('id_slide')))
				$errors[] = $this->l('Invalid slide');
		}
		/* Validation for Slide */
		elseif (Tools::isSubmit('submitSlide'))
		{
			/* Checks state (active) */
			if (!Validate::isInt(Tools::getValue('active_slide')) || (Tools::getValue('active_slide') != 0 && Tools::getValue('active_slide') != 1))
				$errors[] = $this->l('Invalid slide state.');
			/* Checks position */
			if (!Validate::isInt(Tools::getValue('position')) || (Tools::getValue('position') < 0))
				$errors[] = $this->l('Invalid slide position.');
			/* If edit : checks id_slide */
			if (Tools::isSubmit('id_slide'))
			{
				//d(var_dump(Tools::getValue('id_slide')));
				if (!Validate::isInt(Tools::getValue('id_slide')) && !$this->slideExists(Tools::getValue('id_slide')))
					$errors[] = $this->l('Invalid slide ID');
			}
			/* Checks title/url/legend/description/image */
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				if (Tools::strlen(Tools::getValue('title_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('The title is too long.');
				if (Tools::strlen(Tools::getValue('url_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('The URL is too long.');
				if (Tools::strlen(Tools::getValue('description_'.$language['id_lang'])) > 4000)
					$errors[] = $this->l('The description is too long.');
				if (Tools::strlen(Tools::getValue('url_'.$language['id_lang'])) > 0 && !Validate::isUrl(Tools::getValue('url_'.$language['id_lang'])))
					$errors[] = $this->l('The URL format is not correct.');
				if (Tools::getValue('image_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('image_'.$language['id_lang'])))
					$errors[] = $this->l('Invalid filename.');
				if (Tools::getValue('image_old_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('image_old_'.$language['id_lang'])))
					$errors[] = $this->l('Invalid filename.');
				if (Tools::getValue('hdr_image_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('hdr_image_'.$language['id_lang'])))
					$errors[] = $this->l('Invalid header filename.');
				if (Tools::getValue('hdr_image_old_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('hdr_image_old_'.$language['id_lang'])))
					$errors[] = $this->l('Invalid header filename.');
                    
                if (Tools::getValue('mobile_image_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('mobile_image_'.$language['id_lang'])))
					$errors[] = $this->l('Invalid mobile filename.');
				if (Tools::getValue('mobile_image_old_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('mobile_image_old_'.$language['id_lang'])))
					$errors[] = $this->l('Invalid mobile filename.');
			}

			/* Checks title/url/legend/description for default lang */
			$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
			if (Tools::strlen(Tools::getValue('title_'.$id_lang_default)) == 0)
				$errors[] = $this->l('The title is not set.');
			//if (Tools::strlen(Tools::getValue('url_'.$id_lang_default)) == 0)
			//	$errors[] = $this->l('The URL is not set.');
			//if (!Tools::isSubmit('has_picture') && (!isset($_FILES['image_'.$id_lang_default]) || empty($_FILES['image_'.$id_lang_default]['tmp_name'])))
			//	$errors[] = $this->l('The image is not set.');
			if (Tools::getValue('image_old_'.$id_lang_default) && !Validate::isFileName(Tools::getValue('image_old_'.$id_lang_default)))
				$errors[] = $this->l('The image is not set.');
            if (Tools::getValue('hdr_image_old_'.$id_lang_default) && !Validate::isFileName(Tools::getValue('hdr_image_old_'.$id_lang_default)))
				$errors[] = $this->l('The old header image is not set.');
            if (Tools::getValue('mobile_image_old_'.$id_lang_default) && !Validate::isFileName(Tools::getValue('mobile_image_old_'.$id_lang_default)))
				$errors[] = $this->l('The old mobile image is not set.');
		} /* Validation for deletion */
		elseif (Tools::isSubmit('delete_id_slide') && (!Validate::isInt(Tools::getValue('delete_id_slide')) || !$this->slideExists((int)Tools::getValue('delete_id_slide'))))
			$errors[] = $this->l('Invalid slide ID');

		/* Display errors if needed */
		if (count($errors))
		{
			$this->_html .= $this->displayError(implode('<br />', $errors));

			return false;
		}

		/* Returns if validation is ok */

		return true;
	}

	protected function _postProcess()
	{
		$errors = array();
		$shop_context = Shop::getContext();

		/* Processes Slider */
		if (Tools::isSubmit('submitSlider'))
		{
			$shop_groups_list = array();
			$shops = Shop::getContextListShopID();

			foreach ($shops as $shop_id)
			{
				$shop_group_id = (int)Shop::getGroupFromShop($shop_id, true);

				if (!in_array($shop_group_id, $shop_groups_list))
					$shop_groups_list[] = $shop_group_id;

				$res = Configuration::updateValue('SECTIONSLIDER_SPEED', (int)Tools::getValue('SECTIONSLIDER_SPEED'), false, $shop_group_id, $shop_id);
				$res &= Configuration::updateValue('SECTIONSLIDER_PAUSE', (int)Tools::getValue('SECTIONSLIDER_PAUSE'), false, $shop_group_id, $shop_id);
				$res &= Configuration::updateValue('SECTIONSLIDER_LOOP', (int)Tools::getValue('SECTIONSLIDER_LOOP'), false, $shop_group_id, $shop_id);
			}

			/* Update global shop context if needed*/
			switch ($shop_context)
			{
				case Shop::CONTEXT_ALL:
					$res = Configuration::updateValue('SECTIONSLIDER_SPEED', (int)Tools::getValue('SECTIONSLIDER_SPEED'));
					$res &= Configuration::updateValue('SECTIONSLIDER_PAUSE', (int)Tools::getValue('SECTIONSLIDER_PAUSE'));
					$res &= Configuration::updateValue('SECTIONSLIDER_LOOP', (int)Tools::getValue('SECTIONSLIDER_LOOP'));
					if (count($shop_groups_list))
					{
						foreach ($shop_groups_list as $shop_group_id)
						{
							$res = Configuration::updateValue('SECTIONSLIDER_SPEED', (int)Tools::getValue('SECTIONSLIDER_SPEED'), false, $shop_group_id);
							$res &= Configuration::updateValue('SECTIONSLIDER_PAUSE', (int)Tools::getValue('SECTIONSLIDER_PAUSE'), false, $shop_group_id);
							$res &= Configuration::updateValue('SECTIONSLIDER_LOOP', (int)Tools::getValue('SECTIONSLIDER_LOOP'), false, $shop_group_id);
						}
					}
					break;
				case Shop::CONTEXT_GROUP:
					if (count($shop_groups_list))
					{
						foreach ($shop_groups_list as $shop_group_id)
						{
							$res = Configuration::updateValue('SECTIONSLIDER_SPEED', (int)Tools::getValue('SECTIONSLIDER_SPEED'), false, $shop_group_id);
							$res &= Configuration::updateValue('SECTIONSLIDER_PAUSE', (int)Tools::getValue('SECTIONSLIDER_PAUSE'), false, $shop_group_id);
							$res &= Configuration::updateValue('SECTIONSLIDER_LOOP', (int)Tools::getValue('SECTIONSLIDER_LOOP'), false, $shop_group_id);
						}
					}
					break;
			}

			$this->clearCache();

			if (!$res)
				$errors[] = $this->displayError($this->l('The configuration could not be updated.'));
			else
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
		} /* Process Slide status */
		elseif (Tools::isSubmit('changeStatus') && Tools::isSubmit('id_slide'))
		{
			$slide = new SectionSlide((int)Tools::getValue('id_slide'));
			if ($slide->active == 0)
				$slide->active = 1;
			else
				$slide->active = 0;
			$res = $slide->update();
			$this->clearCache();
            $this->cur_group_filter = $slide->grouping;
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Configuration updated')) : $this->displayError($this->l('The configuration could not be updated.')));
		}
		/* Processes Slide */
		elseif (Tools::isSubmit('submitSlide'))
		{
			/* Sets ID if needed */
			if (Tools::getValue('id_slide'))
			{
				$slide = new SectionSlide((int)Tools::getValue('id_slide'));
				if (!Validate::isLoadedObject($slide))
				{
					$this->_html .= $this->displayError($this->l('Invalid slide ID'));
					return false;
				}
			}
			else {
				$slide = new SectionSlide();
			    /* Sets position */
			    $slide->position = (int)Tools::getValue('position');
			}
            
            /* Sets active */
			$slide->active = (int)Tools::getValue('active_slide');

			$slide->start_datetime = Tools::getValue('start_datetime');
			$slide->end_datetime = Tools::getValue('end_datetime');
			
            $slide->product_count = Tools::getValue('product_count');
            $slide->item_page_count = Tools::getValue('item_page_count');
            $slide->item_row_count = Tools::getValue('item_row_count');
            $slide->item_column_count = Tools::getValue('item_column_count');
            $slide->section_type = Tools::getValue('section_type');
            $slide->bgColor = Tools::getValue('bgColor');
            
            $slide->show_title = (int)Tools::getValue('show_title');
            $slide->addon_class = Tools::getValue('addon_class');
            $slide->grouping = Tools::getValue('grouping');
            $this->cur_group_filter = $slide->grouping;
            $slide->layout_type = Tools::getValue('layout_type');
			$slide->show_timer = (int)Tools::getValue('show_timer');

            switch($slide->section_type) {
                case 0:
                    $slide->section_value = Tools::getValue('category_value');
                break;
                case 1:
                    $slide->section_value = Tools::getValue('brand_value');
                break;
				case 10:
					$slide->section_value = Tools::getValue('voucher_value');
				break;
                case 2:
                case 3:
                case 4:
                default:
                    $slide->section_value = NULL;
                    break;
            }
            
            
			/* Sets each langue fields */
			$languages = Language::getLanguages(false);

			foreach ($languages as $language)
			{
				$slide->title[$language['id_lang']] = Tools::getValue('title_'.$language['id_lang']);
				$slide->url[$language['id_lang']] = Tools::getValue('url_'.$language['id_lang']);
				//$slide->legend[$language['id_lang']] = Tools::getValue('legend_'.$language['id_lang']);
				$slide->description[$language['id_lang']] = Tools::getValue('description_'.$language['id_lang']);

				/* Uploads image and sets slide */
				$type = Tools::strtolower(Tools::substr(strrchr($_FILES['image_'.$language['id_lang']]['name'], '.'), 1));
				$imagesize = @getimagesize($_FILES['image_'.$language['id_lang']]['tmp_name']);
				if (isset($_FILES['image_'.$language['id_lang']]) &&
					isset($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
					!empty($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
					!empty($imagesize) &&
					in_array(
						Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1)), array(
							'jpg',
							'gif',
							'jpeg',
							'png'
						)
					) &&
					in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
				)
				{
					$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
					$salt = sha1(microtime());
					if ($error = ImageManager::validateUpload($_FILES['image_'.$language['id_lang']]))
						$errors[] = $error;
					elseif (!$temp_name || !move_uploaded_file($_FILES['image_'.$language['id_lang']]['tmp_name'], $temp_name))
						return false;
					elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name'], null, null, $type))
						$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));

					if ($type=='jpg' || $type=='jpeg'){
						exec("jpegoptim '".dirname(__FILE__).'/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name']."'",$output,$result);
					}
					if ($type=='png'){
						echo "optipng ".dirname(__FILE__).'/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name'];
						exec("optipng '".dirname(__FILE__).'/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name']."'",$output,$result);
					}
					
					if ($type=='png' || $type=='jpg'){
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Exception.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/ResultMeta.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Result.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Source.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Client.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify.php");
						\Tinify\setKey("XZTJyg15cNbhrK3zRBYNKY7ZhdNZK1wG");
						
						$source = \Tinify\fromFile(dirname(__FILE__).'/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name']);
						$source->toFile(dirname(__FILE__).'/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name']);
						
					}
					
						
					if (isset($temp_name))
						@unlink($temp_name);
					$slide->image[$language['id_lang']] = $salt.'_'.$_FILES['image_'.$language['id_lang']]['name'];
				}
				elseif (Tools::getValue('image_old_'.$language['id_lang']) != '')
					$slide->image[$language['id_lang']] = Tools::getValue('image_old_'.$language['id_lang']);
                    
                /* Uploads image and sets slide */
				$type = Tools::strtolower(Tools::substr(strrchr($_FILES['hdr_image_'.$language['id_lang']]['name'], '.'), 1));
				$imagesize = @getimagesize($_FILES['hdr_image_'.$language['id_lang']]['tmp_name']);
				if (isset($_FILES['hdr_image_'.$language['id_lang']]) &&
					isset($_FILES['hdr_image_'.$language['id_lang']]['tmp_name']) &&
					!empty($_FILES['hdr_image_'.$language['id_lang']]['tmp_name']) &&
					!empty($imagesize) &&
					in_array(
						Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1)), array(
							'jpg',
							'gif',
							'jpeg',
							'png'
						)
					) &&
					in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
				)
				{
					$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
					$salt = sha1(microtime());
                    
                    if (!file_exists(dirname(__FILE__).'/images/header')) {
                        mkdir(dirname(__FILE__).'/images/header', 0775, true);
                    }
                    
					if ($error = ImageManager::validateUpload($_FILES['hdr_image_'.$language['id_lang']]))
						$errors[] = $error;
					elseif (!$temp_name || !move_uploaded_file($_FILES['hdr_image_'.$language['id_lang']]['tmp_name'], $temp_name))
						return false;
					elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/images/header/'.$salt.'_'.$_FILES['hdr_image_'.$language['id_lang']]['name'], null, null, $type))
						$errors[] = $this->displayError($this->l('An error occurred during the header image upload process.'));

					if ($type=='jpg' || $type=='jpeg'){
						exec("jpegoptim '".dirname(__FILE__).'/images/header/'.$salt.'_'.$_FILES['hdr_image_'.$language['id_lang']]['name']."'",$output,$result);
					}
					if ($type=='png'){
						echo "optipng ".dirname(__FILE__).'/images/header/'.$salt.'_'.$_FILES['hdr_image_'.$language['id_lang']]['name'];
						exec("optipng '".dirname(__FILE__).'/images/header/'.$salt.'_'.$_FILES['hdr_image_'.$language['id_lang']]['name']."'",$output,$result);
					}
					
					if ($type=='png' || $type=='jpg'){
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Exception.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/ResultMeta.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Result.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Source.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Client.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify.php");
						\Tinify\setKey("XZTJyg15cNbhrK3zRBYNKY7ZhdNZK1wG");
						
						$source = \Tinify\fromFile(dirname(__FILE__).'/images/header/'.$salt.'_'.$_FILES['hdr_image_'.$language['id_lang']]['name']);
						$source->toFile(dirname(__FILE__).'/images/header/'.$salt.'_'.$_FILES['hdr_image_'.$language['id_lang']]['name']);
						
					}
						
					if (isset($temp_name))
						@unlink($temp_name);
					$slide->hdr_image[$language['id_lang']] = $salt.'_'.$_FILES['hdr_image_'.$language['id_lang']]['name'];
				}
				elseif (Tools::getValue('hdr_image_old_'.$language['id_lang']) != '')
					$slide->hdr_image[$language['id_lang']] = Tools::getValue('hdr_image_old_'.$language['id_lang']);
                    
                    
                /* Uploads image and sets slide */
				$type = Tools::strtolower(Tools::substr(strrchr($_FILES['mobile_image_'.$language['id_lang']]['name'], '.'), 1));
				$imagesize = @getimagesize($_FILES['mobile_image_'.$language['id_lang']]['tmp_name']);
				if (isset($_FILES['mobile_image_'.$language['id_lang']]) &&
					isset($_FILES['mobile_image_'.$language['id_lang']]['tmp_name']) &&
					!empty($_FILES['mobile_image_'.$language['id_lang']]['tmp_name']) &&
					!empty($imagesize) &&
					in_array(
						Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1)), array(
							'jpg',
							'gif',
							'jpeg',
							'png'
						)
					) &&
					in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
				)
				{
					$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
					$salt = sha1(microtime());
                    
                    if (!file_exists(dirname(__FILE__).'/images/m')) {
                        mkdir(dirname(__FILE__).'/images/m', 0775, true);
                    }
                    
					if ($error = ImageManager::validateUpload($_FILES['mobile_image_'.$language['id_lang']]))
						$errors[] = $error;
					elseif (!$temp_name || !move_uploaded_file($_FILES['mobile_image_'.$language['id_lang']]['tmp_name'], $temp_name))
						return false;
					elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/images/m/'.$salt.'_'.$_FILES['mobile_image_'.$language['id_lang']]['name'], null, null, $type))
						$errors[] = $this->displayError($this->l('An error occurred during the mobile image upload process.'));

					if ($type=='jpg' || $type=='jpeg'){
						exec("jpegoptim '".dirname(__FILE__).'/images/m/'.$salt.'_'.$_FILES['mobile_image_'.$language['id_lang']]['name']."'",$output,$result);
					}
					if ($type=='png'){
						echo "optipng ".dirname(__FILE__).'/images/m/'.$salt.'_'.$_FILES['mobile_image_'.$language['id_lang']]['name'];
						exec("optipng '".dirname(__FILE__).'/images/m/'.$salt.'_'.$_FILES['mobile_image_'.$language['id_lang']]['name']."'",$output,$result);
					}
					
					if ($type=='png' || $type=='jpg'){
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Exception.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/ResultMeta.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Result.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Source.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify/Client.php");
						require_once(dirname(__FILE__)."/../../tools/tinify-php-master/lib/Tinify.php");
						\Tinify\setKey("XZTJyg15cNbhrK3zRBYNKY7ZhdNZK1wG");
						
						$source = \Tinify\fromFile(dirname(__FILE__).'/images/m/'.$salt.'_'.$_FILES['mobile_image_'.$language['id_lang']]['name']);
						$source->toFile(dirname(__FILE__).'/images/m/'.$salt.'_'.$_FILES['mobile_image_'.$language['id_lang']]['name']);
						
					}
						
					if (isset($temp_name))
						@unlink($temp_name);
					$slide->mobile_image[$language['id_lang']] = $salt.'_'.$_FILES['mobile_image_'.$language['id_lang']]['name'];
				}
				elseif (Tools::getValue('mobile_image_old_'.$language['id_lang']) != '')
					$slide->mobile_image[$language['id_lang']] = Tools::getValue('mobile_image_old_'.$language['id_lang']);
			
			}

			/* Processes if no errors  */
			if (!$errors)
			{
				/* Adds */
				if (!Tools::getValue('id_slide'))
				{
					if (!$slide->add())
						$errors[] = $this->displayError($this->l('The slide could not be added.'));
				}
				/* Update */
				elseif (!$slide->update())
					$errors[] = $this->displayError($this->l('The slide could not be updated.'));
				$this->clearCache();
			}
		} /* Deletes */
		elseif (Tools::isSubmit('delete_id_slide'))
		{
			$slide = new SectionSlide((int)Tools::getValue('delete_id_slide'));
			$res = $slide->delete();
            $this->cur_group_filter = $slide->grouping;

			$this->clearCache();
			if (!$res)
				$this->_html .= $this->displayError('Could not delete.');
			else
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=1&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.($this->cur_group_filter ? '&gfilter='.$this->cur_group_filter : ''));
		}

		/* Display errors if needed */
		if (count($errors))
			$this->_html .= $this->displayError(implode('<br />', $errors));
		elseif (Tools::isSubmit('submitSlide') && Tools::getValue('id_slide'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.($this->cur_group_filter ? '&gfilter='.$this->cur_group_filter : ''));
		elseif (Tools::isSubmit('submitSlide'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=3&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.($this->cur_group_filter ? '&gfilter='.$this->cur_group_filter : ''));
	}

	protected function _prepareHook($params)
	{
		if (!$this->isCached('sectionslider.tpl', $this->getCacheId()))
		{
			$slides = $this->getSlides(true);
			$slides = $this->GetSlideProducts($slides);

			if (!$slides)
				return false;

			$this->smarty->assign(array('sectionslider_slides' => $slides, 'sectionslider_views' => _PS_ROOT_DIR_."/modules/claritassectionslider/views"));
			
		}
		return true;
	}

    public function GetSlideProducts($slides, $sellerId = null) {
		$products='';
        if (is_array($slides))
			foreach ($slides as &$slide)
			{
		//		$slide['sizes'] = @getimagesize((dirname(__FILE__).DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$slide['image']));
		//		if (isset($slide['sizes'][3]) && $slide['sizes'][3])
		//			$slide['size'] = $slide['sizes'][3];
                $isBundle = false;
                $cat_url_rewrite = "";
                switch($slide['section_type']) {
                    case 0: // by category
        	            $category = new Category($slide['section_value'], (int)Context::getContext()->language->id);
			            $nb = (int)$slide['product_count'];
			            $products = $category->getProducts((int)Context::getContext()->language->id, 1, $nb, null, null, false, true, true, $nb);
                        $cat_url_rewrite = $category->link_rewrite;
                        
                        //// get bundle
                        $bundleId = Configuration::get('BUNDLE_PRODUCT_SELECTED_CAT');
                        if ($bundleId == $slide['section_value']) 
                        {
                            $isBundle = true;
                        }
                        else if($bundleId && $bundleId > 0) 
                        {
                            $sql = "SELECT id_category FROM "._DB_PREFIX_."category WHERE id_parent=".(int)$bundleId;
                            $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
                            
                            foreach ($results as $result)
                    	    {
                                if($result['id_category'] == $slide['section_value']) {
                                    $isBundle = true;
                                }
                            }
                        }
                    break;
                    case 1: // by brand
                        $nb = (int)$slide['product_count'];
			            $products = Manufacturer::getProducts($slide['section_value'], (int)Context::getContext()->language->id, 1, $nb, null, NULL, false, true, true, null);	
                    break;
                    case 2: // best seller                   
                		$products = Product::getBestSellerProducts((int) $this->context->language->id, 0, (int)$slide['product_count']);     
                    break;
                    case 3: // new arrival
                        $products = Product::getNewProducts((int) $this->context->language->id, 0, (int)$slide['product_count'],false,null,null,null,30);   // hardcode last 30 days so is faster                                             
                    break;
                    case 4: // featured 
                        $category = new Category((int)Configuration::get('HOME_FEATURED_CAT'), (int)Context::getContext()->language->id);
			            $nb = (int)$slide['product_count'];
			            $products = $category->getProducts((int)Context::getContext()->language->id, 1, $nb, null, null, false, true, true, $nb);
                    break;
                    case 5: // story 
			            $products = $this::getStories();
						$ccount=sizeof($products)-1;
						while($ccount>=0){
							$rawThumbnailArr=explode(";",$products[$ccount]['thumbnail']);
							if ($rawThumbnailArr[3]){
								$processedArr=explode(":",$rawThumbnailArr[3]);
								$processedArr[2]=str_ireplace('"','',$processedArr[2]);
								$processedArr[2]=str_ireplace('.jpg','-270x180.jpg',$processedArr[2]);
								$processedArr[2]=str_ireplace('.png','-270x180.png',$processedArr[2]);
								if (!$processedArr[2]){									
									$processedArr=explode(":",$rawThumbnailArr[7]);
									$processedArr[2]=str_ireplace('"','',$processedArr[2]);
									$processedArr[2]=str_ireplace('.jpg','-270x180.jpg',$processedArr[2]);
									$processedArr[2]=str_ireplace('.png','-270x180.png',$processedArr[2]);
								}
								$products[$ccount]['thumbnail']='https://cdn.motherhood.com.my/'.$processedArr[2];
								$ccount=$ccount-1;
							}else{
								// use GUID  
								$products[$ccount]['thumbnail']=$products[$ccount]['thumbnail2'];
								$products[$ccount]['thumbnail']=str_ireplace('.jpg','-300x197.jpg',$products[$ccount]['thumbnail']);
								$products[$ccount]['thumbnail']=str_ireplace('.png','-300x197.png',$products[$ccount]['thumbnail']);
								
								
								$ccount=$ccount-1;
							}
							
						}
                    break;
                    case 6: // REVIEWS 
			            $comments = $this::getReviews();
						$count=0;
						while($count < sizeof($comments)){
							$productId=$comments[$count]['id_product'];
							$thisProduct=new Product($productId);
							
							$productArr=$comments[$count];
							
							$productArr['new_product']=$thisProduct->new_product;
							$productArr['best_seller']=$thisProduct->best_seller;
							$productArr['free_deliver']=$thisProduct->free_deliver;
							$productArr['best_price']=$thisProduct->best_price;
							$productArr['custom_tag']=$thisProduct->custom_tag;
							$productArr['category'] = Category::getLinkRewrite($thisProduct->id_category_default, $this->context->language->id);
							$productArr['link']= $this->context->link->getProductLink($thisProduct->id, $thisProduct->link_rewrite[$this->context->language->id],$productArr['category'], $thisProduct->ean13);;
							$productArr['name']=$thisProduct->name[$this->context->language->id];
							$productArr['imgLink']=$this->context->link->getImageLink($thisProduct->link_rewrite[1], $comments[$count]['id_image'], 'home_default');
							$productArr['id_image']=$comments[$count]['id_image'];
							//$productArr['legend']=$thisProduct->legend;
							$productArr['fullProduct']=$thisProduct;
							
							$content=$productArr['content'];
							$maxPos = 100;
							if (strlen($content) > $maxPos)
							{
								$lastPos = ($maxPos - 3) - strlen($content);
								if (mb_strlen($content) != strlen($content)) {
									$productArr['content'] = mb_substr($content, 0, 40) . '...';
								} else {
									$productArr['content'] = substr($content, 0, strrpos($content, ' ', $lastPos)) . '...';
								}
							}
							
                            $content=$productArr['content'];
							$maxPos = 100;
							if (strlen($content) > $maxPos)
							{
								$lastPos = ($maxPos - 3) - strlen($content);
								if (mb_strlen($content) != strlen($content)) {
									$productArr['content'] = mb_substr($content, 0, 40) . '...';
								} else {
									$productArr['content'] = substr($content, 0, strrpos($content, ' ', $lastPos)) . '...';
								}
							}
							
							$comments[$count]=$productArr;
							
							$count++;
						}
						
						$products=$comments;
						
                    break;
                    case 7: // html. convert img src to data-src for lazy-load
							// note, for img only, which have title=" " src=""... cannot just use src=" cause it'll effect video also
                        $slide['description'] = str_ireplace('" src="','" src="" data-src="',$slide['description']);
                        $slide['description'] = str_ireplace('s3.amazonaws.com/motherhood.com.my','cdn.motherhood.com.my',$slide['description']);
                    break;
					case 8:
						// brands in category
			            $products = $this->getBrandsInCategory($slide['section_value']);	
					break;
					case 9:
						$products='';
							// official stores
						$sql = '
							SELECT virtual_uri,ams_url_imglogo,a.id_sellerinfo, b.company as companyname
							FROM ps_sellerinfo a
							LEFT JOIN ps_sellerinfo_lang b ON a.id_sellerinfo = b.id_sellerinfo
							LEFT JOIN ps_shop c ON c.id_shop = a.id_shop 
							LEFT JOIN ps_shop_url d ON d.id_shop = a.id_shop AND d.active =1 AND d.main =1
							WHERE (a.id_sellertype1 = 3) AND b.id_lang = 1 AND b.description != "" AND a.id_sellerinfo != 570;
						';
						$resultSeller = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
					
						
						foreach ($resultSeller as $oneSeller)
						{
							$productArr=array();
							$productArr['url']= $oneSeller['virtual_uri'];
							$productArr['name']= $oneSeller['companyname'];
							$productArr['imgLink']=$oneSeller['ams_url_imglogo'];
							$productArr['id_product']=7;
							
							
							//if (Context::getContext()->customer->id==3143){
							$products[]=$productArr;
							//}
						}
						
						break;
					case 10:
						// voucher products']);
                        $nb = (int)$slide['product_count'];
                        $products=$this->getProductsApplyVoucher($slide['section_value'], $nb);
						break;
                }
                
                $slide['isBundle'] = $isBundle;
                $slide['cat_rewrite'] = $cat_url_rewrite;

                if(is_array($products)) {
                    $slide['products'] = $products;
                }
			}
            
        return $slides;
    }
	
	public function getProductsApplyVoucher($id_cart_rule, $nb){
		$id_lang=1;
		$sql = "
			SELECT B.id_product_rule_group, B.type, GROUP_CONCAT(C.id_item) AS item
            FROM ps_cart_rule_product_rule_group A 
			INNER JOIN ps_cart_rule_product_rule B ON A.id_product_rule_group=B.id_product_rule_group
			INNER JOIN ps_cart_rule_product_rule_value C ON B.id_product_rule = C.id_product_rule
			WHERE id_cart_rule='$id_cart_rule' GROUP BY B.id_product_rule_group, B.type
		";

		$cartResults = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

        $productIds = [];
        $isFirst = true;
        $selectProductSQL= 'SELECT p.id_product FROM `'._DB_PREFIX_.'product` p ';
		
        for($i = 0; $i < count($cartResults); $i++)
        {
		    $productRule=$cartResults[$i];

		    if ($productRule)
            {
			    switch ($productRule['type'])
			    {
				    case 'products':
					    $runSQL=$selectProductSQL.' WHERE p.active=1 AND p.id_product IN ('.$productRule['item'].')';
					    break;
				    case 'categories':
					    $runSQL=$selectProductSQL.' INNER JOIN ps_category_product cp on p.id_product=cp.id_product WHERE p.active=1 AND cp.id_category IN ('.$productRule['item'].')';
					    break;
				    case 'manufacturers':
					    $runSQL=$selectProductSQL.' WHERE p.active=1 AND p.id_manufacturer IN ('. $productRule['item'].')';
					    break;
				    case 'seller':
					    $runSQL=$selectProductSQL.' INNER JOIN ps_product_owner po ON p.id_product = po.id_product WHERE p.active=1 AND po.id_owner IN ('. $productRule['item'].')';
					    break;
			    }
                $results = Db::getInstance()->executeS($runSQL);
                $tempProducts = [];
                foreach($results as $p) { $tempProducts[] = $p['id_product']; }
                if($isFirst) { $productIds = $tempProducts; }
                else { $productIds = array_intersect($productIds, $tempProducts); }
                $isFirst = false;
		    }
		}
		
        if(count($productIds) > 0) {
            $productSQL= 
            'SELECT 
                p.*, pl.name, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`,
                pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name,
                t.`rate`, DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new,
			    (p.`price` * ((100 + (t.`rate`))/100)) AS orderprice
		    FROM `'._DB_PREFIX_.'product` p
		    LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
		    LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
		    LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
		    LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
		    LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
		                                               AND tr.`id_country` = '.(int)(_PS_VERSION_>'1.5' ? Context::getContext()->country->id :  Country::getDefaultCountryId()).'
	                                           	       AND tr.`id_state` = 0)
	        LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
		    LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
		    LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
		    WHERE p.id_product IN ('.implode(",", $productIds).') LIMIT '.$nb;
            $resultProducts = Db::getInstance()->executeS( $productSQL );
		}

        //$this->fisherYatesShuffle($resultProducts,date('dH'));  // if want to fixed an hourly random result, use this function. 
		return $resultProducts;
	}
	
	
	protected function fisherYatesShuffle(&$items, $seed)
	{
		@mt_srand($seed);
		for ($i = count($items) - 1; $i > 0; $i--)
		{
			$j = @mt_rand(0, $i);
			$tmp = $items[$i];
			$items[$i] = $items[$j];
			$items[$j] = $tmp;
		}
	}

    
	public function hookdisplayHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'css/flexisel.css');
		$this->context->controller->addCSS($this->_path.'css/sectionslider.css');
		$this->context->controller->addJS($this->_path.'js/jquery.flexisel.js');
		
		
        $this->context->controller->addCSS($this->_path.'css/owl.carousel.css');
        $this->context->controller->addCSS($this->_path.'css/owl.theme.default.css');
		$this->context->controller->addJS($this->_path.'js/owl.carousel.js');
		
 		
		$this->context->controller->addJS($this->_path.'js/sectionslider.js');
		$this->context->controller->addJqueryPlugin(array('bxslider'));

		$config = $this->getConfigFieldsValues();
		$slider = array(
			'speed' => $config['SECTIONSLIDER_SPEED'],
			'pause' => $config['SECTIONSLIDER_PAUSE'],
			'loop' => (bool)$config['SECTIONSLIDER_LOOP'],
		);

		$this->smarty->assign('sectionslider', $slider);
		return $this->display(__FILE__, 'header.tpl');
	}

	public function hookdisplayTop($params)
	{
		return $this->hookdisplayTopColumn($params);
	}

	public function hookdisplayTopColumn($params)
	{
		if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'index')
			return;

		if (!$this->_prepareHook($params))
			return false;

		return $this->display(__FILE__, 'sectionslider.tpl', $this->getCacheId());
	}

	public function hookDisplayHome($params)
	{
		if (!$this->_prepareHook($params))
			return false;

		return $this->display(__FILE__, 'sectionslider.tpl', $this->getCacheId());
	}

    public function hookDisplayBackOfficeHeader()
	{
		if (Tools::getValue('configure') != $this->name)
			return;

		$this->context->controller->addJquery();
		$this->context->controller->addJS($this->_path.'js/admin.js');
	}
    
	public function clearCache()
	{
		$this->_clearCache('sectionslider.tpl');
    }

	public function headerHTML()
	{
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;

		$this->context->controller->addJqueryUI('ui.sortable');
		/* Style & js for fieldset 'slides configuration' */
		$html = '<script type="text/javascript">
			$(function() {
				var $mySlides = $("#slides");
				$mySlides.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateSlidesPosition";
						$.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
						}
					});
				$mySlides.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';

		return $html;
	}

	public function getNextPosition()
	{
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT MAX(hss.`position`) AS `next_position`
			FROM `'._DB_PREFIX_.'sectionslider_slides` hss, `'._DB_PREFIX_.'sectionslider` hs
			WHERE hss.`id_sectionslider_slides` = hs.`id_sectionslider_slides` AND hs.`id_shop` = '.(int)$this->context->shop->id
		);

		return (++$row['next_position']);
	}
    
	public function getSlides($active = null, $grouping = null)
	{
		$this->context = Context::getContext();
		$id_shop = $this->context->shop->id;
		$id_lang = $this->context->language->id;

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_sectionslider_slides` as id_slide, hss.`position`, hss.`active`, hssl.`title`, hss.`bgColor`, hss.addon_class, hss.show_title, hss.show_timer,
			    hssl.`url`, hssl.`description`, hssl.`image`, hssl.`hdr_image`, hssl.`mobile_image`, end_datetime, start_datetime, 
                product_count,item_page_count, item_row_count,item_column_count, grouping, section_type, section_value, layout_type
			FROM '._DB_PREFIX_.'sectionslider hs
			INNER JOIN '._DB_PREFIX_.'sectionslider_slides hss ON (hs.id_sectionslider_slides = hss.id_sectionslider_slides)
			INNER JOIN '._DB_PREFIX_.'sectionslider_slides_lang hssl ON (hss.id_sectionslider_slides = hssl.id_sectionslider_slides)
			WHERE hs.id_shop = '.(int)$id_shop.'
			AND hssl.id_lang = '.(int)$id_lang.
            ($grouping ? ' AND hss.grouping="'.$grouping.'"' : ' ').
			($active ? ' AND ( hss.`active` = 1 AND (  
  (start_datetime IS NULL AND end_datetime IS NULL ) 
  OR ( start_datetime="0000-00-00 00:00:00" AND end_datetime="0000-00-00 00:00:00" ) 
  OR ( start_datetime="0000-00-00 00:00:00" AND CURRENT_TIMESTAMP < end_datetime )
  OR ( CURRENT_TIMESTAMP > start_datetime AND end_datetime="0000-00-00 00:00:00" )
  OR ( CURRENT_TIMESTAMP > start_datetime AND CURRENT_TIMESTAMP < end_datetime )
) )' : ' ').'
			ORDER BY hss.position'
		);
	}

    public function getGroupingFilter()
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
            'SELECT DISTINCT grouping FROM '._DB_PREFIX_.'sectionslider_slides ORDER BY grouping'
		);
	}

	public function getAllImagesBySlidesId($id_slides, $active = null, $id_shop = null)
	{
		$this->context = Context::getContext();
		$images = array();

		if (!isset($id_shop))
			$id_shop = $this->context->shop->id;

		$results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hssl.`image`, hssl.`id_lang`
			FROM '._DB_PREFIX_.'sectionslider hs
			LEFT JOIN '._DB_PREFIX_.'sectionslider_slides hss ON (hs.id_sectionslider_slides = hss.id_sectionslider_slides)
			LEFT JOIN '._DB_PREFIX_.'sectionslider_slides_lang hssl ON (hss.id_sectionslider_slides = hssl.id_sectionslider_slides)
			WHERE hs.`id_sectionslider_slides` = '.(int)$id_slides.' AND hs.`id_shop` = '.(int)$id_shop.
			($active ? ' AND hss.`active` = 1' : ' ')
		);

		foreach ($results as $result)
			$images[$result['id_lang']] = $result['image'];

		return $images;
	}

	public function displayStatus($id_slide, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$html = '<a class="btn '.$class.'" href="'.$this->context->link->getAdminLink('AdminModules').//AdminController::$currentIndex.
			'&configure='.$this->name.
				//'&token='.Tools::getAdminTokenLite('AdminModules').'
				'&changeStatus&id_slide='.(int)$id_slide.'" title="'.$title.'"><i class="'.$icon.'"></i> '.$title.'</a>';

		return $html;
	}

	public function slideExists($id_slide)
	{
		$req = 'SELECT hs.`id_sectionslider_slides` as id_slide
				FROM `'._DB_PREFIX_.'sectionslider` hs
				WHERE hs.`id_sectionslider_slides` = '.(int)$id_slide;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);

		return ($row);
	}

	public function renderList($groupingfilter = null, $empId = null)
	{
        if($empId != null)
            $this->context->employee = new Employee($empId);

        $grouping = $this->getGroupingFilter();
        $defaultFilter = null;

        if($grouping && count($grouping) > 0)
            $defaultFilter = $grouping[0]['grouping'];

        if($groupingfilter == null) {
            if(Tools::getValue('gfilter'))
                $groupingfilter = Tools::getValue('gfilter');
            else
                $groupingfilter = $defaultFilter;
        }

		$slides = $this->getSlides(null, $groupingfilter);
        foreach ($slides as $key => $slide)
		{
			$slides[$key]['status'] = $this->displayStatus($slide['id_slide'], $slide['active']);
			$associated_shop_ids = SectionSlide::getAssociatedIdsShop((int)$slide['id_slide']);
			if ($associated_shop_ids && count($associated_shop_ids) > 1)
				$slides[$key]['is_shared'] = true;
			else
				$slides[$key]['is_shared'] = false;
		}

		$this->context->smarty->assign(
			array(
				'link' => $this->context->link,
				'slides' => $slides,
				'image_baseurl' => $this->_path.'images/',
                'theme_url' => $this->context->link->getAdminLink('Adminclaritassectionslider'),
                'grouping_list' => $grouping,
                'filter' => $groupingfilter,
                'secslider_secure_key' => $this->secure_key,
                'eid' => $this->context->employee->id,
			)
		);

		$this->context->controller->addJqueryUI('ui.sortable');
		return $this->display(__FILE__, 'list.tpl');
	}

	public function renderAddForm()
	{
        $sql = 'SELECT A.id_category, IFNULL(B.fullPath, C.name) AS cat_name'.
                ' FROM '._DB_PREFIX_.'category A'.
                ' LEFT JOIN '._DB_PREFIX_.'category_parent B ON A.id_category = B.id_category'.
                ' INNER JOIN '._DB_PREFIX_.'category_lang C ON A.id_category = C.id_category AND C.id_lang = 1'.
                ' WHERE A.level_depth > 0'.
                ' ORDER BY IFNULL(B.fullPath, C.name)';
        $catList = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        
        $sql = 'SELECT id_manufacturer, name AS brand_name'.
                ' FROM '._DB_PREFIX_.'manufacturer'.
                ' WHERE active = 1'.
                ' ORDER BY name';
        $brandList = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        
        $sql = 'SELECT id_cart_rule, code'.
                ' FROM '._DB_PREFIX_.'cart_rule'.
                ' WHERE active = 1 AND code != "" AND code NOT LIKE "WELCOME%" AND id_customer=0'.
                ' ORDER BY code';
        $voucherList = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Section information'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'file_lang',
						'label' => $this->l('Side Image'),
						'name' => 'image',
						//'required' => true,
						'lang' => true,
						'desc' => sprintf($this->l('Maximum image size: %s.'), ini_get('upload_max_filesize'))
					),
                    array(
						'type' => 'file_lang',
						'label' => $this->l('Side Image (Mobile)'),
						'name' => 'mobile_image',
						'required' => false,
						'lang' => true,
						'desc' => sprintf($this->l('Maximum image size: %s.'), ini_get('upload_max_filesize'))
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Section title'),
						'name' => 'title',
						'required' => true,
						'lang' => true,
					),
                    array(
						'type' => 'switch',
						'label' => $this->l('Display Title'),
						'name' => 'show_title',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'show_title_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'show_title_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Target URL'),
						'name' => 'url',
						//'required' => true,
						'lang' => true,
					),
					//array(
					//	'type' => 'text',
					//	'label' => $this->l('Caption'),
					//	'name' => 'legend',
					//	'required' => true,
					//	'lang' => true,
					//),
					array(
						'type' => 'textarea',
						'label' => $this->l('Description'),
						'name' => 'description',
						'autoload_rte' => true,
						'lang' => true,
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled'),
						'name' => 'active_slide',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Custom Class'),
						'name' => 'addon_class',
                        'required' => false,
                        'desc' => $this->l('Customize class style.')
					),
                    array(                
                        'type' => 'color',
                        'label' => $this->l('Border Color'),
                        'name' => 'bgColor',
                        'lang' => false,
                        //'id'   => 'color_0',
                        'data-hex' => true,
                        'class'   => 'mColorPicker',
                        'desc' => $this->l('Enter color hex code.'),
                    ),
                    array(
						'type' => 'file_lang',
						'label' => $this->l('Header image'),
						'name' => 'hdr_image',
						'required' => false,
						'lang' => true,
						'desc' => sprintf($this->l('Maximum image size: %s.'), ini_get('upload_max_filesize'))
					),
					array(
						'type' => 'datetime',
						'label' => $this->l('Start Date'),
						'name' => 'start_datetime',
						'lang' => true,
					),
					array(
						'type' => 'datetime',
						'label' => $this->l('End Date'),
						'name' => 'end_datetime',
						'lang' => true,
					),
                    array(
						'type' => 'text',
						'label' => $this->l('No. of Products'),
						'name' => 'product_count',
                        'required' => true,
                        'desc' => $this->l('Maximum number of products display under this section.')
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Item per page'),
						'name' => 'item_page_count',
                        'required' => true,
                        'desc' => $this->l('Number of items visible per page.')
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Display row'),
						'name' => 'item_row_count',
                        'required' => true,
                        'desc' => $this->l('Number of row visible at a time.')
					),
                    //array(
                    //    'type' => 'text',
                    //    'label' => $this->l('Number of item per row'),
                    //    'name' => 'item_column_count',
                    //    'required' => true,
                    //    'desc' => $this->l('Number of column for each row. Not required if Display row is 1')
                    //),
                    array(
						'type' => 'text',
						'label' => $this->l('Grouping'),
						'name' => 'grouping',
                        'required' => true,
                        'desc' => $this->l('Section slider group.')
					),
                    array(
                        'type' => 'select',
                        'name' => 'layout_type',
                        'label' => $this->l('Layout Type'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_layout_type' => 'slide',
                                    'name' => $this->l('Slide')
                                ),
                                array(
                                    'id_layout_type' => 'grid',
                                    'name' => $this->l('Grid')
                                )
                            ),
                            'id' => 'id_layout_type',
                            'name' => 'name'
                        )
                    ), 
                    array(
                        'type' => 'select',
                        'name' => 'section_type',
                        'label' => $this->l('Section Type'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_section_type' => '0',
                                    'name' => $this->l('Category')
                                ),
                                //array(
                                //    'id_section_type' => '1',
                                //    'name' => $this->l('Brand')
                                //),
                                array(
                                    'id_section_type' => '2',
                                    'name' => $this->l('Best Seller')
                                ),
                                array(
                                    'id_section_type' => '3',
                                    'name' => $this->l('New Arrivals')
                                ),
                                array(
                                    'id_section_type' => '4',
                                    'name' => $this->l('Featured Product')
                                ),
                                //array(
                                //    'id_section_type' => '5',
                                //    'name' => $this->l('Story Articles')
                                //),
                                //array(
                                //    'id_section_type' => '6',
                                //    'name' => $this->l('Product Reviews')
                                //),
                                array(
                                    'id_section_type' => '7',
                                    'name' => $this->l('Content Only')
                                ),
                                //array(
                                //    'id_section_type' => '8',
                                //    'name' => $this->l('Brands of Category')
                                //),
                                //array(
                                //    'id_section_type' => '9',
                                //    'name' => $this->l('Official Brand Store')
                                //),
                                //array(
                                //    'id_section_type' => '10',
                                //    'name' => $this->l('Voucher Item Highlights')
                                //),
                            ),
                            'id' => 'id_section_type',
                            'name' => 'name'
                        )
                    ),                    
                    array(
                        'type' => 'select',
                        'name' => 'category_value',
                        'label' => $this->l('Category'),
                        'options' => array(
                            'query' => $catList,
                            'id' => 'id_category',
                            'name' => 'cat_name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'brand_value',
                        'label' => $this->l('Brand'),
                        'options' => array(
                            'query' => $brandList,
                            'id' => 'id_manufacturer',
                            'name' => 'brand_name'
                        )
					),
                    array(
						'type' => 'select',
						'name' => 'voucher_value',
						'label' => $this->l('Voucher'),
                        'options' => array(
                            'query' => $voucherList,
                            'id' => 'id_cart_rule',
                            'name' => 'code'
                        ),
                        'desc' => $this->l('Voucher Id to Highlight.')
					),
                    //array(
                    //    'type' => 'switch',
                    //    'label' => $this->l('Display CountDown Timer'),
                    //    'name' => 'show_timer',
                    //    'is_bool' => true,
                    //    'values' => array(
                    //        array(
                    //            'id' => 'show_timer_on',
                    //            'value' => 1,
                    //            'label' => $this->l('Yes')
                    //        ),
                    //        array(
                    //            'id' => 'show_timer_off',
                    //            'value' => 0,
                    //            'label' => $this->l('No')
                    //        )
                    //    ),
                    //),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		if (Tools::isSubmit('id_slide') && $this->slideExists((int)Tools::getValue('id_slide')))
		{
			$slide = new SectionSlide((int)Tools::getValue('id_slide'));
			$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_slide');
			$fields_form['form']['image'] = $slide->image;
            $fields_form['form']['hdr_image'] = $slide->hdr_image;
            $fields_form['form']['mobile_image'] = $slide->mobile_image;

			$has_picture = true;
			$has_hdr_picture=false; // ? what is the default value? 
            $has_mobile_picture = false;

			foreach (Language::getLanguages(false) as $lang) {
				if (!isset($slide->image[$lang['id_lang']]))
					$has_picture &= false;
                    
                if (!isset($slide->hdr_image[$lang['id_lang']]))
					$has_hdr_picture &= false;
                    
                if (!isset($slide->mobile_image[$lang['id_lang']]))
					$has_mobile_picture &= false;
            }
            
			if ($has_picture)
				$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'has_picture');
		    
            if ($has_hdr_picture)
				$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'has_hdr_picture');
                
            if ($has_mobile_picture)
				$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'has_mobile_picture');
		}

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitSlide';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getAddFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'image_baseurl' => $this->_path.'images/'
		);

		$helper->override_folder = '/';

		$languages = Language::getLanguages(false);

		if (count($languages) > 1)
			return $this->getMultiLanguageInfoMsg().$helper->generateForm(array($fields_form));
		else
			return $helper->generateForm(array($fields_form));
	}

	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Speed'),
						'name' => 'SECTIONSLIDER_SPEED',
						'suffix' => 'milliseconds',
						'desc' => $this->l('The duration of the transition between two slides.')
					),
					array(
						'type' => 'text',
						'label' => $this->l('Pause'),
						'name' => 'SECTIONSLIDER_PAUSE',
						'suffix' => 'milliseconds',
						'desc' => $this->l('The delay between two slides.')
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Auto play'),
						'name' => 'SECTIONSLIDER_LOOP',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					)
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitSlider';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		$id_shop_group = Shop::getContextShopGroupID();
		$id_shop = Shop::getContextShopID();

		return array(
			'SECTIONSLIDER_SPEED' => Tools::getValue('SECTIONSLIDER_SPEED', Configuration::get('SECTIONSLIDER_SPEED', null, $id_shop_group, $id_shop)),
			'SECTIONSLIDER_PAUSE' => Tools::getValue('SECTIONSLIDER_PAUSE', Configuration::get('SECTIONSLIDER_PAUSE', null, $id_shop_group, $id_shop)),
			'SECTIONSLIDER_LOOP' => Tools::getValue('SECTIONSLIDER_LOOP', Configuration::get('SECTIONSLIDER_LOOP', null, $id_shop_group, $id_shop)),
		);
	}

	public function getAddFieldsValues()
	{
		$fields = array();

		if (Tools::isSubmit('id_slide') && $this->slideExists((int)Tools::getValue('id_slide')))
		{
			$slide = new SectionSlide((int)Tools::getValue('id_slide'));
			$fields['id_slide'] = (int)Tools::getValue('id_slide', $slide->id);
		}
		else {
			$slide = new SectionSlide();
            
            //// default value
            $slide->bgColor = "#ececec";
        }

		$fields['active_slide'] = Tools::getValue('active_slide', $slide->active);
		$fields['has_picture'] = true;
		$fields['has_hdr_picture'] = true;
        $fields['has_mobile_picture'] = true;
        
		$fields['start_datetime'] = Tools::getValue('start_datetime', $slide->start_datetime);
		$fields['end_datetime'] = Tools::getValue('end_datetime', $slide->end_datetime);

        $fields['product_count'] = Tools::getValue('product_count', $slide->product_count);
        $fields['item_page_count'] = Tools::getValue('item_page_count', $slide->item_page_count);
		$fields['item_row_count'] = Tools::getValue('item_row_count', $slide->item_row_count);
		$fields['item_column_count'] = Tools::getValue('item_column_count', $slide->item_column_count);
		$fields['section_type'] = Tools::getValue('section_type', $slide->section_type);
        $fields['bgColor'] = Tools::getValue('bgColor', $slide->bgColor);
        $fields['show_title'] = Tools::getValue('show_title', $slide->show_title);
        $fields['addon_class'] = Tools::getValue('addon_class', $slide->addon_class);
        $fields['grouping'] = Tools::getValue('grouping', $slide->grouping);
		$fields['layout_type'] = Tools::getValue('layout_type', $slide->layout_type);
		$fields['show_timer'] = Tools::getValue('show_timer', $slide->show_timer);
        
        switch($fields['section_type']) {
            case 0:
                $fields['category_value'] = Tools::getValue('section_value', $slide->section_value);
				$fields['brand_value']='';
				$fields['voucher_value']='';
                break;
            case 1:
                $fields['brand_value'] = Tools::getValue('section_value', $slide->section_value);
				$fields['category_value']='';
				$fields['voucher_value']='';
                break;
            case 7:
                $fields['brand_value'] = '';
				$fields['category_value']='';
				$fields['voucher_value']='';
                break;
            case 10:
                $fields['voucher_value'] = Tools::getValue('section_value', $slide->section_value);
				$fields['category_value']='';
				$fields['brand_value']='';
                break;
        }
        
		$languages = Language::getLanguages(false);

		foreach ($languages as $lang)
		{
			$fields['image'][$lang['id_lang']] = Tools::getValue('image_'.(int)$lang['id_lang']);
			$fields['hdr_image'][$lang['id_lang']] = Tools::getValue('hdr_image_'.(int)$lang['id_lang']);
			$fields['mobile_image'][$lang['id_lang']] = Tools::getValue('mobile_image_'.(int)$lang['id_lang']);
			$fields['title'][$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], $slide->title[$lang['id_lang']]);
			$fields['url'][$lang['id_lang']] = Tools::getValue('url_'.(int)$lang['id_lang'], $slide->url[$lang['id_lang']]);
			//$fields['legend'][$lang['id_lang']] = Tools::getValue('legend_'.(int)$lang['id_lang'], $slide->legend[$lang['id_lang']]);
			$fields['description'][$lang['id_lang']] = Tools::getValue('description_'.(int)$lang['id_lang'], $slide->description[$lang['id_lang']]);
		}
        
		return $fields;
	}

	protected function getMultiLanguageInfoMsg()
	{
		return '<p class="alert alert-warning">'.
					$this->l('Since multiple languages are activated on your shop, please mind to upload your image for each one of them').
				'</p>';
	}

	protected function getWarningMultishopHtml()
	{
		if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL)
			return '<p class="alert alert-warning">'.
						$this->l('You cannot manage slides items from a "All Shops" or a "Group Shop" context, select directly the shop you want to edit').
					'</p>';
		else
			return '';
	}

	protected function getShopContextError($shop_contextualized_name, $mode)
	{
		if (is_array($shop_contextualized_name))
			$shop_contextualized_name = implode('<br/>', $shop_contextualized_name);

		if ($mode == 'edit')
			return '<p class="alert alert-danger">'.
							sprintf($this->l('You can only edit this slide from the shop(s) context: %s'), $shop_contextualized_name).
					'</p>';
		else
			return '<p class="alert alert-danger">'.
							sprintf($this->l('You cannot add slides from a "All Shops" or a "Group Shop" context')).
					'</p>';
	}

	protected function getShopAssociationError($id_slide)
	{
		return '<p class="alert alert-danger">'.
						sprintf($this->l('Unable to get slide shop association information (id_slide: %d)'), (int)$id_slide).
				'</p>';
	}

	protected function getCurrentShopInfoMsg()
	{
		$shop_info = null;

		if (Shop::isFeatureActive())
		{
			if (Shop::getContext() == Shop::CONTEXT_SHOP)
				$shop_info = sprintf($this->l('The modifications will be applied to shop: %s'), $this->context->shop->name);
			else if (Shop::getContext() == Shop::CONTEXT_GROUP)
				$shop_info = sprintf($this->l('The modifications will be applied to this group: %s'), Shop::getContextShopGroup()->name);
			else
				$shop_info = $this->l('The modifications will be applied to all shops and shop groups');

			return '<div class="alert alert-info">'.
						$shop_info.
					'</div>';
		}
		else
			return '';
	}

	protected function getSharedSlideWarning()
	{
		return '<p class="alert alert-warning">'.
					$this->l('This slide is shared with other shops! All shops associated to this slide will apply modifications made here').
				'</p>';
	}
	
	
    public function getStories() 
    {
        $lang_id = $this->context->language->id;
       
        $sql = " 
		SELECT * FROM (
		SELECT 
            post.id,CAST(post.guid AS CHAR) AS 'url',post.post_date,
            CAST(CASE post.post_status WHEN 'publish' THEN DATE_ADD(post.post_date, INTERVAL 1 YEAR) ELSE post.post_date END AS CHAR) AS end_date,
            post.post_modified, 
            CAST(u.user_name AS CHAR) AS user_name,
            CAST(post.post_title AS CHAR) AS post_title, 
            CAST(CONCAT('http://cdn.motherhood.com.my/wp-content/uploads/',IFNULL(thumbnailpostURL.meta_value,'')) AS CHAR) AS 'thumbnail',
            CAST(IFNULL(thumbnailpost.guid,'') AS CHAR) AS 'thumbnail2',
            CAST(IFNULL(thumbnailpost.post_title,'')AS CHAR) AS 'thumbnail_title',
            CAST(IFNULL(thumbnail.meta_value,'0') AS UNSIGNED) AS 'thumbnail_id',
            CAST(IFNULL(video.meta_value,'')AS CHAR) AS 'fv_video',
            CAST(IFNULL(videoimg.meta_value,'')AS CHAR) AS 'fv_video_img',
            CAST(category.`name` AS CHAR) AS 'primary_category',
            CAST(IFNULL(pc.meta_value,'0') AS UNSIGNED) AS 'primary_category_id',
            CAST(post.post_content AS CHAR) AS post_content,
            CAST(thumbnail.meta_value AS CHAR) AS meta_value
            FROM wp_posts post
            LEFT JOIN wp_postmeta thumbnail ON post.id = thumbnail.post_id AND thumbnail.meta_key = '_thumbnail_id'
            LEFT JOIN wp_posts thumbnailpost ON thumbnailpost.id = CAST(IFNULL(thumbnail.meta_value,'0') AS UNSIGNED)
            LEFT JOIN wp_postmeta thumbnailpostURL ON thumbnailpostURL.post_id = CAST(IFNULL(thumbnail.meta_value,'0') AS UNSIGNED) AND thumbnailpostURL.meta_key='amazonS3_info'
            LEFT JOIN wp_postmeta video ON post.id = video.post_id AND video.meta_key = 'fv_video'
            LEFT JOIN wp_postmeta videoimg ON post.id = videoimg.post_id AND videoimg.meta_key = 'fv_video_img'
            LEFT JOIN wp_postmeta pc ON post.id = pc.post_id AND pc.meta_key = '_yoast_wpseo_primary_category'
            LEFT JOIN wp_terms category ON category.term_id = CAST(IFNULL(pc.meta_value,'0') AS UNSIGNED)
            INNER JOIN (SELECT fname.user_id, CASE fname.meta_value WHEN '' THEN nname.meta_value ELSE CONCAT(fname.meta_value, ' ' ,lname.meta_value) END AS user_name
            FROM wp_usermeta fname
            INNER JOIN wp_usermeta lname ON fname.user_id = lname.user_id
            INNER JOIN wp_usermeta nname ON fname.user_id = nname.user_id
            WHERE fname.meta_key = 'first_name' AND lname.meta_key ='last_name' AND nname.meta_key = 'nickname'
            GROUP BY fname.user_id) u ON u.user_id = post.post_author
            WHERE post.post_type = 'post' AND post.post_status NOT IN ('auto-draft')
            AND thumbnailpost.guid IS NOT NULL
            AND post.post_date < CURRENT_TIMESTAMP
            AND post.id IN 
            ( 
            SELECT p.ID
FROM wp_posts p
LEFT JOIN wp_term_relationships rel ON rel.object_id = p.ID
LEFT JOIN wp_term_taxonomy tax ON tax.term_taxonomy_id = rel.term_taxonomy_id
LEFT JOIN wp_terms t ON t.term_id = tax.term_id
WHERE t.term_id=929 AND post_date>=ADDDATE(CURRENT_DATE, INTERVAL -2 MONTH)
)
UNION ALL
		SELECT 
            post.id,CAST(post.guid AS CHAR) AS 'url',post.post_date,
            CAST(CASE post.post_status WHEN 'publish' THEN DATE_ADD(post.post_date, INTERVAL 1 YEAR) ELSE post.post_date END AS CHAR) AS end_date,
            post.post_modified, 
            CAST(u.user_name AS CHAR) AS user_name,
            CAST(post.post_title AS CHAR) AS post_title, 
            CAST(CONCAT('http://cdn.motherhood.com.my/wp-content/uploads/',IFNULL(thumbnailpostURL.meta_value,'')) AS CHAR) AS 'thumbnail',
            CAST(IFNULL(thumbnailpost.guid,'') AS CHAR) AS 'thumbnail2',
            CAST(IFNULL(thumbnailpost.post_title,'')AS CHAR) AS 'thumbnail_title',
            CAST(IFNULL(thumbnail.meta_value,'0') AS UNSIGNED) AS 'thumbnail_id',
            CAST(IFNULL(video.meta_value,'')AS CHAR) AS 'fv_video',
            CAST(IFNULL(videoimg.meta_value,'')AS CHAR) AS 'fv_video_img',
            CAST(category.`name` AS CHAR) AS 'primary_category',
            CAST(IFNULL(pc.meta_value,'0') AS UNSIGNED) AS 'primary_category_id',
            CAST(post.post_content AS CHAR) AS post_content,
            CAST(thumbnail.meta_value AS CHAR) AS meta_value
            FROM wp_2_posts post
            LEFT JOIN wp_2_postmeta thumbnail ON post.id = thumbnail.post_id AND thumbnail.meta_key = '_thumbnail_id'
            LEFT JOIN wp_2_posts thumbnailpost ON thumbnailpost.id = CAST(IFNULL(thumbnail.meta_value,'0') AS UNSIGNED)
            LEFT JOIN wp_2_postmeta thumbnailpostURL ON thumbnailpostURL.post_id = CAST(IFNULL(thumbnail.meta_value,'0') AS UNSIGNED) AND thumbnailpostURL.meta_key='amazonS3_info'
            LEFT JOIN wp_2_postmeta video ON post.id = video.post_id AND video.meta_key = 'fv_video'
            LEFT JOIN wp_2_postmeta videoimg ON post.id = videoimg.post_id AND videoimg.meta_key = 'fv_video_img'
            LEFT JOIN wp_2_postmeta pc ON post.id = pc.post_id AND pc.meta_key = '_yoast_wpseo_primary_category'
            LEFT JOIN wp_2_terms category ON category.term_id = CAST(IFNULL(pc.meta_value,'0') AS UNSIGNED)
            INNER JOIN (SELECT fname.user_id, CASE fname.meta_value WHEN '' THEN nname.meta_value ELSE CONCAT(fname.meta_value, ' ' ,lname.meta_value) END AS user_name
            FROM wp_usermeta fname
            INNER JOIN wp_usermeta lname ON fname.user_id = lname.user_id
            INNER JOIN wp_usermeta nname ON fname.user_id = nname.user_id
            WHERE fname.meta_key = 'first_name' AND lname.meta_key ='last_name' AND nname.meta_key = 'nickname'
            GROUP BY fname.user_id) u ON u.user_id = post.post_author
            WHERE post.post_type = 'post' AND post.post_status NOT IN ('auto-draft')
            AND thumbnailpost.guid IS NOT NULL
            AND post.post_date < CURRENT_TIMESTAMP
            AND post.id IN 
            ( 
            SELECT p.ID
FROM wp_2_posts p
LEFT JOIN wp_2_term_relationships rel ON rel.object_id = p.ID
LEFT JOIN wp_2_term_taxonomy tax ON tax.term_taxonomy_id = rel.term_taxonomy_id
LEFT JOIN wp_2_terms t ON t.term_id = tax.term_id
WHERE t.term_id=448 AND post_date>=ADDDATE(CURRENT_DATE, INTERVAL -2 MONTH)
)

) b
            ORDER BY RAND() DESC LIMIT 9
            
		"; // A.name";
		
		//echo $sql;
        $result = Db::getMotherhoodStoryInstance()->executeS($sql);
        return $result;
    }
	
	
    public function getReviews() 
    {
		$validate = Configuration::get('PRODUCT_COMMENTS_MODERATE');
		
        $lang_id = $this->context->language->id;
       
			$query=' SELECT * FROM (
			SELECT pc.`id_product_comment`,
			(SELECT count(*) FROM `'._DB_PREFIX_.'product_comment_usefulness` pcu WHERE pcu.`id_product_comment` = pc.`id_product_comment` AND pcu.`usefulness` = 1) as total_useful,
			(SELECT count(*) FROM `'._DB_PREFIX_.'product_comment_usefulness` pcu WHERE pcu.`id_product_comment` = pc.`id_product_comment`) as total_advice, 
			IF(c.id_customer, CONCAT(c.`firstname`, \' \',  LEFT(c.`lastname`, 1)), pc.customer_name) customer_name, pc.`content`, pc.`grade`, pc.`date_add`, pc.title
			,pc.id_product,
			i.id_image
			  FROM `'._DB_PREFIX_.'product_comment` pc
			LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = pc.`id_customer`
			JOIN `'._DB_PREFIX_.'product` p ON pc.id_product = p.id_product
			JOIN `'._DB_PREFIX_.'product_owner` po ON po.id_product = p.id_product
			LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
			Shop::addSqlAssociation('image', 'i', true, 'image_shop.cover=1').'
			WHERE pc.deleted=0 '.($validate == '1' ? ' AND pc.`validate` = 1' : '').'
			AND p.active=1 AND price > 0 AND NOT visibility = "none"
			AND pc.date_add >= DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH) AND p.id_product not in (
				SELECT id_product FROM ps_category_product
				WHERE id_category IN (240,241,488,489,490)
			)
			
UNION
SELECT 1000000+crawl_id,
\'\' AS useful,
\'\' AS total_adv,
customer_name,
content,
grade,
ps_product_comment_crawl.date_add,
title,
p.id_product,
i.id_image
FROM ps_product_comment_crawl
			JOIN `'._DB_PREFIX_.'product` p ON ps_product_comment_crawl.id_product = p.id_product AND p.active=1  AND price > 0 AND NOT visibility = "none"
			JOIN `'._DB_PREFIX_.'product_owner` po ON po.id_product = p.id_product
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
		Shop::addSqlAssociation('image', 'i', true, 'image_shop.cover=1').' WHERE 1=1 
		AND p.id_product not in (
				SELECT id_product FROM ps_category_product
				WHERE id_category IN (240,241,488,489,490)
			)
AND ps_product_comment_crawl.date_add >= DATE_ADD(CURRENT_DATE, INTERVAL -1 MONTH)
		) A GROUP BY id_product
			ORDER BY `date_add` DESC LIMIT 12
			';
			
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
			
        return $result;
    }
	
	
	
	protected function getBrandsInCategory($id){
		
		if ($id){
			$sql = '
				SELECT DISTINCT b.id_manufacturer,b.name 
					FROM '._DB_PREFIX_.'product a
					JOIN '._DB_PREFIX_.'manufacturer b ON a.id_manufacturer=b.id_manufacturer AND b.active>0 AND a.active > 0
					WHERE id_product IN
					(
					SELECT id_product FROM ps_category_product
					WHERE id_category='.(int)$id.'
			) ORDER BY b.name
			';
			
			if ($id==391){
				$sql="
					SELECT title AS name, CONCAT('/modules/homeslider/images/',image) AS image, url AS newlink ,start_datetime FROM ps_homeslider_slides a 
					JOIN ps_homeslider_slides_lang b ON a.id_homeslider_slides=b.id_homeslider_slides AND id_lang=1 
					WHERE active=1 AND (start_datetime <=CURRENT_DATE OR start_datetime='0000-00-00 00:00:00') 
					AND ( end_datetime >= CURRENT_DATE OR end_datetime='0000-00-00 00:00:00')
				";
			}
			
			$order_products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		}
			
			foreach ($order_products as &$order_product)
			{
				$order_product['link'] = Tools::link_rewrite($order_product['name']);
			}
		
		return $order_products;
		
	}
	
	
}
