<?php
require_once _PS_MODULE_DIR_ . 'claritassectionslider/claritassectionslider.php';

class claritassectionsliderajaxmmysliderModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        parent::init();
    }

    public function initContent()
    {
        parent::initContent();
        $this->ajax = true; // enable ajax
    }
    
    // respond json or html
    public function displayAjax()
    {
        if ($this->errors)
            die(Tools::jsonEncode(array('hasError' => true, 'errors' => $this->errors)));

        $this->context = Context::getContext();
		$id_lang = $this->context->language->id;
        $sliderId = Tools::getValue('sliderid');
        $imagepos = Tools::getValue('imagepos');
        $sellerId = Tools::getValue('sellerid');
		$hidepriceStr="";
		$nolinkStr="";
		
        $hideprice = Tools::getValue('hideprice');
        $nolink = Tools::getValue('nolink');
		if ($hideprice=='')
			$hideprice=false;
		if ($nolink=='')
			$nolink=false;
		
		if ($nolink && $nolink!='undefined'){
			$nolinkStr='onclick="event.stopPropagation();return false;"';
		}
		if ($hideprice && $hideprice!='undefined'){
			$hidepriceStr=' style="visibility:hidden;display:none"';
		}
        
        if($imagepos != "right") {
            $imagepos = "left";
        }
      
        $slides = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_sectionslider_slides` as id_slide, hss.`position`, hss.`active`, hssl.`title`, hss.`bgColor`,
			    hssl.`url`, hssl.`description`, hssl.`image`, hssl.`hdr_image`, end_datetime, start_datetime, 
                product_count, section_type, section_value, show_title,show_timer, hss.`layout_type`,addon_class
			FROM '._DB_PREFIX_.'sectionslider hs
			INNER JOIN '._DB_PREFIX_.'sectionslider_slides hss ON (hs.id_sectionslider_slides = hss.id_sectionslider_slides)
			INNER JOIN '._DB_PREFIX_.'sectionslider_slides_lang hssl ON (hss.id_sectionslider_slides = hssl.id_sectionslider_slides)
			WHERE hs.id_sectionslider_slides = '.(int)$sliderId.'
			AND hssl.id_lang = '.(int)$id_lang			
		);
  
        $slider = new claritassectionslider();       
        $slides = $slider->GetSlideProducts($slides, $sellerId);

        $this->context->smarty->assign(array('sectionslider_slides' => $slides, 
												'slide_img_pos' => $imagepos, 
												'sectionslider_views' => _PS_ROOT_DIR_."/modules/claritassectionslider/views",
												'hideprice' => $hidepriceStr, 
												'nolink' => $nolinkStr)
									);

        echo $this->context->smarty->fetch(_PS_MODULE_DIR_.'claritassectionslider/views/templates/front/mmyslider.tpl');
        die();
    }
}
