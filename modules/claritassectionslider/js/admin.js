$(document).ready(function() {
    //show new item panel
    $('select#section_type').on('change', function () {
        toggleSectionSliderSelectList($(this).val());
    });

    toggleSectionSliderSelectList($('select#section_type').val());
});

function toggleSectionSliderSelectList(section_type) {
    switch (section_type) {
        case "0":
            $("select#category_value").closest("div.form-group").show();
            $("select#category_value").prop('disabled', false);

            $("select#brand_value").closest("div.form-group").hide();
            $("select#brand_value").prop('disabled', true);
			
            $("select#voucher_value").closest("div.form-group").hide();
            $("select#voucher_value").prop('disabled', true);
			
			break;
        case "1":
            $("select#category_value").closest("div.form-group").hide();
            $("select#category_value").prop('disabled', true);

            $("select#brand_value").closest("div.form-group").show();
            $("select#brand_value").prop('disabled', false);
			
            $("select#voucher_value").closest("div.form-group").hide();
            $("select#voucher_value").prop('disabled', true);
			
            break;
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "9":
            $("select#category_value").closest("div.form-group").hide();
            $("select#category_value").prop('disabled', true);

            $("select#brand_value").closest("div.form-group").hide();
            $("select#brand_value").prop('disabled', true);
			
            $("select#voucher_value").closest("div.form-group").hide();
            $("select#voucher_value").prop('disabled', true);
			
            break;
		case "8":
            $("select#category_value").closest("div.form-group").show();
            $("select#category_value").prop('disabled', false);

            $("select#brand_value").closest("div.form-group").hide();
            $("select#brand_value").prop('disabled', true);
			
            $("select#voucher_value").closest("div.form-group").hide();
            $("select#voucher_value").prop('disabled', true);
			
            break;
        case "10":
            $("select#category_value").closest("div.form-group").hide();
            $("select#category_value").prop('disabled', true);

            $("select#brand_value").closest("div.form-group").hide();
            $("select#brand_value").prop('disabled', true);
			
            $("select#voucher_value").closest("div.form-group").show();
            $("select#voucher_value").prop('disabled', false);
			
            break;
    }
}

function SliderFilterChange(that, eid) {
    var filter = $(that).val();
    $("#sliderList").prepend("<span class='btnProgress'> (In Progress)</span>");

    $.ajax({
        type: 'GET',
        url: "../modules/claritassectionslider/ajax_sectionsliderlist.php?grouping_filter=" + filter + "&eid=" + eid,
        dataType: 'html',
        success: function (data) {
            $("#sliderList").html(data);
            BindSortable();
        },
    })
    .always(function () {
        $("span.btnProgress").remove();
    });

    return false;
}

function BindSortable() {
    var $mySlides = $("#slides");
    $mySlides.sortable({
        opacity: 0.6,
        cursor: "move",
        update: function () {
            var order = $(this).sortable("serialize") + "&action=updateSlidesPosition";
            $.post("../modules/claritassectionslider/ajax_claritassectionslider.php?secure_key=" + secslider_secure_key, order);
        }
    });
    $mySlides.hover(function () {
        $(this).css("cursor", "move");
    },
        function () {
            $(this).css("cursor", "auto");
        });
}