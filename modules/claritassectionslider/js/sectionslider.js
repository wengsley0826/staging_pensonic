$(document).ready(function () {
    sectionSlider.PopulateMMYSlider();
});

var sectionSlider = new Object();

sectionSlider.GetSliderTags = function() {
    return $(".mmyslider");
}

/* <div class="mmyslider" data-sliderid=6 data-imagepos="left|right" data-slidercount=3 data-sellerid=123/> */
sectionSlider.PopulateMMYSlider = function () {
    var sliders = sectionSlider.GetSliderTags();
    
    $.each(sliders, function (i, e) {
        var sliderid = $(e).data("sliderid");
        var imagepos = $(e).data("imagepos");
        var slidercount = $(e).data("slidercount");
        var sellerid = $(e).data("sellerid");
		
        var hideprice = $(e).data("hideprice");
        var nolink = $(e).data("nolink");
		
        $.ajax({
            type: 'GET',
            url: "/module/claritassectionslider/ajaxmmyslider",
            data: 'ajax=true&sliderid=' + sliderid + '&imagepos=' + imagepos + '&sellerid=' + sellerid  + '&hideprice=' + hideprice  + '&nolink=' + nolink,
            dataType: 'html',
            success: function (data) {
                $(e).html(data);

                var section = $("#section-slider .section_container:not(.section-grid) ul.section_slide" + sliderid, $(e));
                var visibleCount = 4;

                if (slidercount != undefined) {
                    visibleCount = slidercount;
                }
                else {
                    visibleCount = 4;
                    if (section.hasClass("section5")) {
                        visibleCount = 5;
                    }

                    if (section.hasClass("type5")) {
                        visibleCount = visibleCount - 1;
                    }
                }

                section.flexisel({
                    visibleItems: visibleCount,
                    flipPage: true,
                    animationSpeed: 500,
                    animationLoop: true,
                    clone: false,
                    autoPlay: false
                });

                $("img", e).unveil(200);
            },
        })
        .always(function () {
            $("span.emailBtnProgress").remove();
        });
    });
}