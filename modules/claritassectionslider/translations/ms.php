<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{sectionslider}prestashop>sectionslider_693b83f5eca43e2bb1675287c37ce9e2'] = 'Gelangsar imej pada laman utama anda';
$_MODULE['<{sectionslider}prestashop>sectionslider_c17aed434289cedd02618451e12c8da6'] = 'Tambah gelangsar imej di laman utama anda.';
$_MODULE['<{sectionslider}prestashop>sectionslider_3f80dc2cdd06939d4f5514362067cd86'] = 'Nilai tidak sah';
$_MODULE['<{sectionslider}prestashop>sectionslider_a6abafe564d3940cc36ee43e2f09400b'] = 'Gelangsar tidak sah';
$_MODULE['<{sectionslider}prestashop>sectionslider_e0ce30bfbf90d2306ecf72f06a83133f'] = 'Keadaan gelangsar tidak sah.';
$_MODULE['<{sectionslider}prestashop>sectionslider_9f79795e050649dc6b8bd0cdc874cbdc'] = 'Kedudukan gelangsar tidak sah.';
$_MODULE['<{sectionslider}prestashop>sectionslider_14f09fd0804a8f1cd0eb757125fc9c28'] = 'Tajuk terlalu panjang.';
$_MODULE['<{sectionslider}prestashop>sectionslider_dc89634d1d28cd4e055531e62047156b'] = 'Kapsyen terlalu panjang.';
$_MODULE['<{sectionslider}prestashop>sectionslider_4477f672766f6f255f760649af8bd92a'] = 'URL terlalu panjang.';
$_MODULE['<{sectionslider}prestashop>sectionslider_62239300ba982b06ab0f1aa7100ad297'] = 'Penerangan terlalu panjang.';
$_MODULE['<{sectionslider}prestashop>sectionslider_980f56796b8bf9d607283de9815fe217'] = 'Format URL tidak betul.';
$_MODULE['<{sectionslider}prestashop>sectionslider_73133ce32267e8c7a854d15258eb17e0'] = 'Nama fail tidak sah.';
$_MODULE['<{sectionslider}prestashop>sectionslider_349097dadf7e6b01dd2af601d54fd59a'] = 'Tajuk tidak ditetapkan.';
$_MODULE['<{sectionslider}prestashop>sectionslider_a9af2809b02444b9470f97dc66ba57a2'] = 'Kapsyen tidak ditetapkan.';
$_MODULE['<{sectionslider}prestashop>sectionslider_0f059227d0a750ce652337d911879671'] = 'URL tidak ditetapkan.';
$_MODULE['<{sectionslider}prestashop>sectionslider_8cf45ba354f4725ec8a0d31164910895'] = 'Imej tidak ditetapkan.';
$_MODULE['<{sectionslider}prestashop>sectionslider_7f82c65d548588c8d5412463c182e450'] = 'Konfigurasi tidak dapat dikemaskini.';
$_MODULE['<{sectionslider}prestashop>sectionslider_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Tatarajah yang baru';
$_MODULE['<{sectionslider}prestashop>sectionslider_7cc92687130ea12abb80556681538001'] = 'Kesalahan terjadi selama gambar upload.';
$_MODULE['<{sectionslider}prestashop>sectionslider_cdf841e01e10cae6355f72e6838808eb'] = 'Gelangsar tidak dapat diletak.';
$_MODULE['<{sectionslider}prestashop>sectionslider_eb28485b92fbf9201918698245ec6430'] = 'Gelangsar tidak dapat dikemaskini.';
$_MODULE['<{sectionslider}prestashop>sectionslider_b9f5c797ebbf55adccdd8539a65a0241'] = 'Tidak-aktif';
$_MODULE['<{sectionslider}prestashop>sectionslider_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Aktif';
$_MODULE['<{sectionslider}prestashop>sectionslider_792744786ed30c5623dd1cf0c16f4ffe'] = 'Pilih fail';
$_MODULE['<{sectionslider}prestashop>sectionslider_272ba7d164aa836995be6319a698be84'] = 'Kapsyen';
$_MODULE['<{sectionslider}prestashop>sectionslider_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Kenerangan';
$_MODULE['<{sectionslider}prestashop>sectionslider_93cba07454f06a4a960172bbd6e2a435'] = 'Ya';
$_MODULE['<{sectionslider}prestashop>sectionslider_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Tidak ada';
$_MODULE['<{sectionslider}prestashop>sectionslider_c9cc8cce247e49bae79f15173ce97354'] = 'Simpan';
$_MODULE['<{sectionslider}prestashop>sectionslider_f4f70727dc34561dfde1a3c529b6205c'] = 'Tetapan';
$_MODULE['<{sectionslider}prestashop>sectionslider_44877c6aa8e93fa5a91c9361211464fb'] = 'Kelajuan';
$_MODULE['<{sectionslider}prestashop>sectionslider_105b296a83f9c105355403f3332af50f'] = 'Jeda';
$_MODULE['<{sectionslider}prestashop>sectionslider_1e6a508c037fc42ef6155eeadbb80331'] = 'Main secara automatik';
$_MODULE['<{sectionslider}prestashop>form_92fbf0e5d97b8afd7e73126b52bdc4bb'] = 'Pilih fail';
$_MODULE['<{sectionslider}prestashop>list_c82324ebbcea34f55627a897b37190e3'] = 'Senarai gelangsar';
$_MODULE['<{sectionslider}prestashop>list_7dce122004969d56ae2e0245cb754d35'] = 'Sunting';
$_MODULE['<{sectionslider}prestashop>list_f2a6c498fb90ee345d997f888fce3b18'] = 'Padam';


return $_MODULE;
