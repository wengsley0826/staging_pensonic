	{if isset($sectionslider_slides)}
		<div id="section-slider">
			{foreach from=$sectionslider_slides item=slide name=sectionslideloop}
				{if ((isset($slide.products) && count($slide.products) > 0) or $slide.section_type==7)}
				{if $slide.section_type==8}
				<div class="row" style="height: 540px;overflow:hidden;">
					<h1 style="color: #36a89f!important; font-weight: bold; font-size: 40px; font-family: 'Lato', sans-serif;">{$slide.title|escape:'htmlall':'UTF-8'}</h1>
					
					{foreach from=$slide.products item=product}
					
						<div class="col-lg-2 col-md-2 col-xs-4" style="padding-left: 5px; padding-right: 5px;text-align:center"><a href="/brands/{$product.link}" target="_blank" rel="noopener"><img class="img-responsive" style="border:3px solid #FAFAFA;padding:20px" onerror="this.parent.parent.style.display='none'" src="{if isset($product.image)}{$product.image}{else}{$link->getMediaLink("/img/m/")}{$product.id_manufacturer}-large_default.jpg{/if}" alt="{$product.name|htmlspecialchars}" width="300"/></a>
	
						<h5 itemprop="name" class="product-title">
							<a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
								{$product["name"]|escape:'html':'UTF-8'}
							</a>
						</h5>
						</div>
					{/foreach}
				</div>
				{else if $slide.layout_type == "grid"}
					{include file="$sectionslider_views/templates/front/mmygrid.tpl" slide=$slide}
				{else}
				<div class="section_container {if (isset($slide.addon_class) && $slide.addon_class!="")}{$slide.addon_class}{/if}">
					{if $slide.show_title}
						{if $slide.hdr_image != ""}
							<div class="section_title_img" style="{if $slide.section_type!=7}border-color:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};{else}border:none;{/if}background:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};">
								<img src="" data-src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/header/`$slide.hdr_image|escape:'htmlall':'UTF-8'`")}" alt="{$slide.title|escape:'htmlall':'UTF-8'}" />
							</div>
						{else}
							<div class="section_title"><span>{$slide.title}</span></div>
						{/if}
					{/if}
					<div id="section-slider-wrapper" class="{if $slide.section_type==10}voucher-wrapper{/if}" style="{if $slide.section_type!=7}border-color:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};{else}border:none;{/if}" >
						{if $slide.image!="" && $slide.section_type!=10}
							<div class="section_img{if (isset($slide.mobile_image)&& $slide.mobile_image != "")}mobile_hidden{/if}" style="float:{$slide_img_pos};" >
							{if $slide.section_type==5}
								<a href="https://{$slide.url|escape:'html':'UTF-8'}&preview=1" title="{$slide.title|escape:'html':'UTF-8'}" target='_story'>
							{else if $slide.isBundle}
								<a href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">
							{else}
								<a href="{$slide.url|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">
							{/if}

							{if (isset($slide.mobile_image) && $slide.mobile_image != "")}
								<picture>
									<source media="(max-width: 580px)" srcset="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/m/`$slide.mobile_image|escape:'htmlall':'UTF-8'`")}">
									<source media="(min-width: 581px)" srcset="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/`$slide.image|escape:'htmlall':'UTF-8'`")}">
									<img
										src="" data-src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/`$slide.image|escape:'htmlall':'UTF-8'`")}"
										{if isset($slide.size) && $slide.size}{$slide.size}{else}width="100%" height="100%"{/if} 
										alt="{$slide.title|escape:'htmlall':'UTF-8'}" title="{$slide.title|escape:'htmlall':'UTF-8'}" 
									/>
								</picture>
								{*
								<img 
									src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/`$slide.image|escape:'htmlall':'UTF-8'`")}"
									{if isset($slide.size) && $slide.size} {$slide.size}{else} width="100%" height="100%"{/if} 
									alt="{$slide.title|escape:'htmlall':'UTF-8'}" 
									srcset="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/m/`$slide.mobile_image|escape:'htmlall':'UTF-8'`")} 580w,
											{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/`$slide.image|escape:'htmlall':'UTF-8'`")} 1024w"
									sizes="100vw"
								/>
								*}
							{else}
								<img 
									src="" data-src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/`$slide.image|escape:'htmlall':'UTF-8'`")}"
									{if isset($slide.size) && $slide.size} {$slide.size}{else} width="100%" height="100%"{/if} 
									alt="{$slide.title|escape:'htmlall':'UTF-8'}" 
								/>
							{/if}
							</a>
							</div>
						{else if $slide.section_type==10}
							<div class="section_voucher" style="float:{$slide_img_pos};" >
								<div class="voucher_desc">{$slide.description}</div>
								<!-- add voucher -->
								<a class="voucher_collect" href="#" codeid="{$slide.section_value}" onclick="saveVouchers({$slide.section_value}); return false;" title="{$slide.title|escape:'html':'UTF-8'}">
									{l s="Collect"}
								</a>
							</div>
						{/if}
						<div class="section_products {if $slide.image=="" && $slide.section_type!=10}section_full{/if}">
							<ul class="clearfix row section_slide{$slide.id_slide} {if $slide.image==""}section5{/if} {if $slide.section_type==7}section_content{/if} {if $slide.section_type==5}type5{/if}" >
							{if $slide.section_type==5}
							{foreach from=$slide.products item=product}
								<li>
									<div class="product-container" >
										<div class="left-block2">
											<div class="product-image-container">
												<a class="product_img_link2" href="{$product.url}&preview=1" title="{$product.post_title|escape:'html':'UTF-8'}" itemprop="url" target='_story'>								
													<img class="replace-2x img-responsive" src="" data-src="{$product.thumbnail}" alt="{$product.post_title|escape:'html':'UTF-8'}" title="{$product.post_title|escape:'html':'UTF-8'}" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">												
												<a class="product-name" href="{$product.url}&preview=1" title="{$product.post_title|escape:'html':'UTF-8'}" itemprop="url" target='_story'>
													{$product.post_title|escape:'html':'UTF-8'}
												</a>
											</h5>
											<div class="clear"></div>
										</div>
									</div>
								</li>			
							{/foreach}
							{else if $slide.section_type==9}
							
							{foreach from=$slide.products item=product}
								<li>
									<div class="product-container" >
										<div class="left-block2">
											<div class="product-image-container">
												<a class="product_img_link2 nuren-click-tag" presta-type='sectionslider-prod' href="{$product.url}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" target='_story'>								
													<img class="replace-2x img-responsive" src="" data-src="{$product.imgLink}" alt="{$product.name|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">												
												<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="{$product.url}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" target='_story'>
													{$product.name|escape:'html':'UTF-8'}
												</a>
											</h5>
											<div class="clear"></div>
										</div>
									</div>
								</li>			
							{/foreach}
							{else if $slide.section_type==6}
							
							{foreach from=$slide.products item=product}
								<li>
									<div class="product-container" itemscope="" itemtype="http://schema.org/Product">
										<div class="left-block">
											<div class="product-image-container">
													<a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
													{*
													{if $product['new_product'] OR $product['best_seller'] OR $product['free_deliver'] OR $product['best_price'] OR $product['custom_tag'] > 0}
													<div class="product-tag {if $product['new_product']}new-product{elseif $product['best_seller']}best-seller{elseif $product['free_deliver']}free-deliver{elseif $product['best_price']}best-price
														{elseif $product['custom_tag']==5}custom-tag5{elseif $product['custom_tag']==6}custom-tag6{elseif $product['custom_tag']==7}custom-tag7{elseif $product['custom_tag']==8}custom-tag8
														{/if}">
														<span class="tooltiptext">{if $product['new_product']}New Product{elseif $product['best_seller']}Best Seller{elseif $product['free_deliver']}Free Delivery{elseif $product['best_price']}Best Price Guaranteed{/if}</span>
													</div>
													{/if}
													*}
													<img class="replace-2x img-responsive" src="" data-src="{$product.imgLink|escape:'html':'UTF-8'}" alt="{$product["name"]|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block right-block-review">
											<h5 itemprop="name" class="product-title">
												<a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
													{$product["name"]|escape:'html':'UTF-8'}
												</a>
											</h5>
											<div class="clear"></div>
											<div class="reviews_list_stars">
												<span class="star_content clearfix">

													{section name="i" start=0 loop=5 step=1}
														{if $product.grade le $smarty.section.i.index}
															<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar2.png" class="list-img-star-category" alt="0"/>
														{else}
															<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar1.png" class="list-img-star-category" alt="0"/>
														{/if}
													{/section}

													<span style="vertical-align:top;line-height:16px;">({$product.grade|escape:'html':'UTF-8'})</span>
													<meta itemprop="worstRating" content = "0" />
													<meta itemprop="ratingValue" content = "{$product.grade|escape:'html':'UTF-8'}" />
													<meta itemprop="bestRating" content = "5" />
												</span>

											</div>
											<div class='line-height-default review-content-height'>
												{$product.content}
											</div>
											<div class='line-height-default'>
											<br>
												<strong itemprop="author">{$product.customer_name|escape:'html':'UTF-8'} </strong>
											</div>
									</div>
								</li>			
							{/foreach}
							{else if $slide.section_type==7}
								<div class="section-content col-xs-12 col-sm-12">{$slide.description}</div>
							{else}
							{foreach from=$slide.products item=product}
								<li>
									<div class="product-container" itemscope="" itemtype="http://schema.org/Product" style="display: inline-block;vertical-align: top;width: auto;">
										<div class="left-block">
											<div class="product-image-container">
												{if $slide.isBundle}
													<a class="product_img_link" href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
												{else}
													<a class="product_img_link" href="{$link->getProductLink($product.id_product,$product.link_rewrite)|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
												{/if}
													{*
													{if $product['new_product'] OR $product['best_seller'] OR $product['free_deliver'] OR $product['best_price'] OR $product['custom_tag'] > 0}
													<div class="product-tag {if $product['new_product']}new-product{elseif $product['best_seller']}best-seller{elseif $product['free_deliver']}free-deliver{elseif $product['best_price']}best-price
														{elseif $product['custom_tag']==5}custom-tag5{elseif $product['custom_tag']==6}custom-tag6{elseif $product['custom_tag']==7}custom-tag7{elseif $product['custom_tag']==8}custom-tag8
														{/if}">
														<span class="tooltiptext">{if $product['new_product']}New Product{elseif $product['best_seller']}Best Seller{elseif $product['free_deliver']}Free Delivery{elseif $product['best_price']}Best Price Guaranteed{/if}</span>
													</div>
													{/if}
													*}
													<img class="replace-2x img-responsive" src="" data-src="{$link->getImageLink($product['link_rewrite'], $product['id_image'], 'home_default')|escape:'html':'UTF-8'}" alt="{$product["name"]|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">
												{if $slide.isBundle}
													<a class="product-name" href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
												{else}
													<a class="product-name" href="{$link->getProductLink($product['id_product'],$product['link_rewrite'])|escape:'htmlall':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
												{/if}
													{$product["name"]|escape:'html':'UTF-8'}
												</a>
											</h5>
											<div class="clear"></div>
											<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
												<span class="price product-price">{convertPrice price=$product["price"]}</span>
												<meta itemprop="priceCurrency" content="{$currency->iso_code}">
												<meta itemprop="price" content="{$product["price"]}">
												{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
													<span class="old-price product-price">
														{displayWtPrice p=$product.price_without_reduction}
													</span>
													{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
													{if $product.specific_prices.reduction_type == 'percentage'}
														<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
													{elseif $product.specific_prices.reduction_type == 'amount' && round($product.specific_prices.reduction * 100.0 / $product.price_without_reduction, 0) >= 5}
														<span class="price-percent-reduction amount_reduction">-{round($product.specific_prices.reduction * 100.0 / $product.price_without_reduction, 0)}%</span>
													{/if}
												{/if}
											</div>
											<div class="button-container" style="border-top: 1px solid #eaeaea;"> 
												{if $product.id_product_attribute == 0 && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}							
													{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}								
														{capture}add=1&amp;id_product={$product.id_product|intval}
															{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}
															{if isset($static_token)}&amp;token={$static_token}{/if}
														{/capture} 
														<a class="button ajax_add_to_cart_button btn btn-default col-xs-6" 
															href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" 
															data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" 
															data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
															{*<img src="../../../img/icon-addcart.jpg" >*}
															<i class="icon-shopping-cart"></i>
															<span>{l s='Add to cart'}</span> 
														</a> 
													{else} 
														<span class="button ajax_add_to_cart_button btn btn-default col-xs-6" style="color:#ccc;">{l s='Out of Stock'}</span>
													{/if}	
												{else}					
													<span class="col-xs-6">&nbsp;</span>
												{/if}
												<a class="button detail_button btn-default col-xs-6" style="text-align:right;" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url"> 
													<i class="icon-list"></i>
													<span>{l s='Details'}</span> 
												</a>	
											</div>	
										</div>
									</div>
								</li>			
							{/foreach}
							{/if}
							</ul>
						</div>
                        <div class="clearfix"></div>
					</div>
 				</div>
				{/if}
				{/if}
			{/foreach}			
		</div>
		<div class="clearfix"></div>	
	{/if}
