<script>
 	theme_url='{$theme_url}';
  secslider_secure_key = '{$secslider_secure_key}';
</script>
<div class="panel"><h3><i class="icon-list-ul"></i> {l s='Section list' mod='claritassectionslider'}
	<span class="panel-heading-action">
    <span style="display:inline-block;vertical-align:top;padding-right: 10px;">
      <select id="grouping_filter" name="grouping_filter" onchange="return SliderFilterChange(this, {$eid});">
        {foreach from=$grouping_list item=grouping}
        <option value="{$grouping.grouping}" {if $filter == $grouping.grouping}selected{/if}>{$grouping.grouping}</option>
        {/foreach}
      </select>
    </span>
    <span style="display:inline-block;">
		  <a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure=claritassectionslider&addSlide=1">
			  <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Add new' mod='claritassectionslider'}" data-html="true">
				  <i class="process-icon-new "></i>
			  </span>
		  </a>
    </span>
	</span>
	</h3>
	<div id="slidesContent">
		<div id="slides" class="slider-unstyled">
		  {foreach from=$slides item=slide}
				<div id="slides_{$slide.id_slide}" class="panel">
					<div class="row">
						<div class="col-lg-1">
							<span><i class="icon-arrows "></i></span>
						</div>
						<div class="col-md-3">
              {if $slide.image!=""}
							  <img src="{$image_baseurl}{$slide.image}" alt="{$slide.title}" class="img-thumbnail" />
              {/if}
						</div>
						<div class="col-md-8">
							<h4 class="pull-left">
								#{$slide.id_slide} - {$slide.title}
								{if $slide.is_shared}
									<div>
										<span class="label color_field pull-left" style="background-color:#108510;color:white;margin-top:5px;">
											{l s='Shared slide' mod='claritassectionslider'}
										</span>
									</div>
								{/if}
							</h4>
							<div class="btn-group-action pull-right">
								{$slide.status}

								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure=claritassectionslider&id_slide={$slide.id_slide}">
									<i class="icon-edit"></i>
									{l s='Edit' mod='claritassectionslider'}
								</a>
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure=claritassectionslider&delete_id_slide={$slide.id_slide}">
									<i class="icon-trash"></i>
									{l s='Delete' mod='claritassectionslider'}
								</a>
							</div>
						</div>
					</div>
				</div>
			{/foreach}
		</div>
	</div>
</div>
