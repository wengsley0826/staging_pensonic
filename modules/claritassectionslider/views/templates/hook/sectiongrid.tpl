<div class="section_container section-grid {if $slide.addon_class!=""}{$slide.addon_class}{/if}">
	{if $slide.show_title}
		{if $slide.hdr_image != ""}
			<div class="section_title_img" style="{if $slide.section_type!=7}border-color:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};{else}border:none;{/if}background:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};">
				<img src="" data-src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/header/`$slide.hdr_image|escape:'htmlall':'UTF-8'`")}" alt="{$slide.title|escape:'htmlall':'UTF-8'}" />
			</div>
		{else}
			<div class="section_title"><span>{$slide.title}</span></div>
		{/if}
	{/if}
	<div class="more-link">
		{if $slide.section_type==5 && $slide.url != ""}
			<a class='nuren-presta-tag' presta-type='sectionslider' href="https://{$slide.url|escape:'html':'UTF-8'}&preview=1" title="{$slide.title|escape:'html':'UTF-8'}" target='_story'>{l s='More' mod='claritassectionslider'} ></a>
		{else if $slide.isBundle}
			<a class='nuren-presta-tag' presta-type='sectionslider' href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">{l s='More' mod='claritassectionslider'} ></a>
		{else if $slide.url != ""}
			<a class='nuren-presta-tag' presta-type='sectionslider' href="{$slide.url|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">{l s='More' mod='claritassectionslider'} ></a>
		{/if}
	</div>
	<div id="section-slider-wrapper">
		<div class="section_products {if $slide.image==""}section_full{/if}">
			<ul class="clearfix row section_slide{$slide.id_slide} {if $slide.image==""}section5{/if} {if $slide.section_type==7}section_content{/if}" >
			{if $slide.section_type==7}
				<div class="section-content col-xs-12 col-sm-12">{$slide.description}</div>
			{else}
				{foreach from=$slide.products item=product}
				<li class="col-xs-6 col-sm-4 col-md-3">
					{if $slide.section_type==5}
						<div class="product-container" >
							<div class="left-block2">
								<div class="product-image-container">
									<a class="product_img_link2 nuren-click-tag" presta-type='sectionslider-prod' href="{$product.url}&preview=1" title="{$product.post_title|escape:'html':'UTF-8'}" itemprop="url" target='_story'>								
										<img class="replace-2x img-responsive" src="" data-src="{$product.thumbnail}" alt="{$product.post_title|escape:'html':'UTF-8'}" title="{$product.post_title|escape:'html':'UTF-8'}" itemprop="image">
									</a>
								</div>
							</div>
							<div class="right-block">
								<h5 itemprop="name" class="product-title">												
									<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="{$product.url}&preview=1" title="{$product.post_title|escape:'html':'UTF-8'}" itemprop="url" target='_story'>
										{$product.post_title|escape:'html':'UTF-8'}
									</a>
								</h5>
								<div class="clear"></div>
							</div>
						</div>		
					{else if $slide.section_type==6}
						<div class="product-container" >
							<div class="left-block">
								<div class="product-image-container">
										<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
										{*
										{if $product['new_product'] OR $product['best_seller'] OR $product['free_deliver'] OR $product['best_price'] OR $product['custom_tag'] > 0}
											<div class="product-tag {if $product['new_product']}new-product{elseif $product['best_seller']}best-seller{elseif $product['free_deliver']}free-deliver{elseif $product['best_price']}best-price
												{elseif $product['custom_tag']==5}custom-tag5{elseif $product['custom_tag']==6}custom-tag6{elseif $product['custom_tag']==7}custom-tag7{elseif $product['custom_tag']==8}custom-tag8{/if}">
												<span class="tooltiptext">{if $product['new_product']}New Product{elseif $product['best_seller']}Best Seller{elseif $product['free_deliver']}Free Delivery{elseif $product['best_price']}Best Price Guaranteed{/if}</span>
											</div>
										{/if}
										*}
										<img class="replace-2x img-responsive" src="" data-src="{$product.imgLink|escape:'html':'UTF-8'}" alt="{$product["name"]|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="image">
									</a>
								</div>
							</div>
							<div class="right-block right-block-review">
								<h5 itemprop="name" class="product-title">
									<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="{$product.link|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
										{$product["name"]|escape:'html':'UTF-8'}
									</a>
								</h5>
								<div class="clear"></div>
								<div class="reviews_list_stars">
									<span class="star_content clearfix">
										{section name="i" start=0 loop=5 step=1}
											{if $product.grade le $smarty.section.i.index}
												<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar2.png" class="list-img-star-category" alt="0"/>
											{else}
												<img src="" data-src="https://media2.motherhood.com.my/modules/ratingsnippets/views/img/rstar1.png" class="list-img-star-category" alt="0"/>
											{/if}
										{/section}

										<span style="vertical-align:top;line-height:16px;">({$product.grade|escape:'html':'UTF-8'})</span>
										<meta itemprop="worstRating" content = "0" />
										<meta itemprop="ratingValue" content = "{$product.grade|escape:'html':'UTF-8'}" />
										<meta itemprop="bestRating" content = "5" />
									</span>

								</div>
								<div class='line-height-default review-content-height'>
									{$product.content}
								</div>
								<div class='line-height-default'>
								<br>
									<strong itemprop="author">{$product.customer_name|escape:'html':'UTF-8'} </strong>
								</div>
						</div>		
					{else}
						<div class="product-container" itemscope="" itemtype="http://schema.org/Product">
							<div class="left-block">
								<div class="product-image-container">
									{if $slide.isBundle}
										<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
									{else}
										<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="{$link->getProductLink($product.id_product,$product.link_rewrite)|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
									{/if}
										{*
										{if $product['new_product'] OR $product['best_seller'] OR $product['free_deliver'] OR $product['best_price'] OR $product['custom_tag'] > 0}
											<div class="product-tag {if $product['new_product']}new-product{elseif $product['best_seller']}best-seller{elseif $product['free_deliver']}free-deliver{elseif $product['best_price']}best-price
												{elseif $product['custom_tag']==5}custom-tag5{elseif $product['custom_tag']==6}custom-tag6{elseif $product['custom_tag']==7}custom-tag7{elseif $product['custom_tag']==8}custom-tag8{/if}">
												<span class="tooltiptext">{if $product['new_product']}New Product{elseif $product['best_seller']}Best Seller{elseif $product['free_deliver']}Free Delivery{elseif $product['best_price']}Best Price Guaranteed{/if}</span>
											</div>
										{/if}
										*}
										<img class="replace-2x img-responsive" src="" data-src="{$link->getImageLink($product['link_rewrite'], $product['id_image'], 'home_default')|escape:'html':'UTF-8'}" alt="{$product["name"]|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="image">
									</a>
								</div>
							</div>
							<div class="right-block">
								<h5 itemprop="name" class="product-title">
									{if $slide.isBundle}
										<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
									{else}
										<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="{$link->getProductLink($product['id_product'],$product['link_rewrite'])|escape:'htmlall':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
									{/if}
										{$product["name"]|escape:'html':'UTF-8'}
									</a>
								</h5>
								<div class="clear"></div>
								<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price">
									<span class="price product-price">{convertPrice price=$product["price"]}</span>
									<meta itemprop="priceCurrency" content="{$currency->iso_code}">
									<meta itemprop="price" content="{$product["price"]}">
									{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
										<span class="old-price product-price">
											{displayWtPrice p=$product.price_without_reduction}
										</span>
										{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
										{if $product.specific_prices.reduction_type == 'percentage'}
											<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
										{elseif $product.specific_prices.reduction_type == 'amount' && round($product.specific_prices.reduction * 100.0 / $product.price_without_reduction, 0) >= 5}
											<span class="price-percent-reduction amount_reduction">-{round($product.specific_prices.reduction * 100.0 / $product.price_without_reduction, 0)}%</span>
										{/if}
									{/if}
								</div>
							</div>
						</div>		
					{/if}
				</li>	
				{/foreach}
			{/if}	
			</ul>
		</div>
        <div class="clearfix"></div>
	</div>
</div>
			