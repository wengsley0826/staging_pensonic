{if $page_name =='index'}
    {if isset($sectionslider_slides)}
		<div id="section-slider">
			{foreach from=$sectionslider_slides item=slide name=sectionslideloop}
				{if ((isset($slide.products) && count($slide.products) > 0) or $slide.section_type==7)}
				
				{if $slide.layout_type == "grid"}
					{include file="$sectionslider_views/templates/hook/sectiongrid.tpl" slide=$slide}
				{else}
				<div class="section_container {if $slide.addon_class!=""}{$slide.addon_class}{/if}">
					{if $slide.show_title}
						{if $slide.hdr_image != ""}
							<div class="section_title_img" style="{if $slide.section_type!=7}border-color:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};{else}border:none;{/if}background:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};">
								<img src="" data-src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/header/`$slide.hdr_image|escape:'htmlall':'UTF-8'`")}" alt="{$slide.title|escape:'htmlall':'UTF-8'}" />
							</div>
						{else}
							<div class="section_title"><span>{$slide.title}</span></div>
						{/if}
					{/if}
					<div id="section-slider-wrapper" style="{if $slide.section_type!=7}border-color:{if $slide.bgColor==""}#ececec{else}{$slide.bgColor}{/if};{else}border:none;{/if}" >
						{if $slide.image!=""}
							<div class="section_img mobile_hidden" >
							{if $slide.section_type==5}
								<a class='nuren-presta-tag' presta-type='sectionslider' href="https://{$slide.url|escape:'html':'UTF-8'}&preview=1" title="{$slide.title|escape:'html':'UTF-8'}" target='_story'>
							{else if $slide.isBundle}
								<a class='nuren-presta-tag' presta-type='sectionslider' href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">
							{else}
								<a class='nuren-presta-tag testtest' presta-type='sectionslider' href="{$slide.url|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">
							{/if}								<img class='nuren-presta-tag' presta-type='sectionslider' 
									src="" data-src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/`$slide.image|escape:'htmlall':'UTF-8'`")}"
									{if isset($slide.size) && $slide.size}{$slide.size}{else}width="100%" height="100%"{/if} 
									alt="{$slide.title|escape:'htmlall':'UTF-8'}" title="{$slide.title|escape:'htmlall':'UTF-8'}" 
								/>							</a>
							</div>
						{/if}
						<div class="section_products {if $slide.image==""}section_full{/if}">							{if $slide.item_row_count > 1}								{assign var="colWidth" value=(99 / ($slide.item_page_count / $slide.item_row_count))}								{assign var="colWidth" value="calc(`$colWidth`% - 16px)"}
							{else}								{assign var="colWidth" value="auto"}							{/if}							<ul class="clearfix row section_slide{$slide.id_slide} {if $slide.image==""}section5{/if} {if $slide.section_type==7}section_content{/if} {if $slide.item_row_count > 1}multirow{/if}" data-slidecount="{$slide.item_page_count}" data-rowcount="{$slide.item_row_count}">							{foreach from=$slide.products item=product name=productcounter}								{if $slide.item_row_count > 1}
									{if $smarty.foreach.productcounter.iteration % $slide.item_page_count == 1}
										<li style="width:100%;">
									{/if}
									{if $smarty.foreach.productcounter.iteration % ($slide.item_page_count / $slide.item_row_count) == 1 || $slide.item_page_count == $slide.item_row_count}
										<div class="row{ceil($smarty.foreach.productcounter.iteration / ($slide.item_page_count / $slide.item_row_count))}">
									{/if}
								{else}
								<li>								{/if}

									<div class="product-container" itemscope="" itemtype="http://schema.org/Product" style="display:inline-block;vertical-align:top;width: {$colWidth}">
										<div class="left-block">
											<div class="product-image-container">
												{if $slide.isBundle}
													<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
												{else}
													<a class="product_img_link nuren-click-tag" presta-type='sectionslider-prod' href="{$link->getProductLink($product.id_product,$product.link_rewrite)|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">													
												{/if}
													{*
                          {if $product['new_product'] OR $product['best_seller'] OR $product['free_deliver'] OR $product['best_price'] OR $product['custom_tag'] > 0}
													  <div class="product-tag {if $product['new_product']}new-product{elseif $product['best_seller']}best-seller{elseif $product['free_deliver']}free-deliver{elseif $product['best_price']}best-price
														  {elseif $product['custom_tag']==5}custom-tag5{elseif $product['custom_tag']==6}custom-tag6{elseif $product['custom_tag']==7}custom-tag7{elseif $product['custom_tag']==8}custom-tag8{/if}">
														  <span class="tooltiptext">{if $product['new_product']}New Product{elseif $product['best_seller']}Best Seller{elseif $product['free_deliver']}Free Delivery{elseif $product['best_price']}Best Price Guaranteed{/if}</span>
													  </div>
													{/if}
													*}
                          <img class="replace-2x img-responsive" src="" data-src="{$link->getImageLink($product['link_rewrite'], $product['id_image'], 'home_default')|escape:'html':'UTF-8'}" alt="{$product["name"]|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="image">
												</a>
											</div>
										</div>
										<div class="right-block">
											<h5 itemprop="name" class="product-title">
												{if $slide.isBundle}
													<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
												{else}
													<a class="product-name nuren-click-tag" presta-type='sectionslider-prod' href="{$link->getProductLink($product['id_product'],$product['link_rewrite'])|escape:'htmlall':'UTF-8'}" title="{$product["name"]|escape:'html':'UTF-8'}" itemprop="url">
												{/if}
													{$product["name"]|escape:'html':'UTF-8'}
												</a>
											</h5>
											<div class="clear"></div>
											<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price" style="height:30px;">
												<span class="price product-price">{convertPrice price=$product["price"]}</span>
												<meta itemprop="priceCurrency" content="{$currency->iso_code}">
												<meta itemprop="price" content="{$product["price"]}">
												{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
													<span class="old-price product-price">
														{displayWtPrice p=$product.price_without_reduction}
													</span>
													{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
													{if $product.specific_prices.reduction_type == 'percentage'}
														<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
													{elseif $product.specific_prices.reduction_type == 'amount' && round($product.specific_prices.reduction * 100.0 / $product.price_without_reduction, 0) >= 5}
														<span class="price-percent-reduction amount_reduction">-{round($product.specific_prices.reduction * 100.0 / $product.price_without_reduction, 0)}%</span>
													{/if}
												{/if}
											</div>											<div class="button-container" style="border-top: 1px solid #eaeaea;"> 												{if $product.id_product_attribute == 0 && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}																				{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}																						{capture}add=1&amp;id_product={$product.id_product|intval}															{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}															{if isset($static_token)}&amp;token={$static_token}{/if}														{/capture} 														<a class="button ajax_add_to_cart_button btn btn-default col-xs-6" 															href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" 															data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" 															data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">															{*<img src="../../../img/icon-addcart.jpg" >*}															<i class="icon-shopping-cart"></i>															<span>{l s='Add to cart'}</span> 														</a> 													{else} 														<span class="button ajax_add_to_cart_button btn btn-default col-xs-6" style="color:#ccc;">{l s='Out of Stock'}</span>													{/if}													{else}																		<span class="col-xs-6">&nbsp;</span>												{/if}												<a class="button detail_button btn-default col-xs-6" style="text-align:right;" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url"> 													<i class="icon-list"></i>													<span>{l s='Details'}</span> 												</a>	
											</div>	
											{if $slide.show_timer == 1}
											<div class="ticker_box">
												<div class="sales_end_label">Time Left</div>
												<div class="sales_end dsCountDown ds-custome" k-data="{$slide.end_datetime}"></div>
											</div>
											{else}{/if}
										</div>
									</div>
								{if $slide.item_row_count > 1}
									{if $smarty.foreach.productcounter.iteration % ($slide.item_page_count / $slide.item_row_count) == 0  || $slide.item_page_count == $slide.item_row_count}
										</div>
									{/if}
									{if $smarty.foreach.productcounter.iteration % ($slide.item_row_count * ($slide.item_page_count / $slide.item_row_count)) == 0 || $smarty.foreach.productcounter.iteration == count($slide.products)}
										</li>										{/if}									
								{else}
									</li>									{/if}							{/foreach}
							</ul>
						</div>
                        <div class="clearfix"></div>
						{if $slide.mobile_image != ""}
							<div class="section_img desktop_hidden" >
							{if $slide.section_type==5}
								<a class='nuren-presta-tag' presta-type='sectionslider' href="https://{$slide.url|escape:'html':'UTF-8'}&preview=1" title="{$slide.title|escape:'html':'UTF-8'}" target='_story'>
							{else if $slide.isBundle}
								<a class='nuren-presta-tag' presta-type='sectionslider' href="/bundles/{$slide.cat_rewrite|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">
							{else}
								<a class='nuren-presta-tag testtest' presta-type='sectionslider' href="{$slide.url|escape:'html':'UTF-8'}" title="{$slide.title|escape:'html':'UTF-8'}">
							{/if}
								<picture>
									<source media="(max-width: 580px)" srcset="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/m/`$slide.mobile_image|escape:'htmlall':'UTF-8'`")}">									<img class='nuren-presta-tag' presta-type='sectionslider' 
										src="" data-src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`claritassectionslider/images/m/`$slide.mobile_image|escape:'htmlall':'UTF-8'`")}"
										{if isset($slide.size) && $slide.size}{$slide.size}{else}width="100%" height="100%"{/if} 
										alt="{$slide.title|escape:'htmlall':'UTF-8'}" title="{$slide.title|escape:'htmlall':'UTF-8'}" 
									/>
								</picture>								</a>
							</div>
						{/if}

					</div>
 				</div>
				{/if}
				{/if}
			{/foreach}			
		</div>
		<div class="clearfix"></div>
		<script>		
		$(document).ready(function(){
			{foreach from=$sectionslider_slides item=slide}
				{if $slide.layout_type == "slide"}					var section = $("#section-slider ul.section_slide{$slide.id_slide}");
					var visibleCount = 4;					var rowCount = 1;

					if(section.data("rowcount") !== undefined) {
						rowCount = section.data("rowcount");
					}					
					if(rowCount == 1) {
						if(section.data("slidecount") !== undefined) {							visibleCount = section.data("slidecount");
						}
					}
					else {
						visibleCount = 1;
					}

					var enableResponsiveBreakpoints = false;

					
					{if $slide.item_row_count == 1}
						enableResponsiveBreakpoints = true;
					{/if}
					
					if(!section.hasClass("section_content")) {
						$("#section-slider ul.section_slide{$slide.id_slide}").flexisel({
						{if $slide.section_type==5}
							visibleItems: visibleCount - 1,
						{else}
							visibleItems: visibleCount,
						{/if}
							flipPage: true,
							animationSpeed: 500,
							animationLoop :true,
							clone: false,
							autoPlay: false,							enableResponsiveBreakpoints:enableResponsiveBreakpoints
						});
					}
			{/if}
			{/foreach}
		});		
		</script>
	{/if}
{/if}