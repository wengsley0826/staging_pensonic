<?php
/**
* 2012-2017 Qualiteam
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@x-cart.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade CloudSearch to newer
* versions in the future. If you wish to customize CloudSearch for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Qualiteam software Ltd <info@x-cart.com>
*  @copyright 2007-2017 Qualiteam
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

if (!defined('_PS_VERSION_'))
	exit;

class CloudSearch extends Module
{
	const CLOUD_SEARCH_DOMAIN = 'cloudsearch.x-cart.com';
	const CLOUD_SEARCH_REGISTER_URL = '/api/v1/register';
	const CLOUD_SEARCH_REQUEST_SECRET_KEY_URL = '/api/v1/getkey';
	const CLOUD_SEARCH_REMOTE_IFRAME_URL = '/api/v1/iframe?key=';

	public function __construct()
	{
		$this->name = 'cloudsearch';
		$this->tab = 'front_office_features';
		$this->version = '1.1.8';
		$this->author = 'Qualiteam';
		$this->need_instance = 1;
		$this->dependencies = array();
		$this->module_key = '386d2ac486367cce38e060af3f632630';

		parent::__construct();

		$this->displayName = 'CloudSearch';
		$this->description = 
			'CloudSearch is the advanced search for PrestaShop that adds live product search with quick and highly relevant search results.
                Power up your store with enterprise-class search tools for stunning conversion rate!';

		$this->confirmUninstall = 'Are you sure you want to uninstall?';

		if (!Configuration::get('CLOUDSEARCH_API_KEY'))
			$this->warning = 'CloudSearch was not installed properly. Please re-install the module.';
	}

	public function getContent()
	{
		$this->context->controller->addJS($this->_path.'js/cloud_search_prestashop_admin.js');
		$this->context->controller->addCSS($this->_path.'css/cloudsearch_admin.css');

		$token = Tools::getAdminTokenLite('AdminCloudSearchDashboard');

		return '<div class="cloud-search-iframe-base" data-token="'.$token.'"></div>';
	}

	public function install()
	{
		if (parent::install() == false)
			return false;

		$opts = array(
			'http' => array(
				'method'	=> 'POST',
				'content'	=> array(),
				'header'	=> 'Content-type: application/x-www-form-urlencoded',
			)
		);

		$context = stream_context_create($opts);

		$url_obj = ShopUrl::getShopUrls($this->context->shop->id)->where('main', '=', 1)->getFirst();
		$shop_url = 'http://'.$url_obj->domain.$url_obj->physical_uri;

		$register_url = 'http://'.self::CLOUD_SEARCH_DOMAIN.self::CLOUD_SEARCH_REGISTER_URL;
		$register_url .= '?shopUrl='.urlencode($shop_url);
		$register_url .= '&shopType=prestashop';
		$register_url .= '&format=php';

		$result = Tools::file_get_contents($register_url, false, $context);

		$api_key_installed = false;

		if ($result)
		{
			$data = unserialize($result);

			if ($data && !empty($data['apiKey']))
			{
				Configuration::updateValue('CLOUDSEARCH_API_KEY', $data['apiKey']);

				$api_key_installed = true;
			}
		}

		$this->registerHook('displayHeader');
		$this->registerHook('displayMobileHeader');

		$this->installTab();

		return $api_key_installed;
	}

	public function uninstall()
	{
		return parent::uninstall()
			&& $this->uninstallTab()
			&& Configuration::deleteByName('CLOUDSEARCH_API_KEY')
			&& Configuration::deleteByName('CLOUDSEARCH_SECRET_KEY');
	}

	public function installTab()
	{
		$tab = new Tab();
		$tab->active = 1;
		$tab->name = array();
		$tab->class_name = 'AdminCloudSearchDashboard';

		foreach (Language::getLanguages(true) as $lang)
			$tab->name[$lang['id_lang']] = 'CloudSearch Dashboard';

		$tab->id_parent = -1;
		$tab->module = $this->name;

		return $tab->add();
	}

	public function uninstallTab()
	{
		$id_tab = (int)Tab::getIdFromClassName('AdminCloudSearchDashboard');

		if ($id_tab)
		{
			$tab = new Tab($id_tab);
			$tab->delete();
		}

		return true;
	}

	public function hookDisplayHeader()
	{
		$this->context->controller->addJS($this->_path.'js/handlebars.min.js');
		$this->context->controller->addjQueryPlugin('hoverIntent');

		$this->context->controller->addJqueryUI('ui.position');
		$this->context->controller->addJqueryUI('effects.core');

		$this->context->controller->addCSS($this->_path.'css/cloudsearch.css');

		$this->context->controller->addJS($this->_path.'js/cloud_search_prestashop.js');

		$this->context->controller->addJS($this->_path.'js/disable_standard_autocomplete.js');

		return '
			<script type="text/javascript">
				var Cloud_Search = {
                    lang: {
                        lbl_products: "' . $this->l('lbl_products') . '",
                        lbl_categories: "' . $this->l('lbl_categories') . '",
                        lbl_manufacturers: "' . $this->l('lbl_manufacturers') . '",
                        lbl_pages: "' . $this->l('lbl_pages') . '",
                        lbl_did_you_mean: "' . $this->l('lbl_did_you_mean') . '",
                        lbl_see_details: "' . $this->l('lbl_see_details') . '",
                        lbl_see_more_results_for: "' . $this->l('lbl_see_more_results_for') . '",
                        lbl_showing_results_for: "' . $this->l('lbl_showing_results_for') . '",
                        lbl_suggestions: "' . $this->l('lbl_suggestions') . '"
                    },
					apiKey: "'.Configuration::get('CLOUDSEARCH_API_KEY').'",
					selector: "input[type=\'text\'].search_query, #search_widget input[type=\'text\']",
					shopType: "prestashop",
					price_template: "' . Tools::displayPrice(0.0) . '"
				};
			</script>
		';
	}

	public function hookDisplayMobileHeader()
	{
		return $this->hookDisplayHeader();
	}

	public function requestSecretKey()
	{
		$post_data = array(
			'apiKey' => Configuration::get('CLOUDSEARCH_API_KEY')
		);

		$context = stream_context_create(array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($post_data)
			)
		));

		$request_url = 'http://'.self::CLOUD_SEARCH_DOMAIN.self::CLOUD_SEARCH_REQUEST_SECRET_KEY_URL;

		Tools::file_get_contents($request_url, false, $context);

		Configuration::loadConfiguration();
	}

	public function storeSecretKeyValue($key)
	{
		Configuration::updateValue('CLOUDSEARCH_SECRET_KEY', $key);
	}

	public function redirectToDashboard()
	{
		$secret_key = Configuration::get('CLOUDSEARCH_SECRET_KEY');

		$url = 'https://'.self::CLOUD_SEARCH_DOMAIN.self::CLOUD_SEARCH_REMOTE_IFRAME_URL.$secret_key;

		Tools::redirect($url);
	}
}
