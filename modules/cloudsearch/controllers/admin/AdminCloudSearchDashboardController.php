<?php
/**
* 2012-2017 Qualiteam
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@x-cart.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade CloudSearch to newer
* versions in the future. If you wish to customize CloudSearch for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Qualiteam software Ltd <info@x-cart.com>
*  @copyright 2007-2017 Qualiteam
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class AdminCloudSearchDashboardController extends ModuleAdminController
{
	public function initContent()
	{
		$module = Module::getInstanceByName('cloudsearch');

		$module->requestSecretKey();

		$module->redirectToDashboard();

		die;
	}
}
