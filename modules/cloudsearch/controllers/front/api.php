<?php
/**
* 2012-2017 Qualiteam
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@x-cart.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade CloudSearch to newer
* versions in the future. If you wish to customize CloudSearch for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Qualiteam software Ltd <info@x-cart.com>
*  @copyright 2007-2017 Qualiteam
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class CloudSearchApiModuleFrontController extends ModuleFrontController
{
	const MAX_ENTITIES_AT_ONCE = 300;

	public function display()
	{
		Context::getContext()->language = new Language(Configuration::get('PS_LANG_DEFAULT'));

		$method = Tools::getValue('method');

		$data = null;

		if ($method == 'products')
			$data = $this->getProducts();
		elseif ($method == 'categories')
			$data = $this->getCategories();
		elseif ($method == 'manufacturers')
			$data = $this->getManufacturers();
		elseif ($method == 'pages')
			$data = $this->getPages();
		elseif ($method == 'info')
			$data = $this->getInfo();
		elseif ($method == 'set_secret_key')
			$data = $this->setSecretKey();

		if ($data !== null)
			$this->outputData($data);
	}

	protected function getProducts()
	{
		$products = Product::getProducts(
			$this->context->language->id,
			$this->getStartOffset(),
			$this->getLimit(),
			'id_product',
			'ASC',
			false,
			true
		);

		if (empty($products) || !is_array($products))
			$products = array();

		foreach ($products as $k => $p)
		{
			$product = new Product($p['id_product']);

			$image = Image::getCover($product->id);

			$image_src = $this->context->link->getImageLink(
				$product->link_rewrite[$this->context->language->id],
				$product->id.'-'.$image['id_image']
			);

			$category = new Category($p['id_category_default']);

            $productUrl = $this->context->link->getProductLink($product, null, $category->link_rewrite[$this->context->language->id]);

			$products[$k] = array(
				'id'			=> $p['id_product'],
				'sku'			=> $p['reference'],
				'name'			=> $p['name'],
				'description'	=> !empty($p['description']) ? $p['description'] : $p['description_short'],
				'url'			=> $productUrl,
				'image_src'		=> $image_src,
				'image_width'	=> 0,
				'image_height'	=> 0,
				'category'		=> array($category->getName()),
				'manufacturer'	=> $p['manufacturer_name'],
				'price'			=> $product->getPrice(),
			);
		}

		return $products;
	}

	protected function getCategories()
	{

		$categories = Category::getCategories(
            false,
            true,
            false,
            'AND c.id_category != ' . Configuration::get('PS_ROOT_CATEGORY'),
            '',
            ' LIMIT ' . $this->getStartOffset() . ', ' . $this->getLimit()
        );

		if (empty($categories) || !is_array($categories))
			$categories = array();

		$result = array();

		foreach ($categories as $cat)
		{
            $cat_obj = new Category($cat['id_category']);

            if ($cat_obj->id_image)
            {
                $image_src = $this->context->link->getCatImageLink(
                    $cat_obj->link_rewrite[$this->context->language->id],
                    $cat_obj->id_image
                );
            }
            else
                $image_src = null;

            $result[] = array(
                'id'			=> $cat['id_category'],
                'name'			=> $cat_obj->getName(),
                'description'	=> $cat['description'],
                'parent'		=> $cat['id_parent'] == Configuration::get('PS_ROOT_CATEGORY') ? 0 : $cat['id_parent'],
                'url'			=> $this->context->link->getCategoryLink($cat['id_category']),
                'image_src'		=> $image_src,
                'image_width'	=> 0,
                'image_height'	=> 0,
            );
		}

		return $result;
	}

	protected function getManufacturers()
	{
		$manufacturers = Manufacturer::getManufacturers();

		if (empty($manufacturers) || !is_array($manufacturers))
			$manufacturers = array();

		$manufacturers = array_slice($manufacturers, $this->getStartOffset(), $this->getLimit());

		foreach ($manufacturers as $k => $m)
		{
			$manufacturer = new Manufacturer($m['id_manufacturer']);

			$manufacturers[$k] = array(
				'id'			=> $m['id_manufacturer'],
				'name'			=> $m['name'],
				'description'	=> !empty($m['description']) ? $m['description'] : $m['description_short'],
				'url'			=> $this->context->link->getManufacturerLink($manufacturer),
				'image_src'		=> null,
				'image_width'	=> 0,
				'image_height'	=> 0,
			);
		}

		return $manufacturers;
	}

	protected function getPages()
	{
		$pages = CMS::getCMSPages($this->context->language->id);

		if (empty($pages) || !is_array($pages))
			$pages = array();

		$result = array();

		foreach ($pages as $page)
		{
			$result[] = array(
				'id'		=> $page['id_cms'],
				'title'		=> $page['meta_title'],
				'content'	=> $page['content'],
				'url'		=> $this->context->link->getCMSLink((int)$page['id_cms'], $page['link_rewrite']),
			);
		}

		return $result;
	}

	protected function getInfo()
	{
		$product_count_sql = '
			SELECT COUNT(DISTINCT p.id_product)
			FROM `'._DB_PREFIX_.'product` p
			'.Shop::addSqlAssociation('product', 'p').
			'WHERE product_shop.`visibility` IN ("both", "catalog") AND product_shop.`active` = 1
		';

		$root_category = (int)Configuration::get('PS_ROOT_CATEGORY');

		$category_count_sql = '
			SELECT COUNT(DISTINCT c.id_category)
			FROM `'._DB_PREFIX_.'category` c
			'.Shop::addSqlAssociation('category', 'c').'
			WHERE `active` = 1 AND c.`id_category` != '.$root_category.'
		';

		$manufacturer_count_sql = '
			SELECT COUNT(DISTINCT m.id_manufacturer)
			FROM `'._DB_PREFIX_.'manufacturer` m
			'.Shop::addSqlAssociation('manufacturer', 'm').'
			WHERE m.`active` = 1
		';

		$num_products = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($product_count_sql);
		$num_categories = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($category_count_sql);
		$num_manufacturers = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($manufacturer_count_sql);
		$num_pages = count($this->getPages());

		return array(
			'numProducts'		=> (int)$num_products,
			'numCategories'		=> (int)$num_categories,
			'numManufacturers'	=> (int)$num_manufacturers,
			'numPages'			=> $num_pages,
			'productsAtOnce'	=> self::MAX_ENTITIES_AT_ONCE,
		);
	}

	protected function outputData($data)
	{
		header('Content-type: application/php');

		echo serialize($data);
	}

	protected function getStartOffset()
	{
        $start = Tools::getValue('start');

        return !empty($start) ? max(0, (int)$start) : 0;
	}

	protected function getLimit()
	{
        $limit = Tools::getValue('limit');
		$limit = !empty($limit) ? (int)$limit : self::MAX_ENTITIES_AT_ONCE;

		return max(min($limit, self::MAX_ENTITIES_AT_ONCE), 0);
	}

	protected function setSecretKey()
	{
		$module = Module::getInstanceByName('cloudsearch');

		if (Tools::getValue('key') && Tools::getValue('signature'))
		{
			$key = Tools::getValue('key');
			$signature = base64_decode(Tools::getValue('signature'));

			if ($this->isSignatureCorrect($key, $signature))
				$module->storeSecretKeyValue($key);
		}

		return array();
	}

	protected function isSignatureCorrect($data, $signature)
	{
		$result = false;

		if (function_exists('openssl_get_publickey'))
		{
			$pub_key_id = openssl_get_publickey($this->getPublicKey());

			$result = openssl_verify($data, $signature, $pub_key_id) == 1;
		}

		return $result;
	}

	protected function getPublicKey()
	{
		return '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+sJv3R+kKUl0okgi7HoN6sGcM
4Lyp4LMkMYqwD0hK618lJwydI5PRMj3+vmCxVZcnoiAM/8XwGmH24y2s7D2/8/co
K55PFPn6T0V5++5oyyObofPe08kDoW6Ft2+yNcshmg1Vd711Vd37LLXWsaWpfcjr
82cfYTelfejE4IO5NQIDAQAB
-----END PUBLIC KEY-----';
	}
}
