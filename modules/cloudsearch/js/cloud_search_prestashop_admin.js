// CloudSearch for PrestaShop

(function ($) {

	var loadDashboard = function () {
		$(function () {
			if ($('.cloud-search-iframe-wrapper').length == 0) {
				var base = $('.cloud-search-iframe-base'),
					token = base.attr('data-token'),
					src = 'index.php?controller=AdminCloudSearchDashboard&token=' + token,
					iframe = $('<iframe class="cloud-search-iframe" src="' + src + '" frameBorder="0" scrolling="no">'),
					loadProgress = $('<div class="progress progress-striped active cloud-search-load-progress"><div class="progress-bar" style="width: 100%;"></div></div>');

				iframe.wrap($('<div class="cloud-search-iframe-wrapper">'));

				base.prepend(iframe.parent());

				base.prepend(loadProgress);

				var wrapper = iframe.parent(),
					doPreloader = (typeof wrapper.block == 'function') && (typeof window.postMessage == 'function');

				if (doPreloader)
					wrapper.block({baseZ: 10});

				function messageListener(e) {
					if (e.origin == 'https://cloudsearch.x-cart.com'
						|| e.origin == 'https://dev.cloudsearch.x-cart.com') {

						if (e.data == 'loaded') {
							doPreloader && wrapper.unblock();

							loadProgress.hide();
						}
					}
				}

				if (window.addEventListener)
					addEventListener('message', messageListener, false);
				else 
					attachEvent('onmessage', messageListener);
			}
		});
	};

	loadDashboard();

})(jQuery);
