$(document).ready(function () {
    setTimeout(function () {
        var $searchBox = $('#search_widget input[type=text]');

        if ($searchBox.psBlockSearchAutocomplete) {
            $searchBox.psBlockSearchAutocomplete('destroy');
        }
    });
});
