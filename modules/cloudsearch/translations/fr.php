<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cloudsearch}prestashop>cloudsearch_34ddc32943116e5e9d3a607090099cff'] = 'Produits';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_c4c04d71f1716cf5d52940cbf5fd5a99'] = 'Catégories';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_f5a58488acc1e30f3475e47317d9f254'] = 'Fabricants';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_8e2a5e8124417dd4b607123ea1b959bf'] = 'Pages';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_d79ff70e9c03c2e131dfc5995f688252'] = 'Vouliez-vous dire ceci';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_6c9073b010130fd07aa8f5afaf4b5ea5'] = 'Plus de détails';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_f6e96f4210e6ad221a2570b961b106a5'] = 'Voici d\'autres résultats pour';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_daa2eedaebc268a42bb354ee5b957d39'] = 'Voici les résultats de votre recherche';
$_MODULE['<{cloudsearch}prestashop>cloudsearch_4dc53063764ae35503b88a6ed9805477'] = 'Suggestions';
