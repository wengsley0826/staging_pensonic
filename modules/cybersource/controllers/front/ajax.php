
<?php

if (!class_exists('cybersource')) {
 include(dirname(__FILE__).'/../../cybersource.php');
 }

class CyberSourceAjaxModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        if (isset($_REQUEST['ajax']) && isset($_REQUEST["type"]) && isset($_REQUEST["orderId"]) && isset($_REQUEST["employeeId"])) {
            $type = $_REQUEST["type"];
            $orderId = $_REQUEST["orderId"];
            $employeeId = $_REQUEST["employeeId"];
            $to_status = $_REQUEST["toStatus"];
            $obj = new cybersource();
            $result = $obj->ajaxCall($type, $orderId, $employeeId, $to_status);
            echo $result;
        }
        else {
            echo false;
        }
        die();
    }
}