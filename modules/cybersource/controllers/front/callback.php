<?php

class CyberSourceCallbackModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $response = array();
        
        foreach($_REQUEST as $key => $val) { 
            $response[$key] = $val;
        }

        $csId = $response["req_reference_number"];
        $transId = "";
        $payment_status = "";
        
        if(isset($response["reason_code"])) {
            $payment_status = $response["reason_code"];
        }
        if(isset($response["transaction_id"])) {
            $transId = $response["transaction_id"];
        }
    
        $logId = $this->SaveLog($_REQUEST, $csId, $payment_status, $transId);
        $logResult = "";
        sleep(1);
        
        $id_cart = $this->getCartIdByCSId($csId);
        $redirect_checkout = false;
        
        if($this->CheckDuplicateLog($logId))
        {
            if($payment_status == "100") 
            {
                $maxRetry = 30;
                $retry_cnt = 1;
                sleep(10);
                $o = $this->getOrderId($id_cart);
                while ($o <= 0 && $retry_cnt < $maxRetry) {
                    $o = $this->getOrderId($id_cart);
                    $retry_cnt++;
                    sleep(5);    
                }

                //if($o > 0) 
                {
                    exit;
                }
            }
            else {
                $o = 0;
                exit;
            }            
        }
        
        if (empty($id_cart)) {
            exit;
        }    
        else {
            $cart = new Cart($id_cart);
        }
        
        $customer = new Customer($cart->id_customer);
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $amount = (float)$response['req_amount'];
        $secureHash = $this->sign($response);
        
        if($secureHash == $response['signature']) {
            print_r("Valid Hash");
            
            if($payment_status == "100") {
                $logResult .= "Process Success Order.";
                $this->UpdateLog($logId, $logResult);
                
                if(!$this->processOrder($id_cart, $amount, $transId, Configuration::get('PS_OS_PAYMENT'), $csId)) 
                {
                    $this->module->validateOrder($id_cart, Configuration::get('PS_OS_PAYMENT'), $amount, $this->module->displayName, 
                        'CyberSource Transaction ID: ' . $transId. ' (CyberSource Id: '. $csId. ')', NULL, (int)$cart->id_currency, false, $customer->secure_key);
                    $logResult .= "||Order Created";
                    $this->UpdateLog($logId, $logResult);
                    
                    //// check total amount. if not same, update order status
                    if(strval($total) != strval($amount)) 
                    {
                        $logResult .= "||Upate Order Status";
                        $this->UpdateLog($logId, $logResult);          
                        $this->updateOrderValidation($orderid);
                    }
                }
            }
            else {
                $this->processOrder($id_cart, $amount, $transId, Configuration::get('PS_OS_ERROR'), $csId);
                $logResult .= "Payment Failed";
                $logResult .= "||Redirected";
                $redirect_checkout = true;
                $this->UpdateLog($logId, $logResult);
            }
        }
        else {
            $payment_status = "hash";
            $logResult .= "Invalid Hash";
            $logResult .= "||Redirected";
            $redirect_checkout = true;
            $this->UpdateLog($logId, $logResult);
        }
        
        if($redirect_checkout)
        {
            $logResult .= "||Redirect Payment Result";
            $this->UpdateLog($logId, $logResult);          
            
        }
        else {
            $logResult .= "||Before update Payment Result";
            $this->UpdateLog($logId, $logResult);          
            
            $maxRetry = 30;
            $retry_cnt = 1;
              
            $o = $this->getOrderId($id_cart);
            while ($o <= 0 && $retry_cnt < $maxRetry) {
                $o = $this->getOrderId($orderid);
                $retry_cnt++;
                sleep(5);    
            }
            

            $this->UpdatePaymentResult($csId, $o, $payment_status, $transId);     
            $logResult .= "||Update Payment Result (o=>". $o.")";
            $this->UpdateLog($logId, $logResult);          
              
        }
        
        exit;
    }
    
    private function UpdatePaymentResult ($csId, $id_order, $payment_status, $transId) {
        $sql = 'UPDATE `'._DB_PREFIX_.'cybersource_cart` '.
                'SET payment_status="'.$payment_status.'",'.
                    'transaction_no="'.$transId.'",'.
                    'id_order="'.$id_order.'" '.
                'WHERE csId='.(int)$csId;
        return Db::getInstance()->execute($sql);
    }
    
    private function SaveLog ($data, $csId, $paymen_status, $transId)
    {
        $postdata = json_encode($data);
        $postdata = str_replace('"', '\"', $postdata);
        
        $sql = 'INSERT INTO `'._DB_PREFIX_.'payment_callback_log` (`module`, `file`,`orderId`, `payment_status`,`transaction_no`,`postdata`,`createdTS`) '.
                'VALUES ("CyberSource", "callback","'.$csId.'","'.$paymen_status.'","'.$transId.'","'.$postdata.'", NOW())';
        $return = Db::getInstance()->execute($sql);
        return Db::getInstance()->Insert_ID();
    }
    
    private function getCartIdByCSId($csId) {
        $sql = 'SELECT id_cart FROM `'. _DB_PREFIX_ .'cybersource_cart` WHERE csId = '. $csId;
        $cartId = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        
        return $cartId;
    }
    
    private function getOrderId($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        if ($row){
            return $row["id_order"];
        }
        else {
            return 0;
        }       
    }
    
    private function CheckDuplicateLog($logId)
    {
        $tempSQL = "SELECT COUNT(1) AS logCount FROM `". _DB_PREFIX_ ."payment_callback_log` A ".
                    "INNER JOIN `". _DB_PREFIX_ ."payment_callback_log` B ON ".
                        "A.payment_status = B.payment_status AND A.transaction_no=B.transaction_no AND ".
                        "A.orderId=B.orderId AND A.id < B.id ".
                    "WHERE B.id=". (int)$logId;
        $stateCount = Db::getInstance()->getRow($tempSQL);
      
        if($stateCount["logCount"] > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private function UpdateLog($logId, $logVal) {
        $sql = 'UPDATE `'._DB_PREFIX_.'payment_callback_log` SET result="'.$logVal.'" WHERE id='.(int)$logId;
        $return = Db::getInstance()->execute($sql);
    }
    
    private function processOrder($id_cart, $amount, $tranID, $id_order_state, $csId) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
        if ($row){
            return $this->module->updateValidatedOrder(
                $id_cart, $row["id_order"], $id_order_state, $amount, $this->module->displayName, 
                'CyberSource Transaction ID: ' . $tranID. ' (CyberSource Id: '. $csId. ')', NULL, false);
        }
        else {
            return false;
        }    
    }
    
    private function updateOrderValidation($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order,current_state FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        if ($row && $row["current_state"] != Configuration::get('PS_OS_AWAITING')){
            $new_history = new OrderHistory();
            $new_history->id_order = (int)$row["id_order"];
            $new_history->id_employee = 1;
            $new_history->changeIdOrderState(Configuration::get('PS_OS_AWAITING'), $row["id_order"]);
            $new_history->addWithemail(true, false);
        }
        else {
            return false;
        }          
    }   

    private function sign ($params) {
      return $this->signData($this->buildDataToSign($params), Configuration::get('CS_SECRET_KEY'));
    }

    private  function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    private  function buildDataToSign($params) {
            $signedFieldNames = explode(",",$params["signed_field_names"]);
            foreach ($signedFieldNames as $field) {
               $dataToSign[] = $field . "=" . $params[$field];
            }
            return $this->commaSeparate($dataToSign);
    }

    private  function commaSeparate ($dataToSign) {
        return implode(",",$dataToSign);
    } 

}