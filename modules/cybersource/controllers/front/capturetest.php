<?php
include_once(dirname(__FILE__).'/../../cybersource.php');

class CyberSourceCaptureTestModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $orderId = $_REQUEST['o'];

        if($orderId > 0) {
            $CS = new CyberSource();

            print_r("START CAPTURE<br />");
            $CS->capturePayment($orderId);


            //print_r("START VOID CAPTURE<br />");
            //$CS->voidCapturePayment($orderId);

        }

        die();
    }
}