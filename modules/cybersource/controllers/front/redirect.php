<?php

class CyberSourceRedirectModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        //$result = array();
        $cart = $this->context->cart;
        
        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
            Tools::redirect('index.php?controller=order&step=1');
        
        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module)
            if ($module['name'] == 'cybersource') {
                $authorized = true;
                break;
            }
        
        if (!$authorized)
            die($this->module->l('This payment method is not available.', 'validation'));
        
        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');
        
        $currency = $this->context->currency;
        
        //total_product amount
        if ($this->context->currency->decimals == 0) {
            $productsPrice = round(Context::getContext()->cart->getOrderTotal(true));
        } else {
            $productsPrice = Context::getContext()->cart->getOrderTotal(true);
        }
        
        $return_url = $this->context->link->getModuleLink('cybersource', 'validation', array(), null, null, null, false, false);
        
        $curTS =  date("Y-m-d H:i:s");
        $curTS2 = gmdate("Y-m-d\TH:i:s\Z", strtotime($curTS));
        Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'cybersource_cart` (`id_cart`, `date_add`, `date_upd`, total_paid) VALUES ('.$cart->id.',"'.$curTS.'","'.$curTS.'",'.$productsPrice.')');
        $csId = (int)Db::getInstance()->Insert_ID();
        $newuuiq = uniqid($csId);
        Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'cybersource_cart` SET `trans_uuiq`="'.$newuuiq.'" WHERE csId='.$csId);
        
        
        Db::getInstance()->execute(
            'INSERT INTO `'._DB_PREFIX_.'cybersource_cartproduct` '.
                '(`csId`,`id_product`,`id_address_delivery`,`id_shop`,`id_product_attribute`,`quantity`,`date_add`) '.
                'SELECT '.$csId.',`id_product`,`id_address_delivery`,`id_shop`,`id_product_attribute`,`quantity`,"'.$curTS.'" '.
                'FROM `'._DB_PREFIX_.'cart_product` '.
                'WHERE id_cart='.$cart->id
        );

        Db::getInstance()->execute(
            'INSERT INTO `'._DB_PREFIX_.'cybersource_cartrule` '.
                '(`csId`,`id_cart_rule`,`date_add`) '.
                'SELECT '.$csId.',`id_cart_rule`,"'.$curTS.'" '.
                'FROM `'._DB_PREFIX_.'cart_cart_rule` '.
                'WHERE id_cart='.$cart->id
        );

        $arrayArg = array(
            //'order_id' => $order_id,
            //'invoice_no' => $order_id,
            'csId' => $csId,
            'amount' => $productsPrice,
            'return_url' => $return_url,
            'default_lang' => $this->context->language->iso_code,
            'curTS' => $curTS2,
            'newuuiq' => $newuuiq,
            'id_address_invoice' => $cart->id_address_invoice
        );
        
        //$this->context->cookie->__unset('cart_id');        
        
        $this->constrctRequest($arrayArg);

        exit;
    }
    
    private function constrctRequest($arg)
    {
        $addr = Db::getInstance()->getRow(
            'SELECT A.firstname,A.lastname,A.address1,A.address2,A.postcode,A.city,B.email,C.iso_code AS country_code '.
            'FROM  `'._DB_PREFIX_.'address` A '.
            'INNER JOIN  `'._DB_PREFIX_.'customer` B ON A.id_customer=B.id_customer '.
            'INNER JOIN  `'._DB_PREFIX_.'country` C ON A.id_country=C.id_country '.
            'WHERE A.id_address='.$arg['id_address_invoice']);

        $secretHash = Configuration::get('GP_SECRET_HASH');
        
        $data = array(
            "access_key" => Configuration::get('CS_ACCESS_KEY'),
            "profile_id" => Configuration::get('CS_PROFILE_ID'),
            "transaction_uuid" => $arg['newuuiq'],
            "signed_field_names" => "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_address_city,bill_to_address_country,bill_to_address_line1,bill_to_address_line2,bill_to_address_postal_code,bill_to_email,bill_to_forename,bill_to_surname",
            "unsigned_field_names" => "",
            "signed_date_time" => $arg['curTS'],
            "locale" => "en-us",
            "transaction_type" => "authorization",
            "reference_number" => $arg['csId'],
            "amount" => $arg['amount'],
            "currency" => "SGD",
            "bill_to_address_city" => $addr['city'],
            "bill_to_address_country" => $addr['country_code'],
            "bill_to_address_line1" => $addr['address1'],
            "bill_to_address_line2" => $addr['address2'],
            "bill_to_address_postal_code" => $addr['postcode'],
            "bill_to_email" => $addr['email'],
            "bill_to_forename" => $addr['firstname'],
            "bill_to_surname" => $addr['lastname']
            );
        
        //$hash = null;
        echo "<form id='cybersourceform' method='post' action='".Configuration::get('CS_ACTION_URL')."'>";
        foreach ($data as $k => $v) {
            echo "<input type='hidden' name='$k' value='$v'>";
        }
        //echo '<input type="hidden" id="submit" name="submit" value="Submit">';
        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . $this->sign($data) . "\"/>\n";
        echo "</form>";
        echo "<script type='text/javascript'>document.getElementById('cybersourceform').submit();</script>";

        //$hash = rtrim($hash, "&");
        //$secureHash = strtoupper(hash_hmac('SHA256', $hash, pack('H*', $secretHash)));
        //$paraFinale = array_merge($data, array('vpc_SecureHash' => $secureHash));
        //$actionurl = Configuration::get('CS_ACTION_URL').'?'.http_build_query($data);

        //header("Location: " . $actionurl);
    }

    private function sign ($params) {
      return $this->signData($this->buildDataToSign($params), Configuration::get('CS_SECRET_KEY'));
    }

    private  function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    private  function buildDataToSign($params) {
            $signedFieldNames = explode(",",$params["signed_field_names"]);
            foreach ($signedFieldNames as $field) {
               $dataToSign[] = $field . "=" . $params[$field];
            }
            return $this->commaSeparate($dataToSign);
    }

    private  function commaSeparate ($dataToSign) {
        return implode(",",$dataToSign);
    }

}