<?php
class CyberSourceValidationModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $response = array();
        
        foreach($_REQUEST as $key => $val) { 
            $response[$key] = $val;
        }

        $csId = $response["req_reference_number"];
        $transId = "";
        $payment_status = "";
        
        if(isset($response["reason_code"])) {
            $payment_status = $response["reason_code"];
        }
        if(isset($response["transaction_id"])) {
            $transId = $response["transaction_id"];
        }
    
        $logId = $this->SaveLog($_REQUEST, $csId, $payment_status, $transId);
        $logResult = "";
        sleep(1);
        
        $id_cart = $this->getCartIdByCSId($csId);
        $redirect_checkout = false;
        
        if($this->CheckDuplicateLog($logId))
        {
            if($payment_status == "100") 
            {
                $maxRetry = 20;
                $retry_cnt = 1;
                sleep(10);
                $o = $this->getOrderId($id_cart);
                while ($o <= 0 && $retry_cnt < $maxRetry) {
                    $o = $this->getOrderId($id_cart);
                    $retry_cnt++;
                    sleep(5);    
                }
            }
            else {
                $o = 0;
            }
            if($o > 0) {
            sleep(10);
                Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$o.'&key='.$customer->secure_key);
            }
            else {
                Tools::redirect('index.php?controller=order&redirect_error=');
            }
        }
        
        if (empty($id_cart)) {
            exit;
        }    
        else {
            $cart = new Cart($id_cart);
        }
        
        $customer = new Customer($cart->id_customer);
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $amount = (float)$response['req_amount'];
        $secureHash = $this->sign($response);
        
        if($secureHash == $response['signature']) {
            print_r("Valid Hash");
            
            if($payment_status == "100") {
                $logResult .= "Process Success Order.";
                $this->UpdateLog($logId, $logResult);
                
                if(!$this->processOrder($id_cart, $amount, $transId, Configuration::get('PS_OS_PAYMENT'), $csId)) 
                {
                    $this->module->validateOrder($id_cart, Configuration::get('PS_OS_PAYMENT'), $amount, $this->module->displayName, 
                        'CyberSource Transaction ID: ' . $transId. ' (CyberSource Id: '. $csId. ')', NULL, (int)$cart->id_currency, false, $customer->secure_key);
                    $logResult .= "||Order Created";
                    $this->UpdateLog($logId, $logResult);
                    
                    //// check total amount. if not same, update order status
                    if(strval($total) != strval($amount)) 
                    {
                        $this->updateOrderValidation($orderid);
                    }
                }
            }
            //// transaction pending
            //else if ($payment_status == "P") {
            //    $logResult .= "Process Pending Order.";
            //    $this->UpdateLog($logId, $logResult);
                
            //    if(!$this->processOrder($id_cart, $amount, $transId, Configuration::get('PS_OS_AWAITING'), $csId)) 
            //    {
            //        $this->module->validateOrder($id_cart, Configuration::get('PS_OS_AWAITING'), $amount, $this->module->displayName, 
            //            'CyberSource Transaction ID: ' . $transaction_ref. ' (CyberSource Id: '. $csId. ')', NULL, (int)$cart->id_currency, false, $customer->secure_key);
            //        $logResult .= "||Order Created";
            //        $this->UpdateLog($logId, $logResult);
                    
            //        ////// check total amount. if not same, update order status
            //        //if(strval($total) != strval($amount)) 
            //        //{
            //        //    $this->updateOrderValidation($orderid);
            //        //}
            //    }
            //}
            else {
                $logResult .= "Payment Failed";
                $logResult .= "||Redirected";
                $redirect_checkout = true;
                $this->UpdateLog($logId, $logResult);
            }
        }
        else {
            $payment_status = "hash";
            $logResult .= "Invalid Hash";
            $logResult .= "||Redirected";
            $redirect_checkout = true;
            $this->UpdateLog($logId, $logResult);
        }
        
        if($redirect_checkout)
        {
            Tools::redirect('index.php?controller=order&redirect_error='.$this->ErrorResponse($payment_status));
        }
        else {
            $maxRetry = 30;
            $retry_cnt = 1;
              
            $o = $this->getOrderId($id_cart);
            while ($o <= 0 && $retry_cnt < $maxRetry) {
                $o = $this->getOrderId($orderid);
                $retry_cnt++;
                sleep(5);    
            }
            
            $this->UpdatePaymentResult($csId, $o, $payment_status, $transId);         
            $logResult .= "||Update Payment Result (o=>". $o.")";
            $this->UpdateLog($logId, $logResult);          
            Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$o.'&key='.$customer->secure_key);
        }
        
        exit;
    }
    
    private function UpdatePaymentResult ($csId, $id_order, $payment_status, $transId) {
        $sql = 'UPDATE `'._DB_PREFIX_.'cybersource_cart` '.
                'SET payment_status="'.$payment_status.'",'.
                    'transaction_no="'.$transId.'",'.
                    'id_order="'.$id_order.'" '.
                'WHERE csId='.(int)$csId;
        return Db::getInstance()->execute($sql);
    }
    
    private function ErrorResponse($responseCode) {
        $responseResult = "";
        switch($responseCode) {
            case "101":
            case "102":
            case "236":
            case "481":
            case 481:
            case 236:
            case 101:
            case 102:
                $responseResult = "Transaction unsuccessful, transaction could not be processed.";
            break;
            case "104":
            case 104:
                $responseResult = "Transaction unsuccessful, duplicate payment.";
            break;
            case "110":
            case "204":
            case "210":
            case 210:
            case 204:
            case 110:
                $responseResult = "Transaction unsuccessful, card insufficient credit.";
            break;
            case "150":
            case 150:
                $responseResult = "Payment processing error. Please try again.";
            break;
            case "151":
            case "152":
            case 151:
            case 152:
                $responseResult = "Payment processing error. System timeout.";
            break;
            case "200":
            case "201":
            case 201:
            case 200:
                $responseResult = "Transaction unsuccessful, transaction declined.";
            break;
            case "202":
            case 202:
                $responseResult = "Transaction unsuccessful, card expired.";
            break;
            case "203":
            case "232":
            case "234":
            case 234:
            case 232:
            case 203:
                $responseResult = "Transaction unsuccessful, transaction declined by bank.";
            break;
            case "205":
            case "208":
             case "240":
            case 240:
            case 208:
            case 205:
                $responseResult = "Transaction unsuccessful, invalid card.";
            break;
            case "207":
            case 207:
                $responseResult = "Transaction unsuccessful, bank system error.";
            break;
            case "211":
            case "230":
            case 230:
            case 211:
                $responseResult = "Transaction unsuccessful, 3D Secure Authentication failed.";
            break;
            case "221":
            case 221:
                $responseResult = "Transaction unsuccessful, transaction blocked.";
            break;
            case "231":
            case 231:
                $responseResult = "Transaction unsuccessful, invalid account.";
            break;
            case "475":
            case 475:
                $responseResult = "Transaction unsuccessful, authentication require.";
            break;
            case "476":
            case 476:
                $responseResult = "Transaction unsuccessful, authentication failed.";
            break;
            case "hash":
                $responseResult = "Transaction unsuccessful, unauthorized return.";
            break;
            default:
                $responseResult = "Transaction failed.";
            break;           
        }
        
        return $responseResult;
    }

    private function SaveLog ($data, $csId, $paymen_status, $transId)
    {
        $postdata = json_encode($data);
        $postdata = str_replace('"', '\"', $postdata);
        
        $sql = 'INSERT INTO `'._DB_PREFIX_.'payment_callback_log` (`module`, `file`,`orderId`, `payment_status`,`transaction_no`,`postdata`,`createdTS`) '.
                'VALUES ("CyberSource", "validation","'.$csId.'","'.$paymen_status.'","'.$transId.'","'.$postdata.'", NOW())';
        $return = Db::getInstance()->execute($sql);
        return Db::getInstance()->Insert_ID();
    }
    
    private function getCartIdByCSId($csId) {
        $sql = 'SELECT id_cart FROM `'. _DB_PREFIX_ .'cybersource_cart` WHERE csId = '. $csId;
        $cartId = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        
        return $cartId;
    }
    
    private function getOrderId($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        if ($row){
            return $row["id_order"];
        }
        else {
            return 0;
        }       
    }
    
    private function CheckDuplicateLog($logId)
    {
        $tempSQL = "SELECT COUNT(1) AS logCount FROM `". _DB_PREFIX_ ."payment_callback_log` A ".
                    "INNER JOIN `". _DB_PREFIX_ ."payment_callback_log` B ON ".
                        "A.payment_status = B.payment_status AND A.transaction_no=B.transaction_no AND ".
                        "A.orderId=B.orderId AND A.id < B.id ".
                    "WHERE B.id=". (int)$logId;
        $stateCount = Db::getInstance()->getRow($tempSQL);
      
        if($stateCount["logCount"] > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private function UpdateLog($logId, $logVal) {
        $sql = 'UPDATE `'._DB_PREFIX_.'payment_callback_log` SET result="'.$logVal.'" WHERE id='.(int)$logId;
        $return = Db::getInstance()->execute($sql);
    }
    
    private function processOrder($id_cart, $amount, $tranID, $id_order_state, $csId) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
        if ($row){
            return $this->module->updateValidatedOrder(
                $id_cart, $row["id_order"], $id_order_state, $amount, $this->module->displayName, 
                'CyberSource Transaction ID: ' . $tranID. ' (CyberSource Id: '. $csId. ')', NULL, false);
        }
        else {
            return false;
        }    
    }
    
    private function updateOrderValidation($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order,current_state FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        if ($row && $row["current_state"] != Configuration::get('PS_OS_AWAITING')){
            $new_history = new OrderHistory();
		    $new_history->id_order = (int)$row["id_order"];
            $new_history->id_employee = 1;
		    $new_history->changeIdOrderState(Configuration::get('PS_OS_AWAITING'), $row["id_order"]);
            $new_history->addWithemail(true, false);
        }
        else {
            return false;
        }          
    }   

    private function sign ($params) {
      return $this->signData($this->buildDataToSign($params), Configuration::get('CS_SECRET_KEY'));
    }

    private  function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    private  function buildDataToSign($params) {
            $signedFieldNames = explode(",",$params["signed_field_names"]);
            foreach ($signedFieldNames as $field) {
               $dataToSign[] = $field . "=" . $params[$field];
            }
            return $this->commaSeparate($dataToSign);
    }

    private  function commaSeparate ($dataToSign) {
        return implode(",",$dataToSign);
    } 
}