<?php 
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');


if (!class_exists('cybersource')) {
   // Put class TestClass here
    include(dirname(__FILE__).'/cybersource.php');
}

if (isset($_REQUEST['ajax']) && isset($_REQUEST["type"]) && isset($_REQUEST["orderId"])) {
    $type = $_REQUEST["type"];
    $orderId = $_REQUEST["orderId"];
    $to_status = $_REQUEST["toStatus"];
    $obj = new cybersource();
    $result = $obj->ajaxCall($type, $orderId, $to_status);
    echo $result;
}
else {
    echo false;
}
die();
?>