<?php

if (!defined('_PS_VERSION_'))
	exit;

class CyberSource extends PaymentModule
{
	protected $_html = '';
	protected $_postErrors = array();

	public $merchantId;
	public $accKey;
	public $secretKey;
    public $actionURL;
	public $apiId;
    public $apiKey;
	public $apiURL;
    
	public function __construct()
	{
		$this->name = 'cybersource';
		$this->tab = 'payments_gateways';
		$this->version = '1.0.0';
		$this->author = 'Claritas';
		$this->controllers = array('callback', 'redirect', 'validation');
		$this->is_eu_compatible = 1;

		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		$config = Configuration::getMultiple(array(
            'CS_PROFILE_ID', 'CS_ACCESS_KEY', 'CS_SECRET_KEY', 'CS_ACTION_URL', 
            'CS_MERCHANT_ID', 'CS_API_ID', 'CS_API_KEY',//'CS_API_URL',
            'CS_CAPTURE_CRON', 'CS_CAPTURE_STATUS', 'CS_LIVE_MODE'
        ));
            
		if (!empty($config['CS_PROFILE_ID']))
			$this->merchantId = $config['CS_PROFILE_ID'];
		if (!empty($config['CS_ACCESS_KEY']))
			$this->accKey = $config['CS_ACCESS_KEY'];
        if (!empty($config['CS_SECRET_KEY']))
			$this->secretKey = $config['CS_SECRET_KEY'];
		if (!empty($config['CS_ACTION_URL']))
			$this->actionURL = $config['CS_ACTION_URL'];
        if (!empty($config['CS_MERCHANT_ID']))
			$this->apiId = $config['CS_MERCHANT_ID'];
		if (!empty($config['CS_API_ID']))
			$this->apiId = $config['CS_API_ID'];
        if (!empty($config['CS_API_KEY']))
			$this->apiKey = $config['CS_API_KEY'];
        //if (!empty($config['CS_API_URL']))
        //    $this->apiURL = $config['CS_API_URL'];
       
		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('CyberSource');
		$this->description = $this->l('Accept payments for your products via Cybersource.');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
		if (!isset($this->merchantId) || !isset($this->accKey) || !isset($this->secretKey) || !isset($this->actionURL)
             || !isset($this->apiId) || !isset($this->apiKey))
			$this->warning = $this->l('Hosted Checkout and API parameters must be configured before using this payment method');	
	}

	public function install()
	{	
		if (!parent::install() || !$this->registerHook('payment') || 
            ! $this->registerHook('displayPaymentEU') || !$this->registerHook('paymentReturn') ||
            ! $this->registerHook('UpdateOrderStatus') )
			return false;

		// install DataBase
        if (!$this->installSQL()) {
            return false;
        }
        
		return true;
	}

	public function uninstall()
	{
		if (!Configuration::deleteByName('CS_PROFILE_ID') 
            || !Configuration::deleteByName('CS_ACCESS_KEY') 
            || !Configuration::deleteByName('CS_SECRET_KEY')
            || !Configuration::deleteByName('CS_ACTION_URL')
            || !Configuration::deleteByName('CS_MERCHANT_ID')
            || !Configuration::deleteByName('CS_API_ID')
            || !Configuration::deleteByName('CS_API_KEY')
            //|| !Configuration::deleteByName('CS_API_URL')
            || !Configuration::deleteByName('CS_CAPTURE_CRON')
            || !Configuration::deleteByName('CS_CAPTURE_STATUS')
            || !Configuration::deleteByName('CS_LIVE_MODE')
            || !$this->uninstallSQL() || !parent::uninstall())
            return false;
        
        return true;
	}

	protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('CS_PROFILE_ID')) {
                $this->_postErrors[] = $this->l('Profile ID is required!');
            }
            if (!Tools::getValue('CS_ACCESS_KEY')) {
                $this->_postErrors[] = $this->l('Hosted Checkout access key is required!');
            }
            if (!Tools::getValue('CS_SECRET_KEY')) {
                $this->_postErrors[] = $this->l('Hosted Checkout secret key is required!');
            }
            if (!Tools::getValue('CS_ACTION_URL')) {
                $this->_postErrors[] = $this->l('Action URL is required!');
            }
            if (!Tools::getValue('CS_API_ID')) {
                $this->_postErrors[] = $this->l('API ID is required!');
            }
            
            if (!Tools::getValue('CS_MERCHANT_ID')) {
                $this->_postErrors[] = $this->l('API Merchant ID is required!');
            }
            if (!Tools::getValue('CS_API_KEY')) {
                $this->_postErrors[] = $this->l('API secret Key is required!');
            }
            //if (!Tools::getValue('CS_API_URL')) {
            //    $this->_postErrors[] = $this->l('API secret Key is required!');
            //}    
        }
	}

	protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('CS_PROFILE_ID', Tools::getValue('CS_PROFILE_ID'));
            Configuration::updateValue('CS_ACCESS_KEY', Tools::getValue('CS_ACCESS_KEY'));
            Configuration::updateValue('CS_SECRET_KEY', Tools::getValue('CS_SECRET_KEY'));
            Configuration::updateValue('CS_ACTION_URL', Tools::getValue('CS_ACTION_URL'));
            
            Configuration::updateValue('CS_MERCHANT_ID', Tools::getValue('CS_MERCHANT_ID'));
            Configuration::updateValue('CS_API_ID', Tools::getValue('CS_API_ID'));
            Configuration::updateValue('CS_API_KEY', Tools::getValue('CS_API_KEY'));
            //Configuration::updateValue('CS_API_URL', Tools::getValue('CS_API_URL'));

            Configuration::updateValue('CS_CAPTURE_CRON', Tools::getValue('CS_CAPTURE_CRON'));
            Configuration::updateValue('CS_CAPTURE_STATUS', Tools::getValue('CS_CAPTURE_STATUS'));

            Configuration::updateValue('CS_LIVE_MODE', Tools::getValue('CS_LIVE_MODE'));            
        }

        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}

	public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
	}

	public function hookPayment($params)
	{
		if (!$this->active)
			return;

		$this->smarty->assign(array(
			'this_path_cs' => $this->_path,
        ));

		return $this->display(__FILE__, 'payment.tpl');
	}

	public function hookDisplayPaymentEU($params)	
	{			
		if (!$this->active)
			return;

		$payment_options = array(
			'cta_text' => $this->l('Pay by Visa / Master'),			
			'logo' => Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/credit.png'),
			'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true)
		);

		return $payment_options;
	}

	public function hookPaymentReturn($params)
	{        
		if (!$this->active)
			return;

		$state = $params['objOrder']->getCurrentState();

		$id_order = (int) Order::getOrderByCartId($params['objOrder']->id_cart);

        $sql = 'SELECT payment_status, transaction_no, receipt_no, id_order FROM `'. _DB_PREFIX_ .'cybersource_cart` WHERE id_order = '. $id_order.' ORDER BY date_upd DESC';
        $paymentinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
		if (in_array($state, array(Configuration::get('PS_OS_PAYMENT'),Configuration::get('PS_OS_AWAITING'))))
		{
			$this->smarty->assign(array(                
                'status' => 'ok',
                'amount' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                'order_id' => $id_order,
                'order_ref' => $params['objOrder']->reference,
                'transaction_ref' => $paymentinfo['transaction_no'],
                'payment_status' => $paymentinfo['payment_status'],
            ));
		}
		else{
			$this->smarty->assign(array(
                'status' => 'failed',
                'amount' => round($params['objOrder']->getOrdersTotalPaid(), 2),
                'order_id' => $id_order,
                'order_ref' => $params['objOrder']->reference,
                'transaction_ref' => $paymentinfo['transaction_no'],
                'payment_status' => $paymentinfo['payment_status'],
            ));
		}

		return $this->display(__FILE__, 'payment_return.tpl');
	}
	
	private function installSQL() {

        $sql = array();
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "payment_callback_log` (
            `id` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `module` varchar(45) DEFAULT NULL,
            `file` varchar(100) DEFAULT NULL,
            `orderId` varchar(45) DEFAULT NULL,
            `payment_status` varchar(20) DEFAULT NULL,
            `transaction_no` varchar(50) DEFAULT NULL,
            `postdata` text,
            `createdTS` datetime DEFAULT NULL,
            `result` varchar(1000) DEFAULT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "cybersource_cart` (
            `csId` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `id_cart` int(11) NOT NULL,
            `date_add` DATETIME NOT NULL,
            `date_upd` DATETIME NOT NULL,
            `total_paid` decimal(10,2) DEFAULT NULL,
            `payment_status` varchar(20) DEFAULT NULL,
            `transaction_no` varchar(50) DEFAULT NULL,
            `receipt_no` varchar(100) DEFAULT NULL,
            `id_order` int(11) DEFAULT NULL,
            `synced` varchar(20) DEFAULT NULL,
            `syncmsg` varchar(500) DEFAULT NULL,
            `syncdata` varchar(2000) DEFAULT NULL,
            `trans_uuiq` varchar(50) DEFAULT NULL,
            `capture_id` varchar(50) DEFAULT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "cybersource_cartproduct` (
            `csLogId` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `csId` int(10) unsigned NOT NULL,
            `id_product` int(10) unsigned NOT NULL,
            `id_address_delivery` int(10) unsigned NOT NULL DEFAULT '0',
            `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
            `id_product_attribute` int(10) unsigned NOT NULL DEFAULT '0',
            `quantity` int(10) unsigned NOT NULL DEFAULT '0',
            `date_add` datetime NOT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "cybersource_cartrule` (
            `csLogId` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `csId` int(10) unsigned NOT NULL,
            `id_cart_rule` int(10) unsigned NOT NULL,
            `date_add` datetime NOT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;

        foreach ($sql as $q) {
            if (!DB::getInstance()->execute($q)) {
                return false;
            }
        }
				
        return true;
    }

    /**
     * Uninstall DataBase table
     * @return boolean if install was successfull
     */
    private function uninstallSQL() {

        $sql = array();
        
        //$sql[] = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "payment_callback_log`";
        //$sql[] = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "globalpayment_cart`";
        
        //foreach ($sql as $q) {
        //    if (!DB::getInstance()->execute($q)) {
        //        return false;
        //    }
        //}
        
        return true;
    }

	public function renderForm()
	{
    	 $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Cybersource Details'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Profile Id'),
                        'name' => 'CS_PROFILE_ID',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Hosted Checkout access Key'),
                        'name' => 'CS_ACCESS_KEY',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Hosted Checkout secret Key'),
                        'name' => 'CS_SECRET_KEY',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Hosted Checkout Action URL'),
                        'name' => 'CS_ACTION_URL',
                        'required' => true
                    ),
                    
                    array(
                        'type' => 'text',
                        'label' => $this->l('API Merchant ID'),
                        'name' => 'CS_MERCHANT_ID',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('API ID'),
                        'name' => 'CS_API_ID',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('API secret key'),
                        'name' => 'CS_API_KEY',
                        'required' => true
                    ),
                    //array(
                    //    'type' => 'text',
                    //    'label' => $this->l('API URL'),
                    //    'name' => 'CS_API_URL',
                    //    'required' => true
                    //),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Auto Capture Payment'),
                        'name' => 'CS_CAPTURE_CRON',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),                    
                    array(
                        'type' => 'select',
					    'label' => $this->l('Auto Capture Status:'),
					    'name' => 'CS_CAPTURE_STATUS',
					    'options' => array(
						    'query' => $this->getOrderStatuses(),
						    'id' => 'id_order_state',
						    'name' => 'name',
						    'default' => array(
							    'label' => $this->l('No Status'),
							    'value' => 0
						    )
					    )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('CyberSource Live'),
                        'name' => 'CS_LIVE_MODE',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('LIVE')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('UAT')
                            )
                        ),
                    ),                    
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );
        
        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->id                       = (int) Tools::getValue('id_carrier');
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm(array(
            $fields_form
        ));
	}

     protected function getOrderStatuses() {
        $allstatuses = Db::getInstance()->ExecuteS('SELECT DISTINCT A.id_order_state, B.name AS status_name'.
                    ' FROM '._DB_PREFIX_.'order_state A'.
                    ' INNER JOIN '._DB_PREFIX_.'order_state_lang B ON A.id_order_state = B.id_order_state'.
                    ' WHERE B.id_lang = 1'.
                    ' ORDER BY B.name'
		);
        
        $status_list = array();
        
        foreach ($allstatuses as $status)
        {
            $status_list[] = array(
                "id_order_state" => $status["id_order_state"],
                "name" => $status["status_name"]
            );
        }
        
        return $status_list;
    }
    
	public function getConfigFieldsValues()
	{
		return array(
			'CS_PROFILE_ID' => Tools::getValue('CS_PROFILE_ID', Configuration::get('CS_PROFILE_ID')),
            'CS_ACCESS_KEY' => Tools::getValue('CS_ACCESS_KEY', Configuration::get('CS_ACCESS_KEY')),
            'CS_SECRET_KEY' => Tools::getValue('CS_SECRET_KEY', Configuration::get('CS_SECRET_KEY')),
            'CS_ACTION_URL' => Tools::getValue('CS_ACTION_URL', Configuration::get('CS_ACTION_URL')),

            'CS_MERCHANT_ID' => Tools::getValue('CS_MERCHANT_ID', Configuration::get('CS_MERCHANT_ID')),
            'CS_API_ID' => Tools::getValue('CS_API_ID', Configuration::get('CS_API_ID')),
            'CS_API_KEY' => Tools::getValue('CS_API_KEY', Configuration::get('CS_API_KEY')),
            //'CS_API_URL' => Tools::getValue('CS_API_URL', Configuration::get('CS_API_URL')),
            'CS_CAPTURE_CRON' => Tools::getValue('CS_CAPTURE_CRON', Configuration::get('CS_CAPTURE_CRON')),
            'CS_CAPTURE_STATUS' => Tools::getValue('CS_CAPTURE_STATUS', Configuration::get('CS_CAPTURE_STATUS')),
            'CS_LIVE_MODE' => Tools::getValue('CS_LIVE_MODE', Configuration::get('CS_LIVE_MODE')),
		);
	}	
    
    public function hookUpdateOrderStatus($params) {
        $autoCapture = Configuration::get('CS_CAPTURE_CRON');
        $id_order = $params["id_order"];
        $new_status = $params["newOrderStatus"];
        $capture_status = Configuration::get("CS_CAPTURE_STATUS");
        $sql = "";
        $remarks = "";

        if($autoCapture == "1" ) {
            $remarks .="STEP1|";
            $syncMsg = null;
            $result = true;
            $orderRef = Tools::brandsOrderNumber($id_order);
            
            if($new_status->id == $capture_status) {
                $paymentMethod = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT module FROM `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order);

                if($paymentMethod == $this->name) 
                {
                    $captureRemarks = $this->capturePayment($id_order);
                    $remarks .= $captureRemarks['remarks'];
                    $sql = $captureRemarks['sql'];
                    $result = $captureRemarks['result'];
                }
            }

            Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                'INSERT `'. _DB_PREFIX_ .'cybersource_capturelog` (`autocapture`,`capturestatus`,`id_order`,`new_status`,`sql`,`date_add`,`remarks`)'.
                'VALUES ("'.$autoCapture.'","'.$capture_status.'","'.$id_order.'","'.$new_status->id.'","'.pSQL($sql).'",NOW(),"'.pSQL($remarks).'")');

            if(!$result) {
                return "Capture payment for Order ".$orderRef." failed. Please contact technical support";
            }
        }
    }
    
    private function getCyberSourceAPIClient() {
        require_once __DIR__. DIRECTORY_SEPARATOR .'vendor/autoload.php';
        require_once __DIR__. DIRECTORY_SEPARATOR .'Resources/ExternalConfiguration.php';

        $merchantID = Configuration::get('CS_MERCHANT_ID');
        $apiKeyID = Configuration::get('CS_API_ID');
        $secretKey = Configuration::get('CS_API_KEY');
        $isLive = Configuration::get('CS_LIVE_MODE');
        $commonElement = new CyberSource\ExternalConfiguration($merchantID,$apiKeyID,$secretKey,$isLive);
        $config = $commonElement->ConnectionHost();
	    $merchantConfig = $commonElement->merchantConfigObject();
	    $apiclient = new CyberSource\ApiClient($config, $merchantConfig);

        return $apiclient;
    }

    public function capturePayment ($id_order)
    {
        $update = false;
        $remarks ="STEP2|";
        $sql = 'SELECT A.csId, A.id_cart, A.payment_status, A.transaction_no, A.id_order, A.total_paid,B.reference '.
                'FROM `'. _DB_PREFIX_ .'cybersource_cart` A '.
                'INNER JOIN `'. _DB_PREFIX_ .'orders` B ON A.id_order=B.id_order '.
                'WHERE A.id_order='.(int)$id_order.' AND A.payment_status="100" AND (A.synced IS NULL OR A.synced NOT IN ("201","200"))';

        $pinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        $remarks .="STEP3|";
            
        if($pinfo != null) {
            $remarks .="STEP4|";
            
            $apiclient = $this->getCyberSourceAPIClient();
	        $api_instance = new CyberSource\Api\CaptureApi($apiclient);

            $cliRefInfoArr = ["code" => Tools::brandsOrderNumber($pinfo['id_order'])];
            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
            $amountDetailsArr = ["totalAmount" => $pinfo['total_paid'],"currency" => "SGD"];
            $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
            $orderInfoArry = ["amountDetails" => $amountDetInfo];
            $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
            $requestArr = [
                "clientReferenceInformation" => $client_reference_information,
                "orderInformation" => $order_information
            ];

            $request = new CyberSource\Model\CapturePaymentRequest($requestArr);
            $api_response = list($response,$statusCode,$httpHeader)=null;

            try {
                //Calling the Api
                $api_response = $api_instance->capturePayment($request, $pinfo['transaction_no']);
                $remarks .="STEP5|";
                
                if($api_response[1] == "201") {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'cybersource_cart` '.
                        'SET synced="'.$api_response[1].'",'.
                            'syncdata="'.pSQL(json_encode($api_response)).'",'.
                            'capture_id="'.$api_response[0]['id'].'",'.
                            'date_upd=NOW() '.
                        'WHERE csId='.$pinfo['csId']);
                    $update = true;

                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'INSERT  `'. _DB_PREFIX_ .'message` (`id_cart`,`id_customer`,`id_employee`,`id_order`,`message`,`private`,`date_add`) '.
                        'SELECT id_cart,id_customer,0,id_order,"CyberSource Capture ID:'.$api_response[0]['id'].'",1,NOW() '.
                        'FROM  `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order); 
                }
            } 
            catch (Cybersource\ApiException $e) {
                $remarks .= "Response=>". json_encode($e->getResponseBody());
                $remarks .= "||Error=>". json_encode($e->getResponseBody());
            }
        }
        else {
            $update= true;
        }

        $result = array();
        $result['sql'] = $sql;
        $result['remarks'] = $remarks;
        $result['result'] = $update;

        return $result;
    }

    public function refundPayment ($id_order, $employeeId, $to_status) 
    {
        $update = false;
        $reason = "";
        $remarks ="STEP2|";
        $sql = 'SELECT A.csId, A.id_cart, A.payment_status, A.transaction_no, A.id_order, A.total_paid,B.reference,A.synced,A.capture_id,B.current_state, B.module '.
                'FROM `'. _DB_PREFIX_ .'cybersource_cart` A '.
                'INNER JOIN `'. _DB_PREFIX_ .'orders` B ON A.id_order=B.id_order '.
                'WHERE A.id_order='.(int)$id_order.' AND A.payment_status="100" '.
                    'AND A.synced IN ("201","200") '.
                    'AND (A.reversed IS NULL OR A.reversed NOT IN ("201","200")) '.
                    'AND (A.voided IS NULL OR A.voided NOT IN ("201","200")) '.
                    'AND (A.refunded IS NULL OR A.refunded NOT IN ("201","200"))';

        $pinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        $remarks .="STEP3|";

        if($pinfo != null && $pinfo['transaction_no'] != null) {
            $remarks .="STEP4|";
            $apiclient = $this->getCyberSourceAPIClient();
            $api_instance = new CyberSource\Api\RefundApi($apiclient);

            $cliRefInfoArr = ["code" => Tools::brandsOrderNumber($pinfo['id_order'])];
            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
            $amountDetailsArr = ["totalAmount" => $pinfo['total_paid'],"currency" => "SGD"];
            $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
            $orderInfoArry = ["amountDetails" => $amountDetInfo];
            $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
            $paymentRequestArr = [
                "clientReferenceInformation" => $client_reference_information,
                "orderInformation" => $order_information
            ];

            try {
                $api_response = list($response,$statusCode,$httpHeader)=null;

                if($pinfo['capture_id'] && $pinfo['capture_id'] != "") {
                    $remarks .="STEP5-RefundCapture|";
                    $paymentRequest = new CyberSource\Model\RefundCaptureRequest($paymentRequestArr);
                    $api_response = $api_instance->refundCapture($paymentRequest, $pinfo['capture_id']);
                }
                else {
                    $remarks .="STEP5-RefundPayment|";
                    $paymentRequest = new CyberSource\Model\RefundPaymentRequest($paymentRequestArr);
                    $api_response = $api_instance->refundPayment($paymentRequest, $pinfo['transaction_no']);
		        }

                if($api_response[1] == "201" || $api_response[1] == "200") {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'cybersource_cart` '.
                        'SET refunded="'.$api_response[1].'",'.
                            'refunddata="'.pSQL(json_encode($api_response)).'",'.
                            'refund_id="'.$api_response[0]['id'].'",'.
                            'date_refund=NOW() '.
                        'WHERE csId='.$pinfo['csId']);
                    $update = true;

                    // check and update order status
                    if($to_status != "0" && $pinfo['current_state'] != $to_status) 
                    {
                        $this->updateOrderStatus($id_order, $employeeId, $to_status);
                    }

                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'INSERT  `'. _DB_PREFIX_ .'message` (`id_cart`,`id_customer`,`id_employee`,`id_order`,`message`,`private`,`date_add`) '.
                        'SELECT id_cart,id_customer,0,id_order,"CyberSource Refund ID:'.$api_response[0]['id'].'",1,NOW() '.
                        'FROM  `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order); 
                }
                else {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'cybersource_cart` '.
                        'SET refunded="'.$api_response[1].'",'.
                            'refunddata="'.pSQL(json_encode($api_response)).'",'.
                            'date_refund=NOW() '.
                        'WHERE csId='.$pinfo['csId']);
                    $update = false;
                }
	        } 
            catch (Cybersource\ApiException $e) {
                $update = false;
                $responseBody = $e->getResponseBody();
                $remarks .= "Response=>". json_encode($responseBody);
                
                if($responseBody && property_exists($responseBody,'message')) {
                    $reason = $responseBody->message;
                }
            }
        }
        else {
            $update = false;
            $reason = "Invalid Order";
        }

        $result = array();
        $result['remarks'] = $remarks;
        $result['result'] = $update;
        $result['reason'] = $reason;
        return json_encode($result);
    }

    public function reverseAuthorize ($id_order, $employeeId, $to_status) 
    {
        $update = false;
        $reason = "";
        $remarks ="STEP2|";
        $sql = 'SELECT A.csId, A.id_cart, A.payment_status, A.transaction_no, A.id_order, A.total_paid,B.reference,A.synced,A.capture_id, B.current_state, B.module '.
                'FROM `'. _DB_PREFIX_ .'cybersource_cart` A '.
                'INNER JOIN `'. _DB_PREFIX_ .'orders` B ON A.id_order=B.id_order '.
                'WHERE A.id_order='.(int)$id_order.' AND A.payment_status="100" '.
                    'AND (A.reversed IS NULL OR A.reversed NOT IN ("201","200")) '.
                    'AND (A.synced IS NULL OR A.synced NOT IN ("201","200")) '.
                    'AND (A.voided IS NULL OR A.voided NOT IN ("201","200")) '.
                    'AND (A.refunded IS NULL OR A.refunded NOT IN ("201" ,"200"))';

        $pinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        $remarks .="STEP3|";

        if($pinfo != null && $pinfo['transaction_no'] != null) {
            $remarks .="STEP4|";
            $apiclient = $this->getCyberSourceAPIClient();
            $api_instance = new CyberSource\Api\ReversalApi($apiclient);

            $cliRefInfoArr = ["code" => Tools::brandsOrderNumber($pinfo['id_order'])];
            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);

            $amountDetailsArr = ["totalAmount" => $pinfo['total_paid'],"currency" => "SGD"];
            $amountDetInfo = new CyberSource\Model\Ptsv2paymentsidreversalsReversalInformationAmountDetails($amountDetailsArr);

            $reversalInformationArr = ["amountDetails" => $amountDetInfo, "reason" => "Reverse Authorization"];
            $reversalInformation = new CyberSource\Model\Ptsv2paymentsidreversalsReversalInformation($reversalInformationArr);

            $paymentRequestArr = [
                "clientReferenceInformation" => $client_reference_information,
                "reversalInformation" => $reversalInformation
            ];
            
            $paymentRequest = new CyberSource\Model\AuthReversalRequest($paymentRequestArr);
            $api_response = list($response,$statusCode,$httpHeader)=null;
                
            try {
                
                $api_response = $api_instance->authReversal($pinfo['transaction_no'], $paymentRequest);
                
                if($api_response[1] == "201" || $api_response[1] == "200") {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'cybersource_cart` '.
                        'SET reversed="'.$api_response[1].'",'.
                            'reversedata="'.pSQL(json_encode($api_response)).'",'.
                            'reverse_id="'.$api_response[0]['id'].'",'.
                            'date_reverse=NOW() '.
                        'WHERE csId='.$pinfo['csId']);
                    $update = true;

                    // check and update order status
                    if($to_status != "0" && $pinfo['current_state'] != $to_status) 
                    {
                        $this->updateOrderStatus($id_order, $employeeId, $to_status);
                    }

                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'INSERT  `'. _DB_PREFIX_ .'message` (`id_cart`,`id_customer`,`id_employee`,`id_order`,`message`,`private`,`date_add`) '.
                        'SELECT id_cart,id_customer,0,id_order,"CyberSource Reversal ID:'.$api_response[0]['id'].'",1,NOW() '.
                        'FROM  `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order); 
                }
                else {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'cybersource_cart` '.
                        'SET reversed="'.$api_response[1].'",'.
                            'reversedata="'.pSQL(json_encode($api_response)).'",'.
                            'date_reverse=NOW() '.
                        'WHERE csId='.$pinfo['csId']);
                    $update = false;
                }
	        } 
            catch (Cybersource\ApiException $e) {
                $update = false;
                $responseBody = $e->getResponseBody();
                $remarks .= "Response=>". json_encode($responseBody);
                
                if($responseBody && property_exists($responseBody,'message')) {
                    $reason = $responseBody->message;
                }
            }
        }
        else {
            $update = false;
            $reason = "Invalid Order";
        }

        $result = array();
        $result['remarks'] = $remarks;
        $result['result'] = $update;
        $result['reason'] = $reason;
        return json_encode($result);
    }

    public function voidCapturePayment ($id_order, $employeeId, $to_status) 
    {
        $update = false;
        $reason = "";
        $remarks ="STEP2|";
        $sql = 'SELECT A.csId, A.id_cart, A.payment_status, A.transaction_no, A.id_order, A.total_paid,B.reference,A.capture_id,B.current_state, B.module '.
                'FROM `'. _DB_PREFIX_ .'cybersource_cart` A '.
                'INNER JOIN `'. _DB_PREFIX_ .'orders` B ON A.id_order=B.id_order '.
                'WHERE A.id_order='.(int)$id_order.' AND A.payment_status="100" AND (A.synced IS NULL OR A.synced NOT IN ("201","200"))';

        $pinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        $remarks .="STEP3|";

        if($pinfo != null && $pinfo['capture_id'] != null) {
            $remarks .="STEP4|";

            $apiclient = $this->getCyberSourceAPIClient();
            $api_instance = new CyberSource\Api\VoidApi($apiclient);

            $cliRefInfoArr = ["code" => Tools::brandsOrderNumber($pinfo['id_order'])];
            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
            $paymentRequestArr = ["clientReferenceInformation" => $client_reference_information];
            $paymentRequest = new CyberSource\Model\VoidCaptureRequest($paymentRequestArr);
            $api_response = list($response,$statusCode,$httpHeader)=null;
            try {
                $api_response = $api_instance->voidCapture($paymentRequest, $pinfo['capture_id']);
                
                if($api_response[1] == "201" || $api_response[1] == "200") {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'cybersource_cart` '.
                        'SET voided="'.$api_response[1].'",'.
                            'voiddata="'.pSQL(json_encode($api_response)).'",'.
                            'void_id="'.$api_response[0]['id'].'",'.
                            'date_void=NOW() '.
                        'WHERE csId='.$pinfo['csId']);
                    $update = true;

                    // check and update order status
                    if($to_status != "0" && $pinfo['current_state'] != $to_status) 
                    {
                        $this->updateOrderStatus($id_order, $employeeId, $to_status);
                    }

                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'INSERT  `'. _DB_PREFIX_ .'message` (`id_cart`,`id_customer`,`id_employee`,`id_order`,`message`,`private`,`date_add`) '.
                        'SELECT id_cart,id_customer,0,id_order,"CyberSource Void ID:'.$api_response[0]['id'].'",1,NOW() '.
                        'FROM  `'. _DB_PREFIX_ .'orders` WHERE id_order='.(int)$id_order); 
                }
                else {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'cybersource_cart` '.
                        'SET voided="'.$api_response[1].'",'.
                            'voiddata="'.pSQL(json_encode($api_response)).'",'.
                            'date_void=NOW() '.
                        'WHERE csId='.$pinfo['csId']);
                    $update = false;
                }

            } catch (Cybersource\ApiException $e) {
                $update = false;
                $responseBody = $e->getResponseBody();
                $remarks .= "Response=>". json_encode($responseBody);
                
                if($responseBody && property_exists($responseBody,'message')) {
                    $reason = $responseBody->message;
                }
            }
        }
        else {
            $update = false;
            $reason = "Invalid Order";
        }

        $result = array();
        $result['remarks'] = $remarks;
        $result['result'] = $update;
        $result['reason'] = $reason;
        return json_encode($result);
    }

    private function updateOrderStatus($id_order, $employeeId, $to_status) 
    {
        $new_history = new OrderHistory();
		$new_history->id_order = (int)$id_order;
        $new_history->id_employee = (int)$employeeId;
		$new_history->changeIdOrderState((int)$to_status, $id_order, true);
		$new_history->addWithemail(true);
    }

    public function ajaxCall ($type, $id_order, $employeeId, $to_status) {
        switch($type) {
            case "hold":
                $result = $this->capturePayment($id_order);
                if($result['result']) 
                {
                    $this->updateOrderStatus($id_order, $employeeId, $to_status);
                }
                
                return json_encode($result);
                break;
            case "void":
                return $this->voidCapturePayment($id_order, $employeeId, $to_status);
                break;
            case "refund":
                return $this->refundPayment($id_order, $employeeId, $to_status);
                break;
            case "reverse":
                return $this->reverseAuthorize($id_order, $employeeId, $to_status);
                break;
            
        }
    }
}
