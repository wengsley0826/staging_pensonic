{if $status == 'ok'}
<div class="alert alert-success">
	{if $payment_status == "100"}
		{l s='We have received your Payment Successfully. Thank you for you Purchases!' mod='cybersource'}
	{*
	*{else if $payment_status == "P"}
	*	{l s='Your payment is pending, we will notify you once the transaction completed. Thank you for you Purchases!' mod='cybersource'}
	*}
	{/if}	
</div>
{else}
<div class="alert alert-warning">
	{l s='An error occurred while processing your order!' mod='cybersource'}
</div>
{/if}
<div class="box cheque-box">
{if $status == 'ok'}
	<p><span class="price"><strong>Amount : {convertPrice price=$amount}</strong></span></p>
	<p><strong>Order #: {Tools::brandsOrderNumber($order_id)}</strong></p>	
	{*<p><strong>Order Reference: {$order_ref}</strong></p>*}
	<p><strong>Transaction ref: {$transaction_ref}</strong></p>	

{else}
	<p><span class="price"><strong>Amount : {convertPrice price=$amount|floatval}</strong></span></p>
	<p><strong>Order #: {Tools::brandsOrderNumber($order_id)}</strong></p>	
	{*<p><strong>Order Reference: {$order_ref}</strong></p>*}
	<p><strong>Transaction ref: {$transaction_ref}</strong></p>
{/if}
</div>