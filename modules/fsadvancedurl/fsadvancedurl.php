<?php
/**
 *  2016 ModuleFactory.co
 *
 *  @author    ModuleFactory.co <info@modulefactory.co>
 *  @copyright 2016 ModuleFactory.co
 *  @license   ModuleFactory.co Commercial License
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

$class_path = dirname(__FILE__).'/classes/';
require_once($class_path.'FsAdvancedUrlDataTransfer.php');
require_once($class_path.'FsAdvancedUrlHelperList.php');
require_once($class_path.'FsAdvancedUrlMessenger.php');
require_once($class_path.'FsAdvancedUrlModule.php');
require_once($class_path.'FsAdvancedUrlProduct.php');
require_once($class_path.'FsAdvancedUrlTools.php');

class Fsadvancedurl extends FsAdvancedUrlModule
{
    protected $duplicate_urls_count = 0;

    public function __construct()
    {
        $this->name = 'fsadvancedurl';
        $this->tab = 'seo';
        $this->version = '1.2.1';
        $this->author = 'ModuleFactory';
        $this->need_instance = 0;
        $this->ps_versions_compliancy['min'] = '1.5';
        $this->module_key = 'a5ff8f76a03ec7aa94b27bba0241ff4d';
        $this->displayName = $this->l('Advanced SEO Friendly URLs');
        $this->description = $this->l('Removes ID from URLs for better SEO');

        $this->register_hooks = array(
            'actionDispatcher',
            'moduleRoutes',
            'actionObjectProductAddAfter',
            'actionObjectProductUpdateAfter',
            'actionObjectCategoryAddAfter',
            'actionObjectCategoryUpdateAfter',
            'actionObjectManufacturerAddAfter',
            'actionObjectManufacturerUpdateAfter',
            'actionObjectSupplierAddAfter',
            'actionObjectSupplierUpdateAfter',
            'actionObjectCMSAddAfter',
            'actionObjectCMSUpdateAfter',
            'displayBackOfficeHeader',
            'displayHeader'
        );

        $this->default_config = array(
            'FSAU_ENABLE_pr' => 1,
            'FSAU_ENABLE_pr_categories' => 1,
            'FSAU_ENABLE_pr_category' => 0,
            'FSAU_ENABLE_pr_anchor_remove' => 0,
            'FSAU_product_rule_RT' => 'category',
            'FSAU_ENABLE_cr' => 1,
            'FSAU_ENABLE_cr_categories' => 1,
            'FSAU_category_rule_RT' => 'parent',
            'FSAU_ENABLE_manufacturer_rule' => 1,
            'FSAU_ENABLE_supplier_rule' => 1,
            'FSAU_ENABLE_cms_rule' => 1,
            'FSAU_MODULE_ROUTE_END' => 0,
            'FSAU_LINK_REWRITE_MODE' => 'regenerate_all',
            'FSAU_LINK_REWRITE_SCHEMA' => $this->generateMultilangualFields('{product_name}'),
            'FSAU_DISABLE_old_rules' => 0,
            'FSAU_HOME_ROUTE_CHECK' => 0,
            'FSAU_ROUTE_FRONT' => ''
        );

        parent::__construct();

        //1.6.0.11 Breaking changes
        $this->pagenotfound_name = version_compare(_PS_VERSION_, '1.6.0.11', '>=') ? 'pagenotfound' : '404';

        $default_tab = (Tools::getValue('conf', 0) == 12)?'fsau_help_tab':'fsau_product_url_tab';
        $this->tab_section = Tools::getValue('tab_section', $default_tab);
    }

    public function preInstall()
    {
        $return = true;

        if (!Tools::isSubmit('reset')) {
            $dispatcher = Dispatcher::getInstance();
            $default_routes = $dispatcher->default_routes;
            $configuration_prefix = 'PS_ROUTE_';

            if (Shop::isFeatureActive()) {
                Shop::setContext(Shop::CONTEXT_ALL);
            }

            foreach (array_keys($default_routes) as $rule) {
                $custom_rule = Configuration::get($configuration_prefix.$rule);
                if ($custom_rule) {
                    $default_routes[$rule]['rule'] = $custom_rule;
                }
            }
            $return = $return && Configuration::updateValue(
                'FSAU_OLD_REWRITE_RULES',
                $this->jsonEncode($default_routes)
            );

            //Delete old rules
            foreach (array_keys($default_routes) as $rule) {
                Configuration::deleteByName($configuration_prefix.$rule);
            }

            if (version_compare(_PS_VERSION_, '1.6.0.11', '>=')) {
                $override_path = dirname(__FILE__).'/override/';
                $override_version_path = dirname(__FILE__).'/override_versions/1.6.0.11/';
                $files_to_copy = Tools::scandir($override_version_path, 'php', '', true);
                if ($files_to_copy) {
                    foreach ($files_to_copy as $file) {
                        Tools::copy($override_version_path.$file, $override_path.$file);
                    }
                }
            }

            if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $override_path = dirname(__FILE__).'/override/';
                $override_version_path = dirname(__FILE__).'/override_versions/1.7.0.0/';
                $files_to_copy = Tools::scandir($override_version_path, 'php', '', true);
                if ($files_to_copy) {
                    foreach ($files_to_copy as $file) {
                        Tools::copy($override_version_path.$file, $override_path.$file);
                    }
                }
            }
        }

        return $return;
    }

    public function install()
    {
        $return = true;
        $return = $return && $this->preInstall();
        $return = $return && parent::install();

        $has_index = $this->hasDbTableIndex('product_lang', 'link_rewrite');
        if (!$has_index) {
            $this->addDbTableIndex('product_lang', 'link_rewrite');
        }

        $has_index = $this->hasDbTableIndex('category_lang', 'link_rewrite');
        if (!$has_index) {
            $this->addDbTableIndex('category_lang', 'link_rewrite');
        }

        $tab = Tab::getInstanceFromClassName('AdminFsadvancedurl', Configuration::get('PS_LANG_DEFAULT'));
        if (!$tab->module) {
            $tab = new Tab();
            $tab->id_parent = -1;
            $tab->position = 0;
            $tab->module = $this->name;
            $tab->class_name = 'AdminFsadvancedurl';
            $tab->active = 1;
            $tab->name = $this->generateMultilangualFields($this->displayName);
            $tab->save();
        }

        return $return;
    }

    #################### ADMIN ####################

    public function getContent()
    {
        $context = Context::getContext();
        $context->controller->addCSS($this->getPath().'views/css/admin.css', 'all');
        $context->controller->addCSS($this->getPath().'views/css/sweetalert.css', 'all');
        if ($this->isPs15()) {
            $context->controller->addCSS($this->getPath().'views/css/bootstrap-progress-bar.css', 'all');
        }
        $context->controller->addJS($this->getPath().'views/js/admin_config.js');
        $context->controller->addJS($this->getPath().'views/js/sweetalert.min.js');


        $error_string = $this->l('Please enable the "%s" option in "%s" -> "%s" -> "%s" panel!');
        $menu_1 = $this->l('Preferences');
        $menu_2 = $this->l('SEO & URLs');
        $panel = $this->l('Set Up URLs');

        $dispatcher = Dispatcher::getInstance();
        $default_routes = $dispatcher->default_routes;

        $html = $this->getCssAndJs();
        if (!Configuration::get('PS_REWRITING_SETTINGS')) {
            $html .= $this->displayError(sprintf(
                $error_string,
                $this->l('Friendly URL'),
                $menu_1,
                $menu_2,
                $panel
            ));
        }

        if (!Configuration::get('PS_CANONICAL_REDIRECT')) {
            $html .= $this->displayError(sprintf(
                $error_string,
                $this->l('Redirect to the canonical URL'),
                $menu_1,
                $menu_2,
                $panel
            ));
        }

        $html .= FsAdvancedUrlMessenger::getMessagesHtml();

        if (Tools::isSubmit('save_'.$this->name)) {
            $valid = true;
            $form_values = array();
            foreach ($this->getConfigKeys() as $config_key) {
                if (Tools::isSubmit($config_key)) {
                    $form_values[$config_key] = Tools::getValue($config_key, Configuration::get($config_key));
                }
            }

            if ($this->getMultilangualConfigKeys()) {
                foreach ($this->getMultilangualConfigKeys() as $multilang_config_key) {
                    if (FsAdvancedUrlTools::isSubmitMultilang($multilang_config_key)) {
                        $form_values[$multilang_config_key] = $this->getMultilangualValue($multilang_config_key);
                    }
                }
            }

            if (Tools::isSubmit('FSAU_ENABLE_pr')) {
                if (Tools::getValue('FSAU_ENABLE_pr_categories') && Tools::getValue('FSAU_ENABLE_pr_category')) {
                    $valid = false;
                    FsAdvancedUrlMessenger::addErrorMessage($this->l(implode(' ', array(
                        'You can not enable both parent categories and default category.',
                        'Please enable only which you want to use.'
                    ))));
                }
            }

            if ($valid) {
                foreach ($form_values as $option_key => $form_value) {
                    Configuration::updateValue($option_key, $form_value, true);
                }

                FsAdvancedUrlMessenger::addSuccessMessage($this->l('Update successful'));
            }

            FsAdvancedUrlTools::redirect($this->url().'&tab_section='.$this->tab_section);
        } elseif (Tools::isSubmit('save_'.$this->name.'_link_rewrite_generator_config')) {
            Configuration::updateValue(
                'FSAU_LINK_REWRITE_SCHEMA',
                self::getMultilangualValue('FSAU_LINK_REWRITE_SCHEMA'),
                true
            );
            Configuration::updateValue(
                'FSAU_LINK_REWRITE_MODE',
                Tools::getValue('FSAU_LINK_REWRITE_MODE', 'regenerate_all'),
                true
            );

            FsAdvancedUrlMessenger::addSuccessMessage($this->l('Update successful'));
            FsAdvancedUrlTools::redirect($this->url().'&tab_section='.$this->tab_section);
        } else {
            $tab_content = array();
            $forms_fields_value = Configuration::getMultiple($this->getConfigKeys());
            if ($this->getMultilangualConfigKeys()) {
                foreach ($this->getMultilangualConfigKeys() as $multilang_config_key) {
                    $forms_fields_value[$multilang_config_key] =
                        $this->getMultilangualConfiguration($multilang_config_key);
                }
            }

            $route_schema = Configuration::get('PS_ROUTE_product_rule');
            if (!$route_schema) {
                $route_schema = $default_routes['product_rule']['rule'];
            }
            $forms_fields_value['FSAU_product_rule'] = '<div class="fsau-schema-url">'.$route_schema.'</div>';

            $route_schema = Configuration::get('PS_ROUTE_category_rule');
            if (!$route_schema) {
                $route_schema = $default_routes['category_rule']['rule'];
            }
            $forms_fields_value['FSAU_category_rule'] = '<div class="fsau-schema-url">'.$route_schema.'</div>';

            $route_schema = Configuration::get('PS_ROUTE_manufacturer_rule');
            if (!$route_schema) {
                $route_schema = $default_routes['manufacturer_rule']['rule'];
            }
            $forms_fields_value['FSAU_manufacturer_rule'] = '<div class="fsau-schema-url">'.$route_schema.'</div>';

            $route_schema = Configuration::get('PS_ROUTE_supplier_rule');
            if (!$route_schema) {
                $route_schema = $default_routes['supplier_rule']['rule'];
            }
            $forms_fields_value['FSAU_supplier_rule'] = '<div class="fsau-schema-url">'.$route_schema.'</div>';

            $route_schema = Configuration::get('PS_ROUTE_cms_rule');
            if (!$route_schema) {
                $route_schema = $default_routes['cms_rule']['rule'];
            }
            $forms_fields_value['FSAU_cms_rule'] = '<div class="fsau-schema-url">'.$route_schema.'</div>';

            $route_schema = Configuration::get('PS_ROUTE_cms_category_rule');
            if (!$route_schema) {
                $route_schema = $default_routes['cms_category_rule']['rule'];
            }
            $forms_fields_value['FSAU_cms_category_rule'] = '<div class="fsau-schema-url">'.$route_schema.'</div>';

            $tab_content_product_url = $this->renderProductUrlSettingsForm($forms_fields_value);
            $tab_content[] = array(
                'id' => 'fsau_product_url_tab',
                'title' => $this->l('Product URL Settings'),
                'content' => $tab_content_product_url
            );

            $tab_content_category_url = $this->renderCategoryUrlSettingsForm($forms_fields_value);
            $tab_content[] = array(
                'id' => 'fsau_category_url_tab',
                'title' => $this->l('Category URL Settings'),
                'content' => $tab_content_category_url
            );

            $tab_content_manufacturer_url = $this->renderManufacturerUrlSettingsForm($forms_fields_value);
            $tab_content[] = array(
                'id' => 'fsau_manufacturer_url_tab',
                'title' => $this->l('Manufacturer URL Settings'),
                'content' => $tab_content_manufacturer_url
            );

            $tab_content_supplier_url = $this->renderSupplierUrlSettingsForm($forms_fields_value);
            $tab_content[] = array(
                'id' => 'fsau_supplier_url_tab',
                'title' => $this->l('Supplier URL Settings'),
                'content' => $tab_content_supplier_url
            );

            $tab_content_cms_url = $this->renderCmsUrlSettingsForm($forms_fields_value);
            $tab_content[] = array(
                'id' => 'fsau_cms_url_tab',
                'title' => $this->l('CMS & CMS Category URL Settings'),
                'content' => $tab_content_cms_url
            );

            $tab_content_duplicate = $this->renderDuplicateList();
            $tab_content_duplicate .= $this->renderProductLinkRewriteGeneratorForm($forms_fields_value);
            $tab_content[] = array(
                'id' => 'fsau_duplicate_tab',
                'title' => (($this->duplicate_urls_count)?'<span class="fsau-duplicate-alert">( !! )</span> ':'').
                    $this->l('Duplicate URLs'),
                'content' => $tab_content_duplicate
            );

            $tab_content_advanced = $this->renderAdvancedSettingsForm($forms_fields_value);
            $tab_content[] = array(
                'id' => 'fsau_advanced_tab',
                'title' => $this->l('Advanced Settings'),
                'content' => $tab_content_advanced
            );

            $this->smartyAssign(array(
                'fsau_performance_url' => $this->context->link->getAdminLink('AdminPerformance'),
                'fsau_seo_url' => $this->context->link->getAdminLink('AdminMeta'),
            ));

            $tab_content_help = $this->smartyFetch('admin/help.tpl');
            $tab_content[] = array(
                'id' => 'fsau_help_tab',
                'title' => $this->l('Help'),
                'content' => $tab_content_help
            );

            $html .= $this->renderTabLayout($tab_content, $this->tab_section);
        }

        return $html;
    }

    protected function renderProductUrlSettingsForm($fields_values)
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $help_string = $this->l('You can edit scheme in "%s" -> "%s" -> "%s" panel!');
        $menu_1 = $this->l('Preferences');
        $menu_2 = $this->l('SEO & URLs');
        $panel = $this->l('Schema of URLs');
        $not_recommend = $this->l('We NOT recommend to change the default settings!');

        $help_category_url = 'http://domain.com/default-cat/product-rewrite.html';
        $help_categories_url = 'http://domain.com/parent-cat/child-cat/product-rewrite.html';

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Advanced URLs To Products')
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'tab_section',
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable advanced URL:'),
                    'name' => 'FSAU_ENABLE_pr',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_pr_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_pr_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l('Select yes to remove ID from product URLs'),
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable default category in URL:'),
                    'name' => 'FSAU_ENABLE_pr_category',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_pr_category_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_pr_category_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l('URLs going to looks like:').' '.$help_category_url,
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable parent categories in URL:'),
                    'name' => 'FSAU_ENABLE_pr_categories',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_pr_categories_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_pr_categories_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l('URLs going to looks like:').' '.$help_categories_url,
                ),
            )
        );

        if ($this->isPsMin17()) {
            $help_anchor = implode('', array(
                $this->l('Removes default combination parameter from the URL, which is after the hash mark'),
                '<br />',
                $help_categories_url,
                '<span class="fsau-line-through fsau-bold">',
                '#/1-size-s/8-color-white',
                '</span>'
            ));

            $fields_form[0]['form']['input'][] = array(
                'type' => ($this->isPs15())?'radio':'switch',
                'label' => $this->l('Remove anchor from URL:'),
                'name' => 'FSAU_ENABLE_pr_anchor_remove',
                'class' => 't',
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'FSAU_ENABLE_pr_anchor_remove_on',
                        'value' => 1,
                        'label' => $this->l('Yes')
                    ),
                    array(
                        'id' => 'FSAU_ENABLE_pr_anchor_remove_off',
                        'value' => 0,
                        'label' => $this->l('No')
                    ),
                ),
                'desc' => $help_anchor,
            );
        }

        $fields_form[0]['form']['input'][] = array(
            'type' => 'select',
            'label' => $this->l('Product not found redirect type:'),
            'name' => 'FSAU_product_rule_RT',
            'options' => array(
                'query' => array(
                    array('id' => 'default', 'name' =>  $this->l('No Redirect (Default)')),
                    array('id' => 'category', 'name' =>  $this->l('Try to redirect to category (Recommended)')),
                    array('id' => '404', 'name' =>  $this->l('Redirect to page not found page')),
                ),
                'id' => 'id',
                'name' => 'name',
            ),
        );

        $fields_form[0]['form']['input'][] = array(
            'type' => 'free',
            'label' => $this->l('Current schema of URL:'),
            'name' => 'FSAU_product_rule',
            'desc' => sprintf($help_string, $menu_1, $menu_2, $panel).'<br />'.$not_recommend,
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = 'fsau_product_url_settings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->getLanguagesForForm();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->show_toolbar = false;
        $helper->submit_action = 'save_'.$this->name;
        $helper->fields_value = $fields_values;
        $helper->fields_value['tab_section'] = 'fsau_product_url_tab';

        $fields_form[0]['form']['submit'] = array('title' => $this->l('Save'));
        if ($this->isPs15()) {
            $fields_form[0]['form']['submit']['class'] = 'button';
        }

        return $helper->generateForm($fields_form);
    }

    protected function renderCategoryUrlSettingsForm($fields_values)
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $help_string = $this->l('You can edit scheme in "%s" -> "%s" -> "%s" panel!');
        $menu_1 = $this->l('Preferences');
        $menu_2 = $this->l('SEO & URLs');
        $panel = $this->l('Schema of URLs');
        $not_recommend = $this->l('We NOT recommend to change the default settings!');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Advanced URLs To Categories')
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'tab_section',
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable advanced URL:'),
                    'name' => 'FSAU_ENABLE_cr',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_cr_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_cr_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l('Select yes to remove ID from category URLs'),
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable parent categories in URL:'),
                    'name' => 'FSAU_ENABLE_cr_categories',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_cr_categories_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_cr_categories_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l(
                        'URLs going to looks like: http://yourdomain.com/parent-cat/child-cat/category-rewrite/'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Category not found redirect type:'),
                    'name' => 'FSAU_category_rule_RT',
                    'options' => array(
                        'query' => array(
                            array('id' => 'default', 'name' =>  $this->l('Redirect to page not found page (Default)')),
                            array(
                                'id' => 'parent',
                                'name' =>  $this->l('Try to redirect to parent category (Recommended)')
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'free',
                    'label' => $this->l('Current schema of URL:'),
                    'name' => 'FSAU_category_rule',
                    'desc' => sprintf($help_string, $menu_1, $menu_2, $panel).'<br />'.$not_recommend,
                ),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = 'fsau_category_url_settings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->getLanguagesForForm();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->show_toolbar = false;
        $helper->submit_action = 'save_'.$this->name;
        $helper->fields_value = $fields_values;
        $helper->fields_value['tab_section'] = 'fsau_category_url_tab';

        $fields_form[0]['form']['submit'] = array('title' => $this->l('Save'));
        if ($this->isPs15()) {
            $fields_form[0]['form']['submit']['class'] = 'button';
        }

        return $helper->generateForm($fields_form);
    }

    protected function renderManufacturerUrlSettingsForm($fields_values)
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $help_string = $this->l('You can edit scheme in "%s" -> "%s" -> "%s" panel!');
        $menu_1 = $this->l('Preferences');
        $menu_2 = $this->l('SEO & URLs');
        $panel = $this->l('Schema of URLs');
        $not_recommend = $this->l('We NOT recommend to change the default settings!');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Advanced URLs To Manufacturers')
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'tab_section',
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable advanced URL:'),
                    'name' => 'FSAU_ENABLE_manufacturer_rule',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_manufacturer_rule_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_manufacturer_rule_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l('Select yes to remove ID from manufacturer URLs'),
                ),
                array(
                    'type' => 'free',
                    'label' => $this->l('Current schema of URL:'),
                    'name' => 'FSAU_manufacturer_rule',
                    'desc' => sprintf($help_string, $menu_1, $menu_2, $panel).'<br />'.$not_recommend,
                ),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = 'fsau_manufacturer_url_settings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->getLanguagesForForm();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->show_toolbar = false;
        $helper->submit_action = 'save_'.$this->name;
        $helper->fields_value = $fields_values;
        $helper->fields_value['tab_section'] = 'fsau_manufacturer_url_tab';

        $fields_form[0]['form']['submit'] = array('title' => $this->l('Save'));
        if ($this->isPs15()) {
            $fields_form[0]['form']['submit']['class'] = 'button';
        }

        return $helper->generateForm($fields_form);
    }

    protected function renderSupplierUrlSettingsForm($fields_values)
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $help_string = $this->l('You can edit scheme in "%s" -> "%s" -> "%s" panel!');
        $menu_1 = $this->l('Preferences');
        $menu_2 = $this->l('SEO & URLs');
        $panel = $this->l('Schema of URLs');
        $not_recommend = $this->l('We NOT recommend to change the default settings!');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Advanced URLs To Suppliers')
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'tab_section',
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable advanced URL:'),
                    'name' => 'FSAU_ENABLE_supplier_rule',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_supplier_rule_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_supplier_rule_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l('Select yes to remove ID from supplier URLs'),
                ),
                array(
                    'type' => 'free',
                    'label' => $this->l('Current schema of URL:'),
                    'name' => 'FSAU_supplier_rule',
                    'desc' => sprintf($help_string, $menu_1, $menu_2, $panel).'<br />'.$not_recommend,
                ),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = 'fsau_supplier_url_settings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->getLanguagesForForm();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->show_toolbar = false;
        $helper->submit_action = 'save_'.$this->name;
        $helper->fields_value = $fields_values;
        $helper->fields_value['tab_section'] = 'fsau_supplier_url_tab';

        $fields_form[0]['form']['submit'] = array('title' => $this->l('Save'));
        if ($this->isPs15()) {
            $fields_form[0]['form']['submit']['class'] = 'button';
        }

        return $helper->generateForm($fields_form);
    }

    protected function renderCmsUrlSettingsForm($fields_values)
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $help_string = $this->l('You can edit scheme in "%s" -> "%s" -> "%s" panel!');
        $menu_1 = $this->l('Preferences');
        $menu_2 = $this->l('SEO & URLs');
        $panel = $this->l('Schema of URLs');
        $not_recommend = $this->l('We NOT recommend to change the default settings!');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Advanced URLs To CMS & CMS Category')
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'tab_section',
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Enable advanced URL:'),
                    'name' => 'FSAU_ENABLE_cms_rule',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_ENABLE_cms_rule_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_ENABLE_cms_rule_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => $this->l('Select yes to remove ID from cms & cms category URLs'),
                ),
                array(
                    'type' => 'free',
                    'label' => $this->l('Current schema of CMS URL:'),
                    'name' => 'FSAU_cms_rule',
                    'desc' => sprintf($help_string, $menu_1, $menu_2, $panel).'<br />'.$not_recommend,
                ),
                array(
                    'type' => 'free',
                    'label' => $this->l('Current schema of CMS Category URL:'),
                    'name' => 'FSAU_cms_category_rule',
                    'desc' => sprintf($help_string, $menu_1, $menu_2, $panel).'<br />'.$not_recommend,
                ),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = 'fsau_cms_url_settings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->getLanguagesForForm();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->show_toolbar = false;
        $helper->submit_action = 'save_'.$this->name;
        $helper->fields_value = $fields_values;
        $helper->fields_value['tab_section'] = 'fsau_cms_url_tab';

        $fields_form[0]['form']['submit'] = array('title' => $this->l('Save'));
        if ($this->isPs15()) {
            $fields_form[0]['form']['submit']['class'] = 'button';
        }

        return $helper->generateForm($fields_form);
    }

    protected function renderDuplicateList()
    {
        $context = Context::getContext();
        $duplicate_fields_list = array();

        if (Shop::isFeatureActive()) {
            $duplicate_fields_list['shop'] = array(
                'title' => $this->l('Shop'),
            );
        }

        $duplicate_fields_list['type'] = array(
            'title' => $this->l('URL type'),
        );

        $duplicate_fields_list['id_object'] = array(
            'title' => $this->l('ID'),
        );

        $duplicate_fields_list['name'] = array(
            'title' => $this->l('Name'),
        );

        $duplicate_fields_list['lang'] = array(
            'title' => $this->l('Lang'),
        );

        $duplicate_fields_list['link_rewrite'] = array(
            'title' => $this->l('Friendly URL'),
        );

        $helper = new FsAdvancedUrlHelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->no_link = true;
        $helper->identifier = 'id';
        $helper->actions = array('editobject');
        $helper->show_toolbar = false;
        $helper->imageType = 'jpg';
        $helper->title[] = $this->l('Duplicate URLs');
        $helper->table = $this->name;
        $helper->module = $this;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $context->link->getAdminLink('AdminModules', false).'&configure='.$this->name;

        $duplicate_urls = array();
        if (Configuration::get('FSAU_ENABLE_pr')) {
            $duplicate_urls = array_merge($duplicate_urls, $this->getProductDuplicate());
        }

        if (Configuration::get('FSAU_ENABLE_cr')) {
            $duplicate_urls = array_merge($duplicate_urls, $this->getCategoryDuplicate());
        }

        if (Configuration::get('FSAU_ENABLE_manufacturer_rule')) {
            $duplicate_urls = array_merge($duplicate_urls, $this->getManufacturerDuplicate());
        }

        if (Configuration::get('FSAU_ENABLE_supplier_rule')) {
            $duplicate_urls = array_merge($duplicate_urls, $this->getSupplierDuplicate());
        }

        if (Configuration::get('FSAU_ENABLE_cms_rule')) {
            $duplicate_urls = array_merge($duplicate_urls, $this->getCMSDuplicate());
            $duplicate_urls = array_merge($duplicate_urls, $this->getCMSCategoryDuplicate());
        }

        $this->duplicate_urls_count = count($duplicate_urls);

        $this->smartyAssign(array(
            'list_title_15' => $this->l('Duplicate URLs'),
            'generated_list' => $helper->generateList($duplicate_urls, $duplicate_fields_list)
        ));

        return $this->smartyFetch('admin/list_wrapper.tpl');
    }

    protected function renderProductLinkRewriteGeneratorForm($fields_values)
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $id_lang = $this->context->language->id;

        $generator_keywords_product_features = array();
        $features = Feature::getFeatures($id_lang);
        foreach ($features as $feature) {
            $f = new Feature($feature['id_feature'], $id_lang);
            $generator_keywords_product_features[] = 'feature_'.str_replace('-', '_', Tools::str2url($f->name));
        }

        $link_rewrite_keywords = array(
            'product_name',
            'product_meta_title',
            'product_meta_keywords',
            'product_ean13',
            'product_upc',
            'product_reference',
            'product_price',
            'product_tags',
            'default_category_name',
            'default_category_meta_title',
            'default_category_link_rewrite',
            'manufacturer_name',
            'manufacturer_meta_title',
            'supplier_name',
            'supplier_meta_title'
        );

        $link_rewrite_keywords = array_merge($link_rewrite_keywords, $generator_keywords_product_features);

        $link_rewrite_desc = $this->generateAvailableKeywordsMultilang(
            $link_rewrite_keywords,
            'FSAU_LINK_REWRITE_SCHEMA_'.$default_lang
        );

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Product Friendly URL Generator')
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'tab_section',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Regeneration Type:'),
                    'name' => 'FSAU_LINK_REWRITE_MODE',
                    'options' => array(
                        'query' => array(
                            array('id' => 'regenerate_all', 'name' =>  $this->l('Regenerate All Product URLs')),
                            array(
                                'id' => 'regenerate_duplicate',
                                'name' =>  $this->l('Regenerate Only Duplicated Product URLs')
                            ),
                            array(
                                'id' => 'append_duplicate',
                                'name' =>  $this->l('Append Extra Information To Duplicated Product URLs')
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Friendly URL Schema:'),
                    'lang' => true,
                    'name' => 'FSAU_LINK_REWRITE_SCHEMA',
                    'size' => 70,
                    'required' => true,
                    'desc' => $link_rewrite_desc
                ),
                array(
                    'type' => 'free',
                    'label' => $this->l('Status:'),
                    'lang' => false,
                    'name' => 'product_link_rewrite_progress_bar',
                    'required' => false
                )
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->show_toolbar = true;
        $helper->submit_action = 'save_'.$this->name.'_link_rewrite_generator_config';
        $helper->toolbar_scroll = false;
        $helper->title[] = $this->l('Product Friendly URL Generator');
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => $this->url().'&save_'.$this->name.'_link_rewrite_generator_config',
                ),
        );


        $helper->fields_value = $fields_values;
        $helper->fields_value['tab_section'] = 'fsau_duplicate_tab';
        $helper->fields_value['product_link_rewrite_progress_bar'] =  $this->smartyFetch('admin/progress_bar.tpl');

        $fields_form[0]['form']['submit'] = array('title' => $this->l('Save'));
        if ($this->isPs15()) {
            $fields_form[0]['form']['submit']['class'] = 'button';
        }

        $fields_form[0]['form']['buttons'][] = array(
            'title' => '<i class="process-icon-update"></i>'.$this->l('Generate'),
            'href' => 'javascript:;',
            'icon' => 'update',
            'js' => 'FSAU.generateProductLinkRewrite();'
        );

        $generated_form = $helper->generateForm($fields_form);

        if ($this->isPs15()) {
            $generated_form .= $this->smartyFetch('admin/product_link_rewrite_generator_form_15.tpl');
        }

        return $generated_form;
    }

    protected function renderAdvancedSettingsForm($fields_values)
    {
        $fields_form = array();
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Advanced Settings')
            ),
            'description' => $this->l('Need help? Please contact us!'),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'tab_section',
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Module Route to end'),
                    'name' => 'FSAU_MODULE_ROUTE_END',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_MODULE_ROUTE_END_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_MODULE_ROUTE_END_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => implode(' ', array(
                        $this->l('If you use a module route like this').' {module}{/:controller}.',
                        $this->l('Enable this.')
                    ))
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Disable old routes'),
                    'name' => 'FSAU_DISABLE_old_rules',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_DISABLE_old_rules_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_DISABLE_old_rules_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => implode(' ', array(
                       $this->l('Occasionally the old rules conflicts with the new ones.'),
                       $this->l('If you experience malfunction, enable this and try again.')
                    ))
                ),
                array(
                    'type' => ($this->isPs15())?'radio':'switch',
                    'label' => $this->l('Extra home route check'),
                    'name' => 'FSAU_HOME_ROUTE_CHECK',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'FSAU_HOME_ROUTE_CHECK_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'FSAU_HOME_ROUTE_CHECK_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'desc' => sprintf(
                        $this->l('Enable if you use a route like this %s or %s and no ending slash.'),
                        '"{categories:/}{rewrite}"',
                        '"{rewrite}"'
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Selected Route to front:'),
                    'name' => 'FSAU_ROUTE_FRONT',
                    'options' => array(
                        'query' => $this->getRoutesForSelector(),
                        'id' => 'id',
                        'name' => 'name',
                    ),
                )
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = 'fsau_advanced_settings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->getLanguagesForForm();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->show_toolbar = false;
        $helper->submit_action = 'save_'.$this->name;
        $helper->fields_value = $fields_values;
        $helper->fields_value['tab_section'] = 'fsau_advanced_tab';

        $fields_form[0]['form']['submit'] = array('title' => $this->l('Save'));
        if ($this->isPs15()) {
            $fields_form[0]['form']['submit']['class'] = 'button';
        }

        return $helper->generateForm($fields_form);
    }

    protected function renderTabLayout($layout, $active_tab)
    {
        $this->smartyAssign(array(
            'fsau_tab_layout' => $layout,
            'fsau_active_tab' => $active_tab
        ));

        if ($this->isPs15()) {
            return $this->smartyFetch('admin/tab_layout_15.tpl');
        }

        return $this->smartyFetch('admin/tab_layout.tpl');
    }

    #################### HOOKS ####################

    public function hookModuleRoutes($params)
    {
        $context = Context::getContext();
        $context->smarty->assign('params_hash', sha1($this->jsonEncode($params)));
        $id_shop = $context->shop->id;
        $id_shop_group = $context->shop->id_shop_group;
        $rules = array();

        if (Configuration::get('FSAU_HOME_ROUTE_CHECK')) {
            $rules['index_rule'] = array(
                'controller' => 'index',
                'rule' => '',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'controller'
                )
            );
        }

        if (Configuration::get('FSAU_ENABLE_manufacturer_rule')) {
            $rules['manufacturer_rule'] = array(
                'controller' => 'manufacturer',
                'rule' => 'manufacturer/{rewrite}.html',
                'keywords' => array(
                    'id' =>            array('regexp' => '[0-9]+'),
                    'rewrite' =>       array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'fsau_rewrite_manufacturer'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_title' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'controller'
                )
            );
        }

        if (Configuration::get('FSAU_ENABLE_supplier_rule')) {
            $rules['supplier_rule'] = array(
                'controller' => 'supplier',
                'rule' => 'supplier/{rewrite}.html',
                'keywords' => array(
                    'id' =>            array('regexp' => '[0-9]+'),
                    'rewrite' =>       array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'fsau_rewrite_supplier'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_title' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'controller'
                )
            );
        }

        if (Configuration::get('FSAU_ENABLE_cr')) {
            $rules['category_rule'] = array(
                'controller' => 'category',
                'rule' => '{categories:/}{rewrite}/',
                'keywords' => array(
                    'id' =>         array('regexp' => '[0-9]+'),
                    'rewrite' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'fsau_rewrite_category'),
                    'categories' => array(
                        'regexp' => '[/_a-zA-Z0-9-\pL]*',
                        'param' =>  'fsau_rewrite_categories'
                    ),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_title' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'controller'
                )
            );

            if (!Configuration::get('FSAU_ENABLE_cr_categories')) {
                $rules['category_rule']['rule'] = '{rewrite}/';
                unset($rules['category_rule']['keywords']['categories']['param']);
            }
        }

        if (Configuration::get('FSAU_ENABLE_cms_rule')) {
            $rules['cms_rule'] = array(
                'controller' => 'cms',
                'rule' => 'content/{rewrite}.html',
                'keywords' => array(
                    'id' =>            array('regexp' => '[0-9]+'),
                    'rewrite' =>       array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'fsau_rewrite_cms'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_title' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'controller'
                )
            );

            $rules['cms_category_rule'] = array(
                'controller' => 'cms',
                'rule' => 'content/{rewrite}/',
                'keywords' => array(
                    'id' =>            array('regexp' => '[0-9]+'),
                    'rewrite' =>       array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'fsau_rewrite_cms_category'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_title' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'controller'
                )
            );
        }

        if (Configuration::get('FSAU_ENABLE_pr')) {
            $rules['product_rule'] = array(
                'controller' => 'product',
                'rule' => '{categories:/}{rewrite}.html',
                'keywords' => array(
                    'id' =>         array('regexp' => '[0-9]+'),
                    'rewrite' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'fsau_rewrite_product'),
                    'ean13' =>      array('regexp' => '[0-9\pL]*'),
                    'category' =>   array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'reference' =>     array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_title' =>    array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'manufacturer' =>  array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'supplier' =>      array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'price' =>         array('regexp' => '[0-9\.,]*'),
                    'tags' =>          array('regexp' => '[a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'controller'
                )
            );

            $force_id_product_attribute = false;
            if ((Tools::getValue('action', 'none') == 'productrefresh')
                && Tools::getValue('ajax', false)
                && Tools::getValue('id_product', false)
                && Tools::getValue('qty', false)
                && Tools::isSubmit('group')) {
                $force_id_product_attribute = true;
            }

            if ($this->isPsMin17() && !$force_id_product_attribute) {
                $rules['product_rule']['keywords']['id_product_attribute'] = array(
                    'regexp' => '[0-9]+'
                );
            }

            if (Configuration::get('FSAU_ENABLE_pr_category')) {
                $rules['product_rule']['rule'] = '{category:/}{rewrite}.html';
                $rules['product_rule']['keywords']['category'] = array(
                    'regexp' => '[/_a-zA-Z0-9-\pL]*',
                    'param' => 'fsau_rewrite_category'
                );
            }

            if (Configuration::get('FSAU_ENABLE_pr_categories')) {
                $rules['product_rule']['rule'] = '{categories:/}{rewrite}.html';
                $rules['product_rule']['keywords']['categories'] = array(
                    'regexp' => '[/_a-zA-Z0-9-\pL]*',
                    'param' => 'fsau_rewrite_categories'
                );
            }
        }

        if (Configuration::get('FSAU_ENABLE_cr')) {
            $rules['layered_rule'] = array(
                'controller' => 'category',
                'rule' => '{categories:/}{rewrite}/{/:selected_filters}',
                'keywords' => array(
                    'id' =>                array('regexp' => '[0-9]+'),
                    'selected_filters' =>  array('regexp' => '.*', 'param' => 'selected_filters'),
                    'rewrite' =>           array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'fsau_rewrite_category'),
                    'meta_keywords' =>     array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    'meta_title' =>        array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'controller'
                )
            );
        }

        $old_rewrite_rules = Configuration::get('FSAU_OLD_REWRITE_RULES');
        $disable_old_rules = Configuration::get('FSAU_DISABLE_old_rules');
        if (Shop::isFeatureActive()) {
            $old_rewrite_rules = Configuration::get('FSAU_OLD_REWRITE_RULES', false, $id_shop_group, $id_shop);
            $disable_old_rules = Configuration::get('FSAU_DISABLE_old_rules', false, $id_shop_group, $id_shop);
        }

        if ($old_rewrite_rules && !$disable_old_rules) {
            $old_rewrite_rules = $this->jsonDecode($old_rewrite_rules, true);
            foreach ($old_rewrite_rules as $rule => $route) {
                $rules['old_'.$rule] = $route;
                $rules['old_'.$rule]['params'] = array();
            }
        }

        return $rules;
    }

    public function hookActionDispatcher($params)
    {
        if ($params['controller_type'] == 1) {
            $context = Context::getContext();
            $id_lang = $context->cookie->id_lang;
            $id_shop = $context->shop->id;
            $dispatcher = Dispatcher::getInstance();
            if (!is_callable(array($dispatcher, 'getRoutes')) || !is_callable(array($dispatcher, 'getRequestUri'))) {
                return false;
            }
            $routes = $dispatcher->getRoutes();

            //Below this version no shop defined in the routes
            if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
                $routes = $routes[$id_shop];
            }

            switch ($params['controller_class']) {
                case 'ProductController':
                    if ($rewrite = Tools::getValue('fsau_rewrite_product')) {
                        $id_parent = $context->shop->id_category;

                        $rewrite_categories = Tools::getValue('fsau_rewrite_categories');
                        if ($rewrite_categories) {
                            $rewrite_categories_array = explode('/', $rewrite_categories);
                            foreach ($rewrite_categories_array as $rewrite_category) {
                                $sql = 'SELECT cl.`id_category` FROM `'._DB_PREFIX_.'category_lang` cl';
                                $sql .= ' LEFT JOIN `'._DB_PREFIX_.'category` c ON cl.id_category = c.id_category';
                                $sql .= ' WHERE cl.`id_shop` = \''.pSQL($id_shop);
                                $sql .= '\' AND cl.`link_rewrite` = \''.pSQL($rewrite_category).'\'';
                                $sql .= ' AND c.id_parent = \''.pSQL($id_parent).'\'';
                                $id_parent = Db::getInstance()->getValue(
                                    $sql.' AND cl.`id_lang` = \''.pSQL($id_lang).'\''
                                );

                                if (!$id_parent) {
                                    $id_parent = Db::getInstance()->getValue($sql);
                                }
                            }
                        }

                        $rewrite_category = Tools::getValue('fsau_rewrite_category');
                        if ($rewrite_category) {
                            $sql = 'SELECT cl.`id_category` FROM `'._DB_PREFIX_.'category_lang` cl';
                            $sql .= ' LEFT JOIN `'._DB_PREFIX_.'category` c ON cl.id_category = c.id_category';
                            $sql .= ' WHERE cl.`id_shop` = \''.pSQL($id_shop);
                            $sql .= '\' AND cl.`link_rewrite` = \''.pSQL($rewrite_category).'\'';
                            $id_parent = Db::getInstance()->getValue(
                                $sql.' AND cl.`id_lang` = \''.pSQL($id_lang).'\''
                            );

                            if (!$id_parent) {
                                $id_parent = Db::getInstance()->getValue($sql);
                            }
                        }

                        $sql = 'SELECT p.`id_product` FROM `'._DB_PREFIX_.'product_lang` pl';
                        $sql .= ' LEFT JOIN `'._DB_PREFIX_.'product` p ON pl.id_product = p.id_product WHERE';
                        $sql .= ' `id_shop` = \''.pSQL($id_shop).'\' AND `link_rewrite` = \''.pSQL($rewrite).'\'';
                        if (Configuration::get('FSAU_ENABLE_pr_categories') ||
                            Configuration::get('FSAU_ENABLE_pr_category')) {
                            $sql .= ' AND p.id_category_default = \''.pSQL($id_parent).'\'';
                        }

                        $id_product = Db::getInstance()->getValue($sql.' AND `id_lang` = \''.pSQL($id_lang).'\'');

                        if (!$id_product) {
                            $id_product = Db::getInstance()->getValue($sql);
                        }

                        if (!$id_product) {
                            if (isset($routes[$id_lang]['old_product_rule'])) {
                                if (preg_match(
                                    $routes[$id_lang]['old_product_rule']['regexp'],
                                    $dispatcher->getRequestUri(),
                                    $m
                                )) {
                                    if (isset($m['id_product'])) {
                                        $id_product = $m['id_product'];
                                    }
                                }
                            }
                        }

                        if ($id_product) {
                            $p = new Product($id_product);
                            if (!$p->active) {
                                if (Tools::getValue('adtoken', false)) {
                                    $gt = Tools::getAdminToken(
                                        'AdminProducts'.(int)Tab::getIdFromClassName('AdminProducts').
                                        (int)Tools::getValue('id_employee')
                                    );
                                    if (Tools::getValue('adtoken') != $gt) {
                                        $id_product = 0;
                                    }
                                } else {
                                    $id_product = 0;
                                }
                            }
                        }

                        if (!$id_product) {
                            $current_url = FsAdvancedUrlTools::getCurrentUrl();
                            $tmp = explode('?', $current_url);
                            $url_part = $tmp[0];

                            if (!FsAdvancedUrlTools::endsWith($url_part, '/')
                                && !FsAdvancedUrlTools::endsWith($url_part, '.html')
                                && !FsAdvancedUrlTools::endsWith($url_part, '.htm')) {
                                $id_category_redirect = $this->getIdCategoryByRewrite($rewrite);
                                if ($id_category_redirect && $id_category_redirect != $context->shop->id_category) {
                                    $redirect_url = $url_part.'/';
                                    FsAdvancedUrlTools::redirect($redirect_url, 'HTTP/1.1 301 Moved Permanently');
                                }
                            }

                            $redirect_type = Configuration::get('FSAU_product_rule_RT');
                            if ($redirect_type == 'category') {
                                $id_category = $context->shop->id_category;
                                $rewrite_categories = Tools::getValue('fsau_rewrite_categories');
                                if ($rewrite_categories) {
                                    $rewrite_categories_array = explode('/', $rewrite_categories);
                                    foreach ($rewrite_categories_array as $rewrite_category) {
                                        $sql = 'SELECT cl.`id_category` FROM `'._DB_PREFIX_.'category_lang` cl';
                                        $sql .= ' LEFT JOIN `'._DB_PREFIX_;
                                        $sql .= 'category` c ON cl.id_category = c.id_category';
                                        $sql .= ' WHERE cl.`id_shop` = \''.pSQL($id_shop);
                                        $sql .= '\' AND cl.`link_rewrite` = \''.pSQL($rewrite_category).'\'';
                                        $sql .= ' AND c.id_parent = \''.pSQL($id_category).'\'';
                                        $id_category = Db::getInstance()->getValue(
                                            $sql.' AND cl.`id_lang` = \''.pSQL($id_lang).'\''
                                        );

                                        if (!$id_category) {
                                            $id_category = Db::getInstance()->getValue($sql);
                                        }
                                    }
                                }

                                if ($id_category != $context->shop->id_category) {
                                    $redirect_url = $context->link->getCategoryLink($id_category);
                                    FsAdvancedUrlTools::redirect($redirect_url, 'HTTP/1.1 301 Moved Permanently');
                                }

                                FsAdvancedUrlTools::redirect(
                                    $context->link->getPageLink($this->pagenotfound_name),
                                    'HTTP/1.1 404 Not Found'
                                );
                            } elseif ($redirect_type == '404') {
                                FsAdvancedUrlTools::redirect(
                                    $context->link->getPageLink($this->pagenotfound_name),
                                    'HTTP/1.1 404 Not Found'
                                );
                            } else {
                                $id_product = Db::getInstance()->getValue(
                                    'SELECT MAX(id_product) FROM `'._DB_PREFIX_.'product`'
                                );
                                $id_product++;
                            }
                        }

                        $_GET['id_product'] = $id_product;
                        unset($_GET['fsau_rewrite_product']);
                        unset($_GET['fsau_rewrite_categories']);
                        unset($_GET['fc']);
                    }
                    break;

                case 'CategoryController':
                    if ($rewrite = Tools::getValue('fsau_rewrite_category')) {
                        $id_parent = $context->shop->id_category;
                        $rewrite_categories = Tools::getValue('fsau_rewrite_categories');

                        if ($rewrite_categories) {
                            $rewrite_categories_array = explode('/', $rewrite_categories);
                            foreach ($rewrite_categories_array as $rewrite_category) {
                                $sql = 'SELECT cl.`id_category` FROM `'._DB_PREFIX_.'category_lang` cl';
                                $sql .= ' LEFT JOIN `'._DB_PREFIX_.'category` c ON cl.id_category = c.id_category';
                                $sql .= ' WHERE cl.`id_shop` = \''.pSQL($id_shop);
                                $sql .= '\' AND cl.`link_rewrite` = \''.pSQL($rewrite_category).'\'';
                                $sql .= ' AND c.id_parent = \''.pSQL($id_parent).'\'';
                                $id_parent = Db::getInstance()->getValue(
                                    $sql.' AND cl.`id_lang` = \''.pSQL($id_lang).'\''
                                );

                                if (!$id_parent) {
                                    $id_parent = Db::getInstance()->getValue($sql);
                                }
                            }
                        }

                        $sql = 'SELECT cl.`id_category` FROM `'._DB_PREFIX_.'category_lang` cl';
                        $sql .= ' LEFT JOIN `'._DB_PREFIX_.'category` c ON cl.id_category = c.id_category';
                        $sql .= ' WHERE cl.`id_shop` = \''.pSQL($id_shop);
                        $sql .= '\' AND cl.`link_rewrite` = \''.pSQL($rewrite).'\'';
                        if (Configuration::get('FSAU_ENABLE_cr_categories')) {
                            $sql .= ' AND c.id_parent = \''.pSQL($id_parent).'\'';
                        }

                        $id_category = Db::getInstance()->getValue($sql.' AND cl.`id_lang` = \''.pSQL($id_lang).'\'');
                        if (!$id_category) {
                            $id_category = Db::getInstance()->getValue($sql);
                        }

                        if (!$id_category) {
                            if (isset($routes[$id_lang]['old_category_rule'])) {
                                if (preg_match(
                                    $routes[$id_lang]['old_category_rule']['regexp'],
                                    $dispatcher->getRequestUri(),
                                    $m
                                )) {
                                    if (isset($m['id_category'])) {
                                        $id_category = $m['id_category'];
                                    }
                                }
                            }
                        }

                        if (!$id_category) {
                            if (Tools::substr($dispatcher->getRequestUri(), -1) == '/') {
                                if (isset($routes[$id_lang]['old_category_rule'])) {
                                    if (preg_match(
                                        $routes[$id_lang]['old_category_rule']['regexp'],
                                        Tools::substr($dispatcher->getRequestUri(), 0, -1),
                                        $m
                                    )) {
                                        if (isset($m['id_category'])) {
                                            $id_category = $m['id_category'];
                                        }
                                    }
                                }
                            }
                        }

                        if ($id_category) {
                            $c = new Category($id_category);
                            if (!$c->active) {
                                $id_category = 0;
                            }
                        }

                        if (!$id_category) {
                            $redirect_type = Configuration::get('FSAU_category_rule_RT');
                            if ($redirect_type == 'parent') {
                                if ($id_parent != $context->shop->id_category) {
                                    $redirect_url = $context->link->getCategoryLink($id_parent);
                                    FsAdvancedUrlTools::redirect($redirect_url, 'HTTP/1.1 301 Moved Permanently');
                                }
                            }

                            FsAdvancedUrlTools::redirect(
                                $context->link->getPageLink($this->pagenotfound_name),
                                'HTTP/1.0 404 Not Found'
                            );
                        }

                        $_GET['id_category'] = $id_category;
                        unset($_GET['fsau_rewrite_category']);
                        unset($_GET['fsau_rewrite_categories']);
                        unset($_GET['fc']);
                    }
                    break;

                case 'ManufacturerController':
                    if ($rewrite = Tools::getValue('fsau_rewrite_manufacturer')) {
                        $id_manufacturer = Db::getInstance()->getValue(
                            'SELECT m.`id_manufacturer` FROM `'._DB_PREFIX_.'manufacturer` m LEFT JOIN `'.
                            _DB_PREFIX_.'manufacturer_shop` ms ON m.id_manufacturer = ms.id_manufacturer
                            WHERE ms.`id_shop` = \''.
                            pSQL($id_shop).'\' AND REPLACE(LOWER(m.`name`), \' \', \'-\') = \''.pSQL($rewrite).'\''
                        );

                        if (!$id_manufacturer) {
                            $manufacturers = Db::getInstance()->executeS(
                                'SELECT m.`id_manufacturer`, m.`name` FROM `'._DB_PREFIX_.'manufacturer` m LEFT JOIN `'.
                                _DB_PREFIX_.'manufacturer_shop` ms ON m.id_manufacturer = ms.id_manufacturer
                                WHERE ms.`id_shop` = \''.pSQL($id_shop).'\''
                            );

                            if ($manufacturers) {
                                foreach ($manufacturers as $manufacturer) {
                                    if ($rewrite == Tools::str2url($manufacturer['name'])) {
                                        $id_manufacturer = $manufacturer['id_manufacturer'];
                                        break;
                                    }
                                }
                            }
                        }

                        if (!$id_manufacturer) {
                            FsAdvancedUrlTools::redirect(
                                $context->link->getPageLink('manufacturer'),
                                'HTTP/1.1 301 Moved Permanently'
                            );
                        }

                        $_GET['id_manufacturer'] = $id_manufacturer;
                        unset($_GET['fsau_rewrite_manufacturer']);
                        unset($_GET['fc']);
                    }
                    break;

                case 'SupplierController':
                    if ($rewrite = Tools::getValue('fsau_rewrite_supplier')) {
                        $id_supplier = Db::getInstance()->getValue(
                            'SELECT s.`id_supplier` FROM `'._DB_PREFIX_.'supplier` s LEFT JOIN `'.
                            _DB_PREFIX_.'supplier_shop` ss ON s.id_supplier = ss.id_supplier WHERE ss.`id_shop` = \''.
                            pSQL($id_shop).'\' AND REPLACE(LOWER(s.`name`), \' \', \'-\') = \''.pSQL($rewrite).'\''
                        );

                        if (!$id_supplier) {
                            $suppliers = Db::getInstance()->executeS(
                                'SELECT s.`id_supplier`, s.`name` FROM `'._DB_PREFIX_.'supplier` s LEFT JOIN `'.
                                _DB_PREFIX_.'supplier_shop` ss ON s.id_supplier = ss.id_supplier
                                WHERE ss.`id_shop` = \''.pSQL($id_shop).'\''
                            );

                            if ($suppliers) {
                                foreach ($suppliers as $supplier) {
                                    if ($rewrite == Tools::str2url($supplier['name'])) {
                                        $id_supplier = $supplier['id_supplier'];
                                        break;
                                    }
                                }
                            }
                        }

                        if (!$id_supplier) {
                            FsAdvancedUrlTools::redirect(
                                $context->link->getPageLink('supplier'),
                                'HTTP/1.1 301 Moved Permanently'
                            );
                        }

                        $_GET['id_supplier'] = $id_supplier;
                        unset($_GET['fsau_rewrite_supplier']);
                        unset($_GET['fc']);
                    }
                    break;

                case 'CmsController':
                    if ($rewrite = Tools::getValue('fsau_rewrite_cms')) {
                        $id_cms = Db::getInstance()->getValue('SELECT `id_cms` FROM `'._DB_PREFIX_.'cms_lang`
                            WHERE `link_rewrite` = \''.pSQL($rewrite).'\'');

                        if (!$id_cms) {
                            $current_url = FsAdvancedUrlTools::getCurrentUrl();
                            $tmp = explode('?', $current_url);
                            $url_part = $tmp[0];

                            if (!FsAdvancedUrlTools::endsWith($url_part, '/')
                                && !FsAdvancedUrlTools::endsWith($url_part, '.html')
                                && !FsAdvancedUrlTools::endsWith($url_part, '.htm')) {
                                $id_cms_category = $this->getIdCMSCategoryByRewrite($rewrite);
                                if ($id_cms_category) {
                                    $redirect_url = $url_part.'/';
                                    FsAdvancedUrlTools::redirect($redirect_url, 'HTTP/1.1 301 Moved Permanently');
                                }
                            }

                            FsAdvancedUrlTools::redirect(
                                $context->link->getPageLink($this->pagenotfound_name),
                                'HTTP/1.1 404 Not Found'
                            );
                        }

                        $_GET['id_cms'] = $id_cms;
                        unset($_GET['fsau_rewrite_cms']);
                        unset($_GET['fc']);
                    } elseif ($rewrite = Tools::getValue('fsau_rewrite_cms_category')) {
                        $id_cms_category = Db::getInstance()->getValue(
                            'SELECT `id_cms_category` FROM `'._DB_PREFIX_.
                            'cms_category_lang` WHERE `link_rewrite` = \''.pSQL($rewrite).'\''
                        );

                        if (!$id_cms_category) {
                            FsAdvancedUrlTools::redirect(
                                $context->link->getPageLink($this->pagenotfound_name),
                                'HTTP/1.1 404 Not Found'
                            );
                        }

                        $_GET['id_cms_category'] = $id_cms_category;
                        unset($_GET['fsau_rewrite_cms_category']);
                        unset($_GET['fc']);
                    }
                    break;

                case 'PageNotFoundController':
                    $current_url = FsAdvancedUrlTools::getCurrentUrl();
                    $not_found_url = $context->link->getPageLink($this->pagenotfound_name);

                    $is_asset = false;
                    if (preg_match('/\.(gif|jpe?g|png|css|js|ico)$/i', parse_url($current_url, PHP_URL_PATH))) {
                        $is_asset = true;
                    }

                    if (FsAdvancedUrlTools::contains($current_url, 'index.php?controller=404')) {
                        if (str_replace('https://', 'http://', $current_url) !=
                            str_replace('https://', 'http://', $not_found_url)) {
                            FsAdvancedUrlTools::redirect($not_found_url, 'HTTP/1.0 404 Not Found');
                        }
                    }

                    if ((Tools::substr($current_url, -1) !== '/') && !$is_asset) {
                        if (str_replace('https://', 'http://', $current_url) !=
                            str_replace('https://', 'http://', $not_found_url)) {
                            if (!FsAdvancedUrlTools::endsWith($current_url, '.html')) {
                                FsAdvancedUrlTools::redirect($current_url.'/', 'HTTP/1.1 301 Moved Permanently');
                            }
                        }
                    }
                    break;
            }
        }
    }

    public function hookDisplayHeader($param)
    {
        if ($this->isPsMin17() && isset($this->context->controller->php_self)
            && in_array($this->context->controller->php_self, array('product'))) {
            $product = $this->context->controller->getProduct();
            if (Validate::isLoadedObject($product)) {
                $attribute_ids = Product::getProductAttributesIds($product->id);
                if ($attribute_ids) {
                    $product_base_url = $this->context->link->getProductLink($product);
                    $product_base_url = explode('#', $product_base_url);
                    $product_base_url = $product_base_url[0];
                    $query_param = '?';
                    if (FsAdvancedUrlTools::contains($product_base_url, '?')) {
                        $query_param = '&';
                    }

                    $pa_urls = array();
                    foreach ($attribute_ids as $id_product_attribute) {
                        $anchor = $product->getAnchor(
                            $id_product_attribute['id_product_attribute'],
                            true
                        );

                        $pa_urls[$anchor] = $product_base_url.$query_param.'id_product_attribute=';
                        $pa_urls[$anchor] .= $id_product_attribute['id_product_attribute'];
                        $pa_urls[$anchor] .= $anchor;
                    }

                    $this->addJS('front.js');
                    $this->smartyAssign(array(
                        'fsau_product_urls' => $pa_urls,
                        'fsau_params_hash' => sha1($this->jsonEncode($param)),
                    ));
                    return $this->smartyFetch('front/css_js.tpl', true);
                }
            }
        }
        return '';
    }

    #################### ADMIN HOOKS ####################

    public function hookActionObjectProductAddAfter($params)
    {
        if (Configuration::get('FSAU_ENABLE_pr')) {
            $p = $params['object'];
            $id_product = $p->id;
            $context = Context::getContext();

            $shops = Shop::getShops();
            $languages = Language::getLanguages(false);
            foreach ($shops as $shop) {
                $id_shop = $shop['id_shop'];
                foreach ($languages as $language) {
                    $id_lang = $language['id_lang'];
                    $iso_lang = $language['iso_code'];
                    $link_rewrite = '';
                    if (isset($p->link_rewrite[$id_lang])) {
                        $link_rewrite = $p->link_rewrite[$id_lang];
                    }

                    $sql = 'SELECT pl.`id_product`, pl.`name` FROM `'._DB_PREFIX_.'product_lang` pl LEFT JOIN `';
                    $sql .= _DB_PREFIX_.'product` p ON pl.`id_product` = p.`id_product`';
                    $sql .= ' WHERE pl.`id_shop` = \''.pSQL($id_shop).'\' AND pl.`link_rewrite` = \'';
                    $sql .= pSQL($link_rewrite).'\' AND pl.`id_lang` = \''.pSQL($id_lang).'\'';
                    $sql .= ' AND pl.`id_product` != \''.pSQL($id_product).'\'';

                    if (Configuration::get('FSAU_ENABLE_pr_categories') ||
                        Configuration::get('FSAU_ENABLE_pr_category')) {
                        $sql .= ' AND p.id_category_default = \''.pSQL($p->id_category_default).'\'';
                    }

                    $result = Db::getInstance()->getRow($sql);
                    if ($result) {
                        $msg = $this->l('Duplicate Friendly URL').': "'.$link_rewrite.'"';
                        $msg .= ' - '.$this->l('Lang').': "'.$iso_lang.'" - '.
                            $this->l('Conflicts with').': "'.$result['name'].'"';
                        $msg .= ' - '.$this->l('ID').': "'.$result['id_product'].'"';
                        $context->controller->errors[] = $msg;
                        break 2;
                    }
                }
            }

            if ($this->isPsMin17()) {
                if (count($context->controller->errors)) {
                    header("HTTP/1.1 400 Bad Request");
                    die($this->jsonEncode(array('step5_link_rewrite' => $context->controller->errors)));
                }
            }
        }
    }

    public function hookActionObjectProductUpdateAfter($params)
    {
        $this->hookActionObjectProductAddAfter($params);
    }

    public function hookActionObjectCategoryAddAfter($params)
    {
        if (Configuration::get('FSAU_ENABLE_cr')) {
            $c = $params['object'];
            $id_category = $c->id;
            $context = Context::getContext();

            $shops = Shop::getShops();
            $languages = Language::getLanguages(false);
            foreach ($shops as $shop) {
                $id_shop = $shop['id_shop'];
                foreach ($languages as $language) {
                    $id_lang = $language['id_lang'];
                    $iso_lang = $language['iso_code'];
                    $link_rewrite = '';
                    if (isset($c->link_rewrite[$id_lang])) {
                        $link_rewrite = $c->link_rewrite[$id_lang];
                    }

                    $sql = 'SELECT cl.`id_category`, cl.`name` FROM `'._DB_PREFIX_.'category_lang` cl LEFT JOIN `';
                    $sql .= _DB_PREFIX_.'category` c ON cl.`id_category` = c.`id_category`';
                    $sql .= ' WHERE cl.`id_shop` = \''.pSQL($id_shop).'\'';
                    $sql .= ' AND cl.`link_rewrite` = \''.pSQL($link_rewrite).'\'';
                    $sql .= ' AND cl.`id_lang` = \''.pSQL($id_lang).'\'';
                    $sql .= ' AND cl.`id_category` != \''.pSQL($id_category).'\'';

                    if (Configuration::get('FSAU_ENABLE_cr_categories')) {
                        $sql .= ' AND c.id_parent = \''.pSQL($c->id_parent).'\'';
                    }

                    $result = Db::getInstance()->getRow($sql);
                    if ($result) {
                        $msg = $this->l('Duplicate Friendly URL').': "'.$link_rewrite.'"';
                        $msg .= ' - '.$this->l('Lang').': "'.$iso_lang.'" - '.
                            $this->l('Conflicts with').': "'.$result['name'].'"';
                        $msg .= ' - '.$this->l('ID').': "'.$result['id_category'].'"';
                        $context->controller->errors[] = $msg;
                        break 2;
                    }
                }
            }
        }
    }

    public function hookActionObjectCategoryUpdateAfter($params)
    {
        $this->hookActionObjectCategoryAddAfter($params);
    }

    public function hookActionObjectManufacturerAddAfter($params)
    {
        if (Configuration::get('FSAU_ENABLE_manufacturer_rule')) {
            $m = $params['object'];
            $id_manufacturer = $m->id;
            $context = Context::getContext();

            $shops = Shop::getShops();
            foreach ($shops as $shop) {
                $id_shop = $shop['id_shop'];
                $name = '';
                if (isset($m->name)) {
                    $name = $m->name;
                }

                $sql = 'SELECT m.`id_manufacturer`, m.`name` FROM `'._DB_PREFIX_;
                $sql .= 'manufacturer` m LEFT JOIN `'._DB_PREFIX_.'manufacturer_shop` ms';
                $sql .= ' ON m.`id_manufacturer` = ms.`id_manufacturer` WHERE ms.`id_shop` = \''.pSQL($id_shop).'\'';
                $sql .= ' AND m.`name` = \''.pSQL($name).'\' AND m.`id_manufacturer` != \''.pSQL($id_manufacturer).'\'';

                $result = Db::getInstance()->getRow($sql);
                if ($result) {
                    $msg = $this->l('Duplicate Name').': "'.$name.'"';
                    $msg .= ' - '.$this->l('Conflicts with').': "'.$result['name'].'"';
                    $msg .= ' - '.$this->l('ID').': "'.$result['id_manufacturer'].'"';
                    $context->controller->errors[] = $msg;
                    break;
                }
            }
        }
    }

    public function hookActionObjectManufacturerUpdateAfter($params)
    {
        $this->hookActionObjectManufacturerAddAfter($params);
    }

    public function hookActionObjectSupplierAddAfter($params)
    {
        if (Configuration::get('FSAU_ENABLE_supplier_rule')) {
            $s = $params['object'];
            $id_supplier = $s->id;
            $context = Context::getContext();

            $shops = Shop::getShops();
            foreach ($shops as $shop) {
                $id_shop = $shop['id_shop'];
                $name = '';
                if (isset($s->name)) {
                    $name = $s->name;
                }

                $sql = 'SELECT s.`id_supplier`, s.`name` FROM `'._DB_PREFIX_.'supplier` s LEFT JOIN `';
                $sql .= _DB_PREFIX_.'supplier_shop` ss ON s.`id_supplier` = ss.`id_supplier`';
                $sql .= ' WHERE ss.`id_shop` = \''.pSQL($id_shop).'\' AND s.`name` = \''.pSQL($name).'\'';
                $sql .= ' AND s.`id_supplier` != \''.pSQL($id_supplier).'\'';

                $result = Db::getInstance()->getRow($sql);
                if ($result) {
                    $msg = $this->l('Duplicate Name').': "'.$name.'"';
                    $msg .= ' - '.$this->l('Conflicts with').': "'.$result['name'].'"';
                    $msg .= ' - '.$this->l('ID').': "'.$result['id_supplier'].'"';
                    $context->controller->errors[] = $msg;
                    break;
                }
            }
        }
    }

    public function hookActionObjectSupplierUpdateAfter($params)
    {
        $this->hookActionObjectSupplierAddAfter($params);
    }

    public function hookActionObjectCMSAddAfter($params)
    {
        if (Configuration::get('FSAU_ENABLE_cms_rule')) {
            $c = $params['object'];
            $id_cms = $c->id;
            $context = Context::getContext();

            $shops = Shop::getShops();
            $languages = Language::getLanguages(false);
            foreach ($shops as $shop) {
                $id_shop = $shop['id_shop'];
                foreach ($languages as $language) {
                    $id_lang = $language['id_lang'];
                    $iso_lang = $language['iso_code'];
                    $link_rewrite = '';
                    if (isset($c->link_rewrite[$id_lang])) {
                        $link_rewrite = $c->link_rewrite[$id_lang];
                    }

                    $sql = 'SELECT cl.`id_cms`, cl.`meta_title` FROM `'._DB_PREFIX_.'cms_lang` cl LEFT JOIN `';
                    $sql .= _DB_PREFIX_.'cms` c ON cl.`id_cms` = c.`id_cms` LEFT JOIN `'._DB_PREFIX_.'cms_shop` cs';
                    $sql .= ' ON cl.`id_cms` = cs.`id_cms` WHERE cs.`id_shop` = \''.pSQL($id_shop).'\'';
                    $sql .= ' AND cl.`link_rewrite` = \''.pSQL($link_rewrite).'\'';
                    $sql .= ' AND cl.`id_lang` = \''.pSQL($id_lang).'\' AND cl.`id_cms` != \''.pSQL($id_cms).'\'';

                    $result = Db::getInstance()->getRow($sql);
                    if ($result) {
                        $msg = $this->l('Duplicate Friendly URL').': "'.$link_rewrite.'"';
                        $msg .= ' - '.$this->l('Lang').': "'.$iso_lang.'" - '.
                            $this->l('Conflicts with').': "'.$result['meta_title'].'"';
                        $msg .= ' - '.$this->l('ID').': "'.$result['id_cms'].'"';
                        $context->controller->errors[] = $msg;
                        break 2;
                    }
                }
            }
        }
    }

    public function hookActionObjectCMSUpdateAfter($params)
    {
        $this->hookActionObjectCMSAddAfter($params);
    }

    public function hookActionObjectCMSCategoryAddAfter($params)
    {
        if (Configuration::get('FSAU_ENABLE_cms_rule')) {
            $cc = $params['object'];
            $id_cms_category = $cc->id;
            $context = Context::getContext();

            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                $id_lang = $language['id_lang'];
                $iso_lang = $language['iso_code'];
                $link_rewrite = '';
                if (isset($cc->link_rewrite[$id_lang])) {
                    $link_rewrite = $cc->link_rewrite[$id_lang];
                }

                $sql = 'SELECT ccl.`id_cms_category`, ccl.`name` FROM `'._DB_PREFIX_;
                $sql .= 'cms_category_lang` ccl LEFT JOIN `'._DB_PREFIX_.'cms_category` cc';
                $sql .= ' ON ccl.`id_cms_category` = cc.`id_cms_category`';
                $sql .= ' WHERE ccl.`link_rewrite` = \''.pSQL($link_rewrite).'\'';
                $sql .= ' AND ccl.`id_lang` = \''.pSQL($id_lang).'\'';
                $sql .= ' AND ccl.`id_cms_category` != \''.pSQL($id_cms_category).'\'';

                $result = Db::getInstance()->getRow($sql);
                if ($result) {
                    $msg = $this->l('Duplicate Friendly URL').': "'.$link_rewrite.'"';
                    $msg .= ' - '.$this->l('Lang').': "'.$iso_lang.'" - '.
                        $this->l('Conflicts with').': "'.$result['name'].'"';
                    $msg .= ' - '.$this->l('ID').': "'.$result['id_cms_category'].'"';
                    $context->controller->errors[] = $msg;
                    break;
                }
            }
        }
    }

    public function hookActionObjectCMSCategoryUpdateAfter($params)
    {
        $this->hookActionObjectCMSCategoryAddAfter($params);
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        $config = array(
            'menu_button_text' => $this->l('Duplicate URLs'),
            'menu_button_url' => $this->url().'&tab_section=fsau_duplicate_tab',
            'params_hash' => sha1($this->jsonEncode($params)),
            'module_path' => $this->getPath()
        );

        $this->smartyAssign(array('fsau_admin_css_js' => $config));
        return $this->smartyFetch('admin/css_js.tpl');
    }

    #################### FUNCTIONS ####################

    protected function getCssAndJs()
    {
        $fsau_js = array(
            'generate_link_rewrite_url' => $this->getAdminAjaxUrl(
                'AdminFsadvancedurl',
                array('ajax' => '1', 'action' => 'generatelinkrewrite')
            ),
            'redirect_url' => $this->url().'&tab_section=fsau_duplicate_tab'
        );

        $this->smartyAssign(array(
            'fsau_js' => $fsau_js,
        ));

        return $this->smartyFetch('admin/css_js_config.tpl');
    }

    public function generateAvailableKeywordsMultilang($keywords, $input_id)
    {
        $js_function = 'FSAU.addKeywordToInput';
        if (count(Language::getLanguages(false)) > 1) {
            $js_function = 'FSAU.addKeywordToInputMultilang';
        }

        $this->smartyAssign(array(
            'fsau_keywords' => $keywords,
            'fsau_input_id' => $input_id,
            'fsau_js_function' => $js_function
        ));

        return $this->smartyFetch('admin/available_keywords.tpl');
    }

    protected function getProductDuplicate()
    {
        $return = array();
        $limit = ' LIMIT 5';

        $sql = 'SELECT pl.`id_product`, pl.`link_rewrite`, pl.`id_shop`, pl.`id_lang`, p.`id_category_default`,';
        $sql .= ' COUNT(pl.`id_product`) as count FROM `'._DB_PREFIX_.'product_lang` pl LEFT JOIN `';
        $sql .= _DB_PREFIX_.'product` p ON pl.`id_product` = p.`id_product`';
        $sql .= ' GROUP BY pl.`id_shop`, pl.`id_lang`, pl.`link_rewrite`';
        if (Configuration::get('FSAU_ENABLE_pr_categories') || Configuration::get('FSAU_ENABLE_pr_category')) {
            $sql .= ', p.`id_category_default`';
        }
        $sql .= ' HAVING count(pl.`link_rewrite`) > 1 ORDER BY pl.`id_shop` ASC';
        $sql .= $limit;
        $duplicates = Db::getInstance()->executeS($sql);

        if ($duplicates) {
            foreach ($duplicates as $duplicate) {
                $sql_more = 'SELECT pl.`id_product`, pl.`link_rewrite`, pl.`id_shop`, pl.`id_lang`,';
                $sql_more .= ' p.`id_category_default`, pl.`name` FROM `'._DB_PREFIX_.'product_lang` pl LEFT JOIN `';
                $sql_more .= _DB_PREFIX_.'product` p ON pl.`id_product` = p.`id_product`';
                $sql_more .= ' WHERE pl.`id_shop` = \''.pSQL($duplicate['id_shop']).'\'';
                $sql_more .= ' AND pl.`link_rewrite` = \''.pSQL($duplicate['link_rewrite']).'\'';
                $sql_more .= ' AND pl.`id_lang` = \''.pSQL($duplicate['id_lang']).'\'';
                if (Configuration::get('FSAU_ENABLE_pr_categories') || Configuration::get('FSAU_ENABLE_pr_category')) {
                    $sql_more .= ' AND p.`id_category_default` = \''.pSQL($duplicate['id_category_default']).'\'';
                }
                $sql_more .= ' GROUP BY pl.`id_product` ORDER BY pl.`id_product` ASC'.$limit;

                $more_infos = Db::getInstance()->executeS($sql_more);
                foreach ($more_infos as $more_info) {
                    $row = array();
                    $row['id'] = 'product_'.$more_info['id_product'];
                    $row['id_object'] = $more_info['id_product'];
                    $row['id_type'] = 'product';
                    $row['type'] = 'Product';
                    $row['name'] = $more_info['name'];
                    $row['link_rewrite'] = $more_info['link_rewrite'];
                    $row['id_lang'] = $more_info['id_lang'];
                    $row['lang'] = Language::getIsoById($more_info['id_lang']);
                    $row['shop'] = '';
                    $shop = Shop::getShop($more_info['id_shop']);
                    if ($shop) {
                        $row['shop'] = $shop['name'];
                    }

                    $return[] = $row;
                }
            }
        }

        return $return;
    }

    protected function getCategoryDuplicate()
    {
        $return = array();
        $limit = ' LIMIT 5';

        $sql = 'SELECT cl.`id_category`, cl.`link_rewrite`, cl.`id_shop`, cl.`id_lang`, c.`id_parent` FROM';
        $sql .= ' `'._DB_PREFIX_.'category_lang` cl LEFT JOIN `';
        $sql .= _DB_PREFIX_.'category` c ON cl.`id_category` = c.`id_category`';
        $sql .= ' GROUP BY cl.`id_shop`, cl.`id_lang`, cl.`link_rewrite`';
        if (Configuration::get('FSAU_ENABLE_cr_categories')) {
            $sql .= ', c.`id_parent`';
        }
        $sql .= ' HAVING count(cl.`link_rewrite`) > 1 ORDER BY cl.`id_shop` ASC';
        $sql .= $limit;
        $duplicates = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ($duplicates) {
            foreach ($duplicates as $duplicate) {
                $sql_more = 'SELECT cl.`id_category`, cl.`link_rewrite`, cl.`id_shop`, cl.`id_lang`,';
                $sql_more .= ' c.`id_parent`, cl.`name` FROM `'._DB_PREFIX_.'category_lang` cl LEFT JOIN `';
                $sql_more .= _DB_PREFIX_.'category` c ON cl.`id_category` = c.`id_category`';
                $sql_more .= ' WHERE cl.`id_shop` = \''.pSQL($duplicate['id_shop']).'\'';
                $sql_more .= 'AND cl.`link_rewrite` = \''.pSQL($duplicate['link_rewrite']).'\'';
                $sql_more .= ' AND cl.`id_lang` = \''.pSQL($duplicate['id_lang']).'\'';
                if (Configuration::get('FSAU_ENABLE_cr_categories')) {
                    $sql_more .= ' AND c.`id_parent` = \''.pSQL($duplicate['id_parent']).'\'';
                }
                $sql_more .= ' GROUP BY cl.`id_category`'.$limit;

                $more_infos = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql_more);
                foreach ($more_infos as $more_info) {
                    $row = array();
                    $row['id'] = 'category_'.$more_info['id_category'];
                    $row['id_object'] = $more_info['id_category'];
                    $row['id_type'] = 'category';
                    $row['type'] = 'Category';
                    $row['name'] = $more_info['name'];
                    $row['link_rewrite'] = $more_info['link_rewrite'];
                    $row['id_lang'] = $more_info['id_lang'];
                    $row['lang'] = Language::getIsoById($more_info['id_lang']);
                    $row['shop'] = '';
                    $shop = Shop::getShop($more_info['id_shop']);
                    if ($shop) {
                        $row['shop'] = $shop['name'];
                    }

                    $return[] = $row;
                }
            }
        }

        return $return;
    }

    protected function getManufacturerDuplicate()
    {
        $return = array();
        $limit = ' LIMIT 5';

        $sql = 'SELECT m.`id_manufacturer`, ms.`id_shop`, m.`name` FROM `'._DB_PREFIX_.'manufacturer` m LEFT JOIN';
        $sql .= ' `'._DB_PREFIX_.'manufacturer_shop` ms ON m.`id_manufacturer` = ms.`id_manufacturer`';
        $sql .= ' GROUP BY ms.`id_shop`, m.`name`';
        $sql .= ' HAVING count(m.`name`) > 1 ORDER BY ms.`id_shop` ASC';
        $sql .= $limit;
        $duplicates = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if ($duplicates) {
            foreach ($duplicates as $duplicate) {
                $sql_more = 'SELECT m.`id_manufacturer`, ms.`id_shop`, m.`name` FROM `';
                $sql_more .= _DB_PREFIX_.'manufacturer` m LEFT JOIN';
                $sql_more .= ' `'._DB_PREFIX_.'manufacturer_shop` ms ON m.`id_manufacturer` = ms.`id_manufacturer`';
                $sql_more .= ' WHERE ms.`id_shop` = \''.pSQL($duplicate['id_shop']).'\'';
                $sql_more .= ' AND m.`name` = \''.pSQL($duplicate['name']).'\'';
                $sql_more .= ' GROUP BY m.`id_manufacturer`'.$limit;

                $more_infos = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql_more);
                foreach ($more_infos as $more_info) {
                    $row = array();
                    $row['id'] = 'manufacturer_'.$more_info['id_manufacturer'];
                    $row['id_object'] = $more_info['id_manufacturer'];
                    $row['id_type'] = 'manufacturer';
                    $row['type'] = 'Manufacturer';
                    $row['name'] = $more_info['name'];
                    $row['shop'] = '';
                    $shop = Shop::getShop($more_info['id_shop']);
                    if ($shop) {
                        $row['shop'] = $shop['name'];
                    }

                    $return[] = $row;
                }
            }
        }

        return $return;
    }

    protected function getSupplierDuplicate()
    {
        $return = array();
        $limit = ' LIMIT 5';

        $sql = 'SELECT s.`id_supplier`, ss.`id_shop`, s.`name` FROM `'._DB_PREFIX_.'supplier` s LEFT JOIN';
        $sql .= ' `'._DB_PREFIX_.'supplier_shop` ss ON s.`id_supplier` = ss.`id_supplier`';
        $sql .= ' GROUP BY ss.`id_shop`, s.`name`';
        $sql .= ' HAVING count(s.`name`) > 1 ORDER BY ss.`id_shop` ASC';
        $sql .= $limit;
        $duplicates = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if ($duplicates) {
            foreach ($duplicates as $duplicate) {
                $sql_more = 'SELECT s.`id_supplier`, ss.`id_shop`, s.`name` FROM `'._DB_PREFIX_.'supplier` s LEFT JOIN';
                $sql_more .= ' `'._DB_PREFIX_.'supplier_shop` ss ON s.`id_supplier` = ss.`id_supplier`';
                $sql_more .= ' WHERE ss.`id_shop` = \''.pSQL($duplicate['id_shop']).'\'';
                $sql_more .= ' AND s.`name` = \''.pSQL($duplicate['name']).'\'';
                $sql_more .= ' GROUP BY s.`id_supplier`'.$limit;

                $more_infos = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql_more);
                foreach ($more_infos as $more_info) {
                    $row = array();
                    $row['id'] = 'supplier_'.$more_info['id_supplier'];
                    $row['id_object'] = $more_info['id_supplier'];
                    $row['id_type'] = 'supplier';
                    $row['type'] = 'Supplier';
                    $row['name'] = $more_info['name'];
                    $row['shop'] = '';
                    $shop = Shop::getShop($more_info['id_shop']);
                    if ($shop) {
                        $row['shop'] = $shop['name'];
                    }

                    $return[] = $row;
                }
            }
        }

        return $return;
    }

    protected function getCMSDuplicate()
    {
        $return = array();
        $limit = ' LIMIT 5';

        $sql = 'SELECT cl.`id_cms`, cl.`link_rewrite`, cs.`id_shop`, cl.`id_lang` FROM `'._DB_PREFIX_.'cms_lang` cl';
        $sql .= ' LEFT JOIN `'._DB_PREFIX_.'cms` c ON cl.`id_cms` = c.`id_cms`';
        $sql .= ' LEFT JOIN `'._DB_PREFIX_.'cms_shop` cs ON cl.`id_cms` = cs.`id_cms`';
        $sql .= ' GROUP BY cs.`id_shop`, cl.`id_lang`, cl.`link_rewrite`';
        $sql .= ' HAVING count(cl.`link_rewrite`) > 1 ORDER BY cs.`id_shop` ASC';
        $sql .= $limit;
        $duplicates = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ($duplicates) {
            foreach ($duplicates as $duplicate) {
                $sql_more = 'SELECT cl.`id_cms`, cl.`link_rewrite`, cs.`id_shop`, cl.`id_lang`, cl.`meta_title` FROM `';
                $sql_more .= _DB_PREFIX_.'cms_lang` cl';
                $sql_more .= ' LEFT JOIN `'._DB_PREFIX_.'cms` c ON cl.`id_cms` = c.`id_cms`';
                $sql_more .= ' LEFT JOIN `'._DB_PREFIX_.'cms_shop` cs ON cl.`id_cms` = cs.`id_cms`';
                $sql_more .= ' WHERE cs.`id_shop` = \''.pSQL($duplicate['id_shop']).'\'';
                $sql_more .= ' AND cl.`link_rewrite` = \''.pSQL($duplicate['link_rewrite']).'\'';
                $sql_more .= ' AND cl.`id_lang` = \''.pSQL($duplicate['id_lang']).'\'';
                $sql_more .= ' GROUP BY cl.`id_cms`'.$limit;

                $more_infos = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql_more);
                foreach ($more_infos as $more_info) {
                    $row = array();
                    $row['id'] = 'cms_'.$more_info['id_cms'];
                    $row['id_object'] = $more_info['id_cms'];
                    $row['id_type'] = 'cms';
                    $row['type'] = 'CMS';
                    $row['name'] = $more_info['meta_title'];
                    $row['link_rewrite'] = $more_info['link_rewrite'];
                    $row['id_lang'] = $more_info['id_lang'];
                    $row['lang'] = Language::getIsoById($more_info['id_lang']);
                    $row['shop'] = '';
                    $shop = Shop::getShop($more_info['id_shop']);
                    if ($shop) {
                        $row['shop'] = $shop['name'];
                    }

                    $return[] = $row;
                }
            }
        }

        return $return;
    }

    protected function getCMSCategoryDuplicate()
    {
        $return = array();
        $limit = ' LIMIT 5';

        $sql = 'SELECT ccl.`id_cms_category`, ccl.`link_rewrite`, ccl.`id_lang` FROM `';
        $sql .= _DB_PREFIX_.'cms_category_lang` ccl';
        $sql .= ' LEFT JOIN `'._DB_PREFIX_.'cms_category` cc ON ccl.`id_cms_category` = cc.`id_cms_category`';
        $sql .= ' GROUP BY ccl.`id_lang`, ccl.`link_rewrite`';
        $sql .= ' HAVING count(ccl.`link_rewrite`) > 1';
        $sql .= $limit;
        $duplicates = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ($duplicates) {
            foreach ($duplicates as $duplicate) {
                $sql_more = 'SELECT ccl.`id_cms_category`, ccl.`link_rewrite`, ccl.`id_lang`, ccl.`name` FROM `';
                $sql_more .= _DB_PREFIX_.'cms_category_lang` ccl LEFT JOIN `'._DB_PREFIX_.'cms_category`';
                $sql_more .= ' cc ON ccl.`id_cms_category` = cc.`id_cms_category`';
                $sql_more .= ' WHERE ccl.`link_rewrite` = \''.pSQL($duplicate['link_rewrite']).'\'';
                $sql_more .= ' AND ccl.`id_lang` = \''.pSQL($duplicate['id_lang']).'\'';
                $sql_more .= ' GROUP BY ccl.`id_cms_category`'.$limit;

                $more_infos = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql_more);
                foreach ($more_infos as $more_info) {
                    $row = array();
                    $row['id'] = 'cmscategory_'.$more_info['id_cms_category'];
                    $row['id_object'] = $more_info['id_cms_category'];
                    $row['id_type'] = 'cms_category';
                    $row['type'] = 'CMS Category';
                    $row['name'] = $more_info['name'];
                    $row['link_rewrite'] = $more_info['link_rewrite'];
                    $row['id_lang'] = $more_info['id_lang'];
                    $row['lang'] = Language::getIsoById($more_info['id_lang']);

                    $return[] = $row;
                }
            }
        }

        return $return;
    }

    protected function getRoutesForSelector()
    {
        $options = array(
            array('id' => '', 'name' => $this->l('- no move route -'))
        );

        $dispatcher = Dispatcher::getInstance();
        if (!is_callable(array($dispatcher, 'getRoutes')) || !is_callable(array($dispatcher, 'getRequestUri'))) {
            return false;
        }
        $routes = $dispatcher->getRoutes();

        if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
            $id_shop = (int)$this->context->shop->id;
            $routes = $routes[$id_shop];
        }

        $id_lang = (int)$this->context->language->id;
        $routes = $routes[$id_lang];

        foreach (array_keys($routes) as $route_name) {
            $options[] = array(
                'id' => $route_name,
                'name' => $route_name
            );
        }

        return $options;
    }

    protected function getIdCategoryByRewrite($rewrite)
    {
        $sql = 'SELECT cl.`id_category` FROM `'._DB_PREFIX_.'category_lang` cl';
        $sql .= ' LEFT JOIN `'._DB_PREFIX_.'category` c ON cl.id_category = c.id_category';
        $sql .= ' WHERE cl.`link_rewrite` = \''.pSQL($rewrite).'\'';
        return Db::getInstance()->getValue($sql);
    }

    protected function getIdCMSCategoryByRewrite($rewrite)
    {
        $sql = 'SELECT ccl.`id_cms_category` FROM `'._DB_PREFIX_.'cms_category_lang` ccl';
        $sql .= ' LEFT JOIN `'._DB_PREFIX_.'cms_category` cc ON ccl.id_cms_category = cc.id_cms_category';
        $sql .= ' WHERE ccl.`link_rewrite` = \''.pSQL($rewrite).'\'';
        return Db::getInstance()->getValue($sql);
    }

    #################### OVERRIDE FUNCTIONS ####################

    public function dispatcherLoadRoutes($routes)
    {
        $context = Context::getContext();

        if (!in_array($context->language->id, $languages = Language::getLanguages())) {
            $languages[] = array('id_lang' => (int)$context->language->id);
        }

        if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
            $id_shop = (int)$context->shop->id;
            foreach ($languages as $lang) {
                $tmp = array();
                if ($route_name = Configuration::get('FSAU_ROUTE_FRONT', '')) {
                    $tmp[$route_name] = $routes[$id_shop][$lang['id_lang']][$route_name];
                    unset($routes[$id_shop][$lang['id_lang']][$route_name]);
                }

                foreach ($routes[$id_shop][$lang['id_lang']] as $route_name => $route) {
                    if (!FsAdvancedUrlTools::startsWith($route['rule'], '{')) {
                        $tmp[$route_name] = $route;
                        unset($routes[$id_shop][$lang['id_lang']][$route_name]);
                    }
                }

                $routes[$id_shop][$lang['id_lang']] = $tmp + $routes[$id_shop][$lang['id_lang']];

                foreach ($routes[$id_shop][$lang['id_lang']] as $route_key => $route) {
                    if (trim($route['rule']) == '{rewrite}') {
                        $route['regexp'] = str_replace(']*)', ']+)', $route['regexp']);
                        unset($routes[$id_shop][$lang['id_lang']][$route_key]);
                        $routes[$id_shop][$lang['id_lang']][$route_key] = $route;
                    }
                }

                if (Configuration::get('FSAU_MODULE_ROUTE_END', 0)) {
                    $route = $routes[$id_shop][$lang['id_lang']]['module'];
                    unset($routes[$id_shop][$lang['id_lang']]['module']);
                    $routes[$id_shop][$lang['id_lang']]['module'] = $route;
                }
            }
        } else {
            foreach ($languages as $lang) {
                $tmp = array();
                if ($route_name = Configuration::get('FSAU_ROUTE_FRONT', '')) {
                    $route = $routes[$lang['id_lang']][$route_name];
                    unset($routes[$lang['id_lang']][$route_name]);
                    $routes[$lang['id_lang']][$route_name] = $route;
                }

                foreach ($routes[$lang['id_lang']] as $route_name => $route) {
                    if (!FsAdvancedUrlTools::startsWith($route['rule'], '{')) {
                        $tmp[$route_name] = $route;
                        unset($routes[$lang['id_lang']][$route_name]);
                    }
                }

                $routes[$lang['id_lang']] = $tmp + $routes[$lang['id_lang']];

                foreach ($routes[$lang['id_lang']] as $route_key => $route) {
                    if (trim($route['rule']) == '{rewrite}') {
                        $route['regexp'] = str_replace(']*)', ']+)', $route['regexp']);
                        unset($routes[$lang['id_lang']][$route_key]);
                        $routes[$lang['id_lang']][$route_key] = $route;
                    }
                }

                if (Configuration::get('FSAU_MODULE_ROUTE_END', 0)) {
                    $route = $routes[$lang['id_lang']]['module'];
                    unset($routes[$lang['id_lang']]['module']);
                    $routes[$lang['id_lang']]['module'] = $route;
                }
            }
        }

        return $routes;
    }

    public function isRemoveAnchor($product, $ipa)
    {
        if ((bool)Configuration::get('FSAU_ENABLE_pr_anchor_remove')) {
            if (!is_object($product)) {
                if (is_array($product) && isset($product['id_product'])) {
                    $id_product = $product['id_product'];
                } elseif ((int)$product) {
                    $id_product = $product;
                } else {
                    throw new PrestaShopException('Invalid product vars');
                }
            } else {
                $id_product = $product->id;
            }

            $ipa_default = Product::getDefaultAttribute($id_product);
            if ((int)$ipa_default == (int)$ipa) {
                return true;
            }
        }

        return false;
    }
}
