<?php
/**
 *  2016 ModuleFactory.co
 *
 *  @author    ModuleFactory.co <info@modulefactory.co>
 *  @copyright 2016 ModuleFactory.co
 *  @license   ModuleFactory.co Commercial License
 */

class Dispatcher extends DispatcherCore
{
    public function getRoutes()
    {
        return $this->routes;
    }

    public function getRequestUri()
    {
        return $this->request_uri;
    }

    protected function loadRoutes($id_shop = null)
    {
        parent::loadRoutes($id_shop);
        if (Module::isEnabled('fsadvancedurl')) {
            $fsadvancedurl = Module::getInstanceByName('fsadvancedurl');
            $this->routes = $fsadvancedurl->dispatcherLoadRoutes($this->routes);
        }
    }
}
