{**
 *  2016 ModuleFactory.co
 *
 *  @author    ModuleFactory.co <info@modulefactory.co>
 *  @copyright 2016 ModuleFactory.co
 *  @license   ModuleFactory.co Commercial License
 *}

<div id="fsau_help" class="panel">
    {if $is_ps_15}
    <fieldset>
        <legend>{l s='Help' mod='fsadvancedurl'}</legend>
        {else}
        <div class="panel-heading">
            <span>{l s='Help' mod='fsadvancedurl'}</span>
        </div>
        {/if}
        <div class="form-wrapper clearfix">
            Thank you for using our module. For the best user experience we provide some examples and information.
            If you need more help, please feel free to contact us.
            <br />
            <h2>Getting Started</h2>
            <p>
                After installation you may delete all your shop's cache in Advanced Parameters -> <a href="{$fsau_performance_url|escape:'html':'UTF-8'|fsauCorrectTheMess}">Performance</a> -> Clear Cache<br />
                <br />
                When you have installed the module, all the URL types are enabled for advanced url.<br />
                On the top of the module configuration page you can see the duplicated contents with a direct edit button.
            </p>
            <h2>Configure the module</h2>
            <h4>Product URL Settings</h4>
            <p>
                In the Advanced URLs To Products tab you can Enabled/Disable advanced URL feature, Enable/Disable parent categories in the URL and also select redirect type if no product found.<br />
                <br />
                To change the schema of the URL navigate to Preferences -> <a href="{$fsau_seo_url|escape:'html':'UTF-8'|fsauCorrectTheMess}">SEO & URLs</a> -> Schema Of URLs panel and set the Route to products.
            </p>
            <br />
            <h4>Category URL Settings</h4>
            <p>
                In the Advanced URLs To Categories tab you can Enabled/Disable advanced URL feature, Enable/Disable parent categories in the URL and also select redirect type if no category found.<br />
                <br />
                To change the schema of the URL navigate to Preferences -> <a href="{$fsau_seo_url|escape:'html':'UTF-8'|fsauCorrectTheMess}">SEO & URLs</a> -> Schema Of URLs panel and set the Route to category.
            </p>
            <br />
            <h4>Manufacturer URL Settings</h4>
            <p>
                In the Advanced URLs To Manufacturers tab you can Enabled/Disable advanced URL feature.<br />
                <br />
                To change the schema of the URL navigate to Preferences -> <a href="{$fsau_seo_url|escape:'html':'UTF-8'|fsauCorrectTheMess}">SEO & URLs</a> -> Schema Of URLs panel and set the Route to manufacturer.
            </p>
            <br />
            <h4>Supplier URL Settings</h4>
            <p>
                In the Advanced URLs To Suppliers tab you can Enabled/Disable advanced URL feature.<br />
                <br />
                To change the schema of the URL navigate to Preferences -> <a href="{$fsau_seo_url|escape:'html':'UTF-8'|fsauCorrectTheMess}">SEO & URLs</a> -> Schema Of URLs panel and set the Route to supplier.
            </p>
            <br />
            <h4>CMS & CMS Category URL Settings</h4>
            <p>
                In the Advanced URLs To Suppliers tab you can Enabled/Disable advanced URL feature.<br />
                <br />
                To change the schema of the URL navigate to Preferences -> <a href="{$fsau_seo_url|escape:'html':'UTF-8'|fsauCorrectTheMess}">SEO & URLs</a> -> Schema Of URLs panel and set the Route to CMS page and Route to CMS category.
            </p>
            <br />
            <h2>Duplicate URLs</h2>
            <p>
                Detect duplicated URLs when saving a Product, Category, Manufacturer, Supplier, CMS, CMS Category. Also offers a unified list with all of duplicated URLs if no records found in that list, your shop is ok.
            </p>
            <br />
            <h2>Recommendation</h2>
            <p>
                When you assign a category to a product, we recommend to assign to the default category the lowest level of the product categories. This will generate the best product URLs when you use parent categories in product URLs. (Recommended to use!)
            </p>
            <br />
            <br />
            <a id="fsau-developed-by" href="https://addons.prestashop.com/en/116_modulefactory" target="_blank">
                <img src="{$module_base_url|escape:'html':'UTF-8'}views/img/help_footer_1280x170.jpg">
            </a>
        </div>
        {if $is_ps_15}
    </fieldset>
    {/if}
</div>
