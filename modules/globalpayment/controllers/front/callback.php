<?php

class GlobalPaymentCallbackModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $enable_capture_cron = Configuration::get('GP_CAPTURE_CRON');
        
        if($enable_capture_cron == "1") 
        {
            $actionurl = Configuration::get('GP_ACTION_URL').'/vpcdps';
            $accCode = Configuration::get('GP_ACC_CODE');
            $merchantId = Configuration::get('GP_MERCHANT_ID');
            $capture_cron_days = Configuration::get('GP_CAPTURE_DAY_AFTER');
            $eTime = date('Y-m-d', strtotime('-'.((int)$capture_cron_days-1).' day'));
            $eTime2 = date('Y-m-d', strtotime('-36 day'));

            $sql = 'SELECT gpId, id_cart, payment_status, transaction_no, receipt_no, id_order, total_paid '.
                    'FROM `'. _DB_PREFIX_ .'globalpayment_cart` '.
                    'WHERE id_order IS NOT NULL AND payment_status="0" AND (synced IS NULL OR synced!="0") AND'.
                    'date_add>="'.$eTime2.'" AND date_add<"'.$eTime.'"';
            $paymentinfos = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        
            if($paymentinfos != null) {
                foreach ($paymentinfos as $pinfo) {
                    $post = array();
                    $post['vpc_Version'] = '1';
                    $post['vpc_Command'] = 'capture';
                    $post['vpc_AccessCode'] = $accCode;
                    $post['vpc_MerchTxnRef'] = $pinfo['gpId'];
                    $post['vpc_Merchant'] = $merchantId;
                    $post['vpc_TransNo'] = $pinfo['transaction_no'];
                    $post['vpc_Amount'] = (string)floor(($pinfo['total_paid'] * 100));
                    $post['vpc_User'] = Configuration::get('GP_VPC_USER');
                    $post['vpc_Password'] = Configuration::get('GP_VPC_PASS');
   
                    $qs = $this->curlPost($actionurl, $post);
                    $result = explode("&", $qs);
                
                    $syncedCode = "";
                    $syncMsg = "";
                    foreach ($result as $k => $str) {
                        $tmp = explode('=', $str);
                        if ($tmp[0] == 'vpc_TxnResponseCode') {
                            $syncedCode = $tmp[1];
                        
                            if ($tmp[1] == '0') {
                                print_r("Success:");
                                print_r($str);   
                            } else {
                                print_r("Failed:");
                                print_r($result);                            
                            }
                        }
                        else if ($tmp[0] == 'vpc_Message') {
                            $syncMsg = $tmp[1];
                        }          
                    }
                
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'globalpayment_cart` '.
                            'SET synced="'.$syncedCode.'",'.
                                'syncmsg="'.pSQL(urldecode($syncMsg)).'",'.
                                'syncdata="'.pSQL($qs).'",'.
                                'date_upd=NOW() '.
                                'WHERE gpId='.$pinfo['gpId']);
                }
            }
        }
        exit;
    }

    private function curlPost($url, $data){
	      $ch = curl_init();
	      curl_setopt($ch, CURLOPT_URL, $url);
	      curl_setopt($ch, CURLOPT_POST, 1);
	      curl_setopt($ch, CURLOPT_FAILONERROR, false);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	      $response = curl_exec($ch);
	      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	      curl_close($ch);
     
	      return $response;
    }
    /*
    private function getCartIdByC2P2Id($p2c2pId) {
        $sql = 'SELECT id_cart FROM `'. _DB_PREFIX_ .'p2c2p_cart` WHERE p2c2pId = '. $p2c2pId;
        $cartId = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        
        return $cartId;
    }
    
    private function calcChildCartTotal($id_cart) {
        //// check is the cart splitted into other carts
        $sql2 = 'SELECT id_cart_child FROM `'. _DB_PREFIX_ .'cart_split` WHERE id_cart_parent = '. $id_cart;
        $total = 0;
        
        if($results =  Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql2)) {
            foreach ($results as $small_cart_id) {
                $cart_c = new Cart((int)$small_cart_id["id_cart_child"]);
                $total += (float)$cart_c->getOrderTotal(true, Cart::BOTH);
            }
        }
       
        return $total;
    }
    
    private function updateOrderValidation($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order,current_state FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        //// check is the cart splitted into other carts
        $sql2 = 'SELECT id_cart_child FROM `'. _DB_PREFIX_ .'cart_split` WHERE id_cart_parent = '. $id_cart;
        
        if ($row && $row["current_state"] != 15){
            $new_history = new OrderHistory();
		    $new_history->id_order = (int)$row["id_order"];
            $new_history->id_employee = 1;
		    $new_history->changeIdOrderState(15, $row["id_order"]);
            $new_history->addWithemail(true, false);
        }
        else if($results =  Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql2)) {
            foreach ($results as $small_cart_id) {
                $sql3 = 'SELECT id_order, current_state FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. (int)$small_cart_id["id_cart_child"];
                $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql3);

                if($row && $row["current_state"] != 15) {
                    $new_history = new OrderHistory();
		            $new_history->id_order = (int)$row["id_order"];
                    $new_history->id_employee = 1;
		            $new_history->changeIdOrderState(15, $row["id_order"]);
                    $new_history->addWithemail(true, false);
                }
            }
            
            return true;
        }
        else {
            return false;
        }          
    }
    
    private function processOrder($id_cart, $amount, $tranID, $id_order_state, $p2c2pId) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        //// check is the cart splitted into other carts
        $sql2 = 'SELECT id_cart_child FROM `'. _DB_PREFIX_ .'cart_split` WHERE id_cart_parent = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
        if ($row){
            return $this->module->updateValidatedOrder(
                $id_cart, $row["id_order"], $id_order_state, $amount, $this->module->displayName, 
                '2C2P Transaction ID: ' . $tranID. ' (2C2P Order: '. $p2c2pId. ')', NULL, false);
        }
        else if($results =  Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql2)) {
            foreach ($results as $small_cart_id) {
                $sql3 = 'SELECT id_order, total_paid FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. (int)$small_cart_id["id_cart_child"];
                $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql3);

                $this->module->updateValidatedOrder(
                    $small_cart_id["id_cart_child"], $row["id_order"], $id_order_state, $row['total_paid'], $this->module->displayName, 
                    '2C2P Transaction ID: ' . $tranID. ' (2C2P Order: '. $p2c2pId. ')', NULL, false);
            }
            
            return true;
        }
        else {
            return false;
        }
        
    }
    
    private function SaveLog ($data, $orderId)
    {
        $postdata = json_encode($data);
        $postdata = str_replace('"', '\"', $postdata);
        
        $sql = 'INSERT INTO `'._DB_PREFIX_.'payment_callback_log` (`module`, `file`,`orderId`, `postdata`,`createdTS`) '.
                'VALUES ("2c2p", "notify2C2P","'.$orderId.'", "'.$postdata.'", NOW())';
        $return = Db::getInstance()->execute($sql);
        return Db::getInstance()->Insert_ID();
    }
    
    private function UpdateLog($logId, $logVal) {
        $sql = 'UPDATE `'._DB_PREFIX_.'payment_callback_log` SET result="'.$logVal.'" WHERE id='.(int)$logId;
        $return = Db::getInstance()->execute($sql);
    }
    
    private function CheckDuplicateLog($logId)
    {
        $tempSQL = "SELECT COUNT(1) AS logCount FROM `". _DB_PREFIX_ ."payment_callback_log` A ".
                    "INNER JOIN `". _DB_PREFIX_ ."payment_callback_log` B ON ".
                        "substring(A.postdata, POSITION('\"payment_status\":' IN A.postdata), 22) = substring(B.postdata, POSITION('\"payment_status\":' IN B.postdata), 22) AND ".
                        "substring(A.postdata, POSITION('\"transaction_ref\":' IN A.postdata), 40) = substring(B.postdata, POSITION('\"transaction_ref\":' IN B.postdata), 40) AND ".
                        "A.orderId=B.orderId AND A.id < B.id ".
                    "WHERE B.id=". (int)$logId;
        $stateCount = Db::getInstance()->getRow($tempSQL);
      
        if($stateCount["logCount"] > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private function InsertSubscribePlan($customerId, $planId, $amount, $transactionId, $appcode, $status, $orderIdPrefix, $recurring_unique_id, $logId) 
    {
        if(!$this->CheckDuplicateLog($logId)) 
        {
            if($status == "000") {
                $active = 2;
            }
            else if($status == "001") {
                $active = 1;
            }
            else if($status == "002") {
                $active = 0;
            }
            else {
                $active = 3;
            }
        
            $sql = "SELECT id_payment,active FROM " . _DB_PREFIX_ . "bundle_payment ".
                    "WHERE planId='".$planId."' AND transactionId='".$transactionId."'";
            $result = Db::getInstance()->getRow($sql);
        
            $t_length = strpos($planId, "R");
            if($t_length !== false) {
                $c2p2_bundle_id = substr($planId, 0, $t_length);
            }
            $c2p2_bundle_id = str_replace($orderIdPrefix, "", $c2p2_bundle_id);
        
            $bundle_sql = 'SELECT id_customer,id_subscribe FROM `'. _DB_PREFIX_ .'p2c2p_bundle` WHERE p2c2pId = '. $c2p2_bundle_id;
            $bundle_result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($bundle_sql);
        
            $subscribeId = 0;
            if ($bundle_result != null) {
                if($bundle_result["id_customer"] > 0) {
                    $customerId = $bundle_result["id_customer"];
                }
                if($bundle_result["id_subscribe"] > 0) {
                    $subscribeId = $bundle_result["id_subscribe"];
                }
            }
        
            Db::getInstance()->Execute("UPDATE " . _DB_PREFIX_ . "bundle_subscribe SET bundle_order_id='".$transactionId."',recurring_unique_id=".$recurring_unique_id.",last_charge_date=NOW(),current_recurring=1 WHERE id_subscribe=".(int)$subscribeId);
        
            if($result == null) 
            {
                $amount = $amount / 100;
                $sql = "INSERT INTO " . _DB_PREFIX_ . "bundle_payment" .
                        "(`id_subscribe`,`createdTS`,`updatedTS`,`customerId`,
                        `planId`,`amount`,`transactionId`,`appcode`,`active`)" .
                       "VALUES (".
                        $subscribeId.",NOW(),NOW(),".(int)$customerId.",".
                        "'".$planId."',".$amount.",'".$planId."','".$appcode."',".$active.")";
                $return = Db::getInstance()->Execute($sql);
                $id_payment = Db::getInstance()->Insert_ID();
            
                if($active == 2) {
                    Db::getInstance()->Execute("UPDATE " . _DB_PREFIX_ . "bundle_subscribe SET planId='".$planId."',active=2,date_upd=NOW() WHERE id_subscribe=".(int)$subscribeId);
                
                    //// create order
                    if (!class_exists('bundleproduct')) {
                        // Put class TestClass here
                        include(dirname(__FILE__).'/../../../bundleproduct/bundleproduct.php');
                    }

                    $bp = new bundleproduct();
                    $bp->generateBundleOrder($subscribeId, $id_payment, Configuration::get('PS_OS_PAYMENT'));
                    $this->UpdateLog($logId, "Create Bundle||Bundle Create Successful");
                }
                else if($active == 3) {
                    Db::getInstance()->Execute("UPDATE " . _DB_PREFIX_ . "bundle_subscribe SET active=8,date_upd=NOW() WHERE id_subscribe=".(int)$subscribeId);
                    $this->UpdateLog($logId, "Bundle Payment Failed");     
                }
                else {
                    //Db::getInstance()->Execute("UPDATE " . _DB_PREFIX_ . "bundle_subscribe SET active=2,updatedTS=NOW() WHERE id_subscribe=".(int)$subscribeId." AND active=15");
                    $this->UpdateLog($logId, "Bundle Payment Error");     
                }
            }
            else if($result["active"] != 2 && $active != 1) 
            {
                $sql = "UPDATE " . _DB_PREFIX_ . "bundle_payment SET active=".$active.",updatedTS=NOW() WHERE id_payment=".(int)$result["id_payment"];
                $return = Db::getInstance()->Execute($sql);
                $this->UpdateLog($logId, "Bundle Payment Invalid");
            }
        }    
        
        echo "CBTOKEN:MPSTATOK";
        exit;
    }
    */
}