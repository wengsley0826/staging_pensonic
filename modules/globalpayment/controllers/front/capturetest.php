
<?php

class GlobalPaymentCaptureTestModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
    //exit;
        $autoCapture = Configuration::get('GP_CAPTURE_CRON');
        $sql1 = 'SELECT A.id_order '.
                        'FROM `'. _DB_PREFIX_ .'globalpayment_cart` A '.
                        'INNER JOIN `'. _DB_PREFIX_ .'orders` B ON A.id_order=B.id_order '.
                        'WHERE A.id_order IS NOT NULL '.
                        ' AND B.current_state NOT IN (6) AND A.payment_status="0" AND (A.synced IS NULL OR A.synced != "0")';
        $orderList = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql1);
 print_r($orderList);     
 print_r("<br /><br />");
 
        foreach($orderList as $o) 
        {
            if($autoCapture == "1") {
                $id_order = $o['id_order'];
                $order = new Order($id_order);
                $new_status = new OrderState($order->current_state);
                $syncMsg = null;
                $result = true;
                $orderRef = "";

                if($new_status->id == Configuration::get("GP_CAPTURE_STATUS")) 
                {
                    $sql = 'SELECT A.gpId, A.id_cart, A.payment_status, A.transaction_no, A.receipt_no, A.id_order, A.total_paid,B.reference '.
                            'FROM `'. _DB_PREFIX_ .'globalpayment_cart` A '.
                            'INNER JOIN `'. _DB_PREFIX_ .'orders` B ON A.id_order=B.id_order '.
                            'WHERE A.id_order='.(int)$id_order.' AND A.payment_status="0" AND (A.synced IS NULL OR A.synced != "0")';
                    $pinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
    print_r($pinfo);
    print_r("<br />");
                    if($pinfo != null) {
                        $actionurl = Configuration::get('GP_ACTION_URL').'/vpcdps';
                        $accCode = Configuration::get('GP_ACC_CODE');
                        $merchantId = Configuration::get('GP_MERCHANT_ID');
            
                        $post = array();
                        $post['vpc_Version'] = '1';
                        $post['vpc_Command'] = 'capture';
                        $post['vpc_AccessCode'] = $accCode;
                        $post['vpc_MerchTxnRef'] = $pinfo['gpId'];
                        $post['vpc_Merchant'] = $merchantId;
                        $post['vpc_TransNo'] = $pinfo['transaction_no'];
                        $post['vpc_Amount'] = (string)floor(($pinfo['total_paid'] * 100));
                        $post['vpc_User'] = Configuration::get('GP_VPC_USER');
                        $post['vpc_Password'] = Configuration::get('GP_VPC_PASS');

                        $qs = $this->curlPost($actionurl, $post);
                        $result = explode("&", $qs);
                
                        $syncedCode = "";
    print_r("<br /><br />");
    print_r($result);
                        foreach ($result as $k => $str) {
                            $tmp = explode('=', $str);
                            if ($tmp[0] == 'vpc_TxnResponseCode') {
                                $syncedCode = $tmp[1];
                        
                                if ($tmp[1] == '0') {
                                    $result = true;
                                } else {
                                    $result = false;
                                }
                            }
                            else if ($tmp[0] == 'vpc_Message') {
                                $result = false;
                                $syncMsg = $tmp[1];
                                $orderRef = $pinfo['id_order'];
                            }          
                        }
                
                        Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                            'UPDATE `'. _DB_PREFIX_ .'globalpayment_cart` '.
                                'SET synced="'.$syncedCode.'",'.
                                    'syncmsg="'.pSQL(urldecode($syncMsg)).'",'.
                                    'syncdata="'.pSQL($qs).'",'.
                                    'date_upd=NOW() '.
                                    'WHERE gpId='.$pinfo['gpId']);
                    }
                }
        
                if(!$result) {
                    print_r("Capture payment for Order ".$orderRef." failed. Please contact technical support");
                }
            }
        }
        exit;
    }
    
     private function curlPost($url, $data){
	      $ch = curl_init();
	      curl_setopt($ch, CURLOPT_URL, $url);
	      curl_setopt($ch, CURLOPT_POST, 1);
	      curl_setopt($ch, CURLOPT_FAILONERROR, false);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	      $response = curl_exec($ch);
	      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	      curl_close($ch);
     
	      return $response;
    }
}