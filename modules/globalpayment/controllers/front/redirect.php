<?php

class GlobalPaymentRedirectModuleFrontController extends ModuleFrontController
{
    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        //$result = array();
        $cart = $this->context->cart;
        
        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
            Tools::redirect('index.php?controller=order&step=1');
        
        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module)
            if ($module['name'] == 'globalpayment') {
                $authorized = true;
                break;
            }
        
        if (!$authorized)
            die($this->module->l('This payment method is not available.', 'validation'));
        
        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');
        
        $currency = $this->context->currency;
        
        //total_product amount
        if ($this->context->currency->decimals == 0) {
            $productsPrice = round(Context::getContext()->cart->getOrderTotal(true));
        } else {
            $productsPrice = Context::getContext()->cart->getOrderTotal(true);
        }
        
        $return_url = $this->context->link->getModuleLink('globalpayment', 'validation', array(), null, null, null, false, false);
        
        Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'globalpayment_cart` (`id_cart`, `date_add`, `date_upd`, total_paid) VALUES ('.$cart->id.',NOW(),NOW(),'.$productsPrice.')');
        $gpId = (int)Db::getInstance()->Insert_ID();
        
        Db::getInstance()->execute(
            'INSERT INTO `'._DB_PREFIX_.'globalpayment_cartproduct` '.
                '(`gpId`,`id_product`,`id_address_delivery`,`id_shop`,`id_product_attribute`,`quantity`,`date_add`) '.
                'SELECT '.$gpId.',`id_product`,`id_address_delivery`,`id_shop`,`id_product_attribute`,`quantity`,NOW() '.
                'FROM `'._DB_PREFIX_.'cart_product` '.
                'WHERE id_cart='.$cart->id
        );

        Db::getInstance()->execute(
            'INSERT INTO `'._DB_PREFIX_.'globalpayment_cartrule` '.
                '(`gpId`,`id_cart_rule`,`date_add`) '.
                'SELECT '.$gpId.',`id_cart_rule`,NOW() '.
                'FROM `'._DB_PREFIX_.'cart_cart_rule` '.
                'WHERE id_cart='.$cart->id
        );

        $arrayArg = array(
            //'order_id' => $order_id,
            //'invoice_no' => $order_id,
            'gpId' => $gpId,
            'amount' => $productsPrice,
            'return_url' => $return_url,
            'default_lang' => $this->context->language->iso_code
        );
        
        //$this->context->cookie->__unset('cart_id');        
        
        echo $this->constrctRequest($arrayArg);
    }
    
    private function constrctRequest($arg)
    {
        $secretHash = Configuration::get('GP_SECRET_HASH');
        
        $data = array(
            "vpc_AccessCode" => Configuration::get('GP_ACC_CODE'),
            "vpc_Amount" => ($arg['amount']*100),
            "vpc_Command" => 'pay',
            "vpc_Locale" => 'en',
            "vpc_MerchTxnRef" => $arg['gpId'],
            "vpc_Merchant" => Configuration::get('GP_MERCHANT_ID'),
            "vpc_OrderInfo" => $arg['gpId'],
            "vpc_ReturnURL" =>  $arg['return_url'],
            "vpc_Version" => '1',
            'vpc_SecureHashType' => 'SHA256');
        
        ksort($data);
        
        $hash = null;
        foreach ($data as $k => $v) {
            if (in_array($k, array('vpc_SecureHash', 'vpc_SecureHashType'))) {
                continue;
            }
            if ((strlen($v) > 0) && ((substr($k, 0, 4)=="vpc_") || (substr($k, 0, 5) =="user_"))) {
                $hash .= $k . "=" . $v . "&";
            }
        }
        
        $hash = rtrim($hash, "&");
        $secureHash = strtoupper(hash_hmac('SHA256', $hash, pack('H*', $secretHash)));
        $paraFinale = array_merge($data, array('vpc_SecureHash' => $secureHash));
        $actionurl = Configuration::get('GP_ACTION_URL').'/vpcpay?'.http_build_query($paraFinale);

        header("Location: " . $actionurl);
    }
}