
<?php

class GlobalPaymentStatusCheckModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $sql1 = 'SELECT DISTINCT A.id_order,B.gpId,C.payment_status,C.transaction_no,C.receipt_no,D.firstname,D.lastname,D.email '.
                        'FROM `'. _DB_PREFIX_ .'orders` A '.
                        'INNER JOIN `'. _DB_PREFIX_ .'customer` D ON A.id_customer=D.id_customer '.
                        'INNER JOIN `'. _DB_PREFIX_ .'globalpayment_cart` B ON A.id_cart=B.id_cart '.
                        'INNER JOIN `'. _DB_PREFIX_ .'payment_callback_log` C ON C.orderId=B.gpId '.
                        'WHERE A.current_state=0 AND C.payment_status="0" AND DATE_ADD(A.date_add, INTERVAL 30 MINUTE) <NOW()';
        $orderList = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql1);

        foreach($orderList as $o) 
        {
            //// update transaction info and order id into globalpayment_cart table
            $sql_update =   'UPDATE `'. _DB_PREFIX_ .'globalpayment_cart`'.
                            ' SET payment_status="'.$o['payment_status'].'",'.
                                ' transaction_no="'.$o['transaction_no'].'",'.
                                ' receipt_no="'.$o['receipt_no'].'",'.
                                ' id_order="'.$o['id_order'].'"'.
                            ' WHERE gpId='.$o['gpId'];
            Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql_update);             
            
            //// update the order status to payment accepted
            // Set the order status
            $extra_vars = array(
                '{firstname}' => $o['firstname'],
			    '{lastname}' =>$o['lastname'],
				'{email}' => $o['email'],
                '{order_name}' => $o['id_order']
            );
            $id_order_state = Configuration::get('PS_OS_PAYMENT');
			$new_history = new OrderHistory();
			$new_history->id_order = (int)$o['id_order'];
			$new_history->changeIdOrderState((int)$id_order_state, $o['id_order'], true);
			$new_history->addWithemail(true, $extra_vars);
        }
        exit;
    }
}