<?php
class GlobalPaymentValidationModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $vpc = array();
        
        foreach($_GET as $key => $val) { 
            if(strpos($key, 'vpc_') === 0) {
                $vpc[$key] = $val;
            }
        }
        
        $gpId = $vpc["vpc_MerchTxnRef"];
        
        $payment_status = "";
        $transaction_no = "";
        $receipt_no = "";
    
        if(isset($vpc["vpc_TxnResponseCode"])) {
            $payment_status = $vpc["vpc_TxnResponseCode"];
        }
    
        if(isset($vpc["vpc_TransactionNo"])) {
            $transaction_no = $vpc["vpc_TransactionNo"];
        }
        
        if(isset($vpc["vpc_ReceiptNo"])) {
            $receipt_no = $vpc["vpc_ReceiptNo"];
        }
        
        $logId = $this->SaveLog($_REQUEST, $gpId, $payment_status, $transaction_no, $receipt_no);
        $logResult = "";
        sleep(1);
        
        $id_cart = $this->getCartIdByGPId($gpId);
        $redirect_checkout = false;
        
        //if($this->CheckDuplicateLog($logId))
        //{
        //    $maxRetry = 20;
        //    $retry_cnt = 1;
        //    sleep(10);
        //    $o = $this->getOrderId($id_cart);
        //    while ($o <= 0 && $retry_cnt < $maxRetry) {
        //        $o = $this->getOrderId($orderid);
        //        $retry_cnt++;
        //        sleep(5);    
        //    }
            
        //    Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$o.'&key='.$customer->secure_key);
        //}
        
        if (empty($id_cart)) {
            exit;
        }    
        else {
            $cart = new Cart($id_cart);
        }
        
        $customer = new Customer($cart->id_customer);
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $amount = (float)$vpc['vpc_Amount'] / 100;
        
        ksort($vpc);
        
        $secretHash = Configuration::get('GP_SECRET_HASH');
        $hash = null;
        foreach ($vpc as $k => $v) {
            if (in_array($k, array('vpc_SecureHash', 'vpc_SecureHashType'))) {
                continue;
            }
            if ((strlen($v) > 0) && ((substr($k, 0, 4)=="vpc_") || (substr($k, 0, 5) =="user_"))) {
                $hash .= $k . "=" . $v . "&";
            }
        }
        
        $hash = rtrim($hash, "&");
        $secureHash = strtoupper(hash_hmac('SHA256', $hash, pack('H*', $secretHash)));
        
        if($secureHash == $vpc['vpc_SecureHash']) {
            print_r("Valid Hash");
            
            if($payment_status == "0") {
                $logResult .= "Process Success Order.";
                $this->UpdateLog($logId, $logResult);
                
                if(!$this->processOrder($id_cart, $amount, $transaction_no, Configuration::get('PS_OS_PAYMENT'), $gpId)) 
                {
                    $this->module->validateOrder($id_cart, Configuration::get('PS_OS_PAYMENT'), $amount, $this->module->displayName, 
                        'Global Payment Transaction ID: ' . $transaction_ref. ' (Global Payment Id: '. $gpId. ')', NULL, (int)$cart->id_currency, false, $customer->secure_key);
                    $logResult .= "||Order Created";
                    $this->UpdateLog($logId, $logResult);
                    
                    //// check total amount. if not same, update order status
                    if(strval($total) != strval($amount)) 
                    {
                        $this->updateOrderValidation($orderid);
                    }
                }
            }
            // transaction pending
            else if ($payment_status == "P") {
                $logResult .= "Process Pending Order.";
                $this->UpdateLog($logId, $logResult);
                
                if(!$this->processOrder($id_cart, $amount, $transaction_no, Configuration::get('PS_OS_AWAITING'), $gpId)) 
                {
                    $this->module->validateOrder($id_cart, Configuration::get('PS_OS_AWAITING'), $amount, $this->module->displayName, 
                        'Global Payment Transaction ID: ' . $transaction_ref. ' (Global Payment Id: '. $gpId. ')', NULL, (int)$cart->id_currency, false, $customer->secure_key);
                    $logResult .= "||Order Created";
                    $this->UpdateLog($logId, $logResult);
                    
                    ////// check total amount. if not same, update order status
                    //if(strval($total) != strval($amount)) 
                    //{
                    //    $this->updateOrderValidation($orderid);
                    //}
                }
            }
            else {
                $logResult .= "Payment Failed";
                $logResult .= "||Redirected";
                $redirect_checkout = true;
                $this->UpdateLog($logId, $logResult);
            }
        }
        else {
            $payment_status = "hash";
            $logResult .= "Invalid Hash";
            $logResult .= "||Redirected";
            $redirect_checkout = true;
            $this->UpdateLog($logId, $logResult);
        }
        
        if($redirect_checkout)
        {
            Tools::redirect('index.php?controller=order&redirect_error='.$this->ErrorResponse($payment_status));
        }
        else {
            $maxRetry = 20;
            $retry_cnt = 1;
              
            $o = $this->getOrderId($id_cart);
            while ($o <= 0 && $retry_cnt < $maxRetry) {
                $o = $this->getOrderId($orderid);
                $retry_cnt++;
                sleep(5);    
            }
            
            $this->UpdatePaymentResult($gpId, $o, $payment_status, $transaction_no, $receipt_no);                   
            Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$o.'&key='.$customer->secure_key);
        }
        
        exit;
    }
    
    private function UpdatePaymentResult ($gpId, $id_order, $payment_status, $transaction_no, $receipt_no) {
        $sql = 'UPDATE `'._DB_PREFIX_.'globalpayment_cart` '.
                'SET payment_status="'.$payment_status.'",'.
                    'transaction_no="'.$transaction_no.'",'.
                    'receipt_no="'.$receipt_no.'",'.
                    'id_order="'.$id_order.'" '.
                'WHERE gpId='.(int)$gpId;
        return Db::getInstance()->execute($sql);
    }
    
    private function ErrorResponse($responseCode) {
        $responseResult = "";
        switch($responseCode) {
            case "1":
            case 1:
                $responseResult = "Transaction unsuccessful, transaction could not be processed.";
            break;
            case "2":
            case 2:
                $responseResult = "Transaction unsuccessful, transaction declined by bank.";
            break;
            case "3":
            case 3:
                $responseResult = "Transaction unsuccessful, no response from bank.";
            break;
            case "4":
            case 4:
                $responseResult = "Transaction unsuccessful, card expired.";
            break;
            case "5":
            case 5:
                $responseResult = "Transaction unsuccessful, card insufficient credit.";
            break;
            case "6":
            case 6:
                $responseResult = "Transaction unsuccessful, bank system error.";
            break;
            case "7":
            case 7:
                $responseResult = "Payment processing error. Please try again.";
            break;
            case "8":
            case 8:
                $responseResult = "Transaction unsuccessful, invalid transaction type.";
            break;
            case "9":
            case 9:
                $responseResult = "Transaction unsuccessful, transaction declined by bank.";
            break;
            case "A":
                $responseResult = "Transaction aborted, please try again.";
            break;
            case "B":
                $responseResult = "Transaction unsuccessful, transaction blocked.";
            break;
            case "C":
                $responseResult = "Transaction cancelled.";
            break;
            case "D":
                $responseResult = "Transaction deferred.";
            break;
            case "E":
                $responseResult = "Transaction unsuccessful, please contact card issuer bank.";
            break;
            case "F":
                $responseResult = "Transaction unsuccessful, 3D Secure Authentication failed.";
            break;
            case "I":
                $responseResult = "Transaction unsuccessful, card security code failed.";
            break;
            case "L":
                $responseResult = "Transaction unsuccessful, duplicate payment.";
            break;
            case "N":
                $responseResult = "Transaction unsuccessful, cardholder is not enrolled in 3D Secure.";
            break;
            case "R":
                $responseResult = "Transaction unsuccessful, retry limit exceeded.";
            break;
            case "T":
                $responseResult = "Transaction unsuccessful, address verification failed.";
            break;
            case "U":
                $responseResult = "Transaction unsuccessful, card security code failed.";
            break;
            case "V":
                $responseResult = "Transaction unsuccessful, address verification and card security code failed.";
            break;
            case "hash":
                $responseResult = "Transaction unsuccessful, unauthorized return.";
            break;
        }
        
        return $responseResult;
    }

    private function SaveLog ($data, $gpId, $paymen_status, $transaction_no, $receipt_no)
    {
        $postdata = json_encode($data);
        $postdata = str_replace('"', '\"', $postdata);
        
        $sql = 'INSERT INTO `'._DB_PREFIX_.'payment_callback_log` (`module`, `file`,`orderId`, `payment_status`,`transaction_no`,`receipt_no`,`postdata`,`createdTS`) '.
                'VALUES ("GlobalPayment", "validation","'.$gpId.'","'.$paymen_status.'","'.$transaction_no.'","'.$receipt_no.'", "'.$postdata.'", NOW())';
        $return = Db::getInstance()->execute($sql);
        return Db::getInstance()->Insert_ID();
    }
    
    private function getCartIdByGPId($gpId) {
        $sql = 'SELECT id_cart FROM `'. _DB_PREFIX_ .'globalpayment_cart` WHERE gpId = '. $gpId;
        $cartId = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        
        return $cartId;
    }
    
    private function getOrderId($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        if ($row){
            return $row["id_order"];
        }
        else {
            return 0;
        }       
    }
    
    private function CheckDuplicateLog($logId)
    {
        $tempSQL = "SELECT COUNT(1) AS logCount FROM `". _DB_PREFIX_ ."payment_callback_log` A ".
                    "INNER JOIN `". _DB_PREFIX_ ."payment_callback_log` B ON ".
                        "A.payment_status = B.payment_status AND A.transaction_no=B.transaction_no AND ".
                        "A.orderId=B.orderId AND A.id < B.id ".
                    "WHERE B.id=". (int)$logId;
        $stateCount = Db::getInstance()->getRow($tempSQL);
      
        if($stateCount["logCount"] > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private function UpdateLog($logId, $logVal) {
        $sql = 'UPDATE `'._DB_PREFIX_.'payment_callback_log` SET result="'.$logVal.'" WHERE id='.(int)$logId;
        $return = Db::getInstance()->execute($sql);
    }
    
    private function processOrder($id_cart, $amount, $tranID, $id_order_state, $gpId) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
        if ($row){
            return $this->module->updateValidatedOrder(
                $id_cart, $row["id_order"], $id_order_state, $amount, $this->module->displayName, 
                'Global Payment Transaction ID: ' . $tranID. ' (Global Payment Id: '. $gpId. ')', NULL, false);
        }
        else {
            return false;
        }    
    }
    
    private function updateOrderValidation($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order,current_state FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        if ($row && $row["current_state"] != Configuration::get('PS_OS_AWAITING')){
            $new_history = new OrderHistory();
		    $new_history->id_order = (int)$row["id_order"];
            $new_history->id_employee = 1;
		    $new_history->changeIdOrderState(Configuration::get('PS_OS_AWAITING'), $row["id_order"]);
            $new_history->addWithemail(true, false);
        }
        else {
            return false;
        }          
    }    
}