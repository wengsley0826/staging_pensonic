<?php

if (!defined('_PS_VERSION_'))
	exit;

class GlobalPayment extends PaymentModule
{
	protected $_html = '';
	protected $_postErrors = array();

	public $merchantId;
	public $accCode;
	public $secretHash;
    public $actionURL;
    public $vpcUser;
    public $vpcPass;
    public $captureCron;
    public $captureDayAfter;
    public $captureStatus;
    
	public function __construct()
	{
		$this->name = 'globalpayment';
		$this->tab = 'payments_gateways';
		$this->version = '1.0.0';
		$this->author = 'Claritas';
		$this->controllers = array('callback', 'redirect', 'validation');
		$this->is_eu_compatible = 1;

		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		$config = Configuration::getMultiple(array(
            'GP_MERCHANT_ID', 'GP_ACC_CODE', 'GP_SECRET_HASH', 
            'GP_ACTION_URL', 'GP_VPC_USER', 'GP_VPC_PASS', 
            'GP_CAPTURE_CRON', 'GP_CAPTURE_STATUS'
            ));
            
		if (!empty($config['GP_MERCHANT_ID']))//ENVIRONMENT  CURRENCY
			$this->merchantId = $config['GP_MERCHANT_ID'];
		if (!empty($config['GP_ACC_CODE']))
			$this->accCode = $config['GP_ACC_CODE'];
		if (!empty($config['GP_SECRET_HASH']))
			$this->secretHash = $config['GP_SECRET_HASH'];
        if (!empty($config['GP_ACTION_URL']))
			$this->actionURL = $config['GP_ACTION_URL'];
		if (!empty($config['GP_VPC_USER']))
			$this->vpcUser = $config['GP_VPC_USER'];
        if (!empty($config['GP_VPC_PASS']))
			$this->vpcPass = $config['GP_VPC_PASS'];
        if (!empty($config['GP_CAPTURE_CRON']))
			$this->captureCron = $config['GP_CAPTURE_CRON'];
   //     if (!empty($config['GP_CAPTURE_DAY_AFTER']))
			//$this->captureDayAfter = $config['GP_CAPTURE_DAY_AFTER'];
        if (!empty($config['GP_CAPTURE_STATUS']))
	        $this->captureStatus = $config['GP_CAPTURE_STATUS'];
            
		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Global Payment');
		$this->description = $this->l('Accept payments for your products via Global Payment.');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
		if (!isset($this->merchantId) || !isset($this->accCode) || !isset($this->secretHash)  || !isset($this->actionURL))
			$this->warning = $this->l('Merchant ID, Access code, Secret Hash and Action URL must be configured before using this payment method');	
	}

	public function install()
	{		
		
		if (!parent::install() || !$this->registerHook('payment') || 
            ! $this->registerHook('displayPaymentEU') || !$this->registerHook('paymentReturn') ||
            ! $this->registerHook('UpdateOrderStatus') )
			return false;

		// install DataBase
        if (!$this->installSQL()) {
            return false;
        }
        
		return true;
	}

	public function uninstall()
	{
		if (!Configuration::deleteByName('GP_MERCHANT_ID') || !Configuration::deleteByName('GP_ACC_CODE') 
            || !Configuration::deleteByName('GP_SECRET_HASH') || !Configuration::deleteByName('GP_ACTION_URL') 
            || !Configuration::deleteByName('GP_VPC_USER') || !Configuration::deleteByName('GP_VPC_PASS')
            || !Configuration::deleteByName('GP_CAPTURE_CRON') 
            || !Configuration::deleteByName('GP_CAPTURE_STATUS')
            || !$this->uninstallSQL() || !parent::uninstall())
            return false;
        
        return true;
	}

	protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('GP_MERCHANT_ID')) {
                $this->_postErrors[] = $this->l('Merchant ID is required!');
            }
            if (!Tools::getValue('GP_ACC_CODE')) {
                $this->_postErrors[] = $this->l('Access Code is required!');
            }
            if (!Tools::getValue('GP_SECRET_HASH')) {
                $this->_postErrors[] = $this->l('Secret Hash is required!');
            }
            if (!Tools::getValue('GP_ACTION_URL')) {
                $this->_postErrors[] = $this->l('Action URL is required!');
            }
            if (!Tools::getValue('GP_VPC_USER')) {
                $this->_postErrors[] = $this->l('VPC User is required!');
            }
            if (!Tools::getValue('GP_VPC_PASS')) {
                $this->_postErrors[] = $this->l('VPC Password is required!');
            }
            //if (!Tools::getValue('GP_CAPTURE_DAY_AFTER')) {
            //    $this->_postErrors[] = $this->l('Sync Payment After (Days) is required!');
            //} 
            //elseif (!Validate::isInt(Tools::getValue('GP_CAPTURE_DAY_AFTER')) || (int)(Tools::getValue('GP_CAPTURE_DAY_AFTER')) <= 0) {
            //    $this->_postErrors[] = $this->l('Invalid value for "Sync Payment After (Days)". Choose a positive integer number.');
            //}
        }
	}

	protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('GP_MERCHANT_ID', Tools::getValue('GP_MERCHANT_ID'));
            Configuration::updateValue('GP_ACC_CODE', Tools::getValue('GP_ACC_CODE'));
            Configuration::updateValue('GP_SECRET_HASH', Tools::getValue('GP_SECRET_HASH'));
            Configuration::updateValue('GP_ACTION_URL', Tools::getValue('GP_ACTION_URL'));
            Configuration::updateValue('GP_VPC_USER', Tools::getValue('GP_VPC_USER'));
            Configuration::updateValue('GP_VPC_PASS', Tools::getValue('GP_VPC_PASS'));
            Configuration::updateValue('GP_CAPTURE_CRON', Tools::getValue('GP_CAPTURE_CRON'));
            Configuration::updateValue('GP_CAPTURE_STATUS', Tools::getValue('GP_CAPTURE_STATUS'));
        }
        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}

	public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
	}

	public function hookPayment($params)
	{

		if (!$this->active)
			return;

		$this->smarty->assign(array(
			'this_path_gp' => $this->_path,
        ));

		return $this->display(__FILE__, 'payment.tpl');
	}

	public function hookDisplayPaymentEU($params)	
	{			
		if (!$this->active)
			return;

		$payment_options = array(
			'cta_text' => $this->l('Pay by Visa / Master'),			
			'logo' => Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/credit.png'),
			'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true)
		);

		return $payment_options;
	}

	public function hookPaymentReturn($params)
	{        
		if (!$this->active)
			return;

		$state = $params['objOrder']->getCurrentState();

		$id_order = (int) Order::getOrderByCartId($params['objOrder']->id_cart);

        $sql = 'SELECT payment_status, transaction_no, receipt_no, id_order FROM `'. _DB_PREFIX_ .'globalpayment_cart` WHERE id_order = '. $id_order.' ORDER BY date_upd DESC';
        $paymentinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
		if (in_array($state, array(Configuration::get('PS_OS_PAYMENT'),Configuration::get('PS_OS_AWAITING'))))
		{
			$this->smarty->assign(array(                
                'status' => 'ok',
                'amount' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                'order_id' => $id_order,
                'order_ref' => $params['objOrder']->reference,
                'transaction_ref' => $paymentinfo['transaction_no'],
                'payment_status' => $paymentinfo['payment_status'],
            ));
		}
		else{
			$this->smarty->assign(array(
                'status' => 'failed',
                'amount' => round($params['objOrder']->getOrdersTotalPaid(), 2),
                'order_id' => $id_order,
                'order_ref' => $params['objOrder']->reference,
                'transaction_ref' => $paymentinfo['transaction_no'],
                'payment_status' => $paymentinfo['payment_status'],
            ));
		}

		return $this->display(__FILE__, 'payment_return.tpl');
	}
	
	private function installSQL() {

        $sql = array();
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "payment_callback_log` (
            `id` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `module` varchar(45) DEFAULT NULL,
            `file` varchar(100) DEFAULT NULL,
            `orderId` varchar(45) DEFAULT NULL,
            `payment_status` varchar(20) DEFAULT NULL,
            `transaction_no` varchar(50) DEFAULT NULL,
            `postdata` text,
            `createdTS` datetime DEFAULT NULL,
            `result` varchar(1000) DEFAULT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "globalpayment_cart` (
            `gpId` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `id_cart` int(11) NOT NULL,
            `date_add` DATETIME NOT NULL,
            `date_upd` DATETIME NOT NULL,
            `total_paid` decimal(10,2) DEFAULT NULL,
            `payment_status` varchar(20) DEFAULT NULL,
            `transaction_no` varchar(50) DEFAULT NULL,
            `receipt_no` varchar(100) DEFAULT NULL,
            `id_order` int(11) DEFAULT NULL,
            `synced` varchar(20) DEFAULT NULL,
            `syncmsg` varchar(500) DEFAULT NULL,
            `syncdata` varchar(2000) DEFAULT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "globalpayment_cartproduct` (
            `gpLogId` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `gpId` int(10) unsigned NOT NULL,
            `id_product` int(10) unsigned NOT NULL,
            `id_address_delivery` int(10) unsigned NOT NULL DEFAULT '0',
            `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
            `id_product_attribute` int(10) unsigned NOT NULL DEFAULT '0',
            `quantity` int(10) unsigned NOT NULL DEFAULT '0',
            `date_add` datetime NOT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;
        
        $sql[] = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "globalpayment_cartrule` (
            `gpLogId` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `gpId` int(10) unsigned NOT NULL,
            `id_cart_rule` int(10) unsigned NOT NULL,
            `date_add` datetime NOT NULL
        ) ENGINE = " . _MYSQL_ENGINE_;

        foreach ($sql as $q) {
            if (!DB::getInstance()->execute($q)) {
                return false;
            }
        }
				
        return true;
    }

    /**
     * Uninstall DataBase table
     * @return boolean if install was successfull
     */
    private function uninstallSQL() {

        $sql = array();
        
        //$sql[] = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "payment_callback_log`";
        //$sql[] = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "globalpayment_cart`";
        
        //foreach ($sql as $q) {
        //    if (!DB::getInstance()->execute($q)) {
        //        return false;
        //    }
        //}
        
        return true;
    }

	public function renderForm()
	{
		 $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Global Payment Details'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Merchant Id'),
                        'name' => 'GP_MERCHANT_ID',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Access Code'),
                        'name' => 'GP_ACC_CODE',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Secret Hash'),
                        'name' => 'GP_SECRET_HASH',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Action URL'),
                        'name' => 'GP_ACTION_URL',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('VPC User'),
                        'name' => 'GP_VPC_USER',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('VPC Password'),
                        'name' => 'GP_VPC_PASS',
                        'required' => true
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Auto Capture Payment'),
                        'name' => 'GP_CAPTURE_CRON',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),                    
                    //array(
                    //    'type' => 'text',
                    //    'label' => $this->l('Sync Payment After (Days)'),
                    //    'name' => 'GP_CAPTURE_DAY_AFTER',
                    //    'required' => true
                    //),
                    array(
                    'type' => 'select',
					'label' => $this->l('Auto Capture Status:'),
					'name' => 'GP_CAPTURE_STATUS',
					'options' => array(
						'query' => $this->getOrderStatuses(),
						'id' => 'id_order_state',
						'name' => 'name',
						'default' => array(
							'label' => $this->l('No Status'),
							'value' => 0
						)
					)
                ),
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            )
        );
        
        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->id                       = (int) Tools::getValue('id_carrier');
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm(array(
            $fields_form
        ));
	}

     protected function getOrderStatuses() {
        $allstatuses = Db::getInstance()->ExecuteS('SELECT DISTINCT A.id_order_state, B.name AS status_name'.
                    ' FROM '._DB_PREFIX_.'order_state A'.
                    ' INNER JOIN '._DB_PREFIX_.'order_state_lang B ON A.id_order_state = B.id_order_state'.
                    ' WHERE B.id_lang = 1'.
                    ' ORDER BY B.name'
		);
        
        $status_list = array();
        
        foreach ($allstatuses as $status)
        {
            $status_list[] = array(
                "id_order_state" => $status["id_order_state"],
                "name" => $status["status_name"]
            );
        }
        
        return $status_list;
    }
    
	public function getConfigFieldsValues()
	{
		return array(
			'GP_MERCHANT_ID' => Tools::getValue('GP_MERCHANT_ID', Configuration::get('GP_MERCHANT_ID')),
            'GP_ACC_CODE' => Tools::getValue('GP_ACC_CODE', Configuration::get('GP_ACC_CODE')),
            'GP_SECRET_HASH' => Tools::getValue('GP_SECRET_HASH', Configuration::get('GP_SECRET_HASH')),
            'GP_ACTION_URL' => Tools::getValue('GP_ACTION_URL', Configuration::get('GP_ACTION_URL')),
            'GP_VPC_USER' => Tools::getValue('GP_VPC_USER', Configuration::get('GP_VPC_USER')),
            'GP_VPC_PASS' => Tools::getValue('GP_VPC_PASS', Configuration::get('GP_VPC_PASS')),
            'GP_CAPTURE_CRON' => Tools::getValue('GP_CAPTURE_CRON', Configuration::get('GP_CAPTURE_CRON')),
            'GP_CAPTURE_STATUS' => Tools::getValue('GP_CAPTURE_STATUS', Configuration::get('GP_CAPTURE_STATUS'))
		);
	}	
    
    public function hookUpdateOrderStatus($params) {
        $autoCapture = Configuration::get('GP_CAPTURE_CRON');
        $id_order = $params["id_order"];
        $new_status = $params["newOrderStatus"];
        $capture_status = Configuration::get("GP_CAPTURE_STATUS");
        $sql = "";
        $remarks = "";
        
        if($autoCapture == "1") {
            $remarks .="STEP1|";
            $syncMsg = null;
            $result = true;
            $orderRef = "";
            
            if($new_status->id == $capture_status) {
                $remarks .="STEP2|";
                $sql = 'SELECT A.gpId, A.id_cart, A.payment_status, A.transaction_no, A.receipt_no, A.id_order, A.total_paid,B.reference '.
                        'FROM `'. _DB_PREFIX_ .'globalpayment_cart` A '.
                        'INNER JOIN `'. _DB_PREFIX_ .'orders` B ON A.id_order=B.id_order '.
                        'WHERE A.id_order='.(int)$id_order.' AND A.payment_status="0" AND (A.synced IS NULL OR A.synced != "0")';
                $pinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
                $remarks .="STEP3|";
               
                if($pinfo != null) {
                    $remarks .="STEP4|";
                    $actionurl = Configuration::get('GP_ACTION_URL').'/vpcdps';
                    $accCode = Configuration::get('GP_ACC_CODE');
                    $merchantId = Configuration::get('GP_MERCHANT_ID');
            
                    $post = array();
                    $post['vpc_Version'] = '1';
                    $post['vpc_Command'] = 'capture';
                    $post['vpc_AccessCode'] = $accCode;
                    $post['vpc_MerchTxnRef'] = $pinfo['gpId'];
                    $post['vpc_Merchant'] = $merchantId;
                    $post['vpc_TransNo'] = $pinfo['transaction_no'];
                    $post['vpc_Amount'] = (string)floor(($pinfo['total_paid'] * 100));
                    $post['vpc_User'] = Configuration::get('GP_VPC_USER');
                    $post['vpc_Password'] = Configuration::get('GP_VPC_PASS');
                    
                    $qs = $this->curlPost($actionurl, $post);
                    $result = explode("&", $qs);
                    $remarks .="STEP5|";
                    
                    $syncedCode = "";
                    
                    foreach ($result as $k => $str) {
                        $tmp = explode('=', $str);
                        if ($tmp[0] == 'vpc_TxnResponseCode') {
                            $syncedCode = $tmp[1];
                        
                            if ($tmp[1] == '0') {
                                $result = true;
                            } else {
                                $result = false;
                            }
                        }
                        else if ($tmp[0] == 'vpc_Message') {
                            $result = false;
                            $syncMsg = $tmp[1];
                            $orderRef = $pinfo['id_order'];
                        }          
                    }
                    $remarks .="STEP6|";
                    
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
                        'UPDATE `'. _DB_PREFIX_ .'globalpayment_cart` '.
                            'SET synced="'.$syncedCode.'",'.
                                'syncmsg="'.pSQL(urldecode($syncMsg)).'",'.
                                'syncdata="'.pSQL($qs).'",'.
                                'date_upd=NOW() '.
                                'WHERE gpId='.$pinfo['gpId']);
                }
            }

            if(!$result) {
                return "Capture payment for Order ".$orderRef." failed. Please contact technical support";
            }
        }

        Db::getInstance(_PS_USE_SQL_SLAVE_)->execute(
            'INSERT `'. _DB_PREFIX_ .'globalpayment_capturelog` (`autocapture`,`capturestatus`,`id_order`,`new_status`,`sql`,`date_add`,`remarks`)'.
            'VALUES ("'.$autoCapture.'","'.$capture_status.'","'.$id_order.'","'.$new_status->id.'","'.pSQL($sql).'",NOW(),"'.$remarks.'")');
    }

    private function curlPost($url, $data){
	      $ch = curl_init();
	      curl_setopt($ch, CURLOPT_URL, $url);
	      curl_setopt($ch, CURLOPT_POST, 1);
	      curl_setopt($ch, CURLOPT_FAILONERROR, false);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	      $response = curl_exec($ch);
	      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	      curl_close($ch);
     
	      return $response;
    }
}
