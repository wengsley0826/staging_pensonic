{if $status == 'ok'}
<div class="alert alert-success">
	{if $payment_status == "0"}
		{l s='We have received your Payment Successfully. Thank you for you Purchases!' mod='globalpayment'}
	{else if $payment_status == "P"}
		{l s='Your payment is pending, we will notify you once the transaction completed. Thank you for you Purchases!' mod='globalpayment'}
	{/if}	
</div>
{else}
<div class="alert alert-warning">
	{l s='An error occurred while processing your order!' mod='globalpayment'}
</div>
{/if}
<div class="box cheque-box">
{if $status == 'ok'}
	<p><span class="price"><strong>Amount : {$amount} </strong></span></p>
	<p><strong>Order #: {$order_id}</strong></p>	
	{*<p><strong>Order Reference: {$order_ref}</strong></p>*}
	<p><strong>Transaction ref: {$transaction_ref}</strong></p>	

{else}
	<p><span class="price"><strong>Amount : {$amount} </strong></span></p>
	<p><strong>Order #: {$order_id}</strong></p>	
	{*<p><strong>Order Reference: {$order_ref}</strong></p>*}
	<p><strong>Transaction ref: {$transaction_ref}</strong></p>
{/if}
</div>