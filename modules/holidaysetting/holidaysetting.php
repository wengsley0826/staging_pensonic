<?php

class HolidaySetting extends Module
{
    protected $_html = '';
    protected $_postErrors = array();

    public function __construct()
    {
        if (!defined('_PS_VERSION_')) {
            exit;
        }
        $this->name = 'holidaysetting';
        $this->tab = 'Holiday_Setting';
        $this->version = '1.0.0';
        $this->author = 'Claritas';
        $this->displayName = 'Holiday Settings';
        $this->bootstrap = true;
        $this->display = 'view';
        $this->is_eu_compatible = 1;
        
        parent::__construct();

        $this->description = $this->l('Holiday Settings');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

    }

    public function install()
    {
        return parent::install();
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        
        return true;
    }

    public function getContent()
    {
        $this->context->controller->addjQueryPlugin('hoverIntent');


        if (Tools::isSubmit('submitHoliday'))   
        {
            $holiday_date = Tools::getValue('holiday_date');
            $holiday_name = Tools::getValue('holiday_name');

            if($holiday_date == "") {
                 $this->_html .= $this->displayError($this->l('Please complete the "Holiday" field.'));
            }
            else if($holiday_name == "") {
                 $this->_html .= $this->displayError($this->l('Please complete the "Description" field.'));
            }

			Db::getInstance()->execute('INSERT '._DB_PREFIX_.'holiday(holiday_date,holiday_name,date_add,date_upd,active) '.
                'VALUES ("'.$holiday_date.'","'.$holiday_name.'",NOW(),NOW(),1)');
		}
		elseif(Tools::isSubmit('deleteholiday'))
        {
            $id_holiday = Tools::getValue('id_holiday', 0);
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'holiday SET active = 0, date_upd=NOW() WHERE id_holiday='.(int)$id_holiday);

            $this->_html .= $this->displayConfirmation($this->l('The holiday has been removed.'));
        }

		$this->_html .= $this->renderForm();
        $this->_html .= $this->renderList();

		return $this->_html;
    }

    public function renderForm($message = null)
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Add holiday'),
                    'icon' => 'icon-link'
                ),
                'input' => array(
                    array(
                        'type' => 'date',
                        'label' => $this->l('Holiday'),
                        'name' => 'holiday_date',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Description'),
                        'name' => 'holiday_name',
                    ),
                ),
                'submit' => array(
                    'name' => 'submitHoliday',
                    'title' => $this->l('Add')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->fields_value = array(
            'holiday_date'=> '',
            'holiday_name'=> ''
        );

        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
            '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->context->controller->getLanguages();
        $helper->default_form_language = (int)$this->context->language->id;

        return $helper->generateForm(array($fields_form));
    }

    public function renderList()
    {
        $holidays = array();

        $sql = 'SELECT l.id_holiday, l.holiday_date, l.holiday_name
				FROM '._DB_PREFIX_.'holiday l
				WHERE l.active>0 ORDER BY l.holiday_date DESC';

        $holidays = Db::getInstance()->executeS($sql);

        
        $fields_list = array(
            'id_holiday' => array(
                'title' => $this->l('Link ID'),
                'type' => 'text',
            ),
            'holiday_date' => array(
                'title' => $this->l('Holiday'),
                'type' => 'text',
            ),
            'holiday_name' => array(
                'title' => $this->l('Description'),
                'type' => 'text',
            ),
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_holiday';
        $helper->table = 'holiday';
        $helper->actions = array('delete');
        $helper->show_toolbar = false;
        $helper->module = $this;
        $helper->title = $this->l('Holiday list');
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        return $helper->generateList($holidays, $fields_list);
    }
}
