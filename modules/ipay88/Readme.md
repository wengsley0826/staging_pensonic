iPay88 payment module for Prestashop

Pre-installation :

1. Unzip file IPAY88 for Prestashop.zip to your Prestashop root directory.


Installation :
1. Log in to PrestaShop Admin Page -> Payment
2. Click on "Install" button
3. You will see a "Configure" tab after installation , click on "Configure" tab.
4. Please input the iPay88 Merchant's Code and Merchant Key for your account.
5. Click "Update Settings" to save all your settings.