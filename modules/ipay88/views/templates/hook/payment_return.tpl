{if $status == '1'}
<p>
    <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/ok.png" align="left">
    <strong>{l s='Your order on %s is complete.' sprintf=$shop_name mod='ipay88'}</strong>
    <br />
    <br />
    <strong>{l s='Your order will be sent soon.' mod='ipay88'}</strong>
    <br />
    <br />
    {l s='You can view your order history by following this link:' mod='ipay88'}
    <a href="{$link->getPageLink('history', true)}">{l s='Order History' mod='ipay88'}</a>
    <br />
    <br />
    {l s='For any questions or for further information, please contact our' mod='ipay88'} 
    <a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='ipay88'}</a>.
    <br />
    <br />
    <strong>{l s='Thank you!' mod='ipay88'}</strong>
</p>
{else}
<p class="warning">
    <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/not_ok.png" align="left">{l s='We noticed a problem with your order. Please, contact us as soon as possible' mod='ipay88'}.
    <br />
    <br />
    {l s='Your order will not be shipped until the issue is addressed' mod='ipay88'} 
    <br />
    <br />
</p>
{/if}