<?php

include_once(_PS_SWIFT_DIR_.'swift_required.php');

// ~/module/notificationjob/ERPExport
class NotificationJobERPExportModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
    
    }
    
   
    public function initContent()
    {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        $erpConfig = Configuration::getMultiple(array(
            'NJ_ERP_CRON', 'NJ_ERP_MAIL', 'NJ_ERP_STATUS', 
            'NJ_ERP_PLANT', 'NJ_ERP_STORELOC', 'NJ_ERP_CUSTACC'
        ));

        
        if($erpConfig['NJ_ERP_CRON'] == "1") 
        {
            $orders = $this->getOrderToProcess($erpConfig['NJ_ERP_STATUS']);
            $order_ids = [];
            $processDate = date("Ymd");
print_r("Order Count => ". count($orders));
print_r("<br />");
            if($orders && count($orders) > 0) 
            {
                for ($i = 0; $i < count($orders); $i++) {
                    if(!in_array($orders[$i]["id_order"], $order_ids)) {
                        $order_ids[] = $orders[$i]["id_order"];

                        $orderCount = Db::getInstance()->getValue("SELECT COUNT(1) FROM ". _DB_PREFIX_ ."order_outbound WHERE id_order=".(int)$orders[$i]["id_order"]);
print_r("Order ".$orders[$i]["id_order"]." in outbound:". $orderCount);
print_r("<br />");
                        if ($orderCount == 0) {
                            Db::getInstance()->execute(
                                "INSERT INTO `". _DB_PREFIX_ ."order_outbound` (`id_order`, `date_add`, `date_upd`, `kerry_status`,`janio_status`)".
                                "VALUES(".$orders[$i]["id_order"].",NOW(),NOW(),0,0)");
                        }
                    }
                }

                $batchFileName = $this->generateOrderERPFile($orders, $processDate, $erpConfig, false);
print_r("Batch File => ". $batchFileName);      
print_r("<br />");  

                $batchReplacementFileName = $this->generateOrderERPFile($orders, $processDate, $erpConfig, true);
print_r("Replacement File => ". $batchReplacementFileName);      
print_r("<br />");  

                $sendResult = $this->sendERPMail($batchFileName, $batchReplacementFileName, $erpConfig['NJ_ERP_MAIL']);

                if($sendResult) {
print_r("Email success");      
print_r("<br />");  
                    $outboundDir = $this->getOutboundLocalPath();
                    $outboundBackupDir = $outboundDir."backup/";
        
                    if(!file_exists ($outboundBackupDir)) {
                        mkdir($outboundBackupDir, 0777, true);
                    }

                    if($batchFileName != "")
                        rename($outboundDir.$batchFileName, $outboundBackupDir.$batchFileName);
                    if($batchReplacementFileName != "")
                        rename($outboundDir.$batchReplacementFileName, $outboundBackupDir.$batchReplacementFileName);

                    Db::getInstance()->execute("UPDATE ". _DB_PREFIX_ ."order_outbound SET erp_status=1 WHERE id_order IN (".implode(",", $order_ids).")");
                }
                else {
print_r("Email failed");      
print_r("<br />");  
                    Db::getInstance()->execute("UPDATE ". _DB_PREFIX_ ."order_outbound SET erp_status=0 WHERE id_order IN (".implode(",", $order_ids).")");
                }
            }
        }
        die();
    }

    public function getOrderToProcess($erpExportStatus) {
        $sql = 
            "select A.id_order, A.date_add,A.total_shipping_tax_incl,A.total_products_wt,
                B.product_reference, B.product_quantity, B.unit_price_tax_incl, B.unit_price_tax_excl, B.product_price,B.product_id,B.product_attribute_id,
                IFNULL(G.unity, 'PAC') AS UOM, A.module
            from ". _DB_PREFIX_ ."orders A
            inner join (
	            select A.id_order, B.box_size, A.total_weightage
	            from (
		            select id_order, SUM(product_weight * product_quantity) AS total_weightage
		            from ". _DB_PREFIX_ ."order_detail
		            group by id_order
	            ) A
	            inner join ". _DB_PREFIX_ ."carrier_box B ON B.min_weightage <= A.total_weightage AND B.max_weightage >= A.total_weightage
            ) H ON A.id_order = H.id_order
            inner join ". _DB_PREFIX_ ."order_detail B ON A.id_order = B.id_order
            inner join ". _DB_PREFIX_ ."customer C ON A.id_customer = C.id_customer
            inner join ". _DB_PREFIX_ ."address D ON A.id_address_delivery = D.id_address
            left join ". _DB_PREFIX_ ."state E ON D.id_state = E.id_state
            inner join ". _DB_PREFIX_ ."country_lang F ON D.id_country = F.id_country AND F.id_lang = 1
            left join ". _DB_PREFIX_ ."product G ON B.product_id = G.id_product
            left join (
                select id_order, MAX(erp_status) AS erp_status
                from ". _DB_PREFIX_ ."order_outbound
                group by id_order
            ) J ON A.id_order = J.id_order 
            where A.current_state IN (".$erpExportStatus.") AND (J.erp_status is null or J.erp_status = 0)
            order by A.id_order";
        $orders = Db::getInstance()->executeS($sql);

        return $orders;
    }

    public function generateOrderERPFile($orders, $processDate, $erpConfig, $replacement) 
    {
        if(count($orders) > 0) 
        {
            $curDate = date('Y-m-d H:i:s');
            
            $outboundDir = $this->getOutboundLocalPath();
            if(!file_exists ($outboundDir)) {
                mkdir($outboundDir, 0777, true);
            }

            if($replacement) 
                $batchFileName = "REPLACEMENT_".$processDate.".csv";
            else
                $batchFileName = "SALEORDER_".$processDate.".csv";
            $batchFile = fopen($outboundDir.$batchFileName, "w");

            $hasItem = false;
            $itemNo = 1;
            $curOrderId = 0;

            //// dont need header
            //$header = $this->generateFileHeader();
            //fwrite($batchFile, $header);
            $orderDiscount = [];

            foreach ($orders as $o) 
            {
                if((!$replacement && $o['module'] != "cashondelivery") || ($replacement && $o['module'] == "cashondelivery")) 
                {
                    $hasItem = true;
                    //// check gift product
                    $sql = "SELECT B.code,B.gift_wbs FROM ". _DB_PREFIX_ ."order_cart_rule A ".
                            " INNER JOIN ". _DB_PREFIX_ ."cart_rule B ON A.id_cart_rule = B.id_cart_rule ".
                            " WHERE A.id_order=".(int)$o['id_order'].
                                " AND B.gift_product=".(int)$o['product_id'].
                                " AND B.gift_product_attribute=".(int)$o['product_attribute_id'];
                    $gifts = Db::getInstance()->executeS($sql);

                    //// check gift product
                    if(!array_key_exists ($o['id_order'], $orderDiscount)) {
                        $discountsql = "SELECT SUM(A.value - IFNULL(C.unit_price_tax_incl,0)) total_discount, SUM(IFNULL(C.unit_price_tax_incl,0)) AS total_gift
                                        FROM ". _DB_PREFIX_ ."order_cart_rule A
                                        INNER JOIN ". _DB_PREFIX_ ."cart_rule B ON A.id_cart_rule = B.id_cart_rule
                                        LEFT JOIN ". _DB_PREFIX_ ."order_detail C ON A.id_order = C.id_order AND B.gift_product = C.product_id AND B.gift_product_attribute = C.product_attribute_id
                                        where A.id_order=".(int)$o['id_order'];
                        $totaldiscount = Db::getInstance()->getRow($discountsql);

                        $orderDiscount[$o['id_order']] = $totaldiscount;
                    }

                    if($curOrderId != $o['id_order']) {
                        $curOrderId = $o['id_order'];
                        $itemNo = 1;
                    }
                    else {
                        $itemNo = $itemNo + 1;
                    }
                
                    $quantity = $o['product_quantity'] - count($gifts);

                    if($quantity > 0)
                    {
                        //// query current order and product to check if this product is a bundle product
                        $bundleSQL = "SELECT B.reference, B.price, A.quantity, C.originalTotal, IFNULL(B.unity, 'PAC') AS UOM ".
                                        "FROM ". _DB_PREFIX_ ."pack A ".
                                        "INNER JOIN ". _DB_PREFIX_ ."product B ON A.id_product_item = B.id_product ".
                                        "INNER JOIN ( ".
				                            "SELECT SUM((B.price * A.quantity)) AS originalTotal ".
                                            "FROM ". _DB_PREFIX_ ."pack A ".
				                            "INNER JOIN ". _DB_PREFIX_ ."product B ON A.id_product_item = B.id_product ".
                                            "WHERE A.id_product_pack=".(int)$o['product_id'].
                                        ") C ".
                                        "WHERE id_product_pack=".(int)$o['product_id'];     
                        $bundles = Db::getInstance()->executeS($bundleSQL);

                        if(count($bundles) > 0) 
                        {
                            for($i = 0; $i < count($bundles); $i++) 
                            {
                                if($i != 0)
                                    $itemNo = $itemNo + 1;

                                $txt = "";
                                $txt .= $this->encodeCSV($itemNo * 10);
                                $txt .= $this->encodeCSV("ZDS1");
                                $txt .= $this->encodeCSV("SG01");
                                $txt .= $this->encodeCSV("50");
                                $txt .= $this->encodeCSV("0");
                                $txt .= $this->encodeCSV("1000");
                                $txt .= $this->encodeCSV("101");
                                $txt .= $this->encodeCSV(date("Y.m.d",strtotime($o['date_add'])));
                                $txt .= $this->encodeCSV($erpConfig['NJ_ERP_CUSTACC']);
                                $txt .= $this->encodeCSV(""); // customer group
                                $txt .= $this->encodeCSV(""); // customer group 1
                                $txt .= $this->encodeCSV(""); // customer group 2
                                $txt .= $this->encodeCSV(""); // customer group 3
                                $txt .= $this->encodeCSV(""); // customer group 4
                                $txt .= $this->encodeCSV(""); // reference number
                                $txt .= $this->encodeCSV(""); // reference tax invoice
                                $txt .= $this->encodeCSV(Tools::brandsOrderNumber($o['id_order']));
                                $txt .= $this->encodeCSV(""); // Order Reason
                                $txt .= $this->encodeCSV(""); // credit card charge
                                $txt .= $this->encodeCSV($bundles[$i]['reference']);
                                $txt .= $this->encodeCSV(""); // description

                                $bundleQuantity = $quantity * $bundles[$i]['quantity'];
                                $txt .= $this->encodeCSV($bundleQuantity); // quantity
                                $txt .= $this->encodeCSV($bundles[$i]['UOM']); // UOM

                                $taxRate = number_format(($o['unit_price_tax_incl'] - $o['unit_price_tax_excl'] ) / $o['unit_price_tax_excl'], 3);
                                $productPriceTax = number_format($bundles[$i]['price'] * (1 + $taxRate), 2);
                                $txt .= $this->encodeCSV(number_format($productPriceTax,2)); // Price incl. tax
                                $txt .= $this->encodeCSV("1"); // Price Per
                                $txt .= $this->encodeCSV($bundles[$i]['UOM']); // Price UoM

                                $sellingPriceTax = number_format($o['unit_price_tax_incl'] * $bundles[$i]['price'] / $bundles[$i]['originalTotal'], 2);
                                $voucherDiscount = $sellingPriceTax * $bundleQuantity * $orderDiscount[$o['id_order']]["total_discount"]/ ($o['total_products_wt'] - $orderDiscount[$o['id_order']]["total_gift"]) / $bundleQuantity;
                                $txt .= $this->encodeCSV(number_format($productPriceTax - $sellingPriceTax + $voucherDiscount,2)); // Discount incl. tax
                                $txt .= $this->encodeCSV("1"); // Discount Per
                                $txt .= $this->encodeCSV($bundles[$i]['UOM']); // Discount UoM

                                $unitShippingCost = $sellingPriceTax * $bundleQuantity * $o['total_shipping_tax_incl'] / ($o['total_products_wt'] - $orderDiscount[$o['id_order']]["total_gift"]) / $bundleQuantity;
                                $txt .= $this->encodeCSV(number_format($unitShippingCost,2)); // Delivery incl. tax
                                $txt .= $this->encodeCSV("1"); // Delivery Per
                                $txt .= $this->encodeCSV($o['UOM']); // Delivery UoM

                                $txt .= $this->encodeCSV(""); // N/A
                                $txt .= $this->encodeCSV($erpConfig['NJ_ERP_PLANT']); // Plant
                                $txt .= $this->encodeCSV($erpConfig['NJ_ERP_STORELOC']); // Storage Location
                                $txt .= $this->encodeCSV(""); // Batch Number
                                $txt .= $this->encodeCSV(""); // WBS
                                $txt .= "\r\n";

                                //print_r($txt);
                                //print_r("<br />");
                                fwrite($batchFile, $txt);
                            }
                        }
                        else {
                            $txt = "";
                            $txt .= $this->encodeCSV($itemNo * 10);
                            $txt .= $this->encodeCSV("ZDS1");
                            $txt .= $this->encodeCSV("SG01");
                            $txt .= $this->encodeCSV("50");
                            $txt .= $this->encodeCSV("0");
                            $txt .= $this->encodeCSV("1000");
                            $txt .= $this->encodeCSV("101");
                            $txt .= $this->encodeCSV(date("Y.m.d",strtotime($o['date_add'])));
                            $txt .= $this->encodeCSV($erpConfig['NJ_ERP_CUSTACC']);
                            $txt .= $this->encodeCSV(""); // customer group
                            $txt .= $this->encodeCSV(""); // customer group 1
                            $txt .= $this->encodeCSV(""); // customer group 2
                            $txt .= $this->encodeCSV(""); // customer group 3
                            $txt .= $this->encodeCSV(""); // customer group 4
                            $txt .= $this->encodeCSV(""); // reference number
                            $txt .= $this->encodeCSV(""); // reference tax invoice
                            $txt .= $this->encodeCSV(Tools::brandsOrderNumber($o['id_order']));
                            $txt .= $this->encodeCSV(""); // Order Reason
                            $txt .= $this->encodeCSV(""); // credit card charge
                            $txt .= $this->encodeCSV($o['product_reference']);
                            $txt .= $this->encodeCSV(""); // description
                            $txt .= $this->encodeCSV($quantity); // quantity
                            $txt .= $this->encodeCSV($o['UOM']); // UOM

                            $sellingPriceNoTax = $o['unit_price_tax_excl'];
                            $sellingPriceTax = $o['unit_price_tax_incl'];
                            $taxRate = number_format(($o['unit_price_tax_incl'] - $o['unit_price_tax_excl'] ) / $o['unit_price_tax_excl'], 3);
                        
                            $productPriceTax = number_format($o['product_price'] * (1 + $taxRate), 2);
                            $txt .= $this->encodeCSV(number_format($productPriceTax,2)); // Price incl. tax
                            $txt .= $this->encodeCSV("1"); // Price Per
                            $txt .= $this->encodeCSV($o['UOM']); // Price UoM

                            $voucherDiscount = $sellingPriceTax * $quantity * $orderDiscount[$o['id_order']]["total_discount"]/ ($o['total_products_wt'] - $orderDiscount[$o['id_order']]["total_gift"]) / $quantity;
                            $discountPriceTax = $productPriceTax - $sellingPriceTax + $voucherDiscount;

                            if(round($discountPriceTax,2) == 0) {
                                $txt .= $this->encodeCSV(""); // Discount incl. tax
                            }
                            else {
                                $txt .= $this->encodeCSV(number_format($discountPriceTax,2)); // Discount incl. tax
                            }
                            $txt .= $this->encodeCSV("1"); // Discount Per
                            $txt .= $this->encodeCSV($o['UOM']); // Discount UoM

                            $unitShippingCost = $sellingPriceTax * $quantity * $o['total_shipping_tax_incl'] / ($o['total_products_wt'] - $orderDiscount[$o['id_order']]["total_gift"]) / $quantity;
                            if(round($unitShippingCost,2) == 0) {
                                $txt .= $this->encodeCSV(""); // Delivery incl. tax
                            }
                            else {
                                $txt .= $this->encodeCSV(number_format($unitShippingCost,2)); // Delivery incl. tax
                            }
                            $txt .= $this->encodeCSV("1"); // Delivery Per
                            $txt .= $this->encodeCSV($o['UOM']); // Delivery UoM

                            $txt .= $this->encodeCSV(""); // N/A
                            $txt .= $this->encodeCSV($erpConfig['NJ_ERP_PLANT']); // Plant
                            $txt .= $this->encodeCSV($erpConfig['NJ_ERP_STORELOC']); // Storage Location
                            $txt .= $this->encodeCSV(""); // Batch Number
                            $txt .= $this->encodeCSV(""); // WBS
                            $txt .= "\r\n";

                            //print_r($txt);
                            //print_r("<br />");
                            fwrite($batchFile, $txt);
                        }
                    }

                    //// free gift
                    for($i = 0; $i < count($gifts); $i++) 
                    {
                        if(!($quantity == 0 && $i == 0))
                            $itemNo = $itemNo + 1;

                        $txt = "";
                        $txt .= $this->encodeCSV($itemNo * 10);
                        $txt .= $this->encodeCSV("ZDS1");
                        $txt .= $this->encodeCSV("SG01");
                        $txt .= $this->encodeCSV("50");
                        $txt .= $this->encodeCSV("0");
                        $txt .= $this->encodeCSV("1000");
                        $txt .= $this->encodeCSV("101");
                        $txt .= $this->encodeCSV(date("Y.m.d",strtotime($o['date_add'])));
                        $txt .= $this->encodeCSV($erpConfig['NJ_ERP_CUSTACC']);
                        $txt .= $this->encodeCSV(""); // customer group
                        $txt .= $this->encodeCSV(""); // customer group 1
                        $txt .= $this->encodeCSV(""); // customer group 2
                        $txt .= $this->encodeCSV(""); // customer group 3
                        $txt .= $this->encodeCSV(""); // customer group 4
                        $txt .= $this->encodeCSV(""); // reference number
                        $txt .= $this->encodeCSV(""); // reference tax invoice
                        $txt .= $this->encodeCSV(Tools::brandsOrderNumber($o['id_order']));
                        $txt .= $this->encodeCSV(""); // Order Reason
                        $txt .= $this->encodeCSV(""); // credit card charge
                        $txt .= $this->encodeCSV($o['product_reference']);
                        $txt .= $this->encodeCSV(""); // description
                        $txt .= $this->encodeCSV("1"); // quantity
                        $txt .= $this->encodeCSV($o['UOM']); // UOM

                        $txt .= $this->encodeCSV(""); // Price incl. tax
                        $txt .= $this->encodeCSV(""); // Price Per
                        $txt .= $this->encodeCSV(""); // Price UoM
                        $txt .= $this->encodeCSV(""); // Discount incl. tax
                        $txt .= $this->encodeCSV(""); // Discount Per
                        $txt .= $this->encodeCSV(""); // Discount UoM

                        $txt .= $this->encodeCSV(""); // Delivery incl. tax
                        $txt .= $this->encodeCSV(""); // Delivery Per
                        $txt .= $this->encodeCSV(""); // Delivery UoM

                        $txt .= $this->encodeCSV(""); // N/A
                        $txt .= $this->encodeCSV($erpConfig['NJ_ERP_PLANT']); // Plant
                        $txt .= $this->encodeCSV($erpConfig['NJ_ERP_STORELOC']); // Storage Location
                        $txt .= $this->encodeCSV(""); // Batch Number
                        $txt .= $this->encodeCSV($gifts[$i]["gift_wbs"]); // WBS
                        $txt .= "\r\n";

                        //print_r($txt);
                        //print_r("<br />");
                        fwrite($batchFile, $txt);

                    }
                }
            }
            
            fclose($batchFile);

            if($hasItem)
                return $batchFileName;
            else {
                unlink($outboundDir.$batchFileName);
                return "";
            }
        }

        return "";
    }

    private function generateFileHeader() {
        $txt = "";
        $txt .= $this->encodeCSV("Item No.");
        $txt .= $this->encodeCSV("Order Type");
        $txt .= $this->encodeCSV("Sales Organization");
        $txt .= $this->encodeCSV("Distribution Channel");
        $txt .= $this->encodeCSV("Division");

        $txt .= $this->encodeCSV("Sales office");
        $txt .= $this->encodeCSV("Sales group");
        $txt .= $this->encodeCSV("Transaction Date");
        $txt .= $this->encodeCSV("Customer Number");
        $txt .= $this->encodeCSV("Customer Group"); // customer group

        $txt .= $this->encodeCSV("Customer Group 1"); // customer group 1
        $txt .= $this->encodeCSV("Customer Group 2"); // customer group 2
        $txt .= $this->encodeCSV("Customer Group 3"); // customer group 3
        $txt .= $this->encodeCSV("Customer Group 4"); // customer group 4
        $txt .= $this->encodeCSV("Reference number"); // reference number

        $txt .= $this->encodeCSV("Reference tax invoice"); // reference tax invoice
        $txt .= $this->encodeCSV("PO Number");
        $txt .= $this->encodeCSV("Order Reason"); // Order Reason
        $txt .= $this->encodeCSV("Credit Card Charge"); // credit card charge
        $txt .= $this->encodeCSV("Material Number");

        $txt .= $this->encodeCSV("Material Description"); // description
        $txt .= $this->encodeCSV("Quantity"); // quantity
        $txt .= $this->encodeCSV("UOM"); // UOM
        $txt .= $this->encodeCSV("Price  include tax"); // Price incl. tax
        $txt .= $this->encodeCSV("Price Per"); // Price Per

        $txt .= $this->encodeCSV("Price UOM"); // Price UoM
        $txt .= $this->encodeCSV("Discount  include tax"); // Discount incl. tax
        $txt .= $this->encodeCSV("Discount Per"); // Discount Per
        $txt .= $this->encodeCSV("Discount UOM"); // Discount UoM
        $txt .= $this->encodeCSV("Delivery cost 1 include tax"); // Discount incl. tax

        $txt .= $this->encodeCSV("Delivery cost per"); // Discount Per
        $txt .= $this->encodeCSV("Delivery cost UOM"); // Discount UoM

        $txt .= $this->encodeCSV("N/A"); // N/A
        $txt .= $this->encodeCSV("Plant"); // Plant
        $txt .= $this->encodeCSV("Storage Location"); // Storage Location
        $txt .= $this->encodeCSV("Batch number"); // Batch Number
        $txt .= $this->encodeCSV("WBS"); // WBS
        $txt .= "\r\n";

        return $txt;
    }

    private function getOutboundLocalPath() 
    {
        $dir = dirname(__FILE__)."/outbound/";
        return $dir;
    }

    private function encodeCSV($val) {
        //$val = str_replace('"','""',$val);
        //return '"'.$val.'",';
        return $val. ',';
    }

    private function sendERPMail($batchFileName, $batchReplacementFileName, $erpRecipients) 
    {
        if($batchFileName != "" || $batchReplacementFileName != "") 
        {
            $recipient = $erpRecipients;
            $recipient = explode(',', $recipient);
            $outboundDir = $this->getOutboundLocalPath();
            $attachments = array();

            if($batchFileName != "") {
                $file = $outboundDir.$batchFileName;            
                $attach = array();
                $attach['content'] = file_get_contents($file);
                $attach['name'] = $batchFileName;
                $attach['mime'] = 'application/csv';

                $attachments[] = $attach;
            }

            if($batchReplacementFileName != "") {
                $replacementFile = $outboundDir.$batchReplacementFileName;
                $attach = array();
                $attach['content'] = file_get_contents($replacementFile);
                $attach['name'] = $batchReplacementFileName;
                $attach['mime'] = 'application/csv';

                $attachments[] = $attach;
            }

            $success = Mail::send(
                (int)Configuration::get('PS_LANG_DEFAULT'),
                'erp_order',
                Mail::l('Order export file for ERP (attachment)', (int)Configuration::get('PS_LANG_DEFAULT')),
                array(),
                $recipient,
                null, null, null,
                $attachments,
                null, _MODULE_DIR_.'notificationjob/mails/', false, 1
            );

            return $success;
        }

        return false;
    }
}
