<?php

// ~/module/notificationjob/synccase
class NotificationJobGenerateBirthdayVoucherModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        print_r("Start generate birthday voucher<br />");
        
        $generateEnabled = Configuration::get('BV_GENERATE_CRON');
        
        if($generateEnabled == "1") 
        {
            print_r("Generate birthday voucher enabled<br />");
            $goldenRuleId = Configuration::get('BV_GOLDEN_RULE');
            $voucherSameBirthdate = Configuration::get('BV_START_SAME_DATE');
            $voucherAvailableDay = Configuration::get('BV_EXPIRY_DAYS');
            $goldenRule = new CartRule($goldenRuleId);
            
            if(Validate::isLoadedObject($goldenRule))
            {            
                print_r("Golden rule valid<br />");
                $result = Db::getInstance()->executeS('SELECT id_customer, email, firstname, lastname, birthday, id_lang, allowEmail FROM `'._DB_PREFIX_.'customer` WHERE MONTH(birthday)='.date('m'));
                
                if($result) {
                    $prefix = $goldenRule->code;
                    $oriName = $goldenRule->name;
                    
                    if($goldenRule->reduction_amount > 0) {
                        $reduction_amount = Tools::displayPrice($goldenRule->reduction_amount, (int)$goldenRule->reduction_currency, false);
                    }
                    else {
                        $reduction_amount = $goldenRule->reduction_percent.'%';
                    }

                    foreach($result AS $r) {
                        $voucherCode = $prefix.date('Y').$r['id_customer'].date('m');
                        $sql = "SELECT COUNT(1) FROM `"._DB_PREFIX_."cart_rule` WHERE code='".$voucherCode."' AND id_customer=".(int)$r['id_customer'];
                        $existingResult = Db::getInstance()->getValue($sql);

                        if($existingResult == 0) {
                            print_r("Generate voucher for ".$r['id_customer']."<br />");
                            $goldenRule->id = 0;
                        
                            $goldenRule->code = $voucherCode;
                            $goldenRule->id_customer = $r['id_customer']; 
                            $goldenRule->active = 1;
                        
                            if($voucherSameBirthdate == 1) {
                                $goldenRule->date_from = date('Y').'-'.date('m-d',strtotime($r['birthday']));
                                $goldenRule->date_to = date('Y-m-d', strtotime($goldenRule->date_from. ' + '.$voucherAvailableDay.' days'));
                            }
                            else {
                                $goldenRule->date_from = date('Y-m-01');
                                $goldenRule->date_to = date('Y-m-07', strtotime($goldenRule->date_from. ' +1 month'));
                            }
                        
                        
                            $languages = Language::getLanguages();
                            foreach ($languages AS $language)
		                    {
			                    $goldenRule->name[intval($language['id_lang'])] = $oriName[intval($language['id_lang'])]." - ". $r['firstname'].' '.$r['lastname'];
		                    }
                        
                            $goldenRule->add();                     
                            CartRule::copyConditions($goldenRuleId, $goldenRule->id);
                            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'cart_rule_combination` WHERE id_cart_rule_1='.(int)$goldenRule->id.' OR id_cart_rule_2='.(int)$goldenRule->id);

                            if($r['allowEmail'] == 1) {
                                $params = array(
                                    '{voucher_amount}' => $reduction_amount,
                                    '{voucher_num}' => $goldenRule->code,
                                    '{firstname}' => $r['firstname'],
                                    '{lastname}' => $r['lastname']
                                );
                        
                                $mail_res = Mail::Send(
                                    (int)$r['id_lang'],
                                    'birthday_voucher',
                                    Mail::l('A gift for you', (int)$r['id_lang']),
                                    $params,
                                    $r['email'],
                                    $r['firstname'].' '.$r['lastname'],
                                    null, null, null, null, _MODULE_DIR_.'notificationjob/mails/', false, null
                                );
                        
                                if($mail_res) {
                                    print_r("send mail success");
                                    print_r("<br />");
                                }
                                else {
                                    print_r("send mail failed");
                                    print_r("<br />");
                                }
                            }
                        
                            print_r($goldenRule->id);
                            print_r('||');
                            print_r($goldenRule->code);
                            print_r("<br />");
                        }
                    }
                }
            }
        }
        
        print_r("<br />End generate");
        die();
    }
}