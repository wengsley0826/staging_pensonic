<?php

// ~/module/notificationjob/productstock
class NotificationJobProductStockModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
    print_r("Start product stock checking");
           $recipient = Configuration::get('NJ_PRODUCT_MAIL');
           $productstock = Configuration::get('NJ_PRODUCT_MIN_QTY');
           
           $sql = "SELECT A.id_product,A.reference,B.name, C.quantity".
                " FROM `"._DB_PREFIX_."product` A".
                " INNER JOIN `"._DB_PREFIX_."product_lang` B ON A.id_product = B.id_product".
                " INNER JOIN `"._DB_PREFIX_."stock_available` C ON A.id_product = C.id_product".
                " WHERE A.active = 1 AND A.available_for_order=1 AND C.id_product_attribute = 0 AND C.quantity<".(int)$productstock;
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

            if ($result && trim($recipient) != "") {
                $products = 
                    '<table class="table table-recap" bgcolor="#ffffff" style="width: 100%; border-collapse: collapse;">'.
                        '<tr>'.
                            '<th bgcolor="#f8f8f8" style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;">Reference</th>'.
                            '<th bgcolor="#f8f8f8" style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;">Product</th>'.
                            '<th bgcolor="#f8f8f8" style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;">Quantity</th>'.
                        '</tr>';
                $products_txt = "";
                
                
                foreach($result as $r) {
                    $products .= "<tr>";
                    $products .= "<td style='border: 1px solid #D6D4D4;'>".$r['reference']."</td>";
                    $products .= "<td style='border: 1px solid #D6D4D4;'>".$r['name']."</td>";
                    $products .= "<td style='border: 1px solid #D6D4D4;text-align:center'>".$r['quantity']."</td>";
                    $products .= "</tr>";
                    
                    $products_txt .= $r['name']."(".$r['reference'].") => ".$r['quantity']."\r\n";
                }
                
                $products.='</table>';
                
                $recipient = explode(',', $recipient);
                
                $sendResult = Mail::Send(
                    1, 'notify_product_stock', Mail::l('Product Stock Low', 1), 
                    array('{products}' => $products, '{products_txt}' => $products_txt), $recipient, null, 
                    null, null, null, 
                    null, _MODULE_DIR_.'notificationjob/mails/', false, 
                    1);
                    
                print_r("send result:");
                if($sendResult) {  
                    print_r("Success");
                }
                else {
                    print_r("Failed");
                }
            }
            
            print_r("<br />End product stock checking");
            die();
    }
}