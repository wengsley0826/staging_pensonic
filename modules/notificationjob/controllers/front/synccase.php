<?php

// ~/module/notificationjob/synccase
class NotificationJobSyncCaseModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        print_r("Start Sync<br />");
        $result = CustomerThread::syncIMAP();
        
        if($result['hasError']) {
            print_r("Sync error<br />");
            print_r($result['errors']);
        }
        else {
            print_r("Sync success");
        }
        
        print_r("<br />End Sync");
        exit;
    }
}