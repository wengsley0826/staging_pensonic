[/{shop_url}] 

Hi {firstname} {lastname}, 

In celebration of your brithday this month, please enjoy {voucher_amount} off your next purchase by applying voucher code {voucher_num} upon checkout as you pay for your order.

Voucher code expires on the last day of your birthday month.


This email is computer generated, do not reply to this email.