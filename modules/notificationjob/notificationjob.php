<?php

class NotificationJob extends Module
{
    protected $_html = '';
    protected $_postErrors = array();

    public function __construct()
    {
        if (!defined('_PS_VERSION_')) {
            exit;
        }
        $this->name = 'notificationjob';
        $this->tab = 'analytics_stats';
        $this->version = '1.0.0';
        $this->author = 'Claritas';
        $this->displayName = 'Notification Job';
        $this->bootstrap = true;
        $this->display = 'view';
        $this->is_eu_compatible = 1;
        
        parent::__construct();

        $this->description = $this->l('Notificatoin job setting');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

    }

    public function install()
    {
        return parent::install() && $this->registerHook('actionValidateOrder');
    }

    public function uninstall()
    {
        return parent::uninstall() && $this->unregisterHook('actionValidateOrder');
    }

    protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('NJ_PRODUCT_MIN_QTY')) {
                $this->_postErrors[] = $this->l('Product Mininum Quantity is required!');
            }
            if (!Tools::getValue('NJ_PRODUCT_MAIL')) {
                $this->_postErrors[] = $this->l('Recipient (Product Stock) is required!');
            }

            //if (!Tools::getValue('NJ_ORDER_MAIL')) {
            //    $this->_postErrors[] = $this->l('Recipient (Order Notification) is required!');
            //}
            
            //if (Tools::getValue('NJ_ORDER_MAX_AMT')=="" || Tools::getValue('NJ_ORDER_MAX_AMT') == null) {
            //    $this->_postErrors[] = $this->l('Order Maximum Amount (SGD) is required!');
            //}
            //else if (!Validate::isFloat(Tools::getValue('NJ_ORDER_MAX_AMT')) || (float)(Tools::getValue('NJ_ORDER_MAX_AMT')) < 0) {
            //    $this->_postErrors[] = $this->l('Order Maximum Amount (SGD) must be positive currency!');
            //}
            
            if (!Tools::getValue('BV_GOLDEN_RULE')) {
                $this->_postErrors[] = $this->l('Birthday voucher rule is required!');
            }
            
            if (!Tools::getValue('BV_EXPIRY_DAYS')) {
                $this->_postErrors[] = $this->l('Birthday voucher available days is required!');
            }
            else if (!Validate::isInt(Tools::getValue('BV_EXPIRY_DAYS')) || (int)(Tools::getValue('BV_EXPIRY_DAYS')) <= 0) {
                $this->_postErrors[] = $this->l('Birthday voucher available days must be positive integer number!');
            }

            //if (!Tools::getValue('NJ_ERP_MAIL')) {
            //    $this->_postErrors[] = $this->l('Recipient (ERP) is required!');
            //}
            //if (!Tools::getValue('NJ_ERP_STATUS')) {
            //    $this->_postErrors[] = $this->l('Order Status (ERP) is required!');
            //}

            //if (!Tools::getValue('NJ_ERP_PLANT')) {
            //    $this->_postErrors[] = $this->l('Plant (ERP) is required!');
            //}
            //if (!Tools::getValue('NJ_ERP_STORELOC')) {
            //    $this->_postErrors[] = $this->l('Storage Location (ERP) is required!');
            //}
            //if (!Tools::getValue('NJ_ERP_CUSTACC')) {
            //    $this->_postErrors[] = $this->l('Customer Account (ERP) is required!');
            //}
        }
	}
    
    protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('NJ_PRODUCT_MIN_QTY', Tools::getValue('NJ_PRODUCT_MIN_QTY'));
            Configuration::updateValue('NJ_PRODUCT_MAIL', Tools::getValue('NJ_PRODUCT_MAIL'));
            
            Configuration::updateValue('NJ_ADMIN_MAIL', Tools::getValue('NJ_ADMIN_MAIL'));
            Configuration::updateValue('NJ_BCC_MAIL', Tools::getValue('NJ_BCC_MAIL'));
            //Configuration::updateValue('NJ_ORDER_MAIL', Tools::getValue('NJ_ORDER_MAIL'));
            //Configuration::updateValue('NJ_ORDER_MAX_AMT', Tools::getValue('NJ_ORDER_MAX_AMT'));

            Configuration::updateValue('BV_GENERATE_CRON', Tools::getValue('BV_GENERATE_CRON'));
            Configuration::updateValue('BV_GOLDEN_RULE', Tools::getValue('BV_GOLDEN_RULE'));
            Configuration::updateValue('BV_START_SAME_DATE', Tools::getValue('BV_START_SAME_DATE'));
            Configuration::updateValue('BV_EXPIRY_DAYS', Tools::getValue('BV_EXPIRY_DAYS'));

            //Configuration::updateValue('NJ_ERP_CRON', Tools::getValue('NJ_ERP_CRON'));
            //Configuration::updateValue('NJ_ERP_MAIL', Tools::getValue('NJ_ERP_MAIL'));
            //Configuration::updateValue('NJ_ERP_STATUS', Tools::getValue('NJ_ERP_STATUS'));

            //Configuration::updateValue('NJ_ERP_PLANT', Tools::getValue('NJ_ERP_PLANT'));
            //Configuration::updateValue('NJ_ERP_STORELOC', Tools::getValue('NJ_ERP_STORELOC'));
            //Configuration::updateValue('NJ_ERP_CUSTACC', Tools::getValue('NJ_ERP_CUSTACC'));
        }
        
        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}
    
    public function getContent()
    {
       if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
    }

    public function renderForm($message = null)
    {
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Product Out of Stock Notification'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Recipient email'),
                    'name' => 'NJ_PRODUCT_MAIL',
                    'required' => true,
                    'desc' => $this->l('Seperate by comma.')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Notify when quantity less than'),
                    'name' => 'NJ_PRODUCT_MIN_QTY',
                    'required' => true
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );
        
        //$fields_form[1]['form'] = array(
        //    'legend' => array(
        //        'title' => $this->l('New Order'),
        //    ),
        //    'input' => array(
        //        array(
        //            'type' => 'text',
        //            'label' => $this->l('Recipient email'),
        //            'name' => 'NJ_ORDER_MAIL',
        //            'required' => true,
        //            'desc' => $this->l('Seperate by comma.')
        //        ),
        //        array(
        //            'type' => 'text',
        //            'label' => $this->l('Order Maximum Amount'),
        //            'name' => 'NJ_ORDER_MAX_AMT',
        //            'required' => true,
        //            'desc' => $this->l('Amount in SGD.')
        //        ),
        //    ),
        //    'submit' => array(
        //        'title' => $this->l('Save')
        //    )
        //);
        
        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Birthday Automated Generation'),
            ),
            'input' => array(
                array(
                        'type' => 'switch',
                        'label' => $this->l('Auto generate birthday voucher'),
                        'name' => 'BV_GENERATE_CRON',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),                    
                array(
                    'type' => 'text',
                    'label' => $this->l('Birthday rule ID'),
                    'name' => 'BV_GOLDEN_RULE',
                    'required' => true,
                    'desc' => $this->l('The voucher ID for golden rule.')
                ),
                array(
                        'type' => 'switch',
                        'label' => $this->l('Voucher start on birthdate'),
                        'desc' => $this->l('Voucher available date start from the date of birthdate or at 1st of birthday month'),
                        'name' => 'BV_START_SAME_DATE',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),   
                array(
                    'type' => 'text',
                    'label' => $this->l('Voucher available days'),
                    'name' => 'BV_EXPIRY_DAYS',
                    'required' => true,
                    'desc' => $this->l('Number of days the voucher available after voucher generated.')
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );

        $fields_form[2]['form'] = array(
            'legend' => array(
                'title' => $this->l('Admin BCC Mail'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Admin email'),
                    'name' => 'NJ_ADMIN_MAIL',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('BCC email'),
                    'name' => 'NJ_BCC_MAIL',
                    'required' => true
                )
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );
         
        //$fields_form[3]['form'] = array(
        //    'legend' => array(
        //        'title' => $this->l('ERP Auto mailer'),
        //    ),
        //    'input' => array(
        //        array(
        //                'type' => 'switch',
        //                'label' => $this->l('Auto email order'),
        //                'name' => 'NJ_ERP_CRON',
        //                'required' => false,
        //                'is_bool' => true,
        //                'values' => array(
        //                    array(
        //                        'id' => 'active_on',
        //                        'value' => 1,
        //                        'label' => $this->l('Enabled')
        //                    ),
        //                    array(
        //                        'id' => 'active_off',
        //                        'value' => 0,
        //                        'label' => $this->l('Disabled')
        //                    )
        //                ),
        //            ),                    
        //        array(
        //            'type' => 'text',
        //            'label' => $this->l('Recipient email'),
        //            'name' => 'NJ_ERP_MAIL',
        //            'required' => true,
        //            'desc' => $this->l('Seperate by comma.')
        //        ),
        //        array(
        //            'type' => 'text',
        //            'label' => $this->l('Trigger on status'),
        //            'name' => 'NJ_ERP_STATUS',
        //            'required' => true,
        //            'desc' => $this->l('Seperate by comma.')
        //        ),
        //        array(
        //            'type' => 'text',
        //            'label' => $this->l('Plant'),
        //            'name' => 'NJ_ERP_PLANT',
        //            'required' => true,
        //        ),
        //        array(
        //            'type' => 'text',
        //            'label' => $this->l('Storage Location'),
        //            'name' => 'NJ_ERP_STORELOC',
        //            'required' => true,
        //        ),
        //        array(
        //            'type' => 'text',
        //            'label' => $this->l('Customer Account'),
        //            'name' => 'NJ_ERP_CUSTACC',
        //            'required' => true,
        //        ),
        //    ),
        //    'submit' => array(
        //        'title' => $this->l('Save')
        //    )
        //);

        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm($fields_form);
    }
    
    protected function getOrderStatuses() {
        $allstatuses = Db::getInstance()->ExecuteS('SELECT DISTINCT A.id_order_state, B.name AS status_name'.
                    ' FROM '._DB_PREFIX_.'order_state A'.
                    ' INNER JOIN '._DB_PREFIX_.'order_state_lang B ON A.id_order_state = B.id_order_state'.
                    ' WHERE B.id_lang = 1'.
                    ' ORDER BY B.name'
		);
        
        $status_list = array();
        
        foreach ($allstatuses as $status)
        {
            $status_list[] = array(
                "id_order_state" => $status["id_order_state"],
                "name" => $status["status_name"]
            );
        }
        
        return $status_list;
    }
    
    public function getConfigFieldsValues()
	{
		return array(
			'NJ_PRODUCT_MIN_QTY' => Tools::getValue('NJ_PRODUCT_MIN_QTY', Configuration::get('NJ_PRODUCT_MIN_QTY')),
            'NJ_PRODUCT_MAIL' => Tools::getValue('NJ_PRODUCT_MAIL', Configuration::get('NJ_PRODUCT_MAIL')),
            //'NJ_ORDER_MAIL' => Tools::getValue('NJ_ORDER_MAIL', Configuration::get('NJ_ORDER_MAIL')),
            //'NJ_ORDER_MAX_AMT' => Tools::getValue('NJ_ORDER_MAX_AMT', Configuration::get('NJ_ORDER_MAX_AMT')),
            
            
            'BV_GENERATE_CRON' => Tools::getValue('BV_GENERATE_CRON', Configuration::get('BV_GENERATE_CRON')),
            'BV_GOLDEN_RULE' => Tools::getValue('BV_GOLDEN_RULE', Configuration::get('BV_GOLDEN_RULE')),
            'BV_START_SAME_DATE' => Tools::getValue('BV_START_SAME_DATE', Configuration::get('BV_START_SAME_DATE')),
            'BV_EXPIRY_DAYS' => Tools::getValue('BV_EXPIRY_DAYS', Configuration::get('BV_EXPIRY_DAYS')),

            'NJ_ADMIN_MAIL' => Tools::getValue('NJ_ADMIN_MAIL', Configuration::get('NJ_ADMIN_MAIL')),
            'NJ_BCC_MAIL' => Tools::getValue('NJ_BCC_MAIL', Configuration::get('NJ_BCC_MAIL')),

            //'NJ_ERP_CRON' => Tools::getValue('NJ_ERP_CRON', Configuration::get('NJ_ERP_CRON')),
            //'NJ_ERP_MAIL' => Tools::getValue('NJ_ERP_MAIL', Configuration::get('NJ_ERP_MAIL')),
            //'NJ_ERP_STATUS' => Tools::getValue('NJ_ERP_STATUS', Configuration::get('NJ_ERP_STATUS')),
            //'NJ_ERP_PLANT' => Tools::getValue('NJ_ERP_PLANT', Configuration::get('NJ_ERP_PLANT')),
            //'NJ_ERP_STORELOC' => Tools::getValue('NJ_ERP_STORELOC', Configuration::get('NJ_ERP_STORELOC')),
            //'NJ_ERP_CUSTACC' => Tools::getValue('NJ_ERP_CUSTACC', Configuration::get('NJ_ERP_CUSTACC')),
		);
	}
    
    public function hookActionValidateOrder($params) 
    {
        $recipient = Configuration::get('NJ_PRODUCT_MAIL');
        
        if(trim($recipient) != "" && $params['order']->module=="cybersource") {
            $recipient = explode(',', $recipient);
            
            $orderProducts = $params['order']->getProductsDetail();
            
            $products = 
                    '<table class="table table-recap" bgcolor="#ffffff" style="width: 100%; border-collapse: collapse;">'.
                        '<tr>'.
                            '<th bgcolor="#f8f8f8" style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;">Reference</th>'.
                            '<th bgcolor="#f8f8f8" style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;">Product</th>'.
                            '<th bgcolor="#f8f8f8" style="border: 1px solid #D6D4D4; background-color: #fbfbfb; color: #333; font-family: Arial; font-size: 13px; padding: 10px;">Quantity</th>'.
                        '</tr>';
                        
             foreach($orderProducts as $r) {
                $products .= "<tr>";
                $products .= "<td style='border: 1px solid #D6D4D4;'>".$r['reference']."</td>";
                $products .= "<td style='border: 1px solid #D6D4D4;'>".$r['product_name']."</td>";
                $products .= "<td style='border: 1px solid #D6D4D4;text-align:center'>".$r['product_quantity']."</td>";
                $products .= "</tr>";
            }
            
            $products.='</table>';
             
            $template_vars = array(
                '{reference}' => $params['order']->reference,
                '{firstname}' => $params['customer']->firstname,
                '{lastname}'  => $params['customer']->lastname,
                '{customer_email}' => $params['customer']->email,
                '{products}' =>$products
            );
            
            $sendResult = Mail::Send(
                    $this->context->language->id, 'notify_order', Mail::l('New Order Created', $this->context->language->id), 
                    $template_vars, $recipient, null, 
                    null, null, null, 
                    null, _MODULE_DIR_.'notificationjob/mails/', false, 
                    $this->context->shop->id);
        }    
    }
}
