<?php

if (!defined('_PS_VERSION_'))
	exit;

class OfflineCreditCard extends PaymentModule
{
	protected $_html = '';
	protected $_postErrors = array();

	public $details;
	public $owner;
	public $address;
	public function __construct()
	{
		$this->name = 'offlinecreditcard';
		$this->tab = 'payments_gateways';
		$this->version = '1.0.0';
		$this->author = 'Claritas';
		$this->controllers = array('payment', 'validation');
		$this->is_eu_compatible = 1;

		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		$config = Configuration::getMultiple(array('OFFLINECC_DETAILS', 'OFFLINECC_OWNER', 'OFFLINECC_ADDRESS'));
		if (!empty($config['OFFLINECC_OWNER']))
			$this->owner = $config['OFFLINECC_OWNER'];
		if (!empty($config['OFFLINECC_DETAILS']))
			$this->details = $config['OFFLINECC_DETAILS'];
		if (!empty($config['OFFLINECC_ADDRESS']))
			$this->address = $config['OFFLINECC_ADDRESS'];

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Manual Credit Card');
		$this->description = $this->l('Accept payments for your products via credit card machine.');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.99.99');

		if (!isset($this->owner) || !isset($this->details) || !isset($this->address))
			$this->warning = $this->l('Account owner and account details must be configured before using this module.');
		if (!count(Currency::checkPaymentCurrencies($this->id)))
			$this->warning = $this->l('No currency has been set for this module.');
	}

	public function install()
	{
		if (!parent::install() || ! $this->registerHook('displayPaymentEU'))
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!Configuration::deleteByName('OFFLINECC_DETAILS')
				|| !Configuration::deleteByName('OFFLINECC_OWNER')
				|| !Configuration::deleteByName('OFFLINECC_ADDRESS')
				|| !parent::uninstall())
			return false;
		return true;
	}

	protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			if (!Tools::getValue('OFFLINECC_DETAILS'))
				$this->_postErrors[] = $this->l('Account details are required.');
			elseif (!Tools::getValue('OFFLINECC_OWNER'))
				$this->_postErrors[] = $this->l('Account owner is required.');
		}
	}

	protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			Configuration::updateValue('OFFLINECC_DETAILS', Tools::getValue('OFFLINECC_DETAILS'));
			Configuration::updateValue('OFFLINECC_OWNER', Tools::getValue('OFFLINECC_OWNER'));
			Configuration::updateValue('OFFLINECC_ADDRESS', Tools::getValue('OFFLINECC_ADDRESS'));
		}
		$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}

	public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
	}

    public function hookPayment($params)
	{
		if (!$this->active)
			return;
		if (!$this->checkCurrency($params['cart']))
			return;

		$this->smarty->assign(array(
			'this_path' => $this->_path,
			'this_path_bw' => $this->_path,
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));
		return $this->display(__FILE__, 'payment.tpl');
	}
    
	public function hookDisplayPaymentEU($params)
	{
		if (!$this->active)
			return;

		if (!$this->checkCurrency($params['cart']))
			return;

		$payment_options = array(
			'cta_text' => $this->l('Pay by Credit Card'),
			'logo' => Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/offlinecreditcard.jpg'),
			'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true)
		);

		return $payment_options;
	}

	public function checkCurrency($cart)
	{
		$currency_order = new Currency($cart->id_currency);
		$currencies_module = $this->getCurrency($cart->id_currency);

		if (is_array($currencies_module))
			foreach ($currencies_module as $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}

	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Contact details'),
					'icon' => 'icon-envelope'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Account owner'),
						'name' => 'OFFLINECC_OWNER',
						'required' => true
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Details'),
						'name' => 'OFFLINECC_DETAILS',
						'desc' => $this->l('Such as bank branch, IBAN number, BIC, etc.'),
						'required' => true
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Bank address'),
						'name' => 'OFFLINECC_ADDRESS',
						'required' => true
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'btnSubmit';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'OFFLINECC_DETAILS' => Tools::getValue('OFFLINECC_DETAILS', Configuration::get('OFFLINECC_DETAILS')),
			'OFFLINECC_OWNER' => Tools::getValue('OFFLINECC_OWNER', Configuration::get('OFFLINECC_OWNER')),
			'OFFLINECC_ADDRESS' => Tools::getValue('OFFLINECC_ADDRESS', Configuration::get('OFFLINECC_ADDRESS')),
		);
	}
}
