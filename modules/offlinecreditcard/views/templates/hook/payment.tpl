<p class="payment_module">
	<a href="{$link->getModuleLink('offlinecreditcard', 'payment')|escape:'html'}" title="{l s='Pay by credit card machine' mod='offlinecreditcard'}">
		<img src="{$this_path_bw}offlinecreditcard.jpg" alt="{l s='Pay by credit card machine' mod='offlinecreditcard'}" width="86" height="49"/>
		{l s='Pay by bank credit card machine' mod='offlinecreditcard'}&nbsp;<span>{l s='(order processing will be longer)' mod='offlinecreditcard'}</span>
	</a>
</p>