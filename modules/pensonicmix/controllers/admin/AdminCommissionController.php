<?php

require_once _PS_MODULE_DIR_ . 'pensonicmix/models/Commission.php';

class AdminCommissionController extends ModuleAdminController
{
	public function __construct()
	{
		$this->bootstrap  = true;
        $this->table      = 'commission';
        $this->identifier = 'commission_id';
        $this->className  = 'Commission';
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->context = Context::getContext();
		
		$this->bulk_actions = array(
		);
		
		parent::__construct();
		
		$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'category_lang` c ON (a.`commission_category` = c.`id_category`) AND c.id_lang=1 ';
		$this->_select .= ' a.*, c.name AS category_name ';
		
		$this->fields_list = array();
		$this->fields_list['commission_id'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'type' => 'int',
		);
		
		$this->fields_list['affiliate_code'] = array(
			'title' => $this->l('Affiliate Code'),
			'align' => 'center'
		);
		
		$this->fields_list['affiliate_name'] = array(
			'title' => $this->l('Affiliate Name'),
			'align' => 'center',
			'width' => '250',
		);
		
		
		$this->fields_list['commission_rate'] = array(
			'title' => $this->l('Commission%'),
			'align' => 'center',
			'align' => 'text-right'
		);
		
		$this->fields_list['commission_max'] = array(
			'title' => $this->l('Max Commission'),
			'align' => 'center',
			'align' => 'text-right',
			'type' => 'price'
		);
		
		$this->fields_list['category_name'] = array(
			'title' => $this->l('Linked Category'),
			'align' => 'center'
		);
		
		$this->fields_list['hideLink'] = array(
			'title' => $this->l('Is Hidden'),
			'align' => 'center'
		);
		
		
		$this->fields_list['create_date'] = array(
			'title' => $this->l('Created'),
			'align' => 'center',
			'type' => 'date'
		);
		
		
		
		parent::__construct();
	}
	
	 public function createTemplate($tpl_name) {
        if (file_exists($this->getTemplatePath().$tpl_name) && $this->viewAccess())
                return $this->context->smarty->createTemplate($this->getTemplatePath() . $tpl_name, $this->context->smarty);
			
            return parent::createTemplate($tpl_name);
    }
	
	public function renderForm(){
		$current_object = $this->loadObject(true);
        $commission_id = Tools::getValue('commission_id');
        $commission = new Commission($commission_id);
		
		$category= Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT c.`id_category`, cl.`name`
			FROM `'._DB_PREFIX_.'category` c
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
			'.Shop::addSqlAssociation('category', 'c').'
			WHERE cl.`id_lang` = 1
			AND c.`id_category` != '.Configuration::get('PS_ROOT_CATEGORY').'
			GROUP BY c.id_category
			ORDER BY cl.`name`');
		
        $this->context->smarty->assign(
            array(
                'show_toolbar' => true,
                'toolbar_btn' => $this->toolbar_btn,
                'toolbar_scroll' => $this->toolbar_scroll,
                'currentIndex' => self::$currentIndex,
			    'currentToken' => $this->token,
				'commission' => $commission,
				'category' => $category,
			    'currentObject' => $current_object,
				)
			);
	
        $this->content .= $this->createTemplate('commission.tpl')->fetch();
		
        return parent::renderForm();	
	}
	
    
}
