<?php

require_once _PS_MODULE_DIR_ . 'pensonicmix/models/Commission.php';

class AdminCommissionOrderListController extends ModuleAdminController
{
	public function __construct()
	{
		$this->bootstrap  = true;
        $this->table      = 'order_pixel';
        $this->identifier = 'id_order_pixel';
		$this->context = Context::getContext();
		
		$this->bulk_actions = array(
		);
		
		parent::__construct();
		
		$this->_join .= ' JOIN `'._DB_PREFIX_.'orders` b ON (a.`id_order` = b.`id_order`)
		JOIN `'._DB_PREFIX_.'commission` c ON a.utm_source = c.affiliate_code
		';
		$this->_select = ' a.id_order_pixel, b.reference, a.utm_source, c.affiliate_name, b.total_paid, a.commission_value, ROUND(a.commission_value*100/b.total_paid) AS commission_percent, b.date_add  ';
		
		$this->fields_list = array();
		$this->fields_list['id_order_pixel'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'type' => 'int',
		);
		
		$this->fields_list['reference'] = array(
			'title' => $this->l('reference Code'),
			'align' => 'center'
		);
		
		$this->fields_list['utm_source'] = array(
			'title' => $this->l('Source'),
			'align' => 'center',
			'width' => '250',
		);
		
		
		$this->fields_list['affiliate_name'] = array(
			'title' => $this->l('Affiliate Name'),
			'align' => 'center',
			'align' => 'text-right',
		);
		
		$this->fields_list['total_paid'] = array(
			'title' => $this->l('Order Paid Amt'),
			'align' => 'center',
			'align' => 'text-right',
			'type' => 'price'
		);
		
		$this->fields_list['commission_value'] = array(
			'title' => $this->l('Commission Amt'),
			'align' => 'center',
			'align' => 'text-right',
			'type' => 'price'
		);
		
		
		$this->fields_list['date_add'] = array(
			'title' => $this->l('Created'),
			'align' => 'center',
			'type' => 'date'
		);
		
		
		
		parent::__construct();
	}
	
}
