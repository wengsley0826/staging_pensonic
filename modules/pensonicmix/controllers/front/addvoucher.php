<?php

// ~/module/pensonicmix/addvoucher
class PensonicMixAddVoucherModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        
        die();
    }

    public function postProcess() {
        $crm_id = $_POST['crm_id'];
        $free_shipping = $_POST['free_shipping'];
        $discount_value = $_POST['discount_value'];
        $discount_type = $_POST['discount_type'];
        $minimum_amount = $_POST['minimum_amount'];
        $quantity = $_POST['quantity'];
        $cap_amount = $_POST['cap_amount'];
        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $voucher_code = $_POST['voucher_code'];
        $voucher_name = $_POST['voucher_name'];

        $result = array();

        if($free_shipping != 0 && $free_shipping != 1) {
            $result['r'] = false;
            $result['d'] = "";
            $result['m'] = "Invalid Free Shipping value";

            echo json_encode($result);
            die();
        }
        else if ($discount_type != "P" && $discount_type != "A" && $discount_type != "") {
            $result['r'] = false;
            $result['d'] = "";
            $result['m'] = "Invalid discount type";

            echo json_encode($result);
            die();
        }
        else if ($quantity <= 0) {
            $result['r'] = false;
            $result['d'] = "";
            $result['m'] = "Invalid quantity";

            echo json_encode($result);
            die();
        }

        if($voucher_code != "") {
            $voucherCodeResult = Db::getInstance()->getRow('SELECT id_cart_rule FROM `'._DB_PREFIX_.'cart_rule` WHERE `code` = "'.pSQL($voucher_code).'"');

            if($voucherCodeResult && $voucherCodeResult["id_cart_rule"] > 0) {
                $result['r'] = false;
                $result['d'] = "";
                $result['m'] = "Same voucher code exists";

                echo json_encode($result);
                die();
            }
        }
        else {
            $voucher_code = Tools::passwdGen();
            $voucherCodeResult = Db::getInstance()->getRow('SELECT id_cart_rule FROM `'._DB_PREFIX_.'cart_rule` WHERE `code` = "'.pSQL($voucher_code).'"');

            while($voucherCodeResult && $voucherCodeResult["id_cart_rule"] > 0) {
                $voucher_code = Tools::passwdGen();
                $voucherCodeResult = Db::getInstance()->getRow('SELECT id_cart_rule FROM `'._DB_PREFIX_.'cart_rule` WHERE `code` = "'.pSQL($voucher_code).'"');
            }
        }

        //check user id exists
        $customerResult = Db::getInstance()->getRow('SELECT id_customer FROM `'._DB_PREFIX_.'customer` WHERE `crm_id` = "'.pSQL($crm_id).'"');
        
        $customerId = 0;
        $email = '';

        if($customerResult && $customerResult['id_customer'] > 0) {
            $customerId = $customerResult['id_customer'];
        }
        else {
            $token = $this->GetCRMAPIToken();

            if($token) {
                $crmuser = $this->GetCRMUSer($crm_id, $token);

                if($crmuser && $crmuser['success']) {
                    //$result = $this->SignupUser($crmuser['data'], $token);

                    //if($result['r']) {
                    //    $customerResult['id_customer'] = $result['d'];
                    //    $customerId = $customerResult['id_customer'];
                    //}

                    $email = $crmuser['data']["Email"];
                }
            }
        }

        if($customerId > 0 || $email != "") {
            $cartRule = new CartRule();
            $cartRule->code = strtoupper($voucher_code);

            if($customerId > 0)
    		    $cartRule->id_customer = $customerId;
            else
                $cartRule->email = $email;

            $cartRule->date_from = date($date_from);
		    $cartRule->date_to = date($date_to);

            $cartRule->quantity = $quantity;
		    $cartRule->quantity_per_user = $quantity;
		    $cartRule->partial_use = 0;
		    $cartRule->highlight = 0;
		    $cartRule->active = 1;

            if($discount_type == "A") {
    		    $cartRule->reduction_amount = $discount_value;
                $cartRule->reduction_percent = 0;
                $cartRule->reduction_cap = 0;
            }
            else if($discount_type == "P") 
            {
                $cartRule->reduction_amount = 0;
                $cartRule->reduction_percent = $discount_value;
                $cartRule->reduction_cap = $cap_amount;
            }
            else {
                $cartRule->reduction_amount = 0;
                $cartRule->reduction_percent = 0;
                $cartRule->reduction_cap = 0;
            }

            $cartRule->free_shipping = $free_shipping;

		    $cartRule->reduction_tax = 1;
		    $cartRule->reduction_currency = 1;


		    $cartRule->minimum_amount = $minimum_amount;
		    $cartRule->minimum_amount_tax = 1;
		    $cartRule->minimum_amount_currency = 1;
		    $cartRule->minimum_amount_shipping = 0;
		    $cartRule->cart_rule_restriction = 1;

            $languages = Language::getLanguages(true);
		    foreach ($languages AS $language)
		    {
			    $cartRule->name[(int)($language['id_lang'])] = $voucher_name;
		    }

            $cartRule->add();

            $result['r'] = true;
            $result['d'] = $cartRule->code;
            $result['m'] = "";
        }
        else {
            $result['r'] = false;
            $result['d'] = "";
            $result['m'] = "Invalid crm_id";
        }

        echo json_encode($result);
        die();
    }

    protected function GetCRMAPIToken() {
        $crmGetTokenURL = _CRM_API_URL_."v1.0/token";
        
        $restDataTS = date("Y-m-d\TH:i:s");
        $hashed = strtoupper(hash_hmac("sha256", _CRM_API_KEY_.$restDataTS, _CRM_API_SECRET_));
        $tokenheaders = array("client_id:"._CRM_API_KEY_, "t:".$restDataTS, "sign:".$hashed);
        
        $result = Tools::FetchURL($crmGetTokenURL, null, $tokenheaders, "GET");
        $result = json_decode($result, true);

        if($result && $result["success"]) {
            return $result["result"]["access_token"];
        } 
        
        return false;
    }

    protected function GetCRMUSer($userId, $crmtoken) {
        $crmGetUserURL = _CRM_API_URL_."GetUserDetail";

        $request = array("RecordId" => $userId);
        $request = json_encode($request);

        $restDataTS = date("Y-m-d\TH:i:s");
        $hashed = strtoupper(hash_hmac("sha256", _CRM_API_KEY_.$crmtoken.$restDataTS, _CRM_API_SECRET_));
        $tokenheaders = array("client_id:"._CRM_API_KEY_, "t:".$restDataTS, "access_token:".$crmtoken, "sign:".$hashed);
        
        $result = Tools::FetchURL($crmGetUserURL, $request, $tokenheaders, "POST");

        return json_decode($result, true);
    }

    protected function SignupUser($userInfo, $token_result) {
        // Preparing customer
        $customer = new Customer();
        $lastname = "";
        $firstname = $userInfo['Name'];
        $_POST['lastname'] = $lastname;
        $_POST['firstname'] = $firstname;
        $customer->firstname = Tools::ucwords($firstname);
        $customer->passwd = Tools::encrypt("PensonicSSOEstore");
        $customer->crm_id = $userInfo["Id"];
        $customer->email = $userInfo["Email"];
        $customer->mobile = $userInfo["Phone"];
        $customer->birthday = date("Y-m-d", strtotime($userInfo["DOB"]));

        $customer->is_guest = 0;
        $customer->active = 1;
        $customer->rectSource = "ESTR";
        $customer->pdpa = 1;
        $customer->verified = 1;
        $customer->logged = 1;

        if($userInfo["Newsletter"] == 'Y') {
            $customer->newsletter = true;
            $customer->allowSMS = true;
            $customer->allowEmail = true;
            $customer->allowCall = true;
            $customer->allowMail = true;
        }

        $customer->verifyKey =  strtolower(Tools::passwdGen(9, 'NO_NUMERIC'));
        while(Customer::getVerifyKey($customer->verifyKey) > 0) {
            $customer->verifyKey =strtolower(Tools::passwdGen(9, 'NO_NUMERIC'));
        }

        if ($customer->add()) {
            $validAddress = false;

            $address = new Address();

            $countrySQL = 'SELECT `id_country`
		        FROM `'._DB_PREFIX_.'country`
		        WHERE `iso_code` = \''.pSQL($userInfo["Country"]).'\'';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($countrySQL);
         
            if (isset($result['id_country']))
            {
                $address->id_country = (int)$result['id_country'];

                $stateSQL = 'SELECT `id_state` FROM `'._DB_PREFIX_.'state`
		            WHERE `iso_code` = \''.pSQL($userInfo["State"]).'\' AND `id_country`='.$address->id_country;
                $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($stateSQL);
            
                if (isset($result['id_state']))
                {
                    $address->id_state = (int)$result['id_state'];

                    $validAddress = true;
                }
            }

            if($validAddress) {
                $address->id_manufacturer = 0;
                $address->id_supplier = 0;
                $address->id_warehouse = 0;
                $address->firstname = $customer->firstname;
                $address->lastname = $customer->lastname;
                $address->active = 1;

                $address->alias = "Default Address";
                $address->id_customer = $customer->id;
                $address->address1 = $userInfo["Address1"];
                $address->address2 = "";
                $address->postcode = $userInfo["Postcode"];
                $address->city = $userInfo["City"];
                $address->phone_mobile = $userInfo["Phone"];

                $addResult = $address->add(true, true);
            }
            
            Hook::exec('actionCustomerAccountAdd', array(
                    '_POST' => $_POST,
                    'newCustomer' => $customer
                ));

            $result['r'] = false;
            $result['m'] = "";
            $result['d'] = $customer->id;

        } else {
            $result['r'] = false;
            $result['m'] = "Unable to add customer";
        }

        return $result;
    }
}