<?php

// ~/module/pensonicmix/test
class PensonicMixExternalAuthModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        $code = Tools::getValue('code');

        if ($code) {
            $token_result = $this->GetAccessToken($code);

            if($token_result != null && isset($token_result["access_token"])) {
			    $accessToken = $token_result["access_token"];
			    $userInfo = $this->GetUserInfo($accessToken);

                //26599f29-8c2b-4f71-8fbb-801a69769420
                $crmId = $userInfo["sub"];
                $email = $userInfo["email"];
                
                $customer = new Customer();
                $customer = $customer->getByEmail($email);
                
                if($customer && $customer->id > 0) {
                    $this->SigninUser($customer, $token_result);
                }
                else {
                    $token = Tools::GetCRMAPIToken();

                    if($token) {
                        $crmuser = Tools::GetCRMUser($crmId, $token);

                        if($crmuser && $crmuser['success']) {
                            $this->SignupUser($crmuser['data'], $token_result);
                        }
                    }
                }
		    }
        }
    }

    protected function SigninUser($customer, $token_result) 
    {
        $this->context->cookie->id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare: CompareProduct::getIdCompareByIdCustomer($customer->id);
        $this->context->cookie->id_customer = (int)($customer->id);
        $this->context->cookie->customer_crmid = (int)$customer->crm_id;
        $this->context->cookie->customer_lastname = $customer->lastname;
        $this->context->cookie->customer_firstname = $customer->firstname;
        $this->context->cookie->logged = 1;
        $this->context->cookie->is_guest = $customer->isGuest();
        $this->context->cookie->passwd = $customer->passwd;
        $this->context->cookie->email = $customer->email;

        if(isset($token_result["id_token"])) {
            $this->context->cookie->signout_id_token = $token_result["id_token"];
	    }

        // Add customer to the context
        $this->context->customer = $customer;

        if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->context->cookie->id_cart) || Cart::getNbProducts($this->context->cookie->id_cart) == 0) && $id_cart = (int)Cart::lastNoneOrderedCart($this->context->customer->id)) {
            $this->context->cart = new Cart($id_cart);
        } else {
            $id_carrier = (int)$this->context->cart->id_carrier;
            $this->context->cart->id_carrier = 0;
            $this->context->cart->setDeliveryOption(null);
            $this->context->cart->id_address_delivery = (int)Address::getFirstCustomerAddressId((int)($customer->id));
            $this->context->cart->id_address_invoice = (int)Address::getFirstCustomerAddressId((int)($customer->id));
        }
        $this->context->cart->id_customer = (int)$customer->id;
        $this->context->cart->secure_key = $customer->secure_key;

        if ($this->ajax && isset($id_carrier) && $id_carrier && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
            $delivery_option = array($this->context->cart->id_address_delivery => $id_carrier.',');
            $this->context->cart->setDeliveryOption($delivery_option);
        }

        $this->context->cart->save();
        $this->context->cookie->id_cart = (int)$this->context->cart->id;
        $this->context->cookie->write();
        $this->context->cart->autosetProductAddress();

        Hook::exec('actionAuthentication', array('customer' => $this->context->customer));

        // Login information have changed, so we check if the cart rules still apply
        CartRule::autoRemoveFromCart($this->context);
        CartRule::autoAddToCart($this->context);

        if(Tools::getValue('app_redirect_uri') != "") {
        print_r("Redirect");
        print_r(Tools::getValue('app_redirect_uri'));
        die();
            Tools::redirect(Tools::getValue('app_redirect_uri'));
        }

        $back = Tools::getValue('back','my-account');

        if ($back == Tools::secureReferrer($back)) {
            Tools::redirect(html_entity_decode($back));
        }

        Tools::redirect('index.php?controller='.$back);

    }

    protected function SignupUser($userInfo, $token_result) {
        // Preparing customer
        $customer = new Customer();
        $lastname = "";
        $firstname = $userInfo['Name'];
        $_POST['lastname'] = $lastname;
        $_POST['firstname'] = $firstname;
        $customer->firstname = Tools::ucwords($firstname);
        $customer->passwd = Tools::encrypt("PensonicSSOEstore");
        $customer->crm_id = $userInfo["Id"];
        $customer->email = $userInfo["Email"];
        $customer->mobile = $userInfo["Phone"];
        $customer->birthday = date("Y-m-d", strtotime($userInfo["DOB"]));

        $customer->is_guest = 0;
        $customer->active = 1;
        $customer->rectSource = "ESTR";
        $customer->pdpa = 1;
        $customer->verified = 1;
        $customer->logged = 1;

        if($userInfo["Newsletter"] == 'Y') {
            $customer->newsletter = true;
            $customer->allowSMS = true;
            $customer->allowEmail = true;
            $customer->allowCall = true;
            $customer->allowMail = true;
        }

        $customer->verifyKey =  strtolower(Tools::passwdGen(9, 'NO_NUMERIC'));
        while(Customer::getVerifyKey($customer->verifyKey) > 0) {
            $customer->verifyKey =strtolower(Tools::passwdGen(9, 'NO_NUMERIC'));
        }

        if ($customer->add()) {
            $this->updateContext($customer);
            $validAddress = false;

            $address = new Address();

            $countrySQL = 'SELECT `id_country`
		        FROM `'._DB_PREFIX_.'country`
		        WHERE `iso_code` = \''.pSQL($userInfo["Country"]).'\'';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($countrySQL);
         
            if (isset($result['id_country']))
            {
                $address->id_country = (int)$result['id_country'];

                $stateSQL = 'SELECT `id_state` FROM `'._DB_PREFIX_.'state`
		            WHERE `iso_code` = \''.pSQL($userInfo["State"]).'\' AND `id_country`='.$address->id_country;
                $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($stateSQL);
            
                if (isset($result['id_state']))
                {
                    $address->id_state = (int)$result['id_state'];

                    $validAddress = true;
                }
            }

            if($userInfo["Address1"] == "") {
                $validAddress = false;
            }


            if($validAddress) {
                $address->id_manufacturer = 0;
                $address->id_supplier = 0;
                $address->id_warehouse = 0;
                $address->firstname = $customer->firstname;
                $address->lastname = $customer->lastname;
                $address->active = 1;

                $address->alias = "Default Address";
                $address->id_customer = $customer->id;
                $address->address1 = $userInfo["Address1"];
                $address->address2 = "";
                $address->postcode = $userInfo["Postcode"];
                $address->city = $userInfo["City"];
                $address->phone_mobile = $userInfo["Phone"];

                $addResult = $address->add(true, true);
            }
            
            $this->context->cart->update();
            Hook::exec('actionCustomerAccountAdd', array(
                    '_POST' => $_POST,
                    'newCustomer' => $customer
                ));
            if (($back = Tools::getValue('back')) && $back == Tools::secureReferrer($back)) {
                Tools::redirect(html_entity_decode($back));
            }

            // redirection: if cart is not empty : redirection to the cart
            if (count($this->context->cart->getProducts(true)) > 0) {
                $multi = (int)Tools::getValue('multi-shipping');
                Tools::redirect('index.php?controller=order'.($multi ? '&multi-shipping='.$multi : ''));
            }
            // else : redirection to the account
            else {
                Tools::redirect('index.php?controller=my-account');
            }
        } else {
            $this->errors[] = Tools::displayError('An error occurred while creating your account.');
        }
    }

    protected function updateContext(Customer $customer, $token_result)
    {
        $this->context->customer = $customer;
        $this->context->smarty->assign('confirmation', 1);
        $this->context->cookie->id_customer = (int)$customer->id;
        $this->context->cookie->customer_crmid = (int)$customer->crm_id;
        $this->context->cookie->customer_lastname = $customer->lastname;
        $this->context->cookie->customer_firstname = $customer->firstname;
        $this->context->cookie->passwd = $customer->passwd;
        $this->context->cookie->logged = 1;
        // if register process is in two steps, we display a message to confirm account creation
        if (!Configuration::get('PS_REGISTRATION_PROCESS_TYPE')) {
            $this->context->cookie->account_created = 1;
        }
        
        $this->context->cookie->email = $customer->email;
        $this->context->cookie->is_guest = !Tools::getValue('is_new_customer', 1);
        // Update cart address
        $this->context->cart->secure_key = $customer->secure_key;

        if(isset($token_result["id_token"])) {
            $this->context->cookie->signout_id_token = $token_result["id_token"];
	    }
    }

    protected function GetUserInfo ($accessToken) {
	    $user_info_endpoint = _SSO_URL_."/connect/userinfo";
	    $headers = array("Authorization: Bearer ".$accessToken);
	    $user_json = json_decode(Tools::FetchURL($user_info_endpoint,null,$headers), true);
	
	    return $user_json;
    }
    
    protected function GetAccessToken($code) {
	    $token_endpoint = _SSO_URL_."/connect/token";
        $token_params = array(
		    'grant_type' => "authorization_code",
            'code' => $code,
            'redirect_uri' => _SSO_CALLBACK_URL_,
            'client_id' => _SSO_CLIENT_ID_,
            'client_secret' => _SSO_CLIENT_SECRET_
	    );
  
        // Convert token params to string format
        $token_params = http_build_query($token_params, null, '&');
        return json_decode(Tools::FetchURL($token_endpoint, $token_params), true);
    }
}