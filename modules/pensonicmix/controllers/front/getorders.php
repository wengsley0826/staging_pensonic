<?php

// ~/module/pensonicmix/addvoucher
class PensonicMixGetOrdersModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        
        die();
    }

    public function postProcess() {
        $dateFrom = Tools::getValue("from");
        
        $sql = 
            "SELECT A.id_order, A.total_paid AS order_total, A.total_discounts_tax_incl AS discount_total,".
	            "A.date_add AS order_date_created, A.date_upd AS order_date_updated,".
                "CASE WHEN A.current_state IN (2,3,4) THEN 'processing' ".
		            "WHEN A.current_state IN (5) THEN 'completed' ".
                    "WHEN A.current_state IN (6,7) THEN 'cancelled' END AS order_status,".
                "B.email, B.crm_id, GROUP_CONCAT(IFNULL(D.code, IFNULL(C.name, ''))) AS coupon ".
            "FROM ". _DB_PREFIX_ ."orders A ".
            "INNER JOIN ". _DB_PREFIX_ ."customer B ON A.id_customer = B.id_customer ".
            "LEFT JOIN pensonic_order_cart_rule C ON A.id_order = C.id_order ".
            "LEFT JOIN pensonic_cart_rule D ON C.id_cart_rule = D.id_cart_rule ".
            "WHERE A.date_upd > '".pSQL($dateFrom)."' AND A.current_state IN (2,3,4,5,6,7) ".
            "GROUP BY A.id_order";
        
        $results = Db::getInstance()->executeS($sql);

        echo json_encode($results);
        die();
    }
}