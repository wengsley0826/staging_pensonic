<?php

// ~/module/pensonicmix/postsignout
class PensonicMixPostSignoutModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        Tools::redirect($this->context->cookie->signoutredirect);        
    }
}