<?php

// ~/module/pensonicmix/salesorder
class PensonicMixSalesOrderModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
         
    }

    protected $sqlConnection = null;

    protected $lineRunningNo = 0;
    
    public function postProcess()
    {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        
        $orders = $this->getOrderIdsToProcess();
print_r($orders);
print_r("<br />");

            $server = Configuration::get("SALES_ORDER_DB_SERVER");
            $username = Configuration::get("SALES_ORDER_DB_USER");
            $password = Configuration::get("SALES_ORDER_DB_PASSWORD");
            $database = Configuration::get("SALES_ORDER_DB_NAME");

            $this->sqlConnection = mssql_connect($server, $username, $password);
            mssql_select_db($database, $this->sqlConnection);
			


        if(count($orders) > 0) {
			
			

            for ($i = 0; $i < count($orders); $i++) {
                $id_order = $orders[$i]["id_order"];
                $sql = "INSERT INTO `". _DB_PREFIX_ ."order_outbound` (`id_order`, `date_add`, `date_upd`, `salesorder_status`, `salesorder_id`)".
                        "VALUES(".$id_order.",NOW(),NOW(),1,0)";
                Db::getInstance()->execute($sql);
                $order_outbound_id = Db::getInstance()->Insert_ID();

                $this->InsertSalesOrder($id_order);

                Db::getInstance()->execute("UPDATE pensonic_order_outbound ".
                "SET salesorder_status = 2 ".
                "WHERE order_outbound_id=".$order_outbound_id);

                //// update order status to In Process
                if($orders[$i]["current_state"] == 2) 
                {
                    $order = new Order($id_order);
                    $new_history = new OrderHistory();
		            $new_history->id_order = (int)$id_order;
                    $new_history->id_employee = 0;
                    $new_history->changeIdOrderState(Configuration::get('PS_OS_PREPARATION'), $order);
                    $new_history->addWithemail(true, false);
                }
            }
			
			
			
				
				

            mssql_close($this->sqlConnection);
        }

        die();
    }

    public function getOrderIdsToProcess() 
    {
        $sql = 
            "SELECT DISTINCT A.id_order, current_state
            FROM ". _DB_PREFIX_ ."orders A
            LEFT JOIN (
                SELECT id_order, MAX(salesorder_status) AS salesorder_status
                FROM ". _DB_PREFIX_ ."order_outbound
                GROUP BY id_order
            ) J ON A.id_order = J.id_order AND J.salesorder_status = 2
            WHERE A.current_state IN (".Configuration::get("SALES_ORDER_FROM_STATUS").") AND J.id_order IS NULL AND A.id_order=10002";
        $orders = Db::getInstance()->executeS($sql);

        $orderIds = [];
        foreach ($orders as $o) 
        {
            $orderIds[] = $o;
        }

        return $orderIds;
    }

    public function getOrderToProcess($id_order) 
    {
        $sql = 
            "SELECT A.id_order,A.id_customer,C.firstname,C.lastname, P.transaction_id,
                DATE_FORMAT(A.date_add, '%Y%m%d') AS order_date, G.reference AS product_reference, 
				B.product_name,
                B.product_quantity,
				IF(IFNULL(G.unity, 'PC')='','PC',IFNULL(G.unity, 'PC')) AS UOM,
                ROUND(B.unit_price_tax_incl,2) AS unit_price_tax_incl, 
				B.total_price_tax_incl,
                
                D.firstname AS delivery_firstname, D.lastname AS delivery_lastname,
                D.address1, D.address2, D.postcode, D.city, E.name AS state_name, F.iso_code AS country_code,
                D.phone, D.phone_mobile,
                
                B.id_order_detail,
				A.total_shipping_tax_incl,
				A.total_discounts_tax_incl,
				2 AS l_type
            FROM ". _DB_PREFIX_ ."orders A
            INNER join ". _DB_PREFIX_ ."order_payment P ON A.reference = P.order_reference
            INNER JOIN ". _DB_PREFIX_ ."order_detail B ON A.id_order = B.id_order
            INNER JOIN ". _DB_PREFIX_ ."customer C ON A.id_customer = C.id_customer
            INNER JOIN ". _DB_PREFIX_ ."address D ON A.id_address_delivery = D.id_address
            LEFT JOIN ". _DB_PREFIX_ ."state E ON D.id_state = E.id_state
            INNER JOIN ". _DB_PREFIX_ ."country F ON D.id_country = F.id_country
            LEFT JOIN ". _DB_PREFIX_ ."product G ON B.product_id = G.id_product
            WHERE A.id_order=".$id_order;
        $orders = Db::getInstance()->executeS($sql);

		if ($orders[0]['total_shipping_tax_incl']>0){
			$shippingCode=Configuration::get('SALES_ORDER_GLCODE_SHIPPING');
			$shippingAmt=$orders[0]['total_shipping_tax_incl'];
			
			$shippingArr=$orders[0];
			$shippingArr['product_reference']=$shippingCode;
			$shippingArr['product_name']="INVOICE SHIPPING";
			$shippingArr['unit_price_tax_incl']=$shippingAmt;
			$shippingArr['total_price_tax_incl']=$shippingAmt;
			$shippingArr['l_type']=1; 
			$shippingArr['product_quantity']=1;
			$orders[]=$shippingArr;
		}
		if ($orders[0]['total_discounts_tax_incl']>0){
			$discountCode=Configuration::get('SALES_ORDER_GLCODE_DISCOUNT');
			$discountAmt=$orders[0]['total_discounts_tax_incl'];
			
			$discountArr=$orders[0];
			$discountArr['product_reference']=$discountCode;
			$discountArr['product_name']="PROMOTION";
			$discountArr['unit_price_tax_incl']=-$discountAmt;
			$discountArr['total_price_tax_incl']=-$discountAmt;
			$discountArr['l_type']=1; 
			$discountArr['product_quantity']=1;
			$orders[]=$discountArr;
		}		
		
        $sql = 
            "
				SELECT B.*, A.value FROM `pensonic_order_cart_rule` A
				JOIN pensonic_cart_rule B ON A.id_cart_rule=B.id_cart_rule
				WHERE cart_voucher_type='payment' AND A.id_order=".$id_order;
        $orderscart = Db::getInstance()->executeS($sql);
		foreach($orderscart as $onePaymentVoucher){
			$paymentGLCode=Configuration::get('SALES_ORDER_GLCODE_PAYMENT_VOUCHER');
			$paymentDescription=Configuration::get('SALES_ORDER_DESCRIPTION_PAYMENT_VOUCHER');
			$cartCode=$onePaymentVoucher['code'];
			$paymentAmt=$onePaymentVoucher['value'];
			
			$discountArr=$orders[0];
			$discountArr['product_reference']=$paymentGLCode;
			$discountArr['product_name']=$paymentDescription." (".$cartCode.") RM ".$paymentAmt;
			$discountArr['unit_price_tax_incl']=$paymentAmt;
			$discountArr['total_price_tax_incl']=$paymentAmt;
			$discountArr['l_type']=1; 
			$discountArr['product_quantity']=1;
			$orders[]=$discountArr;
		}		
		
        return $orders;
    }

    public function getMaxOrderLineNo() {
        $maxVal = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue("SELECT MAX(runningLineNo) FROM `". _DB_PREFIX_ ."order_outbound_item`");

        if(!$maxVal) {
            $maxVal = 0;
        }

        return $maxVal + 10000;
    }

    public function InsertSalesOrder($id_order) 
    {
        $orders = $this->getOrderToProcess($id_order);
        
        //$companyId = 'P051_PSS';
        //$integrationSource = 'ESTORE';
        //$sourceCode = 'SALES';
        //$documentType = 1;
        //$customerNo = 'KLCP078';
        //$currencyCode = '';
        //$currencyExchangeRate = 1;
        //$salesPersonCode = 'KL45_YWM';
        //$type = 2;
        //$locationCode = 'PGWH08EC';
        //$sstTaxCode = 'JNA';
        //$responsibilityCentre = 'PSSKL';

        $configuration = Configuration::getMultiple(array(
            'SALES_ORDER_COMPANY_ID',
            'SALES_ORDER_INTEGRATION_SOURCE',
            'SALES_ORDER_SOURCE_CODE',
            'SALES_ORDER_DOCUMENT_TYPE',
            'SALES_ORDER_CUSTOMER_NO',
            'SALES_ORDER_SALESPERSON_CODE',
            'SALES_ORDER_LOCATION_CODE',
            'SALES_ORDER_SST_TAXCODE',
            'SALES_ORDER_RESPONSIBILITY_CENTRE'
        ));

        $companyId = $configuration['SALES_ORDER_COMPANY_ID']; 
        $integrationSource = $configuration['SALES_ORDER_INTEGRATION_SOURCE'];
        $sourceCode = $configuration['SALES_ORDER_SOURCE_CODE'];
        $documentType = $configuration['SALES_ORDER_DOCUMENT_TYPE'];
        $customerNo = $configuration['SALES_ORDER_CUSTOMER_NO'];
        $currencyCode = '';
        $currencyExchangeRate = 1;
        $salesPersonCode = $configuration['SALES_ORDER_SALESPERSON_CODE'];
        $type = 2;
        $locationCode = $configuration['SALES_ORDER_LOCATION_CODE'];
        $sstTaxCode = $configuration['SALES_ORDER_SST_TAXCODE'];
        $responsibilityCentre = $configuration['SALES_ORDER_RESPONSIBILITY_CENTRE'];

        for ($i = 0; $i < count($orders); $i++) {
            $lineNo = ($i + 1) * 10000;
            //$lineNo2 = $this->getMaxOrderLineNo();

            $customerName = '';

            if($orders[$i]['firstname'] != '' && $orders[$i]['lastname'] != '') {
                $customerName = $orders[$i]['firstname'].' '.$orders[$i]['lastname'];
            }
            else if($orders[$i]['firstname'] != '') {
                $customerName = $orders[$i]['firstname'];
            }
            else if($orders[$i]['lastname'] != '') {
                $customerName = $orders[$i]['lastname'];
            }

            $paymentRef= $orders[$i]['transaction_id'];

            if(strlen($paymentRef) > 19) {
                $paymentRef = substr($paymentRef, 0, 19);
            }

            $paymentRef = $paymentRef.'_'.$orders[$i]['id_order'];

            $shipToName = '';
            if($orders[$i]['delivery_firstname'] != '' && $orders[$i]['delivery_lastname'] != '') {
                $shipToName = $orders[$i]['delivery_firstname'].' '.$orders[$i]['delivery_lastname'];
            }
            else if($orders[$i]['delivery_firstname'] != '') {
                $shipToName = $orders[$i]['delivery_firstname'];
            }
            else if($orders[$i]['delivery_lastname'] != '') {
                $shipToName = $orders[$i]['delivery_lastname'];
            }

            $shipToContact = '';
            if($orders[$i]['phone_mobile'] != '') {
                $shipToContact = $orders[$i]['phone_mobile'];
            }
            else if($orders[$i]['phone'] != '') {
                $shipToContact = $orders[$i]['phone'];
            }
			
			
			$l_type = $orders[$i]['l_type'];

            $sql = 'INSERT INTO [dbo].[IT_Intg_ Trans_ Staging] ('.
                    '[H_Company ID],[H_Integration Source],[H_Source Code],[H_Document Type],[H_Source No_]'.
                    ',[H_Customer Name],[H_Your Reference],[H_Order Date],[H_Currency Code],[H_Currency Exchange Rate]'.
                    ',[H_Salesperson Code],[L_Line No_],[L_Type],[L_No_],[L_Description]'.
                    ',[L_Location Code],[L_Quantity],[L_Unit of Measure Code],[L_Unit Price Excl_ SST],[L_Line Discount %]'.
                    ',[L_Line Discount Amount],[L_Amount Incl_ SST],[L_SST%],[L_SST Amount],[L_Tax Code]'.
                    ',[H_Responsibility Centre],[H_Ship to Name],[H_Ship to Address],[H_Ship to Address 2],[H_Ship Post Code]'.
                    ',[H_Ship to City],[H_Ship to State],[H_Ship Country Code],[H_Ship to Contact],[H_Posting No_]'.

                //',[Batch No_]'.
                //    ',[Source Type],[Entry Type]'.
                //    ',[H_Posting Date]'.
                //    ',[L_Amount],[L_Debit Amount],[L_Credit Amount]'.
                //    ',[H_External Document No_],[L_Shortcut Dimension 1 Code],[L_Shortcut Dimension 2 Code],[H_GST Reason Code],[Imported By]'.
                //    ',[Imported Date Time],[Created From Ext_ Source],[L_VAT Prod_ Posting Group],[L_Gen. Prod_ Posting Group],[H_Posting No_]'.
                //    ',[H_Posting Description],[H_Ship-to Code],[L_Direct Unit Cost],[H_Transfer-from Code]'.
                //    ',[H_Transfer-to Code],[L_Document No_]'.
                
                    ') VALUES ('.
                    '"'.$this->encodeSQL($companyId).'","'.$this->encodeSQL($integrationSource).'","'.$this->encodeSQL($sourceCode).'","'.$this->encodeSQL($documentType).'","'.$this->encodeSQL($customerNo).'",'.
                    '"'.$this->encodeSQL($customerName).'","'.$this->encodeSQL($paymentRef).'","'.$this->encodeSQL($orders[$i]['order_date']).'","","1",'.
                    '"'.$this->encodeSQL($salesPersonCode).'","'.$this->encodeSQL($lineNo).'","'.$this->encodeSQL($l_type).'","'.$this->encodeSQL($orders[$i]['product_reference']).'","'.$this->encodeSQL($orders[$i]['product_name']).'",'.
                    '"'.$this->encodeSQL($locationCode).'","'.$this->encodeSQL($orders[$i]['product_quantity']).'","'.$this->encodeSQL($orders[$i]['UOM']).'","'.$this->encodeSQL($orders[$i]['unit_price_tax_incl']).'","0",'.
                    '"0","'.$this->encodeSQL($orders[$i]['total_price_tax_incl']).'","0","0","'.$this->encodeSQL($sstTaxCode).'",'.
                    '"'.$this->encodeSQL($responsibilityCentre).'","'.$this->encodeSQL($shipToName).'","'.$this->encodeSQL($orders[$i]['address1']).'","'.$this->encodeSQL($orders[$i]['address2']).'","'.$this->encodeSQL($orders[$i]['postcode']).'",'.
                    '"'.$this->encodeSQL($orders[$i]['city']).'","'.$this->encodeSQL($orders[$i]['state_name']).'","'.$this->encodeSQL($orders[$i]['country_code']).'","'.$this->encodeSQL($shipToContact).'","'.$this->encodeSQL(substr($paymentRef,-20)).'"'.
                    ')';
			echo $sql;
            try {
                $result = mssql_query($sql,$this->sqlConnection);
                print_r("Insert Result => ");
                print_r($result);
                print_r("<br />");
                //print_r($sql);
                //print_r("<Br /><Br />");
                $sql = "INSERT INTO `". _DB_PREFIX_ ."order_outbound_item` (runningLineNo,id_order,id_order_detail)".
                        "VALUES(".$lineNo.",".$id_order.",".$orders[$i]['id_order_detail'].")";
                Db::getInstance()->execute($sql);

            }
            catch(Exception $e) {
                print_r("Insert Failed => ");
                print_r($e);
                print_r("<br />");
            }
        }
		
		
			$sql = 'SELECT * FROM [dbo].[IT_Intg_ Trans_ Staging] WHERE [H_Posting No_] = "'.$this->encodeSQL($shipToContact).'","'.$this->encodeSQL(substr($paymentRef,-20)).'" ';
			$result = mssql_query($sql,$this->sqlConnection);
			
			for ($i = 0; $i < mssql_num_rows( $result ); ++$i)
				 {
					 $line = mssql_fetch_row($result);
					 print_r( $line);
				 }
				 
			print_r($result);
				
				
				
    }

    public function encodeSQL($val) {
        $val = str_replace('"', '""', $val);

        //return pSQL($val);
        return $val;
    }
}