<?php

// ~/module/pensonicmix/salesorder
class PensonicMixSyncStockModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
         
    }

    protected $sqlConnection = null;
    protected $insertSQL = "INSERT INTO ". _DB_PREFIX_ ."nav_product_stock (`nav_product_sku`,`nav_product_qty`,`nav_product_uom`) VALUES ('{0}','{1}','{2}')";

    public function postProcess()
    {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        
        $products = $this->getProductStock();

        Db::getInstance()->execute("TRUNCATE TABLE ". _DB_PREFIX_ ."nav_product_stock");
        if(count($products) > 0) {
            foreach($products as $key=>$prod) {
                $sql = str_replace("{0}", $prod["sku"], $this->insertSQL);
                $sql = str_replace("{1}", $prod["qty"], $sql);
                $sql = str_replace("{2}", $prod["uom"], $sql);
                Db::getInstance()->execute($sql);
            }

            $maxStock = 20;

            // update quantity for product with attribute
            Db::getInstance()->execute(
                "UPDATE pensonic_stock_available A ".
                "INNER JOIN pensonic_product_attribute B ON A.id_product = B.id_product AND A.id_product_attribute = B.id_product_attribute ".
                "INNER JOIN pensonic_nav_product_stock C ON C.nav_product_sku = B.reference ".
                "SET A.quantity = CASE WHEN C.nav_product_qty >".$maxStock." THEN ".$maxStock." ELSE C.nav_product_qty END ");

            // update quantity for product without attribute
            Db::getInstance()->execute(
                "UPDATE pensonic_stock_available A ".
                "INNER JOIN pensonic_product B ON A.id_product = B.id_product ".
                "INNER JOIN pensonic_nav_product_stock C ON C.nav_product_sku = B.reference ".
                "SET A.quantity = CASE WHEN C.nav_product_qty >".$maxStock." THEN ".$maxStock." ELSE C.nav_product_qty END ".
                "WHERE A.id_product_attribute = 0");

            // sum quantity for product with attribute
            Db::getInstance()->execute(
                "UPDATE pensonic_stock_available A ".
                "INNER JOIN ( ".
                    "SELECT A.id_product, SUM(B.quantity) AS totalQty ".
	                "FROM pensonic_stock_available A ".
	                "INNER JOIN pensonic_stock_available B ON A.id_product = B.id_product ".
	                "WHERE A.id_product_attribute = 0 AND B.id_product_attribute != 0 ".
	                "GROUP BY A.id_product ".
                ") B ON A.id_product = B.id_product ".
                "SET A.quantity = B.totalQty ".
                "WHERE A.id_product_attribute = 0");

            Db::getInstance()->execute(
                "UPDATE pensonic_product A ".
                "INNER JOIN pensonic_product_attribute B ON A.id_product = B.id_product ".
                "INNER JOIN pensonic_nav_product_stock C ON B.reference = C.nav_product_sku ".
                "SET A.unity = C.nav_product_uom");

            Db::getInstance()->execute(
                "UPDATE pensonic_product A ".
                "INNER JOIN pensonic_nav_product_stock B ON A.reference = B.nav_product_sku ".
                "SET A.unity = B.nav_product_uom");
        }

        die();
    }

    public function getProductStock() 
    {
        $server = Configuration::get("SALES_ORDER_DB_SERVER");
        $username = Configuration::get("SALES_ORDER_DB_USER");
        $password = Configuration::get("SALES_ORDER_DB_PASSWORD");
        $database = Configuration::get("SALES_ORDER_DB_NAME");
        
        $this->sqlConnection = mssql_connect($server, $username, $password);
        mssql_select_db($database, $this->sqlConnection);

        $result = mssql_query("SET ANSI_NULLS ON;");
        $result = mssql_query("SET ANSI_WARNINGS ON;"); 
        $sql = "SELECT [Company ID],[No_],[Description],[Inventory],[Base Unit of Measure],[Qty_ On Sales Order],[Projected Inventory] FROM VM_Inventory";
        $query = mssql_query($sql,$this->sqlConnection);

        $products = [];
        while ($row = mssql_fetch_array($query)) {
            $products[] = array("sku" => $row[1], "qty" => $row[6], "uom" => $row[4]);
        }

        mssql_close($this->sqlConnection);
        
        return $products;
    }
}