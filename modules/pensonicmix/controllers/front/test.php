<?php

// ~/module/pensonicmix/test
class PensonicMixTestModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
         
    }

    public function postProcess()
    {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        
        //Tools::SendPushNotification("Pensonic", "Test Push Notification", "28");
        $code = Tools::getValue('code');
        $redirect_uri = Tools::getValue('app_redirect_uri');

        print_r($code);
        print_r($redirect_uri);
        die();
    }
}