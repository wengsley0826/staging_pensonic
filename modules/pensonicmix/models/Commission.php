<?php

if (!defined('_PS_VERSION_'))
	exit;

class Commission extends ObjectModel
{
	public $commission_id;
	public $affiliate_code;
	public $affiliate_name;
	public $commission_rate;
	public $commission_max;
	public $commission_category;
	public $hideLink;
	
	public static $definition = array(
		'table' => 'commission',
		'primary' => 'commission_id',
		'fields' => array(
		
		'commission_id' =>		    		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
		'affiliate_code' =>					array('type' => self::TYPE_STRING, 'required' => true),
		'affiliate_name' =>					array('type' => self::TYPE_STRING,  'required' => true),
		'commission_rate' =>				array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice' , 'required' => true),
		'commission_max' =>					array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
		'commission_category' =>			array('type' => self::TYPE_INT),
		'hideLink' =>			array('type' => self::TYPE_INT ),
		)
	);
	
}