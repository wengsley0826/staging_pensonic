<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

if (!class_exists('pensonicmix')) {
   // Put class TestClass here
    include(dirname(__FILE__).'/pensonicmix.php');
}

if (isset($_REQUEST['ajax']) && isset($_REQUEST["type"]) && isset($_REQUEST["orderId"])) {
    $type = pSQL($_REQUEST["type"]);
    $orderId = (int)$_REQUEST["orderId"];
    $obj = new pensonicmix();
    $result = $obj->ajaxPensonicMix($type, $orderId);
    echo $result;
}
else if (isset($_REQUEST['cron']) && isset($_REQUEST["type"])) {
    //// cron job update product selling price
    $type = pSQL($_REQUEST["type"]);
    $obj = new enlineamixmod();
    switch($type) {
        case "22":
            $obj->cronUpdateGDEXTrackingNumber();
            break;
    }
}
else {
    echo false;
}
?>