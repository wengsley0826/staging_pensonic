<?php

class PensonicMix extends Module
{
    protected $_html = '';
    protected $_postErrors = array();

    public function __construct()
    {
        if (!defined('_PS_VERSION_')) {
            exit;
        }
        $this->name = 'pensonicmix';
        $this->tab = 'Pensonic_Mix';
        $this->version = '1.0.0';
        $this->author = 'Claritas';
        $this->displayName = 'Pensonic\'S integration settings';
        $this->bootstrap = true;
        $this->display = 'view';
        $this->is_eu_compatible = 1;
        
        parent::__construct();

        $this->description = $this->l('Pensonic\'S integration settings');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

    }

    public function getDefaults()
    {
        return array(
            'GDEX_SEND_SHIPMENT_URL' => '',
            'GDEX_TRACK_SHIPMENT_URL' => '',
            'GDEX_TRACK_FULL_SHIPMENT_URL' => '',
            
            'SALES_ORDER_DB_SERVER' => '',
            'SALES_ORDER_DB_USER' => '',
            'SALES_ORDER_DB_PASSWORD' => '',
            'SALES_ORDER_DB_NAME' => '',

            'SALES_ORDER_COMPANY_ID' => '',
            'SALES_ORDER_INTEGRATION_SOURCE' => '',
            'SALES_ORDER_SOURCE_CODE' => '',
            'SALES_ORDER_DOCUMENT_TYPE' => '',
            'SALES_ORDER_CUSTOMER_NO' => '',
            'SALES_ORDER_SALESPERSON_CODE' => '',
            'SALES_ORDER_LOCATION_CODE' => '',
            'SALES_ORDER_SST_TAXCODE' => '',
            'SALES_ORDER_RESPONSIBILITY_CENTRE' => '',

            
            'SALES_ORDER_FROM_STATUS' => '',

        );
    }


    public function install()
    {
        return parent::install();
    }

    public function uninstall()
    {
        if (
            !Configuration::deleteByName('GDEX_SEND_SHIPMENT_URL') 
            || !Configuration::deleteByName('GDEX_TRACK_SHIPMENT_URL') 
            || !Configuration::deleteByName('GDEX_TRACK_FULL_SHIPMENT_URL') 

            || !Configuration::deleteByName('SALES_ORDER_DB_SERVER') 
            || !Configuration::deleteByName('SALES_ORDER_DB_USER') 
            || !Configuration::deleteByName('SALES_ORDER_DB_PASSWORD') 
            || !Configuration::deleteByName('SALES_ORDER_DB_NAME') 

            || !Configuration::deleteByName('SALES_ORDER_COMPANY_ID') 
            || !Configuration::deleteByName('SALES_ORDER_INTEGRATION_SOURCE') 
            || !Configuration::deleteByName('SALES_ORDER_SOURCE_CODE') 
            || !Configuration::deleteByName('SALES_ORDER_DOCUMENT_TYPE') 
            || !Configuration::deleteByName('SALES_ORDER_CUSTOMER_NO') 
            || !Configuration::deleteByName('SALES_ORDER_SALESPERSON_CODE') 
            || !Configuration::deleteByName('SALES_ORDER_LOCATION_CODE') 
            || !Configuration::deleteByName('SALES_ORDER_SST_TAXCODE') 
            || !Configuration::deleteByName('SALES_ORDER_RESPONSIBILITY_CENTRE') 

            || !Configuration::deleteByName('SALES_ORDER_FROM_STATUS') 

            || !parent::uninstall())
            return false;
        
        return true;
    }

    protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('GDEX_SEND_SHIPMENT_URL')) {
                $this->_postErrors[] = $this->l('GDex send shipping URL is required!');
            }
            if (!Tools::getValue('GDEX_TRACK_SHIPMENT_URL')) {
                $this->_postErrors[] = $this->l('GDex track shipment URL is required!');
            }
            if (!Tools::getValue('GDEX_TRACK_FULL_SHIPMENT_URL')) {
                $this->_postErrors[] = $this->l('GDex track full shipment URL is required!');
            }
        }
	}
    
    protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('GDEX_SEND_SHIPMENT_URL', Tools::getValue('GDEX_SEND_SHIPMENT_URL'));
            Configuration::updateValue('GDEX_TRACK_SHIPMENT_URL', Tools::getValue('GDEX_TRACK_SHIPMENT_URL'));
            Configuration::updateValue('GDEX_TRACK_FULL_SHIPMENT_URL', Tools::getValue('GDEX_TRACK_FULL_SHIPMENT_URL'));

            Configuration::updateValue('SALES_ORDER_DB_SERVER', Tools::getValue('SALES_ORDER_DB_SERVER'));
            Configuration::updateValue('SALES_ORDER_DB_USER', Tools::getValue('SALES_ORDER_DB_USER'));
            Configuration::updateValue('SALES_ORDER_DB_PASSWORD', Tools::getValue('SALES_ORDER_DB_PASSWORD'));
            Configuration::updateValue('SALES_ORDER_DB_NAME', Tools::getValue('SALES_ORDER_DB_NAME'));

            Configuration::updateValue('SALES_ORDER_COMPANY_ID', Tools::getValue('SALES_ORDER_COMPANY_ID'));
            Configuration::updateValue('SALES_ORDER_INTEGRATION_SOURCE', Tools::getValue('SALES_ORDER_INTEGRATION_SOURCE'));
            Configuration::updateValue('SALES_ORDER_SOURCE_CODE', Tools::getValue('SALES_ORDER_SOURCE_CODE'));
            Configuration::updateValue('SALES_ORDER_DOCUMENT_TYPE', Tools::getValue('SALES_ORDER_DOCUMENT_TYPE'));
            Configuration::updateValue('SALES_ORDER_CUSTOMER_NO', Tools::getValue('SALES_ORDER_CUSTOMER_NO'));
            Configuration::updateValue('SALES_ORDER_SALESPERSON_CODE', Tools::getValue('SALES_ORDER_SALESPERSON_CODE'));
            Configuration::updateValue('SALES_ORDER_LOCATION_CODE', Tools::getValue('SALES_ORDER_LOCATION_CODE'));
            Configuration::updateValue('SALES_ORDER_SST_TAXCODE', Tools::getValue('SALES_ORDER_SST_TAXCODE'));
            Configuration::updateValue('SALES_ORDER_RESPONSIBILITY_CENTRE', Tools::getValue('SALES_ORDER_RESPONSIBILITY_CENTRE'));

            Configuration::updateValue('SALES_ORDER_FROM_STATUS', Tools::getValue('SALES_ORDER_FROM_STATUS'));
        }
        
        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}
    
    public function getContent()
    {
       if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
    }

    public function renderForm($message = null)
    {
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('GDEX'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('GDEX Add Shipment URL'),
                    'name' => 'GDEX_SEND_SHIPMENT_URL',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('GDEX Track Latest Shiment URL'),
                    'name' => 'GDEX_TRACK_SHIPMENT_URL',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('GDEX Track Full Shipment URL'),
                    'name' => 'GDEX_TRACK_FULL_SHIPMENT_URL',
                    'required' => true
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            ),
        );
        
        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Navision'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Navision DB Server'),
                    'name' => 'SALES_ORDER_DB_SERVER',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Navision DB Login'),
                    'name' => 'SALES_ORDER_DB_USER',
                    'required' => true
                ),
                array(
                    'type' => 'password',
                    'label' => $this->l('Navision DB Password'),
                    'name' => 'SALES_ORDER_DB_PASSWORD',
                    'required' => true
                ),
                
                array(
                    'type' => 'text',
                    'label' => $this->l('Navision DB Name'),
                    'name' => 'SALES_ORDER_DB_NAME',
                    'required' => true
                ),

                array(
                    'type' => 'text',
                    'label' => $this->l('Process Order Status Id'),
                    'name' => 'SALES_ORDER_FROM_STATUS',
                    'required' => true,
                    'desc' => 'Separate by Comma'
                ),

                array(
                    'type' => 'text',
                    'label' => $this->l('Company ID'),
                    'name' => 'SALES_ORDER_COMPANY_ID',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Integration Source'),
                    'name' => 'SALES_ORDER_INTEGRATION_SOURCE',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Source Code'),
                    'name' => 'SALES_ORDER_SOURCE_CODE',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Document Type'),
                    'name' => 'SALES_ORDER_DOCUMENT_TYPE',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Customer No.'),
                    'name' => 'SALES_ORDER_CUSTOMER_NO',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Salesperson Code'),
                    'name' => 'SALES_ORDER_SALESPERSON_CODE',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Location Code'),
                    'name' => 'SALES_ORDER_LOCATION_CODE',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('SST Tax Code'),
                    'name' => 'SALES_ORDER_SST_TAXCODE',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Responsibility Centre'),
                    'name' => 'SALES_ORDER_RESPONSIBILITY_CENTRE',
                    'required' => true
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );

        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm($fields_form);
    }

    protected function getOrderStatuses() {
        $allstatuses = Db::getInstance()->ExecuteS('SELECT DISTINCT A.id_order_state, B.name AS status_name'.
                    ' FROM '._DB_PREFIX_.'order_state A'.
                    ' INNER JOIN '._DB_PREFIX_.'order_state_lang B ON A.id_order_state = B.id_order_state'.
                    ' WHERE B.id_lang = 1'.
                    ' ORDER BY B.name'
		);
        
        $status_list = array();
        
        foreach ($allstatuses as $status)
        {
            $status_list[] = array(
                "id_order_state" => $status["id_order_state"],
                "name" => $status["status_name"]
            );
        }
        
        return $status_list;
    }
    
    public function getConfigFieldsValues()
	{
    	return array(
			'GDEX_SEND_SHIPMENT_URL' => Tools::getValue('GDEX_SEND_SHIPMENT_URL', Configuration::get('GDEX_SEND_SHIPMENT_URL')),
            'GDEX_TRACK_SHIPMENT_URL' => Tools::getValue('GDEX_TRACK_SHIPMENT_URL', Configuration::get('GDEX_TRACK_SHIPMENT_URL')),
            'GDEX_TRACK_FULL_SHIPMENT_URL' => Tools::getValue('GDEX_TRACK_FULL_SHIPMENT_URL', Configuration::get('GDEX_TRACK_FULL_SHIPMENT_URL')),

            'SALES_ORDER_DB_SERVER' => Tools::getValue('SALES_ORDER_DB_SERVER', Configuration::get('SALES_ORDER_DB_SERVER')),
            'SALES_ORDER_DB_USER' => Tools::getValue('SALES_ORDER_DB_USER', Configuration::get('SALES_ORDER_DB_USER')),
            'SALES_ORDER_DB_PASSWORD' => Tools::getValue('SALES_ORDER_DB_PASSWORD', Configuration::get('SALES_ORDER_DB_PASSWORD')),
            'SALES_ORDER_DB_NAME' => Tools::getValue('SALES_ORDER_DB_NAME', Configuration::get('SALES_ORDER_DB_NAME')),
            
            'SALES_ORDER_COMPANY_ID' => Tools::getValue('SALES_ORDER_COMPANY_ID', Configuration::get('SALES_ORDER_COMPANY_ID')),
            'SALES_ORDER_INTEGRATION_SOURCE' => Tools::getValue('SALES_ORDER_INTEGRATION_SOURCE', Configuration::get('SALES_ORDER_INTEGRATION_SOURCE')),
            'SALES_ORDER_SOURCE_CODE' => Tools::getValue('SALES_ORDER_SOURCE_CODE', Configuration::get('SALES_ORDER_SOURCE_CODE')),
            'SALES_ORDER_DOCUMENT_TYPE' => Tools::getValue('SALES_ORDER_DOCUMENT_TYPE', Configuration::get('SALES_ORDER_DOCUMENT_TYPE')),
            'SALES_ORDER_CUSTOMER_NO' => Tools::getValue('SALES_ORDER_CUSTOMER_NO', Configuration::get('SALES_ORDER_CUSTOMER_NO')),
            'SALES_ORDER_SALESPERSON_CODE' => Tools::getValue('SALES_ORDER_SALESPERSON_CODE', Configuration::get('SALES_ORDER_SALESPERSON_CODE')),
            'SALES_ORDER_LOCATION_CODE' => Tools::getValue('SALES_ORDER_LOCATION_CODE', Configuration::get('SALES_ORDER_LOCATION_CODE')),
            'SALES_ORDER_SST_TAXCODE' => Tools::getValue('SALES_ORDER_SST_TAXCODE', Configuration::get('SALES_ORDER_SST_TAXCODE')),
            'SALES_ORDER_RESPONSIBILITY_CENTRE' => Tools::getValue('SALES_ORDER_RESPONSIBILITY_CENTRE', Configuration::get('SALES_ORDER_RESPONSIBILITY_CENTRE')),
           
            'SALES_ORDER_FROM_STATUS' => Tools::getValue('SALES_ORDER_FROM_STATUS', Configuration::get('SALES_ORDER_FROM_STATUS')),
        );
	}
    
    public function hookHeader($params)
	{
        $aff_info = null;
		
		die('zzz');
        
        $utmSource = Tools::getValue('utm_source');
        if(isset($utmSource) && trim($utmSource) != "") {
            $affiliateCookie = new Cookie("aff_info", "", time() + 60* 60 * 24 * 30);//expired in 30 days
            $affiliateCookie->utm_source = trim($utmSource);
              
            $sessionId = Tools::getValue('session_id');
            
            if(isset($sessionId) && trim($sessionId) != "") {
                $affiliateCookie->session_id = trim($sessionId);
            }
            
            $aff_info = array();
            $aff_info['utm_source'] = $affiliateCookie->utm_source;
            
            if(isset($affiliateCookie->session_id)) {
                 $aff_info['session_id'] = $affiliateCookie->session_id;
            }
			
			if ($this->context->customer && $this->context->customer->id){ 
				Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
					INSERT INTO `'._DB_PREFIX_.'source_tracking` A (id_customer, source, add_date, type) VALUES ("'.$this->context->customer->id.'",
					"'.$utmSource.'",CURRENT_TIMESTAMP,"NEW");
					');
			}else if ($this->context->guest && $this->context->guest->id){
				Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
					INSERT INTO `'._DB_PREFIX_.'source_tracking` A (id_customer, source, add_date, type) VALUES ("'.$this->context->guest->id.'",
					"'.$utmSource.'",CURRENT_TIMESTAMP,"NEW");
					');
			}
			
        
        }
		else {
            $affiliateCookie = new Cookie("aff_info");
    
            if(isset($affiliateCookie) && isset($affiliateCookie->utm_source)) {
                $aff_info = array();
                $aff_info['utm_source'] = $affiliateCookie->utm_source;
				$utmSource=$affiliateCookie->utm_source;
				
                if(isset($affiliateCookie->session_id)) {
                     $aff_info['session_id'] = $affiliateCookie->session_id;
                }
					
				if ($this->context->customer && $this->context->customer->id){
					Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
						INSERT INTO `'._DB_PREFIX_.'source_tracking` A (id_customer, source, add_date, type) VALUES ("'.$this->context->customer->id.'",
						"'.$utmSource.'",CURRENT_TIMESTAMP,"CONTINUE");
						');
					if ($this->context->guest && $this->context->guest->id){
						Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
							UPDATE `'._DB_PREFIX_.'source_tracking` SET id_customer="'.$this->context->customer->id.'"
							WHERE id_customer="'.$this->context->guest->id.'"
						');
						
					}
							
				}else if ($this->context->guest && $this->context->guest->id){
					Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
						INSERT INTO `'._DB_PREFIX_.'source_tracking` A (id_customer, source, add_date, type) VALUES ("'.$this->context->guest->id.'",
						"'.$utmSource.'",CURRENT_TIMESTAMP,"CONTINUE");
						');
				}
            
            }
        }
        
        Media::addJsDef(array('aff_info' => $aff_info));
	}
    
    public function hookFooter($params)
	{
        //$aff_info = null;
        //$affiliateCookie = new Cookie("aff_info");
    
        //if(isset($affiliateCookie) && isset($affiliateCookie->utm_source)) {
        //    $aff_info = array();
        //    $aff_info['utm_source'] = $affiliateCookie->utm_source;
            
        //    if(isset($affiliateCookie->session_id)) {
        //         $aff_info['session_id'] = $affiliateCookie->session_id;
        //    }
            
        //}
        
        //Media::addJsDef(array('aff_info' => $aff_info));
    }
    
    public function hookDisplayOrderConfirmation($params) 
    {
        $order_info = null;

        if (isset($params['objOrder'])) {
            $order = $params['objOrder'];
            
            $order_info = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT DISTINCT A.product_id,A.product_name,A.product_quantity,A.product_reference,A.unit_price_tax_incl,
                    C.id_category,C.name AS category_name
				FROM `'._DB_PREFIX_.'order_detail` A
                INNER JOIN `'._DB_PREFIX_.'product` B ON A.product_id = B.id_product
                INNER JOIN `'._DB_PREFIX_.'category_lang` C ON B.id_category_default = C.id_category
				WHERE id_order='.(int)$order->id.' AND C.id_lang='.$this->context->language->id);
                
            $utmSource = "";
            $clickId = "";
            $affiliateCookie = new Cookie("aff_info");
        
            if(isset($affiliateCookie) && isset($affiliateCookie->utm_source)) {
                $utmSource = $affiliateCookie->utm_source;
            
                if(isset($affiliateCookie->session_id)) {
                    $clickId = $affiliateCookie->session_id;
                }            
            }
        
            $sql = 'INSERT INTO '._DB_PREFIX_.'order_pixel ('.
                        'id_order,date_add,order_value,clickId,customer_type,'.
                        'basket,exclude,utm_source,utm_campaign,utm_medium)'.
                        "VALUES(".$order->id.", NOW(),".$order->total_paid.",'".$clickId."','',".
                        "'".pSQL(json_encode($order_info))."','0','".$utmSource."','','')";
                            
            Db::getInstance()->execute($sql);
        }
        
        
        
        Media::addJsDef(array('order_info' => $order_info));
    }


    /* GDEX integration */
    public function ajaxPensonicMix($type, $orderId) 
    {
        $result = false;
        switch($type) {
            case "11":
                $result = $this->submitGDEXShipment($orderId);
                break;
        }
        
        return $result;
    }

    public function submitGDEXShipment($orderId) 
    {
        $sql = "SELECT A.id_order,A.current_state,A.reference, A.id_customer,".
	                "B.company, B.lastname, B.firstname,". 
                    "B.address1, B.address2,".
                    "B.postcode, B.city,". 
                    "B.phone, B.phone_mobile,".
                    "B.id_state, IFNULL(D.name, '') AS state_name,".
                    "B.id_country, C.name AS country_name ".
                "FROM "._DB_PREFIX_."orders A ".
                "INNER JOIN "._DB_PREFIX_."address B ON A.id_address_delivery = B.id_address ".
                "INNER JOIN "._DB_PREFIX_."country_lang C ON B.id_country = C.id_country AND C.id_lang = 1 ".
                "LEFT JOIN "._DB_PREFIX_."state D ON B.id_state = D.id_state ".
                "where id_order=".(int)$orderId;
        $order = Db::getInstance()->getRow($sql);
        //$accountNo = Configuration::get('ENLINEA_GDEX_ACCOUNT_NO');
        $gdexURL = Configuration::get('GDEX_SEND_SHIPMENT_URL');
        $pieces = Tools::getValue("pieces");
        $updatestatus = Tools::getValue("updatestatus");
        $weight = Tools::getValue("weight");
        if($weight == "") { $weight = 0; }
        $height = Tools::getValue("height");
        if($height == "") { $height = 0; }
        $width = Tools::getValue("width");
        if($width == "") { $width = 0; }
        $length = Tools::getValue("length");
        if($length == "") { $length = 0; }
        
        //// check still got gdex_cn available
        $gdexCounter = Db::getInstance()->getValue('SELECT COUNT(1) FROM `'._DB_PREFIX_.'gdex_cn` WHERE id_order IS NULL');
        if($gdexCounter == 0) {
            return -1;
        }
		
        Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'gdex_cn` SET id_order='.(int)$orderId.' WHERE id_order IS NULL LIMIT 1');
        $gdexData = Db::getInstance()->getRow('SELECT gdex_cn, gdex_acc FROM `'._DB_PREFIX_.'gdex_cn` WHERE id_order='.(int)$orderId.' ORDER BY id_gdex_cn DESC');
        $gdexCN = $gdexData['gdex_cn'];
        $accountNo = $gdexData['gdex_acc'];
        $data = array();
        
        $data["GdexCN"] = $gdexCN;
        $data["AccountNumber"] = $accountNo;
        $data["ConsignmentDate"] = date("Y-m-d");
        
        $data["Product"] = "00001";
		$data["ShipmentType"] = "P";
		
        $data["CODPayment"] = "0.00";
        $data["PickupType"] = "PICKUP";

        $data["Pieces"] = $pieces;
        $data["Weight"] = $weight;
        $data["Height"] = $height;
        $data["Width"] = $width;
        $data["Length"] = $length;
        
        $data["SendingAgentItemNumber"] = $order["id_order"];
        $data["ReferenceID"] = $order["reference"];
        $data["ConsigneeCompany"] = $order["company"];
        $data["ConsigneeName"] = $order["lastname"] ." ". $order["firstname"];
        $data["ConsigneeAddress1"] = $order["address1"];
        $data["ConsigneeAddress2"] = $order["address2"];
        $data["ConsigneeAddress3"] = "";
     
        $data["Town"] = $order["city"];
        
        if($order["state_name"] != "") 
            $data["State"] = $order["state_name"] .", ".$order["country_name"];
        else   
            $data["State"] = $order["country_name"];
            
        $data["Postcode"] = $order["postcode"];
        $data["ConsigneeContactNumber1"] = $order["phone_mobile"];
        $data["ConsigneeContactNumber2"] = $order["phone"];
        
        $data["Remarks"] = "";
        $data["ProductDesc"] = "Reference: ".$order["reference"];

        $result = Tools::GDEXSubmitDelivery($gdexURL, $orderId, $data);
  
        $gdex_status = "";
        $return_status = false;
        
        if($result === true) {
            $gdex_status = "Success";
            $return_status = true;
                
            Db::getInstance()->execute(
                'INSERT `'._DB_PREFIX_.'order_shipping`(id_order, weight, height, width, length, pieces, tracking_number, date_add, date_update,update_order_status)'.
                'VALUES ('.(int)$orderId.','.(float)$weight.','.(float)$height.','.(float)$width.','.(float)$length.','.(float)$pieces.',"'.pSQL($gdexCN).'",NOW(),NOW(), '.$updatestatus.')'
            );
        }
        else if ($result === false) {
            $gdex_status = "Failed";
        }
        else {
            $gdex_status = $result;
        }
        
        $update_sql = 'UPDATE `'._DB_PREFIX_.'gdex_cn` SET status="'.str_replace('"','\"',(string)$gdex_status).'" WHERE id_order='.(int)$orderId.' AND gdex_cn="'.pSQL($gdexCN).'"';
        Db::getInstance()->execute($update_sql);
        
        if($result === true) {
            if($order['current_state'] == 2)
                $this->ajaxUpdateOrderStatus(4, $orderId); 
            
            $this->updateOrderGDEXTrackingNumber($orderId, $gdexCN);
        }
        
        return $return_status;
    }

    public function updateOrderGDEXTrackingNumber($orderId, $tracking_number,$prefix='') 
    {
		if ($prefix)
			$tracking_number = $prefix.": ". $tracking_number;
		else
			$tracking_number = "GDEX: ". $tracking_number;
        $id_order_carrier = Db::getInstance()->getValue('SELECT id_order_carrier FROM `'._DB_PREFIX_.'order_carrier` WHERE id_order='. (int)$orderId);
        $order_carrier = new OrderCarrier($id_order_carrier);
  
        if (Validate::isLoadedObject($order_carrier))
        {
            $order = new Order($orderId); 
			$oldShippingNumber=$order->shipping_number;
            $order->shipping_number = ($order->shipping_number == "" || $order->shipping_number == null) ? $tracking_number : $order->shipping_number.",".$tracking_number;
		    $order->update();

		    // Update order_carrier
		    $order_carrier->tracking_number = ($order_carrier->tracking_number == "" || $order_carrier->tracking_number == null) ? pSQL($tracking_number) 
                            : $order_carrier->tracking_number.",".pSQL($tracking_number);
		    if ($order_carrier->update() && trim($oldShippingNumber)=='' && !($oldShippingNumber == $tracking_number) )
		    {
                // Send mail to customer
			    $customer = new Customer((int)$order->id_customer);
			    $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);
			    if (!Validate::isLoadedObject($customer))
				    return;
			    if (!Validate::isLoadedObject($carrier))
				    return;
			    $templateVars = array(
				    '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
				    '{firstname}' => $customer->firstname,
				    '{lastname}' => $customer->lastname,
				    '{id_order}' => $order->id,
				    '{shipping_number}' => $order->shipping_number,
				    '{order_name}' => $order->getUniqReference()
			    );
							
			    if (@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Package in transit', (int)$order->id_lang), $templateVars,
				    $customer->email, $customer->firstname.' '.$customer->lastname, null, null, null, null,
				    _PS_MAIL_DIR_, true, (int)$order->id_shop))
			    {
				    Hook::exec('actionAdminOrdersTrackingNumberUpdate', array('order' => $order, 'customer' => $customer, 'carrier' => $carrier), null, false, true, false, $order->id_shop);
			    }
            }
        }
    }

    public function cronUpdateGDEXTrackingNumber() {
        $sql = "SELECT A.id_order_shipping, A.id_order, A.tracking_number FROM "._DB_PREFIX_."order_shipping A ".
                "INNER JOIN "._DB_PREFIX_."orders B ON A.id_order = B.id_order WHERE B.current_state IN (3,4)";
        
        $result = Db::getInstance()->executeS($sql);
        $gdexTrackURL = Configuration::get('GDEX_TRACK_SHIPMENT_URL');
 
        foreach($result as $r)
        {
            $tempURL = str_replace("{GDEXCN}", $r["tracking_number"],$gdexTrackURL);
            $response = Tools::GDEXTrackDelivery($tempURL, $r["id_order"], $r["tracking_number"] ); 
            
            print_r("Tracking #:".$r["tracking_number"]);
            print_r($response);
            print_r("<br />");
            if($response !== false) {
                $update_sql = 'UPDATE `'._DB_PREFIX_.'order_shipping` '.
                                'SET status="'.$response['r']['StatusCode'].'"'.
                                    ',status_date="'.$response['r']['StatusDateTime'].'"'.
                                    ',date_update="'.date('Y-m-d H:i:s').'" '.
                                'WHERE id_order_shipping='.$r['id_order_shipping'];
                Db::getInstance()->execute($update_sql);
            }
        }
            
        $sql = "SELECT A.id_order, SUM(CASE WHEN A.status='FD' THEN 1 ELSE 0 END) delivered, COUNT(1) AS shipped, MAX(status_date) AS status_date ".
                    "FROM "._DB_PREFIX_."order_shipping A ".
                    "INNER JOIN "._DB_PREFIX_."orders B ON A.id_order = B.id_order ".
                    "WHERE A.update_order_status=1 AND B.current_state IN (3,4) ".
                    "GROUP BY A.id_order";
					
        $result = Db::getInstance()->executeS($sql);
 
        print_r("<br />Check for delivered order<br />");
        foreach($result as $r) {
            print_r("Order Id:".$r["id_order"]. "|Shipped count:".$r['shipped']."|Delivered count:".$r['delivered']."|Delivered date:".$r['status_date']);
            print_r("<br />");
            
            if($r['delivered'] == $r['shipped'])
            {
				echo "<br>updating to delivered ".$r['id_order']." start";
                $this->ajaxUpdateOrderStatus(5, $r['id_order'], $r['status_date']);
				echo "<br>updating to delivered ".$r['id_order']." done";
            }
        }
    }

    public function ajaxUpdateOrderStatus($id_order_state, $orderId, $deliveryDate = null) 
    {
        $order = new Order($orderId);
		$thisEmpId=1;
        
        if($this->context && isset($this->context)) {
            if($this->context->employee && isset($this->context->employee) && $this->context->employee->id > 0) {
                $thisEmpId = $this->context->employee->id;
            }
            else if($this->context->customer && isset($this->context->customer) && $this->context->customer->id > 0) {
                $thisCust=$this->context->customer->email;
		        $tempEmp = new Employee();
                $tempEmp=$tempEmp->getByEmail($thisCust);
                
                if ($tempEmp)
		            $thisEmpId=(int)$tempEmp->id;
            }
        }
        
        if (Validate::isLoadedObject($order)) {
            $new_history = new OrderHistory();
		    $new_history->id_order = (int)$orderId;
            $new_history->id_employee = $thisEmpId;
            $new_history->changeIdOrderState((int)$id_order_state, $order, false, $deliveryDate);
            if($deliveryDate == null) {
                $new_history->addWithemail(true, false);
            }
            else {
                $new_history->date_add = $deliveryDate;
                $new_history->addWithemail(false, false);
            }
        }
    }
    /* end GDEX Integration */
}
