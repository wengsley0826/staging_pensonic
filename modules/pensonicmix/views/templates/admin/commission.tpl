<div class="panel">
	<h3><i class="icon-tag"></i> {l s='Affiliate Commission'}</h3>
	<form action="{$currentIndex|escape}&amp;token={$currentToken|escape}" id="commission_form" method="post" class="form-horizontal well hidden-print" >
		<div class="row">
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Affiliate Code'}</label>
				<div class="col-lg-9">
					<input type="text" id="affiliate_code" name="affiliate_code" value="{$commission->affiliate_code|escape}" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Affiliate Name'}</label>
				<div class="col-lg-9">
					<input type="text" id="affiliate_name" name="affiliate_name" value="{$commission->affiliate_name|escape}" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Commission Percentage'}</label>
				<div class="col-lg-9">
					<input type="number"  min="0" step="0.01" id="commission_rate" name="commission_rate" value="{$commission->commission_rate}" />
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Max Commission Allowed'}</label>
				<div class="col-lg-9">
					<input type="number"  min="0"  step="0.01" id="commission_max" name="commission_max" value="{$commission->commission_max}" />
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Affiliate Category ID'}</label>
				<div class="col-lg-9">
				
					{assign var=camcat value=$commission->commission_category}
					<select name="commission_category" id="commission_category">
						<option>Choose one</option>
						{foreach from=$category item=categoryy name=myLoop}
						<option {if $camcat == $categoryy.id_category} selected="selected" {/if} value="{$categoryy.id_category}">{$categoryy.name}</option>
						{/foreach}
					</select>
					<br>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-lg-3">{l s='Is Hidden?'}</label>
				<div class="col-lg-9">
					<select name="hideLink" id="hideLink">
						<option value=0 {if $commission->hideLink==0}selected{else}{/if}>No</option>
						<option value=1 {if $commission->hideLink==1}selected{else}{/if}>Yes</option>
					</select>
				</div>
			</div>	
			
			<input type="hidden" id="commission_id" name="commission_id" value="{$commission->commission_id|escape}" />
		<button type="submit" class="btn btn-default pull-right" name="submitAddcommission" id="{$table|escape}_form_submit_btn">{l s='Save'}</button>
		
		</div>
	</form>
</div>