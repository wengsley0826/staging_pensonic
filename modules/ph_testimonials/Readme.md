# Simple Testimonials module created by PrestaHome Team

Changelog
----------------------------------------------------------------
Version 1.1.0 - Date January 16, 2016
--
	Improvements
		* Add alt tag to images
		* Display URL to website of the author if exists
		* Allow to format testimonial (typography, bolds, italics etc.) by administrator from back-office
		* Allw to set dedicated meta_title and meta_description for Testimonials page
		* Possibility to display random testimionals on the homepage



Version 1.0.3 - Date August 14, 2015
--
  	Improvements:
    	* Possibility to insert custom date_add which allows you to add past testimonials

Support
----------------------------------------------------------------
https://addons.prestashop.com/contact-community.php?id_product=18558