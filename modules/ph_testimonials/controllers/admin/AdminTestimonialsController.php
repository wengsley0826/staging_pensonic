<?php
require_once _PS_MODULE_DIR_ . 'ph_testimonials/ph_testimonials.php';

class AdminTestimonialsController extends ModuleAdminController
{
    public $image_url;

    public function __construct()
    {

        $this->table = 'prestahome_testimonial';
        $this->className = 'PrestaHomeTestimonial';
        $this->lang = false;

        $this->bootstrap = true;

        $this->context = Context::getContext();

        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            $this->image_url = _PS_CORE_IMG_DIR_ . 'testimonials';
        } else {
            $this->image_url = _PS_IMG_DIR_ . 'testimonials';
        }

       
        $this->fieldImageSettings = array(
            'name' => 'image',
            'dir' => 'testimonials'
        );

        $this->fields_list = array(
            'id_prestahome_testimonial' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'image' => array(
                'title' => $this->l('Image'),
                'align' => 'center',
                'orderby' => false,
                'image' => 'testimonials',
                'search' => false,
            ),
            'author_name' => array(
                'title' => $this->l('Author'),
                'filter_key' => 'a!author_name',
            ),
            'author_info' => array(
                'title' => $this->l('Author info'),
                'width' => 'auto',
                'filter_key' => 'a!author_info',
            ),
            'author_url' => array(
                'title' => $this->l('URL'),
                'width' => 'auto',
                'filter_key' => 'a!author_url',
            ),
            'author_email' => array(
                'title' => $this->l('E-mail'),
                'width' => 'auto',
                'filter_key' => 'a!author_email',
            ),
            'content' => array(
                'title' => $this->l('Testimonial content'),
                'width' => 'auto',
                'filter_key' => 'a!content',
                'callback' => 'getCleanTestimonialContent',
                'callback_object' => $this
            ),
            'is_featured' => array(
                'title' => $this->l('Featured?'),
                'orderby' => false,
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'type' => 'bool',
                'active' => 'is_featured'
            ),
            'active' => array(
                'title' => $this->l('Displayed'),
                'class' => 'fixed-width-xs',
                'active' => 'status',
                'align' => 'center',
                'type' => 'bool',
                'orderby' => false)
        );

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $this->specificConfirmDelete = false;

        parent::__construct();
    }

    public function getCleanTestimonialContent($content)
    {
        return strip_tags($content);
    }

    public function init()
    {
        parent::init();

        Shop::addTableAssociation($this->table, array('type' => 'shop'));

        if (Shop::getContext() == Shop::CONTEXT_SHOP) {
            $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'prestahome_testimonial_shop` sa ON (a.`id_prestahome_testimonial` = sa.`id_prestahome_testimonial` AND sa.id_shop = '.(int)$this->context->shop->id.') ';
        }

        if (Shop::getContext() == Shop::CONTEXT_SHOP && Shop::isFeatureActive()) {
            $this->_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        }
    }

    public function setMedia()
    {
        $this->addJqueryUI('ui.datepicker');
        parent::setMedia();
    }

    public function renderList()
    {
        if (isset($this->_filter) && trim($this->_filter) == '') {
            $this->_filter = $this->original_filter;
        }

        $this->addRowAction('add');
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        if (Tools::isSubmit('submitBulkdelete'.$this->table) || Tools::isSubmit('delete'.$this->table)) {
            $this->tpl_list_vars['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
            $this->tpl_list_vars['POST'] = $_POST;
        }

        return parent::renderList();
    }

    public function initFormToolBar()
    {
        unset($this->toolbar_btn['back']);
        $this->toolbar_btn['save-and-stay'] = array(
            'short' => 'SaveAndStay',
            'href' => '#',
            'desc' => $this->l('Save and stay'),
        );
        $this->toolbar_btn['back'] = array(
            'href' => self::$currentIndex.'&token='.Tools::getValue('token'),
            'desc' => $this->l('Back to list'),
        );
    }

    public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_title = $this->l('Testimonials');
        

        if ($this->display == 'add' || $this->display == 'edit') {
            $this->page_header_toolbar_btn['back_to_list'] = array(
                'href' => Context::getContext()->link->getAdminLink('AdminPrestaHomeTestimonials'),
                'desc' => $this->l('Back to list', null, null, false),
                'icon' => 'process-icon-back'
            );
        }

       
        if (!isset($this->display)) {
            $this->page_header_toolbar_btn['new_testimonial'] = array(
                'href' => self::$currentIndex.'&addprestahome_testimonial&token='.$this->token,
                'desc' => $this->l('Add new testimonial', null, null, false),
                'icon' => 'process-icon-new'
            );

            $this->page_header_toolbar_btn['configuration'] = array(
                'href' => Context::getContext()->link->getAdminLink('AdminModules').'&configure='.$this->module->name.'&module_name='.$this->module->name,
                'desc' => $this->l('Configuration', null, null, false),
                'icon' => 'process-icon-configure'
            );
        }
            
        parent::initPageHeaderToolbar();
    }

    public function renderForm()
    {
        $this->initToolbar();
        $obj = $this->loadObject(true);

        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $image = $this->image_url.'/'.$obj->id.'.jpg';
        $image_url = ImageManager::thumbnail($image, $this->table.'_'.(int)$obj->id.'.'.$this->imageType, 350, $this->imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        $switch_field = 'radio';
        $switch_field_class = 't';
        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            $this->image_url = _PS_CORE_IMG_DIR_ . 'testimonials';
            $switch_field = 'switch';
            $switch_field_class = '';
        }

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Testimonial'),
                'icon' => 'icon-tags'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Author name'),
                    'name' => 'author_name',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Author information'),
                    'name' => 'author_info',
                    'description' => $this->l('position, company name etc.'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('URL'),
                    'name' => 'author_url',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('E-mail'),
                    'name' => 'author_email',
                ),
                array(
                    'type' => $switch_field,
                    'class' => $switch_field_class,
                    'label' => $this->l('Featured?'),
                    'name' => 'is_featured',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'is_featured_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'is_featured_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                array(
                    'type' => $switch_field,
                    'class' => $switch_field_class,
                    'label' => $this->l('Displayed'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Testimonial content'),
                    'name' => 'content',
                    'id' => 'testimonial_content',
                    'autoload_rte' => true,
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Image'),
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'delete_url' => self::$currentIndex.'&'.$this->identifier.'='.$obj->id.'&token='.$this->token.'&deleteImage=1',
                    'hint' => $this->l('Upload a category logo from your computer.'),
                ),
                array(
                    'type' => 'datetime',
                    'label' => $this->l('Testimonials date'),
                    'desc' => $this->l('This allows you to set custom date for testimonials and for example add testimonials from the past'),
                    'name' => 'date_add',
                    'id' => 'date_add',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'submitAdd'.$this->table
            )
        );

        // Display this field only if multistore option is enabled AND there are several stores configured
        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }

        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $image = ImageManager::thumbnail($this->image_url.'/'.$obj->id.'.jpg', $this->table.'_'.(int)$obj->id.'.'.$this->imageType, 350, $this->imageType, true);

        $this->fields_value = array(
            'image' => $image ? $image : false,
            'size' => $image ? filesize($this->image_url.'/'.$obj->id.'.jpg') / 1000 : false,
            'date_add' => $obj->id ? $obj->date_add : date('Y-m-d H:i:s'),
        );

        return parent::renderForm();
    }

    public function postProcess()
    {
        if (!in_array($this->display, array('edit', 'add'))) {
            $this->multishop_context_group = false;
        }
        if (Tools::isSubmit('forcedeleteImage') || (isset($_FILES['image']) && $_FILES['image']['size'] > 0) || Tools::getValue('deleteImage')) {
            $this->processForceDeleteImage();
            if (Tools::isSubmit('forcedeleteImage')) {
                Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminPrestaHomeTestimonials').'&conf=7');
            }
        }

        if (Tools::isSubmit('is_featuredprestahome_testimonial') && Tools::getValue('id_prestahome_testimonial')) {
            $testimonial = new PrestaHomeTestimonial((int)Tools::getValue('id_prestahome_testimonial'));
            $testimonial->is_featured = !$testimonial->is_featured;
            $testimonial->update();
            Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminPrestaHomeTestimonials').'&conf=4');
        }

        return parent::postProcess();
    }

    public function processForceDeleteImage()
    {
        $testimonial = $this->loadObject(true);
        if (Validate::isLoadedObject($testimonial)) {
            $testimonial->deleteImage(true);
        }
    }

    protected function postImage($id)
    {
        $ret = parent::postImage($id);
        if (($id_prestahome_testimonial = (int)Tools::getValue('id_prestahome_testimonial')) &&
            isset($_FILES) && count($_FILES) && $_FILES['image']['name'] != null &&
            file_exists($this->image_url.'/'.$id_prestahome_testimonial.'.jpg')) {
            $images_types = ImageType::getImagesTypes('products');
            foreach ($images_types as $image_type) {
                @ImageManager::resize(
                    $this->image_url.'/'.$id_prestahome_testimonial.'.jpg',
                    $this->image_url.'/'.$id_prestahome_testimonial.'-'.Tools::stripslashes($image_type['name']).'.jpg',
                    (int)$image_type['width'],
                    (int)$image_type['height']
                );
            }
        }

        return $ret;
    }

    public function processAdd()
    {
        $object = parent::processAdd();
        
        $this->updateAssoShop($object->id);
        
        return $object;
    }

    public function processUpdate()
    {

        $object = parent::processUpdate();

        $this->updateAssoShop($object->id);

        return $object;
    }
}
