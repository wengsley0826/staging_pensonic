<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
/*
* @author    Krystian Podemski <podemski.krystian@gmail.com>
* @copyright  Copyright (c) 2014-2015 Krystian Podemski - www.PrestaHome.com
* @license    You only can use module, nothing more!
*/
require_once _PS_MODULE_DIR_ . 'ph_testimonials/ph_testimonials.php';

class PH_TestimonialsListModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        parent::init();
    }

    public function initContent()
    {
        parent::initContent();

        $this->assignMetaTags();

        $this->context->smarty->assign(array(
            'testimonials' => PrestaHomeTestimonial::getAll(),
            'module_dir' => _MODULE_DIR_.'ph_testimonials/',
        ));

        $this->setTemplate('list.tpl');
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitNewTestimonial')) {
            $extension = array('.jpeg', '.gif', '.jpg', '.png');

            $fileAttachment = Tools::fileAttachment('author_image');

            $testimonial_content    = Tools::getValue('content');
            $testimonial_author     = Tools::getValue('author_name');
            $testimonial_info       = Tools::getValue('author_info');
            $testimonial_url        = Tools::getValue('author_url');

            if (!($testimonial_email = trim(Tools::getValue('author_email'))) || !Validate::isEmail($testimonial_email)) {
                $this->errors[] = Tools::displayError('Invalid email address.');
            } elseif (!$testimonial_content) {
                $this->errors[] = Tools::displayError('Testimonial content cannot be blank.');
            } elseif (!Validate::isCleanHtml($testimonial_content)) {
                $this->errors[] = Tools::displayError('Invalid testimonial content.');
            } elseif (!$testimonial_author) {
                $this->errors[] = Tools::displayError('Author name cannot be blank.');
            } elseif (!Validate::isUrlOrEmpty($testimonial_url)) {
                $this->errors[] = Tools::displayError('URL is invalid.');
            } elseif (!empty($fileAttachment['name']) && $fileAttachment['error'] != 0) {
                $this->errors[] = Tools::displayError('An error occurred during the file-upload process.');
            } elseif (!empty($fileAttachment['name']) && !in_array(Tools::strtolower(Tools::substr($fileAttachment['name'], -4)), $extension) && !in_array(Tools::strtolower(Tools::substr($fileAttachment['name'], -5)), $extension)) {
                $this->errors[] = Tools::displayError('Bad file extension');
            } else {
                if (!count($this->errors)) {
                    $testimonial_instance = new PrestaHomeTestimonial();

                    $testimonial_instance->author_name = $testimonial_author;
                    $testimonial_instance->author_email = $testimonial_email;
                    $testimonial_instance->author_info = $testimonial_info;
                    $testimonial_instance->author_url = $testimonial_url;
                    $testimonial_instance->content = $testimonial_content;

                    $customer = $this->context->customer;
                    if (!$customer->id) {
                        $customer->getByEmail($testimonial_email);
                    }

                    if (isset($customer->id)) {
                        $testimonial_instance->id_customer = (int)$customer->id;
                    }

                    if (isset($customer->id_guest)) {
                        $testimonial_instance->id_guest = (int)$customer->id_guest;
                    }

                    $testimonial_instance->is_featured = 0;
                    $testimonial_instance->active = 0;

                    $testimonial_instance->add();

                    if (Configuration::get('PH_TESTIMONIALS_NOTIFICATIONS')) {
                        $toName = (string)Configuration::get('PS_SHOP_NAME');
                        $fromName = (string)Configuration::get('PS_SHOP_NAME');
                        $to = Configuration::get('PH_TESTIMONIALS_COMMENT_EMAIL');
                        $from = Configuration::get('PS_SHOP_EMAIL');

                        Mail::Send(
                            $this->context->language->id,
                            'new_testimonial',
                            Mail::l('New testimonial from customer in your store', $this->context->language->id),
                            array(
                                '{author_name}' => $testimonial_author,
                                '{author_email}' => $testimonial_email,
                                '{testimonial_content}' => $testimonial_content,
                            ),
                            $to,
                            $toName,
                            $from,
                            $fromName,
                            null,
                            null,
                            _PS_MODULE_DIR_.'ph_testimonials/mails/'
                        );
                    }

                    if (!empty($fileAttachment['tmp_name']) && isset($testimonial_instance->id)) {
                        $image_url = _PS_CORE_IMG_DIR_ . 'testimonials';

                        $tmp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                        if (!$tmp_name) {
                            return Tools::displayError('Problem with upload file');
                        }

                        if (!move_uploaded_file($fileAttachment['tmp_name'], $tmp_name)) {
                            return Tools::displayError('Problem with upload file');
                        }

                        if (!ImageManager::checkImageMemoryLimit($tmp_name)) {
                            $this->errors[] = Tools::displayError('Due to memory limit restrictions, this image cannot be loaded. Please increase your memory_limit value via your server\'s configuration settings. ');
                        }

                        // move original
                        ImageManager::resize(
                            $tmp_name,
                            $image_url.'/'.(int)$testimonial_instance->id.'.jpg',
                            null,
                            null
                        );

                        $images_types = ImageType::getImagesTypes('products');
                        foreach ($images_types as $image_type) {
                            ImageManager::resize(
                                $tmp_name,
                                $image_url.'/'.(int)$testimonial_instance->id.'-'.Tools::stripslashes($image_type['name']).'.jpg',
                                (int)$image_type['width'],
                                (int)$image_type['height']
                            );
                        }
                    }

                    if (count($this->errors) > 1) {
                        array_unique($this->errors);
                    } else {
                        Tools::redirect(Context::getContext()->link->getModuleLink('ph_testimonials', 'list', array('confirmation' => '1')));
                    }
                }
                
            }

        }
        return parent::postProcess();
    }

    protected function assignMetaTags()
    {
        if (Configuration::get('PH_TESTIMONIALS_META_TITLE', $this->context->language->id)) {
            $meta_title = Configuration::get('PH_TESTIMONIALS_META_TITLE', $this->context->language->id);
            $this->context->smarty->assign('meta_title', str_replace(array('%SHOP_NAME%'), array(Configuration::get('PS_SHOP_NAME')), $meta_title));
        } else {
            $this->context->smarty->assign('meta_title', $this->module->l('Testimonials'));
        }

        if (Configuration::get('PH_TESTIMONIALS_META_DESCRIPTION', $this->context->language->id)) {
            $meta_title = Configuration::get('PH_TESTIMONIALS_META_DESCRIPTION', $this->context->language->id);
            $this->context->smarty->assign('meta_description', str_replace(array('%SHOP_NAME%'), array(Configuration::get('PS_SHOP_NAME')), $meta_title));
        }
        
        $this->context->smarty->assign('meta_keywords', "mum's testimonial and online reviews,online baby shop,best parenting websites in malaysia,");
    }
}
