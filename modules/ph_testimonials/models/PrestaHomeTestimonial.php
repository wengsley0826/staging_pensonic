<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
/*
* @author    Krystian Podemski <podemski.krystian@gmail.com>
* @copyright  Copyright (c) 2014-2015 Krystian Podemski - www.PrestaHome.com
* @license    You only can use module, nothing more!
*/
require_once _PS_MODULE_DIR_ . 'ph_testimonials/ph_testimonials.php';

class PrestaHomeTestimonial extends ObjectModel
{
    /**
    @since 1.0.0
    **/
    public $id;
    public $id_prestahome_testimonial;
    public $id_customer;
    public $id_guest;
    public $author_name;
    public $author_info;
    public $author_url;
    public $author_email;
    public $rating = 0;
    public $content;
    public $is_featured = 0;
    public $active = 1;
    public $date_add;
    public $date_upd;

    public $id_image = 'default';
    public $testimonial_image;

    public static $definition = array(
        'table' => 'prestahome_testimonial',
        'primary' => 'id_prestahome_testimonial',
        'multilang' => false,
        'fields' => array(
            'id_customer'       => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_guest'          => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'author_name'       => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true, 'size' => 60),
            'author_info'       => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255),
            'author_url'        => array('type' => self::TYPE_STRING, 'validate' => 'isUrlOrEmpty', 'size' => 255),
            'author_email'      => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 255),
            'rating'            => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'content'           => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'required' => true),
            'is_featured'       => array('type' => self::TYPE_BOOL),
            'active'            => array('type' => self::TYPE_BOOL),
            'date_add'          => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd'          => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
        
        if (isset($this->id) && $this->id > 0) {
            $this->id_image = ($id && file_exists(_PS_CORE_IMG_DIR_.'testimonials/'.(int)$id.'.jpg')) ? (int)$id : false;
            $this->image_dir =  _PS_CORE_IMG_DIR_ . 'testimonials/';
            $this->testimonial_image = $this->getImageLink('testimonial', $this->id_image, ImageType::getFormatedName('home'));
        }
    }

    public function add($autodate = true, $null_values = false)
    {
        $ret = parent::add(false, $null_values);
        return $ret;
    }

    public function delete()
    {
        if ((int)$this->id === 0) {
            return false;
        }

        return $this->deleteImages() && parent::delete();
    }

    public function deleteImages()
    {
        $response = true;

        $images = glob(_PS_CORE_IMG_DIR_ . 'testimonials/'.$this->id.'*');

        foreach ($images as $image) {
            $response &= @unlink($image);
        }
        return $response;
    }

    public static function getAll($id_shop = null, Context $context = null, $filter = null, $selected = array(), $limit = false, $type = 'recent', $random = false)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if (!$id_shop) {
            $id_shop = $context->shop->id;
        }

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('prestahome_testimonial', 'pt');
        $sql->innerJoin('prestahome_testimonial_shop', 'pts', 'pt.id_prestahome_testimonial = pts.id_prestahome_testimonial AND pts.id_shop = '.(int)$id_shop);

        if ($filter) {
            $sql->where('pt.id_prestahome_testimonial '.pSQL($filter).' ('.join(',', pSQL($selected)).')');
        }

        if ($type == 'featured') {
            $sql->where('pt.is_featured = 1');
        }

        $sql->where('pt.active = 1');

        if ($limit) {
            $sql->limit($limit, 0);
        }

        if ($random) {
            $sql->orderBy('RAND()');
        } else {
            $sql->orderBy('pt.date_add DESC');
        }

        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            $image_url = _PS_CORE_IMG_DIR_ . 'testimonials';
        } else {
            $image_url = _PS_IMG_DIR_ . 'testimonials';
        }

        if ($results) {
            foreach ($results as &$result) {
                if (!file_exists($image_url.'/'.(int)$result['id_prestahome_testimonial'].'.jpg')) {
                    $result['image'] = 0;
                } else {
                    $result['image'] = self::getImageLink((int)$result['id_prestahome_testimonial'], ImageType::getFormatedName('home'));
                }
            }
        }

        return $results;
    }

    public static function getImageLink($id_prestahome_testimonial, $type = null)
    {
        $uri_path = __PS_BASE_URI__ . 'img/testimonials/'.(int)$id_prestahome_testimonial.'-'.$type.'.jpg';
        return Context::getContext()->link->protocol_content.Tools::getMediaServer($uri_path).$uri_path;
    }
}
