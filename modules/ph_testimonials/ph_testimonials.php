<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2014 PrestaShop SA
*  @version   Release: $Revision$
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
/**
* @author    Krystian Podemski <podemski.krystian@gmail.com>
* @copyright Copyright (c) 2014-2015 Krystian Podemski - www.PrestaHome.com
* @license   You only can use module, nothing more!
*/
if (!defined('_PS_VERSION_')) {
    exit;
}

if (file_exists(_PS_MODULE_DIR_ . 'ph_testimonials/models/PrestaHomeTestimonial.php')) {
    require_once _PS_MODULE_DIR_ . 'ph_testimonials/models/PrestaHomeTestimonial.php';
}

class PH_Testimonials extends Module
{
    private $html = '';
    public $fields_form;
    public $fields_value;
    public $validation_errors = array();

    protected static $cache_testimonials;

    public static $cfg_prefix = 'PH_TESTIMONIALS_';
    
    public function __construct()
    {
        $this->name = 'ph_testimonials';
        $this->tab = 'front_office_features';
        $this->version = '1.1.0';
        $this->author = 'PrestaHome';
        $this->need_instance = 0;
        $this->is_configurable = 1;
        $this->ps_versions_compliancy['min'] = '1.5.6.0';
        $this->ps_versions_compliancy['max'] = _PS_VERSION_;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        $this->module_key = 'cd28301fc4a10bee8d611b337d9883ce';

        parent::__construct();

        $this->displayName = $this->l('Testimonials Showcase');
        $this->description = $this->l('Display Testimonials from your customers');

        $this->confirmUninstall = $this->l('Are you sure you want to delete this module ?');

        //if ($this->id && !$this->isRegisteredInHook('moduleRoutes')) {
        //    $this->registerHook('moduleRoutes');
        //}
    }

    public function prepareValueForLangs($value)
    {
        $languages = Language::getLanguages(false);

        $output = array();
        foreach ($languages as $language) {
            $output[$language['id_lang']] = $value;
        }

        return $output;
    }

    public function getDefaults()
    {
        return array(
            'TITLE' => $this->prepareValueForLangs('Testimonials from our customers'),
            'TITLE_COLUMN' => $this->prepareValueForLangs('Testimonials'),
            //'SLUG' => $this->prepareValueForLangs('testimonials'),
            'HOMEPAGE_TYPE' => 'recent',
            'COLUMN_TYPE' => 'recent',
            'ITEMS' => 25,
            'ITEMS_COLUMN' => 2,
            'OWL_FROM_THEME' => false,
            'AUTOPLAY' => 5000,
            'CAROUSEL' => true,
            'NAVIGATION' => true,
            'NOTIFICATIONS' => true,
            'COMMENT_EMAIL' => Configuration::get('PS_SHOP_EMAIL'),
            'META_TITLE' => $this->prepareValueForLangs('Testimonials from our customers - %SHOP_NAME%'),
            'META_DESCRIPTION' => $this->prepareValueForLangs('See latest testimonials from our customers'),
            'RANDOM' => 0,
        );
    }

    public function install()
    {
        $this->_clearCache('*');

        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        // Hooks & Install
        return (parent::install()
                && $this->prepareModuleSettings()
                && $this->registerHook('displayLeftColumn')
                && $this->registerHook('displayHome')
                && $this->registerHook('displayHeader')
                //&& $this->registerHook('moduleRoutes')
            );
    }

    public function prepareModuleSettings()
    {
        /**

            Default configuration

        **/
        foreach ($this->getDefaults() as $key => $value) {
            Configuration::updateValue(self::$cfg_prefix.$key, $value, true);
        }

        /**

            Database

        **/
        $sql = array();
        include(dirname(__file__).'/init/install_sql.php');
        foreach ($sql as $s) {
            if (!Db::getInstance()->Execute($s)) {
                die('Error while creating DB');
            }
        }

        /**

            New tab

        **/
        $tab = new Tab();

        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Testimonials');
        }

        $tab->class_name = 'AdminTestimonials';
        $tab->id_parent = Tab::getIdFromClassName('AdminParentModules');
        $tab->module = $this->name;
        $tab->add();

        /**

            Catalog for images

        **/
        if (!file_exists(_PS_IMG_DIR_.'testimonials')) {
            mkdir(_PS_IMG_DIR_.'testimonials', 0777, true);
        }


        /**

            For theme developers - you're welcome!

        **/
        if (file_exists(_PS_MODULE_DIR_.'ph_testimonials/init/my-install.php')) {
            include_once _PS_MODULE_DIR_.'ph_testimonials/init/my-install.php';
        }
       
        return true;
    }

    public function uninstall()
    {
        $this->_clearCache('*');

        if (!parent::uninstall()) {
            return false;
        }

        // Database
        $sql = array();
        include(dirname(__file__) . '/init/uninstall_sql.php');
        foreach ($sql as $s) {
            if (!Db::getInstance()->Execute($s)) {
                return false;
            }
        }

        foreach ($this->getDefaults() as $key => $value) {
            Configuration::deleteByName(self::$cfg_prefix.$key);
        }

        // Tabs
        $idTabs = array();
        $idTabs[] = Tab::getIdFromClassName('AdminPrestaHomeTestimonials');

        foreach ($idTabs as $idTab) {
            if ($idTab) {
                $tab = new Tab($idTab);
                $tab->delete();
            }
        }

        /**

            Empty images folder

        **/
        @array_map('unlink', glob(_PS_IMG_DIR_.'testimonials/*'));

        // For theme developers - you're welcome!
        if (file_exists(_PS_MODULE_DIR_.'ph_testimonials/init/my-uninstall.php')) {
            include_once _PS_MODULE_DIR_.'ph_testimonials/init/my-uninstall.php';
        }

        return true;
    }

    public function _clearCache($template, $cache_id = null, $compile_id = null)
    {
        parent::_clearCache('home.tpl');
    }

    public function getContent()
    {
        $id_shop = (int)$this->context->shop->id;

        $this->initFieldsForm();
        if (Tools::getIsset('save'.$this->name)) {
            $multiLangFields = array();
            foreach ($this->getDefaults() as $field_name => $field_value) {
                if (is_array($field_value)) {
                    $multiLangFields[] = self::$cfg_prefix.$field_name;
                }
            }

            foreach ($_POST as $key => $value) {
                $fieldName = Tools::substr($key, 0, -2);

                if (in_array($fieldName, $multiLangFields)) {
                    $thisFieldValue = array();
                    foreach (Language::getLanguages(true) as $language) {
                        if (Tools::getIsset($fieldName.'_'.$language['id_lang'])) {
                            $thisFieldValue[$language['id_lang']] = Tools::getValue($fieldName.'_'.$language['id_lang']);
                        }
                    }
                    $_POST[$fieldName] = $thisFieldValue;
                }
            }

            foreach ($this->getDefaults() as $field_name => $field_value) {
                if (is_array($field_value)) {
                    Configuration::updateValue($field_name, ${$field_name}, true);
                }
            }
                

            foreach ($this->fields_form as $form) {
                foreach ($form['form']['input'] as $field) {
                    if (isset($field['validation'])) {
                        $errors = array();
                        $value = Tools::getValue($field['name']);

                        if (isset($field['required']) && $field['required'] && $value == false && (string)$value != '0') {
                            $errors[] = sprintf(Tools::displayError('Field "%s" is required.'), $field['label']);
                        } elseif ($value) {
                            if (!Validate::$field['validation']($value)) {
                                $errors[] = sprintf(Tools::displayError('Field "%s" is invalid.'), $field['label']);
                            }
                        }

                        // Set default value
                        if ($value === false && isset($field['default_value'])) {
                            $value = $field['default_value'];
                        }
                            
                        if (count($errors)) {
                            $this->validation_errors = array_merge($this->validation_errors, $errors);
                        } elseif ($value==false) {
                            switch ($field['validation']) {
                                case 'isUnsignedId':
                                case 'isUnsignedInt':
                                case 'isInt':
                                case 'isBool':
                                    $value = 0;
                                    break;
                                default:
                                    $value = '';
                                    break;
                            }
                            Configuration::updateValue($field['name'], $value, true);
                        } else {
                            Configuration::updateValue($field['name'], $value, true);
                        }
                    }
                }
            }

            if (count($this->validation_errors)) {
                $this->html .= $this->displayError(implode('<br/>', $this->validation_errors));
            } else {
                $this->html .= Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&conf=6&token='.Tools::getAdminTokenLite('AdminModules'));
            }
        }

        $helper = $this->initForm();
        foreach ($this->getDefaults() as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $lang => $val) {
                    $helper->fields_value[self::$cfg_prefix.$key][(int)$lang] = Tools::getValue(self::$cfg_prefix.$key.'_'.(int)$lang, Configuration::get(self::$cfg_prefix.$key, (int)$lang));
                }
            } else {
                $helper->fields_value[self::$cfg_prefix.$key] = Configuration::get(self::$cfg_prefix.$key);
            }
        }

        return $this->html.$helper->generateForm($this->fields_form);
    }

    protected function initFieldsForm()
    {
        $from = array();
        $from[] = array('name' => $this->l('Recent'), 'id' => 'recent');
        $from[] = array('name' => $this->l('Featured'), 'id' => 'featured');

        $switch_field = 'radio';
        $switch_field_class = 't';
        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            $switch_field = 'switch';
            $switch_field_class = '';
        }

        $i = 0;
        $this->fields_form[$i]['form'] = array(
            'legend' => array(
                'title' => $this->l('General Settings'),
            ),
            'input' => array(
                //array(
                //    'type' => 'text',
                //    'label' => $this->l('URL slug to list of all testimonials:'),
                //    'name' => self::$cfg_prefix.'SLUG',
                //    'lang' => true,
                //    'validation' => 'isCleanHtml',
                //),
                array(
                    'type' => $switch_field,
                    'label' => $this->l('Enable notifications about new testimonials?'),
                    'name' => self::$cfg_prefix.'NOTIFICATIONS',
                    'is_bool' => true,
                    'class' => $switch_field_class,
                    'values' => array(
                        array(
                            'id' => self::$cfg_prefix.'NOTIFICATIONS_on',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                            
                        ),

                        array(
                            'id' => self::$cfg_prefix.'NOTIFICATIONS_off',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                    'validation' => 'isBool',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('E-mail for notifications:'),
                    'name' => self::$cfg_prefix.'COMMENT_EMAIL',
                    'validation' => 'isEmail',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $i++;

        $this->fields_form[$i]['form'] = array(
            'legend' => array(
                'title' => $this->l('SEO'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta title on testimonials list:'),
                    'name' => self::$cfg_prefix.'META_TITLE',
                    'desc' => $this->l('You can use %SHOP_NAME% to display shop name from your configuration'),
                    'lang' => true,
                    'validation' => 'isCleanHtml',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta description on testimonials list:'),
                    'name' => self::$cfg_prefix.'META_DESCRIPTION',
                    'desc' => $this->l('You can use %SHOP_NAME% to display shop name from your configuration'),
                    'lang' => true,
                    'validation' => 'isCleanHtml',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $i++;

        $this->fields_form[$i]['form'] = array(
            'legend' => array(
                'title' => $this->l('Testimonials on homepage'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title:'),
                    'name' => self::$cfg_prefix.'TITLE',
                    'lang' => true,
                    'validation' => 'isCleanHtml',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Show:'),
                    'name' => self::$cfg_prefix.'HOMEPAGE_TYPE',
                    'validation' => 'isAnything',
                    'options' => array(
                        'query' => $from,
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Visible items:'),
                    'name' => self::$cfg_prefix.'ITEMS',
                    'size' => 2,
                    'required' => true,
                    'validation' => 'isUnsignedInt',
                ),
                array(
                    'type' => $switch_field,
                    'class' => $switch_field_class,
                    'label' => $this->l('Randomize?'),
                    'name' => self::$cfg_prefix.'RANDOM',
                    'desc' => $this->l('This will always randomize displayed testimonials'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => self::$cfg_prefix.'RANDOM_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => self::$cfg_prefix.'RANDOM_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'validation' => 'isBool',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $i++;

        $this->fields_form[$i]['form'] = array(
            'legend' => array(
                'title' => $this->l('Testimonials in column'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title:'),
                    'name' => self::$cfg_prefix.'TITLE_COLUMN',
                    'lang' => true,
                    'validation' => 'isCleanHtml',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Show:'),
                    'name' => self::$cfg_prefix.'COLUMN_TYPE',
                    'validation' => 'isAnything',
                    'options' => array(
                        'query' => $from,
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Visible items:'),
                    'name' => self::$cfg_prefix.'ITEMS_COLUMN',
                    'size' => 2,
                    'required' => true,
                    'validation' => 'isUnsignedInt',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $i++;

        $this->fields_form[$i]['form'] = array(
            'legend' => array(
                'title' => $this->l('Carousel Settings'),
            ),
            'input' => array(
                array(
                    'type' => $switch_field,
                    'class' => $switch_field_class,
                    'label' => $this->l('Use carousel?'),
                    'name' => self::$cfg_prefix.'CAROUSEL',
                    'desc' => $this->l('Show testimonials as carousel on homepage?'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => self::$cfg_prefix.'CAROUSEL_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => self::$cfg_prefix.'CAROUSEL_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        ),
                    ),
                    'validation' => 'isBool',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Autoplay:'),
                    'name' => self::$cfg_prefix.'AUTOPLAY',
                    'size' => 2,
                    'desc' => $this->l('In ms, for example 5000 to play every 5 seconds. Set to 0 to disable Autoplay.'),
                    'required' => true,
                    'validation' => 'isUnsignedInt',
                ),
                array(
                    'type' => $switch_field,
                    'class' => $switch_field_class,
                    'label' => $this->l('Navigation:'),
                    'name' => self::$cfg_prefix.'NAVIGATION',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => self::$cfg_prefix.'NAVIGATION_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => self::$cfg_prefix.'NAVIGATION_off',
                            'value' => 0,
                            'label' => $this->l('No')),
                    ),
                    'desc' => $this->l('Prev & Next links'),
                    'validation' => 'isBool',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $i++;

        $this->fields_form[$i]['form'] = array(
            'legend' => array(
                'title' => $this->l('Miscellaneous'),
            ),
            'input' => array(
                array(
                    'type' => $switch_field,
                    'class' => $switch_field_class,
                    'label' => $this->l('Load Owl Carousel from Theme?'),
                    'name' => self::$cfg_prefix.'OWL_FROM_THEME',
                    'desc' => $this->l('You need owlCarousel v1.3.2 at least'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => self::$cfg_prefix.'OWL_FROM_THEME_on',
                            'value' => 1,
                            'label' => $this->l('Theme')
                        ),
                        array(
                            'id' => self::$cfg_prefix.'OWL_FROM_THEME_off',
                            'value' => 0,
                            'label' => $this->l('Module')
                        ),
                    ),
                    'validation' => 'isBool',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $i++;
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'save'.$this->name;
        $helper->toolbar_btn =  array(
            'save' =>
            array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            )
        );
        return $helper;
    }

    public function assignModuleVariables()
    {
        foreach ($this->getDefaults() as $key => $value) {
            if (is_array($value)) {
                $this->smarty->assign(Tools::strtolower($key), Configuration::get(self::$cfg_prefix.$key, $this->context->language->id));
            } else {
                $this->smarty->assign(Tools::strtolower($key), Configuration::get(self::$cfg_prefix.$key));
            }
        }
    }

    public function hookDisplayHome($params)
    {
        $this->assignModuleVariables();

        $this->smarty->assign(array(
            'testimonials' => PrestaHomeTestimonial::getAll(null, null, null, array(), (int)Configuration::get(self::$cfg_prefix.'ITEMS'), Configuration::get(self::$cfg_prefix.'HOMEPAGE_TYPE'), Configuration::get(self::$cfg_prefix.'RANDOM')),
        ));

        return $this->display(__FILE__, 'home.tpl');
    }


    //public function hookModuleRoutes($params)
    //{
    //    $my_routes = array(
    //        /**
    //            Reviews list
    //        **/
    //        'module-ph_testimonials-list' => array(
    //            'controller' => 'list',
    //            'rule' => Configuration::get(self::$cfg_prefix.'SLUG', (int)Context::getContext()->language->id),
    //            'keywords' => array(),
    //            'params' => array(
    //                'fc' => 'module',
    //                'module' => 'ph_testimonials',
    //            ),
    //        ),
    //    );

    //    return $my_routes;
    //}

    public function hookDisplayLeftColumn($params)
    {
        $this->assignModuleVariables();

        $this->smarty->assign(array(
            'testimonials' => PrestaHomeTestimonial::getAll(null, null, null, array(), (int)Configuration::get(self::$cfg_prefix.'ITEMS_COLUMN'), Configuration::get(self::$cfg_prefix.'COLUMN_TYPE')),
        ));

        return $this->display(__FILE__, 'column.tpl');
    }

    public function hookDisplayRightColumn($params)
    {
        return $this->hookDisplayLeftColumn($params);
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->addCSS($this->_path.'views/css/ph_testimonials.css');
        //$this->context->controller->addCSS($this->_path.'views/css/custom.css');
        $this->context->controller->addJS($this->_path.'views/js/ph_testimonials.js');
        //$this->context->controller->addJS($this->_path.'views/js/custom.js');

        if (!Configuration::get(self::$cfg_prefix.'OWL_FROM_THEME')) {
            $this->context->controller->addJS($this->_path.'views/js/owl.carousel.min.js', 'all');
            $this->context->controller->addCSS($this->_path.'views/css/owlCarousel.css');
        }
    }

    public function getConfigPrefix()
    {
        return self::$cfg_prefix;
    }
}
