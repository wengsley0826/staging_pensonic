{if $testimonials}
<div id="ph_testimonials_module">
	<h1 class="page-heading">
		<span class="cat-name">
			{$title|escape:'html':'UTF-8'}
		</span>
		<div class="testimonialButton">
			<a href="{$link->getModuleLink('ph_testimonials', 'list')|escape:'html':'UTF-8'}" title="{l s="View All Testimonials" mod="ph_testimonials"}">
				{l s="See All Reviews" mod="ph_testimonials"}
			</a>
			{*
			<a href="{$link->getModuleLink('ph_testimonials', 'list')|escape:'html':'UTF-8'}#addtestimonial" title="{l s="Add Testimonial" mod="ph_testimonials"}">
				{l s="Add Testimonial" mod="ph_testimonials"}
			</a>
			*}
		</div>
	</h1>

	<div class="testimonials-carousel-wrapper owl-carousel owl-theme clearfix clearBoth" id="ph_testimonials">
		{foreach $testimonials as $testimonial name='testimonialsCarousel'}
		<div class="testimonials-carousel-item">
			<div class="row">
            
            <ul class="landing-page-testimonial">
            
				{if $testimonial.image}
			<li>
            <div class="image-wrapper">
						<img src="{$testimonial.image|escape:'html':'UTF-8'}" alt="{$testimonial.author_name|escape:'html':'UTF-8'}{if $testimonial.author_info} ({$testimonial.author_info|escape:'html':'UTF-8'}){/if}" class=" "/>
				</div>
                </li>
				{/if}
                
                <li>
				<div class="content-item {if $testimonial.image}col-md-12{else}col-md-12{/if}">
					<blockquote>{$testimonial.content|truncate:150:'...'|strip_tags}{* HTML CONTENT *}
				 		
				 	</blockquote>
                    <a href="{$link->getModuleLink('ph_testimonials', 'list')|escape:'html':'UTF-8'}#ph_testimonials-item-{$testimonial.id_prestahome_testimonial|intval}">
				 			{l s='Read more' mod='ph_testimonials'}
				 		</a> 
					<h4>
						{*
						 *{if isset($testimonial.author_url) && !empty($testimonial.author_url)}
						 *	<a href="{$testimonial.author_url|escape:'html':'UTF-8'}" title="{l s='Go to website of the author of this testimonial' mod='ph_testimonials'}" rel="nofollow">
						 *{/if}
						 *}
						 		<span>
								{$testimonial.author_name|escape:'html':'UTF-8'}
								</span> 
						{*{if isset($testimonial.author_url) && !empty($testimonial.author_url)}
						*	</a>
						*{/if}
						*}
						{*
						*{if $testimonial.author_info}
						*	<em>({$testimonial.author_info|escape:'html':'UTF-8'})</em>
						*{/if}
						*}
					</h4>
				</div>
                </li>
                
                </ul>
			</div>
			
		</div><!-- .testimonials-carousel-item -->
		{/foreach}
	</div><!-- .testimonials-carousel-wrapper -->
</div><!-- .container -->

<script>
{if Configuration::get('PH_TESTIMONIALS_CAROUSEL')}
var testimonialsAutoPlay = {if Configuration::get('PH_TESTIMONIALS_AUTOPLAY')}{Configuration::get('PH_TESTIMONIALS_AUTOPLAY')|intval}{else}false{/if};
var nbTestimonialsCarouselDesktop = {Configuration::get('PH_TESTIMONIALS_ITEMS')|intval};
var nbTestimonialsCarouselTablet = 2;
var nbTestimonialsCarouselMobile = 1;
var nbTestimonialsCarouselNavigation = {if Configuration::get('PH_TESTIMONIALS_NAVIGATION')}true{else}false{/if};
var testimonialsCarouselNextText = '{l s='Next' mod='ph_testimonials' js=1}';
var testimonialsCarouselPrevText = '{l s='Previous' mod='ph_testimonials' js=1}';

$(function() {
    $(window).on('resize', initPhTestimonials);
    $(window).on('load', initPhTestimonials);
});

function initPhTestimonials()
{
	$("#ph_testimonials").owlCarousel({
        autoPlay 			: testimonialsAutoPlay,
        stopOnHover 		: true,
        navigation			: nbTestimonialsCarouselNavigation,
        pagination			: false,
        transitionStyle		: false,
        items 				: 3,
        itemsDesktop 		: 3,
        itemsDesktopSmall 	: nbTestimonialsCarouselTablet,
        itemsTablet 		: [981,nbTestimonialsCarouselTablet],
        itemsMobile			: [550,nbTestimonialsCarouselMobile],
        navigationText: [
	    	'&laquo; ' + testimonialsCarouselPrevText,
	    	testimonialsCarouselNextText + ' &raquo;'
	    ],

    });
}
</script>
{/if}
{/if}