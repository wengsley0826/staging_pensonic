<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_cb10e5209b5a1fb0f2c00352b6f14ce0']
= '将Google跟踪代码管理器脚本集成到您的商店中';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_fa214007826415a21a8456e3e09f999d'] =
'您确定要删除您的详细资料吗？';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_c888438d14855d7d96a2724ee9c306bd'] =
'设置更新';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_f4f70727dc34561dfde1a3c529b6205c'] =
'设置';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_1b3c641cff628747cb4ddfa9a9a5aea1'] =
'GoogleTagManager ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_3caf54374d6b9076f803e0187a618d1c'] =
'ShopName用于任何跟踪脚本';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_c9cc8cce247e49bae79f15173ce97354'] =
'保存';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_463beb4f48af282f8e7e9e6b93792441'] =
'Gogle分析设置';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_852fcba790047368c66f359d2cf2343d'] =
'Google Analytics UA-CODE';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_9f4521c5a50b80b425dcee542fd866b9'] =
'用户标识的自定义维度编号';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_f7066b8582f6d7a8e1fb2178c90f1ad3'] =
'从Google Analytics帐户 - >管理员 - >您的媒体资源中复制它';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_68e1c45b0719ed2561fb3e5f61b38e37'] =
' - > CustomDefinitions  - > Custom Dimensions页面';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_f0d28c8caa5ec452fa85fc88da3938e1'] =
'您的用户ID自定义维度的ID号';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_5ed7aa57d45d795ae733522670a62f66'] =
'CustomDimension Nr。为ecomm_prodid';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_6c2051003b237a9c480f440aa241d080'] =
'您的ecomm_prodid自定义维度的ID号';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_e65bde66994cafb56bfd9a3e44cedaf4'] =
'CustomDimension Nr。为ecomm_pagetype';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_001ff14e827211f4f141d27e27c10e99'] =
'您的ecomm_pagetype自定义维度的ID号';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_13a51d2fd76e7aaec813c62310682b80'] =
'CustomDimension Nr。为ecomm_totalvalue';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_b61a5776fb6a8b72002242d122bdc779'] =
'您的ecomm_totalvalue自定义维度的ID号';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_9f905fdc72973ef20e44c838cc7092fe'] =
'CustomDimension Nr。为CustomerID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_b041ae5170e871535e50a4d4ae460109'] =
'您的客户ID自定义维度的ID号';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_c819895269cc382d73d74f892211a341'] =
'CrossDomainTracking的域名列表';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_76d75e38bf6fde1f0f7d027df7234410'] =
'每个域用逗号分隔：main.com，domain1.com，domain2.com';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_7cda8ce1a19556b0c3ff7ff0ca92a4c4'] =
'AdWords设置';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_fc7b6a2578a0bbf743c872687a604d22'] =
'转化ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_786b5fc70e3d909a6ff9ccedb34f6cbb'] =
'转换语言';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_6be39b2dbc0222e758a4d60f67d1a09f'] =
'转换标签';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_fec9b088a7f88a8f7207cf6977b33833'] =
'Google Merchant Center';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_ad942601ea0a597a51cfd6dec20a2659'] =
'Merchant Center的ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_46ff2c6057d558fbd9f2e9f2577a993e'] =
'用于动态再营销的关联';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_0203c652257cdfddf72039941716645d'] =
'再营销';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_56cecbb57ebca052d02a98d4c6d3fd06'] =
'Google顾客评价';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_8cf6c075ea7b2ec6e646990239896775'] =
'Google顾客评价商家ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_3c8ae28e3c93b06c4c482664794d87bc'] =
'订单交付前的估计天数。';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_f32e86cbc87742812501b4daa41a2b5a'] =
'Facebook像素跟踪';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_d1b341f7f6772a31758f7cda2d94039e'] =
'Facebook像素代码ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_75279663c361ce8264423e26cd9facf7'] =
'HotJar像素跟踪';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_fe7f4f76bc952a29b39992bc1e4f1bb2'] =
'HotJar网站ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_8b1380c3df3ce5ae18c429becca5fd57'] =
'Inspectlet像素跟踪';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_c68b9d97c3b0f14e2e3b2aaa5f6bda0c'] =
'Inspectlet ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_a12826ea5ce7ef9676264a9e00dfc582'] =
'Pinterest像素跟踪';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_cffb5421613415ee3daacf49cfb9eb2f'] =
'Pinterest ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_f2c3eb5a7ec3f87f364a291f5f3b5812'] =
'Google信誉商店徽章';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_68f2501bd9bf29ab2df93fe549215e23'] =
'Google信誉商店ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_8fa597e0faa10a7cc2e81cec386717ff'] =
'Google Trusted Store区域设置';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_2c3b01dc5085c666e7b2d1bfb98dd9af'] =
'Google信誉商店购物ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_7c42d331cd758bd5acd379e4a4e2bd8c'] =
'Google信誉商店购物帐号ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_a53ab02647b7840eca1f703f27c475a7'] =
'Google Trusted Store购物国家/地区';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_2db459ac08ef1e4712c72974a3e05973'] =
'Google信誉商店购物语言';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_0aa36c46d3edb7356e92de581e4e7085'] =
'CrazyEgg跟踪';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_2e0e40ffdf711bf12a8b862b993e8003'] =
'Crazyegg ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_00d23a76e43b46dae9ec7aa9dcbebb32'] =
'启用';
$_MODULE['<{phgoogletagmanager}default-bootstrap>phgoogletagmanager_b9f5c797ebbf55adccdd8539a65a0241'] =
'残';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_f4f70727dc34561dfde1a3c529b6205c'] =
'设置';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_1b3c641cff628747cb4ddfa9a9a5aea1'] =
'GoogleTagManager ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_3e554fa92d38f338a3de2e9430921239'] =
'插入您的Google跟踪代码管理器ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_d734e9d9c23cfe1ee77379cd9c6758ab'] =
'的Adwords';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_4276cec93205b641b3e7e265b0c040ef'] =
'检查是否要添加AdWords转换';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_bfe6cadd2a8ed6d385d6bdf0f43d3db6'] =
'再营销';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_c63cad31777d63cd54f8eee401f4019d'] =
'检查您是否有AdWords再营销广告系列';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_9add54a458f58e61b445f69a9c4c6a54'] =
'增强的电子商务';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_30b35a66e5eb3afb224918dcda02a7c8'] =
'请检查您是否要在Google Analytics中启用增强型电子商务';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_43781db5c40ecc39fd718685594f0956'] =
'保存';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_cf2175b8dd9014899587afa5da798b8c'] =
'AdWords参数';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_cd02ce9aefb46ba77208e4c6eb2fc7a3'] =
'转换ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_20aa447f8c02a221eb737a2aac06a3d1'] =
'插入您的Adwords转换ID';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_b77b432c0b21cd594f00644960dedb1a'] =
'转换语言';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_d3210f83bdc9fde70a0cd9e528b8edbd'] =
'插入您的Adwords转换语言';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_274145361b9f41174a51e0820e4982b6'] =
'转换标签';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_1521668c93803201f4d761c0dd3bb00d'] =
'插入您的Adwords转换标签';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_c5152ff75a7d1ca26108c63b528602f0'] =
'Google Merchant参数';
$_MODULE['<{phgoogletagmanager}default-bootstrap>config_ad942601ea0a597a51cfd6dec20a2659'] =
'Merchant Center的ID';
