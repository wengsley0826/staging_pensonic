{**
* PrestaChamps
*
* NOTICE OF LICENSE
*
* This source file is subject to the Commercial License
* you can't distribute, modify or sell this code
*
* DISCLAIMER
*
* Do not edit or add to this file
* If you need help please contact leo@prestachamps.com
*
* @author     PrestaChamps <leo@prestachamps.com>
* @copyright  PrestaChamps
*}
<noscript data-keepinline="true">{literal}<iframe src="https://www.googletagmanager.com/ns.html?id={/literal}{$gtm|escape:'htmlall':'UTF-8'}{literal}"height="0" width="0" style="display:none;visibility:hidden"></iframe>{/literal}</noscript>


