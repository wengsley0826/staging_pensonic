<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/pm_seointernallinking.php');
if (Tools::getIsset('secure_key') && Tools::getValue('secure_key')) {
	$pm_sil = new pm_seointernallinking();
	$pm_sil->runCrontab(Tools::getValue('secure_key'), trim(strtolower(Tools::getValue('type'))));
}
