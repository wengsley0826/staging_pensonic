CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking` (
  `id_expression` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID of the expression',
  `id_lang` int(11) unsigned NOT NULL COMMENT 'ID Lang of the expression',
  `id_group` int(11) unsigned NOT NULL COMMENT 'ID Group of the expression',
  `expression_content`  varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `associated_url` text NOT NULL COMMENT 'URL associated to the expression',
  `url_title` varchar(100) NOT NULL COMMENT 'Title attribute of the link (100 chars max)',
  `nofollow` tinyint(3) unsigned DEFAULT '0' COMMENT 'Add a rel="nofollow" to the link',
  `new_window` tinyint(3) unsigned DEFAULT '0' COMMENT 'Open the link in a new window',
  `link_position` tinyint(3) unsigned DEFAULT '1' COMMENT '1 is top, 2 is middle, 3 is bottom',
  `active` tinyint(3) unsigned DEFAULT '1' COMMENT '1 is active, 0 is not active',
  PRIMARY KEY (`id_expression`),
  UNIQUE INDEX `group_expression` (`id_group`, `id_lang`, `expression_content`),
  KEY `id_lang` (`id_lang`)
) ENGINE=MYSQL_ENGINE AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_category_rules` (
  `id_group` int(11) unsigned NOT NULL COMMENT 'ID Group',
  `id_category` int(11) unsigned NOT NULL COMMENT 'ID category',
  PRIMARY KEY (`id_group`,`id_category`),
  KEY `id_group` (`id_group`),
  KEY `id_category` (`id_category`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_cms_rules` (
  `id_group` int(11) unsigned NOT NULL COMMENT 'ID Group',
  `id_cms` int(11) unsigned NOT NULL COMMENT 'ID CMS',
  PRIMARY KEY (`id_group`,`id_cms`),
  KEY `id_group` (`id_group`),
  KEY `id_category` (`id_cms`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_group` (
  `id_group` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID of the group',
  `group_type` tinyint(3) unsigned DEFAULT '1' COMMENT 'Group type (1 => products | 2 => cms | 3 => editorial)',
  `category_type` tinyint(3) unsigned DEFAULT '0' COMMENT '0 => inclusion - 1 => exclusion',
  `product_type` tinyint(3) unsigned DEFAULT '0' COMMENT '0 => inclusion - 1 => exclusion',
  `manufacturer_type` tinyint(3) unsigned DEFAULT '0' COMMENT '0 => inclusion - 1 => exclusion',
  `supplier_type` tinyint(3) unsigned DEFAULT '0' COMMENT '0 => inclusion - 1 => exclusion',
  `cms_type` tinyint(3) unsigned DEFAULT '0' COMMENT '0 => inclusion - 1 => exclusion',
  PRIMARY KEY (`id_group`),
  KEY `id_group` (`id_group`),
  KEY `group_type` (`group_type`)
) ENGINE=MYSQL_ENGINE AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_group_shop` (
`id_group`  int(11) unsigned NOT NULL COMMENT 'ID Group' ,
`id_shop`  int(10) unsigned NOT NULL COMMENT 'ID Shop' ,
PRIMARY KEY (`id_group`, `id_shop`),
INDEX `id_group` (`id_group`, `id_shop`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_group_lang` (
  `id_group`  int(10) UNSIGNED NOT NULL COMMENT 'ID Group' ,
  `id_lang`  int(10) UNSIGNED NOT NULL COMMENT 'ID Lang' ,
  `name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Group name' ,
  PRIMARY KEY (`id_group`, `id_lang`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_manufacturer_rules` (
  `id_group` int(11) unsigned NOT NULL COMMENT 'ID Group',
  `id_manufacturer` int(11) unsigned NOT NULL COMMENT 'ID Manufacturer',
  PRIMARY KEY (`id_group`,`id_manufacturer`),
  KEY `id_group` (`id_group`),
  KEY `id_manufacturer` (`id_manufacturer`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_product_rules` (
  `id_group` int(11) unsigned NOT NULL COMMENT 'ID Group',
  `id_product` int(11) unsigned NOT NULL COMMENT 'ID Product',
  PRIMARY KEY (`id_group`,`id_product`),
  KEY `id_group` (`id_group`),
  KEY `id_product` (`id_product`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_supplier_rules` (
  `id_group` int(11) unsigned NOT NULL COMMENT 'ID Group',
  `id_supplier` int(11) unsigned NOT NULL COMMENT 'ID Supplier',
  PRIMARY KEY (`id_group`,`id_supplier`),
  KEY `id_group` (`id_group`),
  KEY `id_supplier` (`id_supplier`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;