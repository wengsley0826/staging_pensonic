$jqPm.fn.extend({
	multiSelectUpdateCount: function() {
		if (typeof(this.data("uiMultiselect")) != "undefined")
			this.data("uiMultiselect")._updateCount();
		else
			this.data("multiselect")._updateCount();
	}
});

$jqPm(document).ready(function() {
	$jqPm("form#formAddExpression").submit(function(e) {
		$jqPm.getJSON(_base_config_url + '&expressionFormValidation=1&' + $jqPm(this).serialize(), function(json) {
			if (json === true)  {
				return true;
			} else {
				$jqPm("form#formAddExpression").data("validator").invalidate(json);
				e.preventDefault();
				return false;
			}
		});
	});

	$jqPm("a.synchroniseeverything").click(function(e) {
		if ($jqPm(this).hasClass('ui-state-disabled')) return false;
		
		if (confirm($jqPm(this).attr('title'))) {
			$jqPm("div.taskDone").removeClass("taskDone");
			$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
			$jqPm(this).pm_ajaxScriptLoad(e);
		}
		return false;
	});
	$jqPm("a.synchroniseallproducts").on('click', function(e) {
		if ($jqPm(this).hasClass('ui-state-disabled')) return false;
		
		if (confirm($jqPm(this).attr('title'))) {
			$jqPm("#syncProductContainer").removeClass('taskDone');
			$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
			$jqPm(this).hide();
			$jqPm("#progressSyncProductInformation").show();
			$jqPm("#progressSyncProductRemainingTime").show();
			$jqPm("#progressSyncProduct").show();
			$jqPm(this).pm_ajaxScriptLoad(e);
		}
		return false;
	});
	$jqPm("a.synchroniseallcmspages").click(function(e) {
		if ($jqPm(this).hasClass('ui-state-disabled')) return false;
		
		if (confirm($jqPm(this).attr('title'))) {
			$jqPm("#syncCMSPagesContainer").removeClass('taskDone');
			$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
			$jqPm(this).hide();
			$jqPm("#progressSyncCMSPagesInformation").show();
			$jqPm("#progressSyncCMSPagesRemainingTime").show();
			$jqPm("#progressSyncCMSPages").show();
			$jqPm(this).pm_ajaxScriptLoad(e);
		}
		return false;
	});
	$jqPm("a.synchroniseallcategories").click(function(e) {
		if ($jqPm(this).hasClass('ui-state-disabled')) return false;
		
		if (confirm($jqPm(this).attr('title'))) {
			$jqPm("#syncCategoriesContainer").removeClass('taskDone');
			$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
			$jqPm(this).hide();
			$jqPm("#progressSyncCategoriesInformation").show();
			$jqPm("#progressSyncCategoriesRemainingTime").show();
			$jqPm("#progressSyncCategories").show();
			$jqPm(this).pm_ajaxScriptLoad(e);
		}
		return false;
	});
	$jqPm("a.synchroniseallmanufacturers").click(function(e) {
		if ($jqPm(this).hasClass('ui-state-disabled')) return false;
		
		if (confirm($jqPm(this).attr('title'))) {
			$jqPm("#syncManufacturersContainer").removeClass('taskDone');
			$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
			$jqPm(this).hide();
			$jqPm("#progressSyncManufacturersInformation").show();
			$jqPm("#progressSyncManufacturersRemainingTime").show();
			$jqPm("#progressSyncManufacturers").show();
			$jqPm(this).pm_ajaxScriptLoad(e);
		}
		return false;
	});
	$jqPm("a.synchroniseeditorial").click(function(e) {
		if ($jqPm(this).hasClass('ui-state-disabled')) return false;
		
		if (confirm($jqPm(this).attr('title'))) {
			$jqPm("#syncEditorialContainer").removeClass('taskDone');
			$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
			$jqPm(this).hide();
			$jqPm("#progressSyncEditorialInformation").show();
			$jqPm("#progressSyncEditorial").show();
			$jqPm(this).pm_ajaxScriptLoad(e);
		}
		return false;
	});
	$jqPm("a.removealllinks").click(function(e) {
		if ($jqPm(this).hasClass('ui-state-disabled')) return false;
		
		if (confirm($jqPm(this).attr('title'))) {
			$jqPm("#deleteAllContainer").removeClass('taskDone');
			$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
			$jqPm("#progressDeleteAllInformation").show();
			$jqPm(this).hide();
			$jqPm(this).pm_ajaxScriptLoad(e);
		}
		return false;
	});
	$jqPm('select#group_type').bind('change', 'keyup', function() {
		switch($jqPm(this).val()) {
			case '1':
				$jqPm('div#categoriesWarning').hide();
				$jqPm('div#manufacturersWarning').hide();
				$jqPm('div#groupProductZone').show();
				$jqPm('div#groupCMSZone').hide();
				$jqPm('div#groupEditorialZone').hide();
				
				$jqPm("ul#configTab li").show();
				$jqPm("ul#configTab li:eq(0) a").trigger('click');
				break;
			case '2':
				$jqPm('div#categoriesWarning').hide();
				$jqPm('div#manufacturersWarning').hide();
				$jqPm('div#groupCMSZone').show();
				$jqPm('div#groupProductZone').hide();
				$jqPm('div#groupEditorialZone').hide();
				break;
			case '3':
				$jqPm('div#categoriesWarning').hide();
				$jqPm('div#manufacturersWarning').hide();
				$jqPm('div#groupEditorialZone').show();
				$jqPm('div#groupProductZone').hide();
				$jqPm('div#groupCMSZone').hide();
				break;
			case '4':
				$jqPm('div#categoriesWarning').show();
				$jqPm('div#manufacturersWarning').hide();
				$jqPm('div#groupProductZone').show();
				$jqPm('div#groupCMSZone').hide();
				$jqPm('div#groupEditorialZone').hide();
				
				$jqPm("ul#configTab li").hide();
				$jqPm("ul#configTab li:eq(0)").show();
				$jqPm("ul#configTab li:eq(0) a").trigger('click');
				break;
			case '5':
				$jqPm('div#manufacturersWarning').show();
				$jqPm('div#categoriesWarning').hide();
				$jqPm('div#groupProductZone').show();
				$jqPm('div#groupCMSZone').hide();
				$jqPm('div#groupEditorialZone').hide();
				
				$jqPm("ul#configTab li").hide();
				$jqPm("ul#configTab li:eq(2)").show();
				$jqPm("ul#configTab li:eq(2) a").trigger('click');
				break;
		}
	});
	if ($jqPm('input#id_group') && $jqPm('input[name=submit_group]')) {
		$jqPm('select#group_type').trigger('change');
		$jqPm('input.search').css('width', '210px');
	}
});