<?php
/**
 *
 * PM_SEOInternalLinking
 *
 * @category  Front Office Features
 * @author    Presta-Module.com <support@presta-module.com>
 * @copyright Presta-Module 2016
 * @version   1.1.12
 *
 * 		_______  ____    ____
 * 	   |_   __ \|_   \  /   _|
 * 		 | |__) | |   \/   |
 * 		 |  ___/  | |\  /| |
 * 		_| |_    _| |_\/_| |_
 * 	   |_____|  |_____||_____|
 *
 *
 *************************************
 **       SEO Internal Linking       *
 **   http://www.presta-module.com   *
 *************************************
 * +
 * + Description:
 * + Languages: EN, FR
 * + PS version: 1.1 to 1.6
 * +
 ************************************* 
 */
include_once (_PS_ROOT_DIR_ . '/modules/pm_seointernallinking/ShopOverrided.php');
require_once (_PS_ROOT_DIR_ . '/modules/pm_seointernallinking/seointernallinkingcoreclass.php');
require_once (_PS_ROOT_DIR_ . '/modules/pm_seointernallinking/seointernallinkingexpressionclass.php');
require_once (_PS_ROOT_DIR_ . '/modules/pm_seointernallinking/seointernallinkinggroupclass.php');
class pm_seointernallinking extends seointernallinkingcoreclass {
	public static $_module_prefix = 'SIL';
	protected $_css_js_to_load = array ('jquery',
									 	'jquerytiptip',
									 	'jqueryui',
										'admincore',
									 	'adminmodule',
									 	'datatables',
									 	'jgrowl',
									 	'multiselect',
										'selectmenu',
										'jquerytools');
	private $_file_to_check = array();
	const INSTALL_SQL_FILE = 'install.sql';
	const UPDATE_SQL_FILE = 'update.sql';
	private $_group_concat_max_len = 1048576;
	protected $_copyright_link = array(
		'link'	=> '',
		'img'	=> '//www.presta-module.com/img/logo-module.JPG'
	);
	public $_exclude_headings = 0;
	public $_description_field = 'both_description';
	public $_default_datatables_length = 10;
	public $_preg_pattern_headings = '/(<h.*?>)(.*?)(<\/h.*?>)/siu';
	public $_max_affected_rows = 8000;
	public $_big_sql_results_memory_limit = '1024M';
	public $_crontab_max_execution_time = 360;
	function __construct() {
		$this->need_instance = 0;
		$this->name = 'pm_seointernallinking';
		$this->module_key = '39bb6143fcc710a105ee8f49cd644b0d';
		if (version_compare(_PS_VERSION_, '1.4.0.0', '<'))
			$this->tab = 'Presta-Module';
		else {
			$this->author = 'Presta-Module';
			$this->tab = 'seo';
		}
		$this->version = '1.1.12';
		parent::__construct();
		$_exclude_headings = Configuration::get('PM_'.self::$_module_prefix.'_EXCLUDE_HEADINGS');
		if ($_exclude_headings === false) Configuration::updateValue('PM_'.self::$_module_prefix.'_EXCLUDE_HEADINGS', $this->_exclude_headings);
		$this->_exclude_headings = (int)Configuration::get('PM_'.self::$_module_prefix.'_EXCLUDE_HEADINGS');
		$_description_field = Configuration::get('PM_'.self::$_module_prefix.'_DESCRIPTION_FIELD');
		if ($_description_field === false) Configuration::updateValue('PM_'.self::$_module_prefix.'_DESCRIPTION_FIELD', $this->_description_field);
		$this->_description_field = Configuration::get('PM_'.self::$_module_prefix.'_DESCRIPTION_FIELD');
		if ($this->_onBackOffice()) {
			$this->displayName = $this->l('SEO Internal Linking');
			$this->description = $this->l('Improve your e-shop internal netlinking ! This module helps you to create a bunch of links on your products, cms and home page.');
			$_default_datatables_length = Configuration::get('PM_'.self::$_module_prefix.'_DEFAULT_DATATABLES_LENGTH');
			if ($_default_datatables_length === false) Configuration::updateValue('PM_'.self::$_module_prefix.'_DEFAULT_DATATABLES_LENGTH', $this->_default_datatables_length);
			$this->_default_datatables_length = (int)Configuration::get('PM_'.self::$_module_prefix.'_DEFAULT_DATATABLES_LENGTH');
			$doc_url_tab['fr'] = '#/fr/seointernallinking/';
			$doc_url_tab['en'] = '#/en/seointernallinking/';
			$doc_url = $doc_url_tab['en'];
			if ($this->_iso_lang == 'fr') $doc_url = $doc_url_tab['fr'];
			$forum_url_tab['fr'] = 'http://www.prestashop.com/forums/topic/163336-modulepm-seo-internal-linking-generateur-de-maillage-interne/';
			$forum_url_tab['en'] = 'http://www.prestashop.com/forums/topic/163337-modulepm-seo-internal-linking-aim-for-the-top-ranking/';
			$forum_url = $forum_url_tab['en'];
			if ($this->_iso_lang == 'fr') $forum_url = $forum_url_tab['fr'];
			$this->_support_link = array(
				array('link' => $forum_url, 'target' => '_blank', 'label' => $this->l('Forum topic')),
				
				array('link' => 'http://addons.prestashop.com/contact-community.php?id_product=4982', 'target' => '_blank', 'label' => $this->l('Support contact')),
			);
		}
	}
	function install() {
		if (!parent::install() 
		OR !$this->installDb() 
		OR !$this->registerHook(((version_compare(_PS_VERSION_, '1.5.0.0', '<')) ? 'updateProduct' : 'actionProductSave'))
		OR (version_compare(_PS_VERSION_, '1.4.0.0', '>=') && !$this->registerHook('categoryUpdate'))
		OR (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && !$this->registerHook('actionObjectManufacturerAddAfter'))
		OR (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && !$this->registerHook('actionObjectManufacturerUpdateAfter'))
		OR !Configuration::updateValue('PM_'.self::$_module_prefix.'_DESCRIPTION_FIELD', 'both_description')
		OR !Configuration::updateValue('PM_'.self::$_module_prefix.'_DEFAULT_DATATABLES_LENGTH', 10)
		OR !Configuration::updateValue('PM_'.self::$_module_prefix.'_EXCLUDE_HEADINGS', 0)
		OR !Configuration::updateValue('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY', Tools::passwdGen(16))
		OR !Configuration::updateValue('PM_'.self::$_module_prefix.'_CRON_LAST_RUN', 0)
		)
			return false;
		$this->checkIfModuleIsUpdate(true, false);
		return true;
	}
	public function checkIfModuleIsUpdate($updateDb = false, $displayConfirm = true) {
		parent::checkIfModuleIsUpdate($updateDb, $displayConfirm);
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && !$this->isRegisteredInHook('actionObjectManufacturerAddAfter'))
			$this->registerHook('actionObjectManufacturerAddAfter');
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && !$this->isRegisteredInHook('actionObjectManufacturerUpdateAfter'))
			$this->registerHook('actionObjectManufacturerUpdateAfter');
		$isUpdate = true;
		if (! $updateDb && $this->version != Configuration::get('PM_'.self::$_module_prefix.'_LAST_VERSION', false))
			return false;
		if ($updateDb) {
			unset($_GET ['makeUpdate']);
			Configuration::updateValue('PM_'.self::$_module_prefix.'_LAST_VERSION', $this->version);
			$this->installDb();
			$this->updateDb();
			if ($isUpdate && $displayConfirm)
				$this->_html .= $this->displayConfirmation($this->l('Module updated successfully'));
			else
				$this->_html .= $this->displayError($this->l('Module update fail'));
		}
		return $isUpdate;
	}
	function installDB() {
		if (! file_exists(dirname(__FILE__) . '/' . self::INSTALL_SQL_FILE))
			return (false);
		else if (! $sql = file_get_contents(dirname(__FILE__) . '/' . self::INSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>='))
			$sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
		else
			$sql = str_replace('MYSQL_ENGINE', 'MyISAM', $sql);
		$sql = preg_split("/;\s*[\r\n]+/", $sql);
		foreach ( $sql as $query )
			if (! self::Db_Execute(trim($query)))
				return (false);
		return true;
	}
	public function l($s, $specific = false, $id_lang = null) {
		if (version_compare(_PS_VERSION_, '1.2.0.0', '<')) {
			$translated_text = parent::l($s);
			if ($translated_text != $s) return $translated_text;
			if ($specific != false) {
				$this->page = $specific;
				$translated_text = parent::l($s);
				if ($translated_text != $s) return $translated_text;
			}
			$this->page = get_class();
			return parent::l($s);
		}
		if (version_compare(_PS_VERSION_, '1.4.0.0', '<')) return parent::l($s, $specific);
		return parent::l($s, $specific, $id_lang);
	}
	public function updateDb() {
		if (! file_exists(dirname(__FILE__) . '/' . self::UPDATE_SQL_FILE))
			return (false);
		else if (! $sql = file_get_contents(dirname(__FILE__) . '/' . self::UPDATE_SQL_FILE))
			return (false);
		$sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>='))
			$sql = str_replace('MYSQL_ENGINE', _MYSQL_ENGINE_, $sql);
		else
			$sql = str_replace('MYSQL_ENGINE', 'MyISAM', $sql);
		$sql = preg_split("/;\s*[\r\n]+/", $sql);
		foreach ( $sql as $query )
			if (! self::Db_Execute(trim($query)))
				return (false);
		return true;
	}
	function uninstall() {
		if (! parent::uninstall())
			return false;
		return true;
	}
	public static function headingsDoNotTouchStart($matches) {
		$hc = base64_encode($matches[2]);
		$heading_content = '';
		for ($i=0; $i < strlen($hc); $i++) {
			$heading_content .= $hc[$i] . '<!---->';
		}
		return $matches[1] . $heading_content . $matches[3];
	}
	public static function headingsDoNotTouchEnd($matches) {
		return $matches[1] . stripslashes(str_replace('<!---->', '', base64_decode($matches[2]))) . $matches[3];
	}
	public function runCrontab($secure_key, $type = 'products') {
		$conf_secure_key = Configuration::get('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY');
		if (!empty($conf_secure_key) && $conf_secure_key == Tools::getValue('secure_key')) {
			ini_set('max_execution_time', $this->_crontab_max_execution_time);
			switch ($type) {
				case 'products':
				default:
					$word_combinaisons = $this->getWordsToLink(NULL, 0, 0, false, true);
					$this->cleanAllHTMLField('products', true);
					Configuration::updateValue('PM_'.self::$_module_prefix.'_CRON_LAST_RUN', time());
					if (self::_isFilledArray($word_combinaisons)) {
						$this->updateHTMLField('products', $word_combinaisons, true, true);
						return true;
					}
				case 'cms':
					$word_combinaisons = $this->getCMSWordsToLink(0, 0, false, true);
					$this->cleanAllHTMLField('cms', true);
					Configuration::updateValue('PM_'.self::$_module_prefix.'_CRON_LAST_RUN', time());
					if (self::_isFilledArray($word_combinaisons)) {
						$this->updateHTMLField('cms', $word_combinaisons, true, true);
						return true;
					}
				case 'categories':
					$word_combinaisons = $this->getCategoriesWordsToLink(NULL, 0, 0, false, true);
					$this->cleanAllHTMLField('categories', true);
					Configuration::updateValue('PM_'.self::$_module_prefix.'_CRON_LAST_RUN', time());
					if (self::_isFilledArray($word_combinaisons)) {
						$this->updateHTMLField('categories', $word_combinaisons, true, true);
						return true;
					}
				case 'manufacturers':
					if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
						$word_combinaisons = $this->getManufacturersWordsToLink(NULL, 0, 0, false, true);
						$this->cleanAllHTMLField('manufacturers', true);
						Configuration::updateValue('PM_'.self::$_module_prefix.'_CRON_LAST_RUN', time());
						if (self::_isFilledArray($word_combinaisons)) {
							$this->updateHTMLField('manufacturers', $word_combinaisons, true, true);
							return true;
						}
					}
				case 'editorial':
					$this->updateEditorialContent(true);
					Configuration::updateValue('PM_'.self::$_module_prefix.'_CRON_LAST_RUN', time());
					return true;
			}
		}
		return false;
	}
	private function addLinkToWord($matches) {
		$word_array = $this->tmpCurrentWord;
		$word = $word_array['expression_content'];
		$url = $word_array['associated_url'];
		$url_title = htmlentities($word_array['url_title'], ENT_COMPAT, 'UTF-8');
		$nofollow = (bool)($word_array['nofollow']);
		$new_window = (bool)($word_array['new_window']);
		$link_position = (int)($word_array['link_position']);
		$updated_description = $this->updateLinks(
			$matches[2],
			'/(?!(?:[^<\[]+[>\]]|[^>\]]+<\/a>))\b('.preg_quote($word).')\b/imu',
			'<a href="'.$url.'" class="pmsil"'. (($nofollow) ? ' rel="nofollow"' : '') . (($url_title != '') ? ' title="'.$url_title.'"' : '') . (($new_window) ? ' target="_blank"' : '') .'>$1</a>',
			$word,
			$link_position
		);
		if ($updated_description != false) {
			return $matches[1] . $updated_description . $matches[3];
		}
		return $matches[1] . $matches[2] . $matches[3];
	}
	private function editorialFileIsRW() {
		return (file_exists(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml') && is_readable(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml') && is_writable(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml'));
	}
	private function updateEditorialContent($from_crontab = false) {
		if (!self::isModuleInstalled('editorial')) return 2;
		$words = $this->getEditorialWordsToLink($from_crontab);
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) {
			$this->cleanAllHTMLField('editorial', $from_crontab);
			$this->updateHTMLField('editorial', $words, true, $from_crontab);
			if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && method_exists($this, '_clearCache'))
				$this->_clearCache('editorial.tpl', 'editorial');
			return 1;
		} else {
			$xml = false;
			if ($this->editorialFileIsRW()) {
				$xml = file_get_contents(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml');
				$xml = $this->deleteAllLinks($xml);
				if (self::_isFilledArray($words)) {
					foreach ($words as $word) {
						$this->tmpCurrentWord = $word;
						if ((bool)$this->_exclude_headings == true) {
							$xml = stripslashes(preg_replace_callback($this->_preg_pattern_headings, 'self::headingsDoNotTouchStart', $xml));
						}
						$xml = stripslashes(preg_replace_callback('/(<paragraph_'.$word['id_lang'].'>)(.+)(<\/paragraph_'.$word['id_lang'].'>)/imu', array($this, 'addLinkToWord'), $xml));
						if ((bool)$this->_exclude_headings == true) {
							$xml = stripslashes(preg_replace_callback($this->_preg_pattern_headings, 'self::headingsDoNotTouchEnd', $xml));
						}
					}
				}
				return @file_put_contents(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml', $xml);
			} else {
				return false;
			}
		}
		return false;
	}
	private function cleanEditorialContent() {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) {
			$this->cleanAllHTMLField('editorial');
			return true;
		} else {
			$xml = false;
			if (file_exists(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml') && is_readable(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml') && is_writable(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml')) {
				$xml = file_get_contents(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml');
				$xml = $this->deleteAllLinks($xml);
				return @file_put_contents(_PS_ROOT_DIR_ . '/modules/editorial/editorial.xml', $xml);	
			} else {
				return false;
			}
		}
	}
	public function displayGlobalOptions() {
		$this->_startForm(array('id' => 'formGlobalOptions', 'iframetarget' => false));
		$this->_displayInputActive(array('obj' => $this, 'key_active' => '_exclude_headings', 'key_db' => '_exclude_headings', 'label' => $this->l('Do not add links in headings (h1, h2, etc.)')));
		$this->_displaySelect(array('obj' => $this, 'options' => array('description' =>  $this->l('Long description'), 'description_short' =>  $this->l('Short description'), 'both_description' =>  $this->l('Short & Long description')), 'key' => '_description_field', 'label' => $this->l('Description field to be used (for the products)'), 'defaultvalue' => false, 'size' => '200px'));
		$this->_displaySelect(array('obj' => $this, 'options' => array(10 => 10, 25 => 25, 50 => 50, 100 => 100, 150 => 150, 200 => 200), 'key' => '_default_datatables_length', 'label' => $this->l('Number of lines to be displayed in the tables'), 'defaultvalue' => false, 'size' => '100px'));
		$this->_displaySubmit($this->l(' Save '), 'submit_global_options');
		$this->_endForm(array('id' => 'formGlobalOptions'));
	}
	public function displaySynchronisationInformations() {
		$this->_html .= '<div id="globalSyncContainer" style="text-align: center">';
		$this->_addButton(array('text' => $this->l('Optimize everything'), 'href' => $this->_base_config_url . '&synchroniseEverything=1', 'onclick' => false, 'icon_class' => 'ui-button-text-only', 'class' => 'synchroniseeverything', 'title' => $this->l('Do you really want to optimize everything').' ?'));
		$this->_html .= '<p id="progressSyncAllInformation" style="display:none;"></p>';
		$this->_html .= '</div>';
		$this->_pmClear();
		$this->_html .= '<div id="syncProductContainer">';
		$this->_html .= '<h4>'.$this->l('Optimize all products').'</h4>';
		$this->_addButton(array('text' => $this->l('Optimize'), 'href' => $this->_base_config_url . '&synchroniseAllProducts=1', 'onclick' => false, 'icon_class' => 'ui-button-text-only', 'class' => 'synchroniseallproducts', 'title' => $this->l('Do you really want to optimize all products').' ?'));
		$this->_html .= '<p id="progressSyncProductInformation" style="display:none;">'.$this->l('Products linking process is in progress... Please wait, it can take some minutes (depends on the size of your catalog)').'</p>';
		$this->_html .= '<p class="progressBar" id="progressSyncProduct" style="display:none;"><span><em style="left:0px;"></em></span></p>';
		$this->_html .= '<p id="progressSyncProductRemainingTime" style="display:none;"></p>';
		$this->_html .= '</div>';
		$this->_pmClear();
		$this->_html .= '<div id="syncCMSPagesContainer">';
		$this->_html .= '<h4>'.$this->l('Optimize all CMS pages').'</h4>';
		$this->_addButton(array('text' => $this->l('Optimize'), 'href' => $this->_base_config_url . '&synchroniseAllCMSPages=1', 'onclick' => false, 'icon_class' => 'ui-button-text-only', 'class' => 'synchroniseallcmspages', 'title' => $this->l('Do you really want to optimize all CMS pages').' ?'));
		$this->_html .= '<p id="progressSyncCMSPagesInformation" style="display:none;">'.$this->l('CMS Pages linking process is in progress... Please wait, it can take some minutes (depends on the number of pages)').'</p>';
		$this->_html .= '<p class="progressBar" id="progressSyncCMSPages" style="display:none;"><span><em style="left:0px;"></em></span></p>';
		$this->_html .= '<p id="progressSyncCMSPagesRemainingTime" style="display:none;"></p>';
		$this->_html .= '</div>';
		$this->_html .= '<div id="syncCategoriesContainer">';
		$this->_html .= '<h4>'.$this->l('Optimize all categories').'</h4>';
		$this->_addButton(array('text' => $this->l('Optimize'), 'href' => $this->_base_config_url . '&synchroniseAllCategories=1', 'onclick' => false, 'icon_class' => 'ui-button-text-only', 'class' => 'synchroniseallcategories', 'title' => $this->l('Do you really want to optimize all categories').' ?'));
		$this->_html .= '<p id="progressSyncCategoriesInformation" style="display:none;">'.$this->l('Categories linking process is in progress... Please wait, it can take some minutes (depends on the number of categories)').'</p>';
		$this->_html .= '<p class="progressBar" id="progressSyncCategories" style="display:none;"><span><em style="left:0px;"></em></span></p>';
		$this->_html .= '<p id="progressSyncCategoriesRemainingTime" style="display:none;"></p>';
		$this->_html .= '</div>';
		$this->_html .= '<div id="syncManufacturersContainer"'.(version_compare(_PS_VERSION_, '1.5.0.0', '<') ? ' style="display: none"' : '').'>';
		$this->_html .= '<h4>'.$this->l('Optimize all manufacturers').'</h4>';
		$this->_addButton(array('text' => $this->l('Optimize'), 'href' => $this->_base_config_url . '&synchroniseAllManufacturers=1', 'onclick' => false, 'icon_class' => 'ui-button-text-only', 'class' => 'synchroniseallmanufacturers', 'title' => $this->l('Do you really want to optimize all manufacturers').' ?'));
		$this->_html .= '<p id="progressSyncManufacturersInformation" style="display:none;">'.$this->l('Manufacturers linking process is in progress... Please wait, it can take some minutes (depends on the number of manufacturers)').'</p>';
		$this->_html .= '<p class="progressBar" id="progressSyncManufacturers" style="display:none;"><span><em style="left:0px;"></em></span></p>';
		$this->_html .= '<p id="progressSyncManufacturersRemainingTime" style="display:none;"></p>';
		$this->_html .= '</div>';
		$this->_html .= '<div id="syncEditorialContainer">';
		$this->_html .= '<h4>'.$this->l('Optimise home page management module').'</h4>';
		$this->_addButton(array('text' => $this->l('Optimize'), 'href' => $this->_base_config_url . '&synchroniseEditorial=1', 'onclick' => false, 'icon_class' => 'ui-button-text-only', 'class' => 'synchroniseeditorial', 'title' => $this->l('Do you really want to optimize editorial module').' ?'));
		$this->_html .= '<p id="progressSyncEditorialInformation" style="display:none;">'.$this->l('Editorial linking process is in progress... Please wait until this text disappear').'</p>';
		$this->_html .= '<p class="progressBar" id="progressSyncEditorial" style="display:none;"><span><em style="left:0px;"></em></span></p>';
		$this->_html .= '</div>';
	}
	public function displayUndoChanges() {
		$this->_showInfo(
			$this->l('If you click on the button below, you will undo every changes made to your products/cms/editorial content.') . '<br />' .
			$this->l('This mean that all the SEO optimization that was made will be lost until you run another optimization.') . '<br /><br />' .
			$this->l('You are warned !')
		);
		$this->_pmClear();
		$this->_html .= '<div id="deleteAllContainer">';
		$this->_html .= '<h4>'.$this->l('Undo all changes made by this module').'</h4>';
		$this->_addButton(array('text' => $this->l('Undo changes'), 'href' => $this->_base_config_url . '&removeAllLinks=1', 'onclick' => false, 'icon_class' => 'ui-button-text-only', 'class' => 'removealllinks', 'title' => $this->l('Do you really want to remove all the links').' ?'));
		$this->_html .= '<p id="progressDeleteAllInformation" style="display:none;">'.$this->l('Removing links process is in progress... Please wait, it can take some minutes (depends on your catalog size and the number of CMS pages)').'</p>';
		$this->_html .= '</div>';
	}
	public function displayCrontabInformations() {
		$this->_html .= '<p>'.$this->l('Last crontab usage').' : <strong>'. ((Configuration::get('PM_'.self::$_module_prefix.'_CRON_LAST_RUN', '0') != '0') ? date('r', Configuration::get('PM_'.self::$_module_prefix.'_CRON_LAST_RUN')) : 'N/A') .'</strong></p>';
		$this->_html .= '<div class="conf pm_confirm">' . $this->l('If you want to automatically optimize all the new or edited pages on your Prestashop installation, please ask your reseller to add this new cron task :') .
		'<br /><br />'. $this->l('Complete optimization :') .
		'<br />' . self::getHttpHost(true, true) . __PS_BASE_URI__ . 'modules/pm_seointernallinking/cron.php?secure_key=' . Configuration::get('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY') .
		'<br /><br />'. $this->l('Optimization of the editorial module only :') .
		'<br />' . self::getHttpHost(true, true) . __PS_BASE_URI__ . 'modules/pm_seointernallinking/cron.php?secure_key=' . Configuration::get('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY') . '&type=editorial' .
		'<br /><br />'. $this->l('Optimization of the products pages only :') .
		'<br />' . self::getHttpHost(true, true) . __PS_BASE_URI__ . 'modules/pm_seointernallinking/cron.php?secure_key=' . Configuration::get('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY') . '&type=products' .
		'<br /><br />'. $this->l('Optimization of the CMS pages only :') .
		'<br />' . self::getHttpHost(true, true) . __PS_BASE_URI__ . 'modules/pm_seointernallinking/cron.php?secure_key=' . Configuration::get('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY') . '&type=cms' .
		'<br /><br />'. $this->l('Optimization of the categories pages only :') .
		'<br />' . self::getHttpHost(true, true) . __PS_BASE_URI__ . 'modules/pm_seointernallinking/cron.php?secure_key=' . Configuration::get('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY') . '&type=categories' .
		(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? '<br /><br />'. $this->l('Optimization of the manufacturers pages only :') .
		'<br />' . self::getHttpHost(true, true) . __PS_BASE_URI__ . 'modules/pm_seointernallinking/cron.php?secure_key=' . Configuration::get('PM_'.self::$_module_prefix.'_CRON_SECURE_KEY') . '&type=manufacturers' : '') .
		'</div>';
	}
	public function getGroupCombinaisonInformations($group_obj) {
		$groupExplain = "";
		if ($group_obj->group_type == 1) {
			$nbCategories = sizeof($group_obj->getCategories());
			$exclusionCategories = $group_obj->category_type;
			$nbProduits = sizeof($group_obj->getProducts());
			$exclusionProduits = $group_obj->product_type;
			$nbFabricants = sizeof($group_obj->getManufacturers());
			$exclusionFabricants = $group_obj->manufacturer_type;
			$nbFournisseurs = sizeof($group_obj->getSuppliers());
			$exclusionFournisseurs = $group_obj->supplier_type;
			if ($nbCategories == 0 && $nbProduits == 0 && $nbFabricants == 0 && $nbFournisseurs == 0) return '';
			if ($nbProduits == 0) $groupExplain .= $this->l('Your products selection will be part').' ';
			if ($nbProduits > 0 && $exclusionProduits == 1) $groupExplain .= $this->l('Your selection will contain all products excepted').' '. $nbProduits .' '.$this->l('of them').', ';
			if ($nbProduits > 0 && $exclusionProduits == 0) $groupExplain .= $this->l('Your selection will contain').' '. $nbProduits .' '.$this->l('product(s)').' ';
			if ($nbProduits > 0) $groupExplain .= $this->l('in').' ';
			if ($nbProduits == 0) $groupExplain .= $this->l('of').' ';
			if ($nbCategories == 0) $groupExplain .= $this->l('all the categories of your website');
			if ($nbCategories > 0 && $exclusionCategories == 1) $groupExplain .= $this->l('all the categories of your website excepted').' '. $nbCategories .' '.$this->l('of them');
			if ($nbCategories > 0 && $exclusionCategories == 0) $groupExplain .= $nbCategories . ' ' . $this->l('categorie(s)');
			if ($nbFabricants == 0) $groupExplain .= ', '.$this->l('of every manufacturer');
			if ($nbFabricants > 0 && $exclusionFabricants == 1) $groupExplain .= ', '.$this->l('of every manufacturer excepted').' '. $nbFabricants .' '.$this->l('of them');
			if ($nbFabricants > 0 && $exclusionFabricants == 0) $groupExplain .= ', '.$this->l('of').' '. $nbFabricants .' '.$this->l('manufacturer(s)');
			if ($nbFournisseurs == 0) $groupExplain .= ', '.$this->l('of every supplier');
			if ($nbFournisseurs > 0 && $exclusionFournisseurs == 1) $groupExplain .= ', '.$this->l('of every supplier excepted').' '. $nbFournisseurs .' '.$this->l('of them');
			if ($nbFournisseurs > 0 && $exclusionFournisseurs == 0) $groupExplain .= ', '.$this->l('of').' '. $nbFournisseurs .' '.$this->l('supplier(s)');
		} else if ($group_obj->group_type == 2) {
			$nbPagesCMS = sizeof($group_obj->getCMSPages());
			$exclusionPagesCMS = $group_obj->cms_type;
			if ($nbPagesCMS == 0) $groupExplain .= $this->l('You have to choose at least one CMS page');
			if ($nbPagesCMS > 0 && $exclusionPagesCMS == 1) $groupExplain .= ''.$this->l('Your selection will contain all the CMS pages excepted').' '. $nbPagesCMS .' '.$this->l('of them').', ';
			if ($nbPagesCMS > 0 && $exclusionPagesCMS == 0) $groupExplain .= ''.$this->l('Your selection will contain').' '. $nbPagesCMS .' '.$this->l('CMS page(s)');
		} else if ($group_obj->group_type == 4) {
			$nbCategories = sizeof($group_obj->getCategories());
			$exclusionCategories = $group_obj->category_type;
			$groupExplain = $this->l('Your selection will contain') . ' ';
			if ($nbCategories > 0 && $exclusionCategories == 1) $groupExplain .= $this->l('all the categories of your website excepted').' '. $nbCategories .' '.$this->l('of them');
			if ($nbCategories > 0 && $exclusionCategories == 0) $groupExplain .= $nbCategories . ' ' . $this->l('categorie(s)');
		} else if ($group_obj->group_type == 5) {
			$nbManufacturers = sizeof($group_obj->getManufacturers());
			$exclusionManufacturers = $group_obj->manufacturer_type;
			$groupExplain = $this->l('Your selection will contain') . ' ';
			if ($nbManufacturers > 0 && $exclusionManufacturers == 1) $groupExplain .= $this->l('all the manufacturers of your website excepted').' '. $nbManufacturers .' '.$this->l('of them');
			if ($nbManufacturers > 0 && $exclusionManufacturers == 0) $groupExplain .= $nbManufacturers . ' ' . $this->l('manufacturer(s)');
		}
		return $groupExplain;
	}
	public function displayGroupTable(){
		$this->_html .= '<div id="displayGroupTable" rel="' . $this->_base_config_url . '&getPanel=displayGroupTable">
						 <table id="groupTable" cellspacing="0" cellpadding="0" class="display"  style="width:100%;">
       					 <thead>
	    					<tr>
	                          <th width="50" style="text-align:center;">' . $this->l('ID') . '</th>
	                          <th style="width:auto; text-align:center;">' . $this->l('Group name') . '</th>
	                          <th style="width:auto; text-align:center;">' . $this->l('Group type') . '</th>
	                          <th style="width:auto; text-align:center;">' . $this->l('Group description') . '</th>
	                          <th width="60" style="text-align:center;">' . $this->l('Edit') . '</th>
	                    	  <th width="60" style="text-align:center;">' . $this->l('Delete') . '</th>
	                        </tr>
	                     </thead>';
		$this->_html .= '<tbody>';
		$groups = $this->getGroups(NULL, $this->_default_language);
		$group_type = $this->getGroupsType();
		if ($groups && sizeof($groups)) {
			foreach ($groups as $group) {
				$group_obj = new seointernallinkinggroupclass($group['id_group']);
				$this->_html .= '<tr>
									<td width="50">' . $group['id_group'] . '</td>
								 	<td style="width:auto">' . $group['name'] . '</td>
								 	<td style="width:auto">' . $group_type[$group['group_type']] . '</td>
									<td style="width:auto">' . $this->getGroupCombinaisonInformations($group_obj) . '</td>
									';
				$this->_html .= '<td style="text-align:center;">';
				$this->_addButton(array('text' => '', 'href' => $this->_base_config_url . '&getPanel=displayFormAddGroup&alter=1&id_group='.(int)$group['id_group'], 'onclick' => false, 'icon_class' => 'ui-icon ui-icon-pencil', 'class' => 'open_on_dialog_iframe', 'title' => '', 'rel' => '800_500_1'));
				$this->_html .= '</td><td style="text-align:center;">';
				$this->_addButton(array('text' => '', 'href' => $this->_base_config_url . '&deleteGroup=1&id_group=' . (int)$group['id_group'], 'onclick' => false, 'icon_class' => 'ui-icon ui-icon-trash', 'class' => 'ajax_script_load pm_confirm', 'title' => $this->l('Delete this group and all the associated expressions') . ' ?'));
				$this->_html .= '</td></tr>';
			}
		}
		$this->_html .= '</tbody>
				</table>
		</div>';
		$this->_initDataTable('groupTable', false, false, $this->_default_datatables_length);
	}
	public function displayExpressionTable(){
		$this->_html .= '<div id="displayExpressionTable" rel="' . $this->_base_config_url . '&getPanel=displayExpressionTable">';
		$groups = $this->getGroups(NULL, $this->_default_language);
		if (!self::_isFilledArray($groups)) {
			$this->_html .= '<p class="error">' . $this->l('You must have at least one group in order to add an expression.') . '</p>';
		} else {
			$this->_addButton(array('text' => $this->l('Add a new expression'), 'href' => $this->_base_config_url . '&getPanel=displayFormAddExpression', 'onclick' => false, 'icon_class' => 'ui-icon ui-icon-circle-plus', 'class' => 'open_on_dialog_iframe', 'title' => '', 'rel' => '800_500_1'));
			$this->_html .= '
							 <table id="expressionTable" cellspacing="0" cellpadding="0" class="display"  style="width:100%;">
							 <thead>
								<tr>
								  <th width="50" style="text-align:center;">' . $this->l('ID') . '</th>
								  <th style="width:auto; text-align:center;">' . $this->l('Expression') . '</th>
								  <th style="text-align:center;">' . $this->l('Language') . '</th>
								  <th style="text-align:center;">' . $this->l('Group') . '</th>
								  <th style="text-align:center;">' . $this->l('URL') . '</th>
								  <th width="30" style="text-align:center;">' . $this->l('No follow') . '</th>
								  <th width="30" style="text-align:center;">' . $this->l('Open in a new window') . '</th>
								  <th width="30" style="text-align:center;">' . $this->l('Active') . '</th>
								  <th width="40" style="text-align:center;">' . $this->l('Duplicate') . '</th>
								  <th width="40" style="text-align:center;">' . $this->l('Edit') . '</th>
								  <th width="40" style="text-align:center;">' . $this->l('Delete') . '</th>
								</tr>
							 </thead>';
			$this->_html .= '<tbody>';
			$expressions = $this->getExpressions();
			if ($expressions && sizeof($expressions)) {
				$language_cache = array();
				foreach ( $expressions as $expressions ) {
					if (!isset($language_cache[$expressions['id_lang']])) $language_cache[$expressions['id_lang']] = new Language($expressions['id_lang']);
					$this->_html .= '<tr>
										<td width="50">' . $expressions['id_expression'] . '</td>
										<td style="width:auto">' . $expressions['expression_content'] . '</td>
										<td style="width:auto">' . $language_cache[$expressions['id_lang']]->name . '</select></td>
										<td style="width:auto">' . $expressions['group_name'] . '</td>
										<td style="text-align:center;"><a href="'.$expressions['associated_url'].'" title="'.$expressions['associated_url'].'" target="_blank" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-link"></span></a></td>
										<td style="text-align:center;">
											<span id="spanNoFollowExpression' . $expressions['id_expression'] . '" style="display:none">'.$expressions['nofollow'].'</span>
											<a href="' . $this->_base_config_url . '&noFollowExpression=1&id_expression=' . $expressions['id_expression'] . '" class="ajax_script_load">
												<img	title="'.($expressions['nofollow'] ? $this->l('Enabled') : $this->l('Disabled')).'"
														alt="'.($expressions['nofollow'] ? $this->l('Enabled') : $this->l('Disabled')).'"
														src="'.$this->_path.'img/module_'.($expressions['nofollow'] ? "install" : "disabled").'.png"
														id="imgNoFollowExpression' . $expressions['id_expression'] . '"/>
											</a>
										</td>
										<td style="text-align:center;">
											<span id="spanNewWindowExpression' . $expressions['id_expression'] . '" style="display:none">'.$expressions['new_window'].'</span>
											<a href="' . $this->_base_config_url . '&newWindowExpression=1&id_expression=' . $expressions['id_expression'] . '" class="ajax_script_load">
												<img	title="'.($expressions['new_window'] ? $this->l('Enabled') : $this->l('Disabled')).'"
														alt="'.($expressions['new_window'] ? $this->l('Enabled') : $this->l('Disabled')).'"
														src="'.$this->_path.'img/module_'.($expressions['new_window'] ? "install" : "disabled").'.png"
														id="imgNewWindowExpression' . $expressions['id_expression'] . '"/>
											</a>
										</td>
										<td style="text-align:center;">
											<span id="spanActiveExpression' . $expressions['id_expression'] . '" style="display:none">'.$expressions['active'].'</span>
											<a href="' . $this->_base_config_url . '&activeExpression=1&id_expression=' . $expressions['id_expression'] . '" class="ajax_script_load">
												<img	title="'.($expressions['active'] ? $this->l('Active') : $this->l('Inactive')).'"
														alt="'.($expressions['active'] ? $this->l('Active') : $this->l('Inactive')).'"
														src="'.$this->_path.'img/module_'.($expressions['active'] ? "install" : "disabled").'.png"
														id="imgActiveExpression' . $expressions['id_expression'] . '"/>
											</a>
										</td>
										';
					$this->_html .= '<td style="text-align:center;">';
					$this->_addButton(array('text' => '', 'href' => $this->_base_config_url . '&getPanel=displayFormAddExpression&duplicate=1&id_expression='.(int)$expressions['id_expression'], 'onclick' => false, 'icon_class' => 'ui-icon ui-icon-extlink', 'class' => 'open_on_dialog_iframe', 'title' => '', 'ref' => '800_500_1'));
					$this->_html .= '</td><td style="text-align:center;">';
					$this->_addButton(array('text' => '', 'href' => $this->_base_config_url . '&getPanel=displayFormAddExpression&alter=1&id_expression='.(int)$expressions['id_expression'], 'onclick' => false, 'icon_class' => 'ui-icon ui-icon-pencil', 'class' => 'open_on_dialog_iframe', 'title' => '', 'ref' => '800_500_1'));
					$this->_html .= '</td><td style="text-align:center;">';
					$this->_addButton(array('text' => '', 'href' => $this->_base_config_url . '&deleteExpression=1&id_expression=' . (int) $expressions['id_expression'], 'onclick' => false, 'icon_class' => 'ui-icon ui-icon-trash', 'class' => 'ajax_script_load pm_confirm', 'title' => $this->l('Delete item #') . $expressions['id_expression'] . ' ?'));
					$this->_html .= '</td></tr>';
				}
			}
			$this->_html .= '</tbody>
					</table>';
			$this->_initDataTable('expressionTable', false, false, $this->_default_datatables_length);
		}
		$this->_html .= '</div>';
	}
	private function getLanguagesForSelectOptions() {
		$options = array();
		foreach (Language::getLanguages(false) as $language)
			$options[$language['id_lang']] = $language['name'];
		return $options;
	}
	private function getExpressions($ids_shop = array()) {
		$result = self::Db_ExecuteS('
			SELECT psil.*, psilgl.name as group_name
			FROM `'._DB_PREFIX_.'pm_seointernallinking` psil
			LEFT JOIN `'._DB_PREFIX_.'pm_seointernallinking_group` psilg ON (psil.id_group=psilg.id_group)
			LEFT JOIN `'._DB_PREFIX_.'pm_seointernallinking_group_lang` psilgl ON (psil.id_group=psilgl.id_group AND psilgl.id_lang='.(int)$this->_default_language.')'
			. self::addSqlAssociation('pm_seointernallinking_group', 'psilg','id_group')
			. self::addSqlGroupBy('psil','id_expression') .
			'ORDER BY `id_expression` ASC
		');
		return $result;
	}
	private function getGroups($id_group = NULL, $id_lang = NULL) {
		$result = self::Db_ExecuteS('
			SELECT sgl.*, sg.*
			FROM `'._DB_PREFIX_.'pm_seointernallinking_group` sg
			LEFT JOIN `'._DB_PREFIX_.'pm_seointernallinking_group_lang` sgl ON (sg.id_group=sgl.id_group '.(($id_lang && is_numeric($id_lang) && $id_lang > 0) ? 'AND sgl.id_lang="'.pSQL($id_lang).'"' : '').')
			'. self::addSqlAssociation('pm_seointernallinking_group', 'sg','id_group') . '
			' . (($id_group && is_numeric($id_group) && $id_group > 0) ? 'WHERE sg.id_group="'.pSQL($id_group).'"' : '')
			. self::addSqlGroupBy('sg','id_group') .
			'ORDER BY sg.id_group ASC
		');
		return $result;
	}
	private function getLinkPositionForSelectOptions() {
		$options = array(
			1 => $this->l('Top'),
			2 => $this->l('Middle'),
			3 => $this->l('Bottom')
		);
		return $options;
	}
	private function getGroupsForSelectOptions() {
		$options = array();
		foreach ($this->getGroups(NULL, $this->_default_language) as $group)
			$options[$group['id_group']] = $group['name'];
		return $options;
	}
	private function getGroupsType() {
		$options = array(
			1 => $this->l('Products'),
			2 => $this->l('CMS'),
			3 => $this->l('Editorial Module'),
			4 => $this->l('Categories'),
		);
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) $options[5] = $this->l('Manufacturers');
		return $options;
	}
	public function displayFormAddExpression($obj) {
		$this->_html = '';
		$updateForm = false;
		if ($obj && isset($obj->id_expression)) $updateForm = true;
		$this->_startForm(array('id' => 'formAddExpression', 'obj' => $obj));
		if ($updateForm) {
			$this->_displayTitle($this->l('Update an expression'));
		} else if (Tools::getValue('duplicate') && Tools::getValue('id_expression')) {
			$this->_displayTitle($this->l('Duplicate an expression'));
		} else {
			$this->_displayTitle($this->l('Add a new Expression'));
		}
		if ($updateForm) $this->_html .= '<input type="hidden" name="id_expression" id="id_expression" value="' . (int) $obj->id_expression . '" />';
		$this->_displaySelect(array('obj' => $obj, 'options' => $this->getLanguagesForSelectOptions(), 'key' => 'id_lang', 'label' => $this->l('Language to be used'), 'defaultvalue' => false, 'selectedvalue' => $this->_default_language, 'size' => '200px'));
		$this->_displaySelect(array('obj' => $obj, 'options' => $this->getGroupsForSelectOptions(), 'key' => 'id_group', 'label' => $this->l('Group to be used'), 'defaultvalue' => false, 'size' => '200px'));
		$this->_html .= '<div class="clear"></div><br />';
		$this->_displayInputText(array('obj' => $obj, 'key' => 'expression_content', 'label' => $this->l('Expression (one or several words)'), 'size' => '200px', 'required' => true));
		$this->_displayInputText(array('obj' => $obj, 'key' => 'associated_url', 'label' => $this->l('URL associated with the expression'), 'size' => '300px', 'required' => true));
		$this->_displayInputText(array('obj' => $obj, 'key' => 'url_title', 'label' => $this->l('URL Title (maximum 100 characters)'), 'size' => '300px', 'required' => true, 'maxlength' => 100));
		$this->_displaySelect(array('obj' => $obj, 'options' => $this->getLinkPositionForSelectOptions(), 'key' => 'link_position', 'label' => $this->l('Position of the link'), 'defaultvalue' => false, 'size' => '200px'));
		$this->_html .= '<div class="clear"></div><br />';
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'nofollow', 'key_db' => 'nofollow', 'label' => $this->l('Add the "nofollow" attribute (search engines will not follow this link)')));
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'new_window', 'key_db' => 'new_window', 'label' => $this->l('Open link in a new window/tab')));
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'active_expression', 'key_db' => 'active', 'label' => $this->l('Activate expression')));
		$this->_displaySubmit($this->l(' Save '), 'submit_expression');
		$this->_endForm(array('id' => 'formAddExpression'));
	}
	public function displayFormAddGroup($obj) {
		$this->_html = '';
		$this->showShopContextWarning();
		$updateForm = false;
		if ($obj && isset($obj->id_group)) $updateForm = true;
		$this->_startForm(array('id' => 'formAddGroup', 'obj' => $obj));
		if ($updateForm) {
			$this->_displayTitle($this->l('Update Group'));
		} else {
			$this->_displayTitle($this->l('Add a new Group'));
		}
		if ($updateForm) {
			$this->_html .= '<input type="hidden" name="id_group" id="id_group" value="' . (int) $obj->id_group . '" />';
		}
		$this->_displayInputTextLang(array('obj' => $obj, 'key' => 'name', 'label' => $this->l('Group name'), 'required' => true));
		$this->_displaySelect(array('obj' => $obj, 'options' => $this->getGroupsType(), 'key' => 'group_type', 'label' => $this->l('Type of Group'), 'defaultvalue' => false, 'size' => '200px'));
		$this->_html .= '<p id="groupInformations"></p>';
		$this->_html .= '<div id="groupProductZone">';
		$this->_html .= '<div id="wrapProductsConfigTab">
              <ul style="height: 30px;" id="configTab">
                <li><a href="#config-products-1">' . $this->l('Categories') . '</a></li>
                <li><a href="#config-products-2">' . $this->l('Products') . '</a></li>
                <li><a href="#config-products-3">' . $this->l('Manufacturers') . '</a></li>
                <li><a href="#config-products-4">' . $this->l('Suppliers') . '</a></li>
            </ul>';
		$this->_html .= '<div id="config-products-1">';
		$this->_html .= '<div id="categoriesWarning" style="display:none">';
		if (version_compare(_PS_VERSION_, '1.5.0.0', '<')) {
			$this->_showWarning(
				  $this->l('This feature needs some Core modifications to your shop.') . '<br />'
				. $this->l('You have to allow HTML into description field for the categories (into Category class and by editing category table).') . '<br />'
				. $this->l('If you are not familiar with that, we can do it for you.').' <a href="http://addons.prestashop.com/contact-community.php?id_product=4982" target="_blank">'.$this->l('Please contact us.').'</a>'
			);
		} else if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
			$pm_htmloncategories = Module::getInstanceByName('pm_htmloncategories');
			if (is_object($pm_htmloncategories) && isset($pm_htmloncategories->active) && $pm_htmloncategories->active != true) {
				$this->_showWarning(
					  $this->l('This feature require the HTML on product & cms categories and manufacturers module.') . '<br />'
					. $this->l('You can buy it here :') . '<br />'
					. '<a href="http://addons.prestashop.com/product.php?id_product=6254" target="_blank">'.$this->l('HTML on product & cms categories and manufacturers').'</a>'
				);
			}
		}
		$this->_html .= '</div>';
		if ($updateForm) $categories_groupe = $obj->getCategories(); else $categories_groupe = array();
		$this->_displayCategoryTree(array(
			'label' => $this->l('Category'),
			'input_name' => 'categories',
			'selected_cat' => ((self::_isFilledArray($categories_groupe)) ? $categories_groupe : array(0)),
			'category_root_id' => (version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? Category::getRootCategory()->id : 1)
		));
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'category_type', 'key_db' => 'category_type', 'label' => $this->l('Categories above have to be excluded')));
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-products-2">';
		$this->_html .= '<div class="product_picker">';
		$this->_displayAjaxSelectMultiple(array(
			'selectedoptions' => ($updateForm ? $obj->getProducts() : array()),
			'key' => 'products',
			'label' => $this->l('Products'),
			'remoteurl' => $this->_base_config_url . '&getItem=1&itemType=product',
			'limit' => 50,
			'limitincrement' => 20,
			'remoteparams' => false,
			'idcolumn' => 'id_product',
			'namecolumn' => 'name',
			'triggeronliclick' => true,
			'displaymore' => true
		));
		$this->_html .= '</div>';
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'product_type', 'key_db' => 'product_type', 'label' => $this->l('Products above have to be excluded')));
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-products-3">';
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
			$this->_html .= '<div id="manufacturersWarning" style="display:none">';
			$pm_htmloncategories = Module::getInstanceByName('pm_htmloncategories');
			if (is_object($pm_htmloncategories) && isset($pm_htmloncategories->active) && $pm_htmloncategories->active != true && version_compare($pm_htmloncategories->version, '1.1.0', '>=')) {
				$this->_showWarning(
					  $this->l('This feature require the HTML on product & cms categories and manufacturers module.') . '<br />'
					. $this->l('You can buy it here :') . '<br />'
					. '<a href="http://addons.prestashop.com/product.php?id_product=6254" target="_blank">'.$this->l('HTML on product & cms categories and manufacturers').'</a>'
				);
			}
			$this->_html .= '</div>';
		}
		$this->_html .= '<div class="manufacturer_picker">';
		$this->_displayAjaxSelectMultiple(array(
			'selectedoptions' => ($updateForm ? $obj->getManufacturers() : array()),
			'key' => 'manufacturers',
			'label' => $this->l('Manufacturers'),
			'remoteurl' => $this->_base_config_url . '&getItem=1&itemType=manufacturer',
			'limit' => 50,
			'limitincrement' => 20,
			'remoteparams' => false,
			'idcolumn' => 'id_manufacturer',
			'namecolumn' => 'name',
			'triggeronliclick' => true,
			'displaymore' => true
		));
		$this->_html .= '</div>';
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'manufacturer_type', 'key_db' => 'manufacturer_type', 'label' => $this->l('Manufacturers above have to be excluded')));
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-products-4">';
		$this->_html .= '<div class="supplier_picker">';
		$this->_displayAjaxSelectMultiple(array(
			'selectedoptions' => ($updateForm ? $obj->getSuppliers() : array()),
			'key' => 'suppliers',
			'label' => $this->l('Suppliers'),
			'remoteurl' => $this->_base_config_url . '&getItem=1&itemType=supplier',
			'limit' => 50,
			'limitincrement' => 20,
			'remoteparams' => false,
			'idcolumn' => 'id_supplier',
			'namecolumn' => 'name',
			'triggeronliclick' => true,
			'displaymore' => true
		));
		$this->_html .= '</div>';
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'supplier_type', 'key_db' => 'supplier_type', 'label' => $this->l('Suppliers above have to be excluded')));		
		$this->_html .= '</div>';
		$this->_html .= '</div>
		<script type="text/javascript">$jqPm(document).ready(function() { $jqPm("#wrapProductsConfigTab").tabs({active: '.(isset($obj->group_type) && $obj->group_type == 5 ? 2 : 0).'}); });</script>';
		$this->_html .= '</div>';
		$this->_html .= '<div id="groupCMSZone">';
		$this->_html .= '<div class="cms_picker">';
		$this->_displayAjaxSelectMultiple(array(
			'selectedoptions' => ($updateForm ? $obj->getCMSPages() : array()),
			'key' => 'cms_pages',
			'label' => $this->l('CMS Pages'),
			'remoteurl' => $this->_base_config_url . '&getItem=1&itemType=cms',
			'limit' => 50,
			'limitincrement' => 20,
			'remoteparams' => false,
			'idcolumn' => 'id_cms',
			'namecolumn' => 'meta_title',
			'triggeronliclick' => true,
			'displaymore' => true
		));
		$this->_html .= '</div>';
		$this->_displayInputActive(array('obj' => $obj, 'key_active' => 'cms_type', 'key_db' => 'cms_type', 'label' => $this->l('CMS Pages above have to be excluded')));
		$this->_html .= '</div>';
		$this->_html .= '<br />';
		$this->_html .= '
		<script type="text/javascript">
		$jqPm("form#formAddGroup").submit(function() {
			var nbPagesCMS = $jqPm("div.cms_picker div.ui-multiselect span.count").html().match(/[\d\.]+/g).join("");
			var nbCategories = $jqPm("input[name=\'categories[]\']:checked, input[name=\'categories[]\'][type=hidden]").size();
			var nbManufacturers = $jqPm("div.manufacturer_picker div.ui-multiselect span.count").html().match(/[\d\.]+/g).join("");
			if ($jqPm("select#group_type").val() == 1 && $jqPm("#groupInformations").html() == "") {
				alert("'.addcslashes($this->l('You must select an item among the different tabs'), '"').'")
				return false;
			} else if ($jqPm("select#group_type").val() == 2 && nbPagesCMS == 0) {
				alert("'.addcslashes($this->l('You have to choose at least one CMS page'), '"').'")
				return false;
			} else if ($jqPm("select#group_type").val() == 4 && nbCategories == 0) {
				alert("'.addcslashes($this->l('You have to choose at least one category'), '"').'")
				return false;
			} else if ($jqPm("select#group_type").val() == 5 && nbManufacturers == 0) {
				alert("'.addcslashes($this->l('You have to choose at least one manufacturer'), '"').'")
				return false;
			}
			return true;
		});
		function getGroupCombinaisonInformations() {
			var groupExplain = "";
			if ($jqPm("select#group_type").val() == 1) {
				var nbCategories = $jqPm("input[name=\'categories[]\']:checked, input[name=\'categories[]\'][type=hidden]").size();
				var exclusionCategories = $jqPm("input#category_type_on:checked").size();
				var nbProduits = $jqPm("div.product_picker div.ui-multiselect span.count").html().match(/[\d\.]+/g).join("");
				var exclusionProduits = $jqPm("input#product_type_on:checked").size();
				var nbFabricants = $jqPm("div.manufacturer_picker div.ui-multiselect span.count").html().match(/[\d\.]+/g).join("");
				var exclusionFabricants = $jqPm("input#manufacturer_type_on:checked").size();
				var nbFournisseurs = $jqPm("div.supplier_picker div.ui-multiselect span.count").html().match(/[\d\.]+/g).join("");
				var exclusionFournisseurs = $jqPm("input#supplier_type_on:checked").size();
				if (nbCategories == 0 && nbProduits == 0 && nbFabricants == 0 && nbFournisseurs == 0) {
					$jqPm("#groupInformations").html("");
					return;
				}
				if (nbProduits == 0) groupExplain += "'.addcslashes($this->l('Your products selection will be part'), '"').' ";
				if (nbProduits > 0 && exclusionProduits == 1) groupExplain += "'.addcslashes($this->l('Your selection will contain all products excepted'), '"').' "+ nbProduits +" '.addcslashes($this->l('of them'), '"').', ";
				if (nbProduits > 0 && exclusionProduits == 0) groupExplain += "'.addcslashes($this->l('Your selection will contain'), '"').' "+ nbProduits +" '.addcslashes($this->l('product(s)'), '"').' ";
				if (nbProduits > 0) groupExplain += "'.addcslashes($this->l('in'), '"').' ";
				if (nbProduits == 0) groupExplain += "'.addcslashes($this->l('of'), '"').' ";
				if (nbCategories == 0) groupExplain += "'.addcslashes($this->l('all the categories of your website'), '"').'";
				if (nbCategories > 0 && exclusionCategories == 1) groupExplain += "'.addcslashes($this->l('all the categories of your website excepted'), '"').' "+ nbCategories +" '.addcslashes($this->l('of them'), '"').'";
				if (nbCategories > 0 && exclusionCategories == 0) groupExplain += "" + nbCategories +" '.addcslashes($this->l('categorie(s)'), '"').'";
				if (nbFabricants == 0) groupExplain += ", '.addcslashes($this->l('of every manufacturer'), '"').'";
				if (nbFabricants > 0 && exclusionFabricants == 1) groupExplain += ", '.addcslashes($this->l('of every manufacturer excepted'), '"').' "+ nbFabricants +" '.addcslashes($this->l('of them'), '"').'";
				if (nbFabricants > 0 && exclusionFabricants == 0) groupExplain += ", '.addcslashes($this->l('of'), '"').' "+ nbFabricants +" '.addcslashes($this->l('manufacturer(s)'), '"').'";
				if (nbFournisseurs == 0) groupExplain += ", '.addcslashes($this->l('of every supplier'), '"').'";
				if (nbFournisseurs > 0 && exclusionFournisseurs == 1) groupExplain += ", '.addcslashes($this->l('of every supplier excepted'), '"').' "+ nbFournisseurs +" '.addcslashes($this->l('of them'), '"').'";
				if (nbFournisseurs > 0 && exclusionFournisseurs == 0) groupExplain += ", '.addcslashes($this->l('of'), '"').' "+ nbFournisseurs +" '.addcslashes($this->l('supplier(s)'), '"').'";
			} else if ($jqPm("select#group_type").val() == 2) {
				$jqPm("select#multiselectcms_pages").multiSelectUpdateCount();
				var nbPagesCMS = $jqPm("div.cms_picker div.ui-multiselect span.count").html().match(/[\d\.]+/g).join("");
				var exclusionPagesCMS = $jqPm("input#cms_type_on:checked").size();
				if (nbPagesCMS == 0) groupExplain += "'.addcslashes($this->l('You have to choose at least one CMS page'), '"').'";
				if (nbPagesCMS > 0 && exclusionPagesCMS == 1) groupExplain += "'.addcslashes($this->l('Your selection will contain all the CMS pages excepted'), '"').' "+ nbPagesCMS +" '.addcslashes($this->l('of them'), '"').', ";
				if (nbPagesCMS > 0 && exclusionPagesCMS == 0) groupExplain += "'.addcslashes($this->l('Your selection will contain'), '"').' "+ nbPagesCMS +" '.addcslashes($this->l('CMS page(s)'), '"').' ";
			} else if ($jqPm("select#group_type").val() == 4) {
				var nbCategories = $jqPm("input[name=\'categories[]\']:checked, input[name=\'categories[]\'][type=hidden]").size();
				var exclusionCategories = $jqPm("input#category_type_on:checked").size();
				if (nbCategories == 0) {
					$jqPm("#groupInformations").html("");
					return;
				}
				groupExplain = "'.addcslashes($this->l('Your selection will contain'), '"').' ";
				if (nbCategories > 0 && exclusionCategories == 1) groupExplain += "'.addcslashes($this->l('all the categories of your website excepted'), '"').' "+ nbCategories +" '.addcslashes($this->l('of them'), '"').'";
				if (nbCategories > 0 && exclusionCategories == 0) groupExplain += "" + nbCategories +" '.addcslashes($this->l('categorie(s)'), '"').'";
			} else if ($jqPm("select#group_type").val() == 5) {
				var nbFabricants = $jqPm("div.manufacturer_picker div.ui-multiselect span.count").html().match(/[\d\.]+/g).join("");
				var exclusionFabricants = $jqPm("input#manufacturer_type_on:checked").size();
				if (nbFabricants == 0) {
					$jqPm("#groupInformations").html("");
					return;
				}
				groupExplain = "'.addcslashes($this->l('Your selection will contain'), '"').' ";
				if (nbFabricants > 0 && exclusionFabricants == 1) groupExplain += "'.addcslashes($this->l('all the manufacturers of your website excepted'), '"').' "+ nbFabricants +" '.addcslashes($this->l('of them'), '"').'";
				if (nbFabricants > 0 && exclusionFabricants == 0) groupExplain += "" + nbFabricants +" '.addcslashes($this->l('manufacturer(s)'), '"').'";
			}
			$jqPm("#groupInformations").html(groupExplain);
		}
		function checkForChanges() {
			getGroupCombinaisonInformations();
			setTimeout(checkForChanges, 1000);
		}
		function countAllMultiSelect() {
			$jqPm("a[href=\'#config-products-2\']").trigger("click");
			$jqPm("select#multiselectproducts").multiSelectUpdateCount();
			$jqPm("a[href=\'#config-products-3\']").trigger("click");
			$jqPm("select#multiselectmanufacturers").multiSelectUpdateCount();
			$jqPm("a[href=\'#config-products-4\']").trigger("click");
			$jqPm("select#multiselectsuppliers").multiSelectUpdateCount();
			$jqPm("a[href=\'#config-products-1\']").trigger("click");
			if ($jqPm("div#groupCMSZone:visible").size()) {
				$jqPm("select#multiselectcms_pages").multiSelectUpdateCount();
			} else {
				$jqPm("div#groupCMSZone").show(0);
				$jqPm("select#multiselectcms_pages").multiSelectUpdateCount();
				$jqPm("div#groupCMSZone").hide(0);
			}
			checkForChanges();
			$jqPm("select#group_type").trigger("change");
		}
		$jqPm(document).ready(function() { setTimeout(countAllMultiSelect, 500); });
		$jqPm("select[name=\'group_type\'], input[name=\'categories[]\'], input.check_all_children, input#category_type_on, input#category_type_off, input#supplier_type_on, input#supplier_type_off, input#manufacturer_type_on, input#manufacturer_type_off, input#product_type_on, input#product_type_off, input#cms_type_on, input#cms_type_off").bind("change", function() {
			getGroupCombinaisonInformations();
		});
		$jqPm("select[name=\'group_type\']").bind("change", function() {
			if ($jqPm("select#group_type").val() == 1) {
				$jqPm("a[href=\'#config-products-2\']").trigger("click");
				$jqPm("select#multiselectproducts").multiSelectUpdateCount();
				$jqPm("a[href=\'#config-products-3\']").trigger("click");
				$jqPm("select#multiselectmanufacturers").multiSelectUpdateCount();
				$jqPm("a[href=\'#config-products-4\']").trigger("click");
				$jqPm("select#multiselectsuppliers").multiSelectUpdateCount();
				$jqPm("a[href=\'#config-products-1\']").trigger("click");
			} else if ($jqPm("select#group_type").val() == 2) {
				$jqPm("select#multiselectcms_pages").multiSelectUpdateCount();
			}
		});
		</script>';
		$this->_displaySubmit($this->l(' Save '), 'submit_group');
		$this->_endForm(array('id' => 'formAddGroup'));
	}
	public function getContent() {
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) $this->_html .= '<div id="pm_backoffice_wrapper" class="pm_bo_ps_'.substr(str_replace('.', '', _PS_VERSION_), 0, 2).'">';
		$this->_displayTitle($this->displayName);
		if (Tools::getValue('makeUpdate')) {
			$this->checkIfModuleIsUpdate(true);
		}
		if (! $this->checkIfModuleIsUpdate(false)) {
			$this->_html .= '
				<div class="warning warn clear"><p>' . $this->l('We have detected that you installed a new version of the module on your shop') . '</p>
					<p style="text-align: center"><a href="' . $this->_base_config_url . '&makeUpdate=1" class="button">' . $this->l('Please click here in order to finish the installation process') . '</a></p>
				</div>';
		} else {
			$this->preProcess();
			$this->_loadCssJsLibraries();
			$this->_postProcess();
			//$this->_showRating(false);
			parent::getContent();
			$this->displayForm();
			//$this->_displaySupport();
		}
		$this->_pmClear();
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) $this->_html .= '</div>';
		return $this->_html;
	}
	protected function preProcess() {
		parent::_preProcess();
		if (Tools::getValue('expressionFormValidation') == 1) {
			$this->_cleanOutput();
			$expression_content = trim(Tools::getValue('expression_content'));
			$id_lang = Tools::getValue('id_lang');
			$id_group = Tools::getValue('id_group');
			$id_expression = Tools::getValue('id_expression', 0);
			$count_total = self::Db_getRow('
				SELECT COUNT(*) as nb
				FROM `' . _DB_PREFIX_ . 'pm_seointernallinking`
				WHERE expression_content LIKE "'.pSQL($expression_content).'"
				AND id_lang="'.pSQL($id_lang).'"
				AND id_group="'.pSQL($id_group).'"
				'.((isset($id_expression) && is_numeric($id_expression) && $id_expression > 0) ? 'AND id_expression!="'.pSQL($id_expression).'"' : '').'
				');
			if ((int)($count_total['nb']) > 0) {
				$this->_html .= json_encode(array('expression_content' => $this->l('This expression already exists into this language & group.')));
			} else {
				$this->_html .= 'true';
			}
			$this->_echoOutput(true);
		}
		if (Tools::getValue('synchroniseEverything') == 1) {		
			session_start();
			$_SESSION['synchronise_everything_process'] = true;
			$this->_cleanOutput();
			$this->_html .= '
				$jqPm("a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks").hide();
				$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
				// Products
				$jqPm("#progressSyncProductInformation").hide();
				$jqPm("#progressSyncAllInformation").html($jqPm("#progressSyncProductInformation").html()).show();
				$jqPm("#progressSyncProductRemainingTime").show();
				$jqPm("#progressSyncProduct span em").css("left", 0);
				$jqPm("#progressSyncProduct").show();
				// CMS
				$jqPm("#progressSyncCMSPagesInformation").hide();
				$jqPm("#progressSyncCMSPagesRemainingTime").show();
				$jqPm("#progressSyncCMSPages span em").css("left", 0);
				$jqPm("#progressSyncCMSPages").show();
				// Catégories
				$jqPm("#progressSyncCategoriesInformation").hide();
				$jqPm("#progressSyncCategoriesRemainingTime").show();
				$jqPm("#progressSyncCategories span em").css("left", 0);
				$jqPm("#progressSyncCategories").show();
				// Fabricants
				$jqPm("#progressSyncManufacturersInformation").hide();
				$jqPm("#progressSyncManufacturersRemainingTime").show();
				$jqPm("#progressSyncManufacturers span em").css("left", 0);
				$jqPm("#progressSyncManufacturers").show();
				// Editorial
				$jqPm("#progressSyncEditorialInformation").hide();
				$jqPm("#progressSyncEditorial span em").css("left", 0);
				$jqPm("#progressSyncEditorial").show();
				$jqPm.ajax( {
					type : "GET",
					url : \'' . $this->_base_config_url . '&synchroniseAllProducts=1' . '\',
					dataType : "script",
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						//alert(msgAjaxError);
					}
				});
			';
			$this->_echoOutput(true);
			return;
		}
		if (Tools::getValue('removeAllLinks') == 1) {
			session_start();
			$this->_cleanOutput();
			$this->cleanAllHTMLField('products');
			$this->cleanAllHTMLField('cms');
			$this->cleanAllHTMLField('categories');
			if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) $this->cleanAllHTMLField('manufacturers');
			$this->cleanEditorialContent();
			$this->_html .= '
				show_info("'. addcslashes($this->l('All the links have been removed !'), '"') .'");
				$jqPm("#deleteAllContainer").addClass(\'taskDone\');
				$jqPm("#progressDeleteAllInformation").hide();
				$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
				$jqPm("a.removealllinks").show();
			';
			session_destroy();
			$this->_echoOutput(true);
			return;
		}
		if (Tools::getValue('synchroniseEditorial') == 1) {
			$this->_cleanOutput();
			session_start();
			if (!isset($_SESSION['clean_html_field_editorial'])) {
				$this->cleanEditorialContent();
				$_SESSION['clean_html_field_editorial'] = true;
			}
			$update_editorial_result = $this->updateEditorialContent();
			if ($update_editorial_result == 1) {
				$this->_html .= 'show_info("'. addcslashes($this->l('Optimization done !'), '"') .'");';
			} else if ($update_editorial_result == 2) {
				$this->_html .= 'show_info("'. addcslashes($this->l('Error while updating editorial module content. The module isn\'t installed.'), '"') .'");';
			} else if ($update_editorial_result == false) {
				if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) {
					$this->_html .= 'show_error("'. addcslashes($this->l('Error while updating editorial module content.'), '"') .'");';
				} else {
					$this->_html .= 'show_error("'. addcslashes($this->l('Error while updating editorial module content. The editorial.xml file is not writable.'), '"') .'");';
				}
			}
			$this->_html .= '
				$jqPm("#syncEditorialContainer").addClass(\'taskDone\');
				$jqPm("#progressSyncEditorialInformation").hide();
				$jqPm("#progressSyncEditorial").hide();
				$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
				$jqPm("a.synchroniseeditorial").show();
			';
			if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
				if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
				if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
				$this->_html .= '
				$jqPm("a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks").show();
				$jqPm("#progressSyncAllInformation").html("").hide();
				';
			}
			session_destroy();
			$this->_echoOutput(true);
			return;
		}
		if (Tools::getValue('synchroniseAllCMSPages') == 1) {
			$this->_cleanOutput();
			session_start();
			if (!isset($_SESSION['clean_html_field_cms_pages']))	{
				$this->cleanAllHTMLField('cms');
				$_SESSION['clean_html_field_cms_pages'] = true;
			}
			if (!isset($_SESSION['iteration_time_start']))	$_SESSION['iteration_time_start'] = time();
			if (!isset($_SESSION['count_total_ajax']) || !isset($_SESSION['count_total_iteration_step'])) {
				$_SESSION['count_total_ajax'] = $this->getCMSWordsToLink(0, 0, true);
				$_SESSION['count_total_iteration'] = 0;
				$_SESSION['count_total_iteration_step'] = (int)ceil($_SESSION['count_total_ajax']/2);
				if ($_SESSION['count_total_ajax'] > $this->_max_affected_rows) $_SESSION['count_total_iteration_step'] = (int)$this->_max_affected_rows;
			} else {
				$_SESSION['count_total_iteration'] += $_SESSION['count_total_iteration_step'];
			}
			if ($_SESSION['count_total_ajax'] == 0) {
				$this->_html .= '
					$jqPm("#progressSyncCMSPages").hide(1000, function() {
						show_info("'. addcslashes($this->l('Optimization done (no rows affected) !'), '"') .'");
						$jqPm("#syncCMSPagesContainer").addClass(\'taskDone\');
						$jqPm("#progressSyncCMSPagesInformation").hide();
						$jqPm("#progressSyncCMSPagesRemainingTime").html(\'\').hide();
				';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
							$jqPm("#progressSyncCMSPages span em").css("left", 0);
							$jqPm.ajax( {
								type : "GET",
								url : \'' . $this->_base_config_url . '&synchroniseAllCategories=1' . '\',
								dataType : "script",
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									//alert(msgAjaxError);
								}
							});
							$jqPm("#progressSyncAllInformation").html($jqPm("#progressSyncCategoriesInformation").html()).show();
					';
				} else {
					$this->_html .= '
								$jqPm("a.synchroniseallcmspages").show();
								$jqPm("#progressSyncCMSPages span em").css("left", 0);
								$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
							';
					session_destroy();
				}
				$this->_html .= '});';
				$this->_echoOutput(true);
				return;
			}
			if (isset($_SESSION['count_total_ajax']) && isset($_SESSION['count_total_iteration']) && $_SESSION['count_total_iteration'] > 0 && $_SESSION['count_total_ajax'] > 0 && $_SESSION['count_total_iteration'] >= $_SESSION['count_total_ajax']) {
				$avancement_synchro = (($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 100) $avancement_synchro = 100;
				$this->_html .= '
					$jqPm("#progressSyncCMSPages span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800, function() {
						$jqPm("#progressSyncCMSPages").hide(2000, function() {
							show_info("'. addcslashes($this->l('Optimization done !'), '"') .'");
							$jqPm("#syncCMSPagesContainer").addClass(\'taskDone\');
							$jqPm("#progressSyncCMSPagesInformation").hide();
							$jqPm("#progressSyncCMSPagesRemainingTime").html(\'\').hide();
				';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
							$jqPm("#progressSyncCMSPages span em").css("left", 0);
							$jqPm.ajax( {
								type : "GET",
								url : \'' . $this->_base_config_url . '&synchroniseAllCategories=1' . '\',
								dataType : "script",
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									//alert(msgAjaxError);
								}
							});
							$jqPm("#progressSyncAllInformation").html($jqPm("#progressSyncCategoriesInformation").html()).show();
					';
				} else {
					$this->_html .= '
								$jqPm("a.synchroniseallcmspages").show();
								$jqPm("#progressSyncCMSPages span em").css("left", 0);
								$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
							';
					session_destroy();
				}
				$this->_html .= '
						});
					});
				';
			} else {
				$avancement_synchro = (int)(($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 0) {
					$remaining_time = trim($this->_getDuration((((time() - $_SESSION['iteration_time_start']) * 100) /  $avancement_synchro) - (time() - $_SESSION['iteration_time_start'])));
					if ($remaining_time != '') $remaining_time .= ' '.$this->l('remaining').'...';
				} else {
					$remaining_time = $this->l('Remaining time calculation in progress...');
				}
				$this->_html .= '
					$jqPm("#progressSyncCMSPages span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800);
					$jqPm("#progressSyncCMSPagesRemainingTime").html(\''.$remaining_time.'\');
					$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
					$jqPm.ajax( {
						type : "GET",
						url : \'' . $this->_base_config_url . '&synchroniseAllCMSPages=1' . '\',
						dataType : "script",
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							//alert(msgAjaxError);
						}
					});
				';
				$words = $this->getCMSWordsToLink($_SESSION['count_total_iteration'], $_SESSION['count_total_iteration_step']);
				$this->updateHTMLField('cms', $words, false);
			}
			$this->_echoOutput(true);
			return;
		}
		if (Tools::getValue('synchroniseAllCategories') == 1) {
			$this->_cleanOutput();
			session_start();
			if (!isset($_SESSION['clean_html_field_categories']))	{
				$this->cleanAllHTMLField('categories');
				$_SESSION['clean_html_field_categories'] = true;
			}
			if (!isset($_SESSION['iteration_time_start']))	$_SESSION['iteration_time_start'] = time();
			if (!isset($_SESSION['count_total_ajax']) || !isset($_SESSION['count_total_iteration_step'])) {
				$_SESSION['count_total_ajax'] = $this->getCategoriesWordsToLink(NULL, 0, 0, true);
				$_SESSION['count_total_iteration'] = 0;
				$_SESSION['count_total_iteration_step'] = (int)ceil($_SESSION['count_total_ajax']/2);
				if ($_SESSION['count_total_ajax'] > $this->_max_affected_rows) $_SESSION['count_total_iteration_step'] = (int)$this->_max_affected_rows;
			} else {
				$_SESSION['count_total_iteration'] += $_SESSION['count_total_iteration_step'];
			}
			if ($_SESSION['count_total_ajax'] == 0) {
				$this->_html .= '
				$jqPm("#progressSyncCategories").hide(1000, function() {
				show_info("'. addcslashes($this->l('Optimization done (no rows affected) !'), '"') .'");
				$jqPm("#syncCategoriesContainer").addClass(\'taskDone\');
				$jqPm("#progressSyncCategoriesInformation").hide();
				$jqPm("#progressSyncCategoriesRemainingTime").html(\'\').hide();
				';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
					$jqPm("#progressSyncCategories span em").css("left", 0);
					$jqPm.ajax( {
					type : "GET",
					url : \'' . $this->_base_config_url . '&'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'synchroniseAllManufacturers=1' : 'synchroniseEditorial=1'). '\',
					dataType : "script",
					error: function(XMLHttpRequest, textStatus, errorThrown) {
					//alert(msgAjaxError);
				}
				});
				$jqPm("#progressSyncAllInformation").html($jqPm("#progressSync'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'Manufacturers' : 'Editorial'). 'Information").html()).show();
				';
				} else {
					$this->_html .= '
					$jqPm("a.synchroniseallcategories").show();
					$jqPm("#progressSyncCategories span em").css("left", 0);
					$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
					';
					session_destroy();
				}
				$this->_html .= '});';
				$this->_echoOutput(true);
				return;
			}
			if (isset($_SESSION['count_total_ajax']) && isset($_SESSION['count_total_iteration']) && $_SESSION['count_total_iteration'] > 0 && $_SESSION['count_total_ajax'] > 0 && $_SESSION['count_total_iteration'] >= $_SESSION['count_total_ajax']) {
				$avancement_synchro = (($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 100) $avancement_synchro = 100;
				$this->_html .= '
				$jqPm("#progressSyncCategories span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800, function() {
				$jqPm("#progressSyncCategories").hide(2000, function() {
				show_info("'. addcslashes($this->l('Optimization done !'), '"') .'");
				$jqPm("#syncCategoriesContainer").addClass(\'taskDone\');
				$jqPm("#progressSyncCategoriesInformation").hide();
				$jqPm("#progressSyncCategoriesRemainingTime").html(\'\').hide();
				';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
					$jqPm("#progressSyncCategories span em").css("left", 0);
					$jqPm.ajax( {
					type : "GET",
					url : \'' . $this->_base_config_url . '&'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'synchroniseAllManufacturers=1' : 'synchroniseEditorial=1'). '\',
					dataType : "script",
					error: function(XMLHttpRequest, textStatus, errorThrown) {
					//alert(msgAjaxError);
				}
				});
				$jqPm("#progressSyncAllInformation").html($jqPm("#progressSync'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'Manufacturers' : 'Editorial'). 'Information").html()).show();
				';
				} else {
					$this->_html .= '
					$jqPm("a.synchroniseallcategories").show();
					$jqPm("#progressSyncCategories span em").css("left", 0);
					$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
					';
					session_destroy();
				}
				$this->_html .= '
			});
			});
			';
			} else {
				$avancement_synchro = (int)(($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 0) {
					$remaining_time = trim($this->_getDuration((((time() - $_SESSION['iteration_time_start']) * 100) /  $avancement_synchro) - (time() - $_SESSION['iteration_time_start'])));
					if ($remaining_time != '') $remaining_time .= ' '.$this->l('remaining').'...';
				} else {
					$remaining_time = $this->l('Remaining time calculation in progress...');
				}
				$this->_html .= '
				$jqPm("#progressSyncCategories span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800);
				$jqPm("#progressSyncCategoriesRemainingTime").html(\''.$remaining_time.'\');
				$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
				$jqPm.ajax( {
				type : "GET",
				url : \'' . $this->_base_config_url . '&synchroniseAllCategories=1' . '\',
				dataType : "script",
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				//alert(msgAjaxError);
			}
			});
			';
				$words = $this->getCategoriesWordsToLink(NULL, $_SESSION['count_total_iteration'], $_SESSION['count_total_iteration_step']);
				$this->updateHTMLField('categories', $words, false);
			}
			$this->_echoOutput(true);
			return;
		}
		if (Tools::getValue('synchroniseAllManufacturers') == 1) {
			$this->_cleanOutput();
			session_start();
			if (!isset($_SESSION['clean_html_field_manufacturers'])) {
				$this->cleanAllHTMLField('manufacturers');
				$_SESSION['clean_html_field_manufacturers'] = true;
			}
			if (!isset($_SESSION['iteration_time_start']))	$_SESSION['iteration_time_start'] = time();
			if (!isset($_SESSION['count_total_ajax']) || !isset($_SESSION['count_total_iteration_step'])) {
				$_SESSION['count_total_ajax'] = $this->getManufacturersWordsToLink(NULL, 0, 0, true);
				$_SESSION['count_total_iteration'] = 0;
				$_SESSION['count_total_iteration_step'] = (int)ceil($_SESSION['count_total_ajax']/2);
				if ($_SESSION['count_total_ajax'] > $this->_max_affected_rows) $_SESSION['count_total_iteration_step'] = (int)$this->_max_affected_rows;
			} else {
				$_SESSION['count_total_iteration'] += $_SESSION['count_total_iteration_step'];
			}
			if ($_SESSION['count_total_ajax'] == 0) {
				$this->_html .= '
				$jqPm("#progressSyncManufacturers").hide(1000, function() {
				show_info("'. addcslashes($this->l('Optimization done (no rows affected) !'), '"') .'");
				$jqPm("#syncManufacturersContainer").addClass(\'taskDone\');
				$jqPm("#progressSyncManufacturersInformation").hide();
				$jqPm("#progressSyncManufacturersRemainingTime").html(\'\').hide();
				';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
					$jqPm("#progressSyncManufacturers span em").css("left", 0);
					$jqPm.ajax( {
					type : "GET",
					url : \'' . $this->_base_config_url . '&synchroniseEditorial=1' . '\',
					dataType : "script",
					error: function(XMLHttpRequest, textStatus, errorThrown) {
					//alert(msgAjaxError);
				}
				});
				$jqPm("#progressSyncAllInformation").html($jqPm("#progressSyncEditorialInformation").html()).show();
				';
				} else {
					$this->_html .= '
					$jqPm("a.synchroniseallmanufacturers").show();
					$jqPm("#progressSyncManufacturers span em").css("left", 0);
					$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
					';
					session_destroy();
				}
				$this->_html .= '});';
				$this->_echoOutput(true);
				return;
			}
			if (isset($_SESSION['count_total_ajax']) && isset($_SESSION['count_total_iteration']) && $_SESSION['count_total_iteration'] > 0 && $_SESSION['count_total_ajax'] > 0 && $_SESSION['count_total_iteration'] >= $_SESSION['count_total_ajax']) {
				$avancement_synchro = (($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 100) $avancement_synchro = 100;
				$this->_html .= '
				$jqPm("#progressSyncManufacturers span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800, function() {
				$jqPm("#progressSyncManufacturers").hide(2000, function() {
				show_info("'. addcslashes($this->l('Optimization done !'), '"') .'");
				$jqPm("#syncManufacturersContainer").addClass(\'taskDone\');
				$jqPm("#progressSyncManufacturersInformation").hide();
				$jqPm("#progressSyncManufacturersRemainingTime").html(\'\').hide();
				';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
					$jqPm("#progressSyncManufacturers span em").css("left", 0);
					$jqPm.ajax( {
					type : "GET",
					url : \'' . $this->_base_config_url . '&synchroniseEditorial=1' . '\',
					dataType : "script",
					error: function(XMLHttpRequest, textStatus, errorThrown) {
					//alert(msgAjaxError);
				}
				});
				$jqPm("#progressSyncAllInformation").html($jqPm("#progressSyncEditorialInformation").html()).show();
				';
				} else {
					$this->_html .= '
					$jqPm("a.synchroniseallmanufacturers").show();
					$jqPm("#progressSyncManufacturers span em").css("left", 0);
					$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
					';
					session_destroy();
				}
				$this->_html .= '
			});
			});
			';
			} else {
				$avancement_synchro = (int)(($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 0) {
					$remaining_time = trim($this->_getDuration((((time() - $_SESSION['iteration_time_start']) * 100) /  $avancement_synchro) - (time() - $_SESSION['iteration_time_start'])));
					if ($remaining_time != '') $remaining_time .= ' '.$this->l('remaining').'...';
				} else {
					$remaining_time = $this->l('Remaining time calculation in progress...');
				}
				$this->_html .= '
				$jqPm("#progressSyncManufacturers span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800);
				$jqPm("#progressSyncManufacturersRemainingTime").html(\''.$remaining_time.'\');
				$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", true).addClass("ui-state-disabled");
				$jqPm.ajax( {
				type : "GET",
				url : \'' . $this->_base_config_url . '&synchroniseAllManufacturers=1' . '\',
				dataType : "script",
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				//alert(msgAjaxError);
			}
			});
			';
				$words = $this->getManufacturersWordsToLink(NULL, $_SESSION['count_total_iteration'], $_SESSION['count_total_iteration_step']);
				$this->updateHTMLField('manufacturers', $words, false);
			}
			$this->_echoOutput(true);
			return;
		}
		if (Tools::getValue('synchroniseAllProducts') == 1) {
			$this->_cleanOutput();
			session_start();
			if (!isset($_SESSION['clean_html_field_products']))	{
				$this->cleanAllHTMLField('products');
				$_SESSION['clean_html_field_products'] = true;
			}
			if (!isset($_SESSION['iteration_time_start']))	$_SESSION['iteration_time_start'] = time();
			if (!isset($_SESSION['count_total_ajax']) || !isset($_SESSION['count_total_iteration_step'])) {
				$_SESSION['count_total_ajax'] = $this->getWordsToLink(NULL, 0, 0, true);
				$_SESSION['count_total_iteration'] = 0;
				$_SESSION['count_total_iteration_step'] = (int)ceil($_SESSION['count_total_ajax']/2);
				if ($_SESSION['count_total_ajax'] > $this->_max_affected_rows) $_SESSION['count_total_iteration_step'] = (int)$this->_max_affected_rows;
			} else {
				$_SESSION['count_total_iteration'] += $_SESSION['count_total_iteration_step'];
			}
			if ($_SESSION['count_total_ajax'] == 0) {
				$this->_html .= '
					$jqPm("#progressSyncProduct").hide(1000, function() {
						show_info("'. addcslashes($this->l('Optimization done (no rows affected) !'), '"') .'");
						$jqPm("#syncProductContainer").addClass(\'taskDone\');
						$jqPm("#progressSyncProductInformation").hide();
						$jqPm("#progressSyncProductRemainingTime").hide().html(\'\');
						';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
							$jqPm("#progressSyncProduct span em").css("left", 0);
							$jqPm.ajax( {
								type : "GET",
								url : \'' . $this->_base_config_url . '&synchroniseAllCMSPages=1' . '\',
								dataType : "script",
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									//alert(msgAjaxError);
								}
							});
							$jqPm("#progressSyncAllInformation").html($jqPm("#progressSyncCMSPagesInformation").html()).show();
							';
				} else {
					$this->_html .= '
							$jqPm("a.synchroniseallproducts").show();
							$jqPm("#progressSyncProduct span em").css("left", 0);
							$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
					';
					session_destroy();
				}
				$this->_html .= '});';
				$this->_echoOutput(true);
				return;
			}
			if (isset($_SESSION['count_total_ajax']) && isset($_SESSION['count_total_iteration']) && $_SESSION['count_total_iteration'] > 0 && $_SESSION['count_total_ajax'] > 0 && $_SESSION['count_total_iteration'] >= $_SESSION['count_total_ajax']) {
				$avancement_synchro = (($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 100) $avancement_synchro = 100;
				$this->_html .= '
					$jqPm("#progressSyncProduct span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800, function() {
						$jqPm("#progressSyncProduct").hide(2000, function() {
							show_info("'. addcslashes($this->l('Optimization done !'), '"') .'");
							$jqPm("#syncProductContainer").addClass(\'taskDone\');
							$jqPm("#progressSyncProductInformation").hide();
							$jqPm("#progressSyncProductRemainingTime").hide().html(\'\');
							';
				if (isset($_SESSION['synchronise_everything_process']) && $_SESSION['synchronise_everything_process'] == true) {
					if (isset($_SESSION['count_total_ajax'])) unset($_SESSION['count_total_ajax']);
					if (isset($_SESSION['count_total_iteration_step'])) unset($_SESSION['count_total_iteration_step']);
					$this->_html .= '
							$jqPm("#progressSyncProduct span em").css("left", 0);
							$jqPm.ajax( {
								type : "GET",
								url : \'' . $this->_base_config_url . '&synchroniseAllCMSPages=1' . '\',
								dataType : "script",
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									//alert(msgAjaxError);
								}
							});
							$jqPm("#progressSyncAllInformation").html($jqPm("#progressSyncCMSPagesInformation").html()).show();
							';
				} else {
					$this->_html .= '
							$jqPm("a.synchroniseallproducts").show();
							$jqPm("#progressSyncProduct span em").css("left", 0);
							$jqPm("a.synchroniseeverything, a.synchroniseeditorial, a.synchroniseallcmspages, a.synchroniseallcategories, a.synchroniseallmanufacturers, a.synchroniseallproducts, a.removealllinks, input[name=submit_global_options]").attr("disabled", false).removeClass("ui-state-disabled");
					';
					session_destroy();
				}
				$this->_html .= '
					});
				});';
			} else {
				$avancement_synchro = (int)(($_SESSION['count_total_iteration'] * 100) / $_SESSION['count_total_ajax']);
				if ($avancement_synchro > 0) {
					$remaining_time = trim($this->_getDuration((((time() - $_SESSION['iteration_time_start']) * 100) /  $avancement_synchro) - (time() - $_SESSION['iteration_time_start'])));
					if ($remaining_time != '') $remaining_time .= ' '.$this->l('remaining').'...';
				} else {
					$remaining_time = $this->l('Remaining time calculation in progress...');
				}
				$this->_html .= '
					$jqPm("#progressSyncProduct span em").animate({left: "'.(int)($avancement_synchro).'%"}, 800);
					$jqPm("#progressSyncProductRemainingTime").html(\''.$remaining_time.'\');
					$jqPm.ajax( {
						type : "GET",
						url : \'' . $this->_base_config_url . '&synchroniseAllProducts=1' . '\',
						dataType : "script",
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							//alert(msgAjaxError);
						}
					});
				';
				$words = $this->getWordsToLink(NULL, $_SESSION['count_total_iteration'], $_SESSION['count_total_iteration_step']);
				$this->updateHTMLField('products', $words, false);
			}
			$this->_echoOutput(true);
			return;
		}
		if (Tools::getValue('getPanel')){
			switch (Tools::getValue('getPanel')){
				case 'displayFormAddExpression' :
					$this->_cleanOutput();
					$obj = false;
					if (Tools::getValue('alter') && Tools::getValue('id_expression')) {
						$id_expression = Tools::getValue('id_expression');
						$obj = new SEOInternalLinkingExpressionClass($id_expression);
					} else if (Tools::getValue('duplicate') && Tools::getValue('id_expression')) {
						$id_expression = Tools::getValue('id_expression');
						$obj_parent = new SEOInternalLinkingExpressionClass($id_expression);
						$obj = new SEOInternalLinkingExpressionClass();
						foreach ($obj_parent->getFields() as $key=>$value) {
							if ($key == 'id_expression') continue;
							$obj->$key = $value;
						}
					} else {
						$obj = new SEOInternalLinkingExpressionClass();
					}
					$this->displayFormAddExpression($obj);
					$this->_echoOutput(true);
					break;
				case 'displayFormAddGroup' :
					$this->_cleanOutput();
					$obj = false;
					if (Tools::getValue('alter') && Tools::getValue('id_group')) {
						$id_group = Tools::getValue('id_group');
						$obj = new SEOInternalLinkingGroupClass($id_group);
					} else {
						$obj = new SEOInternalLinkingGroupClass();
					}
					$this->displayFormAddGroup($obj);
					$this->_echoOutput(true);
					break;
				case 'displayExpressionTable':
					$this->_cleanOutput();
					$this->displayExpressionTable();
					$this->_echoOutput(true);
				case 'displayGroupTable':
					$this->_cleanOutput();
					$this->displayGroupTable();
					$this->_echoOutput(true);
				break;
			}
		}
	}
	protected function _postProcess() {		
		if (Tools::getValue('submit_global_options')) {
			$return = '';
			$this->_exclude_headings = (int)Tools::getValue('_exclude_headings', false);
			if (Tools::getValue('_description_field') != 'description' && Tools::getValue('_description_field') != 'description_short' && Tools::getValue('_description_field') != 'both_description') {
				$this->_description_field = 'description';
			} else {
				$this->_description_field = Tools::getValue('_description_field');
			}
			if (!is_numeric(Tools::getValue('_default_datatables_length'))) {
				$this->_default_datatables_length = 10;
			} else {
				$this->_default_datatables_length = (int)Tools::getValue('_default_datatables_length');
			}
			Configuration::updateValue('PM_'.self::$_module_prefix.'_EXCLUDE_HEADINGS', $this->_exclude_headings);
			Configuration::updateValue('PM_'.self::$_module_prefix.'_DESCRIPTION_FIELD', $this->_description_field);
			Configuration::updateValue('PM_'.self::$_module_prefix.'_DEFAULT_DATATABLES_LENGTH', $this->_default_datatables_length);
			$return .= '
			<script type="text/javascript">parent.reloadPanel("displayGroupTable"); parent.reloadPanel("displayExpressionTable"); parent.show_info("' . addcslashes($this->l('Saved'), '"') . '");</script>
			';
			seointernallinkingcoreclass::_cleanBuffer();
			echo $return;
			die();
		} elseif (Tools::getValue('submit_expression')) {
			$return = '';
			$this->postProcessAddExpression();
		} elseif (Tools::getValue('deleteExpression') && Tools::getValue('id_expression')) {
			$return = '';
			$obj = new SEOInternalLinkingExpressionClass(Tools::getValue('id_expression'));
			if ($obj->delete()) {
				$return .= 'reloadPanel("displayExpressionTable"); show_info("' . addcslashes($this->l('Expression has been deleted.'), '"') . '");';
			}
			else
				$return .= 'show_error("' . addcslashes($this->l('Error while deleting the expression'), '"') . '");';
			seointernallinkingcoreclass::_cleanBuffer();
			echo $return;
			die();
		} elseif (Tools::getValue('activeExpression') && Tools::getValue('id_expression')) {
			$return = '';
			$obj = new SEOInternalLinkingExpressionClass(Tools::getValue('id_expression'));
			$obj->active = ($obj->active ? 0 : 1);
			if ($obj->save()) {
				$return .= '$jqPm("#imgActiveExpression' . $obj->id_expression . '").attr("src","'.$this->_path.'img/module_' . ($obj->active ? 'install' : 'disabled') . '.png").attr("alt","' . ($obj->active ? 'Active' : 'Inactive') . '").attr("title","' . ($obj->active ? 'Active' : 'Inactive') . '");';
				$return .= '$jqPm("#spanActiveExpression' . $obj->id_expression . '").parents("td").children("span").html("'. ($obj->active ? '1' : '0') .'");';
				$return .= 'show_info("' . addcslashes($this->l('Saved'), '"') . '");';
			}
			else {
				$return .= 'show_info("' . addcslashes($this->l('Error while updating the expression'), '"') . '");';
			}
			seointernallinkingcoreclass::_cleanBuffer();
			echo $return;
			die();
		} elseif (Tools::getValue('noFollowExpression') && Tools::getValue('id_expression')) {
			$return = '';
			$obj = new SEOInternalLinkingExpressionClass(Tools::getValue('id_expression'));
			$obj->nofollow = ($obj->nofollow ? 0 : 1);
			if ($obj->save()) {
				$return .= '$jqPm("#imgNoFollowExpression' . $obj->id_expression . '").attr("src","'.$this->_path.'img/module_' . ($obj->nofollow ? 'install' : 'disabled') . '.png").attr("alt","' . ($obj->nofollow ? 'Enabled' : 'Disabled') . '").attr("title","' . ($obj->nofollow ? 'Enabled' : 'Disabled') . '");';
				$return .= '$jqPm("#spanNoFollowExpression' . $obj->id_expression . '").parents("td").children("span").html("'. ($obj->nofollow ? '1' : '0') .'");';
				$return .= 'show_info("' . addcslashes($this->l('Saved'), '"') . '");';
			}
			else { 
				$return .= 'show_info("' . addcslashes($this->l('Error while updating the no follow attribute'), '"') . '");';
			}
			seointernallinkingcoreclass::_cleanBuffer();
			echo $return;
			die();
		} elseif (Tools::getValue('newWindowExpression') && Tools::getValue('id_expression')) {
			$return = '';
			$obj = new SEOInternalLinkingExpressionClass(Tools::getValue('id_expression'));
			$obj->new_window = ($obj->new_window ? 0 : 1);
			if ($obj->save()) {
				$return .= '$jqPm("#imgNewWindowExpression' . $obj->id_expression . '").attr("src","'.$this->_path.'img/module_' . ($obj->new_window ? 'install' : 'disabled') . '.png").attr("alt","' . ($obj->new_window ? 'Enabled' : 'Disabled') . '").attr("title","' . ($obj->new_window ? 'Enabled' : 'Disabled') . '");';
				$return .= '$jqPm("#spanNewWindowExpression' . $obj->id_expression . '").parents("td").children("span").html("'. ($obj->new_window ? '1' : '0') .'");';
				$return .= 'show_info("' . addcslashes($this->l('Saved'), '"') . '");';
			}
			else {
				$return .= 'show_info("' . addcslashes($this->l('Error while updating the open in a new window option'), '"') . '");';
			}
			seointernallinkingcoreclass::_cleanBuffer();
			echo $return;
			die();
		} else if (Tools::getValue('submit_group')) {
			$return = '';
			$this->postProcessAddGroup();
		} elseif (Tools::getValue('deleteGroup') && Tools::getValue('id_group')) {
			$return = '';
			$obj = new SEOInternalLinkingGroupClass(Tools::getValue('id_group'));
			if ($obj->delete()) {
				$return .= 'reloadPanel("displayGroupTable");reloadPanel("displayExpressionTable");show_info("' . addcslashes($this->l('Group has been deleted.'), '"') . '");';
			}
			else
				$return .= 'show_error("' . addcslashes($this->l('Error while deleting the group'), '"') . '");';
			seointernallinkingcoreclass::_cleanBuffer();
			echo $return;
			die();
		}
		parent::_postProcess();
	}
	private function postProcessAddExpression() {
		$this->_cleanOutput();
		$obj = new SEOInternalLinkingExpressionClass(Tools::getValue('id_expression', false));
		$this->_html = '';
		$this->_errors = self::_retroValidateController($obj);
		if (! sizeof($this->_errors)) {
			$this->copyFromPost($obj);
			$obj->active = Tools::getValue('active_expression');
			if (! $obj->save())
				$this->_errors[] = $this->l('Error while saving in the database');
			if (! sizeof($this->_errors)) {
				$this->_html .= '<script type="text/javascript">
									parent.parent.show_info("' . addcslashes($this->l('Saved'), '"') . '");
									parent.parent.reloadPanel("displayExpressionTable");
									parent.parent.closeDialogIframe();
								</script>';
			}
		}
		if (sizeof($this->_errors)) {
			$this->_html .= '<script type="text/javascript">
								parent.parent.show_error("' . addcslashes(implode('<br />', $this->_errors), '"') . '");
							</script>';
		}
		$this->_echoOutput(true);
	}
	private function postProcessAddGroup() {
		global $cookie;
		$this->_cleanOutput();
		$obj = new SEOInternalLinkingGroupClass(Tools::getValue('id_group', false));
		$this->_html = '';
		$this->_errors = self::_retroValidateController($obj);
		if (! sizeof($this->_errors)) {
			$this->copyFromPost($obj);
			if (! $obj->save())
				$this->_errors [] = $this->l('Error while saving in the database');
			if (! sizeof($this->_errors)) {
				$this->_html .= '<script type="text/javascript">
									parent.parent.show_info("' . addcslashes($this->l('Saved'), '"') . '");
									parent.parent.reloadPanel("displayGroupTable");
									parent.parent.reloadPanel("displayExpressionTable");
									parent.parent.closeDialogIframe();
								</script>';
			}
		}
		if (sizeof($this->_errors)) {
			$this->_html .= '<script type="text/javascript">
								parent.parent.show_error("' . addcslashes(implode('<br />', $this->_errors), '"') . '");
							</script>';
		}
		$this->_echoOutput(true);
	}
	protected function displayForm() {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '<') && !$this->editorialFileIsRW()) {
			$this->_showWarning($this->l('The file editorial.xml does not exists or is not writable... Please correct the permissions on this file (modules/editorial/editorial.xml)'));
		}
		$this->_html .= '<div id="wrapConfigTab">
              <ul style="height: 30px;" id="configTab">
                <li><a href="#config-1">' . $this->l('General Options') . '</a></li>
                <li><a href="#config-2">' . $this->l('List of Groups') . '</a></li>
                <li><a href="#config-3">' . $this->l('List of Expressions') . '</a></li>
                <li><a href="#config-4">' . $this->l('Optimization') . '</a></li>
                <li><a href="#config-5">' . $this->l('Delete Internal Linking') . '</a></li>
                <li><a href="#config-6">' . $this->l('Crontab') . '</a></li>
            </ul>';
		$this->_html .= '<div id="config-1">';
		$this->displayGlobalOptions();
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-2">';
		$this->_addButton(array('text' => $this->l('Create group'), 'href' => $this->_base_config_url . '&getPanel=displayFormAddGroup', 'onclick' => false, 'icon_class' => 'ui-icon ui-icon-circle-plus', 'class' => 'open_on_dialog_iframe', 'title' => '', 'rel' => '800_500_1'));
		$this->displayGroupTable();
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-3">';
		$this->displayExpressionTable();
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-4">';
		$this->displaySynchronisationInformations();
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-5">';
		$this->displayUndoChanges();
		$this->_html .= '</div>';
		$this->_html .= '<div id="config-6">';
		$this->displayCrontabInformations();
		$this->_html .= '</div>';
		$this->_html .= '</div>
		<script type="text/javascript">$jqPm(document).ready(function() { $jqPm("#wrapConfigTab").tabs(); });</script>';
	}
	function hookHeader($params) {
		return $this->display(__FILE__, $this->name . '_header.tpl');
	}
	function _setGroupConcatMaxLength() {
		return self::Db_Execute('SET SESSION group_concat_max_len='.$this->_group_concat_max_len);
	}
	function getWordsToLink($id_product = NULL, $start = 0, $limit = 0, $count = false, $from_crontab = false) {
		$this->_setGroupConcatMaxLength();
		$sql_group_search = '
		SELECT * FROM
		(SELECT silg.*, 
		GROUP_CONCAT(DISTINCT silpr.id_product SEPARATOR \',\') AS products,
		GROUP_CONCAT(DISTINCT silcr.id_category SEPARATOR \',\') AS categories,
		GROUP_CONCAT(DISTINCT silmr.id_manufacturer SEPARATOR \',\') AS manufacturers,
		GROUP_CONCAT(DISTINCT silsr.id_supplier SEPARATOR \',\') AS suppliers
		FROM `' . _DB_PREFIX_ . 'pm_seointernallinking_group` silg
		LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_manufacturer_rules` silmr ON silmr.id_group=silg.id_group
		LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_supplier_rules` silsr ON silsr.id_group=silg.id_group
		LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_category_rules` silcr ON silcr.id_group=silg.id_group
		LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_product_rules` silpr ON silpr.id_group=silg.id_group
		WHERE silg.group_type=1
		GROUP BY silg.id_group)
		AS `group_search`
		WHERE (products IS NOT NULL OR categories IS NOT NULL OR manufacturers IS NOT NULL OR suppliers IS NOT NULL)
		';
		$result_group_search = self::Db_ExecuteS($sql_group_search);
		$sql_product_search_query = array();
		ini_set('memory_limit', $this->_big_sql_results_memory_limit);
		$result_products_global = array();
		if (self::_isFilledArray($result_group_search)) {
			foreach ($result_group_search as $group) {				
				$and_word = true;
				$sql_product_search = '
				(SELECT DISTINCT '.((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? 'p_shop.id_shop, ' : '').'p.id_product, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position
				FROM `' . _DB_PREFIX_ . 'product` p
				LEFT JOIN `' . _DB_PREFIX_ . 'category_product` cp ON cp.id_product=p.id_product
				LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking` sil ON sil.id_group=\''.$group['id_group'].'\'' .
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_group_shop` silgs ON silgs.id_group = sil.id_group ' : '')
				. self::addSqlAssociation('product', 'p','id_product', true, null, ($from_crontab ? 'all' : false), NULL, true) .
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' AND p_shop.id_shop=silgs.id_shop ' : '') .
				' WHERE sil.active=1';
				if (isset($id_product) && is_numeric($id_product) && $id_product > 0) {
					$sql_product_search .= ($and_word ? ' AND ' : ' ').'p.id_product=\''. (int)$id_product .'\' ';
				}
				if ($group['categories'] != '') {
					if ($group['category_type'] == 0) $sql_product_search .= ($and_word ? ' AND ' : ' ').'cp.id_category IN ('.$group['categories'].')';	
					else $sql_product_search .= ($and_word ? ' AND ' : ' ').'cp.id_category NOT IN ('.$group['categories'].')';								
					if (!$and_word) $and_word = true;
				}
				if ($group['products'] != '') {
					if ($group['product_type'] == 0) $sql_product_search .= ($and_word ? ' AND ' : ' ').'p.id_product IN ('.$group['products'].')'; 		
					else $sql_product_search .= ($and_word ? ' AND ' : ' ').'p.id_product NOT IN ('.$group['products'].')'; 								
					if (!$and_word) $and_word = true;
				}
				if ($group['manufacturers'] != '') {
					if ($group['manufacturer_type'] == 0) $sql_product_search .= ($and_word ? ' AND ' : ' ').'p.id_manufacturer IN ('.$group['manufacturers'].')'; 	
					else $sql_product_search .= ($and_word ? ' AND ' : ' ').'p.id_manufacturer NOT IN ('.$group['manufacturers'].')'; 								
					if (!$and_word) $and_word = true;
				}
				if ($group['suppliers'] != '') {
					if ($group['supplier_type'] == 0) $sql_product_search .= ($and_word ? ' AND ' : ' ').' p.id_supplier IN ('.$group['suppliers'].')'; 			
					else $sql_product_search .= ($and_word ? ' AND ' : ' ').'p.id_supplier NOT IN ('.$group['suppliers'].')'; 										
					if (!$and_word) $and_word = true;
				}
				$sql_product_search .= ')';
				$sql_product_search_query[] = $sql_product_search;
			}
			if (self::_isFilledArray($sql_product_search_query)) {
				$sql_product_search_global = implode($sql_product_search_query, ' UNION');
				if ($count) {
					$sql_product_search_global_count = 'SELECT COUNT(*) as nb FROM ('.$sql_product_search_global.') AS table_count';
					$count_total = self::Db_getRow($sql_product_search_global_count);
					return (int)($count_total['nb']);
				}
				if ($limit != 0) {
					$sql_product_search_global = 'SELECT * FROM (SELECT * FROM ('.$sql_product_search_global.') AS table_result LIMIT '.$start.','.$limit.') as table_result2 ORDER BY RAND()';
				}
				$result_products_global = self::Db_ExecuteS($sql_product_search_global);
			}
		}
		if ($count) return 0;
		if (is_array($result_products_global)) return $result_products_global;
		return false;
	}
	function getCMSWordsToLink($start = 0, $limit = 0, $count = false, $from_crontab = false) {
		$this->_setGroupConcatMaxLength();
		$sql_group_search = '
		SELECT * FROM
		(SELECT silg.*, 
		GROUP_CONCAT(DISTINCT silcr.id_cms SEPARATOR \',\') AS cms_pages
		FROM `' . _DB_PREFIX_ . 'pm_seointernallinking_group` silg
		LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_cms_rules` silcr ON silcr.id_group=silg.id_group
		WHERE silg.group_type=2
		GROUP BY silg.id_group)
		AS `group_search`
		WHERE cms_pages IS NOT NULL
		';
		$result_group_search = self::Db_ExecuteS($sql_group_search);
		$sql_cms_search_query = array();
		ini_set('memory_limit', $this->_big_sql_results_memory_limit);
		$result_cms_global = array();
		if (self::_isFilledArray($result_group_search)) {
			foreach ($result_group_search as $group) {
				$and_word = true;
				$sql_cms_search = '
				(SELECT DISTINCT '.((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? 'c_shop.id_shop, ' : '').'c.id_cms, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position
				FROM `' . _DB_PREFIX_ . 'cms` c
				LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking` sil ON sil.id_group=\''.$group['id_group'].'\'' .
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_group_shop` silgs ON silgs.id_group = sil.id_group ' : '')
				. self::addSqlAssociation('cms', 'c','id_cms', true, null, ($from_crontab ? 'all' : false), NULL, true) .
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' AND c_shop.id_shop=silgs.id_shop ' : '') .
				'WHERE sil.active=1';
				if ($group['cms_type'] == 0) $sql_cms_search .= ' AND c.id_cms IN ('.$group['cms_pages'].')';			
				else $sql_cms_search .= ' AND c.id_cms NOT IN ('.$group['cms_pages'].')';								
				$sql_cms_search .= ')';
				$sql_cms_search_query[] = $sql_cms_search;
			}
			if (self::_isFilledArray($sql_cms_search_query)) {
				$sql_cms_search_global = implode($sql_cms_search_query, ' UNION');
				if ($count) {
					$sql_cms_search_global_count = 'SELECT COUNT(*) as nb FROM ('.$sql_cms_search_global.') AS table_count';
					$count_total = self::Db_getRow($sql_cms_search_global_count);
					return (int)($count_total['nb']);
				}
				if ($limit != 0) {
					$sql_cms_search_global = 'SELECT * FROM ('.$sql_cms_search_global.') AS table_result LIMIT '.$start.','.$limit;
				}
				$result_cms_global = self::Db_ExecuteS($sql_cms_search_global);
			}
		}
		if ($count) return 0;
		if (self::_isFilledArray($result_cms_global)) return $result_cms_global;
		return false;
	}
	function getCategoriesWordsToLink($id_category = NULL, $start = 0, $limit = 0, $count = false, $from_crontab = false) {
		$this->_setGroupConcatMaxLength();
		$sql_group_search = '
		SELECT * FROM
		(SELECT silg.*,
		GROUP_CONCAT(DISTINCT silcr.id_category SEPARATOR \',\') AS categories
		FROM `' . _DB_PREFIX_ . 'pm_seointernallinking_group` silg
		LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_category_rules` silcr ON silcr.id_group=silg.id_group
		WHERE silg.group_type=4
		GROUP BY silg.id_group)
		AS `group_search`
		WHERE categories IS NOT NULL
		';
		$result_group_search = self::Db_ExecuteS($sql_group_search);
		$sql_categories_search_query = array();
		ini_set('memory_limit', $this->_big_sql_results_memory_limit);
		$result_categories_global = array();
		if (self::_isFilledArray($result_group_search)) {
			foreach ($result_group_search as $group) {
				$and_word = true;
				$sql_categories_search = '
				(SELECT DISTINCT '.((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? 'c_shop.id_shop, ' : '').'c.id_category, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position
				FROM `' . _DB_PREFIX_ . 'category` c
				LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking` sil ON sil.id_group=\''.$group['id_group'].'\' ' .
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_group_shop` silgs ON silgs.id_group = sil.id_group ' : '')
				. self::addSqlAssociation('category', 'c','id_category', true, null, ($from_crontab ? 'all' : false), NULL, true) . 
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' AND c_shop.id_shop=silgs.id_shop ' : '') . '
				WHERE sil.active=1';
				if (isset($id_category) && is_numeric($id_category) && $id_category > 0) {
					$sql_categories_search .= ' AND c.id_category=\''. (int)$id_category .'\' ';
				}
				if ($group['category_type'] == 0) $sql_categories_search .= ' AND c.id_category IN ('.$group['categories'].')';				
				else $sql_categories_search .= ' AND c.id_category NOT IN ('.$group['category_type'].')';									
				$sql_categories_search .= ')';
				$sql_categories_search_query[] = $sql_categories_search;
			}
			if (self::_isFilledArray($sql_categories_search_query)) {
				$sql_categories_search_global = implode($sql_categories_search_query, ' UNION');
				if ($count) {
					$sql_categories_search_global_count = str_replace(
							'c.id_category, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position',
							'c.id_category, sil.id_lang, sil.expression_content',
							$sql_categories_search_global
					);
					$sql_categories_search_global_count = 'SELECT COUNT(*) as nb FROM ('.$sql_categories_search_global_count.') AS table_count';
					$count_total = self::Db_getRow($sql_categories_search_global_count);
					return (int)($count_total['nb']);
				}
				if ($limit != 0) {
					$sql_categories_search_global = 'SELECT * FROM ('.$sql_categories_search_global.') AS table_result GROUP BY id_category,id_lang,expression_content LIMIT '.$start.','.$limit;
				}
				$result_categories_global = self::Db_ExecuteS($sql_categories_search_global);
			}
		}
		if ($count) return 0;
		if (self::_isFilledArray($result_categories_global)) return $result_categories_global;
		return false;
	}
	function getManufacturersWordsToLink($id_manufacturer = NULL, $start = 0, $limit = 0, $count = false, $from_crontab = false) {
		$this->_setGroupConcatMaxLength();
		$sql_group_search = '
		SELECT * FROM
		(SELECT silg.*,
		GROUP_CONCAT(DISTINCT silmr.id_manufacturer SEPARATOR \',\') AS manufacturers
		FROM `' . _DB_PREFIX_ . 'pm_seointernallinking_group` silg
		LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_manufacturer_rules` silmr ON silmr.id_group=silg.id_group
		WHERE silg.group_type=5
		GROUP BY silg.id_group)
		AS `group_search`
		WHERE manufacturers IS NOT NULL
		';
		$result_group_search = self::Db_ExecuteS($sql_group_search);
		$sql_manufacturers_search_query = array();
		ini_set('memory_limit', $this->_big_sql_results_memory_limit);
		$result_manufacturers_global = array();
		if (self::_isFilledArray($result_group_search)) {
			foreach ($result_group_search as $group) {
				$and_word = true;
				$sql_manufacturers_search = '
				(SELECT DISTINCT '.((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? 'm_shop.id_shop, ' : '').'m.id_manufacturer, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position
				FROM `' . _DB_PREFIX_ . 'manufacturer` m
				LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking` sil ON sil.id_group=\''.$group['id_group'].'\' ' .
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_group_shop` silgs ON silgs.id_group = sil.id_group ' : '')
				. self::addSqlAssociation('manufacturer', 'm','id_manufacturer', true, null, ($from_crontab ? 'all' : false), NULL, true) . 
				((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? ' AND m_shop.id_shop=silgs.id_shop ' : '') . '
				WHERE sil.active=1';
				if (isset($id_manufacturer) && is_numeric($id_manufacturer) && $id_manufacturer > 0) {
					$sql_manufacturers_search .= ' AND m.id_manufacturer=\''. (int)$id_manufacturer .'\' ';
				}
				if ($group['manufacturer_type'] == 0) $sql_manufacturers_search .= ' AND m.id_manufacturer IN ('.$group['manufacturers'].')';		
				else $sql_manufacturers_search .= ' AND m.id_manufacturer NOT IN ('.$group['manufacturer_type'].')';								
				$sql_manufacturers_search .= ')';
				$sql_manufacturers_search_query[] = $sql_manufacturers_search;
			}
			if (self::_isFilledArray($sql_manufacturers_search_query)) {
				$sql_manufacturers_search_global = implode($sql_manufacturers_search_query, ' UNION');
				if ($count) {
					$sql_manufacturers_search_global_count = str_replace(
							'm.id_manufacturer, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position',
							'm.id_manufacturer, sil.id_lang, sil.expression_content',
							$sql_manufacturers_search_global
					);
					$sql_manufacturers_search_global_count = 'SELECT COUNT(*) as nb FROM ('.$sql_manufacturers_search_global_count.') AS table_count';
					$count_total = self::Db_getRow($sql_manufacturers_search_global_count);
					return (int)($count_total['nb']);
				}
				if ($limit != 0) {
					$sql_manufacturers_search_global = 'SELECT * FROM ('.$sql_manufacturers_search_global.') AS table_result GROUP BY id_manufacturer,id_lang,expression_content LIMIT '.$start.','.$limit;
				}
				$result_manufacturers_global = self::Db_ExecuteS($sql_manufacturers_search_global);
			}
		}
		if ($count) return 0;
		if (self::_isFilledArray($result_manufacturers_global)) return $result_manufacturers_global;
		return false;
	}
	function getEditorialWordsToLink($from_crontab = false) {
		ini_set('memory_limit', $this->_big_sql_results_memory_limit);
		if (version_compare(_PS_VERSION_, '1.5.0.0', '<')) {
			$sql_editorial_search_query = '
				SELECT 1 AS id_editorial, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position
				FROM `' . _DB_PREFIX_ . 'pm_seointernallinking_group` silg
				LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking` sil ON sil.id_group=silg.id_group 
				WHERE silg.group_type=3
				AND sil.active=1
			';
		} else {
			$sql_editorial_search_query = '
				SELECT DISTINCT '.((version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) ? 'e.id_shop, ' : '').'e.id_editorial, sil.id_lang, sil.expression_content, sil.associated_url, sil.url_title, sil.nofollow, sil.new_window, sil.link_position
				FROM `' . _DB_PREFIX_ . 'editorial` e
				LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking_group` silg ON 1=1
				LEFT JOIN `' . _DB_PREFIX_ . 'pm_seointernallinking` sil ON sil.id_group=silg.id_group 
				'. self::addSqlAssociation('editorial', 'e','id_editorial', true, null, ($from_crontab ? 'all' : false), 'editorial', true) . ' 
				WHERE silg.group_type=3
				AND sil.active=1
			';
		}
		$result_editorial_global = self::Db_ExecuteS($sql_editorial_search_query);
		if (self::_isFilledArray($result_editorial_global)) return $result_editorial_global;
		return array();
	}
	function deleteAllLinks($description) {
		$description = preg_replace('#<a(?:.[^>]*)?rel="pmsil nofollow"(?:.[^<]*)?>(.[^<][^/]*)</a>#', '$1', $description);
		$description = preg_replace('#<a(?:.[^>]*)?rel="pmsil"(?:.[^<]*)?>(.[^<][^/]*)</a>#', '$1', $description);
		$description = preg_replace('#<a(?:.[^>]*)?class="pmsil"(?:.[^<]*)?>(.[^<][^/]*)</a>#', '$1', $description);
		return $description;
	}
	function updateLinksAlternative($subject, $pattern, $replacement, $word, $link_position, $usingEntities = false) {
		$html_table = get_html_translation_table(HTML_ENTITIES, ENT_COMPAT);
		foreach ($html_table as $char=>$entities) $subject = str_replace($entities, $char, $subject);
		unset($html_table[chr(60)]); unset($html_table[chr(62)]); unset($html_table[chr(34)]); unset($html_table[chr(38)]);
		foreach ($html_table as $char=>$entities) {
			$entities = str_replace('&', '1SILENTITIES1', $entities);
			$entities = str_replace(';', '2SILENTITIES2', $entities);
			$subject = str_replace($char, $entities, $subject);
			if ($usingEntities) {
				$word = str_replace('&', '1SILENTITIES1', $word);
				$word = str_replace(';', '2SILENTITIES2', $word);
			}
			$word = str_replace($char, $entities, $word);
		}
		$pattern = '/(?!(?:[^<\[]+[>\]]|[^>\]]+<\/a>))\b('.preg_quote($word).')\b/imu';
		$resultats = preg_split($pattern, $subject, -1, PREG_SPLIT_OFFSET_CAPTURE);
		$chaines_ok = array();
		foreach ($resultats as $resultat) {
			$offset = $resultat[1];
			$substr_start = $offset + strlen($resultat[0]);
			$substr_end = strlen($word) + 1;
			$chaine_a_traiter = substr($subject, $substr_start, $substr_end);
			if ($chaine_a_traiter != false) {
				$chaine_traitee = preg_replace($pattern, $replacement, $chaine_a_traiter, 1, $count);
				if ($count > 0) {
					$chaine_finale = substr($subject, 0, $substr_start);
					$chaine_finale .= $chaine_traitee;
					$chaine_finale .= substr($subject, $substr_start + $substr_end);
					foreach ($html_table as $char=>$entities) {
						$entities = str_replace('&', '1SILENTITIES1', $entities);
						$entities = str_replace(';', '2SILENTITIES2', $entities);
						$chaine_finale = str_replace($entities, $char, $chaine_finale);
					}
					$chaines_ok[] = $chaine_finale;
				}
			}
		}
		if (self::_isFilledArray($chaines_ok)) {
			$chaines_ok_count = count($chaines_ok);
			switch ($link_position) {
				case 1:
				default:
					$index = 0;
					break;
				case 2:
					$index = ceil($chaines_ok_count/2) - 1;
					break;
				case 3:
					$index = $chaines_ok_count - 1;
					break;
			}
			return $chaines_ok[$index];
		}
		return false;
	}
	function updateLinks($subject, $pattern, $replacement, $word, $link_position, $usingEntities = false) {
		$resultats = preg_split($pattern, $subject, -1, PREG_SPLIT_OFFSET_CAPTURE);
		$chaines_ok = array();
		foreach ($resultats as $resultat) {
			$offset = $resultat[1];
			$substr_start = $offset + strlen($resultat[0]);
			$substr_end = strlen($word) + 1;
			$chaine_a_traiter = substr($subject, $substr_start, $substr_end);
			if ($chaine_a_traiter != false) {
				$chaine_traitee = preg_replace($pattern, $replacement, $chaine_a_traiter, 1, $count);
				if ($count > 0) {
					$chaine_finale = substr($subject, 0, $substr_start);
					$chaine_finale .= $chaine_traitee;
					$chaine_finale .= substr($subject, $substr_start + $substr_end);
					$chaines_ok[] = $chaine_finale;
				}
			}
		}
		if (self::_isFilledArray($chaines_ok)) {
			$chaines_ok_count = count($chaines_ok);
			switch ($link_position) {
				case 1:
				default:
					$index = 0;
					break;
				case 2:
					$index = ceil($chaines_ok_count/2) - 1;
					break;
				case 3:
					$index = $chaines_ok_count - 1;
					break;
			}
			return $chaines_ok[$index];
		}
		return $this->updateLinksAlternative($subject, $pattern, $replacement, $word, $link_position, $usingEntities);
	}
	function updateProductDescription($product) {
		$this->cleanAllHTMLField('products', false, $product->id);
		$word_combinaisons = $this->getWordsToLink($product->id);
		if (self::_isFilledArray($word_combinaisons)) {
			$result = $this->updateHTMLField('products', $word_combinaisons, true);
		}
		return true;
	}
	function updateCategoryDescription($category) {
		$this->cleanAllHTMLField('categories', false, $category->id);
		$word_combinaisons = $this->getCategoriesWordsToLink($category->id);
		if (self::_isFilledArray($word_combinaisons)) {
			$result = $this->updateHTMLField('categories', $word_combinaisons, true);
		}
		return true;
	}
	function updateManufacturerDescription($manufacturer) {
		$this->cleanAllHTMLField('manufacturers', false, $manufacturer->id);
		$word_combinaisons = $this->getManufacturersWordsToLink($manufacturer->id);
		if (self::_isFilledArray($word_combinaisons)) {
			$result = $this->updateHTMLField('manufacturers', $word_combinaisons, true);
		}
		return true;
	}
	function updateHTMLField($field_type = 'products', $word_combinaisons, $clean_first = false, $from_crontab = false) {
		if (!self::_isFilledArray($word_combinaisons)) return;
		switch ($field_type) {
			case 'editorial':
				$sql_association_table = 'editorial';
				$table = '`'._DB_PREFIX_.'editorial_lang`';
				$table_alias = 'el';
				$identifier_shop = 'el_shop.`id_shop`';
				$identifier_field = 'id_editorial';
				$descriptions_field = array('body_paragraph');
				$join_table_name = 'editorial';
				break;
			case 'cms':
				$sql_association_table = 'cms';
				$table = '`'._DB_PREFIX_.'cms_lang`';
				$table_alias = 'cmsl';
				$identifier_shop = 'cmsl_shop.`id_shop`';
				$identifier_field = 'id_cms';
				$descriptions_field = array('content');
				$join_table_name = NULL;
				break;
			case 'categories':
				$sql_association_table = 'category';
				$table = '`'._DB_PREFIX_.'category_lang`';
				$table_alias = 'catl';
				$identifier_shop = 'catl.`id_shop`';
				$identifier_field = 'id_category';
				$descriptions_field = array('description');
				$join_table_name = NULL;
				break;
			case 'manufacturers':
				$sql_association_table = 'manufacturer';
				$table = '`'._DB_PREFIX_.'manufacturer_lang`';
				$table_alias = 'manl';
				$identifier_shop = '';
				$identifier_field = 'id_manufacturer';
				$descriptions_field = array('description');
				$join_table_name = NULL;
				break;
			case 'products':
			default:
				$sql_association_table = 'product';
				$table = '`'._DB_PREFIX_.'product_lang`';
				$table_alias = 'pl';
				$identifier_shop = 'pl.`id_shop`';
				$identifier_field = 'id_product';
				if ($this->_description_field == 'both_description') {
					$descriptions_field = array('description', 'description_short');
				} else {
					$descriptions_field = array($this->_description_field);
				}
				$join_table_name = NULL;
				break;
		}
		$exclude_headings = (bool)$this->_exclude_headings;
		if (session_id() != '') {
			if (!isset($_SESSION['ajax_to_clean'])) $_SESSION['ajax_to_clean'] = array();
			$to_clean = $_SESSION['ajax_to_clean'];
		} else {
			$to_clean = array();
		}
		$columns_description = array();
		foreach ($descriptions_field as $description_field) $columns_description[] = '`'.$description_field.'`';
		$columnsContent = array();
		$hashReplaceQueryList = array();
		foreach ($word_combinaisons as $word_array) {
			$id_value = (int)$word_array[$identifier_field];
			$id_lang = (int)$word_array['id_lang'];
			if (isset($word_array['id_shop'])) $id_shop = (int)$word_array['id_shop'];
			else $id_shop = false;
			$word = $word_array['expression_content'];
			$url = $word_array['associated_url'];
			$url_title_sha1 = sha1('PMSIL-URL-' . $word_array['url_title']);
			$url_title = htmlentities($word_array['url_title'], ENT_COMPAT, 'UTF-8');
			$nofollow = (bool)($word_array['nofollow']);
			$new_window = (bool)($word_array['new_window']);
			$link_position = (int)$word_array['link_position'];
			$columns_condition = array();
			foreach ($descriptions_field as $description_field)
				$columns_condition[] = '(`'.$description_field.'` LIKE "%'.pSQL($word).'%" OR `'.$description_field.'` LIKE "%'.pSQL(htmlentities($word, ENT_COMPAT, 'UTF-8'), true).'%")';
			$sql = 'SELECT DISTINCT '. ($id_shop != false && !empty($identifier_shop) ? $identifier_shop.', ' : '') . $table_alias.'.`'.$identifier_field.'`, `id_lang`, '.implode($columns_description, ', ').'
			FROM ' . $table  . ' ' . $table_alias
			. (isset($word_array['id_shop']) ? self::addSqlAssociation($sql_association_table, $table_alias,$identifier_field, true, null, array($id_shop), $join_table_name, true) : '') .
			' WHERE `id_lang`="'.$id_lang.'"
			AND '.$table_alias.'.`'.$identifier_field.'`="'.$id_value.'" '
			. (!empty($identifier_shop) && $sql_association_table == 'product' && isset($word_array['id_shop']) ? ' AND '.$table_alias.'_shop.`id_shop`='.$identifier_shop : '')
			. (!empty($identifier_shop) && $sql_association_table == 'category' && isset($word_array['id_shop']) ? ' AND '.$table_alias.'_shop.`id_shop`='.$identifier_shop : '') .
			'AND ('.implode($columns_condition, ' OR ').')'
			;
			$result = self::Db_ExecuteS($sql);
			if ($result && is_array($result)) {
				foreach ($result as $key=>$row) {
					$rowIdentifier = $field_type . '-' . (isset($word_array['id_shop']) ? $word_array['id_shop'] : 0) . '-' . $row['id_lang'] . '-' . $row[$identifier_field];
					if (!isset($to_clean[$rowIdentifier]) && $clean_first == true) {
						$to_clean[$rowIdentifier] = true;
					}
					foreach ($descriptions_field as $description_field) {
						if (!isset($columnsContent[$rowIdentifier][$description_field])) {
							$columnsContent[$rowIdentifier][$description_field] = $row[$description_field];
						}
					}
					if (isset($to_clean[$rowIdentifier]) && $to_clean[$rowIdentifier] == true) {
						foreach ($descriptions_field as $description_field) {
							$columnsContent[$rowIdentifier][$description_field] = $this->deleteAllLinks($columnsContent[$rowIdentifier][$description_field]);
						}
					}
					if ((bool)$this->_exclude_headings == true) {
						foreach ($descriptions_field as $description_field) {
							$columnsContent[$rowIdentifier][$description_field] = stripslashes(preg_replace_callback($this->_preg_pattern_headings, 'self::headingsDoNotTouchStart', $columnsContent[$rowIdentifier][$description_field]));
						}
					}
					foreach ($descriptions_field as $description_field) {
						$update_links_result = $this->updateLinks(
							$columnsContent[$rowIdentifier][$description_field],
							'/(?!(?:[^<\[]+[>\]]|[^>\]]+<\/a>))\b('.preg_quote($word).')\b/imu',
							'<a href="'.$url.'" class="pmsil"'. (($nofollow) ? ' rel="nofollow"' : '') . (($url_title_sha1 != '') ? ' title="'.$url_title_sha1.'"' : '') . (($new_window) ? ' target="_blank"' : '') .'>$1</a>',
							$word,
							$link_position
						);
						if (!$update_links_result) {
							if (substr(htmlentities($word, ENT_COMPAT, 'UTF-8'), -1) == ';') {
								$update_links_result = $this->updateLinks(
									$columnsContent[$rowIdentifier][$description_field],
									'/(?!(?:[^<\[]+[>\]]|[^>\]]+<\/a>))(\b'.preg_quote(rtrim(htmlentities($word, ENT_COMPAT, 'UTF-8'), ';')).'\b;)/imu',
									'<a href="'.$url.'" class="pmsil"'. (($nofollow) ? ' rel="nofollow"' : '') . (($url_title_sha1 != '') ? ' title="'.$url_title_sha1.'"' : '') . (($new_window) ? ' target="_blank"' : '') .'>$1</a>',
									htmlentities($word, ENT_COMPAT, 'UTF-8'),
									$link_position,
									true
								);
							} else {
								$update_links_result = $this->updateLinks(
									$columnsContent[$rowIdentifier][$description_field],
									'/(?!(?:[^<\[]+[>\]]|[^>\]]+<\/a>))\b('.preg_quote(htmlentities($word, ENT_COMPAT, 'UTF-8')).')\b/imu',
									'<a href="'.$url.'" class="pmsil"'. (($nofollow) ? ' rel="nofollow"' : '') . (($url_title_sha1 != '') ? ' title="'.$url_title_sha1.'"' : '') . (($new_window) ? ' target="_blank"' : '') .'>$1</a>',
									htmlentities($word, ENT_COMPAT, 'UTF-8'),
									$link_position,
									true
								);
							}
						}
						if ($update_links_result != false) {
							$columnsContent[$rowIdentifier][$description_field] = $update_links_result;
						}
					}
					if ((bool)$this->_exclude_headings == true) {
						foreach ($descriptions_field as $description_field) {
							$columnsContent[$rowIdentifier][$description_field] = stripslashes(preg_replace_callback($this->_preg_pattern_headings, 'self::headingsDoNotTouchEnd', $columnsContent[$rowIdentifier][$description_field]));
						}
					}
					foreach ($descriptions_field as $description_field) {
						if (empty($columnsContent[$rowIdentifier][$description_field])) {
							continue;
						}
						$hashReplaceQueryList[] = 'UPDATE '.$table.' SET '.$description_field.'=REPLACE('.$description_field.', "'. pSQL($url_title_sha1) .'", "'. pSQL($url_title) .'") WHERE '.$identifier_field.'="'.pSQL($row[$identifier_field]).'" AND id_lang="'.pSQL($row['id_lang']).'"'.(isset($row['id_shop']) && $field_type != 'editorial' && $field_type != 'cms' ? ' AND id_shop="'.pSQL($row['id_shop']).'"' : '');
						$sql_update = 'UPDATE '.$table.' SET '.$description_field.'="'.pSQL($columnsContent[$rowIdentifier][$description_field], true).'" WHERE '.$identifier_field.'="'.pSQL($row[$identifier_field]).'" AND id_lang="'.pSQL($row['id_lang']).'"'.(isset($row['id_shop']) && $field_type != 'editorial' && $field_type != 'cms' ? ' AND id_shop="'.pSQL($row['id_shop']).'"' : '');
						$result_update = self::Db_Execute($sql_update);
					}
					$to_clean[$rowIdentifier] = false;
				}
			}
		}
		$_SESSION['ajax_to_clean'] = $to_clean;
		if (sizeof($hashReplaceQueryList)) {
			$hashReplaceQueryList = array_unique($hashReplaceQueryList);
			foreach ($hashReplaceQueryList as $hashReplaceQuery) {
				self::Db_Execute($hashReplaceQuery);
			}
		}
	}
	function cleanAllHTMLField($field_type = 'products', $from_crontab = false, $id_identifier = NULL) {
		switch ($field_type) {
			case 'editorial':
				$sql_association_table = 'editorial_lang';
				$table = '`'._DB_PREFIX_.'editorial_lang`';
				$table_alias = 'el';
				$descriptions_field = array('body_paragraph');
				$identifier_shop = '';
				$identifier_field = 'id_editorial';
				$shop_condition = '';
				$shop_inclusion = self::addSqlAssociation($sql_association_table, $table_alias,$identifier_field, true, null, ($from_crontab ? 'all' : false), 'editorial');
				if (version_compare(_PS_VERSION_, '1.4.0.0', '>=') && !Module::isInstalled('editorial')) return;
				break;
			case 'cms':
				$sql_association_table = 'cms';
				$table = '`'._DB_PREFIX_.'cms_lang`';
				$table_alias = 'cmsl';
				$descriptions_field = array('content');
				$identifier_shop = '';
				$identifier_field = 'id_cms';
				$shop_condition = '';
				$shop_inclusion = self::addSqlAssociation($sql_association_table, $table_alias,$identifier_field, true, null, ($from_crontab ? 'all' : false));
				break;
			case 'categories':
				$sql_association_table = 'category';
				$table = '`'._DB_PREFIX_.'category_lang`';
				$table_alias = 'catl';
				$descriptions_field = array('description');
				$identifier_shop = 'catl.`id_shop`';
				$identifier_field = 'id_category';
				if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
					$shop_condition = self::addShopCondition($identifier_shop, $from_crontab);
					if (!empty($shop_condition)) $shop_condition = ' AND '. $shop_condition;
				} else {
					$shop_condition = '';
				}
				$shop_inclusion = '';
				break;
			case 'manufacturers':
				$sql_association_table = 'manufacturer';
				$table = '`'._DB_PREFIX_.'manufacturer_lang`';
				$table_alias = 'manl';
				$descriptions_field = array('description');
				$identifier_shop = '';
				$identifier_field = 'id_manufacturer';
				$shop_condition = '';
				$shop_inclusion = self::addSqlAssociation($sql_association_table, $table_alias,$identifier_field, true, null, ($from_crontab ? 'all' : false));
				break;
			case 'products':
			default:
				$sql_association_table = 'product';
				$table = '`'._DB_PREFIX_.'product_lang`';
				$table_alias = 'pl';
				$descriptions_field = array('description', 'description_short');
				$identifier_shop = 'pl.`id_shop`';
				$identifier_field = 'id_product';
				if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
					$shop_condition = self::addShopCondition($identifier_shop, $from_crontab);
					if (!empty($shop_condition)) $shop_condition = ' AND '. $shop_condition;
				} else {
					$shop_condition = '';
				}
				$shop_inclusion = '';
				break;
		}
		$exclude_headings = (bool)$this->_exclude_headings;
		$to_clean = array();
		$columns_description = array();
		$columns_condition = array();
		foreach ($descriptions_field as $description_field) {
			$columns_description[] = '`'.$description_field.'`';
			$columns_condition[] = '`'.$description_field.'` LIKE "%pmsil%"';
		}
		ini_set('memory_limit', $this->_big_sql_results_memory_limit);
		$sql = '
			SELECT DISTINCT '.(self::isMultishop() && !empty($identifier_shop) ? $identifier_shop.', ' : '').$table_alias.'.`'.$identifier_field.'`, '.$table_alias.'.`id_lang`, '.implode($columns_description, ', ').'
			FROM '.$table.' '.$table_alias.' '
			. $shop_inclusion . '
			WHERE ('.implode($columns_condition, ' OR ') . ') ' . $shop_condition;
		if ($field_type == 'products' && $id_identifier != NULL && is_numeric($id_identifier)) 
			$sql .= ' AND '.$table_alias.'.`id_product`='.(int)$id_identifier;
		else if ($field_type == 'categories' && $id_identifier != NULL && is_numeric($id_identifier))
			$sql .= ' AND '.$table_alias.'.`id_category`='.(int)$id_identifier;
		else if ($field_type == 'manufacturers' && $id_identifier != NULL && is_numeric($id_identifier))
			$sql .= ' AND '.$table_alias.'.`id_manufacturer`='.(int)$id_identifier;
		$result = self::Db_ExecuteS($sql);
		if ($result && is_array($result)) {
			foreach ($result as $key=>$row) {
				foreach ($descriptions_field as $description_field) {
					$row[$description_field] = $this->deleteAllLinks($row[$description_field]);		
					$sql_update = 'UPDATE '.$table.' SET '.$description_field.'="'.pSQL($row[$description_field], true).'" WHERE '.$identifier_field.'="'.pSQL($row[$identifier_field]).'" AND id_lang="'.pSQL($row['id_lang']).'"';
					if (isset($row['id_shop'])) $sql_update .= ' AND id_shop="'.(int)$row['id_shop'].'"';
					$result_update = self::Db_Execute($sql_update);
				}
			}
		}
	}
	public function hookCategoryUpdate($params) {
		$category = $params['category'];
		if ($category) $this->updateCategoryDescription($category);
		return false;
	}
	public function hookUpdateProduct($params) {
		$product = $params['product'];
		if ($product) return $this->updateProductDescription($product);
		return false;
	}
	public function hookActionProductSave($params) {
		$product = new Product($params['id_product']);
		if ($product) return $this->updateProductDescription($product);
		return false;
	}
	public function hookActionObjectManufacturerAddAfter($params) {
		$manufacturer = $params['object'];
		if ($manufacturer) return $this->updateManufacturerDescription($manufacturer);
		return false;
	}
	public function hookActionObjectManufacturerUpdateAfter($params) {
		return $this->hookActionObjectManufacturerAddAfter($params);
	}
}
