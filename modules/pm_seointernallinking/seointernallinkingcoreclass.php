<?php
/**
 *
 * PM_CoreFunctions
 *
 * @author    Presta-Module.com <support@presta-module.com>
 * @copyright Presta-Module 2014
 * @version   1.0.0
 *
 *************************************
 **   Core Functions For PM Addons   *
 **   http://www.presta-module.com   *
 **             V 1.0.0              *
 *************************************
 * + Description: Core Functions For PM Addons
 * + Languages: EN, FR
 * + PS version: 1.1 to 1.6
 * ***********************************
 **/
class seointernallinkingcoreclass extends Module {
	// Begin AttributesDeclaration
	protected $_html;
	protected $_html_at_end;
	protected $_base_config_url;
	protected $_default_language;
	protected $_fields_options;
	protected $_iso_lang;
	protected $_languages;
	protected $_css_files;
	protected $_js_files;
	protected $_smarty;
	protected $_cookie;
	protected $_employee;
	protected $_coreClassName;
	protected $_registerOnHooks;
	public static $_module_prefix = 'SIL';
	protected $_debug_mode = false;
	protected $_copy_and_partner = false;
	protected $_support_link = false;
	protected $_css_js_lib_loaded = array ();
	protected $_temp_upload_dir = '/uploads/temp/';
	protected $styles_flag_lang_init;
	protected static $_new_hook_name = array(
		'home'			=> 'displayHome',
		'leftColumn'	=> 'displayLeftColumn',
		'rightColumn'	=> 'displayRightColumn',
		'top'			=> 'displayTop'
	);
	function __construct() {
		$this->_coreClassName = strtolower(get_class());
		parent::__construct();
		$this->_initClassVar();
	}
	public function install(){
		if (parent::install() == false OR $this->_registerHooks() == false)
		  return false;
		return true;
    }
	public function checkIfModuleIsUpdate($updateDb = false, $displayConfirm = true) {
		return true;
	}
	protected function _registerHooks() {
		if(!isset($this->_registerOnHooks) || !self::_isFilledArray($this->_registerOnHooks)) return true;
		foreach($this->_registerOnHooks as $hook) {
			if(!$this->registerHook($hook)) return false;
		}
		return true;
	}
	public static function jsonEncode($data) {
		if (function_exists('json_encode')) return json_encode($data);
		else {
			include_once(_PS_TOOL_DIR_.'json/json.php');
			$pearJson = new Services_JSON();
			return $pearJson->encode($data);
		}
	}
	public static function getHttpHost($http = false, $entities = false) {
		$host = (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST']);
		if ($entities) $host = htmlspecialchars($host, ENT_COMPAT, 'UTF-8');
		if ($http) $host = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').$host;
		return $host;
	}
	public static function isModuleInstalled($moduleName) {
		if (version_compare(_PS_VERSION_, '1.2.0.0', '>=')) return Module::isInstalled($moduleName);
		self::Db_Execute('SELECT `id_module` FROM `'._DB_PREFIX_.'module` WHERE `name` = \''.pSQL($moduleName).'\'');
		return (bool)self::Db_NumRows();
	}
	public static function Db_autoExecute($table, $values, $type, $where = false, $limit = false, $use_cache = 1) {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) return Db::getInstance(_PS_USE_SQL_SLAVE_)->autoExecute($table, $values, $type, $where, $limit, $use_cache);
		else return Db::getInstance()->autoExecute($table, $values, $type, $where, $limit);
	}
	public static function Db_ExecuteS($q) {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($q);
		else return Db::getInstance()->ExecuteS($q);
	}
	public static function Db_Execute($q) {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) return Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute($q);
		else return Db::getInstance()->Execute($q);
	}
	public static function Db_NumRows() {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) return Db::getInstance(_PS_USE_SQL_SLAVE_)->NumRows();
		else return Db::getInstance()->NumRows();
	}
	public static function Db_getRow($q) {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($q);
		else return Db::getInstance()->getRow($q);
	}
	private function getProductsOnLive($q, $limit, $start) {
		$result = self::Db_ExecuteS('
		SELECT p.`id_product`, CONCAT(p.`id_product`, \' - \', IFNULL(CONCAT(NULLIF(TRIM(p.reference), \'\'), \' - \'), \'\'), pl.`name`) AS name
		FROM `' . _DB_PREFIX_ . 'product` p, `' . _DB_PREFIX_ . 'product_lang` pl'. (version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? ', `' . _DB_PREFIX_ . 'product_shop` ps ' : '') . '
		WHERE p.`id_product`=pl.`id_product`
		'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? ' AND p.`id_product`=ps.`id_product` ' : '').'
		'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? Shop::addSqlRestriction(false, 'ps') : '').'
		AND pl.`id_lang`=' . (int)$this->_default_language . '
		AND p.`active` = 1
		AND ((p.`id_product` LIKE \'%' . pSQL($q) . '%\') OR (pl.`name` LIKE \'%' . pSQL($q) . '%\') OR (p.`reference` LIKE \'%' . pSQL($q) . '%\') OR (pl.`description` LIKE \'%' . pSQL($q) . '%\') OR (pl.`description_short` LIKE \'%' . pSQL($q) . '%\'))
		'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'GROUP BY p.`id_product`' : '').'
		ORDER BY pl.`name` ASC ' . ($limit ? 'LIMIT ' . $start . ', ' . (int) $limit : ''));
		return $result;
	}
	private function getSuppliersOnLive($q, $limit, $start) {
		$result = self::Db_ExecuteS('
        SELECT s.`id_supplier`, s.`name`
        FROM `' . _DB_PREFIX_ . 'supplier` s
		'. self::addSqlAssociation('supplier', 's', 'id_supplier') . '
        WHERE (s.name LIKE \'%' . pSQL($q) . '%\')
        '.((version_compare(_PS_VERSION_, '1.4.0.0', '>=')) ? 'AND s.`active` = 1' : '').'
        ORDER BY s.`name` ' . ($limit ? 'LIMIT ' . $start . ', ' . ( int ) $limit : ''));
		return $result;
	}
	private function getManufacturersOnLive($q, $limit, $start) {
		$result = self::Db_ExecuteS('
        SELECT m.`id_manufacturer`, m.`name`
        FROM `' . _DB_PREFIX_ . 'manufacturer` m
		'. self::addSqlAssociation('manufacturer', 'm', 'id_manufacturer') . '
        WHERE (m.name LIKE \'%' . pSQL($q) . '%\')
		'.((version_compare(_PS_VERSION_, '1.4.0.0', '>=')) ? 'AND m.`active` = 1' : '').'
        ORDER BY m.`name` ' . ($limit ? 'LIMIT ' . $start . ', ' . ( int ) $limit : ''));
		return $result;
	}
	private function getCMSPagesOnLive($q, $limit, $start) {
		$result = self::Db_ExecuteS('
        SELECT c.`id_cms`, cl.`meta_title`
        FROM `' . _DB_PREFIX_ . 'cms` c
		LEFT JOIN `'._DB_PREFIX_.'cms_lang` cl ON c.id_cms=cl.id_cms
		' . self::addSqlAssociation('cms', 'c','id_cms') . '
        WHERE (cl.meta_title LIKE \'%' . pSQL($q) . '%\')
		AND cl.`id_lang`=' . (int)$this->_default_language . '
        '.((version_compare(_PS_VERSION_, '1.4.0.0', '>=')) ? 'AND c.`active` = 1' : '').'
        ORDER BY cl.`meta_title` ' . ($limit ? 'LIMIT ' . $start . ', ' . ( int ) $limit : ''));
		return $result;
	}
	protected function _pmClearCache() {
		if (version_compare(_PS_VERSION_, '1.4.0.0', '<') || Configuration::get('PS_FORCE_SMARTY_2')) {
			return $this->_smarty->clear_cache(null, self::$_module_prefix);
		} else {
			return $this->_smarty->clearCache(null, self::$_module_prefix);
		}
		return true;
	}
	protected function getContent() {
	}
	public static function _getFileExtension($filename) {
		$split = explode('.', $filename);
		$extension = end($split);
		return strtolower($extension);
	}
	protected function _pmClear(){
		$this->_html .= '<div class="clear"></div>';
	}
	protected function _showWarning($text) {
		$this->_html .= '<div class="ui-widget">
        <div style="margin-top: 20px;margin-bottom: 20px;  padding: 0 .7em;" class="ui-state-error ui-corner-all">
          <p><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-alert"></span>
          ' . $text . '
        </div>
      </div>';
	}
	protected function _showRating($show = false) {
		$dismiss = (int)(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? Configuration::getGlobalValue('PM_'.self::$_module_prefix.'_DISMISS_RATING') : Configuration::get('PM_'.self::$_module_prefix.'_DISMISS_RATING'));
		if ($show && $dismiss != 1 && self::_getNbDaysModuleUsage() >= 3) {
            //$this->_html .= '
            //<div id="addons-rating-container" class="ui-widget note">
            //    <div style="margin-top: 20px; margin-bottom: 20px; padding: 0 .7em; text-align: center;" class="ui-state-highlight ui-corner-all">
            //        <p class="invite">'
            //            . $this->l('You are satisfied with our module and want to encourage us to add new features ?', $this->_coreClassName)
            //            . '<br/>'
            //            . '<a href="http://addons.prestashop.com/ratings.php" target="_blank"><strong>'
            //            . $this->l('Please rate it on Prestashop Addons, and give us 5 stars !', $this->_coreClassName)
            //            . '</strong></a>
            //        </p>
            //        <p class="dismiss">'
            //            . '[<a href="javascript:void(0);">'
            //            . $this->l('No thanks, I don\'t want to help you. Close this dialog.', $this->_coreClassName)
            //            . '</a>]
            //         </p>
            //    </div>
            //</div>';
		}
	}
	protected function _showInfo($text) {
		$this->_html .= '<div class="ui-widget">
        <div style="margin-top: 20px;margin-bottom: 20px;  padding: 0 .7em;" class="ui-state-highlight ui-corner-all">
          <p><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
          ' . $text . '
        </div>
      </div>';
	}
	protected function _displayTitle($title) {
		$this->_html .= '<h2>' . $title . '</h2>';
	}
	protected function _displaySubTitle($title) {
		$this->_html .= '<h3 class="pmSubTitle">' . $title . '</h3>';
	}
	public function _displayErrorsJs($include_script_tag = false) {
		if($include_script_tag) $this->_html .= '<script type="text/javascript">';
		if (sizeof($this->errors)) {
			foreach ( $this->errors as $key => $error )
				$this->_html .= 'parent.parent.show_error("' . $error . '");';
		}
		if($include_script_tag) $this->_html .= '</script>';
	}
	
	private function _getPMdata() {
		$param = array();
		$param[] = 'ver-'._PS_VERSION_;
		$param[] = 'current-'.$this->name;
		
		$result = self::Db_ExecuteS('SELECT DISTINCT name FROM '._DB_PREFIX_.'module WHERE name LIKE "pm_%"');
		if ($result && self::_isFilledArray($result)) {
			foreach ($result as $module) {
				$instance = Module::getInstanceByName($module['name']);
				if ($instance && isset($instance->version)) $param[] = $module['name'].'-'.$instance->version;
			}
		}
		return urlencode(base64_encode(implode('|', $param)));
	}
	protected function _displayCS() {
        //$this->_html .= '<div id="pm_panel_cs_modules_bottom" class="pm_panel_cs_modules_bottom"><br />';
        //$this->_displayTitle($this->l('Check all our modules', $this->_coreClassName));
        //$this->_html .= '<iframe src="//www.presta-module.com/cross-selling-addons-modules-footer?pm='.$this->_getPMdata().'" scrolling="no"></iframe></div>';
	}
	protected function _displaySupport() {
        //$this->_html .= '<div id="pm_footer_container" class="ui-corner-all ui-tabs ui-tabs-panel">';
        //$this->_displayCS();
        //$this->_html .= '<div id="pm_support_informations" class="pm_panel_bottom"><br />';
        //if (method_exists($this, '_displayTitle'))
        //    $this->_displayTitle($this->l('Information & Support', (isset($this->_coreClassName) ? $this->_coreClassName : false)));
        //else
        //    $this->_html .= '<h2>' . $this->l('Information & Support', (isset($this->_coreClassName) ? $this->_coreClassName : false)) . '</h2>';
        //$this->_html .= '<ul class="pm_links_block">';
        //$this->_html .= '<li class="pm_module_version"><strong>' . $this->l('Module Version: ', (isset($this->_coreClassName) ? $this->_coreClassName : false)) . '</strong> ' . $this->version . '</li>';
        //if (isset($this->_getting_started) && self::_isFilledArray($this->_getting_started))
        //    $this->_html .= '<li class="pm_get_started_link"><a href="javascript:;" class="pm_link">'. $this->l('Getting started', (isset($this->_coreClassName) ? $this->_coreClassName : false)) .'</a></li>';
        //if (self::_isFilledArray($this->_support_link))
        //    foreach($this->_support_link as $infos)
        //        $this->_html .= '<li class="pm_useful_link"><a href="'.$infos['link'].'" target="_blank" class="pm_link">'.$infos['label'].'</a></li>';
        //$this->_html .= '</ul>';
        //if (isset($this->_copyright_link) && $this->_copyright_link) {
        //    $this->_html .= '<div class="pm_copy_block">';
        //    if (isset($this->_copyright_link['link']) && !empty($this->_copyright_link['link'])) $this->_html .= '<a href="'.$this->_copyright_link['link'].'"'.((isset($this->_copyright_link['target']) AND $this->_copyright_link['target']) ? ' target="'.$this->_copyright_link['target'].'"':'').''.((isset($this->_copyright_link['style']) AND $this->_copyright_link['style']) ? ' style="'.$this->_copyright_link['style'].'"':'').'>';
        //    $this->_html .= '<img src="'.str_replace('_PATH_',$this->_path,$this->_copyright_link['img']).'" />';
        //    if (isset($this->_copyright_link['link']) && !empty($this->_copyright_link['link'])) $this->_html .= '</a>';
        //    $this->_html .= '</div>';
        //}
        //$this->_html .= '</div>';
        //$this->_html .= '</div>';
        //if (isset($this->_getting_started) && self::_isFilledArray($this->_getting_started)) {
        //    $this->_html .= "<script type=\"text/javascript\">
        //    $('.pm_get_started_link a').click(function() { $.fancybox([";
        //    $get_started_image_list = array();
        //    foreach ($this->_getting_started as $get_started_image)
        //        $get_started_image_list[] = "{ 'href': '".$get_started_image['href']."', 'title': '".htmlentities($get_started_image['title'], ENT_QUOTES, 'UTF-8')."' }";
        //    $this->_html .= implode(',', $get_started_image_list);
        //    $this->_html .= "
        //            ], {
        //            'padding'			: 0,
        //            'transitionIn'		: 'none',
        //            'transitionOut'		: 'none',
        //            'type'				: 'image',
        //            'changeFade'		: 0
        //        }); });
        //    </script>";
        //}
        //if (method_exists($this, '_includeHTMLAtEnd')) $this->_includeHTMLAtEnd();
	}
	protected function _preProcess() {
		if (isset($_GET['dismissRating'])) {
			$this->_cleanOutput();
			if (version_compare(_PS_VERSION_, '1.5.0.0', '>='))
				Configuration::updateGlobalValue('PM_'.self::$_module_prefix.'_DISMISS_RATING', 1);
			else
				Configuration::updateValue('PM_'.self::$_module_prefix.'_DISMISS_RATING', 1);
			die;
		}
		else if (isset($_GET ['pm_load_function'])) {
			if(method_exists($this, $_GET ['pm_load_function'])) {
				$this->_cleanOutput();
				if(Tools::getValue('class')) {
					if(class_exists ( Tools::getValue('class') )) {
						$class = Tools::getValue('class');
						$obj = new $class();
						if(Tools::getValue($obj->identifier)) {
							$obj = new $class(Tools::getValue($obj->identifier));
						}
						$params = array('obj'=>$obj,'class'=>$class, 'method'=>$_GET ['pm_load_function'],'reload_after'=>Tools::getValue('pm_reload_after'),'js_callback'=>Tools::getValue('pm_js_callback'));
						$this->_preLoadFunctionProcess($params);
						$this->$_GET ['pm_load_function']($params);
					}else {
						$this->_cleanOutput();
						$this->_showWarning($this->l('Class', $this->_coreClassName).' '.Tools::getValue('class').' '.$this->l('does not exists', $this->_coreClassName));
						$this->_echoOutput(true);
					}
				}
				else {
					$params = array('method' => $_GET ['pm_load_function'],'reload_after'=>Tools::getValue('pm_reload_after'),'js_callback'=>Tools::getValue('pm_js_callback'));
					$this->_preLoadFunctionProcess($params);
					$this->$_GET ['pm_load_function']($params);
				}
				$this->_echoOutput(true);
			}else {
				$this->_cleanOutput();
				$this->_showWarning($this->l('Please send class name into "class" var', $this->_coreClassName));
				$this->_echoOutput(true);
			}
		}
		elseif(isset($_GET ['pm_delete_obj'])) {
			if(Tools::getValue('class')) {
				if(class_exists ( Tools::getValue('class') )) {
					$class = Tools::getValue('class');
					$obj = new $class();
					$obj = new $class(Tools::getValue($obj->identifier));
					$this->_preDeleteProcess(array('obj'=>$obj,'class'=>$class));
					if($obj->delete()) {
						$this->_cleanOutput();
						$this->_postDeleteProcess(array('class'=>$class));
						$this->_echoOutput(true);
					}
					else {
						$this->_cleanOutput();
						$this->_showWarning($this->l('Error while deleting object', $this->_coreClassName));
						$this->_echoOutput(true);
					}
				}else {
					$this->_cleanOutput();
					$this->_showWarning($this->l('Class', $this->_coreClassName).' '.Tools::getValue('class').' '.$this->l('does not exists', $this->_coreClassName));
					$this->_echoOutput(true);
				}
			}
			else {
				$this->_cleanOutput();
				$this->_showWarning($this->l('Please send class name into "class" var', $this->_coreClassName));
				$this->_echoOutput(true);
			}
		}
		elseif(isset($_POST ['pm_save_order'])) {
			if(!Tools::getValue('order')) {
				$this->_cleanOutput();
				$this->_showWarning($this->l('Not receive IDS', $this->_coreClassName));
				$this->_echoOutput(true);
			}
			elseif(!Tools::getValue('destination_table')) {
				$this->_cleanOutput();
				$this->_showWarning($this->l('Please send destination table', $this->_coreClassName));
				$this->_echoOutput(true);
			}
			elseif(!Tools::getValue('field_to_update')) {
				$this->_cleanOutput();
				$this->_showWarning($this->l('Please send name of position field', $this->_coreClassName));
				$this->_echoOutput(true);
			}
			elseif(!Tools::getValue('identifier')) {
				$this->_cleanOutput();
				$this->_showWarning($this->l('Please send identifier', $this->_coreClassName));
				$this->_echoOutput(true);
			}
			else {
				$order = Tools::getValue('order');
				$identifier = Tools::getValue('identifier');
				$field_to_update = Tools::getValue('field_to_update');
				$destination_table = Tools::getValue('destination_table');
				foreach ($order as $position => $id) {
					$id = preg_replace("/^\w+_/", "", $id);
					$data = array($field_to_update=>$position);
					Db::getInstance()->AutoExecute(_DB_PREFIX_ . $destination_table, $data, 'UPDATE', $identifier.' = ' . (int) $id);
			}
				$this->_cleanOutput();
				$this->_echoOutput(true);
			}
		}
		elseif (isset($_GET ['getPanel']) && $_GET ['getPanel']) {
			self::_cleanBuffer();
			switch ($_GET ['getPanel']) {
				case 'getChildrenCategories':
					if (Tools::getValue('id_category_parent')){
						$children_categories = self::getChildrenWithNbSelectedSubCat(Tools::getValue('id_category_parent'), Tools::getValue('selectedCat'), $this->_default_language);
						die(self::jsonEncode($children_categories));
					}
					break;
			}
		}
	}
	protected function _preCopyFromPost() {
	}
	protected function _postCopyFromPost($params) {
	}
	protected function _preDeleteProcess($params) {
	}
	protected function _preLoadFunctionProcess(&$params) {
	}
	protected function _postDeleteProcess($params) {
		if(isset($params['include_script_tag']) && $params['include_script_tag']) $this->_html .= '<script type="text/javascript">';
		if(isset($_GET['pm_reload_after']) && $_GET['pm_reload_after'])
			$this->_reloadPanels($_GET['pm_reload_after']);
		if(isset($_GET['pm_js_callback']) && $_GET['pm_js_callback'])
			$this->_getJsCallback($_GET['pm_js_callback']);
		$this->_html .= 'parent.parent.show_info("'.$this->l('Successfully deleted', $this->_coreClassName).'");';
		if(isset($params['include_script_tag']) && $params['include_script_tag']) $this->_html .= '</script>';
	}
	protected function _getJsCallback($js_callback) {
		$js_callbacks = explode('|',$js_callback);
		foreach($js_callbacks as $js_callback) {
			$this->_html .= 'parent.parent.'.$js_callback.'();';
		}
	}
	protected function _reloadPanels($reload_after) {
		$reload_after = explode('|',$reload_after);
		foreach($reload_after as $panel) {
			$this->_html .= 'parent.parent.reloadPanel("'.$panel.'");';
		}
	}
	protected function _postSaveProcess($params) {
		if(isset($params['include_script_tag']) && $params['include_script_tag']) $this->_html .= '<script type="text/javascript">';
		if(isset($params['reload_after']) && $params['reload_after'])
			$this->_reloadPanels($params['reload_after']);
		if(isset($params['js_callback']) && $params['js_callback'])
			$this->_getJsCallback($params['js_callback']);
		$this->_html .= 'parent.parent.show_info("'.$this->l('Successfully saved', $this->_coreClassName).'");';
		if(isset($params['include_script_tag']) && $params['include_script_tag']) $this->_html .= '</script>';
	}
	protected function _postProcess() {
		if(Tools::getValue('pm_save_obj')) {
			if(class_exists ( Tools::getValue('pm_save_obj') )) {
				$class = Tools::getValue('pm_save_obj');
				$obj = new $class();
				if(Tools::getValue($obj->identifier)) {
					$obj = new $class(Tools::getValue($obj->identifier));
				}
				$this->errors = self::_retroValidateController($obj);
				if(!self::_isFilledArray($this->errors)) {
					$this->copyFromPost($obj);
					if($obj->save()) {
						$this->_cleanOutput();
						$this->_postSaveProcess(array('class'=>$class,'obj'=>$obj,'include_script_tag'=>true,'reload_after'=>Tools::getValue('pm_reload_after'),'js_callback'=>Tools::getValue('pm_js_callback')));
						$this->_echoOutput(true);
					}else {
						$this->_cleanOutput();
						$this->_showWarning($this->l('Error while saving object', $this->_coreClassName));
						$this->_echoOutput(true);
					}
				}else { $this->_cleanOutput();$this->_displayErrorsJs(true); $this->_echoOutput(true);}
			}else {
				$this->_cleanOutput();
				$this->_showWarning($this->l('Class', $this->_coreClassName).' '.Tools::getValue('class').' '.$this->l('does not exists', $this->_coreClassName));
				$this->_echoOutput(true);
			}
		} else if (Tools::getValue('getItem')) {
			$this->_cleanOutput();
			$item = Tools::getValue('itemType');
			$query = Tools::getValue('q', false);
			if (! $query || strlen($query) < 1) {
				self::_cleanBuffer();
				die();
			}
			$limit = Tools::getValue('limit', 100);
			$start = Tools::getValue('start', 0);
			switch ($item) {
				case 'product' :
					$items = $this->getProductsOnLive($query, $limit, $start);
					$item_id_column = 'id_product';
					$item_name_column = 'name';
					break;
				case 'supplier' :
					$items = $this->getSuppliersOnLive($query, $limit, $start);
					$item_id_column = 'id_supplier';
					$item_name_column = 'name';
					break;
				case 'manufacturer' :
					$items = $this->getManufacturersOnLive($query, $limit, $start);
					$item_id_column = 'id_manufacturer';
					$item_name_column = 'name';
					break;
				case 'cms' :
					$items = $this->getCMSPagesOnLive($query, $limit, $start);
					$item_id_column = 'id_cms';
					$item_name_column = 'meta_title';
					break;
			}
			if ($items)
				foreach ( $items as $row )
					$this->_html .= $row [$item_id_column] . '=' . $row [$item_name_column] . "\n";
			$this->_echoOutput(true);
			die();
		}
	}
	protected function _initClassVar() {
		global $cookie, $smarty, $currentIndex, $employee;
		$this->_cookie = $cookie;
		$this->_smarty = $smarty;
		$this->_employee = $employee;
		$this->_base_config_url = ((version_compare(_PS_VERSION_, '1.5.0.0', '<')) ? $currentIndex : $_SERVER['SCRIPT_NAME'].(($controller = Tools::getValue('controller')) ? '?controller='.$controller: '')) . '&configure=' . $this->name . '&token=' . Tools::getValue('token');
		$this->_default_language = (int) Configuration::get('PS_LANG_DEFAULT');
		$this->_iso_lang = Language::getIsoById($this->_cookie->id_lang);
		$this->_languages = Language::getLanguages(false);
	}
	protected function _startForm($configOptions) {
		$defaultOptions = array(
			'action' => false,
			'target' => 'dialogIframePostForm',
			'iframetarget' => true
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		if ($configOptions['iframetarget']) $this->_headerIframe();
		$this->_html .= '<form action="' . ($configOptions['action'] ? $configOptions['action'] : $this->_base_config_url) . '" method="post" class="width3" id="' . $configOptions['id'] . '" target="' . $configOptions['target'] . '">';
		if(isset($configOptions['obj']) && $configOptions['obj'] && isset($configOptions['obj']->id) && $configOptions['obj']->id) {
			$this->_html .= '<input type="hidden" name="'.$configOptions['obj']->identifier.'" value="'.$configOptions['obj']->id.'" />';
		}
		if(isset($configOptions['obj']) && $configOptions['obj'])
			$this->_html .= '<input type="hidden" name="pm_save_obj" value="'.get_class($configOptions['obj']).'" />';
		if(isset($configOptions['params']['reload_after']) && $configOptions['params']['reload_after'])
			$this->_html .= '<input type="hidden" name="pm_reload_after" value="'.$configOptions['params']['reload_after'].'" />';
		if(isset($configOptions['params']['js_callback']) && $configOptions['params']['js_callback'])
			$this->_html .= '<input type="hidden" name="pm_js_callback" value="'.$configOptions['params']['js_callback'].'" />';
	}
	protected function _endForm($configOptions) {
		$defaultOptions = array(
			'id' => NULL,
			'iframetarget' => true,
			'jquerytoolsvalidatorfunction' => false
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		$this->_html .= '</form>';
		if ($configOptions['id'] != NULL && in_array('jquerytools', $this->_css_js_to_load)) {
			$this->_html .= '
			<script type="text/javascript">
				$jqPm("#'.$configOptions['id'].'").validator({
					lang: "'.$this->_iso_lang.'",
					messageClass: "formValidationError",
					errorClass: "elementErrorAssignedClass",
					position: "center bottom"
				})'.(($configOptions['jquerytoolsvalidatorfunction'] != false) ? '.submit('.$configOptions['jquerytoolsvalidatorfunction'].')' : '').';
			</script>';
		}
		if ($configOptions['iframetarget']) $this->_footerIframe();
	}
	public function _retrieveFormValue($type, $fieldName, $fieldDbName = false, $obj, $defaultValue = '', $compareValue = false, $key = false) {
		if (! $fieldDbName) $fieldDbName = $fieldName;
		switch ($type) {
			case 'text' :
				if ($key)
					return htmlentities(stripslashes(Tools::getValue($fieldName, ($obj && isset($obj->{$fieldDbName} [$key]) ? $obj->{$fieldDbName} [$key] : $defaultValue))), ENT_COMPAT, 'UTF-8');
				else
					return htmlentities(stripslashes(Tools::getValue($fieldName, ( $obj && isset($obj->{$fieldDbName}) ? $obj->{$fieldDbName} : $defaultValue))), ENT_COMPAT, 'UTF-8');
				break;
			case 'textpx' :
				if ($key)
					return intval(preg_replace('#px#', '', Tools::getValue($fieldName, ( $obj && isset($obj->{$fieldDbName}) ? $obj->{$fieldDbName} [$key] : $defaultValue))));
				else
					return intval(preg_replace('#px#', '', Tools::getValue($fieldName, ( $obj && isset($obj->{$fieldDbName}) ? $obj->{$fieldDbName} : $defaultValue))));
				break;
			case 'select' :
				return ((Tools::getValue($fieldName, ( $obj && isset($obj->{$fieldDbName}) ? $obj->{$fieldDbName} : $defaultValue)) == $compareValue) ? ' selected="selected"' : '');
				break;
			case 'radio' :
			case 'checkbox' :
				if(isset($obj->$fieldName) && is_array($obj->$fieldName) && sizeof($obj->$fieldName) && isset($obj->{$fieldDbName})  )
					return ( ( in_array($compareValue,$obj->$fieldName) ) ? ' checked="checked"' : '');
				return ((Tools::getValue($fieldName, ($obj && isset($obj->{$fieldDbName}) ? $obj->{$fieldDbName} : $defaultValue)) == $compareValue) ? ' checked="checked"' : '');
				break;
		}
	}
	protected function _startFieldset($title, $icone = false, $hide = true, $onclick = false) {
		$this->_html .= '<fieldset>';
		if ($title || $hide) $this->_html .= '<legend class="ui-state-default" style="cursor:pointer;" onclick="$jqPm(this).next(\'div\').slideToggle(\'fast\'); '.
		($onclick?$onclick:'').'">' . ($icone ? '<img src="' . $icone . '" alt="' . $title . '" title="' . $title . '" /> ' : '') . '' . $title . ' <small ' . (! $hide ? 'style="display:none;"' : '') . '>' . $this->l('Click here to edit', $this->_coreClassName) . '</small></legend>';
		$this->_html .= '<div' . ($hide ? ' class="hideAfterLoad"' : '') . '>';
	}
	protected function _endFieldset() {
		$this->_html .= '</div>';
		$this->_html .= '</fieldset>';
		$this->_html .= '<br />';
	}
	protected function _displayAjaxSelectMultiple($configOptions) {
		$defaultOptions = array(
			'remoteurl' => false,
			'limit' => 50,
			'limitincrement' => 20,
			'remoteparams' => false,
			'tips' => false,
			'triggeronliclick' => true,
			'displaymore' => true,
			'idcolumn' => '',
			'namecolumn' => ''
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		$this->_html .= '<label>' . $configOptions['label'] . '</label>
	    				<div class="margin-form">';
		$this->_html .= '<select id="multiselect' . $configOptions['key'] . '" class="multiselect" multiple="multiple" name="' . $configOptions['key'] . '[]">';
		if ($configOptions['selectedoptions'] && is_array($configOptions['selectedoptions']) && sizeof($configOptions['selectedoptions'])) {
			$index_column = false;
			if (isset($configOptions['namecolumn']) && isset($configOptions['idcolumn']) && !empty($configOptions['namecolumn']) && !empty($configOptions['idcolumn'])) $index_column = true;
			foreach ( $configOptions['selectedoptions'] as $value => $option ) {
				if ($index_column) {
					$this->_html .= '<option value="' . (int) $option[$configOptions['idcolumn']] . '" selected="selected">' . htmlentities($option[$configOptions['namecolumn']], ENT_COMPAT, 'UTF-8') . '</option>';
				} else {
					$this->_html .= '<option value="' . (int) $value . '" selected="selected">' . htmlentities($option, ENT_COMPAT, 'UTF-8') . '</option>';
				}
			}
		}
		$this->_html .= '</select>';
		if (isset($configOptions['tips']) && $configOptions['tips']) {
			$this->_html .= '<img title="'.$configOptions['tips'].'" id="' . $configOptions['key'] . '-tips" class="pm_tips" src="' . $this->_path . 'img/question.png" width="16px" height="16px" />';
			$this->_html .= '<script type="text/javascript">initTips("#' . $configOptions['key'] . '")</script>';
		}
		$this->_pmClear();
		$this->_html .= '</div>';
		$this->_html .= '<script type="text/javascript">
			$jqPm("#multiselect' . $configOptions['key'] . '").multiselect({
				locale: {
						addAll:\''.addcslashes($this->l('Add all', $this->_coreClassName), "'").'\',
						removeAll:\''.addcslashes($this->l('Remove all', $this->_coreClassName), "'").'\',
						itemsCount:\''.addcslashes($this->l('#{count} items selected', $this->_coreClassName), "'").'\',
						itemsTotal:\''.addcslashes($this->l('#{count} items total', $this->_coreClassName), "'").'\',
						busy:\''.addcslashes($this->l('Please wait...', $this->_coreClassName), "'").'\',
						errorDataFormat:\''.addcslashes($this->l('Cannot add options, unknown data format', $this->_coreClassName), "'").'\',
						errorInsertNode:"'.addcslashes($this->l('There was a problem trying to add the item', $this->_coreClassName).':\n\n\t[#{key}] => #{value}\n\n'.addcslashes($this->l('The operation was aborted.', $this->_coreClassName), '"'), "'").'",
						errorReadonly:\''.addcslashes($this->l('The option #{option} is readonly', $this->_coreClassName), "'").'\',
						errorRequest:\''.addcslashes($this->l('Sorry! There seemed to be a problem with the remote call. (Type: #{status})', $this->_coreClassName), "'").'\',
						sInputSearch:\''.addcslashes($this->l('Please enter the first letters of the search item', $this->_coreClassName), "'").'\',
						sInputShowMore:\''.addcslashes($this->l('Show more', $this->_coreClassName), "'").'\'
					},
				remoteUrl: "' . $configOptions['remoteurl'] . '",
				remoteLimit:' . (int) $configOptions['limit'] . ',
				remoteStart:0,
				remoteLimitIncrement:' . (int) $configOptions['limitincrement'] . ($configOptions['remoteparams'] ? ', remoteParams: { ' . $configOptions['remoteparams'] . ' }' : '') . ',
				triggerOnLiClick: '. (($configOptions['triggeronliclick'] == true) ? 'true' : 'false') .',
				displayMore: '. (($configOptions['displaymore'] == true) ? 'true' : 'false') .'
			});
		    </script>';
		$this->_pmClear();
	}
	protected function _displayInputActive($configOptions) {
		$defaultOptions = array(
			'defaultvalue' => false,
			'tips' => false,
			'onclick' => false
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		$this->_html .= '<label>' . $configOptions['label'] . '</label>
	    <div class="margin-form"><label class="t" for="' . $configOptions['key_active'] . '_on" style="float:left;"><img src="../img/admin/enabled.gif" alt="' . $this->l('Yes', $this->_coreClassName) . '" title="' . $this->l('Yes', $this->_coreClassName) . '" /></label>
	      <input type="radio" name="' . $configOptions['key_active'] . '" id="' . $configOptions['key_active'] . '_on" ' . ($configOptions['onclick'] ? 'onclick="' . $configOptions['onclick'] . '"' : '') . ' value="1" ' . $this->_retrieveFormValue('radio', $configOptions['key_active'], $configOptions['key_db'], $configOptions['obj'], $configOptions['defaultvalue'], 1) . '  style="float:left;" />
	      <label class="t" for="' . $configOptions['key_active'] . '_on" style="float:left;"> ' . $this->l('Yes', $this->_coreClassName) . '</label>
	      <label class="t" for="' . $configOptions['key_active'] . '_off" style="float:left;"><img src="../img/admin/disabled.gif" alt="' . $this->l('No', $this->_coreClassName) . '" title="' . $this->l('No', $this->_coreClassName) . '" style="margin-left: 10px;" /></label>
	      <input type="radio" name="' . $configOptions['key_active'] . '" id="' . $configOptions['key_active'] . '_off" ' . ($configOptions['onclick'] ? 'onclick="' . $configOptions['onclick'] . '"' : '') . ' value="0" ' . $this->_retrieveFormValue('radio', $configOptions['key_active'], $configOptions['key_db'], $configOptions['obj'], $configOptions['defaultvalue'], 0) . '  style="float:left;"/>
	      <label class="t" for="' . $configOptions['key_active'] . '_off" style="float:left;"> ' . $this->l('No', $this->_coreClassName) . '</label>';
		if (isset($configOptions['tips']) && $configOptions['tips']) {
			$this->_html .= '<img title="'.$configOptions['tips'].'" id="' . $configOptions['key_active'] . '-tips" class="pm_tips" src="' . $this->_path . 'img/question.png" width="16px" height="16px" />';
			$this->_html .= '<script type="text/javascript">initTips("#' . $configOptions['key_active'] . '")</script>';
		}
		$this->_pmClear();
		$this->_html .= '</div>';
	}
	private function _parseOptions($defaultOptions = array(), $options = array()) {
		if (self::_isFilledArray($options)) $options = array_change_key_case($options, CASE_LOWER);
		if (isset($options['tips']) && !empty($options['tips'])) $options['tips'] = stripslashes(addcslashes($options['tips'], '"'));
		if (self::_isFilledArray($defaultOptions)) {
			$defaultOptions = array_change_key_case($defaultOptions, CASE_LOWER);
			foreach ($defaultOptions as $option_name=>$option_value)
				if (!isset($options[$option_name])) $options[$option_name] = $defaultOptions[$option_name];
		}
		return $options;
	}
	protected function _displayInputText($configOptions) {
		$defaultOptions = array(
			'type' => 'text',
			'size' => '150px',
			'defaultvalue' => false,
			'min' => false,
			'max' => false,
			'maxlength' => false,
			'onkeyup' => false,
			'onchange' => false,
			'required' => false,
			'tips' => false
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		$this->_html .= '<label>' . $configOptions['label'] . '</label>
		    <div class="margin-form">
		      <input style="width:' . $configOptions['size'] . '" type="'. $configOptions['type'] .'" name="' . $configOptions['key'] . '" id="' . $configOptions['key'] . '" value="' . $this->_retrieveFormValue('text', $configOptions['key'], false, $configOptions['obj'], $configOptions['defaultvalue']) . '" class="ui-corner-all ui-input-pm" '.(($configOptions['required'] == true) ? 'required="required" ' : '') . ($configOptions['onkeyup'] ? ' onkeyup="' . $configOptions['onkeyup'] . '"' : '') . ($configOptions['onchange'] ? ' onchange="' . $configOptions['onchange'] . '"' : '') . (($configOptions['min'] != false) ? 'min="'.(int)$configOptions['min'].'" ' : '').(($configOptions['max'] != false) ? 'max="'.(int)$configOptions['max'].'" ' : '').(($configOptions['maxlength'] != false) ? 'maxlength="'.(int)$configOptions['maxlength'].'" ' : '').'/>';
		if (isset($configOptions['tips']) && $configOptions['tips']) {
			$this->_html .= '<img title="'.$configOptions['tips'].'" id="' . $configOptions['key'] . '-tips" class="pm_tips" src="' . $this->_path . 'img/question.png" width="16px" height="16px" />';
			$this->_html .= '<script type="text/javascript">initTips("#' . $configOptions['key'] . '")</script>';
		}
		$this->_pmClear();
		$this->_html .= '</div>';
	}
	protected function _displayInputTextLang($configOptions) {
		$defaultOptions = array(
			'size' => '150px',
			'min' => false,
			'max' => false,
			'maxlength' => false,
			'onkeyup' => false,
			'onchange' => false,
			'required' => false,
			'tips' => false
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		$this->_html .= '<label>' . $configOptions['label'] . '</label>
             <div class="margin-form">';
		foreach ( $this->_languages as $language ) {
			$this->_html .= '
		        <div id="lang' . $configOptions['key'] . '_' . $language ['id_lang'] . '" class="pmFlag pmFlagLang_' . $language ['id_lang'] . '" style="display: ' . ($language ['id_lang'] == $this->_default_language ? 'block' : 'none') . '; float: left;">
		          <input style="width:' . $configOptions['size'] . ';" type="text" id="' . $configOptions['key'] . '_' . $language ['id_lang'] . '" name="' . $configOptions['key'] . '_' . $language ['id_lang'] . '" value="' . $this->_retrieveFormValue('text', $configOptions['key'] . '_' . $language ['id_lang'], $configOptions['key'], $configOptions['obj'], false, false, $language ['id_lang']) . '"' . ($configOptions['onkeyup'] ? ' onkeyup="' . $configOptions['onkeyup'] . '"' : '') . ($configOptions['onchange'] ? ' onchange="' . $configOptions['onchange'] . '"' : '') . (($configOptions['required'] == true && $language['id_lang'] == $this->_default_language) ? ' required="required" ' : '') . (($configOptions['min'] != false && $language['id_lang'] == $this->_default_language) ? 'min="'.(int)$configOptions['min'].'" ' : '').(($configOptions['max'] != false && $language['id_lang'] == $this->_default_language) ? 'max="'.(int)$configOptions['max'].'" ' : '').(($configOptions['maxlength'] != false && $language['id_lang'] == $this->_default_language) ? 'maxlength="'.(int)$configOptions['maxlength'].'" ' : '') . ' class="ui-corner-all ui-input-pm" />
		        </div>';
		}
		$this->displayPMFlags();
		if (isset($configOptions['tips']) && $configOptions['tips']) {
			$this->_html .= '<img title="'.$configOptions['tips'].'" id="' . $configOptions['key'] . '-tips" class="pm_tips" src="' . $this->_path . 'img/question.png" width="16px" height="16px" />';
			$this->_html .= '<script type="text/javascript">initTips("#' . $configOptions['key'] . '")</script>';
		}
		$this->_pmClear();
		$this->_html .= '</div>';
	}
	protected function _displaySelect($configOptions) {
		$defaultOptions = array(
			'size' => '200px',
			'defaultvalue' => false,
			'options' => array(),
			'onchange' => false,
			'tips' => false
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		$this->_html .= '<label>' . $configOptions['label'] . '</label>
		    <div class="margin-form pm_displaySelect">
		      <select id="' . $configOptions['key'] . '" name="' . $configOptions['key'] . '" style="width:' . $configOptions['size'] . '">';
		if($configOptions['defaultvalue'])
			 $this->_html .= '<option value="0">' . $configOptions['defaultvalue'] . '</option>';
		foreach ( $configOptions['options'] as $value => $text_value ) {
			$this->_html .= '<option value="' . ($value) . '" ' . $this->_retrieveFormValue('select', $configOptions['key'], false, $configOptions['obj'], '0', $value) . ' '.(isset($configOptions['class']) && self::_isFilledArray($configOptions['class'][$value]) ? 'class="' . $configOptions['class'][$value] . '"':'').'>' . $text_value . '</option>';
		}
		$this->_html .= '</select>';
		if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
			$this->_html .= '<script type="text/javascript">
				$jqPm("#' . $configOptions['key'] . '").chosen({ disable_search: true, max_selected_options: 1, inherit_select_classes: true });';
		} else {
			$this->_html .= '<script type="text/javascript">
								$jqPm("#' . $configOptions['key'] . '").selectmenu({wrapperElement: "<div class=\'ui_select_menu\' />"});';
		}
		if ($configOptions['onchange']) {
			$this->_html .= '$jqPm("#' . $configOptions['key'] . '").unbind("change").bind("change",function() { ' . $configOptions['onchange'] . ' });';
		}
		$this->_html .= '</script>';
		if (isset($configOptions['tips']) && $configOptions['tips']) {
			$this->_html .= '<img title="'.$configOptions['tips'].'" id="' . $configOptions['key'] . '-tips" class="pm_tips" src="' . $this->_path . 'img/question.png" width="16px" height="16px" />';
			$this->_html .= '<script type="text/javascript">initTips("#' . $configOptions['key'] . '")</script>';
		}
		$this->_pmClear();
		$this->_html .= '</div>';
	}
	protected function _displayCategoryTree($configOptions) {
		$defaultOptions = array(
			'input_name' => 'categoryBox',
			'selected_cat' => array(0),
			'use_radio' => false,
			'category_root_id' => (version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? Category::getRootCategory()->id : 1)
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		$selectedCat = $this->getCategoryInformations(Tools::getValue('categoryBox', $configOptions['selected_cat']), $this->_default_language, $configOptions['input_name'], $configOptions['use_radio']);
		$this->_html .= '<div class="category-tree-table-container">';
		$this->_html .= '<label>' . $configOptions['label'] . '</label>
					<div class="margin-form">';
		$this->_html .= '<script type="text/javascript">
							var post_selected_cat;
							post_selected_cat = \'' . implode(',', array_keys($selectedCat)) . '\';
						</script>
						<div class="category-tree-table">
							<table cellpadding="5">
								<tr id="tr_categories">
									<td colspan="2">
						';
		$trads = array ('selected' => $this->l('selected', $this->_coreClassName), 'Collapse All' => $this->l('Collapse All', $this->_coreClassName), 'Expand All' => $this->l('Expand All', $this->_coreClassName), 'Check All' => $this->l('Check All', $this->_coreClassName), 'Uncheck All' => $this->l('Uncheck All', $this->_coreClassName) );
		$this->_html .= $this->_renderAdminCategorieTree($trads, $selectedCat, $configOptions['input_name'], $configOptions['use_radio'], $configOptions['category_root_id']) . '
									</td>
								</tr>
								<tr>
									<td colspan="2" style="padding-bottom:5px;"><hr style="width:100%;" /></td>
								</tr>
							</table>
						</div>
					</div>
				</div>';
		$this->_pmClear();
	}
	private static function getCategoryInformations($ids_category, $id_lang = null)
	{
		if ($id_lang === null) $id_lang = $this->_default_language;
		if (!self::_isFilledArray($ids_category)) return;
		$categories = array();
		if (isset($ids_category[0]['id_category'])) {
			$ids_category_tmp = array();
			foreach ($ids_category as $cat) $ids_category_tmp[] = $cat['id_category'];
			$ids_category = $ids_category_tmp;
		} else if (is_object($ids_category[0]) && isset($ids_category[0]->id_category)) {
			$ids_category_tmp = array();
			foreach ($ids_category as $cat) $ids_category_tmp[] = $cat->id_category;
			$ids_category = $ids_category_tmp;
		}
		$results = Db::getInstance()->ExecuteS('
			SELECT c.`id_category`, cl.`name`, cl.`link_rewrite`, cl.`id_lang`
			FROM `'._DB_PREFIX_.'category` c
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? Shop::addSqlRestrictionOnLang('cl'):'').')
			WHERE cl.`id_lang` = '.(int)$id_lang.'
			AND c.`id_category` IN ('.implode(',', array_map('intval', $ids_category)).')
		');
		foreach($results as $category) $categories[$category['id_category']] = $category;
		return $categories;
	}
	private function _renderAdminCategorieTree($trads, $selected_cat = array(), $input_name = 'categoryBox', $use_radio = false, $category_root_id = 1){
		if (!$use_radio) $input_name = $input_name.'[]';
		$html = '';
		$root_is_selected = false;
		foreach($selected_cat AS $cat){
			if (self::_isFilledArray($cat)) {
				if ($cat['id_category'] != $category_root_id) $html .= '<input type="hidden" name="'.$input_name.'" value="'.$cat['id_category'].'" >';
				elseif($cat['id_category'] == $category_root_id) $root_is_selected = true;
			} else {
				if($cat != $category_root_id) $html .= '<input type="hidden" name="'.$input_name.'" value="'.$cat.'" >';
				else $root_is_selected = true;
			}
		}
		$root_category = new Category($category_root_id, $this->_default_language);
		$root_category_name = $root_category->name;
		$html .= '<script type="text/javascript">';
		if (self::_isFilledArray($selected_cat)){
			if (isset($selected_cat[0]))
				$html .= 'var selectedCat = "'.implode(',', $selected_cat).'";';
			else
				$html .= 'var selectedCat = "'.implode(',', array_keys($selected_cat)).'";';
		}
		else
			$html .= 'var selectedCat = "";';
		$html .= '	var selectedLabel = \''.$trads['selected'].'\';
					var home = \''.addcslashes($root_category_name, "'").'\';
					var use_radio = '.(int)$use_radio.';
					var category_root_id = '.(int)$category_root_id.';
				</script>
				<link type="text/css" rel="stylesheet" href="'.$this->_path.'css/jquery.treeview.css" />';
		$html .= '<script src="'.$this->_path.'js/treeview/jquery.treeview.js" type="text/javascript"></script>
				 <script src="'.$this->_path.'js/treeview/jquery.treeview.async.js" type="text/javascript"></script>
				 <script src="'.$this->_path.'js/treeview/jquery.treeview.edit.js" type="text/javascript"></script>
				 <script src="'.$this->_path.'js/admin-categories-tree.js" type="text/javascript"></script>
				 <script type="text/javascript">
					var inputName = "'.$input_name.'";
				 </script>';
		$html .= '<div class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
					<a rel="'. $input_name .'" href="#" id="collapse_all" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-all" style="padding:3px;">'.$trads['Collapse All'].'</a> &nbsp;
					<a rel="'. $input_name .'" href="#" id="expand_all" class="ui-button ui-widget ui-state-default ui-button-text-only  ui-corner-all" style="padding:3px;">'.$trads['Expand All'].'</a> &nbsp;
					'.(!$use_radio ? '- <a href="#" rel="'. $input_name .'" id="check_all" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-all" style="padding:3px;">'.$trads['Check All'].'</a> &nbsp;
					<a href="#" rel="'. $input_name .'" id="uncheck_all" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-all" style="padding:3px;">'.$trads['Uncheck All'].'</a>' : '').'
				</div>';
		$html .= '<ul id="categories-treeview" class="filetree">';
		$html .= '<li id="'.(int)$category_root_id.'" class="hasChildren">';
		$html .= '<span class="folder"><input type="'.(!$use_radio ? 'checkbox' : 'radio').'" name="'.$input_name.'" value="'.$category_root_id.'" '.($root_is_selected ? 'checked' : '').' onclick="clickOnCategoryBox($jqPm(this));" /> '.$root_category_name.'</span>';
		$html .= '<ul>
						<li><span class="placeholder">&nbsp;</span></li>
				  </ul>
				</li>
			</ul>';
		return $html;
	}
	private static function getAllSubCategories($id_cat, $id_lang, $all_sub_categories = array()) {
		$category = new Category((int)$id_cat);
		$sub_cats = $category->getSubcategories($id_lang);
		if(count($sub_cats) > 0)
			foreach ($sub_cats AS $sub_cat) {
				$all_sub_categories[] = $sub_cat['id_category'];
				self::getAllSubCategories($sub_cat['id_category'], $id_lang, $all_sub_categories);
			}
		return $all_sub_categories;
	}
	public static function getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang)
	{
		$selectedCat = explode(',', str_replace(' ', '', $selectedCat));
		if (!is_array($selectedCat)) $selectedCat = array();
		if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')) {
			return Db::getInstance()->ExecuteS('
					SELECT c.`id_category`, c.`level_depth`, cl.`name`, IF((
					SELECT COUNT(*)
					FROM `'._DB_PREFIX_.'category` c2
					WHERE c2.`id_parent` = c.`id_category`
			) > 0, 1, 0) AS has_children, '.($selectedCat ? '(
					SELECT count(c3.`id_category`)
					FROM `'._DB_PREFIX_.'category` c3
					WHERE c3.`nleft` > c.`nleft`
					AND c3.`nright` < c.`nright`
					AND c3.`id_category`  IN ('.implode(',', array_map('intval', $selectedCat)).')
			)' : '0').' AS nbSelectedSubCat
					FROM `'._DB_PREFIX_.'category` c
					LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? Shop::addSqlRestrictionOnLang('cl'):'').')
					WHERE `id_lang` = '.(int)($id_lang).'
					AND c.`id_parent` = '.(int)($id_parent).'
					ORDER BY `position` ASC');
		} else {
			$homecat = new Category((int)$id_parent, (int)$id_lang);
			$categories = $homecat->recurseLiteCategTree();
			$categories_table = array();
			if (self::_isFilledArray($categories)) {
				foreach ($categories['children'] as $categorie) {
					$categorie_obj = new Category((int)$categorie['id'], (int)$id_lang);
					$all_sub_categories = self::getAllSubCategories((int)$categorie['id'], (int)$id_lang);
					$categories_table[] = array(
						'id_category' => $categorie['id'],
						'level_depth' => $categorie_obj->level_depth,
						'name' => $categorie['name'],
						'has_children' => (int)(is_array($categorie['children']) && sizeof($categorie['children'])),
						'nbSelectedSubCat' => sizeof(array_intersect($selectedCat, array_values($all_sub_categories)))
					);
				}
			}
			return $categories_table;
		}
	}
	protected function _loadCssJsLibrary($library, $rememberLoadedLibrary = true) {
		if (in_array($library, $this->_css_js_lib_loaded))
			return;
		switch ($library) {
			case 'admincore' :
				$this->_html .= '<link type="text/css" rel="stylesheet" href="' . $this->_path . 'css/adminCore.css" />
	        					 <script type="text/javascript" src="' . $this->_path . 'js/adminCore.js"></script>';
				$this->_html .= '<script type="text/javascript">
					var _modulePath = "' . $this->_path . '";
					var _base_config_url = "' . $this->_base_config_url . '";
					var _id_employee = ' . ( int ) $this->_cookie->id_employee . ';
					var id_language = Number(' . $this->_default_language . ');
				</script>';
				break;
			case 'adminmodule' :
				if (file_exists(dirname(__FILE__) . '/css/admin.css'))
					$this->_html .= '<link type="text/css" rel="stylesheet" href="' . $this->_path . 'css/admin.css" />';
				if (file_exists(dirname(__FILE__) . '/js/admin.js'))
					$this->_html .= '<script type="text/javascript" src="' . $this->_path . 'js/admin.js"></script>';
				break;
			case 'jquery' :
					if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
						$this->context->controller->addJqueryUI(array('ui.draggable', 'ui.droppable', 'ui.sortable', 'ui.widget', 'ui.dialog', 'ui.tabs'), 'base');
					} else {
						if (version_compare(_PS_VERSION_, '1.5.0.0', '<'))
							$this->_html .= '<script type="text/javascript" src="'.$this->_path . 'js/jquery.min.js"></script>';
						$this->_html .= ' <link type="text/css" rel="stylesheet" href="' . $this->_path . 'js/jqueryui/1.8.9/themes/custom-theme/jquery-ui-1.8.16.custom.css" />
							<script type="text/javascript" src="'.$this->_path . 'js/jquery-ui-1.8.11.min.js"></script>';
					}
					$this->_html .= '<script type="text/javascript">';
					if (version_compare(_PS_VERSION_, '1.5.0.0', '<'))
						$this->_html .= 'var $jqPm = jQuery.noConflict(true);';
					else
						$this->_html .= 'var $jqPm = jQuery;';
					$this->_html .= '</script>';
			break;
			case 'jquerytiptip':
				$this->_html .= '<script type="text/javascript" src="'.$this->_path . 'js/jquery.tipTip.js"></script>';
				break;
			case 'jquerytools':
				$this->_html .= '<script type="text/javascript" src="'.$this->_path . 'js/jquery.tools.min.js"></script>';
				break;
			case 'jgrowl' :
				$this->_html .= '<link type="text/css" rel="stylesheet" href="' . $this->_path . 'js/jGrowl/jquery.jgrowl.css" />
		    					 <script type="text/javascript" src="' . $this->_path . 'js/jGrowl/jquery.jgrowl_minimized.js"></script>';
				break;
			case 'multiselect' :
				$this->_html .= '<link rel="stylesheet" href="' . $this->_path . 'js/multiselect/ui.multiselect.css" type="text/css" />
								 <script type="text/javascript" src="' . $this->_path . 'js/multiselect/jquery.tmpl.1.1.1.js"></script>
								 <script type="text/javascript" src="' . $this->_path . 'js/multiselect/jquery.blockUI.js"></script>
								 <script type="text/javascript" src="' . $this->_path . 'js/multiselect/ui.multiselect.js"></script>';
				break;
			case 'datatables' :
				$this->_html .= '<script type="text/javascript" src="' . $this->_path . 'js/datatables/jquery.dataTables.min.js"></script>
		  						<link rel="stylesheet" href="' . $this->_path . 'js/datatables/demo_table_jui.css" type="text/css" />';
				break;
			case 'selectmenu' :
				if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
					$this->context->controller->addJqueryPlugin('chosen');
				} else {
					$this->_html .= '<script type="text/javascript" src="' . $this->_path . 'js/ui.selectmenu.js"></script>';
				}
				break;
		}
		if ($rememberLoadedLibrary)
			$this->_css_js_lib_loaded [] = $library;
	}
	protected function _loadCssJsLibraries($rememberLoadedLibrary = true) {
		if (self::_isFilledArray($this->_css_js_to_load)) {
			foreach ($this->_css_js_to_load as $library) {
				$this->_loadCssJsLibrary($library, $rememberLoadedLibrary);
			}
		}
	}
	private function _includeHTMLAtEnd() {
		$this->_html .= '<script type="text/javascript">$jqPm(\'.hideAfterLoad\').hide();</script>';
		$this->_html .= $this->_html_at_end;
	}
	protected function _addButton($configOptions) {
		$defaultOptions = array(
			'text' => '',
			'href' => '',
			'title' => '',
			'onclick' => false,
			'icon_class' => false,
			'class' => false,
			'rel' => false,
			'target' => false,
			'id' => false
		);
		$configOptions = $this->_parseOptions($defaultOptions, $configOptions);
		if(!$configOptions['id'])
			$curId = 'button_' . uniqid();
		else $curId = $configOptions['id'];
		$this->_html .= '<a href="' . htmlentities($configOptions['href'], ENT_COMPAT, 'UTF-8') . '" title="' . htmlentities($configOptions['title'], ENT_COMPAT, 'UTF-8') . '" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' . ($configOptions['class'] ? ' ' . htmlentities($configOptions['class'], ENT_COMPAT, 'UTF-8') . '' : '') . '" id="' . $curId . '" ' . ($configOptions['text'] ? 'style="padding-right:5px;"' : '') . ' ' . ($configOptions['rel'] ? 'rel="' . $configOptions['rel'] . '"' : '')  . ($configOptions['target'] ? ' target="' . $configOptions['target'] . '"' : '') . '>
		' . ($configOptions['icon_class'] ? '<span class="' . htmlentities($configOptions['icon_class'], ENT_COMPAT, 'UTF-8') . '" style="float: left; margin-right: .3em;"></span>' : '') . '
		' . $configOptions['text'] . '
		</a>';
		if ($configOptions['onclick']) $this->_html .= '<script type="text/javascript">$jqPm("#' . $curId . '").unbind("click").bind("click", function() { ' . $configOptions['onclick'] . ' });</script>';
	}
	protected function _displaySubmit($value, $name) {
		$this->_pmClear();
		$this->_html .= '<center><input type="submit" value="' . $value . '" name="' . $name . '" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" /></center><br />';
	}
	protected function _headerIframe() {
		if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
			$assets = array();
			$backupHtml = $this->_html;
			$this->_loadCssJsLibraries(false);
			foreach ($this->context->controller->css_files as $cssUri=>$media)
				if (!preg_match('/gamification/i', $cssUri))
					$assets[] = '<link href="'.$cssUri.'" rel="stylesheet" type="text/css" media="'.$media.'" />';
			foreach ($this->context->controller->js_files as $jsUri)
				if (!preg_match('#gamification|notifications\.js|help\.js#i', $jsUri))
					$assets[] = '<script type="text/javascript" src="'.$jsUri.'"></script>';
			$assets[] = '<script type="text/javascript">$jqPm = jQuery;</script>';
			$this->_html = $backupHtml;
		}
		$this->_html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="' . $this->_iso_lang . '" lang="' . $this->_iso_lang . '">
	      <head>
	        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	        <title>PrestaShop</title>
	        '.(version_compare(_PS_VERSION_, '1.5.0.0', '>=') && version_compare(_PS_VERSION_, '1.6', '<') ? '<script type="text/javascript" src="'.$this->_path . 'js/jquery.min.js"></script>' : '').'
	        '.(version_compare(_PS_VERSION_, '1.6.0.0', '>=') ? implode('', $assets) : '').'
	      </head>
	      <body style="background:#fff;" class="pm_bo_ps_'.substr(str_replace('.', '', _PS_VERSION_), 0, 2).'">';
		$this->_loadCssJsLibraries();
	}
	protected function _footerIframe() {
		$this->_html .= '<iframe name="dialogIframePostForm" id="dialogIframePostForm" frameborder="0" marginheight="0" marginwidth="0" width="' . ($this->_debug_mode ? '500' : '0') . '" height="' . ($this->_debug_mode ? '500' : '0') . '"></iframe>';
		$this->_includeHTMLAtEnd();
		$this->_html .= '</body></html>';
	}
	protected function _initDataTable($id_table, $returnHTML = false, $returnAsScript = false) {
		$return = '';
		if (! $returnAsScript)
			$return .= '<script type="text/javascript">
		 		var oTable' . $id_table . ' = undefined;
		 		$jqPm(document).ready(function(){';
		$return .= 'oTable' . $id_table . ' = $jqPm(\'#' . $id_table . '\').dataTable( {
				        "sDom": \'R<"H"lfr>t<"F"ip<\',
				        "bJQueryUI": true,
				        "bStateSave": true,
				        "sPaginationType": "full_numbers",
				        "bDestory": true,
				        "oLanguage": {
				          "sLengthMenu": "' . $this->l('Display', $this->_coreClassName) . ' _MENU_ ' . $this->l('records per page', $this->_coreClassName) . '",
				          "sZeroRecords": "' . $this->l('Nothing found - sorry', $this->_coreClassName) . '",
				          "sInfo": "' . $this->l('Showing', $this->_coreClassName) . ' _START_ ' . $this->l('to', $this->_coreClassName) . ' _END_ ' . $this->l('of', $this->_coreClassName) . ' _TOTAL_ ' . $this->l('records', $this->_coreClassName) . '",
				          "sInfoEmpty": "' . $this->l('Showing', $this->_coreClassName) . ' 0 ' . $this->l('to', $this->_coreClassName) . ' 0 ' . $this->l('of', $this->_coreClassName) . ' 0 ' . $this->l('records', $this->_coreClassName) . '",
				          "sInfoFiltered": "(' . $this->l('filtered from', $this->_coreClassName) . ' _MAX_ ' . $this->l('total records', $this->_coreClassName) . ')",
				          "sPageNext": "' . $this->l('Next', $this->_coreClassName) . '",
				          "sPagePrevious": "' . $this->l('Previous', $this->_coreClassName) . '",
				          "sPageLast": "' . $this->l('Last', $this->_coreClassName) . '",
				          "sPageFirst": "' . $this->l('First', $this->_coreClassName) . '",
				          "sSearch": "' . $this->l('Search', $this->_coreClassName) . '",
						  oPaginate: {
							  "sFirst":"' . $this->l('First', $this->_coreClassName) . '",
							  "sPrevious":"' . $this->l('Previous', $this->_coreClassName) . '",
							  "sNext":"' . $this->l('Next', $this->_coreClassName) . '",
							  "sLast":"' . $this->l('Last', $this->_coreClassName) . '"
						  }
				        }
				      } );';
		if (! $returnAsScript)
			$return .= ' });</script>';
		if ($returnHTML)
			return $return;
		$this->_html .= $return;
	}
	protected function copyFromPost(&$destination, $destination_type = 'object', $data = false ) {
		$this->_preCopyFromPost();
		$clearTempDirectory = false;
		if(!$data) $data = $_POST;
		foreach ( $data as $key => $value ) {
			if (preg_match('/_temp_file$/', $key) && $value) {
				$final_destination = dirname(__FILE__) . Tools::getValue($key . '_destination');
				$final_file = $final_destination . $value;
				$temp_file = dirname(__FILE__) . $this->_temp_upload_dir . $value;
				if (self::_isRealFile($temp_file)) {
					rename($temp_file, $final_file);
				}
				$key = preg_replace('/_temp_file$/', '', $key);
				if ($old_file = Tools::getValue($key . '_old_file')) {
					if (self::_isRealFile($final_destination . Tools::getValue($key . '_old_file')))
						@unlink($final_destination . Tools::getValue($key . '_old_file'));
				}
				$clearTempDirectory = true;
			} elseif (preg_match('/_unlink$/', $key)) {
				$key = preg_replace('/_unlink$/', '', $key);
				$final_file = dirname(__FILE__) . Tools::getValue($key . '_temp_file_destination') . Tools::getValue($key . '_temp_file');
				$temp_file = dirname(__FILE__) . $this->_temp_upload_dir . Tools::getValue($key . '_temp_file');
				if (self::_isRealFile($final_file))
					@unlink($final_file);
				if (self::_isRealFile($temp_file))
					@unlink($temp_file);
				$value = '';
				$clearTempDirectory = true;
			} elseif (preg_match('/activestatus/', $key)) {
				$key = 'active';
			}elseif (preg_match('/height$|width$/i', $key)) {
				$value=trim($value);
				if(!Validate::isInt($value)){
					$value = '' ;
					continue;
				}
				$unit = (Tools::getValue($key . '_unit') == 1?'px':'%');
				$value = $value.$unit ;
			}
			if (key_exists($key, $destination))
				if($destination_type == 'object')
					$destination->{$key} = $value;
				else
					$destination[$key] = $value;
		}
		if($destination_type == 'object'){
			$rules = call_user_func(array (get_class($destination), 'getValidationRules' ), get_class($destination));
			if (sizeof($rules ['validateLang'])) {
				$languages = Language::getLanguages(false);
				foreach ( $languages as $language )
					foreach ( $rules ['validateLang'] as $field => $validation ) {
						if ((isset($data [$field . '_' . intval($language ['id_lang']) . '_temp_file_lang'])
						&& $data [$field . '_' . intval($language ['id_lang']) . '_temp_file_lang'])
						|| (isset($data [$field . '_all_lang']) && !$destination->{$field} [intval($language ['id_lang'])]
						&& $data [$field . '_all_lang']
						&& isset($data [$field . '_' . intval($this->_default_language) . '_temp_file_lang'])
						&& $data [$field . '_' . intval($this->_default_language) . '_temp_file_lang'])) {
							if(isset($data [$field . '_all_lang'])
							&& $data [$field . '_all_lang']
							&& $language ['id_lang'] != $this->_default_language) {
								$key_default_language = $field . '_' . intval($this->_default_language) . '_temp_file_lang';
								$old_file = $data[$key_default_language];
								$new_temp_file_lang = uniqid().'.'.self::_getFileExtension($data[$key_default_language]);
							}
							$key = $field . '_' . intval($language ['id_lang']) . '_temp_file_lang';
							$final_destination = dirname(__FILE__) . Tools::getValue($key . '_destination_lang');
							if(isset($data [$field . '_all_lang']) && $data [$field . '_all_lang'] && $language ['id_lang'] != $this->_default_language)  {
								$final_file = $final_destination . $new_temp_file_lang;
								$temp_file = dirname(__FILE__) . $this->_temp_upload_dir . $old_file;
							}
							else {
								$final_file = $final_destination . Tools::getValue($key);
								$temp_file = dirname(__FILE__) . $this->_temp_upload_dir . Tools::getValue($key);
							}
							if (self::_isRealFile($temp_file)) {
								copy($temp_file, $final_file);
							}
							$key = preg_replace('/_temp_file_lang$/', '', $key);
							if ($old_file = Tools::getValue($key . '_old_file_lang')) {
								if (self::_isRealFile($final_destination . Tools::getValue($key . '_old_file_lang')))
									@unlink($final_destination . Tools::getValue($key . '_old_file_lang'));
							}
							if(isset($data [$field . '_all_lang'])
							&& $data [$field . '_all_lang']
							&& $language ['id_lang'] != $this->_default_language) {
								$destination->{$field} [intval($language ['id_lang'])] = $new_temp_file_lang;
							}
							else
								$destination->{$field} [intval($language ['id_lang'])] = $_POST [$field . '_' . intval($language ['id_lang']) . '_temp_file_lang'];
							$clearTempDirectory = true;
						}
						if (isset($_POST [$field . '_' . intval($language ['id_lang']) . '_unlink_lang']) && $_POST [$field . '_' . intval($language ['id_lang']) . '_unlink_lang']) {
							$key = $field . '_' . intval($language ['id_lang']) . '_unlink_lang';
							$key = preg_replace('/_unlink_lang$/', '', $key);
							$final_file = dirname(__FILE__) . Tools::getValue($key . '_temp_file_lang_destination_lang') . Tools::getValue($key . '_old_file_lang');
							$temp_file = dirname(__FILE__) . $this->_temp_upload_dir . Tools::getValue($key . '_old_file_lang');
							if (self::_isRealFile($final_file))
								@unlink($final_file);
							if (self::_isRealFile($temp_file))
								@unlink($temp_file);
							$destination->{$field} [intval($language ['id_lang'])] = '';
							$clearTempDirectory = true;
						}
						if (isset($_POST [$field . '_' . intval($language ['id_lang'])])) {
							$destination->{$field} [intval($language ['id_lang'])] = $_POST [$field . '_' . intval($language ['id_lang'])];
						}
					}
			}
		}
		else{
			$rules = call_user_func(array($destination['class_name'], 'getValidationRules'),$destination['class_name']);
			if (sizeof($rules ['validateLang'])) {
				$languages = Language::getLanguages();
				foreach ( $languages as $language )
					foreach ( $rules ['validateLang'] as $field => $validation ) {
						if (isset($data [$field . '_' . intval($language ['id_lang']) . '_temp_file_lang']) && $_POST [$field . '_' . intval($language ['id_lang']) . '_temp_file_lang']) {
							$key = $field . '_' . intval($language ['id_lang']) . '_temp_file_lang';
							$final_destination = dirname(__FILE__) . Tools::getValue($key . '_destination_lang');
							$final_file = $final_destination . Tools::getValue($key);
							$temp_file = dirname(__FILE__) . $this->_temp_upload_dir . Tools::getValue($key);
							if (self::_isRealFile($temp_file)) {
								rename($temp_file, $final_file);
							}
							$key = preg_replace('/_temp_file_lang$/', '', $key);
							if ($old_file = Tools::getValue($key . '_old_file_lang'))
								if (self::_isRealFile($final_destination . Tools::getValue($key . '_old_file_lang')))
									@unlink($final_destination . Tools::getValue($key . '_old_file_lang'));
							$destination[$field] [intval($language ['id_lang'])] = $_POST [$field . '_' . intval($language ['id_lang']) . '_temp_file_lang'];
							$clearTempDirectory = true;
						}
						if (isset($destination [$field . '_' . intval($language ['id_lang']) . '_unlink_lang']) && $_POST [$field . '_' . intval($language ['id_lang']) . '_unlink_lang']) {
							$key = $field . '_' . intval($language ['id_lang']) . '_unlink_lang';
							$key = preg_replace('/_unlink_lang$/', '', $key);
							$final_file = dirname(__FILE__) . Tools::getValue($key . '_temp_file_lang_destination_lang') . Tools::getValue($key . '_old_file_lang');
							$temp_file = dirname(__FILE__) . $this->_temp_upload_dir . Tools::getValue($key . '_old_file_lang');
							if (self::_isRealFile($final_file))
								@unlink($final_file);
							if (self::_isRealFile($temp_file))
								@unlink($temp_file);
							$destination[$field] [intval($language ['id_lang'])] = '';
							$clearTempDirectory = true;
						}
						if (isset($destination [$field . '_' . intval($language ['id_lang'])])){
							$destination[$field] [intval($language ['id_lang'])] = $destination [$field . '_' . intval($language ['id_lang'])];
						}
					}
				}
			}
			if ($clearTempDirectory)
				$this->_clearDirectory(dirname(__FILE__) . $this->_temp_upload_dir);
			$this->_postCopyFromPost(array('destination'=>$destination));
	}
	public static function _isFilledArray($array) {
		return ($array && is_array($array) && sizeof($array));
	}
	protected function _getDuration($seconds, $use = null, $zeros = false) {
		$periods = array ($this->l('hours', $this->_coreClassName) => 3600, $this->l('minutes', $this->_coreClassName) => 60, $this->l('seconds', $this->_coreClassName) => 1);
		$seconds = (float) $seconds;
		$segments = array();
		foreach ($periods as $period => $value) {
			if ($use && strpos($use, $period[0]) === false) continue;
			$count = floor($seconds / $value);
			if ($count == 0 && !$zeros) continue;
			$segments[strtolower($period)] = $count;
			$seconds = $seconds % $value;
		}
		$string = array();
		foreach ($segments as $key => $value) {
			$segment_name = substr($key, 0, -1);
			$segment = $value . ' ' . $segment_name;
			if ($value != 1) $segment .= 's';
			$string[] = $segment;
		}
		return implode(', ', $string);
	}
	protected function _cleanOutput() {
		$this->_html = '';
		self::_cleanBuffer();
	}
	public static function _cleanBuffer() {
		if (ob_get_length() > 0) ob_clean();
	}
	protected function _echoOutput($die = false) {
		echo $this->_html;
		if ($die) die();
	}
	protected function _clearDirectory($dir) {
		if (!$dh = @opendir($dir)) return;
		while (false !== ($obj = readdir($dh))) {
			if ($obj == '.' || $obj == '..') continue;
			if (! @unlink($dir . '/' . $obj)) $this->_clearDirectory($dir . '/' . $obj);
		}
		closedir($dh);
		return;
	}
	public static function _isRealFile($filename) {
		return (file_exists($filename) && ! is_dir($filename));
	}
	protected function displayPMFlags($class = false) {
		if(!$this->styles_flag_lang_init) {
			$this->_html .= '<style type="text/css" media="all">';
			foreach ( $this->_languages as $language ) {
				$this->_html .= '.pmSelectFlag a.pmFlag_' . $language ['id_lang'].', .chosen-drop li.pmFlag_' . $language ['id_lang'].', .chosen-drop li.pmFlag_' . $language ['id_lang'].', .pmFlag_' . $language ['id_lang'].' .ui-selectmenu-status, .pmFlag_' . $language ['id_lang'].' a {background-image:url(../img/l/'.(int)($language['id_lang']).'.jpg), linear-gradient(transparent,transparent)!important; background-position:8px 4px;background-repeat:no-repeat;}
				.pmFlag_' . $language ['id_lang'].' a { background-position:center center;background-repeat:no-repeat;}';
			}
			$this->_html .= '</style>';
			$this->styles_flag_lang_init = true;
		}
		$key = uniqid();
		if ($class) $this->_html .= '<div class="' . htmlentities($class, ENT_COMPAT, 'UTF-8') . '">';
		$this->_html .= '<select id="'.$key.'" style="width:50px;" class="pmSelectFlag">';
		$currentIdLang = $this->_default_language;
		foreach ( $this->_languages as $language ) {
			$this->_html .= '<option value="' . (int)($language['id_lang']) . '" class="pmFlag_' . $language ['id_lang'].'" '.($language ['id_lang'] == $this->_default_language ? 'selected="selected"' : 'selected=""').'>&nbsp;&nbsp;</option>';
			if ($language ['id_lang'] == $this->_default_language) $currentIdLang = $this->_default_language;
		}
		$this->_html .= '</select>';
		if($class) $this->_html .= '</div>';
		if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
			$this->_html .= '<script type="text/javascript">
				$jqPm("#' . $key . '").val("'.$currentIdLang.'");
				$jqPm("#' . $key . '").unbind("change").on("change chosen:ready", function(e, p) {
					var currentIdLang = $jqPm("#' . $key . '").val();
					$jqPm(".pmFlag").hide();
					$jqPm(".pmFlagLang_"+currentIdLang).show();
					$jqPm(".pmSelectFlag").val(currentIdLang);
					$jqPm(".pmSelectFlag").trigger("click");
					$jqPm("#' . $key . '_chosen a.chosen-single").attr("class", "chosen-single").addClass("pmFlag_"+currentIdLang);
				});
				$jqPm("#' . $key . '").chosen({ disable_search: true, max_selected_options: 1, inherit_select_classes: true });
			</script>';
		} else {
			$this->_html .= '<script type="text/javascript">
			$jqPm("#' . $key . '").val("'.$currentIdLang.'");
			$jqPm("#' . $key . '").selectmenu({wrapperElement: "<div class=\'ui_select_menu_lang\' />"});
			$jqPm("#' . $key . '").unbind("change").bind("change",function() {
				var currentIdLang = $jqPm("#' . $key . '").val();
				$jqPm(".pmFlag").hide();
				$jqPm(".pmFlagLang_"+currentIdLang).show();
				$jqPm(".pmSelectFlag").val(currentIdLang);
				$jqPm(".pmSelectFlag").trigger("click");
			});
			</script>
			';
		}
		return $key;
	}
	public static function isMultishop() {
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) return true;
		return false;
	}
	public static function addSqlAssociation($table, $alias, $identifier, $inner_join = true, $on = null, $shops = false, $join_table = NULL, $active_check = false)
	{
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) {
			if ($join_table == NULL || empty($join_table)) $join_table = _DB_PREFIX_.$table.'_shop';
			else $join_table = _DB_PREFIX_.$join_table;
			if ($shops == 'all') $ids_shop = array_values(Shop::getCompleteListOfShopsID());
			else if (is_array($shops) && sizeof($shops)) $ids_shop = array_values($shops);
			else if (is_numeric($shops) ) $ids_shop = array($shops);
			else $ids_shop = array_values(Shop::getContextListShopID());
			if ($active_check) {
				foreach ($ids_shop as $key_shop=>$id_shop)
					if (!self::_isModuleActive('pm_seointernallinking', $id_shop)) unset($ids_shop[$key_shop]);
				if (!sizeof($ids_shop)) $ids_shop = array(0);
			}
			$table_alias = $alias.'_shop';
			if (strpos($table, '.') !== false) list($table_alias, $table) = explode('.', $table);
			$sql = (($inner_join) ? ' INNER' : ' LEFT').' JOIN `'.$join_table.'` '.$table_alias.'
						ON '.$table_alias.'.'.$identifier.' = '.$alias.'.'.$identifier.'
						AND '.$table_alias.'.id_shop IN ('.implode(', ', $ids_shop).') '
						.(($on) ? ' AND '.$on : '');
			return $sql;
		}
		return;
	}
	public static function addShopCondition($identifier = 'id_shop', $shops = false)
	{
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) {
			if ($shops == 'all') $ids_shop = array_values(Shop::getCompleteListOfShopsID());
			else if (is_array($shops) && sizeof($shops)) $ids_shop = array_values($shops);
			else if (is_numeric($shops) ) $ids_shop = array($shops);
			else $ids_shop = array_values(Shop::getContextListShopID());
			$sql = ' '.$identifier.' IN ('.implode(', ', $ids_shop).') ';
			return $sql;
		}
		return;
	}
	public static function addSqlGroupBy($alias, $identifier)
	{
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive()) return ' GROUP BY '.$alias.'.'.$identifier.' ';
		return;
	}
	public function showShopContextWarning() {
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=') && Shop::isFeatureActive() && sizeof(Shop::getContextListShopID()) > 1)
			$this->_showWarning($this->l('You are working on more than one shop at the same time... be careful !', $this->_coreClassName));
	}
	public static function _retroValidateController($obj) {
		if (version_compare(_PS_VERSION_, '1.5.0.0', '<')) {
			return $obj->validateControler();
		} else {
			return $obj->validateController();
		}
	}
	protected static function _getNbDaysModuleUsage() {
		$sql = 'SELECT DATEDIFF(NOW(),date_add)
				FROM '._DB_PREFIX_.'configuration
				WHERE name = \''.pSQL('PM_'.self::$_module_prefix.'_LAST_VERSION').'\'
				ORDER BY date_add ASC';
		return (int)Db::getInstance()->getValue($sql);
	}
	protected function _onBackOffice() {
		if (isset($this->_cookie->id_employee) && Validate::isUnsignedId($this->_cookie->id_employee)) return true;
		return false;
	}
	private static $_isModuleActiveCache = array();
	protected static function _isModuleActive($moduleName, $id_shop = false) {
		if (!Validate::isModuleName($moduleName)) return false;
		if (isset(self::$_isModuleActiveCache[$moduleName.(int)$id_shop])) return self::$_isModuleActiveCache[$moduleName.(int)$id_shop];
		$active = false;
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
			$id_module = Db::getInstance()->getValue('SELECT `id_module` FROM `'._DB_PREFIX_.'module` WHERE `name` = \''.pSQL($moduleName).'\'');
			if ($id_module) {
				if (!$id_shop) $id_shop = (int)Context::getContext()->shop->id;
				if (Db::getInstance()->getValue('SELECT `id_module` FROM `'._DB_PREFIX_.'module_shop` WHERE `id_module` = '.(int)$id_module.' AND `id_shop` = '.(int)$id_shop))
					$active = true;
			}
		} else {
			$obj = Module::getInstanceByName($moduleName);
			if (is_object($obj) && isset($obj->active)) $active = $obj->active;
		}
		self::$_isModuleActiveCache[$moduleName.(int)$id_shop] = $active;
		return $active;
	}
}
?>
