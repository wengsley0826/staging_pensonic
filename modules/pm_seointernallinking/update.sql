ALTER TABLE `PREFIX_pm_seointernallinking` MODIFY COLUMN `expression_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin AFTER `id_group`;

CREATE TABLE IF NOT EXISTS `PREFIX_pm_seointernallinking_group_shop` (
`id_group`  int(11) unsigned NOT NULL COMMENT 'ID Group' ,
`id_shop`  int(10) unsigned NOT NULL COMMENT 'ID Shop' ,
PRIMARY KEY (`id_group`, `id_shop`),
INDEX `id_group` (`id_group`, `id_shop`)
) ENGINE=MYSQL_ENGINE DEFAULT CHARSET=utf8;