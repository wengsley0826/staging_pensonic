<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPConfigController extends PBPControllerCore
{

	public function __construct($sibling)
	{
		parent::__construct($sibling);
		// @Todo: load only when required
		$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/lib/popup.js');
		$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/lib/tools.js');

		$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/lib/tools.css');
		$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/lib/popup.css');
	}

	public function route()
	{
		$return = '';
		switch (Tools::getValue('route'))
		{
			case 'pbpconfigtabscontroller' :
				$config_tabs_controller = new PBPConfigTabsController($this->sibling);
				die($config_tabs_controller->route());

			case 'pbpconfiggeneralcontroller' :
				$config_general_controller = new PBPConfigGeneralController($this->sibling);
				die($config_general_controller->route());

			default:
				$config_tabs_controller = new PBPConfigTabsController($this->sibling);
				$config_general_controller = new PBPConfigGeneralController($this->sibling);

				$return .= $config_general_controller->route();
				$return .= $config_tabs_controller->route();
				return $return;
				break;
		}
	}

}