<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPConfigGeneralController extends PBPControllerCore
{
	protected $sibling;

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);

		if ($sibling !== null)
			$this->sibling = &$sibling;

		// @Todo: load only when required
		$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/admin/config/general.js');
	}

	public function renderForm()
	{
		$pbp_general = array();
		$pbp_general['pbp_quickview_enabled'] = Configuration::get('pbp_quickview_enabled');
		$pbp_general['pbp_parent_attr_display'] = Configuration::get('pbp_parent_attr_display');
		$pbp_general['pbp_location'] = Configuration::get('pbp_location');
		$pbp_general['pbp_display_bundle_total'] = Configuration::get('pbp_display_bundle_total');

		Context::getContext()->smarty->assign(array(
			'module_config_url' => $this->module_config_url,
			'pbp_general' => $pbp_general
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/general.tpl');
	}

	public function processForm()
	{
		Configuration::updateValue('pbp_quickview_enabled', Tools::getValue('pbp_quickview_enabled'));
		Configuration::updateValue('pbp_parent_attr_display', Tools::getValue('pbp_parent_attr_display'));
		Configuration::updateValue('pbp_location', Tools::getValue('pbp_location'));
		Configuration::updateValue('pbp_display_bundle_total', Tools::getValue('pbp_display_bundle_total'));
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'processform' :
				die($this->processForm());

			default:
				return ($this->renderForm());
		}
	}

}