<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPConfigTabsController extends PBPControllerCore
{
	protected $sibling;

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);

		if ($sibling !== null)
			$this->sibling = &$sibling;

		// @Todo: load only when required
		$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/admin/config/tabs.js');
	}

	public function renderList()
	{
		$tabs_collection = (new PBPTabLangModel())->loadAll(Context::getContext()->language->id);

		Context::getContext()->smarty->assign(array(
			'tabs' => $tabs_collection,
			'module_config_url' => $this->module_config_url,
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/tabs_list.tpl');
	}

	public function renderForm()
	{
		$languages = Language::getLanguages();
		$title_localised_array = array();

		if (Tools::getValue('id_tab') != '')
		{
			foreach ($languages as $language)
			{
				$pbp_tab_model = new PBPTabLangModel();
				$pbp_tab_model->load(Tools::getValue('id_tab'), $language['id_lang']);
				$title_localised_array[$language['id_lang']] = $pbp_tab_model->title;
			}
		}
		else 
		{
			foreach ($languages as $language)
				$title_localised_array[$language['id_lang']] = '';
		}

		Context::getContext()->smarty->assign(array(
			'languages' => $languages,
			'id_lang_default' => Configuration::get('PS_LANG_DEFAULT', null, Context::getContext()->shop->id_shop_group, Context::getContext()->shop->id),
			'module_config_url' => $this->module_config_url,
			'id_tab' => Tools::getValue('id_tab'),
			'title_localised_array' => $title_localised_array
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/tab_add.tpl');
	}

	public function processForm()
	{
		$languages = Language::getLanguages();

		$ajax_response = new PBPAjaxResponse();
		$id_lang_default = Configuration::get('PS_LANG_DEFAULT');

		if (Tools::getValue('title_'.$id_lang_default) == '')
			$ajax_response->addMessage('Missing tab name', '#title_'.$id_lang_default, true);

		if ($ajax_response->hasErrors())
			$ajax_response->ajaxDie();

		if (Tools::getValue('id_tab') == 0)
		{
			$id_tab = PBPTabLangModel::getNextTabId();
			foreach ($languages as $language)
			{
				$tab_lang_model = new PBPTabLangModel();
				$tab_lang_model->id_tab = $id_tab;
				$tab_lang_model->id_lang = $language['id_lang'];

				if (Tools::getValue('title_'.(int)$language['id_lang']) != '')
					$tab_lang_model->title = Tools::getValue('title_'.(int)$language['id_lang']);
				else
					$tab_lang_model->title = Tools::getValue('title_'.(int)$id_lang_default);

				$tab_lang_model->add();
			}
		}
		else
		{
			$id_tab = (int)Tools::getValue('id_tab');
			if ($id_tab == 0) return false;

			foreach ($languages as $language)
			{
				$tab_lang_model = new PBPTabLangModel();
				$tab_lang_model->id_tab = $id_tab;
				$tab_lang_model->id_lang = $language['id_lang'];

				if (Tools::getValue('title_'.(int)$language['id_lang']) != '')
					$tab_lang_model->title = Tools::getValue('title_'.(int)$language['id_lang']);
				else
					$tab_lang_model->title = Tools::getValue('title_'.(int)$id_lang_default);

				$tab_lang_model->updateTitle();
			}
		}
	}

	public function remove()
	{
		if ((int)Tools::getValue('id_tab') == 0) return false;
		PBPTabLangModel::deleteTab(Tools::getValue('id_tab'));
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderform' :
				return ($this->renderForm());

			case 'processform' :
				die($this->processForm());

			case  'remove' :
				die($this->remove());

			default:
				return ($this->renderList());
		}
	}

}