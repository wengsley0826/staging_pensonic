<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPAdminProductTabController extends PBPControllerCore
{
	public $pbp_admin_general_controller;

	public function __construct($sibling)
	{
		$this->sibling = $sibling;
		//$this->setModuleBaseAdminUrl();
		$this->base_url = Tools::getShopProtocol().Tools::getShopDomain().__PS_BASE_URI__;
		$this->key_tab = 'ModuleProductbundlespro';
		$this->pbp_admin_bundles_controller = new PBPAdminTabBundlesController($this->sibling);
		$this->pbp_admin_general_controller = new PBPAdminProductTabGeneralController($this->sibling);
		$this->pbp_admin_bundles_existing_controller = new PBPAdminTabBundlesExistingController($this->sibling);
	}

	public function setMedia()
	{
		if (Tools::getValue('controller') == 'AdminProducts')
		{
			$this->_addCSS();
			$this->_addJS();
		}
	}

	private function _addJS()
	{
		Context::getContext()->controller->addJS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/js/lib/popup.js');
		Context::getContext()->controller->addJS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/js/lib/tools.js');
		Context::getContext()->controller->addJS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/js/lib/mpproductsearchwidget.js');
		Context::getContext()->controller->addJS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/js/admin/producttab/PBPProductTabBundleExistingController.js');
		Context::getContext()->controller->addJS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/js/admin/producttab/PBPProductTabBundlesController.js');
	}

	private function _addCSS()
	{
		Context::getContext()->controller->addCSS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/css/lib/tools.css');
		Context::getContext()->controller->addCSS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/css/lib/popup.css');
		Context::getContext()->controller->addCSS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/css/lib/popup.css');
		Context::getContext()->controller->addCSS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/css/admin/producttab.css');
	}

	public function route()
	{
		$return = '';
		switch (Tools::getValue('route'))
		{
			case 'pbpadmintabbundlescontroller' :
				die($this->pbp_admin_bundles_controller->route());

			case 'pbpadmintabbundlesexistingcontroller' :
				die($this->pbp_admin_bundles_existing_controller->route());

			case 'pbpadmintabgeneralcontroller' :
				die($this->pbp_admin_general_controller->route());

			default :
				$return .= $this->pbp_admin_general_controller->route();
				$return .= $this->pbp_admin_bundles_controller->route();
				return $return;
		}
	}

}