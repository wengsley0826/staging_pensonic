<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPAdminProductTabGeneralController extends PBPControllerCore
{
	public function setMedia()
	{
		if (Tools::getValue('controller') == 'AdminProducts')
			$this->_addJS();
	}

	private function _addJS()
	{
		Context::getContext()->controller->addJS($this->sibling->base_url.'modules/'.$this->sibling->name.'/views/js/admin/producttab/PBPProductTabGeneralController.js');
	}

	public function renderForm()
	{
		$pbp_product_option = new PBPProductOptionModel();
		$pbp_product_option->load(Tools::getValue('id_product'));

		Context::getContext()->smarty->assign(array(
			'pbp_product_option' => $pbp_product_option,
			'module_tab_url' => $this->module_tab_url,
			'module_url' => $this->module_url			
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/general.tpl');
	}

	public function processForm()
	{
		$product_option_model = new PBPProductOptionModel();
		$product_option_model->load(Tools::getValue('id_product'));
		$product_option_model->id_product = (int)Tools::getValue('id_product');
		$product_option_model->disabled_addtocart = (int)Tools::getValue('disabled_addtocart');
		$product_option_model->save();
	}

	public function route()
	{
		switch (Tools::getValue('action')) 
		{
			case 'processform' :
				die($this->processForm());

			default :
				return $this->renderForm();
		}
	}


}