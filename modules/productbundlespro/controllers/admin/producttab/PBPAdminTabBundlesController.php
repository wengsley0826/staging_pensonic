<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPAdminTabBundlesController extends PBPControllerCore
{
	public function renderList()
	{
		$product_bundles = new PBPBundleModel();
		$pbp_bundles = $product_bundles->getByProduct(Tools::getValue('id_product'));

		Context::getContext()->smarty->assign(array(
			'module_tab_url' => $this->module_tab_url,
			'module_url' => $this->module_url,
			'product_bundles' => $pbp_bundles
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/bundles.tpl');
	}

	public function renderForm()
	{
		$tabs_model = new PBPTabLangModel();
		$tabs_collection = $tabs_model->loadAll(Context::getContext()->language->id);

		if ((int)Tools::getValue('id_bundle') > 0) {
			$pbp_bundle_model = new PBPBundleModel();
			$pbp_bundle_model->loadSingle(Tools::getValue('id_bundle'), Tools::getValue('id_product'));
			$bundle = $pbp_bundle_model;
		}		
		else {
			$bundle = new PBPBundleModel();
		}


		Context::getContext()->smarty->assign(array(
			'module_url' => $this->module_url,
			'tabs_collection' => $tabs_collection,
			'bundle' => $bundle
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/bundles_add.tpl');
	}

	public function getBundleProductData() {
		$pbp_product_model = new PBPProductModel();
		$pbp_product_model->load(Tools::getValue('id_product'), Tools::getValue('id_bundle'));

		$product = new Product(Tools::getValue('id_product'), false, Context::getContext()->language->id);

		$pbp_product_model->product_name = $product->name;
		return Tools::jsonEncode($pbp_product_model);
	}

	public function processForm()
	{
		// new bundle
		if ((int)Tools::getValue('id_bundle') == 0) {
			$bundle = new PBPBundleModel();
			$bundle->id_bundle = $bundle->getMewBundleID();
			$bundle->id_product = Tools::getValue('id_product');
			$bundle->id_tab = Tools::getValue('id_tab');
			$bundle->enabled = (int)Tools::getValue('enabled');
			$bundle->add();
		}
		else {
			$bundle = new PBPBundleModel();
			$bundle->loadSingle(Tools::getValue('id_bundle'), Tools::getValue('id_product'));
			$bundle->id_tab = Tools::getValue('id_tab');
            $bundle->enabled = (int)Tools::getValue('enabled');
			$bundle->update();
		}
		// Add the bundle products

		PBPProductModel::deleteBundleProducts($bundle->id_bundle);

		$post_bundle_products = Tools::getValue('bundle_products');

		if (is_array($post_bundle_products))
		{
			foreach ($post_bundle_products as $bundle_product)
			{
				$pbp_product = new PBPProductModel();
				$pbp_product->id_bundle = $bundle->id_bundle;
				$pbp_product->id_product = $bundle_product['id_product'];
				$pbp_product->discount_type = $bundle_product['discount_type'];
				$pbp_product->discount_amount = $bundle_product['discount_amount'];
				//$pbp_product->discount_tax = $bundle_product['discount_tax'];
				$pbp_product->discount_tax = 'tax_ex';
				$pbp_product->add();
			}
		}

		$return = array();
		$return['id_bundle'] = $bundle->id_bundle;
		return Tools::jsonEncode($return);
	}

	public function renderProductList()
	{
		$pbp_product = new PBPProductModel();
		$products = $pbp_product->loadByBundle(Tools::getValue('id_bundle'), Context::getContext()->language->id);

		Context::getContext()->smarty->assign(array(
			'products' => $products
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/bundles_productlist.tpl');
	}

	public function deleteProduct()
	{
		PBPProductModel::deleteBundleProducts(Tools::getValue('id_bundle'), Tools::getValue('id_product'));
	}

	public function deleteBundle()
	{
		PBPProductModel::deleteBundleProducts(Tools::getValue('id_bundle'));
		$bundle = new PBPBundleModel();
		$bundle->loadSingle(Tools::getValue('id_bundle'), Tools::getValue('id_product'));
		$bundle->delete();
	}


	public function copyBundle()
	{
		$bundleOld = new PBPBundleModel();
		$bundleOld = $bundleOld->load(Tools::getValue('id_bundle'));
		$bundleOld = $bundleOld[0];
	
		$bundle = new PBPBundleModel();
		$bundle->id_bundle = $bundle->getMewBundleID();
		$bundle->id_product = $bundleOld->id_product;
		$bundle->id_tab = $bundleOld->id_tab;
		$bundle->enabled = $bundleOld->enabled;
		$bundle->add();
		
		
		$pbp_productOld = new PBPProductModel();
		$products = $pbp_productOld->loadByBundle(Tools::getValue('id_bundle'), Context::getContext()->language->id);
		foreach($products as $oneProduct){
			$pbp_product = new PBPProductModel();
			$pbp_product->id_bundle = $bundle->id_bundle;
			$pbp_product->id_product = $oneProduct->id_product;
			$pbp_product->discount_type = $oneProduct->discount_type;
			$pbp_product->discount_amount = $oneProduct->discount_amount;
			$pbp_product->discount_tax = 'tax_ex';
			$pbp_product->add();
		}
	}
	
	
	
	public function exportBundle()
	{
		$bundleOld = new PBPBundleModel();
		$bundleOld = $bundleOld->load(Tools::getValue('id_bundle'));
		$bundleOld = $bundleOld[0];
	
		$pbp_productOld = new PBPProductModel();
		$products = $pbp_productOld->loadByBundle(Tools::getValue('id_bundle'), Context::getContext()->language->id);
		$productsInnerLoop = $pbp_productOld->loadByBundle(Tools::getValue('id_bundle'), Context::getContext()->language->id);
		
		foreach($products as $oneProduct){
			
			// make new bundle, tied to this product
			$bundle = new PBPBundleModel();
			$bundle->id_bundle = $bundle->getMewBundleID();
			$bundle->id_product = $oneProduct->id_product;
			$bundle->id_tab = $bundleOld->id_tab;
			$bundle->enabled = $bundleOld->enabled;
			$bundle->add();
		
			foreach($productsInnerLoop as $oneProductInner){
				
				$pbp_product = new PBPProductModel();
				$pbp_product->id_bundle = $bundle->id_bundle;
				
				// if the product is same as the current loop's product, replace with the original product
				if ($oneProductInner->id_product == $oneProduct->id_product )
					$pbp_product->id_product = $bundleOld->id_product;
				else
					$pbp_product->id_product = $oneProductInner->id_product;
				
				$pbp_product->discount_type = $oneProductInner->discount_type;
				$pbp_product->discount_amount = $oneProductInner->discount_amount;
				$pbp_product->discount_tax = 'tax_ex';
				$pbp_product->add();
			}
		}

		
	}
	
	
	
	public function processBundlePositions()
	{
		$position = 0;
		foreach (Tools::getValue('bundle_positions') as $post_bundle_id)
		{
			$pbp_bundle = new PBPBundleModel($post_bundle_id);
			$pbp_bundle->position = $position;
			$position ++;
			$pbp_bundle->update();
		}
	}

	public function processBundleProductPositions()
	{
		$position = 0;
		foreach (Tools::getValue('bundle_product_positions') as $post_product_bundle_id)
		{
			$pbp_product = new PBPProductModel($post_product_bundle_id);
			$pbp_product->position = $position;
			$position++;
			$pbp_product->update();
		}
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderlist' :
				die($this->renderList());

			case 'renderproductlist' :
				die($this->renderProductList());

			case 'renderform' :
				die($this->renderForm());

			case 'getproductbundledata' :
				die ($this->getBundleProductData());

			case 'processform' :
				die($this->processForm());

			case 'deleteproduct' :
				die($this->deleteProduct());

			case 'deletebundle' :
				die($this->deleteBundle());

			case 'copybundle' :
				die($this->copyBundle());

			case 'exportbundle' :
				die($this->exportBundle());
				
			case 'processbundlepositions' :
				die($this->processBundlePositions());

			case 'processbundleproductpositions' :
				die($this->processBundleProductPositions());

			default :
				return $this->renderList();
		}
	}
}