<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPAdminTabBundlesExistingController extends PBPControllerCore
{
	public function renderForm()
	{
		$tabs_model = new PBPTabLangModel();
		$tabs_collection = $tabs_model->loadAll(Context::getContext()->language->id);

		Context::getContext()->smarty->assign(array(
			'module_tab_url' => $this->module_tab_url,
			'module_url' => $this->module_url,
			'tabs_collection' => $tabs_collection
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/bundles_existing.tpl');
	}

	public function renderProductBundles()
	{
		$pbp_bundle_model = new PBPBundleModel();
		$bundles = $pbp_bundle_model->getByProduct(Tools::getValue('id_product'));

		Context::getContext()->smarty->assign(array(
			'module_tab_url' => $this->module_tab_url,
			'module_url' => $this->module_url,
			'bundles' => $bundles
		));

		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/bundles_existing_list.tpl');
	}

	public function processAddBundle()
	{
		$pbp_bundle_model = new PBPBundleModel();
		$pbp_bundle_model->loadSingle(Tools::getValue('id_bundle'), Tools::getValue('id_product'));

		$pbp_bundle_model->id_bundle = (int)Tools::getValue('id_bundle');
		$pbp_bundle_model->id_product = (int)Tools::getValue('id_product');
		$pbp_bundle_model->id_tab = (int)Tools::getValue('id_tab');
		$pbp_bundle_model->position = 0;
		$pbp_bundle_model->enabled = 1;
		$pbp_bundle_model->save();
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderform':
				return $this->renderForm();
			case 'renderproductbundles' :
				return $this->renderProductBundles();
			case 'processaddbundle' :
				return $this->processAddBundle();
		}
	}
}