<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPCartController extends PBPControllerCore
{

	protected $sibling;

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);
		if ($sibling !== null)
			$this->sibling = &$sibling;
	}

	public function setMedia()
	{
		if (Context::getContext()->controller->php_self == 'order')
			$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/front/cart.js');
	}

	private static function _addTax($taxManager, $price_display, $use_tax, $price)
	{
		// for price display: 0 = Display Tax Incl. 1 = Display Tax Ecl.
		if ($use_tax)
			return $taxManager->addTaxes($price);

		if (!$use_tax || empty($use_tax))
			return $price;
	}


	/**
	 * @param $price_original
	 * @param $discount_type
	 * @param $discount_amount
	 * @param $allowed_discount_qty
	 * @return mixed
	 */
	public function getDiscountedPrice($price_original, $discount_type, $discount_amount, $qty, $allowed_discount_qty, $params, $product_tax_calculator, $price_display)
	{
		switch ($discount_type)
		{
			case 'percentage' :
				//$discounted_price = $price_original - (($discount_amount / 100) * $price_original);
				$discounted_price = ($price_original * $allowed_discount_qty) - (($discount_amount / 100) * ($price_original * $allowed_discount_qty));
				$non_discounted_price = ($qty - $allowed_discount_qty) * $price_original;
				return ($discounted_price + $non_discounted_price) / $qty;

			case 'money' :
				if (Context::getContext()->currency->id != Configuration::get('PS_CURRENCY_DEFAULT')) {
					$discount_amount = Tools::convertPrice($discount_amount, Context::getContext()->currency->id);
				}
                if ($params['use_tax'] == 0) {
                    $discounted_price = ($price_original * $allowed_discount_qty) - ($discount_amount * $allowed_discount_qty);
                } else {
                    $discount_amount = self::_addTax($product_tax_calculator, $price_display, $params['use_tax'], $discount_amount);
                    $discounted_price = ($price_original * $allowed_discount_qty) - ($discount_amount * $allowed_discount_qty);
                }
				$non_discounted_price = ($qty - $allowed_discount_qty) * $price_original;
				return ($discounted_price + $non_discounted_price) / $qty;
		}
	}

	private function getIdAddress()
    {
        // start get id_address from cart
        $id_address = null;

        if (isset($this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
            && $this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')} > 0) {
            $id_address = $this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        }

        if (empty($id_address) && !empty($id_customer)) {
            $addresses = $this->context->customer->getAddresses($this->context->language->id);
            if (!empty($addresses) && isset($addresses[0])) {
                $id_address = $addresses[0]['id_address'];
            }
        }
        return $id_address;
    }	

	public function hookPbpPriceCalculation($params)
	{
    	static $address = null;
		static $context = null;

        // set up tax calculator
        if ($address === null) {
            $address = new Address($this->getIdAddress());
		}
		
        $address->id_country = $params['id_country'];
        $address->id_state = $params['id_state'];
        $address->postcode = $params['zipcode'];

        $tax_manager = TaxManagerFactory::getManager($address, Product::getIdTaxRulesGroupByIdProduct((int)$params['id_product'], $context));
        $product_tax_calculator = $tax_manager->getTaxCalculator();

		if (isset($this->context->cookie->id_customer)) {
            $price_display = (int)Product::getTaxCalculationMethod((int)$this->context->cookie->id_customer);
        }
		else {
            $price_display = (int)Product::getTaxCalculationMethod();
        }
		
		if (Cache::getInstance()->retrieve("product-pbCart".$params['id_product'])){
			$cart_product_info=Cache::getInstance()->retrieve("product-pbCart".$params['id_product']);
		}else{
			$cart_product_info = new Product($params['id_product'], true);
			Cache::getInstance()->store("product-pbCart".$params['id_product'],$cart_product_info);
		}
		$cart_product = new PBPCartProductModel();
		$cart_product->load($params['id_cart'], $params['id_product'], $params['id_product_attribute'], Context::getContext()->shop->id);

		/* is parent product in the basket ? */
		$cart_product->load($params['id_cart'], $params['id_product'], $params['id_product_attribute'], Context::getContext()->shop->id);
    
    	//if (!empty($cart_product->id_parent_pbp_product) && !empty($cart_product->id_parent_pbp_product_ipa))
		if (!empty($cart_product->id_parent_pbp_product))
		{
			$cart_parent_product = new PBPCartProductModel();
			$cart_parent_product->load($params['id_cart'], $cart_product->id_parent_pbp_product, $cart_product->id_parent_pbp_product_ipa, Context::getContext()->shop->id);

			// get bundle based on parent product, to check if all products of bundle are in the cart
			$bundle_model = new PBPBundleModel();
			$bundle = $bundle_model->getByID($cart_product->id_pbp_bundle, true);
			$found_all = false;

			if (!empty($bundle[0]))
			{
				$bundle = $bundle[0];

				foreach ($bundle->products as $bundle_product)
				{
					$bundle_product->id_product;
					$cart_products = PBPCartProductModel::getCartProductsLight($params['id_cart']);

					$found_all = false;
					foreach ($cart_products as $c_product)
					{
						if ($c_product['id_product'] == $bundle_product->id_product)
							$found_all = true;
					}
					if (!$found_all) break;
				}
			}


			if (!empty($cart_parent_product->id) && $found_all)
			{
				$allowed_discount_qty = $cart_parent_product->quantity;

				if ($allowed_discount_qty > $cart_product->quantity)
					$allowed_discount_qty = $cart_product->quantity;

				$pbp_product = new PBPProductModel();
				$pbp_product->load($cart_product->id_product, $cart_product->id_pbp_bundle, Context::getContext()->shop->id);

				/*if ($params['use_tax'] && $pbp_product->discount_type == 'money') {
                    $pbp_product->discount_amount = $pbp_product->discount_amount * (1+($cart_product_info->tax_rate / 100));
                    //$pbp_product->discount_amount = $pbp_product->discount_amount * (1+($cart_product_info->tax_rate / 100));
                }*/

				$price_discounted = $this->getDiscountedPrice($params['price'], $pbp_product->discount_type, $pbp_product->discount_amount, $cart_product->quantity, $allowed_discount_qty, $params, $product_tax_calculator, $price_display);

				return $price_discounted;
			}
		}

		return $params['price'];
	}


	/*
	 * Ensures Discount badge is displayed for items in the cart which are part of a bundle and have been discounted
	 */
	public function addDiscountFlagToCartProducts($cart_products)
	{
		foreach ($cart_products as &$product)
		{
			// does this product belong to a bundle?
			$pbp_cart_product = new PBPCartProductModel();
			$pbp_cart_product->load(Context::getContext()->cart->id, $product['id_product'], $product['id_product_attribute'], Context::getContext()->shop->id);

			if ($pbp_cart_product->id_pbp_bundle > 0 && $pbp_cart_product->id_parent_pbp_product > 0)
			{
				$product['is_discounted'] = 1;
				$product['reduction_applies'] = 1;
			}
		}
		return $cart_products;
	}

}