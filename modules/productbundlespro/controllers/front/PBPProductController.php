<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPProductController extends PBPControllerCore
{
	protected $sibling;

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);
		if ($sibling !== null) {
            $this->sibling = &$sibling;
        }
	}

	public function setMedia()
	{
		if (Context::getContext()->controller->php_self == 'product')
		{
			$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/front/product.js');
			$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front/product.css');
		}
	}

	private function _applyDiscount($price, $amount, $type)
	{
		if ($type == 'percentage') {
			$price = $price - (($price / 100) * $amount);
		}			

		if ($type == 'money') {
			if (Context::getContext()->currency->id == Configuration::get('PS_CURRENCY_DEFAULT')) {
				$price = $price - $amount;
			} else {
				$amount = Tools::convertPrice($amount, Context::getContext()->currency->id);
				$price = $price - $amount;
			}
		}
		return $price;
	}

	public function hookDisplayFooter($params)
	{
		$pbp_product_options = new PBPProductOptionModel();

		$pbp_general = array();
		$pbp_general['pbp_location'] = Configuration::get('pbp_location');

		if (Tools::getValue('id_product') != '')
			$pbp_product_options->load(Tools::getValue('id_product'));

		$this->sibling->smarty->assign(array(
			//'tabs_collection' => $tabs_collection,
			'pbp_product_options' => $pbp_product_options,
			'pbp_general' => $pbp_general
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/front/hook_display_footer.tpl');
	}

	public function hookRenderTabs($params)
	{
		$usetax = (Product::getTaxCalculationMethod((int)$this->context->customer->id) != PS_TAX_EXC);

		$pbp_general = array();
		$pbp_general['pbp_location'] = Configuration::get('pbp_location');
		$pbp_general['pbp_parent_attr_display'] = Configuration::get('pbp_parent_attr_display');
		$pbp_general['pbp_display_bundle_total'] = Configuration::get('pbp_display_bundle_total');

		$id_lang = Context::getContext()->language->id;

		$tabs_collection = array();

		$bundle = new PBPBundleModel();
		$bundles_collection = $bundle->getByProduct(Tools::getValue('id_product'), true);

		foreach ($bundles_collection as $bundle)
		{
			$tab = new PBPTabLangModel();
			$tab->load($bundle->id_tab, $id_lang);

			if (!empty($tab->id))
			{
				if (empty($tabs_collection[$tab->id]))
				{
					$tab->bundles[] = $bundle;
					$tabs_collection[$tab->id] = $tab;
				}
				else
					$tabs_collection[$tab->id]->bundles[] = $bundle;
			}
		}
		$parent_product = new Product(Tools::getValue('id_product'), true, $this->context->language->id, $this->context->shop->id);
		$parent_product->link_rewrite = $parent_product->link_rewrite[Context::getContext()->language->id];
		$parent_cover_image = $parent_product->getCover($parent_product->id);
		$parent_product->cover_image = $parent_cover_image['id_image'];

		$parent_product_price = Product::getPriceStatic(Tools::getValue('id_product'), $usetax, Product::getDefaultAttribute(Tools::getValue('id_product')));

		/* Assign Images and attributes to each of the child products in each bundle */
		$link = new Link();
		if (!empty($tabs_collection))
		{
			foreach ($tabs_collection as $tab)

				foreach ($tab->bundles as $bundle)
				{
					$bundle->available = true;
					$bundle_price_discounted = $parent_product_price;
					$bundle_price_original = $parent_product_price;

					if (!empty($bundle->products) && is_array($bundle->products))
						foreach ($bundle->products as &$bundle_product)
						{
							$product = new Product($bundle_product->id_product, true, $this->context->language->id, $this->context->shop->id);

							if (!$product->active || !$product->available_for_order || $product->quantity == 0)
								$bundle->available = false;

							$product->id_product_attribute = Product::getDefaultAttribute($product->id);

							/* get Image */
							$cover_image = $product->getCover($product->id);
							$bundle_product->id_image = $cover_image['id_image'];
							$bundle_product->link_rewrite = $product->link_rewrite;
							$bundle_product->name = $product->name;
							$bundle_product->url = $link->getProductLink($product);

							$attributes_groups = $product->getAttributesGroups($this->context->language->id);
							$groups = array();

							foreach ($attributes_groups as $attribute_group) {
								if ($attribute_group['quantity'] > 0) {
									$ipa = $attribute_group['id_product_attribute'];
									$groups[$ipa]['id_product_attribute'] = $ipa;
									if (!isset($groups[$ipa]['label']))
										$groups[$ipa]['label'] = $attribute_group['public_group_name'].':'.$attribute_group['attribute_name'].', ';
									else
										$groups[$ipa]['label'] .= $attribute_group['public_group_name'].':'.$attribute_group['attribute_name'].', ';

									$groups[$ipa]['label'] = rtrim($groups[$ipa]['label'], ', ');

									if ($attribute_group['default_on'])
										$groups[$ipa]['default'] = '1';
									else
										$groups[$ipa]['default'] = '0';
								}
                                $groups[$ipa]['label'] = rtrim($groups[$ipa]['label'], ', ');
							}

							$bundle_product->attribute_groups = $groups;
							$bundle_product->product->id_product = $bundle_product->id_product;
							$bundle_product->product->price = Product::getPriceStatic($bundle_product->product->id_product, false);

							if ($bundle_product->discount_type == 'money')
								$bundle_product->product->discount_price = $bundle_product->product->price - $bundle_product->discount_amount;

							if ($bundle_product->discount_type == 'percentage')
								$bundle_product->product->discount_price = $bundle_product->product->price * (1 - ($bundle_product->discount_amount / 100));

							$bundle_product->product->discount_saving = $bundle_product->product->price - $bundle_product->product->discount_price;

							if ($usetax) {
								$bundle_product->product->discount_price = $bundle_product->product->discount_price * (1+($product->tax_rate / 100));
								$bundle_product->product->discount_saving = $bundle_product->product->discount_saving * (1 + ($product->tax_rate / 100));
							}

							$bundle_price_discounted += $bundle_product->product->discount_price;
							$bundle_price_original += $bundle_product->product->price;
						}

					$bundle->bundle_price_discounted = $bundle_price_discounted;
					$bundle->bundle_price_original = $bundle_price_original;
				}
		}


		/* clean up tabs, remove bundles with no products */
		$tabs_collection_final = array();
		foreach ($tabs_collection as $tab)
		{
			$bundles = array();
			foreach ($tab->bundles as $bundle)
			{
				if (count($bundle->products) > 0 && $bundle->available)
					$bundles[] = $bundle;
			}
			$tabs_collection_final[$tab->id_pbp_tab] = $tab;
			$tabs_collection_final[$tab->id_pbp_tab]->bundles = $bundles;
		}

		/* Add attributes to parent product if we need to */
		$attributes_groups = array();
		if ($pbp_general['pbp_parent_attr_display'])
		{
			$attributes_groups = $parent_product->getAttributesGroups($this->context->language->id);
			$groups = array();

			foreach ($attributes_groups as $attribute_group)
			{
				if ($attribute_group['quantity'] > 0)
				{
					$ipa = $attribute_group['id_product_attribute'];
					$groups[$ipa]['id_product_attribute'] = $ipa;
					if (!isset($groups[$ipa]['label']))
						$groups[$ipa]['label'] = $attribute_group['public_group_name'].':'.$attribute_group['attribute_name'].', ';
					else
						$groups[$ipa]['label'] .= $attribute_group['public_group_name'].':'.$attribute_group['attribute_name'].', ';

					$groups[$ipa]['label'] = rtrim($groups[$ipa]['label'], ', ');						

					if ($attribute_group['default_on'])
						$groups[$ipa]['default'] = '1';
					else
						$groups[$ipa]['default'] = '0';
				}
			}
			$parent_product->attribute_groups = $groups;
		}

		$this->sibling->context->smarty->assign(array(
			'tabs_collection' => $tabs_collection_final,
			'parent_product' => $parent_product,
			'id_lang' => Context::getContext()->language->id,
			'pbp_general' => $pbp_general
		));

		if ($pbp_general['pbp_location'] == 'description-after')
			return $this->sibling->display($this->sibling->module_file, 'views/templates/front/hook_product_tabs_compact.tpl');
		else {
			if ($pbp_general['pbp_parent_attr_display']) {
                return $this->sibling->display($this->sibling->module_file, 'views/templates/front/hook_product_tabs_attr.tpl');
            }
			else {
                return $this->sibling->display($this->sibling->module_file, 'views/templates/front/hook_product_tabs.tpl');
            }
		}
	}

	/**
	 * Gets the total bundle price and bundle savings information to update front end (ajax request when attributes change)
	 */
	public function getBundlePrices()
	{
		$json_return = array();
		$json_return['bundle_products'] = array();

		$id_product_parent = (int)Tools::getValue('pbp_cart_parent_product')['id_product'];
		$ipa_parent = (int)Tools::getValue('pbp_cart_parent_product')['ipa'];

		// get price of parent product
		$product = new Product(Tools::getValue('pbp_cart_parent_product')['id_product'], true);
		$price = Product::getPriceStatic($id_product_parent, false, $ipa_parent);

		if (Tools::getValue('pbp_cart_products') == '') {
		    return false;
        }

		// get prices of products in the bundle along with discount information
		foreach (Tools::getValue('pbp_cart_products') as $bundle_product_post)
		{
			$bundle_price = 0.00;
			$bundle_product = new Product((int)$bundle_product_post['id_product']);
			$bundle_price = Product::getPriceStatic($bundle_product_post['id_product'], false, $bundle_product_post['ipa']);
			$bundle_price_original = $bundle_price;

			// apply discount
			$pbp_bundle_product = new PBPProductModel();
			$pbp_bundle_product->load($bundle_product_post['id_product'], (int)Tools::getValue('id_bundle'), Context::getContext()->shop->id);

			if ($pbp_bundle_product->discount_type == 'money')
				$bundle_price = $bundle_price - $pbp_bundle_product->discount_amount;

			if ($pbp_bundle_product->discount_type == 'percentage')
				$bundle_price = $bundle_price * (1 - ($pbp_bundle_product->discount_amount / 100));

			$price += $bundle_price;

			$offer_price = $bundle_price + ($bundle_price * ($product->tax_rate / 100));
			$saving = $bundle_price_original - $bundle_price;
			$saving = $saving + ($saving * ($product->tax_rate / 100));
			$saving_percent = (($bundle_price_original - $bundle_price) / ($bundle_price_original)) * 100;

			$json_return['bundle_products'][] = array(
				'id_product' => $bundle_product_post['id_product'],
				'offer_price' => $offer_price,
				'offer_price_formatted' => Tools::displayPrice($offer_price, Currency::getCurrencyInstance((Context::getContext()->cart->id_currency), false)),
				'saving' => $saving,
				'saving_percent' => $saving_percent,
				'saving_formatted' => Tools::displayPrice($saving, Currency::getCurrencyInstance((Context::getContext()->cart->id_currency), false))
			);
		}
		$price = $price + ($price * ($product->tax_rate / 100));

		$json_return['bundle_total'] = $price;
		$json_return['bundle_total_formatted'] = Tools::displayPrice($price, Currency::getCurrencyInstance((Context::getContext()->cart->id_currency), false));

		return $json_return;
	}

	protected function addBundleProductToCart($product_cart_data, $parent_product_cart_data = null)
	{
		if (empty(Context::getContext()->cart->id))
		{
			Context::getContext()->cart->add();
			if ($this->context->cart->id)
				$this->context->cookie->id_cart = (int)$this->context->cart->id;
			else
				return false;
		}

		$cart = Context::getContext()->cart;

		$pbp_cart_product_model = new PBPCartProductModel();

		if (empty($product_cart_data['ipa']))
			$product_cart_data['ipa'] = Product::getDefaultAttribute($product_cart_data['id_product']);

		$pbp_cart_product_model->load(Context::getContext()->cart->id, $product_cart_data['id_product'], $product_cart_data['ipa'], Context::getContext()->shop->id);

		if (empty($pbp_cart_product_model->id_cart))
		{
			$pbp_product_model = new PBPProductModel();
			$pbp_product_model->load($product_cart_data['id_product'], Tools::getValue('id_bundle'), Context::getContext()->shop->id);

			/* get information about the parent product, based on the bundle ID */
			//$pbp_bundle = new PBPBundleModel($pbp_product_model->id_bundle);

			$pbp_cart_product_model->id_product = $product_cart_data['id_product'];
			$pbp_cart_product_model->id_product_attribute = $product_cart_data['ipa'];
			$pbp_cart_product_model->id_cart = $cart->id;
			$pbp_cart_product_model->id_address_delivery = $cart->id_address_delivery;
			$pbp_cart_product_model->id_shop = Context::getContext()->shop->id;
			$pbp_cart_product_model->quantity = 1;
			$pbp_cart_product_model->date_add = date('Y-m-d H:i:s');

			if (isset($parent_product_cart_data))
			{
				$pbp_cart_product_model->id_pbp_bundle = $pbp_product_model->id_bundle;
				$pbp_cart_product_model->id_parent_pbp_product = $parent_product_cart_data['id_product'];
				$pbp_cart_product_model->id_parent_pbp_product_ipa = $parent_product_cart_data['ipa'];
			}
			$pbp_cart_product_model->add();
		}
		else
		{
			// increase quantity
			$pbp_cart_product_model->quantity ++;
			$pbp_cart_product_model->updateQty();
		}

		//include_once(_PS_ROOT_DIR_  . "/modules/agilesellershipping/SellerShipping.php");
		//SellerShipping::cleanCartCarrier($this->context->cart->id);

	}

	public function addBundleToCart()
	{
		$parent_product = Tools::getValue('pbp_cart_parent_product');

		foreach (Tools::getValue('pbp_cart_products') as $add_product)
			$this->addBundleProductToCart($add_product, $parent_product);

		/* Now add the parent product to the basket too */
		$this->addBundleProductToCart($parent_product);
		$json = array();
		$json['redirect_url'] = $cart_link = $this->context->link->getPageLink('order', true, (int)$this->context->language->id);
		return $json;
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'add_bundle_to_cart' :
				return Tools::jsonEncode($this->addBundleToCart());

			case 'get_bundle_prices' :
				die (Tools::jsonEncode($this->getBundlePrices()));

			case 'hookrendertabs' :
				die($this->hookRenderTabs(array()));
		}
	}

}