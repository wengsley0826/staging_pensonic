<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class ProductBundlesProAjaxModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->ajax = true;
        // your code here
        parent::initContent();
        $this->route();
    }

    public function route()
    {
        switch (Tools::getValue('route')) {
            case 'pbpproductcontroller' :
                $front_product_controller = new PBPProductController($this);
                die($front_product_controller->route());

            case 'mpproductsearchwidgetcontroller' :
                $mp_product_search_widget = new MPProductSearchWidgetController();
                die(Tools::jsonEncode($mp_product_search_widget->route()));
        }
    }
}