<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPPriceHelper
{

	/**
	 * Get price of product including combination price without Tax
	 * @param $id_product
	 * @param $ipa
	 */
	public static function getPrice($id_product, $ipa)
	{
		print Product::getPriceStatic($id_product, false, $ipa);
		/*$product = new Product($id_product, true);
		$combination = new Combination($ipa);

		if (!$combination->default_on)
			return $product->price + $combination->price;
		else
			$product->price;*/
	}

}