<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

/* Library */
include_once(_PS_MODULE_DIR_."/productbundlespro/lib/classes/PBPControllerCore.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/lib/classes/PBPInstallCore.php");

/* Helpers */
include_once(_PS_MODULE_DIR_."/productbundlespro/helpers/PBPAjaxResponse.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/helpers/PBPPriceHelper.php");

/* Models */
include_once(_PS_MODULE_DIR_."/productbundlespro/models/PBPInstall.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/models/PBPCartProductModel.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/models/PBPProductModel.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/models/PBPProductTabModel.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/models/PBPTabLangModel.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/models/PBPBundleModel.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/models/PBPProductOptionModel.php");

/* Controllers */
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/config/PBPConfigController.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/config/PBPConfigTabsController.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/config/PBPConfigGeneralController.php");


include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/producttab/PBPAdminProductTabController.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/producttab/PBPAdminTabBundlesController.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/producttab/PBPAdminTabBundlesExistingController.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/producttab/PBPAdminProductTabGeneralController.php");


include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/front/PBPProductController.php");
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/front/PBPCartController.php");

/* Widget Controllers */
include_once(_PS_MODULE_DIR_."/productbundlespro/controllers/admin/widget/MPProductSearchWidgetController.php");