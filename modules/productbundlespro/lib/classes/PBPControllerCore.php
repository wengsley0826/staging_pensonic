<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class PBPControllerCore extends Module
{
	protected $sibling;
	protected $module_url;
	protected $module_tab_url;
	protected $helper_form;
	protected $helper_list;

	protected $key_tab = 'ModuleProductbundlespro';

	public function __construct($sibling)
	{
		$this->sibling = $sibling;
		parent::__construct();
		if (AdminController::$currentIndex != '')
		{
			$this->module_tab_url = AdminController::$currentIndex.'&'.'updateproduct&id_product='.Tools::getValue('id_product').'&token='.Tools::getAdminTokenLite('AdminProducts').'&key_tab='.$this->key_tab;
			$this->module_config_url = AdminController::$currentIndex.'&configure=productbundlespro&token='.Tools::getAdminTokenLite('AdminModules');
			//$this->module_url = Tools::getShopProtocol().Tools::getShopDomain().__PS_BASE_URI__.'modules/productbundlespro/';
			if (Tools::getShopDomain() != $_SERVER['HTTP_HOST'])
				$domain = $_SERVER['HTTP_HOST'];
			else
				$domain = Tools::getShopDomain();

			if (empty($_SERVER['HTTPS']) || !$_SERVER['HTTPS'])
				$this->module_url = "http://".$domain.__PS_BASE_URI__.'modules/productbundlespro/';
			else
				$this->module_url = "https://".$domain.__PS_BASE_URI__.'modules/productbundlespro/';
		}

	}

	/* Protected Methods */

	protected function redirect($url_params)
	{
		$url = AdminController::$currentIndex.'&configure='.$this->sibling->name.'&'.$url_params.'&token='.Tools::getAdminTokenLite('AdminModules');
		Tools::redirectAdmin($url);
	}

	protected function redirectProductTab($url_params = '')
	{
		if ($url_params != '')
			$url = AdminController::$currentIndex.'&'.$url_params.'&updateproduct&id_product='.Tools::getValue('id_product').'&token='.Tools::getAdminTokenLite('AdminProducts');
		else
			$url = AdminController::$currentIndex.'&'.'updateproduct&id_product='.Tools::getValue('id_product').'&token='.Tools::getAdminTokenLite('AdminProducts');
		Tools::redirectAdmin($url);
	}

	protected function setupHelperForm()
	{
		$this->helper_form = new HelperForm();
		$this->helper_form->module = $this->sibling;
		$this->helper_form->identifier = $this->identifier;
		$this->helper_form->token = Tools::getAdminTokenLite('AdminModules');
		$this->helper_form->show_toolbar = false;

		$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		foreach (Language::getLanguages(false) as $lang)
			$this->helper_form->languages[] = array(
				'id_lang' => $lang['id_lang'],
				'iso_code' => $lang['iso_code'],
				'name' => $lang['name'],
				'is_default' => ($id_lang == $lang['id_lang'] ? 1 : 0)
			);

		$this->helper_form->currentIndex = AdminController::$currentIndex.'&configure='.$this->sibling->name;
		$this->helper_form->default_form_language = $id_lang;
		$this->helper_form->allow_employee_form_lang = $id_lang;
		$this->helper_form->toolbar_scroll = true;
	}

	protected function setupHelperList($title)
	{
		$this->helper_list = new HelperList();
		$this->helper_list->shopLinkType = '';
		$this->helper_list->simple_header = true;
		$this->helper_list->actions = array('edit', 'delete');

		$this->helper_list->show_toolbar = true;
		$this->helper_list->title = $title;

		$this->helper_list->currentIndex = AdminController::$currentIndex.'&configure='.$this->sibling->name;
		$this->helper_list->token = Tools::getAdminTokenLite('AdminModules');
		return null;
	}
}