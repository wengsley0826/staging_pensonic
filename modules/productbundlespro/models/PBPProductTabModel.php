<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

class PBPProductTabModel extends ObjectModel
{
	/** @var integer Tab ID */
	public $id_tab;

	/** @var integer Product ID */
	public $id_product;

	/** @var integer Shop ID */
	public $id_shop;

	/** @var string Title */
	public $title;

	/** @var integer Language ID */
	public $id_lang;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'pbp_product_tab',
		'primary' => 'id',
		'fields' => array(
			'id_product' => array('type' => self::TYPE_INT),
			'id_shop' => array('type' => self::TYPE_INT)
		)
	);

	public function loadAll($id_lang, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from(self::$definition['table'], 'pt');
		$sql->innerJoin('pbp_tabs_lang', 'tl', 'pt.id_tab = tl.id_tab AND tl.id_lang = '.(int)$id_lang);
		$sql->where('id_shop = '.(int)$id_shop);
		$results = Db::getInstance()->executeS($sql);
		print '<pre>';
		print_r($results);
		print '</pre>';
	}

	public function loadAllByProduct($id_product, $id_lang, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from(self::$definition['table'], 'pt');
		$sql->innerJoin('pbp_tabs_lang', 'tl', 'pt.id_tab = tl.id_tab AND tl.id_lang = '.(int)$id_lang);
		$sql->where('id_product = '.(int)$id_product);
		$sql->where('id_shop = '.(int)$id_shop);
		$results = Db::getInstance()->executeS($sql);

		if (!empty($results))
		{
			$product_tabs_collection = $this->hydrateCollection('PBPProductTabModel', $results);

			if (!empty($product_tabs_collection))
			{
				foreach ($product_tabs_collection as &$product_tab)
				{
					$bundle = new PBPBundleModel();
					$product_tab->bundles = $bundle->getByProductTab($product_tab->id_product, $product_tab->id_tab);
				}
				return $product_tabs_collection;
			}
		}
		else return false;
	}

}
