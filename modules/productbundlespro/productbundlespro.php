<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/productbundlespro/lib/bootstrap.php');

class ProductBundlesPro extends Module
{
	public $module_file = __FILE__;

	public function __construct()
	{
		$this->name = 'productbundlespro';
		$this->tab = 'others';
		$this->version = '1.1.5';
		$this->author = 'Musaffar Patel';
		$this->need_instance = 0;
		$this->module_key = '63ee9e0503bede842a1cbb2fadd20426';
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

		parent::__construct();
		$this->displayName = $this->l('Product Bundles Pro');
		$this->description = $this->l('Increase cart order value by creating tailored product bundles together with discounts to cross sell on your product pages');
		$this->bootstrap = true;
		$this->base_url = Tools::getShopProtocol().Tools::getShopDomain().__PS_BASE_URI__;

		$this->registerHook('displayProductButtons');
	}

	public function install()
	{
		if (parent::install() == false
			|| !$this->registerHook('productTabContent')
			|| !$this->registerHook('backOfficeHeader')
			|| !$this->registerHook('displayLeftColumnProduct')
			|| !$this->registerHook('displayAdminProductsExtra')
			|| !$this->registerHook('actionCartSave')
			|| !$this->registerHook('pbpPriceCalculation')
			|| !$this->registerHook('pbpDeleteCartProduct')
			|| !$this->registerHook('displayHeader')
			|| !$this->registerHook('backOfficeHeader')
			|| !$this->registerHook('displayFooter')
			|| !$this->registerHook('displayPBP')
			|| !$this->registerHook('displayProductButtons')
			|| !$this->install_module())
			return false;
		return true;
	}

	public function install_module()
	{
		PBPInstall::install_db();
		return true;
	}

	public function uninstall()
	{
		PBPInstall::uninstall();
		return parent::uninstall();
	}

	/* Call set media for all the various controllers in this module.  Each controller will decide if the time is appropriate for queuing it's css and js */
	public function setMedia()
	{
		(new PBPAdminProductTabController($this))->setMedia();
		(new PBPAdminProductTabGeneralController($this))->setMedia();
		(new PBPProductController($this))->setMedia();
		(new PBPCartController($this))->setMedia();
	}


	public function getContent()
	{
		$controller_config = new PBPConfigController($this);
		return $controller_config->route();
	}

	/*** Hooks ***/

	public function hookPbpPriceCalculation($params)
	{
		//get IDs of all products in the cart, except for this one
		$front_cart_controller = new PBPCartController($this);
		return $front_cart_controller->hookPbpPriceCalculation($params);
	}

	public function hookDisplayLeftColumnProduct($params)
	{
		/*$front_product_controller = new PBPProductController($this);
		return $front_product_controller->hookRenderTabs($params);*/
	}

	public function hookDisplayPBP($params)
	{
		$front_product_controller = new PBPProductController($this);
		return $front_product_controller->hookRenderTabs($params);
	}


	public function hookDisplayAdminProductsExtra($params)
	{
		$pbp_admin_producttab_controller = new PBPAdminProductTabController($this);
		return $pbp_admin_producttab_controller->route();
	}

	public function hookDisplayHeader($params)
	{
		$this->setMedia();
	}

	public function hookDisplayFooter($params)
	{
		if (Context::getContext()->controller->php_self == 'product')
		{
			$front_product_controller = new PBPProductController($this);
			return $front_product_controller->hookDisplayFooter($params);
		}
	}

	public function hookDisplayProductButtons($params)
	{
		if (Tools::getValue('content_only') == '1' && Configuration::get('pbp_quickview_enabled') == '1')
		{
			$front_product_controller = new PBPProductController($this);
			return $front_product_controller->hookDisplayFooter($params);
		}
	}

	public function hookBackOfficeHeader($params)
	{
		$this->setMedia();
	}


}