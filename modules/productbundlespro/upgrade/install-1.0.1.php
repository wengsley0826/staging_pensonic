<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/productbundlespro/lib/bootstrap.php');

function upgrade_module_1_0_1($object)
{
	$return = false;
	$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pbp_product_option` (
		  `id_option` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `id_product` int(10) unsigned NOT NULL,
		  `disabled_addtocart` tinyint(3) unsigned NOT NULL DEFAULT \'0\',
		PRIMARY KEY (`id_option`)
	)ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

	return true;
}