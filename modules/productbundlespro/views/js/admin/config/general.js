/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

PBPAdminConfigGeneralController = function(wrapper)
{
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	/* Methods */

	self.save = function() {
		Tools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=pbpconfiggeneralcontroller&action=processform',
			async: true,
			cache: false,
			//dataType: "json",
			data: $(self.wrapper + " :input").serialize(),
			success: function (result) {
				Tools.waitEnd();
			}
		});
		return false;
	};

	self.init = function() {
	};
	self.init();

	$("body").on("click", self.wrapper + " #pbp-general-save", function () {
		self.save();
		return false;
	});


};