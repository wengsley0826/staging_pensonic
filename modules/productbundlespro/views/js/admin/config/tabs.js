/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

PBPAdminConfigTabsController = function(wrapper)
{
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	self.popupFormId = 'pbp-popup-form';
	self.list = "pbp-tabs";
	self.popup; //instance of modal popup

	/* Methods */

	self.refreshList = function() {
		Tools.waitStart();
		var url = module_config_url + '&route=pbpconfigtabscontroller';
		$("#pbp-tabs").load(
				url + ' #pbp-tabs > *',
				{
					'is_ajax': 1
				},
				function () {
					Tools.waitEnd();
				}
		);
		return false;
	};

	self.openForm = function(id_tab) {
		Tools.waitStart();
		var url = module_config_url + '&route=pbpconfigtabscontroller&action=renderform';
		console.log(url);
		if (typeof (id_tab) !== 'undefined')
			url = url + '&id_tab=' + id_tab;
		self.popup = new MPPopup(self.popupFormId, '#pbp-tabs');
		self.popup.showContent(url, null, function() {
			Tools.waitEnd();
			self.popup.setHeight('auto');
		});
		return false;
	};

	self.deleteTab = function(id_tab) {
		var url = module_config_url + '&route=pbpconfigtabscontroller&action=remove';
		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {'id_tab': id_tab},
			success: function (result) {
				self.refreshList();
			}
		});
	};

	self.init = function() {
	};
	self.init();

	$("body").on("click", self.wrapper + " #add-tab", function () {
		self.openForm();
		return false;
	});

	$("body").on("click", self.wrapper + " .pbp-edit", function () {
		self.openForm($(this).attr("data-id_tab"));
		return false;
	});

	$("body").on("click", self.wrapper + " .pbp-delete", function () {
		if (confirm('Are you sure you wante to delete the tab?'))
			self.deleteTab($(this).attr("data-id_tab"));
		return false;
	});


	/* Popup form events */
	$("body").on("click", self.wrapper + " #pbp-tab-popup-save", function () {
		Tools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=pbpconfigtabscontroller&action=processform',
			async: true,
			cache: false,
			dataType: "json",
			data: $("#" + self.popupFormId + " :input").serialize(),
			success: function (result) {
				Tools.waitEnd();

				if (!Tools.handleAjaxResponse(result, self.$wrapper.find(".alert"))) return false;

				self.popup.close();
				self.refreshList();

			}
		});
		return false;
	});

	$("body").on("click", self.wrapper + " #pbp-popup-close", function () {
		self.popup.close();
		return false;
	});

};