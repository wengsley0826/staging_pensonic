/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */
 
PBPProductTabBundleExistingController = function(wrapper) {

	self.wrapper = wrapper;
	self,$wrapper = $(wrapper);
	self.popupEditFormId = 'pbp-popup-existingbundle';

	self.selected_id_bundle = 0;   
	self.selected_id_product = 0;

	self.openForm = function(id_bundle) {
		var url = module_tab_url + '&route=pbpadmintabbundlesexistingcontroller&action=renderform&id_product=' + id_product;

		if (typeof (id_bundle) !== 'undefined') {
			url = url + '&id_bundle=' + id_bundle;
			self.id_bundle = id_bundle;
		} else
			self.id_bundle = '';

		self.popup = new MPPopup(self.popupEditFormId, self.wrapper);
		self.popup.showContent(url, null, function () {
			Tools.waitEnd();
		});
		return false;	
	};	

	self.displayProductBundlesList = function(id_product) {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlesexistingcontroller&action=renderproductbundles&id_product=' + id_product;

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product': id_product
			},
			success: function (result) {
				$("#pbp-bundles-existing-list").html(result);
				Tools.waitEnd();
			}
		});		
	};

	self.saveBundle = function() {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlesexistingcontroller&action=processaddbundle&id_product=' + self.selected_id_product;

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_tab' : self.$wrapper.find("#id_tab").val(),
				'id_product': http_get.id_product,
				'id_bundle' : self.selected_id_bundle
			},
			success: function (result) {
				Tools.waitEnd();
			}
		});		
	};

	self.init = function() {
		productsearch = new MPProductSearchWidget("#" + self.popupEditFormId);
	};
	self.init();

	$("body").on("click", self.wrapper + " #pbp-add-bundle-existing", function () {
		self.openForm();
		return false;
	});

	$("body").on("click", self.wrapper + " #search-results-table .result-item", function () {
		self.selected_id_product = $(this).attr('data-id_product');		
		self.displayProductBundlesList($(this).attr('data-id_product'));
		return false;
	});

	$("body").on("click", self.wrapper + " #pbp-bundles-existing-list .pbp-bundle", function () {
		self.$wrapper.find("#pbp-bundles-existing-list .pbp-bundle").removeClass('selected');
		$(this).addClass("selected");
		self.selected_id_bundle = $(this).attr("data-id");
		return false;
	});

	$("body").on("click", self.wrapper + " #pbp-existing-bundle-add", function () {
		self.saveBundle();
		return false;
	});	

}