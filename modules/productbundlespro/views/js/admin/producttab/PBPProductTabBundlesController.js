/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

PBPAdminTabBundlesController = function(wrapper) {
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	self.popupEditFormId = 'pbp-popup-editbundle';
	self.popupEditBundleId = 'panel2';

	self.list = "pbp-bundles";
	self.productList = "pbp-bundle-products";
	self.popup; //instance of modal popup
	self.id_bundle = ''; //id of bundle being edited

	self.popupExisting = new PBPProductTabBundleExistingController(self.wrapper);

	self.openForm = function(id_bundle) {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=renderform&id_product=' + id_product;

		if (typeof (id_bundle) !== 'undefined') {
			url = url + '&id_bundle=' + id_bundle;
			self.id_bundle = id_bundle;
		} else
			self.id_bundle = '';

		self.popup = new MPPopup(self.popupEditFormId, self.wrapper);
		self.popup.showContent(url, null, function () {
			self.refreshProductList();
			Tools.waitEnd();
		});
		return false;
	};


	/* save theposition of the bundles after a drag drop event */
	self.saveBundleListPositions = function() {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=processbundlepositions&id_product=' + id_product;

		var bundle_positons = [];
		$("#bundlesTable tbody tr").each(function(index) {
			bundle_positons[index] = $(this).attr("data-id");
		});
		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product': id_product,
				'bundle_positions' : bundle_positons
			},
			success: function (result) {
				Tools.waitEnd();
			}
		});
	};

	self.refreshBundleList = function() {
		Tools.waitStart();

		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=renderlist #' + self.list + ' > *';

		$("#" + self.list).load(
			url,
			{
				'is_ajax': 1
			},
			function () {
				$("#bundlesTable").tableDnD({
					dragHandle: 'dragHandle',
					onDragClass: 'myDragClass',
					onDragStart: function (table, row) {
					},
					onDrop: function (table, row) {
						self.saveBundleListPositions();
					}
				});
				Tools.waitEnd();
			}
		);
		return false;
	};


	self.deleteBundle = function(id_bundle) {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=deletebundle';

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product' : id_product,
				'id_bundle': id_bundle
			},
			success: function (result) {
				self.refreshBundleList();
			}
		});

	};
	
	self.copyBundle = function(id_bundle) {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=copybundle';

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product' : id_product,
				'id_bundle': id_bundle
			},
			success: function (result) {
				self.refreshBundleList();
			}
		});

	};
	
	self.exportBundle = function(id_bundle) {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=exportbundle';

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product' : id_product,
				'id_bundle': id_bundle
			},
			success: function (result) {
				self.refreshBundleList();
			}
		});

	};

	self.init = function() {
		productsearch = new MPProductSearchWidget("#" + self.popupEditFormId);
		self.refreshBundleList();
	};
	self.init();

	$("body").on("click", self.wrapper + " #pbp-add-bundle", function () {
		self.openForm();
		return false;
	});

	$("body").on("click", self.wrapper + " .pbp-edit-bundle", function () {
		self.openForm($(this).attr("data-id"), self.refreshProductList);
		return false;
	});

	$("body").on("click", self.wrapper + " .pbp-delete-bundle", function () {
		self.deleteBundle($(this).attr("data-id"));
		return false;
	});

	$("body").on("click", self.wrapper + " .pbp-delete-bundle", function () {
		self.deleteBundle($(this).attr("data-id"));
		return false;
	});

	$("body").on("click", self.wrapper + " .pbp-copy-bundle", function () {
		self.copyBundle($(this).attr("data-id"));
		return false;
	});

	$("body").on("click", self.wrapper + " .pbp-export-bundle", function () {
		if (confirm('Confirm export this bundle to other products of this bundle?'))
			self.exportBundle($(this).attr("data-id"));
		return false;
	});


	/* Popup Methods */

	self.saveBundleProductListPositions = function() {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=processbundleproductpositions&id_product=' + id_product;

		var bundle_product_positons = [];
		$("#productsTable tbody tr").each(function (index) {
			var id_pbp_product = $(this).attr("data-id_pbp_product");
			if (typeof (id_pbp_product) !== 'undefined')
				bundle_product_positons[index] = $(this).attr("data-id_pbp_product");
		});

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product': id_product,
				'bundle_product_positions': bundle_product_positons
			},
			success: function (result) {
				Tools.waitEnd();
			}
		});
	};

	// refresh the list of products in the bundle
	self.refreshProductList = function() {
		Tools.waitStart();

		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=renderproductlist&id_bundle=' + self.id_bundle;

		$("#" + self.productList).load(
			url,
			{
				'is_ajax': 1
			},
			function () {
				$("#productsTable").tableDnD({
					dragHandle: 'dragHandle',
					onDragClass: 'myDragClass',
					onDragStart: function (table, row) {
					},
					onDrop: function (table, row) {
						self.saveBundleProductListPositions();
					}
				});
				Tools.waitEnd();
			}
		);
		return false;
	};

	self.getProductsListAsJson = function() {
		var products = [];

		$products_table = $('#' + self.popupEditFormId).find("#productsTable");
		$products_table.find("tr").each(function (i, obj) {
			if (typeof($(obj).attr("data-id_product")) !== 'undefined' && $(obj).attr("data-id_product") != '')
				products.push({
					'id_product' : $(obj).attr("data-id_product"),
					'name' : $(obj).attr("data-name"),
					'discount_type': $(obj).attr("data-discount_type"),
					'discount_amount': $(obj).attr("data-discount_amount"),
					'discount_tax': $(obj).attr("data-discount_tax")
				});
		});
		return products;
	};


	/* delete a product from the bundle */
	self.deleteProduct = function(id_product) {
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=deleteproduct';

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product': id_product,
				'id_bundle': self.id_bundle
			},
			success: function (result) {
				self.refreshProductList();
			}
		});
	};

	/**
	 * Load Add product, prepoulate with existing product information
 	 * @param id_product
     */
	self.editProduct = function(id_product) {
		var $panel2 = $('#' + self.popupEditFormId);
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=getproductbundledata';

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'id_product': id_product,
				'id_bundle': self.id_bundle
			},
			success: function (result) {
				$panel2.find("input#id_product").val(result.id_product);
				$panel2.find("input#id_pbp_product").val(result.id_pbp_product);
				$panel2.find("input#product_search").val(result.product_name);
				$panel2.find("input#discount_amount").val(result.discount_amount);
				if (result.discount_type == 'percentage') {
					$panel2.find("select#discount_type").val('percentage');
				} else {
					$panel2.find("select#discount_type").val('money');
				}
				$panel2.find("input#discount_type").val(result.discount_amount);
				self.popup.showSubPanel('panel2');
			}
		});
		return false;
	};

	/* Popup Events */

	/* Save the bundle and it's products */
	$("body").on("click", self.wrapper + " #pbp-popup-save", function () {

		var bundle_products = self.getProductsListAsJson();
		var url = module_tab_url + '&route=pbpadmintabbundlescontroller&action=processform';

        // convulated way to get enabled state from Prestashop switch control, direct approach does not work
		var form_data = $('#' + self.popupEditFormId).find(" :input[name='enabled']").serialize();
        var enabled = form_data.replace('enabled=', '');

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_tab' : $('#' + self.popupEditFormId).find("select[name='id_tab']").val(),
				'id_bundle' : self.id_bundle,
				'id_product' : id_product,
				'enabled' : enabled,
				'bundle_products': bundle_products
			},
			dataType : 'json',
			success: function (result) {
				self.id_bundle = result.id_bundle;
				self.refreshBundleList();
			}
		});
		return false;
	});

	/* Delete icon for bundle product */
	$("body").on("click", "#" + self.popupEditFormId + " .pbp-bundle-product-delete", function () {
		self.deleteProduct($(this).attr("data-id_product"));
		return false;
	});


	/**
	 * Edit icon for bundle product
 	 */
	$("body").on("click", "#" + self.popupEditFormId + " .pbp-bundle-product-edit", function () {
		self.editProduct($(this).attr("data-id_product"));
	});


	$("body").on("click", self.wrapper + " #pbp-popup-close", function () {
		self.popup.close();
		return false;
	});


	/**** Panel 2 *****/

	/* Methods */

	self.applyProductEditChanges = function() {
		$panel2 = $("#" + self.popupEditFormId);

		var id_pbp_product = $panel2.find("input[name='id_pbp_product']").val();
		var id_product = $panel2.find("input[name='id_product']").val();
		var name = $panel2.find("input[name='product_search']").val();
		var discount_type = $panel2.find("select[name='discount_type']").val();
		var discount_amount = $panel2.find("input[name='discount_amount']").val();
		var discount_tax = $panel2.find("select[name='discount_tax']").val();

		$products_table = $('#' + self.popupEditFormId).find("#productsTable");

		// is the product being edited (does it already exist in the product list table?)
		if (parseInt(id_pbp_product) > 0) {
			if ($products_table.find("tr[data-id_pbp_product='" + id_pbp_product + "']").length > 0) {
				$row = $products_table.find("tr[data-id_pbp_product='" + id_pbp_product + "']");
				$row.attr("data-id_product", id_product);
				$row.attr("data-id_pbp_product", id_pbp_product);
				$row.attr("data-name", name);
				$row.attr("data-discount_type", discount_type);
				$row.attr("data-discount_amount", discount_amount);

				$row.find("td.id_product").html(id_product);
				$row.find("td.name").html(name);
				$row.find("td.discount_type").html(discount_type);
				$row.find("td.discount_amount").html(discount_amount);
				return false;
			}
		}

		var $cloned = $products_table.find("tr.cloneable").clone();
		$cloned.removeClass("cloneable");
		$cloned.removeClass("hidden");
		$cloned.addClass("result-item");

		/* set data */
		$cloned.find("td.id_product").html(id_product);
		$cloned.attr("data-id_product", id_product);

		$cloned.find("td.name").html(name);
		$cloned.attr("data-name", name);

		$cloned.find("td.discount_type").html(discount_type);
		$cloned.attr("data-discount_type", discount_type);

		$cloned.find("td.discount_amount").html(discount_amount);
		$cloned.attr("data-discount_amount", discount_amount);

		$cloned.find("td.discount_tax").html(discount_tax);
		$cloned.attr("data-discount_tax", discount_tax);

		$cloned.appendTo($products_table.find("tbody"));
		$products_table.find("tr.no-products").hide();
	};

	self.initDropdownPanel = function () {
		Tools.waitEnd();
	};


	/* events */

	$("body").on("click", "#" + self.popupEditFormId + " #pbp-edit-product", function () {
		self.popup.showSubPanel('panel2');
		return false;
	});

	$("body").on("click", "#" + self.popupEditFormId + " #pbp-edit-product-done", function () {
		self.applyProductEditChanges();
		self.popup.hideSubPanel('panel2');
		return false;
	});


	$("body").on("click", "#" + self.popupEditFormId + " #pbp-edit-product-cancel", function () {
		self.popup.hideSubPanel('panel2');
		return false;
	});


};