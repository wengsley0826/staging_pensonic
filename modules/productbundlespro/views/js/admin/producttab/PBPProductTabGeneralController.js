/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */
 
PBPAdminTabGeneralController = function (wrapper) {

	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	self.saveForm = function() {
		Tools.waitStart();
		var url = module_tab_url + '&route=pbpadmintabgeneralcontroller&action=processform&id_product=' + id_product;

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product': id_product,
				'disabled_addtocart' : Number(self.$wrapper.find("#pbp-disabled_addtocart").is(":checked"))
			},
			success: function (result) {
				console.log(result);
				Tools.waitEnd();
			}
		});
	};

	self.init = function() {
	};
	self.init();

	$("body").on("click", self.wrapper + " #pbp-general-save", function () {
		self.saveForm();
		return false;
	});

};