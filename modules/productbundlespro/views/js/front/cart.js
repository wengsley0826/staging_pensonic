/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

PBPCartController = function () {
	var self = this;

	self.init = function () {
		/* event bindings */
		$('.cart_quantity_down').off('click').on('click', function (e) {
			result = self.onCartQtyChange($(this), 'down');

			if (typeof result !== 'undefined' && result == true) {
				window.location.href = window.location.href;
				return false;
			}
			else if (typeof result !== 'undefined' && result == false) {
				//invoke original handler
				e.preventDefault();
				downQuantity($(this).attr('id').replace('cart_quantity_down_', ''));
				return false;
			}
		});
		$('.cart_quantity_up').off('click').on('click', function (e) {
			result = self.onCartQtyChange($(this), 'up');
			if (typeof result !== 'undefined' && result == true) {
				window.location.href = window.location.href;
				return false;
			}
			else if (typeof result !== 'undefined' && result == false) {
				//invoke original handler
				e.preventDefault();
				upQuantity($(this).attr('id').replace('cart_quantity_up_', ''));
				return false;
			}
		});

		/* override the cart quantity typeWatch (keypress events) */
		$('.cart_quantity_input').off('keydown cut paste input');
		$('.cart_quantity_input').typeWatch({
			highlight: true, wait: 600, captureLength: 0, callback: function (val) {
				result = self.onCartQtyChange(this.el, 'numeric', val);
				if (typeof result !== 'undefined' && result == true) {
					window.location.href = window.location.href;
					return false;
				}
				else if (typeof result !== 'undefined' && result == false) {
					//invoke original handler
					updateQty(val, true, this.el);
				}
			}
		});

		$('.cart_quantity_delete').off('click').on('click', function (e) {
			e.preventDefault();
			window.location.href = $(this).attr("href");
			return false;
		});

	};


	self.init();

	/* event handlers */
	self.onCartQtyChange = function (sender, direction, qty_val) {
		var return_result = false;
		var customizationId = 0;
		var id_product = 0;
		var id_product_attribute = 0;
		var id_address_delivery = 0;
		var ids = 0;

		if (direction == 'down') id = $(sender).attr('id').replace('cart_quantity_down_', '');
		if (direction == 'up') id = $(sender).attr('id').replace('cart_quantity_up_', '');
		if (direction == 'numeric') id = $(sender).attr('name').replace('quantity_', '');

		if (direction != 'numeric') var val = parseInt($('input[name=quantity_' + id + ']').val());
		if (direction == 'down')
			var newVal = val - 1;
		if (direction == 'up')
			var newVal = val + 1;
		if (direction == 'numeric')
			var newVal = qty_val;

		if (newVal < 0) newVal = 0;

		ids = id.split('_');
		id_product = parseInt(ids[0]);
		if (typeof(ids[1]) !== 'undefined')
			id_product_attribute = parseInt(ids[1]);
		if (typeof(ids[2]) !== 'undefined' && ids[2] !== 'nocustom')
			id_customization = parseInt(ids[2]);
		if (typeof(ids[3]) !== 'undefined')
			id_address_delivery = parseInt(ids[3]);

		baseUri = baseUri.replace('index.php', '');

		if (newVal > 0 || $('#product_' + id + '_gift').length) {
			$.ajax({
				type: 'POST',
				headers: {"cache-control": "no-cache"},
				url: baseUri + module_controller_url + '?rand=' + new Date().getTime(),
				async: false,
				cache: false,
				dataType: 'json',
				data: 'controller=PPBSCartController'
				+ '&do=updatePPBSQty'
				+ '&id_product=' + id_product
				+ '&ipa=' + id_product_attribute
				+ '&id_address_delivery=' + id_address_delivery
				+ '&op=down'
				+ ((id_customization !== 0) ? '&id_customization=' + id_customization : '')
				+ '&qty=' + newVal
				+ '&token=' + static_token
				+ '&allow_refresh=1',
				success: function (result) {
					if (result.cart_customization_qty_updated == true) {
						return_result = true;
					}
					else {
						return_result = false;
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					return_result = false;
				}
			});
		}
		return return_result;
	}
};

$(document).ready(function() {
	pbp_controller = new PBPCartController();
});