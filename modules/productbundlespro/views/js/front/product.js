/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

PBPFrontProductController = function(wrapper) {
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);

	/* Methods */

	self.getMainProductCartInfo = function($dom_bundle) {
		product = new Object();
		product.id_product = id_product;
		product.ipa = $(idCombination).val();

		if (typeof $dom_bundle !== 'undefined') {
			if ($dom_bundle.find("select.parent_product_ipa").length > 0) {
				product.ipa = $dom_bundle.find("select.parent_product_ipa").val();
			}
		}
		return product;
	};

	self.constructCartProductArray = function($dom_bundle) {

		pbp_cart_products = [];
		var added = false;

		$dom_bundle.find(".bundle-product").each(function () {
			if ($(this).find("select.bundle_product_ipa").length > 0)
				var value = $(this).find("select.bundle_product_ipa").val();

			product = new Object();
			product.id_product = $(this).attr("data-id_product");
			product.ipa = value;
			pbp_cart_products.push(product);
			added = true;
		});
		return pbp_cart_products;
	};

	/**
	 *
 	 * @param $bundle jquery bundle element
	 */
	self.updateBundlePrices = function($bundle) {
		var pbp_cart_products = self.constructCartProductArray($bundle);

		$.ajax({
			type: 'POST',
			headers: {"cache-control": "no-cache"},
			url: pbp_front_ajax_url + '?route=pbpproductcontroller&action=get_bundle_prices&rand=' + new Date().getTime(),
			cache: false,
			dataType : "json",
			data: {
				id_product: id_product,
				id_bundle : $bundle.attr('data-id_bundle'),
				'pbp_cart_products' : pbp_cart_products,
				'pbp_cart_parent_product': self.getMainProductCartInfo($bundle)
			},
			success: function (result) {
				if (typeof result.bundle_products !== 'undefined') {
					for (i = 0; i <= result.bundle_products.length -1; i++) {
						var $bundle_product = $bundle.find(".bundle-product[data-id_product='"+ result.bundle_products[i].id_product+"']");
						$bundle_product.find(".pbp-discount-amount").html(result.bundle_products[i].saving_formatted + ' (' + result.bundle_products[i].saving_percent +  '%)');
						$bundle_product.find(".pbp-offer-price").html(result.bundle_products[i].offer_price_formatted);
						$bundle.find(".pbp_bundle_total").html(result.bundle_total_formatted);
					}
				}
			}
		});

	};


	self.addPBPToCart = function ($dom_bundle) {
		var pbp_cart_products = self.constructCartProductArray($dom_bundle);

		$.ajax({
			type: 'POST',
			headers: {"cache-control": "no-cache"},
			url: pbp_front_ajax_url + '?route=pbpproductcontroller&action=add_bundle_to_cart&rand=' + new Date().getTime(),
			cache: false,
			dataType : "json",
			data: {
				id_product: id_product,
				id_bundle : $dom_bundle.attr('data-id_bundle'),
				'pbp_cart_products' : pbp_cart_products,
				'pbp_cart_parent_product': self.getMainProductCartInfo($dom_bundle)
			},
			success: function (result) {
				if (confirm('Products Added, goto Cart?'))
					window.location.href = result.redirect_url;
				else
					window.location.reload();
			}
		});
	};


	self.init = function() {
	};
	self.init();

	self.$wrapper.find('.btn_add_bundle_cart').click(function() {
		self.addPBPToCart($(this).parents(".bundle"));
		return false;
	});

	/**
	 * on hange of any bundle attribute drop down select
 	 */
	$("body").on("change", self.wrapper + " .bundle select", function () {
		self.updateBundlePrices($(this).parents(".bundle"));
	});

	self.$wrapper.find('.tab-links a').click(function () {
		self.$wrapper.find(".pbp-tab-content").hide();
		self.$wrapper.find(".pbp-tab-links a").removeClass("active");
		$(this).addClass("active");
		var tab_id = $(this).attr("data-tab_id");
		self.$wrapper.find(".pbp-tab-content-"+tab_id).show();
		return false;
	});

};