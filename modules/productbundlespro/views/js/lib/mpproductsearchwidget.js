/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

MPProductSearchWidget = function(wrapper) {
	var self = this;
	self.wrapper = wrapper;

	self.pk = 'id_product'; //unique idebntifier for each row,. data binded to database table

	self.init = function() {
	};
	self.init();

	self.showResultsList = function () {
		$(self.wrapper).find("#product-search-results").slideDown();
	};

	self.hideResultsList = function () {
		$(self.wrapper).find("#product-search-results").slideUp();
	};

	self.clearResultsList = function () {
		$(self.wrapper).find("#search-results-table tbody tr.result-item").remove();
	};

	self.addToResultList = function (jsonRow) {
		$results_table = $(self.wrapper).find("#search-results-table");

		var $cloned = $results_table.find("tr.cloneable").clone();
		$cloned.removeClass("cloneable");
		$cloned.removeClass("hidden");
		$cloned.addClass("result-item");

		$.each(jsonRow, function (key, value) {
			$cloned.find("td."+key).html(value);
			$cloned.attr("data-"+key, value);
		});
		$cloned.appendTo($results_table.find("tbody"));
	};

	self.onResultSelect = function (id, selected_text) {
		$(self.wrapper).find('input[name="'+self.pk+'"]').val(id);
		$(self.wrapper + " input#product_search").val(selected_text);
	};

	self.popupProcessSearch = function (search_string, scope) {
		var url = pbp_ajax_url + '?route=mpproductsearchwidgetcontroller&action=processsearch';
		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'search_string': search_string,
				'scope': scope
			},
			success: function (jsonData) {
				self.clearResultsList();
				self.showResultsList();
				for (var x = 0; x <= jsonData.length - 1; x++) {
					self.addToResultList(jsonData[x])
				}
				return false;
			}
		});
	};

	$("body").on("keyup", self.wrapper + " #product_search", function () {
		if ($(this).val().length >= 3) {
			self.popupProcessSearch($(this).val(), $('#' + self.popupFormId + " #scope").val())
		}		
	});

	$("body").on("click", self.wrapper + " tr.result-item", function () {
		self.onResultSelect($(this).attr("data-"+self.pk), $(this).attr("data-name"));
		self.hideResultsList();
	});
};