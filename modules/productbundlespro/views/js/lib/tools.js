/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

var Tools = {

	waitStart: function () {
		$("body").append('<div class="mp-wait-wrapper"><i class="icon icon-circle-o-notch icon-spin"></i></div>');
	},

	waitEnd: function () {
		$(".mp-wait-wrapper").remove();
	},

	handleAjaxResponse : function(json, $error_msg_wrapper) {
		var result = true;
		var error_msg = '';

		if (typeof json === 'undefined' || json == null || json == '') return true;

		if (typeof json.meta !== 'undefined') {
			if (typeof json.meta.error !== 'undefined') {
				if (json.meta.error == true) {
					result = false;
					if (typeof json.content !== 'undefined') {
						for (i=0; i<= json.content.length-1; i++) {
							$(json.content[i].dom_element).addClass("error");
							error_msg += json.content[i].message + "<br>";
						}
					}
					if (error_msg != '') {
						$error_msg_wrapper.html(error_msg);
						$error_msg_wrapper.show();
					}

				}
			}
		}
		return result;
	}

};

