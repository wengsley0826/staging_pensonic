{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="pbp-general" class="panel">
	<div class="panel-heading">
		<i class="icon-cogs"></i> General Settings
	</div>

	<div class="form-wrapper row">

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display in Product Quick View?
			</label>

			<div class="col-lg-10">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="pbp_quickview_enabled" id="pbp_quickview_enabled_on" value="1" {if $pbp_general.pbp_quickview_enabled eq "1"}checked{/if}>
					<label for="pbp_quickview_enabled_on">Yes</label>
					<input type="radio" name="pbp_quickview_enabled" id="pbp_quickview_enabled_off" value="0" {if $pbp_general.pbp_quickview_enabled neq "1"}checked{/if}>
					<label for="pbp_quickview_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display Attributes of parent product in bundle?
			</label>

			<div class="col-lg-10">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="pbp_parent_attr_display" id="pbp_parent_attr_display_on" value="1" {if $pbp_general.pbp_parent_attr_display eq "1"}checked{/if}>
					<label for="pbp_parent_attr_display_on">Yes</label>
					<input type="radio" name="pbp_parent_attr_display" id="pbp_parent_attr_display_off" value="0" {if $pbp_general.pbp_parent_attr_display neq "1"}checked{/if}>
					<label for="pbp_parent_attr_display_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display bundle total in bundles?
			</label>

			<div class="col-lg-10">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="pbp_display_bundle_total" id="pbp_display_bundle_total_on" value="1" {if $pbp_general.pbp_display_bundle_total eq "1"}checked{/if}>
					<label for="pbp_display_bundle_total_on">Yes</label>
					<input type="radio" name="pbp_display_bundle_total" id="pbp_display_bundle_total_off" value="0" {if $pbp_general.pbp_display_bundle_total neq "1"}checked{/if}>
					<label for="pbp_display_bundle_total_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>


		<div class="form-group row">
			<label class="control-label col-lg-2">
				Display Location
			</label>

			<div class="col-lg-10">
				<select name="pbp_location" id="pbp_location">
					<option value="tabs-before">Above Product Tabs</option>
					<option value="description-after">After Product Description</option>
				</select>

				{if !empty($pbp_general.pbp_location)}
					<script>
						$(document).ready(function() {
							$("#pbp_location").val('{$pbp_general.pbp_location|escape:'htmlall':'UTF-8'}');
						});
					</script>
				{else}
					<script>
						$(document).ready(function () {
							$("#pbp_location").val('tabs-before');
						});
					</script>
				{/if}

			</div>
		</div>
	</div>
	<!-- /.form-wrapper -->

	<div class="panel-footer">
		<button type="button" value="1" id="pbp-general-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>

<script>
	module_config_url = '{$module_config_url|escape:'quotes':'UTF-8'}';
	$(document).ready(function () {
		pbp_general = new PBPAdminConfigGeneralController("#pbp-general");
	});
</script>