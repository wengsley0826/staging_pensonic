{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<input type="hidden" name="id_tab" value="{$id_tab|escape:'htmlall':'UTF-8'}">

<div id="panel1" class="panel">
	<h3>Add / Edit Tab</h3>

	<div class="alert alert-danger" style="display: none"></div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-2" for="">
					<span class="label-tooltip" title="tile of the tab as displayed on your product page">Title</span>
				</label>

				{foreach from=$languages item=language}
					<div class="translatable-field lang-{$language.id_lang|escape:'htmlall':'UTF-8'}" style="{if $language.id_lang eq $id_lang_default}display: block;{else}display:none;{/if}">
						<div class="col-lg-7">
							<input type="text" id="title_{$language.id_lang|escape:'htmlall':'UTF-8'}" class="form-control updateCurrentText " name="title_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{if isset({$title_localised_array[$language.id_lang|escape:'htmlall':'UTF-8']})}{$title_localised_array[$language.id_lang]|escape:'htmlall':'UTF-8'}{/if}" required="required">
						</div>

						<div class="col-lg-2">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
								{$language.iso_code|escape:'htmlall':'UTF-8'}
								<span class="caret"></span>
							</button>


							<ul class="dropdown-menu">
								{foreach from=$languages item=language_dropdown}
									<li>
										<a href="javascript:hideOtherLanguage({$language_dropdown.id_lang|escape:'htmlall':'UTF-8'});">{$language_dropdown.name|escape:'htmlall':'UTF-8'}</a>
									</li>
								{/foreach}
							</ul>

						</div>
					</div>
				{/foreach}
			</div>
		</div>
	</div>

	<div class="panel-footer">
		<a href="#close" id="pbp-popup-close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="pbp-tab-popup-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> {l s='Save' mod='productbundlespro'}
		</button>
	</div>
</div>