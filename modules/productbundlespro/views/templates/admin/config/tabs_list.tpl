{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="pbp-tabs" class="panel">
	<h3>{l s='Bundle Tabs' mod='productbundlespro'}</h3>

	<table id="fieldsTable" class="table">
		<thead>
		<tr>
			<th><span class="title_box">{l s='Name' mod='productbundlespro'}</span></th>
			<th><span class="title_box">{l s='Action' mod='productbundlespro'}</span></th>
		</tr>
		</thead>

		<tbody>
			{foreach from=$tabs item=tab}
				<tr>
					<td>
						{$tab->title|escape:'htmlall':'UTF-8'}
					</td>
					<td>
						<a href="#edit" class="pbp-edit" data-id_tab="{$tab->id_tab|escape:'htmlall':'UTF-8'}" style="padding:6px 8px"><i class="icon icon-pencil"></i></a>
						<a href="#delete" class="pbp-delete" data-id_tab="{$tab->id_tab|escape:'htmlall':'UTF-8'}" style="padding:6px 8px"><i class="icon icon-trash"></i></a>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

	<a href="#add-tab" id="add-tab" class="btn btn-default" style="margin-top:20px;">
		<i class="icon-plus-sign"></i> {l s='Add a new Tab' mod='productbundlespro'}
	</a>
</div>
<script>
	module_config_url = '{$module_config_url|escape:'quotes':'UTF-8'}';
	console.log(module_config_url);
	$(document).ready(function () {
		pbp_tabs = new PBPAdminConfigTabsController("#pbp-tabs");
	});
</script>