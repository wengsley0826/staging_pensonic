{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="pbp-bundles" class="panel product-tab">

	<h3>{l s='Bundles' mod='productbundlespro'}</h3>

	<div class="alert alert-info">
		Don't forget - you must add the short code
		<strong>{literal}{hook h="displayPBP"}{/literal}</strong> to your themes product.tpl to display the bundles.
	</div>

	<table id="bundlesTable" class="table tableDnD">
		<thead>
		<tr class="nodrag nodrop">
			<th><span class="title_box">{l s='ID' mod='productbundlespro'}</span></th>
			<th><span class="title_box">{l s='Tab' mod='productbundlespro'}</span></th>
			<th><span class="title_box">{l s='Products' mod='productbundlespro'}</span></th>
			<th><span class="title_box">{l s='Action' mod='productbundlespro'}</span></th>
			<th><span class="title_box">{l s='Position' mod='productbundlespro'}</span></th>
		</tr>
		</thead>

		<tbody>
		{foreach from=$product_bundles item=bundle}
			<tr data-id="{$bundle->id_bundle|escape:'htmlall':'UTF-8'}">
				<td>{$bundle->id_bundle|escape:'htmlall':'UTF-8'}</td>
				<td>{$bundle->tab->title|escape:'htmlall':'UTF-8'}</td>
				<td>{$bundle->products|@count|escape:'htmlall':'UTF-8'}</td>
				<td>
					<a href="#edit" class="pbp-edit-bundle btn" data-id="{$bundle->id_bundle|escape:'htmlall':'UTF-8'}"><i class="icon icon-pencil"></i></a>
					<a href="#copy" class="pbp-copy-bundle btn" data-id="{$bundle->id_bundle|escape:'htmlall':'UTF-8'}"><i class="icon icon-copy"></i></a>
					<a href="#export" class="pbp-export-bundle btn" data-id="{$bundle->id_bundle|escape:'htmlall':'UTF-8'}"><i class="icon icon-truck"></i></a>
					<a href="#delete" class="pbp-delete-bundle btn" data-id="{$bundle->id_bundle|escape:'htmlall':'UTF-8'}"><i class="icon icon-trash"></i></a>
				</td>
				<td class="dragHandle pointer">
					<div class="dragGroup">
						&nbsp;
					</div>
				</td>
			</tr>
		{/foreach}
		</tbody>
	</table>

	<a href="#add_bundle" id="pbp-add-bundle" class="btn btn-default" style="margin-top:20px;">
		<i class="icon-plus-sign"></i> {l s='Add a bundle' mod='productbundlespro'}
	</a>

	<a href="#add_bundle_existing" id="pbp-add-bundle-existing" class="btn btn-default" style="margin-top:20px;">
		<i class="icon-plus-sign"></i> {l s='Add a existing bundle from another product' mod='productbundlespro'}
	</a>

</div>

<script>
	module_tab_url = '{$module_tab_url|escape:'quotes':'UTF-8'}';
	module_url = '{$module_url|escape:'quotes':'UTF-8'}';
	pbp_ajax_url = "{$link->getModuleLink('productbundlespro', 'ajax', array())|escape:'quotes':'UTF-8'}";

	http_get = {$smarty.get|json_encode};
	$(document).ready(function () {
		if (typeof(pbp_admin_tab_bundles_controller) === 'undefined')
			pbp_admin_tab_bundles_controller = new PBPAdminTabBundlesController("#pbp-bundles");
	});
</script>