{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

{if !empty($id_product)}
	<input type="hidden" name="id_product" value="{$id_product|intval}">
{/if}

<div id="panel1" class="panel">
	<h3>Add / Edit Bundle</h3>

	<div class="row">
		<div class="col-lg-12">

			<div class="form-group">
				<label class="control-label col-lg-3">
					{l s='Bundle Enabled' mod='productbundlespro'}
				</label>

				<div class="col-lg-9 ">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="enabled" id="enabled_on" value="1" {if $bundle->enabled eq "1"}checked{/if}>
						<label for="enabled_on">Yes</label>
						<input type="radio" name="enabled" id="enabled_off" value="0" {if $bundle->enabled neq "1"}checked{/if}>
						<label for="enabled_off">No</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-4" for="spm_currency_0">
					<span class="label-tooltip" title="Choose a tab">Choose a tab</span>
				</label>

				<div class="col-lg-6">
					<select id="id_tab" name="id_tab">
						{foreach from=$tabs_collection item=tab}
							{if isset($bundle) && $bundle->id_tab eq $tab->id_tab}
								<option value="{$tab->id_tab|escape:'htmlall':'UTF-8'}" selected>{$tab->title|escape:'htmlall':'UTF-8'}</option>
							{else}
								<option value="{$tab->id_tab|escape:'htmlall':'UTF-8'}">{$tab->title|escape:'htmlall':'UTF-8'}</option>
							{/if}
						{/foreach}
					</select>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">

				<div class="col-lg-6">
					<a id="pbp-edit-product" href="#pbp-edit-product" class="btn btn-default">
						<i class="icon icon-list"></i>
						<span>{l s='Add Product' mod='productbundlespro'}</span>
					</a>
				</div>

			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-12">
			<div id="pbp-bundle-products"></div>
		</div>
	</div>


	<div class="panel-footer">
		<a id="pbp-popup-close" href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="pbp-popup-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> {l s='Save' mod='productbundlespro'}
		</button>
	</div>
</div>

{* Start : Panel2 Dropdown Values *}
<div id="panel2" class="panel subpanel">
	<h3>Add / Edit Product in bundle</h3>

	<input type="hidden" name="id_pbp_product" id="id_pbp_product" value="" />

	<div class="row">
		<div class="form-group">
			<label class="control-label col-lg-2">
				<span class="label-tooltip" title="">Search</span>
			</label>

			<div class="col-lg-8">
				<div class="input-group col-xs-9">
					<input id="product_search" type="text" name="product_search" value="">
					<span class="input-group-addon">
						<i class="icon-search"></i>
					</span>
				</div>

				{* Product search results *}
				<div id="product-search-results">
					<input id="id_product" name="id_product" type="hidden">

					<table id="search-results-table" class="table">
						<thead>
						<tr class="nodrag nodrop">
							<th style="width:10%"><span class="title_box">{l s='ID' mod='productbundlespro'}</span></th>
							<th style="width:30%"><span class="title_box">{l s='Ref' mod='productbundlespro'}</span></th>
							<th><span class="title_box">{l s='Name' mod='productbundlespro'}</span></th>
						</tr>
						</thead>
						<tbody>
						<tr class="cloneable hidden" style="cursor:pointer">
							<td class="id_product" data-bind="id_product"></td>
							<td class="name" data-bind="name"></td>
						</tr>
						</tbody>
					</table>
				</div>
				{* / Product Search Results *}
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-2">
				<span class="label-tooltip" title="">Apply Discount of</span>
			</label>

			<div class="col-lg-8">
				<div class="input-group col-xs-9">
					<input id="discount_amount" type="text" name="discount_amount" value="">
					<select name="discount_type" id="discount_type">
						<option value="percentage">%</option>
						<option value="money">Decimal/Money</option>
					</select>
				</div>
			</div>
		</div>

	<div class="panel-footer">
		<a id="pbp-edit-product-cancel" href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>

		<button type="submit" id="pbp-edit-product-done" class="btn btn-default pull-right">
			<i class="process-icon-check icon-check"></i> Done
		</button>
	</div>

</div>
{* End: Panel2 Dropdown Values *}