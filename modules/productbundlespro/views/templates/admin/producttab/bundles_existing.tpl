{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="pbp-bundle_existing" class="panel">
	<h3>Add Existing Bundle</h3>

	<div class="row">

		{* start: left *}
		<div class="col-xs-6">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label class="control-label">
							<span class="label-tooltip" title="Choose a tab">Choose a tab</span>
						</label>

						<select id="id_tab" name="id_tab">
							{foreach from=$tabs_collection item=tab}
								{if isset($bundle) && $bundle->id_tab eq $tab->id_tab}
									<option value="{$tab->id_tab|escape:'htmlall':'UTF-8'}" selected>{$tab->title|escape:'htmlall':'UTF-8'}</option>
								{else}
									<option value="{$tab->id_tab|escape:'htmlall':'UTF-8'}">{$tab->title|escape:'htmlall':'UTF-8'}</option>
								{/if}
							{/foreach}
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">
							<span class="label-tooltip" title="type in the first few characters of a product name to select a bundle from">{l s='Type in product name' mod='productbundlespro'}</span>
						</label>

						<div class="input-group">
							<input id="product_search" type="text" name="product_search" value="">
							<span class="input-group-addon">
								<i class="icon-search"></i>
							</span>
						</div>

						{* Product search results *}
						<div id="product-search-results">
							<input name="id_product" type="hidden">

							<table id="search-results-table" class="table">
								<thead>
								<tr class="nodrag nodrop">
									<th style="width:10%"><span class="title_box">{l s='ID' mod='productbundlespro'}</span></th>
									<th style="width:30%"><span class="title_box">{l s='Ref' mod='productbundlespro'}</span></th>
									<th><span class="title_box">{l s='Name' mod='productbundlespro'}</span></th>
								</tr>
								</thead>
								<tbody>
								<tr class="cloneable hidden" style="cursor:pointer">
									<td class="id_product" data-bind="id_product"></td>
									<td class="name" data-bind="name"></td>
								</tr>
								</tbody>
							</table>
						</div>
						{* / Product Search Results *}



					</div>

				</div>
			</div>
		</div>
		{* end: left *}

		{* start: right *}
		<div class="col-xs-6">
			<div class="row">
				<div class="col-xs-12">
					<div id="pbp-bundles-existing-list"></div>
				</div>
			</div>
		</div>
		{* end: right *}

	</div>


	<div class="row">
		<div class="panel-footer">
			<a id="pbp-edit-product-cancel" href="#close" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>

			<button type="submit" id="pbp-existing-bundle-add" class="btn btn-default pull-right">
				<i class="process-icon-check icon-check"></i> Add
			</button>
		</div>

	</div>