{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="pbp-general" class="panel">
    <h3>General Options</h3>

    <div class="row">
		<div class="col-md-3">
			{if !empty($pbp_product_option->disabled_addtocart)}
				<input type="checkbox" name="pbp-disabled_addtocart" id="pbp-disabled_addtocart" style="margin-right:10px;" value="1" checked>
			{else}
				<input type="checkbox" name="pbp-disabled_addtocart" id="pbp-disabled_addtocart" style="margin-right:10px;" value="1">
			{/if}
			<label class="control-label">
				Disable parent product Add to Basket?
			</label>
		</div>
    </div>

	<div class="panel-footer">
		<button type="button" value="1" id="pbp-general-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>

<script>
	module_tab_url = '{$module_tab_url|escape:'quotes':'UTF-8'}';
	module_url = '{$module_url|escape:'quotes':'UTF-8'}';
	$(document).ready(function () {
		if (typeof(pbp_admin_tab_general_controller) === 'undefined')
			pbp_admin_tab_general_controller = new PBPAdminTabGeneralController("#pbp-general");
	});
</script>