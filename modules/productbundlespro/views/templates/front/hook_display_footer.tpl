{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<script>
	$(document).ready(function() {
		var pbp_front_ajax_url = "{$link->getModuleLink('productbundlespro', 'ajax', array())|escape:'quotes':'UTF-8'}";
		var url = pbp_front_ajax_url + '?route=pbpproductcontroller&action=hookrendertabs&rand=' + new Date().getTime();

		var pbp_product_options = {$pbp_product_options|@json_encode};
		var pbp_general = {$pbp_general|@json_encode};

		if (typeof(pbp_product_options) !== 'undefined') {
			if (pbp_product_options.disabled_addtocart == 1)
				$("#add_to_cart").hide();
		}

		$.get(
			url,
			{
				id_product : id_product
			},
		function (data) { // Loads content into the 'data' variable.

			switch (pbp_general.pbp_location) {
				case 'description-after' :
					$('.pb-center-column').append(data);
					break;
				default:
					$('.primary_block').after(data);
					break;
			}
		});
	});
</script>

{if !empty($pbp_product_options->disabled_addtocart)}
	<style>
		#add_to_cart, #aw_add_to_cart {
			display: none !important;
		}
	</style>
{/if}