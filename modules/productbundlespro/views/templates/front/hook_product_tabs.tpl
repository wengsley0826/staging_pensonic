{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div class="row">
	<div id="pbp-product-tabs" class="col-xs-12 {$pbp_general.pbp_location|escape:'htmlall':'UTF-8'}">
		{if $tabs_collection|@count gt 1}
			<div class="tab-links">
				{$i = 1}
				{foreach from=$tabs_collection item=tab}
					<a data-tab_id="{$tab->id|escape:'htmlall':'UTF-8'}" class="{if $i==1}active{/if}">{$tab->title|escape:'htmlall':'UTF-8'}</a>
					{$i = $i+1}
				{/foreach}
			</div>
		{/if}

		{$counter = 1}
		{foreach from=$tabs_collection item=tab}
			<div class="tab-content-wrapper">
				<div class="pbp-tab-content pbp-tab-content-{$tab->id|escape:'htmlall':'UTF-8'}" style="{if $counter gt 1}display:none;{/if}">
					{foreach from=$tab->bundles item=bundle}
						<div class="bundle col-xs-12 col-sm-12 col-md-6" data-id_bundle="{$bundle->id_bundle|escape:'htmlall':'UTF-8'}">

							<div class="content">
								<span class="bundle-title">
									{l s='Buy it with ' mod='productbundlespro'} 
									<span class="product-title">
										{$bundle->products[0]->name|escape:'htmlall':'UTF-8'}
									</span>
									{*{$parent_product->name[$id_lang]}*}
								</span>

								<div class="parent">
									<img itemprop="image" src="{$link->getImageLink($parent_product->link_rewrite, $parent_product->cover_image, 'medium_default')|escape:'html':'UTF-8'}" />
								</div>

								<div class="bundle-products">
									{if !empty($bundle->products)}
										{foreach from=$bundle->products item=bundle_product}
											<div class="bundle-product" data-id_product="{$bundle_product->id_product|escape:'htmlall':'UTF-8'}">
												<div class="image">
													<a href="{$bundle_product->url|escape:'htmlall':'UTF-8'}">
														<img src="{$link->getImageLink($bundle_product->link_rewrite, $bundle_product->id_image, 'medium_default')|escape:'html':'UTF-8'}" class="bundle_product_thumb"/>
													</a>	
												</div>

												<div class="info">
													{if $bundle_product->discount_type eq 'percentage'}
														<span class="pbp-discount">
															{l s='SAVE:' mod='productbundlespro'}
															<span class="pbp-discount-amount">{convertPrice price=$bundle_product->product->discount_saving|floatval} ({$bundle_product->discount_amount|string_format:"%.0f"}%)</span>
														</span>
														<span class="pbp-discount">
															{l s='Offer Price:' mod='productbundlespro'}
															<span class="pbp-offer-price">{convertPrice price=$bundle_product->product->discount_price|floatval}</span>
														</span>
													{else}
														<span class="pbp-discount">
															{l s='SAVE:' mod='productbundlespro'}
															<span class="pbp-discount-amount">{convertPrice price=$bundle_product->product->discount_saving|floatval}</span>
														</span>
														<span class="pbp-discount">
															{l s='Offer Price:' mod='productbundlespro'}
															<span class="pbp-offer-price">{convertPrice price=$bundle_product->product->discount_price|floatval}</span>
														</span>
													{/if}
													{if count($bundle_product->attribute_groups) > 0}
														<select class="bundle_product_ipa">
															{foreach from=$bundle_product->attribute_groups item=attribute_group name=attribute_group}
																<option value="{$attribute_group.id_product_attribute|escape:'htmlall':'UTF-8'}">{$attribute_group.label|escape:'htmlall':'UTF-8'}</option>
															{/foreach}
														</select>
													{/if}
												</div>
											</div>
										{/foreach}
									{/if}
								</div>

								<div class="box-info-product">
									{if $pbp_general.pbp_display_bundle_total eq 1}
										<div class="bundle-total">
											{displayPrice price=$bundle->bundle_price_discounted}
										</div>
									{/if}
									<button type="submit" name="Submit" class="btn_add_bundle_cart exclusive">
										<span>{l s='Add to cart' mod='productbundlespro'}</span>
									</button>
								</div>
							</div>
						</div>
					{/foreach}
				</div>
			</div>
			{$counter = $counter+1}
		{/foreach}
	</div>
</div>


<script>
	$(document).ready(function() {
		pbp_front_ajax_url = "{$link->getModuleLink('productbundlespro', 'ajax', array())|escape:'quotes':'UTF-8'}";
		var pbp_front_product_controller = new PBPFrontProductController('#pbp-product-tabs');
	});
</script>

