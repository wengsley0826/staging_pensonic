<?php

class SenangPayCallbackModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $response = array();
        
        foreach($_REQUEST as $key => $val) { 
            $response[$key] = $val;
        }

        $spayId = $response["order_id"];
        $status = $response["status_id"];
        $hash = $response["hash"];
        
        $transId = "";
        $msg = "";

        if(isset($response["transaction_id"])) {
            $transId = $response["transaction_id"];
        }

        if(isset($response["msg"])) {
            $msg = $response["msg"];
        }
    
    
        $logId = $this->SaveLog($_REQUEST, $spayId, $status, $transId);
        $logResult = "";
        sleep(1);
        
        $id_cart = $this->getCartIdBySPayId($spayId);
        $redirect_checkout = false;
        
        if($this->CheckDuplicateLog($logId))
        {
            if($status == "1") 
            {
                $maxRetry = 20;
                $retry_cnt = 1;
                sleep(10);
                $o = $this->getOrderId($id_cart);
                while ($o <= 0 && $retry_cnt < $maxRetry) {
                    $o = $this->getOrderId($id_cart);
                    $retry_cnt++;
                    sleep(10);    
                }

                if($o > 0) 
                {
                    echo "OK";
                    die();
                }
            }
            else {
                $o = 0;
                echo "OK";
                die();
            }            
        }
        
        if (empty($id_cart)) {
            exit;
        }    
        else {
            $cart = new Cart($id_cart);
        }
        
        $customer = new Customer($cart->id_customer);
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);

        $secretkey = Configuration::get('SPAY_SECRET_KEY');
        $secureHash = hash_hmac('sha256', $secretkey.urldecode($status).urldecode($spayId).urldecode($transId).urldecode($msg), $secretkey);
        
        if($secureHash == $hash) {
            if($status == "1") {
                $logResult .= "Process Success Order.";
                $this->UpdateLog($logId, $logResult);
                
                if(!$this->processOrder($id_cart, $total, $transId, Configuration::get('PS_OS_PAYMENT'), $spayId)) 
                {
                    $extra_vars = array();
                    $extra_vars['transaction_id'] = $transId;

                    $this->module->validateOrder($id_cart, Configuration::get('PS_OS_PAYMENT'), $total, $this->module->displayName, 
                        'SenangPay Transaction ID: ' . $transId. ' (SenangPay Id: '. $spayId. ')'.$msg, $extra_vars, (int)$cart->id_currency, false, $customer->secure_key);
                    $logResult .= "||Order Created";
                    $this->UpdateLog($logId, $logResult);                    
                }
            }
            else {
                $logResult .= "Payment Failed";
                $logResult .= "||Redirected".$msg;
                $redirect_checkout = true;
                $this->UpdateLog($logId, $logResult);
            }
        }
        else {
            $status = "hash";
            $logResult .= "Invalid Hash";
            $logResult .= "||Redirected".$msg;
            $redirect_checkout = true;
            $this->UpdateLog($logId, $logResult);
        }
        
        if($redirect_checkout)
        {
            $logResult .= "||Redirect Payment Result";
            $this->UpdateLog($logId, $logResult);          
            
        }
        else {
            $logResult .= "||Before update Payment Result";
            $this->UpdateLog($logId, $logResult);          
            
            $maxRetry = 30;
            $retry_cnt = 1;
              
            $o = $this->getOrderId($id_cart);
            while ($o <= 0 && $retry_cnt < $maxRetry) {
                $o = $this->getOrderId($orderid);
                $retry_cnt++;
                sleep(5);    
            }
            

            $this->UpdatePaymentResult($spayId, $o, $status, $transId);     
            $logResult .= "||Update Payment Result (o=>". $o.")";
            $this->UpdateLog($logId, $logResult);          
              
        }
        
        echo "OK";
        die();
    }
    
    private function UpdatePaymentResult ($spayId, $id_order, $payment_status, $transId) {
        $sql = 'UPDATE `'._DB_PREFIX_.'senangpay_cart` '.
                'SET payment_status="'.$payment_status.'",'.
                    'transaction_no="'.$transId.'",'.
                    'id_order="'.$id_order.'" '.
                'WHERE spayId='.(int)$spayId;
        return Db::getInstance()->execute($sql);
    }
    
    private function SaveLog ($data, $spayId, $paymen_status, $transId)
    {
        $postdata = json_encode($data);
        $postdata = str_replace('"', '\"', $postdata);
        
        $sql = 'INSERT INTO `'._DB_PREFIX_.'payment_callback_log` (`module`, `file`,`orderId`, `payment_status`,`transaction_no`,`postdata`,`createdTS`) '.
                'VALUES ("SenangPay", "callback","'.$spayId.'","'.$paymen_status.'","'.$transId.'","'.$postdata.'", NOW())';
        $return = Db::getInstance()->execute($sql);
        return Db::getInstance()->Insert_ID();
    }
    
    private function getCartIdBySPayId($spayId) {
        $sql = 'SELECT id_cart FROM `'. _DB_PREFIX_ .'senangpay_cart` WHERE spayId = '. $spayId;
        $cartId = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        
        return $cartId;
    }
    
    private function getOrderId($id_cart) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
       
        if ($row){
            return $row["id_order"];
        }
        else {
            return 0;
        }       
    }
    
    private function CheckDuplicateLog($logId)
    {
        $tempSQL = "SELECT COUNT(1) AS logCount FROM `". _DB_PREFIX_ ."payment_callback_log` A ".
                    "INNER JOIN `". _DB_PREFIX_ ."payment_callback_log` B ON ".
                        "A.payment_status = B.payment_status AND A.transaction_no=B.transaction_no AND ".
                        "A.orderId=B.orderId AND A.id < B.id ".
                    "WHERE B.id=". (int)$logId;
        $stateCount = Db::getInstance()->getRow($tempSQL);
      
        if($stateCount["logCount"] > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    
    private function UpdateLog($logId, $logVal) {
        $sql = 'UPDATE `'._DB_PREFIX_.'payment_callback_log` SET result="'.$logVal.'" WHERE id='.(int)$logId;
        $return = Db::getInstance()->execute($sql);
    }
    
    private function processOrder($id_cart, $amount, $tranID, $id_order_state, $spayId) {
        //// check is the cart exist in the order
        $sql = 'SELECT id_order FROM `'. _DB_PREFIX_ .'orders` WHERE id_cart = '. $id_cart;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
        if ($row){
            $extra_vars = array();
            $extra_vars['transaction_id'] = $tranID;
            return $this->module->updateValidatedOrder(
                $id_cart, $row["id_order"], $id_order_state, $amount, $this->module->displayName, 
                'SenangPay Transaction ID: ' . $tranID. ' (SenangPay Id: '. $spayId. ')', $extra_vars, false);
        }
        else {
            return false;
        }    
    }
}