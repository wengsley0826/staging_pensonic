<?php

class SenangPayCancelModuleFrontController extends ModuleFrontController
{    
    public function postProcess()
    {   
        $logId = $this->SaveLog($_REQUEST);
        sleep(1);
        Tools::redirect('index.php?controller=order&redirect_error=Payment Cancel');
    }

    private function SaveLog ($data)
    {
        $postdata = json_encode($data);
        $postdata = str_replace('"', '\"', $postdata);
        
        $sql = 'INSERT INTO `'._DB_PREFIX_.'payment_callback_log` (`module`, `file`,`orderId`, `payment_status`,`transaction_no`,`receipt_no`,`postdata`,`createdTS`) '.
                'VALUES ("SenangPay", "cancel","0","","","", "'.$postdata.'", NOW())';
        $return = Db::getInstance()->execute($sql);
        return Db::getInstance()->Insert_ID();
    }
}