<?php

class SenangPayRedirectModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        //$result = array();
        $cart = $this->context->cart;
        
        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
            Tools::redirect('index.php?controller=order&step=1');
        
        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module)
            if ($module['name'] == 'senangpay') {
                $authorized = true;
                break;
            }
        
        if (!$authorized)
            die($this->module->l('This payment method is not available.', 'validation'));
        
        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');
        
        $currency = $this->context->currency;
        
        //total_product amount
        if ($this->context->currency->decimals == 0) {
            $productsPrice = round(Context::getContext()->cart->getOrderTotal(true));
        } else {
            $productsPrice = Context::getContext()->cart->getOrderTotal(true);
        }
        
        $curTS =  date("Y-m-d H:i:s");
        Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'senangpay_cart` (`id_cart`, `date_add`, `date_upd`, total_paid) VALUES ('.$cart->id.',"'.$curTS.'","'.$curTS.'",'.$productsPrice.')');
        $spayId = (int)Db::getInstance()->Insert_ID();
        
        Db::getInstance()->execute(
            'INSERT INTO `'._DB_PREFIX_.'senangpay_cartproduct` '.
                '(`spayId`,`id_product`,`id_address_delivery`,`id_shop`,`id_product_attribute`,`quantity`,`date_add`) '.
                'SELECT '.$spayId.',`id_product`,`id_address_delivery`,`id_shop`,`id_product_attribute`,`quantity`,"'.$curTS.'" '.
                'FROM `'._DB_PREFIX_.'cart_product` '.
                'WHERE id_cart='.$cart->id
        );

        Db::getInstance()->execute(
            'INSERT INTO `'._DB_PREFIX_.'senangpay_cartrule` '.
                '(`spayId`,`id_cart_rule`,`date_add`) '.
                'SELECT '.$spayId.',`id_cart_rule`,"'.$curTS.'" '.
                'FROM `'._DB_PREFIX_.'cart_cart_rule` '.
                'WHERE id_cart='.$cart->id
        );

        $detail = "Pensonic_eStore_Cart_".$cart->id ;
       
        $arrayArg = array(
            'spayId' => $spayId,
            'amount' => $productsPrice,
            'detail' => $detail,
            'id_address_invoice' => $cart->id_address_invoice
        );
        
        $this->constrctRequest($arrayArg);

        exit;
    }
    
    private function constrctRequest($arg)
    {
        $addr = Db::getInstance()->getRow(
            'SELECT A.firstname,A.lastname,B.email,A.phone,A.phone_mobile '.
            'FROM  `'._DB_PREFIX_.'address` A '.
            'INNER JOIN  `'._DB_PREFIX_.'customer` B ON A.id_customer=B.id_customer '.
            'WHERE A.id_address='.$arg['id_address_invoice']);

        $phone = $addr['phone_mobile'] != "" ? $addr['phone_mobile'] : $addr['phone'];

        $data = array(
            "amount" => $arg['amount'],
            "order_id" => $arg['spayId'],
            "detail" => $arg['detail'],
            "name" => $addr['firstname'].' '.$addr['lastname'],
            "email" => $addr['email'],
            "phone" => $phone
            );
        
        $isLiveMode = Configuration::get('SPAY_LIVE_MODE');
        $actionUrl = "https://sandbox.senangpay.my/payment/";

        if($isLiveMode) {
            $actionUrl = "https://app.senangpay.my/payment/";
        }

        $merchantId = Configuration::get('SPAY_MERCHANT_ID');
        $actionUrl .= $merchantId;
        $secretkey = Configuration::get('SPAY_SECRET_KEY');

        $hashed_string = hash_hmac('sha256', $secretkey.urldecode($arg['detail']).urldecode($arg['amount']).urldecode($arg['spayId']), $secretkey);

        echo "<form id='senangpayform' method='post' action='".$actionUrl."'>";
        foreach ($data as $k => $v) {
            echo "<input type='hidden' name='$k' value='$v'>";
        }
        echo "<input type=\"hidden\" id=\"hash\" name=\"hash\" value=\"" . $hashed_string . "\"/>\n";
        echo "</form>";
        echo "<script type='text/javascript'>document.getElementById('senangpayform').submit();</script>";
    }
}