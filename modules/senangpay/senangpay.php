<?php

if (!defined('_PS_VERSION_'))
	exit;

class SenangPay extends PaymentModule
{
	protected $_html = '';
	protected $_postErrors = array();

	public $merchantId;
	public $secretKey;
    
	public function __construct()
	{
		$this->name = 'senangpay';
		$this->tab = 'payments_gateways';
		$this->version = '1.0.0';
		$this->author = 'Claritas';
		$this->controllers = array('callback', 'redirect', 'validation');
		$this->is_eu_compatible = 1;

		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		$config = Configuration::getMultiple(array(
            'SPAY_MERCHANT_ID', 'SPAY_SECRET_KEY'
        ));
            
		if (!empty($config['SPAY_MERCHANT_ID']))
			$this->merchantId = $config['SPAY_MERCHANT_ID'];
		if (!empty($config['SPAY_SECRET_KEY']))
			$this->secretKey = $config['SPAY_SECRET_KEY'];
        
		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('SenangPay');
		$this->description = $this->l('Accept payments for your products via SenangPay.');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
		if (!isset($this->merchantId) || !isset($this->secretKey))
			$this->warning = $this->l('Merchant ID and Secret Key must be configured before using this payment method');	
	}

	public function install()
	{	
		if (!parent::install() || !$this->registerHook('payment') || 
            ! $this->registerHook('displayPaymentEU') || !$this->registerHook('paymentReturn') ||
            ! $this->registerHook('UpdateOrderStatus') )
			return false;

		// install DataBase
        if (!$this->installSQL()) {
            return false;
        }
        
		return true;
	}

	public function uninstall()
	{
		if (!Configuration::deleteByName('SPAY_MERCHANT_ID') 
            || !Configuration::deleteByName('SPAY_SECRET_KEY') 
            || !Configuration::deleteByName('SPAY_LIVE_MODE') 
            || !$this->uninstallSQL() || !parent::uninstall())
            return false;
        
        return true;
	}

	protected function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('SPAY_MERCHANT_ID')) {
                $this->_postErrors[] = $this->l('Merchant ID is required!');
            }
            if (!Tools::getValue('SPAY_SECRET_KEY')) {
                $this->_postErrors[] = $this->l('Secret Key is required!');
            }    
        }
	}

	protected function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('SPAY_MERCHANT_ID', Tools::getValue('SPAY_MERCHANT_ID'));
            Configuration::updateValue('SPAY_SECRET_KEY', Tools::getValue('SPAY_SECRET_KEY'));
            Configuration::updateValue('SPAY_LIVE_MODE', Tools::getValue('SPAY_LIVE_MODE'));            
        }

        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}

	public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}
		else
			$this->_html .= '<br />';

		$this->_html .= $this->renderForm();

		return $this->_html;
	}

	public function hookPayment($params)
	{
		if (!$this->active)
			return;

		$this->smarty->assign(array(
			'this_path_cs' => $this->_path,
        ));

		return $this->display(__FILE__, 'payment.tpl');
	}

	public function hookDisplayPaymentEU($params)	
	{			
		if (!$this->active)
			return;

		$payment_options = array(
			'cta_text' => $this->l('Pay by Visa / Master'),			
			'logo' => Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/credit.png'),
			'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true)
		);

		return $payment_options;
	}

	public function hookPaymentReturn($params)
	{        
		if (!$this->active)
			return;

		$state = $params['objOrder']->getCurrentState();

		$id_order = (int) Order::getOrderByCartId($params['objOrder']->id_cart);

        $sql = 'SELECT payment_status, transaction_no, receipt_no, id_order FROM `'. _DB_PREFIX_ .'senangpay_cart` WHERE id_order = '. $id_order.' ORDER BY date_upd DESC';
        $paymentinfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        
		if (in_array($state, array(Configuration::get('PS_OS_PAYMENT'),Configuration::get('PS_OS_AWAITING'))))
		{
			$this->smarty->assign(array(                
                'status' => 'ok',
                'amount' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                'order_id' => $id_order,
                'order_ref' => $params['objOrder']->reference,
                'transaction_ref' => $paymentinfo['transaction_no'],
                'payment_status' => $paymentinfo['payment_status'],
            ));
		}
		else{
			$this->smarty->assign(array(
                'status' => 'failed',
                'amount' => round($params['objOrder']->getOrdersTotalPaid(), 2),
                'order_id' => $id_order,
                'order_ref' => $params['objOrder']->reference,
                'transaction_ref' => $paymentinfo['transaction_no'],
                'payment_status' => $paymentinfo['payment_status'],
            ));
		}

		return $this->display(__FILE__, 'payment_return.tpl');
	}
	
	private function installSQL() {

        $sql = array();
        
        foreach ($sql as $q) {
            if (!DB::getInstance()->execute($q)) {
                return false;
            }
        }
				
        return true;
    }

    private function uninstallSQL() {

        $sql = array();
        
        return true;
    }

	public function renderForm()
	{
    	 $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('SenangPay Details'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Merchant Id'),
                        'name' => 'SPAY_MERCHANT_ID',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Secret Key'),
                        'name' => 'SPAY_SECRET_KEY',
                        'required' => true
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('SenangPay Live'),
                        'name' => 'SPAY_LIVE_MODE',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('LIVE')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('SANDBOX')
                            )
                        ),
                    ),                    
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );
        
        $helper                           = new HelperForm();
        $helper->show_toolbar             = false;
        $helper->table                    = $this->table;
        $lang                             = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form                = array();
        $helper->id                       = (int) Tools::getValue('id_carrier');
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'btnSubmit';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm(array(
            $fields_form
        ));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'SPAY_MERCHANT_ID' => Tools::getValue('SPAY_MERCHANT_ID', Configuration::get('SPAY_MERCHANT_ID')),
            'SPAY_SECRET_KEY' => Tools::getValue('SPAY_SECRET_KEY', Configuration::get('SPAY_SECRET_KEY')),
            'SPAY_LIVE_MODE' => Tools::getValue('SPAY_LIVE_MODE', Configuration::get('SPAY_LIVE_MODE'))
		);
	}	
    
    public function hookUpdateOrderStatus($params) {
        
    }
    
    private function updateOrderStatus($id_order, $employeeId, $to_status) 
    {
        $new_history = new OrderHistory();
		$new_history->id_order = (int)$id_order;
        $new_history->id_employee = (int)$employeeId;
		$new_history->changeIdOrderState((int)$to_status, $id_order, true);
		$new_history->addWithemail(true);
    }
}
