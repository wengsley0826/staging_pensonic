<?php

if (!defined('_PS_VERSION_'))
	exit;

class StatsCustByDOB extends ModuleGrid
{
	private $html = '';
    private $query;
	private $columns;
	private $default_sort_column;
	private $default_sort_direction;
	private $empty_message;
	private $paging_message;
    
	public function __construct()
	{
		$this->name = 'statscustbydob';
		$this->tab = 'analytics_stats';
		$this->version = '1.6.0';
		$this->author = 'Claritas';
		$this->need_instance = 0;

		parent::__construct();

        $this->empty_message = $this->l('Empty recordset returned.');
        $this->paging_message = sprintf($this->l('Displaying %1$s of %2$s'), '{0} - {1}', '{2}');
        
        $this->columns = array(
			array(
				'id' => 'id_customer',
				'header' => $this->l('Customer ID'),
				'dataIndex' => 'id_customer',
				'align' => 'left'
			),
			array(
				'id' => 'firstname',
				'header' => $this->l('First name'),
				'dataIndex' => 'firstname',
				'align' => 'left'
			),
            array(
				'id' => 'lastname',
				'header' => $this->l('Last name'),
				'dataIndex' => 'lastname',
				'align' => 'left'
			),
            array(
				'id' => 'email',
				'header' => $this->l('Email'),
				'dataIndex' => 'email',
				'align' => 'left'
			),
            array(
				'id' => 'mobile',
				'header' => $this->l('Mobile'),
				'dataIndex' => 'mobile',
				'align' => 'left'
			),
			array(
				'id' => 'birthday',
				'header' => $this->l('DOB'),
				'dataIndex' => 'birthday',
				'align' => 'left'
			),
			array(
				'id' => 'verified',
				'header' => $this->l('Verified'),
				'dataIndex' => 'verified',
				'align' => 'center'
			),
            array(
				'id' => 'last_order_date',
				'header' => $this->l('Last Order'),
				'dataIndex' => 'last_order_date',
				'align' => 'left'
			)
		);
        
		$this->displayName = $this->l('Customer - Birthday Month');
		$this->description = $this->l('Adds a tab to filter customers by birthday month to the Stats dashboard.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7.0.99');
       
	}

	public function install()
	{
		return parent::install() && $this->registerHook('AdminStatsModules');
	}

	public function hookAdminStatsModules($params)
	{
        if (Tools::getValue('statsdob_month'))
			$this->context->cookie->statsdob_month = Tools::getValue('statsdob_month');
        else 
            $this->context->cookie->statsdob_month = date('m', strtotime('-1 month'));
        
        $engine_params = array(
			'id' => 'id_customer',
			'title' => $this->displayName,
			'columns' => $this->columns,
			'defaultSortColumn' => $this->default_sort_column,
			'defaultSortDirection' => $this->default_sort_direction,
			'emptyMessage' => $this->empty_message,
			'pagingMessage' => $this->paging_message
		);
        
        if (Tools::getValue('export'))
			$this->csvExport($engine_params);
           
        $ru = AdminController::$currentIndex.'&module='.$this->name.'&token='.Tools::getValue('token');
 
		$this->html .= '
		<script type="text/javascript">$(\'#calendar\').hide();</script>

		<div class="panel-heading">'
			.$this->l('Customer List by Birth month').
		'</div>
		<form action="'.Tools::safeOutput($ru).'" method="post" class="form-horizontal">
			<div class="row row-margin-bottom">
				<label class="control-label col-lg-3">'.$this->l('Birth Month').'</label>
				<div class="col-lg-6">
					<select name="statsdob_month" onchange="this.form.submit();">
						<option value="1" '.($this->context->cookie->statsdob_month == 1 ? 'selected="selected"' : '').'>January</option>
                        <option value="2" '.($this->context->cookie->statsdob_month == 2 ? 'selected="selected"' : '').'>February</option>
                        <option value="3" '.($this->context->cookie->statsdob_month == 3 ? 'selected="selected"' : '').'>March</option>
                        <option value="4" '.($this->context->cookie->statsdob_month == 4 ? 'selected="selected"' : '').'>April</option>
                        <option value="5" '.($this->context->cookie->statsdob_month == 5 ? 'selected="selected"' : '').'>May</option>
                        <option value="6" '.($this->context->cookie->statsdob_month == 6 ? 'selected="selected"' : '').'>June</option>
                        <option value="7" '.($this->context->cookie->statsdob_month == 7 ? 'selected="selected"' : '').'>July</option>
                        <option value="8" '.($this->context->cookie->statsdob_month == 8 ? 'selected="selected"' : '').'>August</option>
                        <option value="9" '.($this->context->cookie->statsdob_month == 9 ? 'selected="selected"' : '').'>September</option>
                        <option value="10" '.($this->context->cookie->statsdob_month == 10 ? 'selected="selected"' : '').'>October</option>
                        <option value="11" '.($this->context->cookie->statsdob_month == 11 ? 'selected="selected"' : '').'>November</option>
                        <option value="12" '.($this->context->cookie->statsdob_month == 12 ? 'selected="selected"' : '').'>December</option>
					</select>
					<input type="hidden" name="submitBirthMonth" value="1" />
				</div>
			</div>
		</form>';

		$this->html .= $this->engine($engine_params).'
			<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=1&statsdob_month='.$this->context->cookie->statsdob_month).'">
				<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
			</a>';
		
        return $this->html;
	}
    
    public function getData() {
    	$this->query = 'SELECT DISTINCT c.id_customer, c.firstname, c.lastname, c.email, c.mobile, CASE WHEN c.verified=1 THEN "Yes" ELSE "No" END AS verified, 
                        CASE WHEN YEAR(c.birthday)=1900 THEN CONCAT("NA-",LPAD(MONTH(c.birthday),2,0),"-",LPAD(DAY(c.birthday),2,0)) ELSE c.birthday END AS birthday,
                        IFNULL(o.last_order_date,"") AS last_order_date
				FROM '._DB_PREFIX_.'customer c
                LEFT JOIN (SELECT id_customer, MAX(date_add) AS last_order_date FROM '._DB_PREFIX_.'orders GROUP BY id_customer) o ON c.id_customer = o.id_customer
                LEFT JOIN '._DB_PREFIX_.'address a ON c.id_customer = a.id_customer AND a.id_state IN (314,325,326) 
                WHERE a.id_address IS NULL AND c.active=1 and c.deleted=0 AND MONTH(c.birthday)='.(int)$this->context->cookie->statsdob_month;
		$values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);

        $this->_values = $values;
        $this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');
    }
}
