<?php

if (!defined('_PS_VERSION_'))
	exit;

class StatsCustByPROD extends ModuleGrid
{
	private $html = '';
    private $query;
	private $columns;
	private $default_sort_column;
	private $default_sort_direction;
	private $empty_message;
	private $paging_message;

	public function __construct()
	{
		$this->name = 'statscustbyprod';
		$this->tab = 'analytics_stats';
		$this->version = '1.6.0';
		$this->author = 'Claritas';
		$this->need_instance = 0;

		parent::__construct();

        $this->empty_message = $this->l('Empty recordset returned.');
        $this->paging_message = sprintf($this->l('Displaying %1$s of %2$s'), '{0} - {1}', '{2}');
        
        $this->columns = array(
			array(
				'id' => 'id_customer',
				'header' => $this->l('Customer ID'),
				'dataIndex' => 'id_customer',
				'align' => 'left'
			),
			array(
				'id' => 'firstname',
				'header' => $this->l('First name'),
				'dataIndex' => 'firstname',
				'align' => 'left'
			),
            array(
				'id' => 'lastname',
				'header' => $this->l('Last name'),
				'dataIndex' => 'lastname',
				'align' => 'left'
			),
            array(
				'id' => 'email',
				'header' => $this->l('Email'),
				'dataIndex' => 'email',
				'align' => 'left'
			),
            array(
				'id' => 'mobile',
				'header' => $this->l('Mobile'),
				'dataIndex' => 'mobile',
				'align' => 'left'
			),
			array(
				'id' => 'birthday',
				'header' => $this->l('DOB'),
				'dataIndex' => 'birthday',
				'align' => 'left'
			),
			array(
				'id' => 'verified',
				'header' => $this->l('Verified'),
				'dataIndex' => 'verified',
				'align' => 'center'
			),
            array(
				'id' => 'id_order',
				'header' => $this->l('Order Id'),
				'dataIndex' => 'id_order',
				'align' => 'left',
			),
            //array(
            //    'id' => 'order_ref',
            //    'header' => $this->l('Order Ref'),
            //    'dataIndex' => 'order_ref',
            //    'align' => 'left'
            //),
            array(
				'id' => 'order_status',
				'header' => $this->l('Order Status'),
				'dataIndex' => 'order_status',
				'align' => 'left'
			),
            array(
				'id' => 'order_date',
				'header' => $this->l('Order Date'),
				'dataIndex' => 'order_date',
				'align' => 'left'
			)
		);
        
		$this->displayName = $this->l('Customer - Order Product');
		$this->description = $this->l('Adds a tab to filter customers by birthday month to the Stats dashboard.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7.0.99');
       
	}

	public function install()
	{
		return parent::install() && $this->registerHook('AdminStatsModules');
	}

	public function hookAdminStatsModules($params)
	{
        $this->context->cookie->statsdob_prod = Tools::getValue('statsdob_prod');
        $this->context->cookie->statsdob_prodid = Tools::getValue('statsdob_prodid');
        $this->context->cookie->statsdob_orderfrom = Tools::getValue('statsdob_orderfrom');
        $this->context->cookie->statsdob_orderto = Tools::getValue('statsdob_orderto');
        
        if(trim($this->context->cookie->statsdob_orderfrom) == "") {
            $this->context->cookie->statsdob_orderfrom = date('Y-m-01');
        }
        
        if(trim($this->context->cookie->statsdob_orderto) == "") {
            $this->context->cookie->statsdob_orderto = date('Y-m-01', strtotime(" +1 months"));
        }
        
        $engine_params = array(
			'id' => 'id_customer',
			'title' => $this->displayName,
			'columns' => $this->columns,
			'defaultSortColumn' => $this->default_sort_column,
			'defaultSortDirection' => $this->default_sort_direction,
			'emptyMsg' => ((trim($this->context->cookie->statsdob_prod) == "" && trim($this->context->cookie->statsdob_prodid) == "") || trim($this->context->cookie->statsdob_orderfrom) == "" ||trim($this->context->cookie->statsdob_orderto) == "") 
                ? "Please fill in product to search and date range" : $this->empty_message,
			'pagingMessage' => $this->paging_message
		);
        
        if (Tools::getValue('export'))
			$this->csvExport($engine_params);
            
        $ru = AdminController::$currentIndex.'&module='.$this->name.'&token='.Tools::getValue('token');

		$this->html .= '
		<script type="text/javascript">$(\'#calendar\').hide();
        $(document).ready(function() {
            if ($("form#product_search_form .datepicker").length > 0)
			    $("form#product_search_form .datepicker").datepicker({
				    prevText: "",
				    nextText: "",
				    dateFormat: "yy-mm-dd"
			    });
	    });
        </script>

		<div class="panel-heading">'
			.$this->l('Customer List by Order Product').
		'</div>
		<form id="product_search_form" action="'.Tools::safeOutput($ru).'" method="post" class="form-horizontal">
		    <div class="row row-margin-bottom">
				<label class="control-label col-lg-3">'.$this->l('Order Date').'</label>
				<div class="col-lg-3">
					<input type="text" name="statsdob_orderfrom" value="'.$this->context->cookie->statsdob_orderfrom.'" class="datepicker form-control" style="cursor:text;color:#555;">
				</div>
                <div class="col-lg-3">
                    <input type="text" name="statsdob_orderto" value="'.$this->context->cookie->statsdob_orderto.'" class="datepicker form-control" style="cursor:text;color:#555;">
                </div>
                <div class="col-lg-3"></div>
			</div>
			<div class="row row-margin-bottom">
				<label class="control-label col-lg-3">'.$this->l('Purchased Product').'</label>
				<div class="col-lg-6">
					<input name="statsdob_prod" type="text" value="'.$this->context->cookie->statsdob_prod.'"/>
				</div>
            </div>
            <div class="row row-margin-bottom">
				<label class="control-label col-lg-3">'.$this->l('Purchased Product ID').'</label>
				<div class="col-lg-6">
					<input name="statsdob_prodid" type="text" value="'.$this->context->cookie->statsdob_prodid.'"/>
				</div>
                <div class="col-lg-3">
                    <button name="Search" value="Search" onclick="this.form.submit();">'.$this->l('Search').'</button>
                </div>
			</div>
		</form>';

		$this->html .= $this->engine($engine_params).'
			<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=1&statsdob_prod='.$this->context->cookie->statsdob_prod.'&statsdob_prodid='.$this->context->cookie->statsdob_prodid.'&statsdob_orderfrom='.$this->context->cookie->statsdob_orderfrom.'&statsdob_orderto='.$this->context->cookie->statsdob_orderto).'">
				<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
			</a>';
		
        return $this->html;
	}
    
    public function getData() {
    	$this->query = 'SELECT DISTINCT c.id_customer, c.firstname, c.lastname, c.email, c.mobile, 
                    CASE WHEN c.verified=1 THEN "Yes" ELSE "No" END AS verified, 
                    CASE WHEN YEAR(c.birthday)=1900 THEN CONCAT("NA-",LPAD(MONTH(c.birthday),2,0),"-",LPAD(DAY(c.birthday),2,0)) ELSE c.birthday END AS birthday, 
                    ('.Tools::$orderStartNo. '+ (o.id_order -1) *'. Tools::$orderIncrement.') AS id_order, 
                    o.reference AS order_ref,osl.name AS order_status, o.date_add AS order_date
				FROM '._DB_PREFIX_.'customer c
                INNER JOIN '._DB_PREFIX_.'orders o ON c.id_customer=o.id_customer
                INNER JOIN '._DB_PREFIX_.'order_detail od ON o.id_order=od.id_order
                INNER JOIN '._DB_PREFIX_.'order_state_lang osl ON o.current_state=osl.id_order_state AND osl.id_lang=1
                WHERE c.active=1 and c.deleted=0 AND o.date_add>="'.$this->context->cookie->statsdob_orderfrom.'" AND o.date_add<"'.$this->context->cookie->statsdob_orderto.'"' ;
                
        if(trim($this->context->cookie->statsdob_prod) != "") {
            $this->query = $this->query. ' AND od.product_name LIKE "%'.$this->context->cookie->statsdob_prod.'%"';
        }
        
        if(trim($this->context->cookie->statsdob_prodid) != "") {
            $this->query = $this->query. ' AND od.product_id= "'.$this->context->cookie->statsdob_prodid.'"';
        }
        
        if(trim($this->context->cookie->statsdob_prod) != "" || trim($this->context->cookie->statsdob_prodid) != "") {  
            $values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);
            $this->_values = $values;
            $this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');
        }
    }
}
