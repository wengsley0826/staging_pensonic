<?php

if (!defined('_PS_VERSION_'))
	exit;
    
class StatsOrderByVoucher extends ModuleGrid
{
	private $html = '';
    private $query;
	private $columns;
	private $default_sort_column;
	private $default_sort_direction;
	private $empty_message;
	private $paging_message;

	public function __construct()
	{
		$this->name = 'statsorderbyvoucher';
		$this->tab = 'analytics_stats';
		$this->version = '1.0.0';
		$this->author = 'Claritas';
		$this->need_instance = 0;

		parent::__construct();

        $this->empty_message = $this->l('Empty recordset returned.');
        $this->paging_message = sprintf($this->l('Displaying %1$s of %2$s'), '{0} - {1}', '{2}');
        
        $this->columns = array(
			array(
				'id' => 'id_order',
				'header' => $this->l('Order ID'),
				'dataIndex' => 'id_order',
				'align' => 'left'
			),
            array(
				'id' => 'order_status',
				'header' => $this->l('Order Status'),
				'dataIndex' => 'order_status',
				'align' => 'left'
			),
            array(
				'id' => 'order_date',
				'header' => $this->l('Order Date'),
				'dataIndex' => 'order_date',
				'align' => 'left'
			),
            array(
				'id' => 'firstname',
				'header' => $this->l('First name'),
				'dataIndex' => 'firstname',
				'align' => 'left'
			),
            array(
				'id' => 'lastname',
				'header' => $this->l('Last name'),
				'dataIndex' => 'lastname',
				'align' => 'left'
			),
            array(
				'id' => 'email',
				'header' => $this->l('Email'),
				'dataIndex' => 'email',
				'align' => 'left'
			),
            array(
				'id' => 'phone',
				'header' => $this->l('Phone'),
				'dataIndex' => 'phone',
				'align' => 'left'
			),
			array(
				'id' => 'payment',
				'header' => $this->l('Payment'),
				'dataIndex' => 'payment',
				'align' => 'left'
			),
			array(
				'id' => 'total_paid',
				'header' => $this->l('Total'),
				'dataIndex' => 'total_paid',
				'align' => 'right'
			),
			array(
				'id' => 'vouchers',
				'header' => $this->l('Vouchers'),
				'dataIndex' => 'vouchers',
				'align' => 'left'
			),
			array(
				'id' => 'products',
				'header' => $this->l('Products'),
				'dataIndex' => 'products',
				'align' => 'left',
                'class' => 'multiline'
			)
		);
        
		$this->displayName = $this->l('Order by voucher');
		$this->description = $this->l('Adds a tab to filter orders by voucher to the Stats dashboard.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7.0.99');
       
	}

	public function install()
	{
		return parent::install() && $this->registerHook('AdminStatsModules');
	}

	public function hookAdminStatsModules($params)
	{
        $this->context->cookie->statsorder_voucher = Tools::getValue('statsorder_voucher');
        $this->context->cookie->statsorder_from = Tools::getValue('statsorder_from');
        $this->context->cookie->statsorder_to = Tools::getValue('statsorder_to');
        
        //if(trim($this->context->cookie->statsorder_from) == "") {
        //    $this->context->cookie->statsorder_from = date('Y-m-01');
        //}
        
        //if(trim($this->context->cookie->statsorder_to) == "") {
        //    $this->context->cookie->statsorder_to = date('Y-m-01', strtotime(" +1 months"));
        //}
        
        $engine_params = array(
			'id' => 'id_order',
			'title' => $this->displayName,
			'columns' => $this->columns,
			'defaultSortColumn' => $this->default_sort_column,
			'defaultSortDirection' => $this->default_sort_direction,
			'emptyMsg' => (trim($this->context->cookie->statsorder_voucher) == "" || trim($this->context->cookie->statsorder_from) == "" ||trim($this->context->cookie->statsorder_to) == "") 
                ? "Please fill in voucher to search and date range" : $this->empty_message,
			'pagingMessage' => $this->paging_message
		);
        
        if (Tools::getValue('export'))
			$this->csvExport($engine_params);
        
        $ru = AdminController::$currentIndex.'&module='.$this->name.'&token='.Tools::getValue('token');  
		
        $this->html .= '
		<script type="text/javascript">$(\'#calendar\').hide();
        $(document).ready(function() {
            if ($("form#order_search_form .datepicker").length > 0)
			    $("form#order_search_form .datepicker").datepicker({
				    prevText: "",
				    nextText: "",
				    dateFormat: "yy-mm-dd"
			    });
	    });
        </script>

		<div class="panel-heading">'
			.$this->l('Order List by Voucher').
		'</div>
		<form id="order_search_form" action="'.Tools::safeOutput($ru).'" method="post" class="form-horizontal">
		    <div class="row row-margin-bottom">
				<label class="control-label col-lg-3">'.$this->l('Order Date').'</label>
				<div class="col-lg-3">
					<input type="text" autocomplete="off" name="statsorder_from" value="'.$this->context->cookie->statsorder_from.'" class="datepicker form-control" style="cursor:text;color:#555;">
				</div>
                <div class="col-lg-3">
                    <input type="text" autocomplete="off" name="statsorder_to" value="'.$this->context->cookie->statsorder_to.'" class="datepicker form-control" style="cursor:text;color:#555;">
                </div>
                <div class="col-lg-3"></div>
			</div>
			<div class="row row-margin-bottom">
				<label class="control-label col-lg-3">'.$this->l('Voucher').'</label>
				<div class="col-lg-6">
					<input name="statsorder_voucher" type="text" value="'.$this->context->cookie->statsorder_voucher.'"/>
					<input type="hidden" name="submitVoucher" value="1" />
    			</div>
                <div class="col-lg-3">
                    <button name="Search" value="Search" onclick="this.form.submit();">'.$this->l('Search').'</button>
                </div>
			</div>
		</form>';

		$this->html .= $this->engine($engine_params).'
			<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=1'.
                '&statsorder_voucher='.$this->context->cookie->statsorder_voucher.
                '&statsorder_from='.$this->context->cookie->statsorder_from.
                '&statsorder_to='.$this->context->cookie->statsorder_to).'">
				<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
			</a>';
		
        return $this->html;
	}
    
    public function getData() {
    	$this->query = 
                'SELECT DISTINCT ('.Tools::$orderStartNo. '+ (C.id_order -1) *'. Tools::$orderIncrement.') AS id_order,
                    D.name AS order_status, C.date_add AS order_date, 
	                E.firstname, E.lastname, E.email, IFNULL(E.mobile,"") AS phone,C.payment, C.total_paid,
                    GROUP_CONCAT(DISTINCT A.name, " [", A.id_cart_rule,"]") AS vouchers,
                    GROUP_CONCAT(F.product_name SEPARATOR "\n") AS products
				FROM '._DB_PREFIX_.'order_cart_rule A
                INNER JOIN '._DB_PREFIX_.'cart_rule B ON A.id_cart_rule = B.id_cart_rule
                INNER JOIN '._DB_PREFIX_.'orders C ON A.id_order = C.id_order
                INNER JOIN '._DB_PREFIX_.'order_state_lang D ON C.current_state = D.id_order_state AND D.id_lang = 1
                INNER JOIN '._DB_PREFIX_.'customer E ON C.id_customer = E.id_customer  
                INNER JOIN '._DB_PREFIX_.'order_detail F ON C.id_order = F.id_order              
                WHERE (
                    A.name LIKE "%'.$this->context->cookie->statsorder_voucher.'%" OR 
                    B.description LIKE "%'.$this->context->cookie->statsorder_voucher.'%" OR
                    B.code LIKE "%'.$this->context->cookie->statsorder_voucher.'%") '.
                ($this->context->cookie->statsorder_from != "" ? 'AND C.date_add>="'.$this->context->cookie->statsorder_from.'"' : '').
                ($this->context->cookie->statsorder_to != "" ? 'AND C.date_add<"'.$this->context->cookie->statsorder_to.'"' : '').
                ' GROUP BY C.id_order';

        if(trim($this->context->cookie->statsorder_voucher) != "") {  
            $values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);
            $this->_values = $values;
            $this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');
        }
    }
}
