{if isset($htmlitems) && $htmlitems}
	{if $hook == "top"}
		{foreach name=items from=$htmlitems item=hItem }
			<div class="htmlcontent-item htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} {if $smarty.foreach.items.index > 3 && ($smarty.foreach.items.index % 4 == 0 || ($smarty.foreach.items.index - 1) % 4 == 0)} no-css {else} no-css {/if}">
				{if $hItem.url}
					<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
				{/if}
					{if $hItem.image}
						<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img img-responsive" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" />
					{/if}
					{if $hItem.title && $hItem.title_use == 1}
						<h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
					{/if}
					{if $hItem.html}
						<div class="item-html">
							{$hItem.html}
						</div>
					{/if}
				{if $hItem.url}
					</a>
				{/if}
			</div>
		{/foreach}
		<div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}"></div>
	{else}
		<div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}"{if $hook == 'footer'} style="clear:both"{/if}>
			<ul class="htmlcontent-home clearfix row">
				{foreach name=items from=$htmlitems item=hItem}
					{if $hook == 'left' || $hook == 'right'}
						<li class="htmlcontent-item htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-12">
					{else}
						<li class="htmlcontent-item htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-4">
					{/if}
							{if $hItem.url}
								<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
							{/if}
								{if $hItem.image}
									<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img {if $hook == 'left' || $hook == 'right'}img-responsive{/if}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" />
								{/if}
								{if $hItem.title && $hItem.title_use == 1}
									<h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
								{/if}
								{if $hItem.html}
									<div class="item-html">
										{$hItem.html}
									</div>
								{/if}
							{if $hItem.url}
								</a>
							{/if}
						</li>
				{/foreach}
			</ul>
		</div>
	{/if}
{/if}
