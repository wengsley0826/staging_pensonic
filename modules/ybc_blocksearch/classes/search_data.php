<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

class Search_data{
    private $is17 = false;
    private $per_page = 1;
    private $id_group = 'product';
    private $page = 1;
    private $orderBy = 'position';
    private $orderWay = 'ASC';
    private $randSeed = 1;
    private $keyWords = '';
    private $id_only = false;

    private $search_on_desc = 0;
    private $is_ajax = false;

    public function __construct($id_group = 'product', $per_page = 3, $page = 1 , $orderBy = 'position', $orderWay = 'ASC',$keyWords='')
    {
        $this->is17 = version_compare(_PS_VERSION_, '1.7', '>=');
        $this->id_group = $id_group;
        $this->per_page = $per_page;
        $this->page = $page;
        $this->orderBy = $orderBy;
        $this->orderWay = $orderWay;
        $this->orderWay = $orderWay;
        $this->keyWords = $keyWords;
        $this->search_on_desc = Configuration::get('YBC_BLOCKSEARCH_ON_DESC');
    }

    public function setRandSeed($randSeed)
    {
        $this->randSeed = $randSeed;
        return $this;
    }

    public function setIdGroup($id_group)
    {
        $this->id_group = $id_group;
        return $this;
    }

    public function setPage($Page)
    {
        $this->page = $Page;
        return $this;
    }

    public function setPerPage($per_page)
    {
        $this->per_page = $per_page;
        return $this;
    }

    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    public function setOrderWay($orderWay)
    {
        $this->orderWay = $orderWay;
        return $this;
    }

    public function setKeyWords($keyWords)
    {
        $this->keyWords = $keyWords;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIdOnly() {
        return $this->id_only;
    }

    /**
     * @param bool $id_only
     */
    public function setIdOnly($id_only) {
        $this->id_only = $id_only;
    }


    /**
     * @return bool
     */
    public function isIsAjax() {
        return $this->is_ajax;
    }

    /**
     * @param bool $is_ajax
     */
    public function setIsAjax($is_ajax) {
        $this->is_ajax = $is_ajax;
    }


    public function getPages($methods)
    {
        if (!$methods)
            return 1;
        $nbTotal = (int)$this->{$methods}(true);
        return ceil($nbTotal/$this->per_page);
    }
    public function getSuggestion($total= false){
        if ($total){
            return $this->_getSuggestion($this->keyWords,0,true,$this->id_only);
        }
        return $this->_getSuggestion($this->keyWords,$this->per_page,false,$this->id_only);
    }

    public function getCategoryBlock($total = false){
        if ($total){
            return $this->_getCategoryBlog($this->keyWords,0,0,true,$this->id_only);
        }
        return $this->_getCategoryBlog($this->keyWords,$this->page,$this->per_page,false,$this->id_only);
    }

    public function getBlockPostInBlock($total = false){
        if ($total){
            return $this->_getBlogPostInBlog($this->keyWords,0,0,true,$this->id_only);
        }

        return $this->_getBlogPostInBlog($this->keyWords,$this->page,$this->per_page,false,$this->id_only);
    }

    public function getManufacture($total = false){
        if ($total){
            return $this->_getManufacture($this->keyWords,0,0,true,$this->id_only);
        }
        return $this->_getManufacture($this->keyWords,$this->page,$this->per_page,false,$this->id_only);
    }

    public function getCmsPage($total = false){
        if ($total){
            return $this->_getCmsPage($this->keyWords,0,0,true,$this->id_only);
        }
        return $this->_getCmsPage($this->keyWords,$this->page,$this->per_page,false,$this->id_only);
    }

    public function getCategory($total = false){
        if ( $total ){
            return $this->_getCategory($this->keyWords,0,0,true,$this->id_only);
        }
        return $this->_getCategory($this->keyWords,$this->page,$this->per_page,false,$this->id_only);
    }

    public function getProduct($total = false,$ajax = false){
        if ( $total ){
            return $this->getTotalProduct($this->keyWords);
        }
        return $this->_getProduct($this->keyWords,$this->page,$this->per_page,false,$ajax, $this->orderBy, $this->orderWay,$this->id_only);
    }

    public function _getSuggestion($keyWords = false,$per_page=3,$getTotal = false){
        $suggestions = Ybc_search_suggestion::find($keyWords,$per_page);
        if ( $getTotal ) {
            return count($suggestions);
        }
        if ( $suggestions ){
            foreach ( $suggestions as &$suggestion ){
                $suggestion['title'] = $suggestion['keyword'];
                $suggestion['link'] = Context::getContext()->link->getModuleLink('ybc_blocksearch','blocksearch',array('search_query'=>$suggestion['keyword']),Tools::usingSecureMode());
                $suggestion['id_item'] = $suggestion['id_suggestion'];
            }
            return $suggestions;
        }
    }

    public function _getCategoryBlog($keyWords = false,$page = 0,$per_page = 3,$getTotal = false,$id_only =false){
        $context = Context::getContext();
        $page = (int)$page;
        if ($page <= 0){
            $page = 1;
        }
        if ( !$keyWords ){
            $keyWords = $this->keyWords;
        }

        $sql = 'SELECT '.( $getTotal ? ' COUNT(DISTINCT c.`id_category`) AS total ' : ' c.`id_category`,c.`thumb`, cl.`title`,cl.`description` ').' FROM `'._DB_PREFIX_.'ybc_blog_category` c 
                LEFT JOIN `'._DB_PREFIX_.'ybc_blog_category_lang` cl ON c.`id_category` = cl.`id_category` 
                LEFT JOIN `'._DB_PREFIX_.'ybc_blog_category_shop` cs ON c.`id_category` = cs.`id_category`
                WHERE 1 AND cs.`id_shop` = '.(int)$context->language->id.'  
                        AND cl.`id_lang` = '.(int)$context->shop->id;

            if ($keyWords && $id_only){
                $sql .=' AND c.`id_category` LIKE \'%'.pSQL($keyWords).'%\' ';
            }else if ($keyWords){
                $sql .= ' AND cl.`title` LIKE \'%'.pSQL($keyWords).'%\' 
                '.($this->search_on_desc ? ' OR cl.`description` LIKE \'%'.pSQL($keyWords).'%\'':'').' 
                 OR c.`id_category` LIKE \'%'.pSQL($keyWords).'%\' ';
            }

            if(! $getTotal) {
                $sql .= ' GROUP BY c.`id_category` ';
            }
            if($per_page > 0 && !$getTotal){
                $sql .= ' limit ' . (int) ($page-1)*$per_page . ',' . (int) $per_page;
            }
        if ( $getTotal ) {
            return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }
        if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql)){
            foreach ( $result as &$row){
                $row['link'] = $this->getLinkModuleBlock('blog',array('id_category'=>$row['id_category']));
                $row['url_img'] = $row['thumb'] ? __PS_BASE_URI__ . 'modules/ybc_blog/views/img/category/thumb/'.$row['thumb'] : '';
                $row['id_item'] = $row['id_category'];
                $row['shor_description'] = ($row['description']) ? $this->truncate(Tools::getDescriptionClean($row['description'])) : '';
            }
            return $result;
        }

        return array();
    }

    public function getLinkModuleBlock($controller = 'blog', $params = array(),$id_lang=0){
        $context = Context::getContext();
        $id_lang =  $id_lang ? $id_lang : $context->language->id;
        $alias = Configuration::get('YBC_BLOG_ALIAS',$context->language->id);
        $friendly = (int)Configuration::get('YBC_BLOG_FRIENDLY_URL') && (int)Configuration::get('PS_REWRITING_SETTINGS') ? true : false;
        $blockLink = new Ybc_blog_link_class();
        $subfix = (int)Configuration::get('YBC_BLOG_URL_SUBFIX') ? '.html' : '';
        $page = isset($params['page']) && $params['page'] ? $params['page'] : '';
        if(trim($page)!='')
        {
            $page = $page.'/';
        }
        else
            $page='';

        if($friendly && $alias)
        {
            $url = $blockLink->getBaseLinkFriendly(null, null).$blockLink->getLangLinkFriendly($id_lang, null, null).$alias.'/';
            if($controller=='blog')
            {
                if(isset($params['id_post']) && $postAlias = $this->getPostAlias((int)$params['id_post']))
                {
                    if(Configuration::get('YBC_BLOG_URL_NO_ID'))
                        $url .= (($subAlias = Configuration::get('YBC_BLOG_ALIAS_POST',$id_lang)) ? $subAlias : 'post').'/'.$postAlias.$subfix;
                    else
                        $url .= (($subAlias = Configuration::get('YBC_BLOG_ALIAS_POST',$id_lang)) ? $subAlias : 'post').'/'.$params['id_post'].'-'.$postAlias.$subfix;
                }
                elseif(isset($params['id_category']) && $categoryAlias = $this->getCategoryAlias((int)$params['id_category'],$context))
                {
                    if(Configuration::get('YBC_BLOG_URL_NO_ID'))
                        $url .= (($subAlias = Configuration::get('YBC_BLOG_ALIAS_CATEGORY',$id_lang)) ? $subAlias : 'category').($page ? '/'.rtrim($page) : '/').$categoryAlias.$subfix;
                    else
                        $url .= (($subAlias = Configuration::get('YBC_BLOG_ALIAS_CATEGORY',$id_lang)) ? $subAlias : 'category').($page ? '/'.rtrim($page) : '/').$params['id_category'].'-'.$categoryAlias.$subfix;
                }
                else
                {
                    if($page)
                        $url .= trim($page,'/');
                    else
                        $url = rtrim($url,'/');
                }
                return $url;
            }
        }
        $extra='';
        if($params)
            foreach($params as $key=> $param)
                $extra ='&'.$key.'='.$param;
        return Tools::getShopDomainSsl(true).__PS_BASE_URI__.'index.php?fc=module&module=ybc_blog&controller='.$controller.'&id_lang='.$context->language->id.$extra;
    }
    private function getCategoryAlias($id_category,Context $context)
    {
        if (!$context){
            $context = Context::getContext();
        }
        $req = "SELECT cl.url_alias
            FROM "._DB_PREFIX_."ybc_blog_category_lang cl
            WHERE cl.id_category = ".(int)$id_category.' AND cl.id_lang='.(int)$context->language->id;
        $row = Db::getInstance()->getRow($req);
        if(isset($row['url_alias']))
            return $row['url_alias'];
        return false;
    }

    public function _getBlogPostInBlog($keyWords = false,$page = 0,$per_page = 3,$getTotal = false, $id_only = false){
        $context = Context::getContext();
        $idLang = $context->language->id;
        $idShop = $context->shop->id;

        $page = (int)$page;
        if ($page <= 0){
            $page = 1;
        }

        $sql = 'SELECT '.($getTotal? 'COUNT(DISTINCT p.`id_post`)' : 'p.`id_post`,p.`thumb`,pl.`title`,pl.`description`, pl.`short_description`' ).' FROM `'._DB_PREFIX_.'ybc_blog_post` p 
                LEFT JOIN `'._DB_PREFIX_.'ybc_blog_post_lang` pl ON ( p.`id_post` = pl.`id_post`) 
                LEFT JOIN `'._DB_PREFIX_.'ybc_blog_post_shop` ps ON ( p.`id_post` = ps.`id_post`) 
                WHERE 1 AND pl.`id_lang` = '.(int)$idLang.'
                       AND ps.`id_shop` = '.(int)$idShop;

        if ($keyWords && $id_only){
            $sql .= ' AND p.`id_post` LIKE \'%'.pSQL($keyWords).'%\'';
        }else if ($keyWords){
            $sql .=  ' AND pl.`title` LIKE \'%'.pSQL($keyWords).'%\' 
               '.( $this->search_on_desc ? ' OR pl.description LIKE \'%'.pSQL($keyWords).'%\' OR pl.short_description LIKE \'%'.pSQL($keyWords).'%\' ' : '').' 
                OR p.`id_post` LIKE \'%'.pSQL($keyWords).'%\' ';
        }
        if ( ! $getTotal ){
            $sql .= ' GROUP BY p.`id_post`';
        }
        if( $per_page > 0 && !$getTotal ){
            $sql .= ' limit ' . (int) ($page-1)*$per_page . ',' . (int) $per_page;
        }

        if ( $getTotal ) {
            return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }

        if ( $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql) ){
            foreach ( $result as &$row){
                $row['link'] = $this->getLinkModuleBlock('blog',array('id_post'=>$row['id_post']));
                $row['url_img'] = $row['thumb'] ? __PS_BASE_URI__ . 'modules/ybc_blog/views/img/post/thumb/'.$row['thumb'] : '';
                $row['id_item'] = $row['id_post'];
                $row['shor_description'] = ($row['description'])? $this->truncate(Tools::getDescriptionClean($row['description'])) : ( $row['short_description'] ? $this->truncate(Tools::getDescriptionClean($row['short_description'])) : '' );
            }
            return $result;
        }

        return array();
    }

    private function getPostAlias($id_post)
    {
        $req = "SELECT pl.url_alias
            FROM "._DB_PREFIX_."ybc_blog_post_lang pl
            WHERE pl.id_post = ".(int)$id_post.' AND pl.id_lang='.(int)Context::getContext()->language->id;
        $row = Db::getInstance()->getRow($req);
        if(isset($row['url_alias']))
            return $row['url_alias'];
        return false;
    }

    public function _getManufacture($keyWords = false,$page = 0,$per_page = 3,$getTotal = false,$id_only = false,$active = true){
        $page = (int)$page;
        if ($page <= 0){
            $page = 1;
        }

        $context = Context::getContext();
        $idLang = $context->language->id;
        $sql = '
		SELECT '.($getTotal ? ' COUNT(DISTINCT m.`id_manufacturer`) ' : ' m.`id_manufacturer`,m.`name`,ml.`short_description`,ml.`description` ').'
		FROM `' . _DB_PREFIX_ . 'manufacturer` m'
            . Shop::addSqlAssociation('manufacturer', 'm') .
            'INNER JOIN `' . _DB_PREFIX_ . 'manufacturer_lang` ml ON (m.`id_manufacturer` = ml.`id_manufacturer` AND ml.`id_lang` = ' . (int) $idLang . ')' .
            'WHERE 1 ';
        if($keyWords && $id_only){
            $sql .=' AND m.`id_manufacturer` LIKE \'%'.pSQL($keyWords).'%\' ';
        }elseif ($keyWords){
            $sql .=' AND m.`name` LIKE \'%'.pSQL($keyWords).'%\' 
            '.($this->search_on_desc ? ' OR ml.`description` LIKE \'%'.pSQL($keyWords).'%\' ':'').
            ' OR m.`id_manufacturer` LIKE \'%'.pSQL($keyWords).'%\' OR ml.`short_description` LIKE \'%'.pSQL($keyWords).'%\' ';
        }
            $sql .= ($active ? ' AND m.`active` = 1 ' : '') .
            (!$getTotal ? ' GROUP BY m.`id_manufacturer`' : '').
            ( ( $per_page > 0 && !$getTotal ) ? ' limit ' . (int) ($page-1)*$per_page . ',' . (int) $per_page : '');
        if($getTotal){
            return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }

        $manufacturers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ( $manufacturers ){
            foreach ( $manufacturers as &$manufacturer ){
                $manufacturer['title'] = $manufacturer['name'];
                $manufacturer['link'] = $context->link->getManufacturerLink($manufacturer['id_manufacturer']);
                $manufacturer['id_item'] = $manufacturer['id_manufacturer'];
                $manufacturer['shor_description'] = $manufacturer['short_description']? $this->truncate(Tools::getDescriptionClean($manufacturer['short_description'])) : ($manufacturer['description'] ? $this->truncate(Tools::getDescriptionClean($manufacturer['description'])) : '');
                if (! $this->isIsAjax()){
                    $manufacturer['url_img'] = $this->getManufacturerImageLink($manufacturer['id_manufacturer']);
                    $manufacturer['count_product'] = Manufacturer::getProducts($manufacturer['id_manufacturer'],$context->language->id,false,false,null,null,true);
                }
            }
            return $manufacturers;
        }
        return array();
    }

    public function _getCmsPage($keyWords = false,$page = 0,$per_page = 3,$getTotal = false,$id_only = false,$selection = null, $active = true, Link $link = null){
        $context = Context::getContext();
        $idLang = $context->language->id;
        $link = $context->link;
        if ($page <= 0){
            $page = 1;
        }
        $sql = '
		SELECT '.($getTotal ? 'COUNT(DISTINCT c.`id_cms`)' : ' c.`id_cms`, cl.`link_rewrite`, cl.`meta_title`, cl.`content`,cl.`meta_description` ').' 
		FROM `' . _DB_PREFIX_ . 'cms` c
		LEFT JOIN `' . _DB_PREFIX_ . 'cms_lang` cl ON (c.`id_cms` = cl.`id_cms` AND cl.`id_lang` = ' . (int) $idLang . ')
		' . Shop::addSqlAssociation('cms', 'c') . '
		WHERE 1
		' . (($selection !== null) ? ' AND c.`id_cms` IN (' . implode(',', array_map('intval', $selection)) . ')' : '') .
            ($active ? ' AND c.`active` = 1 ' : '');
        if($keyWords && $id_only){
            $sql .=' AND c.`id_cms` LIKE \'%'.pSQL($keyWords).'%\' ';
        }else if($keyWords){
            $sql .=' AND cl.`meta_title` LIKE \'%'.pSQL($keyWords).'%\' 
                    '.($this->search_on_desc ? ' OR cl.meta_description LIKE \'%'.pSQL($keyWords).'%\' OR cl.content LIKE \'%'.pSQL($keyWords).'%\' ':'').
                    ' OR c.`id_cms` LIKE \'%'.pSQL($keyWords).'%\' ';
        }

        if(!$getTotal){
            $sql .=' GROUP BY c.`id_cms` ORDER BY c.`position` ';
        }
        if($per_page > 0 && !$getTotal){
            $sql .=' limit ' . (int) ($page-1)*$per_page . ',' . (int) $per_page;
        }

        if($getTotal){
            return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        $links = array();
        if ($result) {
            foreach ($result as $row) {
                $row['title'] = $row['meta_title'];
                $row['link'] = $link->getCMSLink((int) $row['id_cms'], $row['link_rewrite']);
                $row['id_item'] = $row['id_cms'];
                $row['shor_description'] = ($row['meta_description']) ? $this->truncate(Tools::getDescriptionClean($row['meta_description'])) : (($row['content']) ? $this->truncate(Tools::getDescriptionClean($row['content'])) : '');
                $links[] = $row;
            }
            return $links;
        }
        return array();
    }

    public function _getCategory($keyWords = false,$page = 0,$per_page = 3,$getTotal = false, $id_only = false){
        $context = Context::getContext();
        $idLang = $context->language->id;
        $idShop = $context->shop->id;
        $link = $context->link;
        if ($page <= 0){
            $page = 1;
        }

        $sql = 'SELECT '.( $getTotal ? ' COUNT(DISTINCT c.`id_category`) AS total ' : ' cl.name,c.`id_category`,cl.`description` ').' FROM `'._DB_PREFIX_.'category` c 
                LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON ( c.`id_category` = cl.`id_category` AND cl.`id_shop` = '.(int)$idShop.' )
                LEFT JOIN `'._DB_PREFIX_.'category_shop` cs ON ( c.`id_category` = cs.`id_category`)
                WHERE cl.`id_lang` = '.(int)$idLang.' 
                    AND cs.`id_shop` = '.(int)$idShop.' 
                    AND c.`id_category` NOT IN (1,2) ';

        if($keyWords){
            if ($id_only){
                $sql .= ' AND c.`id_category` LIKE \'%'.pSQL($keyWords).'%\'';
            }else{
                $sql .= ' AND cl.`name` LIKE \'%'.pSQL($keyWords).'%\' 
                '.($this->search_on_desc ? ' OR cl.`description` LIKE \'%'.pSQL($keyWords).'%\'' : '')
                        .' OR c.`id_category` LIKE \'%'.pSQL($keyWords).'%\' ';
            }
        }
        if(!$getTotal){
            $sql .= ' GROUP By c.`id_category`';
        }
        if( $per_page > 0 && !$getTotal ){
            $sql .=' limit ' . (int) ($page-1)*$per_page . ',' . (int) $per_page;
        }

        if ($getTotal){
            return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }
        if ( $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql)){

            foreach ( $result as &$row ){
                $row['title'] = $row['name'];
                $row['link'] = $link->getCategoryLink($row['id_category']);
                $category = new Category($row['id_category'], $context->language->id);
                if ( $category->id_image){
                    $url_img = $context->link->getCatImageLink($category->link_rewrite, $category->id_image, false);
                    $row['url_img'] = $url_img;
                }
                $row['id_item'] = $row['id_category'];
                $row['shor_description'] = ($row['description'] ) ? $this->truncate(Tools::getDescriptionClean($row['description'])) : '';
                if (! $this->isIsAjax() ){
                    $row['count_product'] = $this->getCountProductByIdCategory($row['id_category']);
                }
            }
            return $result;
        }
        return array();
    }

    public function getCountProductByIdCategory($id_category){
        if ( ! $id_category ){
            return null;
        }

        $idSupplier = (int) Tools::getValue('id_supplier');
        $active = true;
        $front = true;
        $sql = 'SELECT COUNT(cp.`id_product`) AS total
					FROM `' . _DB_PREFIX_ . 'product` p
					' . Shop::addSqlAssociation('product', 'p') . '
					LEFT JOIN `' . _DB_PREFIX_ . 'category_product` cp ON p.`id_product` = cp.`id_product`
					WHERE cp.`id_category` = ' . (int) $id_category .
               ($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '') .
               ($active ? ' AND product_shop.`active` = 1' : '') .
               ($idSupplier ? 'AND p.id_supplier = ' . (int) $idSupplier : '');

        return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
    }

    public function getTotalProduct($expr = false){
        if (!$expr && $this->keyWords){
            $expr = $this->keyWords;
        }
        if ($this->is_ajax && $this->id_only){
            return $this->getProductByIdOnly(false, $expr,false, false, false, false,true);
        }elseif ((int)Tools::strlen($expr) < (int)Configuration::get('YBC_BLOCKSEARCH_MINCHART')){
            return $this->getProductByIdOnly(false, $expr,false, false, false, false,true);
        }

        $context = Context::getContext();
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
        $id_lang = $context->language->id;
        $intersect_array = array();
        $score_array = array();
        //$words = Search::extractKeyWords($expr, $id_lang, false, $context->language->iso_code);
        
        $sanitizedString = Search::sanitize($expr, $id_lang, false, $context->language->iso_code);
        $words = explode(' ', $sanitizedString);
        if (strpos($expr, '-') !== false) {
            $sanitizedString = Search::sanitize($expr, $id_lang, false, $context->language->iso_code, true);
            $words2 = explode(' ', $sanitizedString);
            // foreach word containing hyphen, we want to index additional word removing the hyphen
            // eg: t-shirt => tshirt
            foreach ($words2 as $word) {
                if (strpos($word, '-') !== false) {
                    $word = str_replace('-', '', $word);
                    if (!empty($word)) {
                        $words[] = $word;
                    }
                }
            }
            $words = array_merge($words, $words2);
        }
        
        $words = array_unique($words);

        foreach ($words as $key => $word) {
            if (!empty($word) && Tools::strlen($word) >= (int) Configuration::get('PS_SEARCH_MINWORDLEN')) {
                $word = str_replace('%', '\\%', $word);
                $word = str_replace('_', '\\_', $word);
                $start_search = Configuration::get('PS_SEARCH_START') ? '%' : '';
                $end_search = Configuration::get('PS_SEARCH_END') ? '' : '%';
                $intersect_array[] = 'SELECT si.id_product
					FROM ' . _DB_PREFIX_ . 'search_word sw
					LEFT JOIN ' . _DB_PREFIX_ . 'search_index si ON sw.id_word = si.id_word
					WHERE sw.id_lang = ' . (int)$id_lang . '
						AND sw.id_shop = ' . (int)$context->shop->id . '
						AND sw.word LIKE
					' . ($word[0] == '-' ? ' \'' . $start_search . pSQL(Tools::substr($word, 1,
                    PS_SEARCH_MAX_WORD_LENGTH)) . $end_search . '\'' : ' \'' . $start_search . pSQL
                    (Tools::substr($word, 0, PS_SEARCH_MAX_WORD_LENGTH)) . $end_search . '\'');
                if ($word[0] != '-')
                    $score_array[] = 'sw.word LIKE \'' . $start_search . pSQL(Tools::substr($word, 0,
                        PS_SEARCH_MAX_WORD_LENGTH)) . $end_search . '\'';
            } else {
                unset($words[$key]);
            }
        }

        $sql_groups = '';
        if (Group::isFeatureActive()) {
            $groups = FrontController::getCurrentCustomerGroups();
            $sql_groups = 'AND cg.`id_group` ' . (count($groups) ? 'IN (' . implode(',', $groups) . ')' : '= 1');
        }

        $results = $db->executeS('
		SELECT DISTINCT cp.`id_product`
		FROM `' . _DB_PREFIX_ . 'category_product` cp
		' . (Group::isFeatureActive() ? 'INNER JOIN `' . _DB_PREFIX_ . 'category_group` cg ON cp.`id_category` = cg.`id_category`' : '') . '
		INNER JOIN `' . _DB_PREFIX_ . 'category` c ON cp.`id_category` = c.`id_category`
		INNER JOIN `' . _DB_PREFIX_ . 'product` p ON cp.`id_product` = p.`id_product`
		' . Shop::addSqlAssociation('product', 'p', false) . '
		WHERE c.`active` = 1
		AND product_shop.`active` = 1
		AND product_shop.`visibility` IN ("both", "search")
		AND product_shop.indexed = 1
		' . $sql_groups, true, false);

        $eligible_products = array();
        foreach ($results as $row) {
            $eligible_products[] = $row['id_product'];
        }

        $eligible_products2 = array();
        foreach ($intersect_array as $keyWords) {
            foreach ($db->executeS($keyWords, true, false) as $row) {
                $eligible_products2[] = $row['id_product'];
            }
        }
        $eligible_products = array_unique(array_intersect($eligible_products, array_unique($eligible_products2)));
        if (!count($eligible_products)) {
            return 0;
        }

        $product_pool = '';
        foreach ($eligible_products as $id_product) {
            if ($id_product) {
                $product_pool .= (int) $id_product . ',';
            }
        }
        if (empty($product_pool)) {
            return 0;
        }
        $product_pool = ((strpos($product_pool, ',') === false) ? (' = ' . (int) $product_pool . ' ') : (' IN (' . rtrim($product_pool, ',') . ') '));
        $sql = 'SELECT COUNT(*)
				FROM ' . _DB_PREFIX_ . 'product p
				' . Shop::addSqlAssociation('product', 'p') . '
				INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
					p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . '
				)
				LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE p.`id_product` ' . $product_pool;
        $total = $db->getValue($sql, false);

        return (int) $total;
    }

    public function _getProduct($keyWords = false,$page = 0,$per_page = 3,$getTotal = false,$ajax = false, $order_by = 'position', $order_way = 'desc',$id_only = false){
        $module = new Ybc_blockSearch();
        if ( ! $getTotal){
            $getTotal =  ceil($this->getTotalProduct($keyWords)/$per_page);
        }
        $context = Context::getContext();
        $idLang = $context->language->id;
        if ($page <= 0){
            $page = 1;
        }

        if ( $ajax ){
            if ($id_only){
                $searchResults = $this->getProductByIdOnly((int)$idLang, $keyWords,$page, $per_page, $order_by, $order_way);
            }else{
                $searchResults = Search::find((int)$idLang, $keyWords, $page, $per_page, $order_by, $order_way, true);
            }

            $id_group = isset($context->customer->id) && $context->customer->id? Customer::getDefaultGroupId((int)$context->customer->id) : (int)Group::getCurrent()->id;
            $group = new Group($id_group);
            $useTax = $group->price_display_method? false : true;

            foreach ( $searchResults as &$product ){
                $product['link'] = $context->link->getProductLink($product['id_product'], $product['prewrite'], $product['crewrite']);
                $product['title'] =  $product['pname'];
                if((int)Configuration::get('YBC_BLOCKSEARCH_SHOW_PRODUCT_IMAGE'))
                {
                    $productObj = new Product((int)$product['id_product'], true, (int)$context->cookie->id_lang, $context->shop->id);
                    $images = $productObj->getImages((int)$context->cookie->id_lang);
                    if(isset($images[0]))
                        $id_image = Configuration::get('PS_LEGACY_IMAGES') ? ($productObj->id.'-'.$images[0]['id_image']) : $images[0]['id_image'];
                    else{
                        $id_image = $context->language->iso_code.'-default';
                    }
                    $product['img_url'] =  $context->link->getImageLink($productObj->link_rewrite, $id_image,$this->is17 ?ImageType::getFormattedName('cart') : call_user_func('ImageType::' . 'getFormatedName','cart'));
                }
                else
                    $product['img_url'] = '';

                if ( (int)Configuration::get('YBC_BLOCKSEARCH_SHOW_PRODUCT_PRICE') ){
                    $p = new Product($product['id_product'], true, $context->language->id, $context->shop->id);
                    $id_attribute = (int)DB::getInstance()->getValue('SELECT `id_product_attribute` 
                                                                        FROM `'._DB_PREFIX_.'product_attribute` 
                                                                        WHERE  1 AND `id_product`= '.(int)$product['id_product'].' 
                                                                                 AND default_on = 1');
                    $product['price'] = $p->getPrice($useTax,$id_attribute ? $id_attribute : null);
                    if(($oldPrice = $p->getPriceWithoutReduct(!$useTax,$id_attribute ? $id_attribute : null)) && $oldPrice!=$product['price'])
                        $product['old_price'] = Tools::convertPrice($oldPrice);
                    $product['price'] = Tools::displayPrice($product['price']);
                    if(isset($product['old_price']))
                        $product['old_price'] = Tools::displayPrice($product['old_price']);
                }

                if ( (int)Configuration::get('YBC_BLOCKSEARCH_SHOW_PRODUCT_DESCRIPTION') ){
                    $sql_get_des = 'SELECT `description_short` FROM `'._DB_PREFIX_.'product_lang` 
                                            WHERE 1 AND `id_product` = '.(int)$product['id_product'].' 
                                             AND id_lang = '.(int)$context->language->id.' 
                                             AND id_shop = '.(int)$context->shop->id.' ';
                    $shor_description = DB::getInstance()->getValue($sql_get_des);
                    if ($shor_description){
                        $shor_description = $this->truncate(Tools::getDescriptionClean($shor_description));
                    }
                    $product['shor_description'] = $shor_description;
                }

                if ( (int)Configuration::get('YBC_BLOCKSEARCH_SHOW_PRODUCT_RATING') && $module->checkModuleEnable('productcomments')){
                    $product['rating'] = $module->hookDisplayReviews(array('id_product'=>$product['id_product']));
                }
            }
        }


        if ( $ajax ){
            return $searchResults ? $searchResults : false;
        }
        if ($this->id_only || (int)Tools::strlen($keyWords) < (int)Configuration::get('YBC_BLOCKSEARCH_MINCHART')){
            $products = $this->getProductByIdOnly((int)$idLang, $keyWords, (int)$page, $per_page, $order_by, $order_way, false);
        }else{
            $products = Search::find((int)$idLang, $keyWords, (int)$page, $per_page, $order_by, $order_way, false);
        }

        $total_product  = $products['total'];
        if ($this->is17) {
            $products = self::productsForTemplate($products['result']);
        }else{
            $products = $products['result'];
        }

        return array(
            'products' => $products,
            'totalPage' => ceil($total_product/$per_page)
        );
    }

    public static function productsForTemplate($products, Context $context = null)
    {
        if (!$products || !is_array($products))
            return array();
        if (!$context)
            $context = Context::getContext();
        $assembler = new ProductAssembler($context);
        $presenterFactory = new ProductPresenterFactory($context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
            new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                $context->link
            ),
            $context->link,
            new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
            new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
            $context->getTranslator()
        );

        $products_for_template = array();

        foreach ($products as $rawProduct) {
            $products_for_template[] = $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($rawProduct),
                $context->language
            );
        }
        return $products_for_template;
    }

    /**
     * Prepare a word for the SQL requests (Remove hyphen if present, add percentage signs).
     *
     * @internal Public for tests
     *
     * @param string $word
     *
     * @return string
     */
    public static function getSearchParamFromWord($word)
    {
        $word = str_replace(array('%', '_'), array('\\%', '\\_'), $word);
        $start_search = Configuration::get('PS_SEARCH_START') ? '%' : '';
        $end_search = Configuration::get('PS_SEARCH_END') ? '' : '%';
        $start_pos = (int) ($word[0] == '-');

        return $start_search . pSQL(Tools::substr($word, $start_pos, PS_SEARCH_MAX_WORD_LENGTH)) . $end_search;
    }

    public function truncate1($string, $length = false){
        if ( ! $string ) return '';
        if ( ! $length ){
            $length = $this->isIsAjax() ? (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION') : (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION_PAGES');
        }
        if ( $length && Tools::strlen($string) > $length) {
            $string = Tools::substr($string, 0, $length) . ' ...';
        }
        return $string;
    }

    public function getProductByIdOnly($id_lang,$keyWords = false,$page_number=1,$page_size = 1, $order_by = 'position', $order_way = 'desc',$getTotal = false){
        $db = Db::getInstance();
        $context = Context::getContext();
        if ( !$id_lang)
            $id_lang = Context::getContext()->language->id;
        if($getTotal){
            $sql = 'SELECT COUNT(DISTINCT p.id_product) as total   
					FROM ' . _DB_PREFIX_ . 'product p
					INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
						p.`id_product` = pl.`id_product`
						AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . '
					)
					' . Shop::addSqlAssociation('product', 'p') . '
					INNER JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (
						product_shop.`id_category_default` = cl.`id_category`
						AND cl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('cl') . '
					)
					WHERE 1 
					';
            if ($this->is_ajax){
                $sql .= ' AND p.`id_product` LIKE \'%'.pSQL($keyWords).'%\' ';
            }else{
                $sql .= ' AND p.`id_product` LIKE \'%'.pSQL($keyWords).'%\' OR pl.`name` LIKE \'%'.pSQL($keyWords).'%\' 
                            OR  pl.`description` LIKE \'%'.pSQL($keyWords).'%\' OR pl.`description_short` LIKE \'%'.pSQL($keyWords).'%\' ';
            }

            return (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }

        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            return false;
        }

        if ($page_number < 1) {
            $page_number = 1;
        }
        if ($page_size < 1) {
            $page_size = 1;
        }
        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by = pSQL($order_by[0]) . '.`' . pSQL($order_by[1]) . '`';
        }
        $alias = '';
        if ($order_by === 'price') {
            $alias = 'product_shop.';
        } elseif (in_array($order_by, array('date_upd', 'date_add'))) {
            $alias = 'p.';
        }
        if ( $order_by == 'name'){
            $order_by = 'pname';
        }

        if ($order_by == 'position'){
            $alias = 'p.';
            $order_by = 'id_product';
        }
        if ($this->is_ajax){
            $sql = 'SELECT DISTINCT p.id_product, pl.name pname, cl.name cname,
						cl.link_rewrite crewrite, pl.link_rewrite prewrite 
					FROM ' . _DB_PREFIX_ . 'product p
					INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
						p.`id_product` = pl.`id_product`
						AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . '
					)
					' . Shop::addSqlAssociation('product', 'p') . '
					INNER JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (
						product_shop.`id_category_default` = cl.`id_category`
						AND cl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('cl') . '
					)
					WHERE p.`id_product`LIKE \'%'.pSQL($keyWords).'%\' 
					' . ($order_by ? 'ORDER BY  ' . $alias . $order_by : '') . ($order_way ? ' ' . $order_way : '') . ' LIMIT '.$page_size;
            return $db->executeS($sql, true, false);
        }


        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
				pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`name`,
			 image_shop.`id_image` id_image, il.`legend`, m.`name` manufacturer_name ,
				DATEDIFF(
					p.`date_add`,
					DATE_SUB(
						"' . date('Y-m-d') . ' 00:00:00",
						INTERVAL ' . (Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20) . ' DAY
					)
				) > 0 new' . (Combination::isFeatureActive() ? ', product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, IFNULL(product_attribute_shop.`id_product_attribute`,0) id_product_attribute' : '') . '
				FROM ' . _DB_PREFIX_ . 'product p
				' . Shop::addSqlAssociation('product', 'p') . '
				INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
					p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . '
				)
				' . (Combination::isFeatureActive() ? 'LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_shop` product_attribute_shop FORCE INDEX (id_product)
				    ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop=' . (int) $context->shop->id . ')' : '') . '
				' . Product::sqlStock('p', 0) . '
				LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m FORCE INDEX (PRIMARY) 
				    ON m.`id_manufacturer` = p.`id_manufacturer`
				LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop FORCE INDEX (id_product)
					ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop=' . (int) $context->shop->id . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) $id_lang . ')
				WHERE p.`id_product`LIKE \'%'.pSQL($keyWords).'%\' OR pl.`name` LIKE \'%'.pSQL($keyWords).'%\' 
                            OR  pl.`description` LIKE \'%'.pSQL($keyWords).'%\' OR pl.`description_short` LIKE \'%'.pSQL($keyWords).'%\' 
				GROUP BY product_shop.id_product
				' . ($order_by ? 'ORDER BY  ' . $alias . $order_by : '') . ($order_way ? ' ' . $order_way : '') . '
				LIMIT ' . (int) (($page_number - 1) * $page_size) . ',' . (int) $page_size;
        $result = $db->executeS($sql, true, false);
        $sql = 'SELECT COUNT(*)
				FROM ' . _DB_PREFIX_ . 'product p
				' . Shop::addSqlAssociation('product', 'p') . '
				INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
					p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . '
				)
				LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE p.`id_product`LIKE \'%'.pSQL($keyWords).'%\' OR pl.`name` LIKE \'%'.pSQL($keyWords).'%\' 
                            OR  pl.`description` LIKE \'%'.pSQL($keyWords).'%\' OR pl.`description_short` LIKE \'%'.pSQL($keyWords).'%\' ';
        $total = $db->getValue($sql, false);
        if (!$result) {
            $result_properties = false;
        } else {
            $result_properties = Product::getProductsProperties((int) $id_lang, $result);
        }
        return array('total' => $total, 'result' => $result_properties);
    }

    public function ets_strip_all_tags($string, $remove_breaks = false) {
        $string = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $string );
        $string = strip_tags($string);

        if ( $remove_breaks )
            $string = preg_replace('/[\r\n\t ]+/', ' ', $string);

        return trim( $string );
    }

    public function truncate( $text, $num_words = false, $more = null ) {
        if ( ! $text ) return '';
        if ( ! $num_words ){
            $num_words = $this->isIsAjax() ? (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION') : (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION_PAGES');
        }

        if ( null === $more ) {
            $more = ( ' &hellip;' );
        }

        $text = $this->ets_strip_all_tags( $text );

        $words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );
        $sep = ' ';

        if ( count( $words_array ) > $num_words ) {
            array_pop( $words_array );
            $text = implode( $sep, $words_array );
            $text = $text . $more;
        } else {
            $text = implode( $sep, $words_array );
        }

        return $text;
    }

    /**
     * Returns a link to a manufacturer image for display.
     *
     * @param $idManufacturer
     * @param null $type image type (small_default, medium_default, large_default, etc.)
     *
     * @return string
     */
    public function getManufacturerImageLink($idManufacturer, $type = null)
    {
        $idManufacturer = (int) $idManufacturer;

        if (file_exists(_PS_MANU_IMG_DIR_ . $idManufacturer . (empty($type) ? '.jpg' : '-' . $type . '.jpg'))) {
            $uriPath = _THEME_MANU_DIR_ . $idManufacturer . (empty($type) ? '.jpg' : '-' . $type . '.jpg');
        } elseif (!empty($type) && file_exists(_PS_MANU_IMG_DIR_ . $idManufacturer . '.jpg')) { // !empty($type) because if is empty, is already tested
            $uriPath = _THEME_MANU_DIR_ . $idManufacturer . '.jpg';
        } elseif (file_exists(_PS_MANU_IMG_DIR_ . Context::getContext()->language->iso_code . (empty($type) ? '.jpg' : '-default-' . $type . '.jpg'))) {
            $uriPath = _THEME_MANU_DIR_ . Context::getContext()->language->iso_code . (empty($type) ? '.jpg' : '-default-' . $type . '.jpg');
        } else {
            $uriPath = _THEME_MANU_DIR_ . Context::getContext()->language->iso_code . '.jpg';
        }
        $https_link = (Tools::usingSecureMode() && Configuration::get('PS_SSL_ENABLED')) ? 'https://' : 'http://';
        return $https_link.Tools::getMediaServer($uriPath) . $uriPath;
    }
}
