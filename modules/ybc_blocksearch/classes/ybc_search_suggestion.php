<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2018 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
    exit;

class Ybc_search_suggestion extends ObjectModel{
    public $id_shop;
    public $keyword;
    public $searches;

    public static $definition = array(
        'table' => 'ybc_search_suggestion',
        'primary' => 'id_suggestion',
        'multilang' => true,
        'fields' => array(
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            // Lang fields
            'searches' => array('type' => self::TYPE_INT,'lang' => true, 'validate' => 'isUnsignedId'),
            'keyword' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml',  'size' => 50),
        ),
    );

    public function __construct($id_rule = null)
    {
        parent::__construct($id_rule);
    }


    public static function getSuggestionById($id_suggestion = false){
        if ( ! $id_suggestion) return false;
        $context = Context::getContext();
        $sql = 'SELECT sgl.`keyword`, sgl.`id_lang` FROM `'._DB_PREFIX_.'ybc_search_suggestion` sg
                LEFT JOIN `'._DB_PREFIX_.'ybc_search_suggestion_lang` sgl ON sg.`id_suggestion` = sgl.`id_suggestion` 
                WHERE sg.`id_shop` = '.(int)$context->shop->id.' 
                 AND sg.`id_suggestion` = '.(int)$id_suggestion.' ';
        if ( $data = DB::getInstance()->executeS($sql)){
            return $data;
        }
        return false;
    }

    public static function find($keyWord = false,$limit=3){
        if ( ! $keyWord) return false;
        $context = Context::getContext();
        $sql = 'SELECT sgl.* FROM `'._DB_PREFIX_.'ybc_search_suggestion` sg
                LEFT JOIN `'._DB_PREFIX_.'ybc_search_suggestion_lang` sgl ON sg.`id_suggestion` = sgl.`id_suggestion` 
                WHERE sg.`id_shop` = '.(int)$context->shop->id.' 
                 AND sgl.`id_lang` = '.(int)$context->language->id.' 
                 AND sgl.`keyword` LIKE \'%'.$keyWord.'%\' 
                 GROUP BY sgl.`id_suggestion` ORDER BY sgl.`searches` DESC, sgl.`keyword` ASC LIMIT '.(int)$limit.'
                 ';
        if ( $data = DB::getInstance()->executeS($sql)){
            return $data;
        }
        return false;
    }
}