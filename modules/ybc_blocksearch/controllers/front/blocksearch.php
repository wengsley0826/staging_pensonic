<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
    exit;

Class Ybc_blocksearchBlocksearchModuleFrontController extends ModuleFrontController{
    private $id_lang;
    private $id_shop;
    public $limit;
    public $is_ajax = false;

    public $enable_cateogry_product = 1;
    public $enable_manufacturer = 1;
    public $enable_cms = 1;
    public $enable_block_post = 1;
    public $enable_suggestion =1;
    public $enable_search_product =1;

    public function __construct()
    {
        parent::__construct();
        $this->template = $this->module->is17 ? 'module:ybc_blocksearch/views/templates/front/search.tpl' : 'search16.tpl';
        $this->id_lang = $this->context->language->id;
        $this->id_shop = $this->context->shop->id;

        if (Tools::isSubmit('ajaxKeyWord') && Tools::getValue('ajaxKeyWord')){
            $suggestions = array();
            $keyWord = Tools::replaceAccentedChars(urldecode(Tools::getValue('search_query')));
            $datas = Ybc_search_suggestion::find($keyWord);
            if ( $datas ){
                for ( $i =0, $total_data = count($datas);$i<$total_data;$i++){
                    $suggestions[] = array(
                        'value' => $datas[$i]['keyword'],
                    );
                }
            }else{
                $suggestions[] = array('value'=>'');
            }

            die(Tools::jsonEncode(
                array(
                    'suggestions' => $suggestions
                )
            ));
        }

        if ( Tools::isSubmit('add_number_key') && Tools::getValue('add_number_key')){
            $keyWord = Tools::getValue('keyword');
            $sql = 'UPDATE `'._DB_PREFIX_.'ybc_search_suggestion_lang` SET searches=searches+1 WHERE keyword=\''.$keyWord.'\' ';
            die(Tools::jsonEncode(
                array(DB::getInstance()->execute($sql))
            ));
        }

        if ( Tools::isSubmit('ajaxSearch') && Tools::getValue('ajaxSearch')){
            $this->limit = (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_AJAX') > 0? Configuration::get('YBC_BLOCKSEARCH_LIMIT_AJAX') : 3;
            $this->is_ajax = true;
        }else{
            $this->limit = (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_PAGE')  > 0? (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_PAGE') : 12;
        }

        $this->enable_search_product = (int)Configuration::get('YBC_BLOCKSEARCH_ENABLE_PRODUCT');
        $this->enable_cateogry_product = (int)Configuration::get('YBC_BLOCKSEARCH_ENABLE_BY_CAT');
        $this->enable_manufacturer = (int)Configuration::get('YBC_BLOCKSEARCH_ENABLE_BY_MANUFACTURER');
        $this->enable_cms = (int)Configuration::get('YBC_BLOCKSEARCH_ENABLE_BY_CMSPOST');
        $this->enable_block_post = (int)Configuration::get('YBC_BLOCKSEARCH_ENABLE_BY_BLOGPOST');
        $this->enable_suggestion = (int)Configuration::get('YBC_BLOCKSEARCH_SEARCH_SUGGESTION');
    }

    public function init() {
        parent::init();

        if ( Tools::isSubmit('searchajaxclicktab') && Tools::getValue('searchajaxclicktab')){
            $params = array(
                'id_list' => Tools::getValue('id_list'),
                'keyWord' => Tools::getValue('search_query'),
                'resAjax' => true
            );
            $res = $this->module->hookDisplayDataContent($params);
            die(Tools::jsonEncode(array_merge($params, array(
                'html' => $res ? $res : false,
            ))));
        }

    }
    public function initContent() {
        parent::initContent();
        $this->context->smarty->assign(
            array(
                'path' => $this->module->getBreadCrumb(),
                'breadcrumb' => $this->module->is17 ? $this->module->getBreadCrumb() : false,
            )
        );
        $typeword = Tools::getValue('search_query', '');

        if ( ! $typeword || Tools::strlen($typeword) < 1)
        {
            $this->context->smarty->assign(
                array(
                    'no_type' =>true,
                    'status' => $this->module->l('Please enter keyword of search','blocksearch')
                )
            );
        }else{
            $keyWord = Tools::replaceAccentedChars(urldecode(Tools::getValue('search_query')));
            $res = array();

            $search_data = new Search_data();
            $search_data->setPerPage($this->limit);
            $search_data->setKeyWords($keyWord);

            if ( $this->is_ajax && Tools::strlen($keyWord) < (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_AJAX')){
                $search_data->setIdOnly(true);
            }

            $sortBy = ($this->context->cookie->ybc_search_order && Configuration::get('YBC_BLOCKSEARCH_ALLOW_SORT') )? $this->context->cookie->ybc_search_order : Configuration::get('YBC_BLOCKSEARCH_RESULT_BY');

            $data_sort = explode('_',$sortBy);
            if ($data_sort && is_array($data_sort)){
                if(isset($data_sort[0])) $search_data->setOrderBy($data_sort[0]);
                if(isset($data_sort[1]))$search_data->setOrderWay($data_sort[1]);
            }

            $title_product = $this->module->l('Products','blocksearch');
            $title_cat_pro = $this->module->l('Product categories','blocksearch');
            $title_manu = $this->module->l('Brands','blocksearch');
            $title_cms = $this->module->l('CMS pages','blocksearch');
            $title_cat_block = $this->module->l('BLOG categories','blocksearch');
            $title_post_block = $this->module->l('BLOG posts','blocksearch');
            // search product in type
            if ( $this->is_ajax ){
                $search_data->setIsAjax(true);
                $suggestions = array();
                $show_full = true;
                if ( $this->enable_suggestion && $resSuggestion = $search_data->getSuggestion(false)){
                    $suggestions[] = array(
                        'value' => 'suggestion',
                        'data' => $resSuggestion,
                        'heading' => $this->module->l('Suggestions','blocksearch'),
                        'total' => ''
                    );
                }

                if ( $this->enable_search_product && $resProduct = $search_data->getProduct(false,true)){
                    $suggestions[] = array(
                        'value' => 'product',
                        'data' => $resProduct,
                        'heading' => $title_product,
                    );

                }
                if ( $this->enable_cateogry_product && $resCategories = $search_data->getCategory(false)){

                    $suggestions[] = array(
                        'value' => 'category',
                        'data' => $resCategories,
                        'heading' => $title_cat_pro,
                    );
                }

                if ( $this->enable_manufacturer && $manufacturers = $search_data->getManufacture(false)){
                    $suggestions[] = array(
                        'value' => 'manufacturer',
                        'data' => $manufacturers,
                        'heading' => $title_manu,
                    );

                }

                if ( $this->enable_cms && $cmsPage = $search_data->getCmsPage(false) ){
                    $suggestions[] = array(
                        'value' => 'cms',
                        'data' => $cmsPage,
                        'heading' => $title_cms,
                    );

                }
                if ( $this->enable_block_post && $this->module->checkModuleEnable('ybc_blog')){
                    if ($catBlockModule =  $search_data->getCategoryBlock(false)){
                        $suggestions[] = array(
                            'value' => 'catBlock',
                            'data' => $catBlockModule,
                            'heading' => $title_cat_block,
                        );

                    }

                    if($blockBlockModule = $search_data->getBlockPostInBlock(false)){
                        $suggestions[] = array(
                            'value' => 'blockBlock',
                            'data' => $blockBlockModule,
                            'heading' => $title_post_block,

                        );

                    }
                }
                if ( !$suggestions ){
                    $suggestions[] = array(
                        'value' => 'noData' ,
                        'data' => array(),
                        'heading' =>$this->module->l('Sorry, nothing found for','blocksearch'),
                        'total' => ''
                    );
                }

                die(Tools::jsonEncode(
                    array(
                        'suggestions' => $suggestions,
                        'all_total'=>$this->module->l('See all results','blocksearch'),
                        'show_full' => $show_full
                    )
                ));
            }

            if ( Tools::isSubmit('id_list') && $id_list = Tools::getValue('id_list')){
                $page = (int)Tools::getValue('page') > 0 ? (int)Tools::getValue('page') : 1;
                $params = array();
                $params['page'] = $page;
                $params['keyWord'] = Tools::getValue('search_query') ? Tools::getValue('search_query') : '';
                $params['ajax'] = true;
                $params['wrapper'] = (Tools::getValue('pagination') || Tools::getValue('searchajaxsort')) ? false : true;
                $params['id_list'] = $id_list;

                if ( Tools::isSubmit('searchajaxsort') && Tools::getValue('searchajaxsort')){
                    if (Configuration::get('YBC_BLOCKSEARCH_ALLOW_SORT') && ($sortBy = Tools::strtolower(trim(Tools::getValue('sort_by'))))){
                        if ($sortBy && !in_array($sortBy, array('position_asc', 'name_asc', 'name_desc', 'price_asc', 'price_desc')))
                            $sortBy = 'position_asc';
                        if ($sortBy) {
                            $this->context->cookie->ybc_search_order = $sortBy;
                            $this->context->cookie->write();
                        }
                        $params['order'] = $sortBy;
                    }
                }
                $res = $this->module->hookDisplayDataContent($params);
                if ( Tools::getValue('pagination') || ( Tools::getValue('searchajaxsort') ) ){
                    die(Tools::jsonEncode(
                        array(
                            'html' => $res['html'],
                            'hasData' => $res['hasData'],
                            'id_list' => $id_list,
                            'paggination' => $res['pagginations']
                        )
                    ));
                }
            }

            $array_querys = array('product','category','manufacturer','cms');
            if ( $this->module->checkModuleEnable('ybc_blog')){
                $array_querys = array_merge($array_querys,array('catBlog','blockBlog'));
            }

            $array_data = array();
            foreach ( $array_querys as $value ) {
                switch ($value) {
                    case 'product':
                        if ($this->enable_search_product && $total = $search_data->getTotalProduct($keyWord)) {
                            $array_data[] = array(
                                'id'=>$value,
                                'total'=>$total,
                                'heading' => $title_product
                            );
                        }
                        break;
                    case 'category':
                        if ($this->enable_cateogry_product && $total = $search_data->getCategory(true)) {
                            $array_data[] = array(
                                'id'=>$value,
                                'total'=>$total,
                                'heading' => $title_cat_pro
                            );
                        }
                        break;
                    case 'manufacturer':
                        if ($this->enable_manufacturer && $total = $search_data->getManufacture(true)) {
                            $array_data[] = array(
                                'id'=>$value,
                                'total'=>$total,
                                'heading' => $title_manu
                            );
                        }
                        break;
                    case 'cms':
                        if ($this->enable_cms && $total = $search_data->getCmsPage(true)) {
                            $array_data[] = array(
                                'id'=>$value,
                                'total'=>$total,
                                'heading' => $title_cms
                            );
                        }
                        break;
                    case 'catBlog':
                        if ($this->enable_block_post && $total = $search_data->getCategoryBlock(true)) {
                            $array_data[] = array(
                                'id'=>$value,
                                'total'=>$total,
                                'heading' => $title_cat_block
                            );
                        }
                        break;
                    case 'blockBlog':
                        if ($this->enable_block_post && $total = $search_data->getBlockPostInBlock(true)) {
                            $array_data[] = array(
                                'id'=>$value,
                                'total'=>$total,
                                'heading' => $title_post_block
                            );
                        }
                        break;
                    default:
                        $total = '';
                }
            }

            $this->context->smarty->assign(
                array(
                    'total' => isset($total) && $total ? $total : '',
                    'data' => $array_data,
                    'keyWord' => $keyWord,
                    'html' => isset($res['html']) && $res['html'] ? $res['html'] : false,
                    'id_list' => isset($id_list) && $id_list ? $id_list : false,
                    'title_page' => Configuration::get('YBC_BLOCKSEARCH_TITLE_PAGE',$this->context->language->id)
                )
            );

        }
        $this->setTemplate($this->template);
    }
}
