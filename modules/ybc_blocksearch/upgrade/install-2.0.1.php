<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <etssoft.jsc@gmail.com>
 *  @copyright  2007-2020 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
    exit;

function upgrade_module_2_0_1($object)
{
    if ($shops = Shop::getShops()){
        foreach ($shops as $shop){
            Configuration::updateValue('YBC_BLOCKSEARCH_RESULT_BY','cp.position asc',false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_SHOW_PRODUCT_PRICE',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_ENABLE_PRODUCT',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_SHOW_PRODUCT_RATING',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_SHOW_PRODUCT_DESCRIPTION',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_ALLOW_SORT',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_OVERLAY',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_ON_DESC',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION',20,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION_PAGES',50,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_LIMIT_AJAX',6,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_LIMIT_PAGE',9,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_MINCHART',3,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_SEARCH_SUGGESTION',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_ENABLE_BY_MANUFACTURER',1,false,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_TITLE_PAGE','Search results for',true,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_CURENT_ACTIVE','general',true,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_BUTTON_COLOR','#2fb5d2',true,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_HOVER_COLOR','#2592a9',true,null,(int)$shop['id_shop']);
        }
    }
    if (!(bool)DB::getInstance()->getValue('SELECT `id_meta` FROM `'._DB_PREFIX_.'meta` WHERE page=\'module-ybc_blocksearch-blocksearch\' ')){
        $meta = new Meta();
        $meta->page = 'module-ybc_blocksearch-blocksearch';
        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $meta->url_rewrite[$language['id_lang']] = 'search-result';
            $meta->title[$language['id_lang']] = 'search';
        }
        $meta->add();
    }
    return Configuration::deleteByName('YBC_BLOCKSEARCH_DEPTH_LEVEL')
        && Configuration::deleteByName('YBC_BLOCKSEARCH_CAT_PREFIX')
        && Configuration::deleteByName('YBC_BLOCKSEARCH_EXCLUDED_CATS')
        && $object->registerHook('moduleRoutes')
        && $object->registerHook('displayDataContent')
        && Db::getInstance()->execute("
        CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."ybc_search_suggestion` (
          `id_suggestion` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `id_shop` int(11) NOT NULL,
          PRIMARY KEY (`id_suggestion`,`id_shop`)
        ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=UTF8")
        && Db::getInstance()->execute("CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."ybc_search_suggestion_lang` (
          `id_suggestion` int(11) NOT NULL,
          `id_lang` int(11) NOT NULL,
          `keyword` varchar(200) NOT NULL,
          `searches` int(11) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id_suggestion`,`id_lang`)
         ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=UTF8")
        ;
}
