<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <etssoft.jsc@gmail.com>
 *  @copyright  2007-2020 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
    exit;

function upgrade_module_2_0_2()
{
    if ($shops = Shop::getShops()){
        foreach ($shops as $shop){
            Configuration::updateValue('YBC_BLOCKSEARCH_BUTTON_COLOR','#2fb5d2',true,null,(int)$shop['id_shop']);
            Configuration::updateValue('YBC_BLOCKSEARCH_HOVER_COLOR','#2592a9',true,null,(int)$shop['id_shop']);
        }
    }
    return Configuration::deleteByName('YBC_BLOCKSEARCH_MAINCOLOR')
           && Configuration::deleteByName('YBC_BLOCKSEARCH_MAINCOLOR');
}
