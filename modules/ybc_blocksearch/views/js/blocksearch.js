/*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*/

var instantSearchQueries = [];

function tryToCloseInstantSearch()
{
	var $oldCenterColumn = $('#old_center_column');
	if ($oldCenterColumn.length > 0)
	{
		$('#center_column').remove();
		$oldCenterColumn.attr('id', 'center_column').show();
		return false;
	}
}

function stopInstantSearchQueries()
{
	for(var i=0; i<instantSearchQueries.length; i++)
		instantSearchQueries[i].abort();
	instantSearchQueries = [];
}

// Quang update

if( typeof( UPDATE_SEARCH ) == 'undefined' )
    window.UPDATE_SEARCH = {};

window.UPDATE_SEARCH = $.extend( {
	init : function () {
		this.overlay();
		this.add_params_url();
		this.addActiveFirstItem();
		this.click_item_id();
		this.addEvenSearch();
		this.paggination();
		this.clickTab();
		this.sort_product();
		this.refrest_discount('.content_tab_product');
		this.click_link_see_all();
		//this.click_view();

	},
	click_item_id : function () {
		$(document).on('click','.item_cat_click',function (e) {
			e.preventDefault();
			var element = $(this);
			var id = element.attr('data-id');
			var data_type = element.attr('data-search-type');
		});
	},
	addEvenSearch : function () {
		var $searchQuery = $('.ybc_search_query');
		var append = $('.search_block_results');
		var serviceUrl = search_url;

		$('#search_query_top').devbridgeAutocomplete({
			minChars: (typeof minChars !== "undefined") ? minChars : 3,
			appendTo: append,
			deferRequestBy : 300,
			triggerSelectOnValidInput: false,
			serviceUrl: serviceUrl,
			forceFixPosition : true,
			params : {ajaxSearch : 'true',rand : new Date().getTime()},
			onSearchStart: function () {
			    //$('.ybc_search_query').addClass('loadding');
				if ( $('.search_block_keyword').hasClass('active') ){
					$('.search_block_keyword').removeClass('active');
				}
				if ( ! $('.button-search').hasClass('active')){
					//$('.button-search').addClass('active');
				}
			},
			onSelect: function (suggestion) {
				$('#search_query_top').val('');
			},
			onHide : function (container) {
				$('.overlay_search').removeClass('active');
			},
			onSearchComplete: function (query, suggestions,all_total,show_full) {
				$('.button-search').removeClass('active');
				if ( ! suggestions.length){
					if ( $('.overlay_search').length > 0 && $('.overlay_search').hasClass('active')){
						$('.overlay_search').removeClass('active');
					}
				}
                $('.ybc_search_query').removeClass('loadding');

				if ( $('#search_block_top .autocomplete-suggestions').css('display') == 'block' && $('.overlay_search').length > 0 && !$('.overlay_search').hasClass('active')){
					$('.overlay_search').addClass('active');
				}

                if ( suggestions.length > 0 && suggestions[0].value != 'noData' && all_total){
                	if ( $('.box_button_see').length > 0){
						$('.box_button_see').remove();
					}
					$('.search_block_results .autocomplete-suggestions').append('<div class="box_button_see"><a class="ets_link_see" href="javascript:void(0)">'+all_total+'</a></div>');
				}else{
					if ( $('.box_button_see').length > 0){
						$('.box_button_see').remove();
					}
				}

			},
			onSearchError : function (query, jqXHR, textStatus, errorThrown) {
				$('.button-search').removeClass('active');
				$('.ybc_search_query').removeClass('loadding');
			},
			beforeRender: function (container) {
				$(container).removeAttr('style');
			},

			formatResult: function (suggestion, currentValue) {
				var pattern = '(' + $.Autocomplete.utils.escapeRegExChars(currentValue) + ')';
				var html = '';

				if ( suggestion.value == 'noData'){
					html += '<div class="content_nodata"><span class="title_nodata">'+suggestion.heading+'</span> '+'<strong>'+currentValue+'</strong></div>';
				}else{
					if ( suggestion.data ){
						html +='<ul>';
						if (suggestion.heading ){
							html += '<li class="search_result_header"><span class="title_group">'+(suggestion.total ? '<span class="total">'+suggestion.total+'</span>':'')+ suggestion.heading+'</span></li>';
						}
						$.each(suggestion.data,function (i,v) {
							html += '<li class="'+suggestion.value+'" '+(v.title ? 'data-keyword="'+v.title+'"':'')+'>';

							if ( v.link ){
								html += '<a class="'+suggestion.value+'_name" href="'+v.link+'">';
							}

							if (v.img_url){
								html += '<img class="search-image" src="' + v.img_url + '">';
							}
							html += '<div class="ybc_search_item_content">';
							if ( v.title ){
								html += '<span class="search-name">' + v.title.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>') + '</span>';
							}
							if (v.price) {
								html +='<div class="box_price">';
								if (v.old_price){
									html +='<span class="regular-price">'+v.old_price+'</span>';
								}
								html +='<span>'+v.price+'</span>';
								html += '</div>';
							}
							if ( v.rating ){
								html += v.rating;
							}
							if ( v.shor_description){
								html += '<div class="item_shor_des">';
								html += '<span class="span_short">'+v.shor_description+'</span>';
								html += '</div>';
							}
							html += '</div>';
							if ( v.link ){
								html += '</a>';
							}


							html += '</li>';
						});
						html +='</ul>';
					}
				}
				return html;
			},
		});

		$(document).on('click','.suggestion',function () {
			var self = $(this);
			if (self.hasClass('is_add')){
				return false;
			}
			self.addClass('is_add');
			UPDATE_SEARCH.addSearchCount($(this).attr('data-keyword'));
			$('#search_query_top').val($(this).attr('data-keyword'));
		});
	},
	paggination : function(){
		$(document).on('click','.pagination_search a',function (evt) {
			evt.preventDefault();
			var obj = $(this);
			if ( $(this).hasClass('active')){
				return false;
			}
			obj.addClass('active');
			$('.content_tab').addClass('loadding');
			var id_list = obj.attr('data-list-id');
            var wrap_content = $('.content_tab_'+id_list);
			var wrap_pagination = $('.pagination_search_'+id_list);
			if(obj.attr('href') != ''){
				$.ajax({
					url: obj.attr('href'),
					type: 'post',
					dataType: 'json',
					data: {
						'pagination' : true
					},
					success: function(json)
					{
						if ( ! json ){
							obj.removeClass('active');
							return false;
						}
						if ( json.html && wrap_content.length > 0 ){
							wrap_content.html(json.html);
						}
						if ( json.paggination && wrap_pagination.length > 0 ){
							wrap_pagination.replaceWith(json.paggination);
						}
						UPDATE_SEARCH.refrest_discount('.content_tab_product');
						$('.content_tab').removeClass('loadding');

						//obj.removeClass('active');
						if(document.createEvent){
							var evt = document.createEvent("MutationEvents");
							evt.initMutationEvent("DOMContentLoaded", true, true, document, "", "", "", 0);
							document.dispatchEvent(evt);
						}
						window.history.pushState("", "", obj.attr('href'));
					},
					error: function()
					{
						//obj.removeClass('active');
					}
				});
			}
		});
	},
	clickTab : function () {
		$(document).on('click','.wrap_header_tap a.ajax_tab',function (evt) {
			evt.stopPropagation();
			var obj = $(this);
			var id_list = obj.attr('data-tab-id');
			$('.wrap_header_tap li.active').removeClass('active');
			$(this).addClass('active').parent('li').addClass('active');
			var content_tab = $('.content_tab');
			if ( content_tab.children('.wrap_content_'+id_list).length > 0){
				content_tab.children('.wrap_content:not(.hidden)').addClass('hidden');
				content_tab.children('.wrap_content_'+id_list).removeClass('hidden');

				var url = content_tab.children('.wrap_content_'+id_list).find('.pagination_search li.active a').attr('href');
				window.history.pushState("", "", url);
				return false;
			}
			if ( content_tab.hasClass('loadding')){
				return;
			}
			content_tab.addClass('loadding');
			$.ajax({
				url: search_url,
				type: 'post',
				dataType: 'json',
				data: {
					'id_list' : id_list,
					'search_query' : $('input[name="key_word"]').val(),
					'searchajaxclicktab' : true
				},
				success: function(json)
				{
					if ( json && json.html){
						content_tab.children('.wrap_content:not(.hidden)').addClass('hidden');
						if ( content_tab.length > 0 ){
							content_tab.append(json.html);
						}

						var url = content_tab.children('.wrap_content_'+id_list).find('.pagination_search li.active a').attr('href');
						window.history.pushState("", "", url);
					}
					content_tab.removeClass('loadding');
					obj.removeClass('active');
				},
				error: function()
				{
					content_tab.removeClass('loadding');
					obj.removeClass('active');
				}
			});
			evt.preventDefault();
		});
	},
	addActiveFirstItem : function () {
		var item_tab = $('.wrap_header_tap > ul > li:first-child');
		if ( !$('.wrap_header_tap > ul > li').hasClass('active') && item_tab.length > 0 ){
			item_tab.addClass('active').children('a').addClass('active');
		}
	},
    refrest_discount : function ($wrapper) {
        $($wrapper+" .js-product-miniature").each(function(t, e) {
            if($(this).find(".discount-product").length>0)
                var n = $(this).find(".discount-product");
            else
                var n = $(this).find(".discount-percentage");

            var i = $(this).find(".on-sale");
            var r = $(this).find(".new");
            if(n.length)
            {
                //r.css("top", n.height()+'px');
                if( r.length > 0 && i.length <= 0)
                {
                    n.css("top", '-'+($(this).find(".thumbnail-container").height() - $(this).find(".product-description").height() - 10)+'px');
                    r.css("top", (n.height() + 30)+'px');
                }
                else if ( r.length > 0 && i.length > 0){
                    n.css("top", '-'+($(this).find(".thumbnail-container").height() - $(this).find(".product-description").height() - i.height() - 20)+'px');
                    r.css("top", (n.height() + i.height() + 30)+'px');
                }
                else if (r.length <= 0 && i.length > 0){
                    n.css("top", '-'+($(this).find(".thumbnail-container").height() - $(this).find(".product-description").height() - i.height() - 20)+'px');
                }
                else {
                    n.css("top", '-'+($(this).find(".thumbnail-container").height() - $(this).find(".product-description").height() - 10)+'px');
                }
            } else {
                if ( n.length <= 0 && i.length > 0){
                    r.css("top", (i.height() + 20)+'px');
                }
            }
        });
    },
	sort_product : function () {
		$(document).on('change','.ybc_search_sort_by',function(){
			if(!(typeof $(this).attr('disabled') !== typeof undefined && $(this).attr('disabled') !== false && $(this).attr('disabled')=='disabled')) {
				var obj = $(this);
				var id_list = $(this).attr('data-id-list');
				var wrap_content = $('.content_tab_'+id_list);
				var wrap_pagination = $('.pagination_search_'+id_list);
				$('.content_tab').addClass('loadding');
				//$(this).attr('disabled', 'disabled');
				$.ajax({
					url: search_url+ '?id_list='+id_list+'&rand=' + new Date().getTime(),
					type: 'post',
					dataType: 'json',
					data: {
						searchajaxsort: true,
						sort_by: $(this).val(),
						id_list:id_list,
						search_query : $('input[name="key_word"]').val(),
					},
					success: function(json)
					{
						$('.content_tab').removeClass('loadding');
						if ( ! json ){
							obj.removeClass('active');
							return false;
						}
						if ( json.html && wrap_content.length > 0 ){
							wrap_content.html(json.html);
						}
						if ( json.paggination && wrap_pagination.length > 0 ){
							wrap_pagination.replaceWith(json.paggination);
						}
						if(document.createEvent){
							var evt = document.createEvent("MutationEvents");
							evt.initMutationEvent("DOMContentLoaded", true, true, document, "", "", "", 0);
							document.dispatchEvent(evt);
						}
					},
					error: function() {
						$('.content_tab').removeClass('loadding');
					}
				});
			}
		});
	},
	click_link_see_all : function () {
		$(document).on('click', '.ets_link_see', function(e){
			e.preventDefault();
			$('form#ybc_searchbox').submit();
		});
	},
	click_view : function () {
		var view = UPDATE_SEARCH.getCookie('ybc_display_product');
		if ( ! view ){
			view = 'list';
		}

		if (view && view != 'grid'){
			UPDATE_SEARCH.ybc_display(view);
			//$('.ybc_view_list').addClass('active');
            UPDATE_SEARCH.action_click_view_lis();
		}
		else{
			$('.ybc_view_grid').addClass('active');
		}

		$(document).on('click', '.ybc_view_grid', function(e){
			e.preventDefault();
			if ( $('.ybc_view_list').hasClass('active') ){
				$('.ybc_view_list').removeClass('active');
			}
			if ( ! $(this).hasClass('active')){
				$(this).addClass('active');
			}
			UPDATE_SEARCH.ybc_display('grid');
		});

		$(document).on('click', '.ybc_view_list', function(e){
			e.preventDefault();
			UPDATE_SEARCH.action_click_view_lis();
		});
	},
	addSearchCount : function(keyword){
		if ( !keyword ){
			return;
		}

		$.ajax({
			dataType: 'json',
			type : 'POST',
			url:search_url,
			data : {
				"keyword" : keyword,
				"add_number_key" : true,
				"ajax" : true
			},
			success : function(json){

			},
			error: function (error) {
			}
		});

		return false;
	},
    action_click_view_lis : function(){
        if ( $('.ybc_view_grid').hasClass('active') ){
            $('.ybc_view_grid').removeClass('active');
        }
        if ( ! $('.ybc_view_list').hasClass('active')){
            $('.ybc_view_list').addClass('active');
        }
        UPDATE_SEARCH.ybc_display('list');
    },
	ybc_display : function(view){
		if (view == 'list') {
			$('.content_tab_product').removeClass('grid').addClass('list');
			UPDATE_SEARCH.setCookie('ybc_display_product', 'list',1);
		}else{
			$('.content_tab_product').removeClass('list').addClass('grid');
			UPDATE_SEARCH.setCookie('ybc_display_product', 'grid',1);
		}
	},
	add_params_url : function(){
		if ( (typeof search_query !== "undefined") && search_query && (typeof UPDATE_SEARCH.getUrlParameter("search_query") == "undefined")){
			UPDATE_SEARCH.queuecomplete(search_query)
		}
	},
	getUrlParameter : function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
	},
	queuecomplete : function(key){
		if(window.location.href.indexOf('?') > -1 ) {
			var url = window.location.href+"&search_query="+key;
		}else{
			var url = window.location.href+"?search_query="+key;
		}
		window.history.pushState("", "", url);
	},
	setCookie : function (cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	},
	getCookie : function(cname){
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	},
	overlay : function(){
		if ( typeof YBC_BLOCKSEARCH_OVERLAY !== "undefined" && parseInt(YBC_BLOCKSEARCH_OVERLAY)){
			if ( $('header').length > 0){
				$('header').first().before('<div class="overlay_search"></div>')
			}
		}
	}
});

$(document).ready(function () {
	UPDATE_SEARCH.init();
	$(document).on('click','#search_block_top .autocomplete-suggestions', function (e) {
		//e.stopPropagation();
		//return false;
	});
	$(document).on('click','.overlay_search', function(e){
		e.stopPropagation();
		if ( $('.overlay_search').length > 0 && $('.overlay_search').hasClass('active')){
			$('.overlay_search').removeClass('active');
		}
		$('#search_block_top .autocomplete-suggestions').hide();

	});

	$(document).on('click','.autocomplete-suggestion a',function (e) {
		e.preventDefault();
		$('.search_block_results .autocomplete-suggestions').addClass('active_click');
		$('.search_block_results .autocomplete-suggestions').show();
		$('.overlay_search').show();
		$('.overlay_search').addClass('active');
		window.location.href = $(this).attr('href');
	});
});
