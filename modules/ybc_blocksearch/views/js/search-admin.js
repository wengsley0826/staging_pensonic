/*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*/

var UPDATE_SEARCH_ADMIN = {
    init: function () {
        this.are_ready();
        this.addTabAdmin();
        this.action_change_swich();
        this.add_miss_product();
        this.click_add_keyword();
        this.click_edit_keyword();
        this.click_cance_keyword();
        this.click_delete_keyword();
        this.clickSubmitForm();
    },
    are_ready : function(){
        if ( $('.alert.alert-success').length > 0 ){
            setTimeout(function () {
                $('.alert.alert-success').addClass('hidden');
            }, 5000);
        }
    },
    addTabAdmin: function () {
        var current_tab_active = 'general';
        $('.ets_search_form_tab .ets_search_' + current_tab_active).addClass('active');
        $('.ets_search_form .ets_search_form_' + current_tab_active).addClass('active');
        $('form').addClass(current_tab_active);
        $(document).on('click', '.ets_search_form_tab > li', function () {
            if (!$(this).hasClass('active')) {
                $('.ets_search_form > div, .ets_search_form_tab > li').removeClass('active');
                $(this).addClass('active');
                $('.ets_search_form > div.ets_search_form_' + $(this).attr('data-tab')).addClass('active');
                $('#YBC_BLOCKSEARCH_CURENT_ACTIVE').val($(this).attr('data-tab'));
                var html_of_div = $(this).html();
                $('.panel-heading').empty();
                $('.panel-heading').html(html_of_div)
            }
        });
    },
    clickSubmitForm: function () {
        $(document).on('click', '.ets_search_config[name="saveConfig"]', function (e) {
            e.preventDefault();
            var el_click = $(this);
            if (el_click.hasClass('loading')) {
                return;
            }
            el_click.addClass('loading');
            var form = $('.ets_search_config[name="saveConfig"]').parents("form");
            var form_data = new FormData(form.get(0));
            form_data.append('ajax', true);
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: form.attr('action'),
                data: form_data,
                processData: false,
                contentType: false,
                success: function (json) {
                    UPDATE_SEARCH_ADMIN.showMessage(json.message, json.messageType);
                    el_click.removeClass('loading');
                },
                error: function (error) {
                    el_click.removeClass('loading');
                }
            });

            return false;
            //$('.hc_submit_config[name="saveConfig"]').closest("form").submit();
        });
    },
    action_change_swich: function () {
        UPDATE_SEARCH_ADMIN.handle_switch_des();
        $(document).on('change', 'input[name="YBC_BLOCKSEARCH_SHOW_PRODUCT_DESCRIPTION"]', function () {
            UPDATE_SEARCH_ADMIN.handle_switch_des();
        });
        UPDATE_SEARCH_ADMIN.handle_switch_sort();
        $(document).on('change', 'input[name="YBC_BLOCKSEARCH_ALLOW_SORT"]', function () {
            UPDATE_SEARCH_ADMIN.handle_switch_sort();
        });
    },
    handle_switch_des: function () {
        if (parseInt($('input[name="YBC_BLOCKSEARCH_SHOW_PRODUCT_DESCRIPTION"]:checked').val()) == 1) {
            $('.group_number_des').show();
        } else {
            $('.group_number_des').hide();
        }
    },
    handle_switch_sort: function () {
        if (parseInt($('input[name="YBC_BLOCKSEARCH_ALLOW_SORT"]:checked').val()) == 1) {
            $('.group_sort').show();
        } else {
            $('.group_sort').hide();
        }
    },
    add_miss_product: function () {
        $(document).on('click', '.action_index_product', function (e) {
            e.preventDefault();
            var el_click = $(this);
            var form = $(this).closest('form').first();
            if (form.hasClass('loading')) {
                return;
            }
            form.addClass('loading');
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: form.attr('action')+'&index_product=true&ajax=true',
                success: function (json) {
                    if (json){
                        UPDATE_SEARCH_ADMIN.showMessage(json.message, json.messageType);
                        if ( $('.ybc_miss_product').length > 0 ){
                            $('.ybc_miss_product').remove();
                        }
                    }

                    form.removeClass('loading');
                },
                error: function (error) {
                    form.removeClass('loading');
                }
            });

            return false;
        });
    },
    click_add_keyword : function () {
        $(document).on('click','.ybc_add_keyword,.ybc_update_keyword',function (e) {
            e.preventDefault();
            var self = $(this);
            if ( self.hasClass('is_load')){
                return;
            }
            self.addClass('is_load');
            var form =$(this).closest("form").first();
            var form_data = new FormData(form.get(0));
            form_data.append('add_keyword',true);
            $.ajax(
                {
                    dataType: 'json',
                    type : 'POST',
                    url:form.attr('action'),
                    data : form_data,
                    processData : false,
                    contentType :false,
                    success : function(json){
                        if (json && json.messageType =='success'){
                            var table_wrap = $('.table.ybc_search_suggestion > tbody');
                            var html = '';
                            html +='<tr class="'+((table_wrap.children('tr').first().hasClass('odd')) ? '':'odd')+'">';
                            html += '<td class="fixed-width-xs center" data-id-suggestion="'+json.id_suggestion+'">'+json.id_suggestion+'</td>';
                            html += '<td class=" left">'+json.keyword+'</td>';
                            html += '<td class=" center">0</td>';
                            html += '<td class=" text-right">';
                            html +='<div class="btn-group-action">';
                            html +='<div class="btn-group pull-right">';
                            html +='<a class="edit btn btn-default" title="'+json.edit_text+'" href="#">';
                            html +='<i class="icon-pencil"></i>';
                            html +='&nbsp;'+json.edit_text;
                            html +='</a>';
                            html +='<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
                            html +='<i class="icon-caret-down"></i>&nbsp;';
                            html +='</button>';
                            html +='<ul class="dropdown-menu">';
                            html +='<li>';
                            html +='<a class="delete" title="'+json.delete_text+'" href="#" onclick="if (confirm(\'Delete selected item?\')){return true;}else{event.stopPropagation(); event.preventDefault();};">';
                            html +='<i class="icon-trash"></i>';
                            html +='&nbsp;'+json.delete_text;
                            html +='</a>';
                            html +='</li>';
                            html +='</ul>';
                            html +='</div>';
                            html +='</div>';
                            html += '</td>';
                            html +='</tr>';
                            if ( self.hasClass('ybc_add_keyword')){
                                table_wrap.prepend( html );
                                if ($('.list-empty').length > 0 ){
                                    $('.list-empty').remove();
                                }
                                $('.ybc_input_keyword').attr('value','');
                            }

                            if ( self.hasClass('ybc_update_keyword')){

                                table_wrap.find('td[data-id-suggestion="'+json.id_suggestion+'"]').first().parent('tr').replaceWith(html);
                            }
                        }
                        UPDATE_SEARCH_ADMIN.showMessage(json.message,json.messageType);

                        self.removeClass('is_load');
                    },
                    error: function (error) {
                        self.removeClass('is_load');
                    }
                });

            return false;
        });
    },
    click_edit_keyword : function (){
        $(document).on('click','.table.ybc_search_suggestion a.edit',function (e) {
            e.preventDefault();
            self = $(this);
            var el_edit = $('.table.ybc_search_suggestion a.edit');
            if ( self.hasClass('is_load') && el_edit.hasClass('disable')){
                return;
            }
            self.addClass('is_load');
            el_edit.addClass('disable');
            var id_suggestion = parseInt(self.closest('tr').first().children('td').first().text());
            var form =$(this).closest("form").first();
            $.ajax({
                dataType: 'json',
                type : 'POST',
                url:form.attr('action'),
                data : {
                    "id_suggestion" : id_suggestion,
                    "edit_suggestion" : true,
                    "ajax" : true
                },
                success : function(json){
                    if (json && json.hasData){
                        $.each(json.data,function (i,v) {
                            $('#YBC_BLOCKSEARCH_TYPE_KEYWORD_'+v.id_lang).attr('value',v.keyword);
                        });
                        $('#YBC_BLOCKSEARCH_ID_KEYWORD').attr('value',json.id_suggestion);

                        if ( ! $('.ybc_add_keyword.btn').hasClass('hidden')){
                            $('.ybc_add_keyword.btn').addClass('hidden');
                        }
                        $('.box_update').removeClass('hidden');
                    }
                    self.removeClass('is_load');
                    el_edit.removeClass('disable');

                    $([document.documentElement, document.body]).animate({
                        scrollTop: $(".row_ybc_blocksearch_type_keyword").offset().top -300
                    }, 1000);
                },
                error: function (error) {
                    self.removeClass('is_load');
                    el_edit.removeClass('disable');
                }
            });


            return false;
        });
    },
    click_delete_keyword : function(){
        $(document).on('click','#table-ybc_search_suggestion .delete',function (e) {
            e.preventDefault();
            self = $(this);
            if ( self.hasClass('is_load')){
                return;
            }
            self.addClass('is_load');
            var id_suggestion = parseInt(self.closest('tr').first().children('td').first().text());
            var form =$(this).closest("form").first();
            $.ajax({
                dataType: 'json',
                type : 'POST',
                url:form.attr('action'),
                data : {
                    "id_suggestion" : id_suggestion,
                    "delete_suggestion" : true,
                    "ajax" : true
                },
                success : function(json){
                    if (json){
                        UPDATE_SEARCH_ADMIN.showMessage(json.message,json.messageType);
                        if ( json.messageType == 'success'){
                            self.closest('tr').first().remove();
                        }
                    }
                    self.removeClass('is_load');

                },
                error: function (error) {
                    self.removeClass('is_load');
                }
            });

            return false;
        });
    },
    click_cance_keyword : function(){
        $(document).on('click','.ybc_cace_update',function (e) {
            e.preventDefault();
            $('.ybc_input_keyword').attr('value','');
            $('#YBC_BLOCKSEARCH_ID_KEYWORD').attr('value','');
            if ( ! $('.box_update').hasClass('hidden')){
                $('.box_update').addClass('hidden');
            }
            $('.ybc_add_keyword.btn').removeClass('hidden');
        });
    },

    showMessage: function (message, type) {

        if ($('.ybc_search_alert').length > 0) {
            $('.ybc_search_alert').remove();
        }

        if ($('.ybc_search_alert').length <= 0) {
            $('body').append('<div class="ybc_search_alert hidden"></div>');
        }
        $('.ybc_search_alert').addClass('hidden').removeClass('error').removeClass('success').addClass(type == 'error' ? 'error alert alert-danger' : 'success alert alert-success').html(message).removeClass('hidden');
        if (type != 'error') {
            setTimeout(function () {
                $('.ybc_search_alert').addClass('hidden');
            }, 5000);
        }else{
            setTimeout(function () {
                $('.ybc_search_alert').addClass('hidden');
            }, 8000);
        }
    },
};

$(document).ready(function () {
    UPDATE_SEARCH_ADMIN.init();
});
