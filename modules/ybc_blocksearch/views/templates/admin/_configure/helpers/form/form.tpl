{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{extends file="helpers/form/form.tpl"}
{block name="input"}
    {if $input.type == 'text' && $input.name=='YBC_BLOCKSEARCH_TYPE_KEYWORD'}
    {if isset($input.lang) AND $input.lang}
        {if $languages|count > 1}
            <div class="form-group">
        {/if}
        {foreach $languages as $language}
            {assign var='value_text' value=$fields_value[$input.name][$language.id_lang]}
            {if $languages|count > 1}
                <div class="translatable-field lang-{$language.id_lang|escape:'html':'UTF-8'}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
                <div class="col-lg-7">
            {else}
                <div class="col-lg-7">
            {/if}
            {if $input.type == 'tags'}
            {literal}
                <script type="text/javascript">
                    $().ready(function () {
                        var input_id = '{/literal}{if isset($input.id)}{$input.id|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{else}{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{/if}{literal}';
                        $('#' + input_id).tagify({
                            delimiters: [13, 44],
                            addTagPrompt: '{/literal}{l s='Add tag' js=1}{literal}'
                        });
                        $({/literal}'#{$table|escape:'html':'UTF-8'}{literal}_form').submit(function () {
                            $(this).find('#' + input_id).val($('#' + input_id).tagify('serialize'));
                        });
                    });
                </script>
            {/literal}
            {/if}
        {if isset($input.maxchar) || isset($input.prefix) || isset($input.suffix)}
            <div class="input-group{if isset($input.class)} {$input.class|escape:'html':'UTF-8'}{/if}">
        {/if}
            {if isset($input.maxchar) && $input.maxchar}
                <span id="{if isset($input.id)}{$input.id|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{else}{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{/if}_counter"
                      class="input-group-addon">
													<span class="text-count-down">{$input.maxchar|intval}</span>
												</span>
            {/if}
            {if isset($input.prefix)}
                <span class="input-group-addon">
													  {$input.prefix|escape:'html':'UTF-8'}
													</span>
            {/if}
            <input type="text"
                   id="{if isset($input.id)}{$input.id|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{else}{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{/if}"
                   name="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}"
                   class="{if isset($input.class)}{$input.class|escape:'html':'UTF-8'}{/if}{if $input.type == 'tags'} tagify{/if}"
                   value="{if isset($input.string_format) && $input.string_format}{$value_text|string_format:$input.string_format|escape:'html':'UTF-8'}{else}{$value_text|escape:'html':'UTF-8'}{/if}"
                   onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();"
                    {if isset($input.size)} size="{$input.size|escape:'html':'UTF-8'}"{/if}
                    {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
                    {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
                    {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
                    {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
                    {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
                    {if isset($input.required) && $input.required} required="required" {/if}
                    {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder|escape:'html':'UTF-8'}"{/if} />
            {if isset($input.suffix)}
                <span class="input-group-addon">
													  {$input.suffix|escape:'html':'UTF-8'}
													</span>
            {/if}
        {if isset($input.maxchar) || isset($input.prefix) || isset($input.suffix)}
            </div>
        {/if}
            {if $languages|count > 1}
                </div>
                <div class="col-lg-4">
                    <button type="button"
                            class="btn btn-default dropdown-toggle"
                            tabindex="-1"
                            data-toggle="dropdown">
                        {$language.iso_code|escape:'html':'UTF-8'}
                        <i class="icon-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu">
                        {foreach from=$languages item=language}
                            <li>
                                <a href="javascript:hideOtherLanguage({$language.id_lang|escape:'html':'UTF-8'});"
                                   tabindex="-1">{$language.name|escape:'html':'UTF-8'}</a>
                            </li>
                        {/foreach}
                    </ul>
                    <a class="ybc_add_keyword btn btn-default" href="javascript:void(0)"><i class="icon-plus"></i>{l s='Add' mod='ybc_blocksearch'}</a>
                    <div class="box_update hidden">
                        <a class="ybc_update_keyword btn btn-default" href="javascript:void(0)"><i class="icon-wrench"></i>{l s='Update' mod='ybc_blocksearch'}</a>
                        <a class="ybc_cace_update btn btn-default" href="javascript:void(0)"><i class="icon-times"></i>{l s='Cancel' mod='ybc_blocksearch'}</a>
                    </div>
                </div>
                </div>
                    {else}
                </div>
                <a class="ybc_add_keyword btn btn-default" href="javascript:void(0)"><i class="icon-plus"></i>{l s='Add' mod='ybc_blocksearch'}</a>
                <div class="box_update hidden">
                    <a class="ybc_update_keyword btn btn-default" href="javascript:void(0)"><i class="icon-wrench"></i>{l s='Update' mod='ybc_blocksearch'}</a>
                    <a class="ybc_cace_update btn btn-default" href="javascript:void(0)"><i class="icon-times"></i>{l s='Cancel' mod='ybc_blocksearch'}</a>
                </div>
            {/if}


        {/foreach}
        {if isset($input.maxchar) && $input.maxchar}
            <script type="text/javascript">
                $(document).ready(function () {
                    {foreach from=$languages item=language}
                    countDown($("#{if isset($input.id)}{$input.id|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{else}{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{/if}"), $("#{if isset($input.id)}{$input.id|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{else}{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'html':'UTF-8'}{/if}_counter"));
                    {/foreach}
                });
            </script>
        {/if}
        {if $languages|count > 1}
            </div>
        {/if}
    {/if}

    {else}
        {$smarty.block.parent}
    {/if}
{/block}
{block name="field"}
    {$smarty.block.parent}
	{if $input.type == 'file' &&  isset($input.display_img) && $input.display_img}
        <label class="control-label col-lg-3" style="font-style: italic;">{l s='Uploaded image: ' mod='ybc_blocksearch'}</label>
        <div class="col-lg-9">
    		<a  class="ybc_fancy" href="{$input.display_img|escape:'html':'UTF-8'}"><img title="{l s='Click to see full size image' mod='ybc_blocksearch'}" style="display: inline-block; max-width: 200px;" src="{$input.display_img|escape:'html':'UTF-8'}" /></a>
            {if isset($input.img_del_link) && $input.img_del_link && !(isset($input.required) && $input.required)}
                <a onclick="return confirm('{l s='Do you want to delete this image?' mod='ybc_blocksearch'}');" style="display: inline-block; text-decoration: none!important;" href="{$input.img_del_link|escape:'html':'UTF-8'}"><span style="color: #666"><i style="font-size: 20px;" class="process-icon-delete"></i></span></a>
            {/if}
        </div>
    {/if}
{/block}


{block name="input_row"}
	{if $input.name=='YBC_BLOCKSEARCH_WARNING'}
		<div class="ets_search_form_tab_content">
			<div class="ets_search_form_tab_div">
                <div class="header_search_tab">
                    <h3 class="title">{l s='Total Search Pro' mod='ybc_blocksearch'}</h3>
                    <p class="search_sub_title">{l s='Search by products, categories, CMS pages, brands and more instantly' mod='ybc_blocksearch'}</p>
                </div>
				<ul class="ets_search_form_tab">
					<li class="ets_search_general" data-tab="general"><i class="icon-cogs"></i> {l s='General settings' mod='ybc_blocksearch'}</li>
					<li class="ets_search_result" data-tab="result"><i class="icon-sort-amount-desc"></i> {l s='Search result' mod='ybc_blocksearch'}</li>
                    <li class="ets_search_keyword" data-tab="keyword"><i class="icon-keyboard"></i> {l s='Popular search keywords' mod='ybc_blocksearch'}</li>
				</ul>
			</div>
			<div class="ets_search_form">
				<div class="ets_search_form_general">
				{if $input.show}
					<div class="alert alert-info ybc_miss_product" role="alert">
						{l s='Indexed products:' mod='ybc_blocksearch'} <strong>{$input.show|escape:'html':'UTF-8'}.</strong>
						{l s='The "indexed" products have been analyzed by PrestaShop and will appear in the results of a front office search.' mod='ybc_blocksearch'}
						<a href="#" class="action_index_product">{l s='Click here to add' mod='ybc_blocksearch'} <strong>{$input.index_miss|intval}</strong> {l s='missing products to the index' mod='ybc_blocksearch'}</a>
					</div>
				{/if}
				{/if}
				{if $input.name=='YBC_BLOCKSEARCH_SHOW_PRODUCT_IMAGE'}
				</div><div class="ets_search_form_result">
				{/if}
				{if $input.name=='YBC_BLOCKSEARCH_SEARCH_SUGGESTION'}
					</div><div class="ets_search_form_keyword">
				{/if}
				<div class="form-group-wrapper row_{strtolower($input.name)|escape:'html':'UTF-8'}">
					{if $input.type=='suggestion'}
						{$input.suggestion nofilter}
					{else}
						{$smarty.block.parent}
					{/if}
				</div>
				{if $input.name=='YBC_BLOCKSEARCH_SUGGESTION_FORM'}
				</div>
			</div>
		</div>
	{/if}
{/block}
{block name="footer"}
    {capture name='form_submit_btn'}{counter name='form_submit_btn'}{/capture}
	{if isset($fieldset['form']['submit']) || isset($fieldset['form']['buttons'])}
		<div class="panel-footer">               
            {if isset($fieldset['form']['submit']) && !empty($fieldset['form']['submit'])}
			<button type="submit" value="1"	id="{if isset($fieldset['form']['submit']['id'])}{$fieldset['form']['submit']['id']|escape:'html':'UTF-8'}{else}{$table|escape:'html':'UTF-8'}_form_submit_btn{/if}{if $smarty.capture.form_submit_btn > 1}_{($smarty.capture.form_submit_btn - 1)|intval}{/if}" name="{if isset($fieldset['form']['submit']['name'])}{$fieldset['form']['submit']['name']|escape:'html':'UTF-8'}{else}{$submit_action|escape:'html':'UTF-8'}{/if}{if isset($fieldset['form']['submit']['stay']) && $fieldset['form']['submit']['stay']}AndStay{/if}" class="{if isset($fieldset['form']['submit']['class'])}{$fieldset['form']['submit']['class']|escape:'html':'UTF-8'}{else}btn btn-default pull-right{/if}">
				<i class="{if isset($fieldset['form']['submit']['icon'])}{$fieldset['form']['submit']['icon']|escape:'html':'UTF-8'}{else}process-icon-save{/if}"></i> {$fieldset['form']['submit']['title']|escape:'html':'UTF-8'}
			</button>
			{/if}
            
		</div>
	{/if}
{/block}
