{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
<!-- Block search module TOP -->
<script type="text/javascript">
var id_lang ={$id_lang|intval};
</script>
<div id="search_block_top" class="col-sm-12 col-md-6 col-lg-5 clearfix	pull-right">
	<form id="ybc_searchbox" method="post" action="{if isset($page_search) && $page_search}{$page_search|escape:'html':'UTF-8'}{/if}" >
        <input class="search_query form-control ybc_search_query" type="text" id="search_query_top" name="search_query" placeholder="{if isset($YBC_BLOCKSEARCH_PLACEHOLDER) && $YBC_BLOCKSEARCH_PLACEHOLDER}{$YBC_BLOCKSEARCH_PLACEHOLDER|escape:'html':'UTF-8'}{else}{l s='Enter product name ...' mod='ybc_blocksearch'}{/if}" value="{$search_query|escape:'htmlall':'UTF-8'|stripslashes}" />
		<button type="submit"  class="btn btn-default button-search" >
			<span>{l s='Search' mod='ybc_blocksearch'}</span>
		</button>
	</form>
	<div class="search_block_results" id="search_block_results">
	</div>
</div>

<style type="text/css">
	{if isset($button_color) && $button_color}
		#search_block_top .btn.button-search{
			background-color: {$button_color|escape:'html':'UTF-8'};
		}
		#search_block_top .btn.button-search{
			color:{$button_color|escape:'html':'UTF-8'};
		}

	{/if}
	{if isset($button_hover) && $button_hover}
		#search_block_top .btn.button-search:hover{
			background-color: {$button_hover|escape:'html':'UTF-8'};
		}
	{/if}
	{if isset($main_color) && $main_color}
		/*span.title_group{
			color:{* $main_color|escape:'html':'UTF-8' *};
			border-color: {* $main_color|escape:'html':'UTF-8' *};
		}*/
		.title_group span.total,
		.search_block_results .box_button_see{
			background-color:{$main_color|escape:'html':'UTF-8'};
		}
		.wrap_header_tap ul li.active a{
			background-color: {$main_color|escape:'html':'UTF-8'};
		}
	{/if}
	{if isset($text_color) && $text_color}
		#search_block_top .btn.button-search::before{
			color:{$text_color|escape:'html':'UTF-8'};
		}
		.title_group span.total{
			color:{$text_color|escape:'html':'UTF-8'};
		}
		a.ets_link_see{
			color:{$text_color|escape:'html':'UTF-8'} !important;
		}
		.wrap_header_tap ul li.active a{
			color:{$text_color|escape:'html':'UTF-8'} !important;
		}
		.wrap_header_tap ul li.active a span{
			color:{$text_color|escape:'html':'UTF-8'};
		}
	{/if}

</style>
<!-- /Block search module TOP -->
