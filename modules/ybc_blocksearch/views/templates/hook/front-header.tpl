{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}

{if isset($minChars) && $minChars}
    <script type="text/javascript">
        var minChars ='{$minChars|intval}';
        var main_color='{$main_color|escape:'html':'utf-8'}';
        var text_color='{$text_color|escape:'html':'utf-8'}';
    </script>
{/if}
{if isset($search_query) && $search_query}
    <script type="text/javascript">
        var search_query ='{$search_query|escape:'html':'utf-8'}';
    </script>
{/if}
{if isset($is176) && $is176}
    <script type="text/javascript">
        var is176 ='{$is176|escape:'html':'utf-8'}';
    </script>
{/if}
{if isset($YBC_BLOCKSEARCH_OVERLAY) && $YBC_BLOCKSEARCH_OVERLAY}
    <script type="text/javascript">
        var YBC_BLOCKSEARCH_OVERLAY ='{$YBC_BLOCKSEARCH_OVERLAY|intval}';
        var YBC_BLOCKSEARCH_SEARCH_SUGGESTION ='{$YBC_BLOCKSEARCH_SEARCH_SUGGESTION|intval}';
    </script>
{/if}