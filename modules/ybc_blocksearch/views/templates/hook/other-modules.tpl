{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
{if isset($intro) && $intro}
    <div class="li_othermodules ">
        {if isset($refsLink) && $refsLink}
            <a class="link_othermodules refs_othermodules" href="{$other_modules_link|escape:'html':'UTF-8'}" target="_blank">
                <span class="tab-title">{l s='Other modules mode by ETS-Soft' mod='ybc_blocksearch'}</span>
            <a/>
        {else}
            <div class="link_othermodules" data-href="{$other_modules_link|escape:'html':'UTF-8'}">
                <span class="tab-title">{l s='Other modules mode by ETS-Soft' mod='ybc_blocksearch'}</span>
            </div>
        {/if}
    </div>
{/if}