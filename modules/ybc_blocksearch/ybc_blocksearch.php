<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  @version  Release: $Revision$
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

require_once(dirname(__FILE__) . '/classes/search_data.php');
require_once(dirname(__FILE__) . '/classes/ybc_search_suggestion.php');
class Ybc_blockSearch extends Module
{
    private $errorMessage;
    public $configs;
    public $baseAdminPath;

    private $_html;
    public $url_ajax = '';
    public $is17 = false;

    private $total_product = 0;
    private $indexed_product = 0;

    public $title_garena = '';
    public $title_result = '';
    public $title_keyword = '';

    public $fields_list = array();
    protected $list_id = '';
    protected $_filter;
    protected $_filterHaving;
	public function __construct()
	{
        $this->refs = 'https://prestahero.com/';
		$this->name = 'ybc_blocksearch';
		$this->tab = 'search_filter';
		$this->version = '2.0.3';
		$this->author = 'ETS-Soft';
		$this->need_instance = 0;
        $this->bootstrap = true;
		parent::__construct();

        $this->module_key = '825a85c8179fde60df0fd209e69246f9';
        $this->author_address = '0xd81C21A85a637315C623D9c1F9D4f5Bb3144A617';
		$this->displayName = $this->l('Total Search Pro');
		$this->description = $this->l('Search everywhere, quick, convenient and smart. Allow customer to navigate through everywhere on your website easily.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->is17 = version_compare(_PS_VERSION_, '1.7', '>=');
        list($this->total_product, $this->indexed_product) = Db::getInstance()->getRow('SELECT COUNT(*) as "0", SUM(product_shop.indexed) as "1" FROM ' . _DB_PREFIX_ . 'product p ' . Shop::addSqlAssociation('product', 'p') . ' WHERE product_shop.`visibility` IN ("both", "search") AND product_shop.`active` = 1');
        $this->title_garena = $this->l('General settings');
        $this->title_result = $this->l('Search result');
        $this->title_keyword = $this->l('Popular search keywords');
        $this->list_id = 'ybc_search_suggestion';
        $this->shortlink = 'https://mf.short-link.org/';
        if(Tools::getValue('configure')==$this->name && Tools::isSubmit('othermodules'))
        {
            $this->displayRecommendedModules();
        }
        //Config fields
        $this->configs = array(
            'YBC_BLOCKSEARCH_WARNING' => array(
                'label' => $this->l('Notification warning'),
                'type' => 'hidden',
                'show' => $this->indexed_product == $this->total_product ? false : (int)$this->indexed_product.'/'.(int)$this->total_product,
                'index_miss' => $this->total_product-$this->indexed_product,
            ),
            'YBC_BLOCKSEARCH_ENABLE_PRODUCT' => array(
                'label' => $this->l('Enable product search'),
                'type' => 'switch',
                'default' => 1
            ),
            'YBC_BLOCKSEARCH_ENABLE_BY_CAT' => array(
                'label' => $this->l('Enable product category search'),
                'type' => 'switch',
                'default' => 1
            ),
            'YBC_BLOCKSEARCH_ENABLE_BY_MANUFACTURER' => array(
                'label' => $this->l('Enable manufacturer search'),
                'type' => 'switch',
                'default' => 1
            ),
            'YBC_BLOCKSEARCH_ENABLE_BY_CMSPOST' => array(
                'label' => $this->l('Enable CMS page search'),
                'type' => 'switch',
                'default' => 1
            ),
            'YBC_BLOCKSEARCH_MINCHART' => array(
                'label' => $this->l('Minimum number of input characters'),
                'type' => 'text',
                'class' => 'fixed-width-xs',
                'default' => '3',
                'required' => true,
                'validate' => 'isInt',
                'desc' => $this->l('Set up the minimum number of input characters for search text. At least 3 characters')
            ),
            /*'YBC_BLOCKSEARCH_SEE_ALL_TEXT' => array(
                'label' => $this->l('See all results'),
                'type' => 'text',
                'form_group_class' => 'group_result_text',
                'default' => $this->l('See all results'),
                'lang' => true,
            ),*/
            'YBC_BLOCKSEARCH_PLACEHOLDER' => array(
                'label' => $this->l('Search box input placeholder'),
                'type' => 'text',
                'form_group_class' => 'group_placeholder',
                'default' => $this->l('Search...'),
                'required' => true,
                'lang' => true,
            ),
            'YBC_BLOCKSEARCH_BUTTON_COLOR' => array(
                'label' => $this->l('Search button color'),
                'type' => 'color',
                'default' => '#2fb5d2',
            ),
            'YBC_BLOCKSEARCH_HOVER_COLOR' => array(
                'label' => $this->l('Hover color'),
                'type' => 'color',
                'default' => '#2592a9',
            ),

            'YBC_BLOCKSEARCH_SHOW_PRODUCT_IMAGE' => array(
                'label' => $this->l('Show product image in Ajax search result'),
                'type' => 'switch',
                'default' => 1
            ),
            'YBC_BLOCKSEARCH_SHOW_PRODUCT_PRICE' => array(
                'label' => $this->l('Show product price in Ajax search result'),
                'type' => 'switch',
                'default' => 1
            ),
            'YBC_BLOCKSEARCH_SHOW_PRODUCT_DESCRIPTION' => array(
                'label' => $this->l('Show short description in Ajax search result'),
                'type' => 'switch',
                'default' => 1
            ),
            'YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION' => array(
                'label' => $this->l('Limit word number of short description in Ajax search result'),
                'type' => 'text',
                'default' => '20',
                'class' => 'fixed_width_number',
                'form_group_class' => 'group_number_des',
                'validate' => 'isInt',
            ),
            'YBC_BLOCKSEARCH_LIMIT_CHARACTER_DESCRIPTION_PAGES' => array(
                'label' => $this->l('Limit word number of short description in Search result page'),
                'type' => 'text',
                'default' => '50',
                'class' => 'fixed_width_number',
                'validate' => 'isInt',
            ),
            'YBC_BLOCKSEARCH_LIMIT_AJAX' => array(
                'label' => $this->l('Number of Ajax search result'),
                'type' => 'text',
                'default' => '3',
                'class' => 'fixed_width_number',
                'validate' => 'isInt',
            ),
            'YBC_BLOCKSEARCH_LIMIT_PAGE' => array(
                'label' => $this->l('Number of result on Search result page'),
                'type' => 'text',
                'default' => '12',
                'class' => 'fixed_width_number',
                'validate' => 'isInt',
            ),
            'YBC_BLOCKSEARCH_ON_DESC' => array(
                'label' => $this->l('Search on description'),
                'type' => 'switch',
                'default' => 1,
                'desc' => $this->l('Search on the description of products, categories, manufacturers, suppliers, etc.')
            ),
            'YBC_BLOCKSEARCH_TITLE_PAGE' => array(
                'label' => $this->l('Title of Search result page'),
                'type' => 'text',
                'form_group_class' => 'group_placeholder',
                'default' => $this->l('Search results for'),
                'lang' => true,
            ),
            'YBC_BLOCKSEARCH_ALLOW_SORT' => array(
                'label' => $this->l('Enable "Sort by" feature on the front office'),
                'type' => 'switch',
                'default' => 1,
            ),
            'YBC_BLOCKSEARCH_RESULT_BY' => array(
                'label' => $this->l('Default product sort by'),
                'type' => 'select',
                'form_group_class' => 'group_sort',
                'options' => array(
                    'query' => array(
                        array(
                            'id_option' => 'position_desc',
                            'name' => $this->l('Relevance')
                        ),
                        array(
                            'id_option' => 'name_asc',
                            'name' => $this->l('Product name: A-Z')
                        ),
                        array(
                            'id_option' => 'name_desc',
                            'order_way' => '',
                            'name' => $this->l('Product name: Z-A')
                        ),
                        array(
                            'id_option' => 'price_asc',
                            'name' => $this->l('Price: Lowest first')
                        ),
                        array(
                            'id_option' => 'price_desc',
                            'name' => $this->l('Price: Highest first')
                        ),
                    ),
                    'id' => 'id_option',
                    'name' => 'name'
                ),
                'default' => 'position_asc',
            ),
            'YBC_BLOCKSEARCH_OVERLAY' => array(
                'label' => $this->l('Enable overlay background while searching'),
                'type' => 'switch',
                'default' => 1,
            ),

            'YBC_BLOCKSEARCH_SEARCH_SUGGESTION' => array(
                'label' => $this->l('Enable search suggestion'),
                'type' => 'switch',
                'default' => 1,
            ),
            'YBC_BLOCKSEARCH_TYPE_KEYWORD' => array(
                'label' => $this->l('Popular search keyword'),
                'type' => 'text',
                'default' => $this->l(''),
                'lang' => true,
                'class' => 'ybc_input_keyword'
            ),
            'YBC_BLOCKSEARCH_SUGGESTION_FORM' => array(
                'label' => $this->l('SUGGESTION_FORM'),
                'type' => 'suggestion',
                'suggestion'=> ''
            ),
            'YBC_BLOCKSEARCH_ID_KEYWORD' => array(
                'label' => '',
                'type' => 'hidden',
            ),
            'YBC_BLOCKSEARCH_CURENT_ACTIVE' => array(
                'label' => '',
                'type' => 'hidden',
            ),

        );
        if( $this->checkModuleEnable('ybc_blog') ){
            $temp = array('YBC_BLOCKSEARCH_ENABLE_BY_BLOGPOST' => array(
                'label' => $this->l('Enable BLOG post search'),
                'type' => 'switch',
                'default' => 1
            ));
            $this->array_insert($this->configs,5,$temp);
        }
        if( $this->checkModuleEnable('productcomments') ){
            $temp = array('YBC_BLOCKSEARCH_SHOW_PRODUCT_RATING' => array(
                'label' => $this->l('Show product rating in Ajax search result'),
                'type' => 'switch',
                'default' => 1
            ));
            $this->array_insert($this->configs,11,$temp);
        }
	}

    public function install()
	{
        if (version_compare(_PS_VERSION_, '1.7', '>=')){
            Module::disableByName('ps_searchbar'); //1.7
        }else{
            Module::disableByName('blocksearch');
        }


        //Module::disableByName('blocksearch'); 1.6
        //Module::disableByName('ps_searchbar'); //1.7
        $dir = _PS_ROOT_DIR_.'/override/controllers/front/listing';
        if(!is_dir($dir)){
            @mkdir($dir, 0777);
        }
        Configuration::updateValue('PS_SEARCH_AJAX',1);
		if (!parent::install()
            || !$this->_installDb()
            || !$this->registerHook('top')
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('displayMobileTopSiteMap')
            || !$this->registerHook('displayProductSearchList')
            || !$this->registerHook('displayDataContent')
            || !$this->registerHook('displayReviews')
        )
			return false;
		return true;
	}


    public function _installDb()
    {

        $languages = Language::getLanguages(false);
        if($this->configs)
        {
            foreach($this->configs as $key => $config)
            {
                if(isset($config['lang']) && $config['lang'])
                {
                    $values = array();
                    foreach($languages as $lang)
                    {
                        $values[$lang['id_lang']] = isset($config['default']) ? $config['default'] : '';
                    }
                    Configuration::updateValue($key, $values,true);
                }
                else
                    Configuration::updateValue($key, isset($config['default']) ? $config['default'] : '',true);
            }
        }
        $res = true;
        $sqls = array();
        $sqls[] = "
        CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."ybc_search_suggestion` (
          `id_suggestion` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `id_shop` int(11) NOT NULL,
          PRIMARY KEY (`id_suggestion`)
        ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=UTF8";

        $sqls[]="CREATE INDEX idx_id_shop ON `" . _DB_PREFIX_ . "ybc_search_suggestion` (id_shop)";

        $sqls[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."ybc_search_suggestion_lang` (
          `id_suggestion` int(11) NOT NULL,
          `id_lang` int(11) NOT NULL,
          `keyword` varchar(200) NOT NULL,
          `searches` int(11) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id_suggestion`,`id_lang`)
         ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=UTF8";

        if($sqls)
        {
            foreach($sqls as $sql)
            {
                $res &=Db::getInstance()->execute($sql);
            }
        }

        if (!(bool)DB::getInstance()->getValue('SELECT `id_meta` FROM `'._DB_PREFIX_.'meta` WHERE page=\'module-ybc_blocksearch-blocksearch\' ')){
            $meta = new Meta();
            $meta->page = 'module-ybc_blocksearch-blocksearch';
            $languages = Language::getLanguages(false);
            foreach ($languages as $language){
                $meta->url_rewrite[$language['id_lang']] = 'search-result';
                $meta->title[$language['id_lang']] = 'search';
            }
            $res &=$meta->add();
        }
        return $res;
    }
    /**
     * @see Module::uninstall()
     */
    public function uninstall()
    {
        return parent::uninstall()&&$this->_uninstallDb();
    }
    private function _uninstallDb()
    {
        if($this->configs)
        {
            foreach($this->configs as $key => $config)
            {
                Configuration::deleteByName($key);
            }
            unset($config);
        } 
        $dirs = array('config');
        foreach($dirs as $dir)
        {
            $files = glob(dirname(__FILE__).'/views/img/'.$dir.'/*'); 
            foreach($files as $file){ 
              if(is_file($file) && $file != dirname(__FILE__).'/views/img/'.$dir.'/index.php')
                @unlink($file); 
            }
        }

        return
            true && Db::getInstance()->execute("DROP TABLE IF EXISTS 
			    `" . _DB_PREFIX_ . "ybc_search_suggestion`,
				`" . _DB_PREFIX_ . "ybc_search_suggestion_lang`");
    }       
	public function hookdisplayMobileTopSiteMap($params)
	{
		$this->smarty->assign(array('hook_mobile' => true, 'instantsearch' => false));
		$params['hook_mobile'] = true;
		return $this->hookTop($params);
	}

	public function hookDisplayHeader($params)
	{
	    $this->url_ajax = $this->context->link->getModuleLink('ybc_blocksearch','blocksearch',array(),Tools::usingSecureMode());
		$this->context->controller->addCSS(($this->_path).'views/css/blocksearch.css', 'all');
        $this->context->controller->addCSS(($this->_path).'views/css/nice-select.css', 'all');
        $this->context->controller->addCSS(($this->_path).'views/css/font-awesome.min.css', 'all');

        $this->context->controller->addJS($this->_path.'views/js/dropdown.js');

        if (!$this->is17){
            $this->context->controller->addCSS(_THEME_CSS_DIR_ . 'product_list.css');
        }

		if (Configuration::get('PS_INSTANT_SEARCH'))
			$this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
		if (Configuration::get('PS_SEARCH_AJAX') || Configuration::get('PS_INSTANT_SEARCH'))
		{
			Media::addJsDef(array('search_url' => $this->url_ajax));
            $this->context->controller->addJS($this->_path.'views/js/autocomplete.js');
			$this->context->controller->addJS($this->_path.'views/js/blocksearch.js');
		}
        if ( $this->checkModuleEnable('productcomments') && Dispatcher::getInstance()->getController() == 'blocksearch' ){
            if (version_compare(_PS_VERSION_, '1.7.6.0', '>=') ){
                $jsList = [];
                $cssList = [];
                $cssList[] = '/modules/productcomments/views/css/productcomments.css';
                $jsList[] = '/modules/productcomments/views/js/jquery.rating.plugin.js';

                $jsList[] = '/modules/productcomments/views/js/post-comment.js';
                $jsList[] = '/modules/productcomments/views/js/list-comments.js';
                $jsList[] = '/modules/productcomments/views/js/jquery.simplePagination.js';
                foreach ($cssList as $cssUrl) {
                    $this->context->controller->registerStylesheet(sha1($cssUrl), $cssUrl, ['media' => 'all', 'priority' => 80]);
                }
                foreach ($jsList as $jsUrl) {
                    $this->context->controller->registerJavascript(sha1($jsUrl), $jsUrl, ['position' => 'bottom', 'priority' => 80]);
                }
                $this->context->smarty->assign(
                    array(
                        'is176' => true
                    )
                );
            }else{
                $jsList =array();
                $jsList[] = '/modules/productcomments/js/jquery.rating.pack.js';
                foreach ($jsList as $jsUrl) {
					$this->context->controller->addJS($jsUrl);
                    //$this->context->controller->registerJavascript(sha1($jsUrl), $jsUrl, ['position' => 'bottom', 'priority' => 80]);
                }
            }
        }

		$this->context->smarty->assign(
		    array(
		        'YBC_BLOCKSEARCH_SEARCH_SUGGESTION' => (int)Configuration::get('YBC_BLOCKSEARCH_SEARCH_SUGGESTION'),
                'YBC_BLOCKSEARCH_OVERLAY' => (int)Configuration::get('YBC_BLOCKSEARCH_OVERLAY'),
		        'minChars' => (int)Configuration::get('YBC_BLOCKSEARCH_MINCHART') > 0 ? Configuration::get('YBC_BLOCKSEARCH_MINCHART') : 3,
                'search_query'=> Tools::getValue('search_query'),
                'main_color' => '#2fb5d2',
                'text_color' => '#ffffff',
                'button_color' =>Configuration::get('YBC_BLOCKSEARCH_BUTTON_COLOR') ? Configuration::get('YBC_BLOCKSEARCH_BUTTON_COLOR') : '#2fb5d2',
                'button_hover' =>Configuration::get('YBC_BLOCKSEARCH_HOVER_COLOR') ? Configuration::get('YBC_BLOCKSEARCH_HOVER_COLOR') : '#ffffff'
            )
        );

		return $this->display(__FILE__,'front-header.tpl');
	}

    public function hookTop($params)
	{
		$this->calculHookCommon($params);
		$this->smarty->assign(array(
                'blocksearch_type' => 'top',
                'id_lang' => $this->context->language->id,
                'search_query' => (string)Tools::getValue('search_query'),
                'warring_min_chart' => $this->l('Please enter at least').' '.Configuration::get('YBC_BLOCKSEARCH_MINCHART').' '.$this->l('characters for search')
			)
		);

        $this->smarty->assign(array(
            'YBC_BLOCKSEARCH_PLACEHOLDER' => Configuration::get('YBC_BLOCKSEARCH_PLACEHOLDER',$this->context->language->id),
        ));

		return $this->display(__FILE__, 'blocksearch-top.tpl');
	}

	public function hookDisplayNav($params)
	{
		return $this->hookTop($params);
	}
	private function calculHookCommon()
	{
		$this->smarty->assign(array(
			'ENT_QUOTES' =>		ENT_QUOTES,
			'search_ssl' =>		Tools::usingSecureMode(),
			'ajaxsearch' =>		Configuration::get('PS_SEARCH_AJAX'),
			'instantsearch' =>	Configuration::get('PS_INSTANT_SEARCH'),
			'self' =>			dirname(__FILE__),
            'page_search' =>$this->context->link->getModuleLink('ybc_blocksearch','blocksearch',array(),Tools::usingSecureMode()),
		));

		return true;
	}
    public function getContent()
	{
        $this->_postConfig();
        $this->_html .= $this->displayAdinJs();
        //Display errors if have
        if($this->errorMessage)
            $this->_html .= $this->errorMessage;
        //Render views
        $this->renderConfig();
        return $this->_html.$this->displayOtherModules();
    }
    public function displayOtherModules()
    {
        $intro = true;
        $localIps = array(
            '127.0.0.1',
            '::1'
        );
		$baseURL = Tools::strtolower(self::getBaseModLink());
		if(!Tools::isSubmit('intro') && (in_array(Tools::getRemoteAddr(), $localIps) || preg_match('/^.*(localhost|demo|test|dev|:\d+).*$/i', $baseURL)))
		    $intro = false;
        $this->smarty->assign(array(
			'other_modules_link' => isset($this->refs) ? $this->refs.$this->context->language->iso_code : $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name.'&othermodules=1',
            'intro' => $intro,
            'refsLink' => isset($this->refs) ? $this->refs.$this->context->language->iso_code : false,
		));
        return $this->display(__FILE__, 'other-modules.tpl');
    }
    public function displayAdinJs()
    {
        //die(Configuration::get('YBC_BLOCKSEARCH_CURENT_ACTIVE') ? Configuration::get('YBC_BLOCKSEARCH_CURENT_ACTIVE') : Tools::getValue('YBC_BLOCKSEARCH_CURENT_ACTIVE','general'));
        $this->smarty->assign(array(
            'clearCacheTxt' => $this->l('Empty categories tree cache'), 
            'clearCacheAlertTxt' => $this->l('Cache has been successfully deleted'),
            'clearCacheLink' => $this->getAdminLink().'&deletesearchcache=1',//$this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&deletesearchcache=1',
            'admin_js_search_url' => $this->_path . 'views/js/search-admin.js',
            'admin_js_other' => $this->_path . 'views/js/other.js',
            'admin_css_search_url' => $this->_path . 'views/css/search-admin.css',
            'admin_css_other' => $this->_path . 'views/css/other.css',
            'current_tab_active' => Configuration::get('YBC_BLOCKSEARCH_CURENT_ACTIVE') ? Configuration::get('YBC_BLOCKSEARCH_CURENT_ACTIVE') : Tools::getValue('YBC_BLOCKSEARCH_CURENT_ACTIVE','general'),
            'url_ajax_admin' => $this->getAdminLink()
        ));
        return $this->display(__FILE__,'admin.tpl');
    }
    public function renderConfig()
    {
        $configs = $this->configs;
        $fields_form = array(
			'form' => array(
				'input' => array(),
                'submit' => array(
					'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right ets_search_config'
				)
            ),
		);
        if($configs)
        {
            foreach($configs as $key => $config)
            {
                $confFields = array(
                    'name' => $key,
                    'type' => $config['type'],
                    'label' => $config['label'],
                    'desc' => isset($config['desc']) ? $config['desc'] : false,
                    'required' => isset($config['required']) && $config['required'] ? true : false,
                    'options' => isset($config['options']) && $config['options'] ? $config['options'] : array(),
                    'show' => isset($config['show']) && $config['show'] ? $config['show'] : false,
                    'index_miss' => isset($config['index_miss']) && $config['index_miss'] ? $config['index_miss'] : false,
                    'suggestion' => isset($config['suggestion']) ? $this->adminSuggestion() : false,
                    'values' => isset($config['values']) ? $config['values'] : ($config['type'] == 'switch' ? array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ) : false),
                    'class' => isset($config['class']) ? $config['class'] : false,
                    'form_group_class' => isset($config['form_group_class']) ? $config['form_group_class'] : false,
                    'lang' => isset($config['lang']) ? $config['lang'] : false
                );
                if($config['type'] == 'file')
                {
                    if($imageName = Configuration::get($key))
                    {
                        $confFields['display_img'] = $this->_path.'images/config/'.$imageName;
                        if(!isset($config['required']) || (isset($config['required']) && !$config['required']))
                            $confFields['img_del_link'] = $this->baseAdminPath.'&delimage=yes&image='.$key; 
                    }
                }
                $fields_form['form']['input'][] = $confFields;
            }
        }        
        $helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'saveConfig';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&control=config';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $fields = array();        
        $languages = Language::getLanguages(false);
        $helper->override_folder = '/';
        if(Tools::isSubmit('saveConfig'))
        {            
            if($configs)
            {                
                foreach($configs as $key => $config)
                {
                    if(isset($config['lang']) && $config['lang'])
                        {                        
                            foreach($languages as $l)
                            {
                                $fields[$key][$l['id_lang']] = Tools::getValue($key.'_'.$l['id_lang'],isset($config['default']) ? $config['default'] : '');
                            }
                        }
                        else
                            $fields[$key] = Tools::getValue($key,isset($config['default']) ? $config['default'] : '');
                }
            }
        }
        else
        {
            if($configs)
            {
                    foreach($configs as $key => $config)
                    {
                        if(isset($config['lang']) && $config['lang'])
                        {                    
                            foreach($languages as $l)
                            {
                                $fields[$key][$l['id_lang']] = Configuration::get($key,$l['id_lang']);
                            }
                        }
                        else
                            $fields[$key] = Configuration::get($key);                   
                    }
            }
        }
        $helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $fields,
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,                      
        );
        
        $this->_html .= $helper->generateForm(array($fields_form));		
    }
    private function _postConfig()
    {
        if (Tools::isSubmit('in_tabkeyword') && Tools::getValue('in_tabkeyword')){
            $this->updateConfigCurrent();
        }
        if ( Tools::isSubmit('index_product') && Tools::getValue('index_product')){
            $errors = array();
            ini_set('max_execution_time', 7200);
            Search::indexation(Tools::getValue('full'));
            if(Tools::getValue('ajax')){
                die(
                    Tools::jsonEncode(
                        array(
                            'messageType' => !count($errors) ? 'success' : 'error',
                            'message' => count($errors) ? $this->displayError($errors) : $this->displayConfirmation($this->l('Added missing products to the index successfully. Indexed products: ').$this->total_product.'/'.$this->total_product)
                        )
                    )
                );
            }
        }

        if(Tools::isSubmit('deletesearchcache'))
        {
            if(@file_exists(dirname(__FILE__).'/cache/options.html'))
                @unlink(dirname(__FILE__).'/cache/options.html');
            die(Tools::jsonEncode(array('deleted' => 1)));
        }
        $errors = array();
        $languages = Language::getLanguages(false);
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $configs = $this->configs;

        //Delete image
        if(Tools::isSubmit('delimage'))
        {
            $image = Tools::getValue('image');
            if(isset($configs[$image]) && !isset($configs[$image]['required']) || (isset($configs[$image]['required']) && !$configs[$image]['required']))
            {
                $imageName = Configuration::get($image);
                $imagePath = dirname(__FILE__).'/images/config/'.$imageName;
                if($imageName && file_exists($imagePath))
                {
                    @unlink($imagePath);
                    Configuration::updateValue($image,'');
                }
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
            }
            else
                $errors[] = $configs[$image]['label'].$this->l(' is required');
        }

        // Add keyword and Update
        if ( Tools::isSubmit('add_keyword')){
            $this->updateConfigCurrent();
            $id_suggestion = (int)Tools::getValue('YBC_BLOCKSEARCH_ID_KEYWORD');
            $search_suggestion = new Ybc_search_suggestion($id_suggestion);
            $languages = Language::getLanguages(false);
            if ( ! (bool)trim(Tools::getValue('YBC_BLOCKSEARCH_TYPE_KEYWORD_'.Configuration::get('PS_LANG_DEFAULT'))) ){
                $errors[] = $this->l('Cannot leave popular search keyword blank in the default language.');
            }
            if (count($errors))
            {
                $this->errorMessage = $this->displayError($errors);
            }else{
                $search_suggestion->id_shop = $this->context->shop->id;
                foreach ($languages as $language) {
                    $search_suggestion->keyword[$language['id_lang']] = trim(Tools::getValue('YBC_BLOCKSEARCH_TYPE_KEYWORD_'.$language['id_lang'])) != '' ? trim(Tools::getValue('YBC_BLOCKSEARCH_TYPE_KEYWORD_'.$language['id_lang'])) :  trim(Tools::getValue('YBC_BLOCKSEARCH_TYPE_KEYWORD_'.Configuration::get('PS_LANG_DEFAULT')));
                    $search_suggestion->searches[$language['id_lang']] = trim(Tools::getValue('searches_'.$language['id_lang'])) != '' ? trim(Tools::getValue('searches_'.$language['id_lang'])) :  '0';
                }
                if (!$search_suggestion->save()) {
                    $errors[] = $this->l('Failed to save. There was an unknown error happened');
                }
            }

            die(Tools::jsonEncode(
                array(
                    'messageType' => $errors ? 'error' : 'success',
                    'action_add' => $id_suggestion ? false : true,
                    'edit_text' => $this->l('Edit'),
                    'delete_text' => $this->l('Delete'),
                    'id_suggestion' => $errors ? false : ( $id_suggestion ? $id_suggestion : $this->getMaxId('suggestion','id_suggestion')),
                    'keyword' => Tools::getValue('YBC_BLOCKSEARCH_TYPE_KEYWORD_'.Configuration::get('PS_LANG_DEFAULT')),
                    'message' => $errors ? $this->displayError($errors) : $this->displayConfirmation($this->l('Updated successfully')),
                )
            ));
        }
        // Edit keyword
        if (Tools::isSubmit('edit_suggestion') && Tools::getValue('edit_suggestion')){
            $this->updateConfigCurrent();
            $id_suggestion = Tools::getValue('id_suggestion');
            $data = Ybc_search_suggestion::getSuggestionById($id_suggestion);
            if ( Tools::isSubmit('ajax')){
                die(Tools::jsonEncode(
                    array(
                        'hasData' => $data ? true : false,
                        'data' =>$data,
                        'id_suggestion' => $id_suggestion
                    )
                ));
            }
        }

        // Delete Keyword
        if (Tools::isSubmit('delete_suggestion') && Tools::getValue('delete_suggestion')){
            $this->updateConfigCurrent();
            $id_suggestion = Tools::getValue('id_suggestion');
            $search_suggestion = new Ybc_search_suggestion($id_suggestion);
            $res = $search_suggestion->delete();
            if ( Tools::isSubmit('ajax')){
                die(Tools::jsonEncode(
                    array(
                        'messageType' => ! $res ? 'error' : 'success',
                        'message' => ! $res ? $this->displayError($res) : $this->displayConfirmation($this->l('Updated successfully')),
                    )
                ));
            }
        }

        if (Tools::getValue('submitFilterybc_search_suggestion')){
            $this->updateConfigCurrent();
        }

        if(Tools::isSubmit('saveConfig') && (! Tools::getValue('submitFilterybc_search_suggestion') && ! Tools::isSubmit('submitResetybc_search_suggestion')) )
        {
            if($configs)
            {
                foreach($configs as $key => $config)
                {
                    if(isset($config['lang']) && $config['lang'])
                    {
                        if(isset($config['required']) && $config['required'] && $config['type']!='switch' && trim(Tools::getValue($key.'_'.$id_lang_default) == ''))
                        {
                            $errors[] = $config['label'].' '.$this->l('is required');
                        }
                    }
                    else
                    {
                        if(isset($config['required']) && $config['required'] && isset($config['type']) && $config['type']=='file')
                        {
                            if(Configuration::get($key)=='' && !isset($_FILES[$key]['size']))
                                $errors[] = $config['label'].' '.$this->l('is required');
                            elseif(isset($_FILES[$key]['size']))
                            {
                                $fileSize = round((int)$_FILES[$key]['size'] / (1024 * 1024));
                                if($fileSize > 100)
                                    $errors[] = $config['label'].$this->l(' cannot be larger than 100Mb');
                            }
                        }
                        else
                        {
                            if(isset($config['required']) && $config['required'] && $config['type']!='switch' && trim(Tools::getValue($key) == ''))
                            {
                                $errors[] = $config['label'].' '.$this->l('is required');
                            }
                            elseif(Tools::getValue($key) && isset($config['validate']) && method_exists('Validate',$config['validate']))
                            {
                                $validate = $config['validate'];
                                if(!Validate::$validate(trim(Tools::getValue($key))))
                                    $errors[] = $config['label'].' '.$this->l('is invalid');
                                unset($validate);
                            }
                            elseif(!is_array(Tools::getValue($key)) && !Validate::isCleanHtml(trim(Tools::getValue($key))))
                            {
                                $errors[] = $config['label'].' '.$this->l('is invalid');
                            }
                        }
                    }
                }
            }

            //Custom validation

            if (Tools::getValue('YBC_BLOCKSEARCH_MINCHART') !='' && (int)Tools::getValue('YBC_BLOCKSEARCH_MINCHART') <3){
                $errors[] = $this->l('"Minimum number of input characters" must be greater than 3');
            }
            if(!$errors)
            {
                if($configs)
                {
                    foreach($configs as $key => $config)
                    {
                        if ( $key == 'YBC_BLOCKSEARCH_TYPE_KEYWORD' ){
                            continue;
                        }
                        if(isset($config['lang']) && $config['lang'])
                        {
                            $valules = array();
                            foreach($languages as $lang)
                            {
                                if($config['type']=='switch')
                                    $valules[$lang['id_lang']] = (int)trim(Tools::getValue($key.'_'.$lang['id_lang'])) ? 1 : 0;
                                else
                                    $valules[$lang['id_lang']] = trim(Tools::getValue($key.'_'.$lang['id_lang'])) ? trim(Tools::getValue($key.'_'.$lang['id_lang'])) : trim(Tools::getValue($key.'_'.$id_lang_default));
                            }
                            Configuration::updateValue($key,$valules);
                        }
                        else
                        {
                            if($config['type']=='switch')
                            {
                                Configuration::updateValue($key,(int)trim(Tools::getValue($key)) ? 1 : 0);
                            }
                            if($config['type']=='file')
                            {
                                //Upload file
                                if(isset($_FILES[$key]['tmp_name']) && isset($_FILES[$key]['name']) && $_FILES[$key]['name'])
                                {
                                    $salt = sha1(microtime());
                                    $type = Tools::strtolower(Tools::substr(strrchr($_FILES[$key]['name'], '.'), 1));
                                    $imageName = $salt.'.'.$type;
                                    $fileName = dirname(__FILE__).'/images/config/'.$imageName;
                                    if(file_exists($fileName))
                                    {
                                        $errors[] = $config['label'].$this->l(' already exists. Try to rename the file then reupload');
                                    }
                                    else
                                    {

                                        $imagesize = @getimagesize($_FILES[$key]['tmp_name']);

                                        if (!$errors && isset($_FILES[$key]) &&
                                            !empty($_FILES[$key]['tmp_name']) &&
                                            !empty($imagesize) &&
                                            in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
                                        )
                                        {
                                            $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                                            if ($error = ImageManager::validateUpload($_FILES[$key]))
                                                $errors[] = $error;
                                            elseif (!$temp_name || !move_uploaded_file($_FILES[$key]['tmp_name'], $temp_name))
                                                $errors[] = $this->l('Can not upload the file');
                                            elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))
                                                $errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
                                            if (isset($temp_name))
                                                @unlink($temp_name);
                                            if(!$errors)
                                            {
                                                if(Configuration::get($key)!='')
                                                {
                                                    $oldImage = dirname(__FILE__).'/images/config/'.Configuration::get($key);
                                                    if(file_exists($oldImage))
                                                        @unlink($oldImage);
                                                }
                                                Configuration::updateValue($key, $imageName);
                                            }
                                        }
                                    }
                                }
                                //End upload file
                            }
                            else
                                Configuration::updateValue($key,trim(Tools::getValue($key)));
                        }
                    }
                }
            }

            if(Tools::getValue('ajax')){
                die(
                    Tools::jsonEncode(
                        array(
                            'messageType' => !count($errors) ? 'success' : 'error',
                            'message' => count($errors) ? $this->displayError($errors) : $this->displayConfirmation($this->l('Updated successfully'))
                        )
                    )
                );
            }
            if (count($errors))
            {
               $this->errorMessage = $this->displayError($errors);
            }
            else
            {
                if(@file_exists(dirname(__FILE__).'/cache/options.html'))
                    @unlink(dirname(__FILE__).'/cache/options.html');
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=4&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
            }
        }
    }

   // Quang update
    public function getMaxId($tbl, $primaryKey)
    {
        $req = 'SELECT max(`'.pSQL($primaryKey).'`) as maxid
			FROM `'._DB_PREFIX_.'ybc_search_'.pSQL($tbl).'` tbl';
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);
        return isset($row['maxid']) ? (int)$row['maxid'] : 0;
    }
    public function adminSuggestion(){
        $this->getFieldList();
        $content = $this->renderList(array(
            'fields_list' => $this->fields_list,
            'title' => $this->l('Suggestion'),
            'actions' => array('edit', 'delete'),
            'orderBy' => 'sg.id_suggestion',
            'orderWay' => 'DESC',
            'model' => '',
            'no_link' => true,
            'bulk_actions' => false,
            'list_id' => 'ybc_search_suggestion',
            'table' =>  'ybc_search_suggestion',
            'identifier' => 'id_suggestion'
        ));
        return $content;
    }

    public function renderList($params)
    {
        if (!$params)
            return $this->_html;
        $fields_list = $params['fields_list'];
        //$this->initToolbar();
        $helper = new HelperList();
        $helper->title = $params['title'];
        $helper->table = $params['table'];
        $helper->identifier = $params['identifier'];
        if (version_compare(_PS_VERSION_, '1.6.1.0', '>=')) {
            $helper->_pagination = array(10, 50, 100, 300, 1000);
            $helper->_default_pagination = 10;
        }
        $helper->_defaultOrderBy = isset($params['orderBy']) && $params['orderBy'] ? $params['orderBy'] : '';
        $helper->lang = true;
        $helper->explicitSelect = true;
        if (isset($params['orderBy']) && $params['orderBy'] == 'id_suggestion') {
            $helper->position_identifier = 'sg.id_suggestion';
        }

        if (Tools::isSubmit('submitResetybc_search_suggestion')) {
            $this->processResetFilters();
        }

        $this->processFilter($params);
        //Sort order
        $order_by = urldecode(Tools::getValue($this->list_id . 'Orderby'));
        if (!$order_by) {
            if ($this->context->cookie->{$this->list_id . 'Orderby'}) {
                $order_by = $this->context->cookie->{$this->list_id . 'Orderby'};
            } elseif ($helper->orderBy) {
                $order_by = $helper->orderBy;
            } else {
                $order_by = $helper->_defaultOrderBy;
            }
        }
        if ( $order_by == 'id_suggestion'){
            $order_by = 'sg.id_suggestion';
        }
        $order_way = urldecode(Tools::getValue($this->list_id . 'Orderway'));
        if (!$order_way) {
            if ($this->context->cookie->{$this->list_id . 'Orderway'}) {
                $order_way = $this->context->cookie->{$this->list_id . 'Orderway'};
            } elseif ($helper->orderWay) {
                $order_way = $helper->orderWay;
            } else {
                $order_way = $params['orderWay'];
            }
        }
        if (isset($this->fields_list[$order_by]) && isset($this->fields_list[$order_by]['filter_key']))
            $order_by = $this->fields_list[$order_by]['filter_key'];
        //Pagination.
        $limit = Tools::getValue($helper->table . '_pagination');
        if (!$limit) {
            if (isset($this->context->cookie->{$this->list_id . '_pagination'}) && $this->context->cookie->{$this->list_id . '_pagination'})
                $limit = $this->context->cookie->{$this->list_id . '_pagination'};
            else
                $limit = (version_compare(_PS_VERSION_, '1.6.1.0', '>=') ? $helper->_default_pagination : 20);
        }
        if ($limit) {
            $this->context->cookie->{$this->list_id . '_pagination'} = $limit;
        } else {
            unset($this->context->cookie->{$this->list_id . '_pagination'});
        }
        $start = 0;

        if ((int)Tools::getValue('submitFilter' . $this->list_id)) {
            $start = ((int)Tools::getValue('submitFilter' . $this->list_id) - 1) * $limit;
        } elseif (empty($start) && isset($this->context->cookie->{$this->list_id . '_start'}) && Tools::isSubmit('export' . $helper->table)) {
            $start = $this->context->cookie->{$this->list_id . '_start'};
        }
        if ($start) {
            $this->context->cookie->{$this->list_id . '_start'} = $start;
        } elseif (isset($this->context->cookie->{$this->list_id . '_start'})) {
            unset($this->context->cookie->{$this->list_id . '_start'});
        }
        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way) || !is_numeric($start) || !is_numeric($limit)) {
            $this->errorMessage = Tools::displayError($this->l('get list params is not valid'));
        }

        $helper->orderBy = $order_by;
        if (preg_match('/[.!]/', $order_by)) {
            $order_by_split = preg_split('/[.!]/', $order_by);
            $order_by = bqSQL($order_by_split[0]) . '.`' . bqSQL($order_by_split[1]) . '`';
        } elseif ($order_by) {
            $order_by = '`' . bqSQL($order_by) . '`';
        }

        $helper->listTotal = $this->getSuggestion(array(
            'nb' => true,
            'filter' => $this->_filter,
            'having' => $this->_filterHaving,
        ));

        $list = $this->getSuggestion(array(
            'filter' => $this->_filter,
            'having' => $this->_filterHaving,
            'start' => $start,
            'limit' => $limit,
            'sort' => $order_by . ' ' . Tools::strtoupper($order_way),
        ));

        $helper->orderWay = Tools::strtoupper($order_way);
        $helper->toolbar_btn = array();
        $helper->shopLinkType = '';
        $helper->row_hover = true;
        $helper->no_link = $params['no_link'];
        $helper->simple_header = false;
        $helper->actions = $params['actions'];
        $helper->show_toolbar = true;
        $helper->module = $this;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $this->getAdminLink(array('token' => false)) . '&control=config&in_tabkeyword=true';
        $helper->bulk_actions = $params['bulk_actions'] ? array(
            'enableSelection' => array(
                'text' => $this->l('Enable selection'),
                'icon' => 'icon-power-off text-success'
            ),
            'disableSelection' => array(
                'text' => $this->l('Disable selection'),
                'icon' => 'icon-power-off text-danger'
            ),
            'divider' => array(
                'text' => 'divider'
            ),
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        ) : false;

        return $helper->generateList($list, $fields_list);
    }

    public function processFilter($params)
    {
        if (empty($params))
            return false;
        if (!isset($this->list_id)) {
            $this->list_id = 'ybc_search_suggestion';
        }
        $prefix = null;

        // Filter memorization
        if (!empty($_POST) && isset($this->list_id)) {
            foreach ($_POST as $key => $value) {
                if ($value === '') {
                    unset($this->context->cookie->{$prefix . $key});
                } elseif (stripos($key, $this->list_id . 'Filter_') === 0) {
                    $this->context->cookie->{$prefix . $key} = !is_array($value) ? $value : serialize($value);
                } elseif (stripos($key, 'submitFilter') === 0) {
                    $this->context->cookie->$key = !is_array($value) ? $value : serialize($value);
                }
            }
        }

        if (!empty($_GET) && isset($this->list_id)) {
            foreach ($_GET as $key => $value) {
                if (stripos($key, $this->list_id . 'Filter_') === 0) {
                    $this->context->cookie->{$prefix . $key} = !is_array($value) ? $value : serialize($value);
                } elseif (stripos($key, 'submitFilter') === 0) {
                    $this->context->cookie->$key = !is_array($value) ? $value : serialize($value);
                }
                if (stripos($key, $this->list_id . 'Orderby') === 0 && Validate::isOrderBy($value)) {
                    if ($value === '' || $value == $params['orderBy']) {
                        unset($this->context->cookie->{$prefix . $key});
                    } else {
                        $this->context->cookie->{$prefix . $key} = $value;
                    }
                } elseif (stripos($key, $this->list_id . 'Orderway') === 0 && Validate::isOrderWay($value)) {
                    if ($value === '' || $value == $params['orderWay']) {
                        unset($this->context->cookie->{$prefix . $key});
                    } else {
                        $this->context->cookie->{$prefix . $key} = $value;
                    }
                }
            }
        }

        $filters = $this->context->cookie->getFamily($prefix . $this->list_id . 'Filter_');

        foreach ($filters as $key => $value) {
            /* Extracting filters from $_POST on key filter_ */
            if ($value != null && !strncmp($key, $prefix . $this->list_id . 'Filter_', 7 + Tools::strlen($prefix . $this->list_id))) {
                $key = Tools::substr($key, 7 + Tools::strlen($prefix . $this->list_id));
                /* Table alias could be specified using a ! eg. alias!field */
                $tmp_tab = explode('!', $key);
                $filter = count($tmp_tab) > 1 ? $tmp_tab[1] : $tmp_tab[0];

                if ($field = $this->filterToField($key, $filter)) {
                    $type = (array_key_exists('filter_type', $field) ? $field['filter_type'] : (array_key_exists('type', $field) ? $field['type'] : false));
                    if (($type == 'date' || $type == 'datetime') && is_string($value))
                        $value = Tools::unSerialize($value);
                    $key = isset($tmp_tab[1]) ? $tmp_tab[0] . '.`' . $tmp_tab[1] . '`' : '`' . $tmp_tab[0] . '`';
                    $sql_filter = '';
                    /* Only for date filtering (from, to) */
                    if (is_array($value)) {
                        if (isset($value[0]) && !empty($value[0])) {
                            if (!Validate::isDate($value[0])) {
                                $this->errors[] = Tools::displayError('The \'From\' date format is invalid (YYYY-MM-DD)');
                            } else {
                                $sql_filter .= ' AND ' . pSQL($key) . ' >= \'' . pSQL(Tools::dateFrom($value[0])) . '\'';
                            }
                        }

                        if (isset($value[1]) && !empty($value[1])) {
                            if (!Validate::isDate($value[1])) {
                                $this->errors[] = Tools::displayError('The \'To\' date format is invalid (YYYY-MM-DD)');
                            } else {
                                $sql_filter .= ' AND ' . pSQL($key) . ' <= \'' . pSQL(Tools::dateTo($value[1])) . '\'';
                            }
                        }
                    } else {
                        $sql_filter .= ' AND ';
                        $check_key = ($key == 'id_suggestion' || $key == '`id_suggestion`');
                        $alias = 'sg';
                        if ($type == 'int' || $type == 'bool') {
                            $sql_filter .= (($check_key || $key == '`active`') ? $alias . '.' : '') . pSQL($key) . ' = ' . (int)$value . ' ';
                        } elseif ($type == 'decimal') {
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' = ' . (float)$value . ' ';
                        } elseif ($type == 'select') {
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' = \'' . pSQL($value) . '\' ';
                        } elseif ($type == 'price') {
                            $value = (float)str_replace(',', '.', $value);
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' = ' . pSQL(trim($value)) . ' ';
                        } else {
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' LIKE \'%' . pSQL(trim($value)) . '%\' ';
                        }
                    }
                    if (isset($field['havingFilter']) && $field['havingFilter'])
                        $this->_filterHaving .= $sql_filter;
                    else
                        $this->_filter .= $sql_filter;
                }
            }
        }
    }

    public function getSuggestion($params){
        $shop_id = $this->context->shop->id;
        $id_lang = $this->context->language->id;

        $all_shop = (Shop::getContext() == Shop::CONTEXT_ALL);
        $rq_sql = 'SELECT sg.*, sgl.* FROM `'._DB_PREFIX_.'ybc_search_suggestion` sg 
                    LEFT JOIN `'._DB_PREFIX_.'ybc_search_suggestion_lang` sgl ON sg.`id_suggestion` = sgl.`id_suggestion` 
                    WHERE 1 '.( $all_shop ? '' : ' AND sg.`id_shop` = '.($shop_id).' ').' AND sgl.`id_lang` = '.(int)$id_lang. (isset($params['filter']) && $params['filter'] ? $params['filter'] : '') .'    
                    GROUP BY sg.`id_suggestion` ' ;
        if (isset($params['nb']) && $params['nb'])
            return ($nb = Db::getInstance()->executeS($rq_sql)) ? count($nb) : 0;
        $rq_sql .= ' ORDER BY ' . (isset($params['sort']) && $params['sort'] ? $params['sort'] : 'sg.`id_suggestion`')
            . ((isset($params['start']) && $params['start'] !== false) && (isset($params['limit']) && $params['limit']) ? " LIMIT " . (int)$params['start'] . ", " . (int)$params['limit'] : '');

        $rq = Db::getInstance()->executeS($rq_sql);
        return $rq;
    }
    public function getAdminLink($args = array())
    {
        $uri = $this->context->link->getAdminLink('AdminModules', isset($args['token']) ? $args['token'] : true) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        if ($args) {
            $urls = array();
            foreach ($args as $key => $param) {
                if ($key != 'token') {
                    $urls[] = $key . '=' . $param;
                }
            }
            if ($urls) {
                $uri .= '&' . implode('&', $urls);
            }
        }
        return $uri;
    }
    public function checkModuleEnable($module_name){
	    if( !$module_name ) return false;
	    return Module::isInstalled($module_name) && Module::isEnabled($module_name);
    }

    public function array_insert (&$array, $position, $insert_array) {
        $first_array = array_splice ($array, 0, $position);
        $array = array_merge ($first_array, $insert_array, $array);
    }


    public function hookDisplayDataContent($params){
	    if( ! isset($params['id_list']) || !  $params['id_list']) {
	        return false;
        }
	    $id_list = $params['id_list'];
        $page = (isset($params['page']) && (int)$params['page'] >= 1) ? (int)$params['page'] : 1;
        $perpage = (isset($params['perpage']) &&$params['perpage']>0) ? $params['perpage'] :( (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_PAGE') > 0 ? (int)Configuration::get('YBC_BLOCKSEARCH_LIMIT_PAGE') : 12);
        $keyWord = isset($params['keyWord']) && $params['keyWord'] ? $params['keyWord'] : false;
        $searh_data = new Search_data();
        $searh_data->setKeyWords($keyWord);
        $searh_data->setPage($page);
        $searh_data->setPerPage($perpage);

        if ( (int)Tools::strlen($keyWord) < (int)Configuration::get('YBC_BLOCKSEARCH_MINCHART')){
            //$searh_data->setIdOnly(true);
        }
        $total_page = 0;
        if ( $id_list == 'product') {
            if (Configuration::get('YBC_BLOCKSEARCH_ALLOW_SORT') && isset($params['order']) && $params['order']){
                $sortBy = $params['order'];
            }
            else{
                $sortBy = $this->context->cookie->ybc_search_order && Configuration::get('YBC_BLOCKSEARCH_ALLOW_SORT') ? $this->context->cookie->ybc_search_order : Configuration::get('YBC_BLOCKSEARCH_RESULT_BY');
            }
            $data_sort = explode('_',$sortBy);
            if ($data_sort && is_array($data_sort)){
                if(isset($data_sort[0]))$searh_data->setOrderBy($data_sort[0]);
                if(isset($data_sort[1]))$searh_data->setOrderWay($data_sort[1]);
            }
            $products = $searh_data->getProduct();
            $total_page = $products['totalPage'];
            $total = $searh_data ->getProduct(true);
            $title = $this->l('products');
        }
        if ( $id_list == 'category'){
            $datas = $searh_data->getCategory();
            $total_page = $searh_data->getPages('getCategory');
            $total = $searh_data->getCategory(true);
            $title = $this->l('category');
        }
        if ( $id_list == 'catBlog'){
            $datas = $searh_data->getCategoryBlock();
            $total_page = $searh_data->getPages('getCategoryBlock');
            $total = $searh_data->getCategoryBlock(true);
            $title = $this->l('category block');
        }

        if ( $id_list == 'blockBlog' ){
            $datas = $searh_data->getBlockPostInBlock();
            $total_page = $searh_data->getPages('getBlockPostInBlock');
            $total = $searh_data->getBlockPostInBlock(true);
            $title = $this->l('block post');
        }

        if ( $id_list == 'cms' ){
            $datas = $searh_data->getCmsPage();
            $total_page = $searh_data->getPages('getCmsPage');
            $total = $searh_data->getCmsPage(true);
            $title = $this->l('CMS');
        }
        if ( $id_list == 'manufacturer'){
            $datas = $searh_data->getManufacture();
            $total_page = $searh_data->getPages('getManufacture');
            $total = $searh_data->getManufacture(true);
            $title = $this->l('brand');
        }

        $paggiration = $this->displayPaggination($id_list,$page,$perpage,$total_page,$keyWord);
        $this->smarty->assign(array(
            'datas' => $id_list == 'product' ? $products['products'] : $datas,
            'paggination' => $paggiration,
            'wrapper' => isset($params['wrapper']) ? $params['wrapper'] : true,
            'page' => $page,
            'id_list' =>$id_list,
            'sort_lists' => Configuration::get('YBC_BLOCKSEARCH_ALLOW_SORT') ? $this->configs['YBC_BLOCKSEARCH_RESULT_BY']['options']['query'] : false,
            'search_ajax_link' => $this->url_ajax,
            'sort_by' => isset($sortBy) ? $sortBy : false,
            'total' => $total ? $total : '',
            'start' => $page == '1' ? '1' : (($page-1)*$perpage),
            'end' => ($page*$perpage) >= $total ? $total : ($page*$perpage),
            'title' =>$title,
			'key_word' =>$keyWord
        ));

        if ( isset($params['ajax']) && $params['ajax'] ){
            return array(
                'html' =>  $id_list == 'product' ? $this->display(__FILE__, 'product-list' . ($this->is17 ? '-17' : '') . '.tpl') : $this->display(__FILE__,'list-item.tpl'),
                'hasData' => $id_list == 'product' ? ($products['products'] ? true : false) : (count($datas) ? true : false),
                'pagginations' => $paggiration,
                'id_list' =>$id_list
            );
        }else{
            if ( $id_list == 'product') {
                return $this->display(__FILE__, 'product-list' . ($this->is17 ? '-17' : '') . '.tpl');
            }else{
                return $this->display(__FILE__,'list-item.tpl');
            }

        }
        return false;
    }
    public function getFieldList(){
        $this->fields_list = array(
            'id_suggestion' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'class' => 'fixed-width-xs id_suggestion',
            ),
            'keyword' => array(
                'title' => $this->l('Keyword'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
                'align' => 'left',
            ),
            'searches' => array(
                'title' => $this->l('Searches'),
                'align' => 'center',
                'type' => 'text',
                'search' => false,
                'orderby' => true,
            ),
        );
    }
    public function processResetFilters($list_id = null)
    {
        $this->updateConfigCurrent();
        $fields_list = $this->getFieldList();
        if ($list_id === null) {
            $list_id = isset($this->list_id) ? $this->list_id : $this->name;
        }
        $prefix = null;
        $filters = $this->context->cookie->getFamily($prefix . $list_id . 'Filter_');
        if (!empty($filters))
            foreach ($filters as $cookie_key => $filter) {
                if (strncmp($cookie_key, $prefix . $list_id . 'Filter_', 7 + Tools::strlen($prefix . $list_id)) == 0) {
                    $key = Tools::substr($cookie_key, 7 + Tools::strlen($prefix . $list_id));
                    if (is_array($fields_list) && array_key_exists($key, $fields_list)) {
                        $this->context->cookie->$cookie_key = null;
                    }
                    unset($this->context->cookie->$cookie_key, $filter);
                }
            }

        if (isset($this->context->cookie->{'submitFilter' . $list_id})) {
            unset($this->context->cookie->{'submitFilter' . $list_id});
        }
        if (isset($this->context->cookie->{$prefix . $list_id . 'Orderby'})) {
            unset($this->context->cookie->{$prefix . $list_id . 'Orderby'});
        }
        if (isset($this->context->cookie->{$prefix . $list_id . 'Orderway'})) {
            unset($this->context->cookie->{$prefix . $list_id . 'Orderway'});
        }
        $_POST = array();
    }

    protected function filterToField($key, $filter)
    {
        if (!isset($this->fields_list) || !$this->fields_list){
            $this->fields_list = $this->getFieldList();
        }

        foreach ($this->fields_list as $field)
            if (array_key_exists('filter_key', $field) && $field['filter_key'] == $key)
                return $field;
        if (array_key_exists($filter, $this->fields_list))
            return $this->fields_list[$filter];
        return false;
    }
    public function displayPaggination($list_id = false,$page = 1,$perpage,$total_pages =1,$search_query=''){

	    $this->smarty->assign(
	        array(
	            'total_pages' => $total_pages,
                'page' =>$page,
                'perpage' => $perpage,
                'list_id' =>$list_id,
                'search_ajax_link' => $this->context->link->getModuleLink('ybc_blocksearch','blocksearch',array('id_list'=>$list_id,'search_query'=>$search_query),Tools::usingSecureMode()),
                'search_query' => $search_query,
            )
        );
	    return $this->display(__FILE__,'paggination.tpl');
    }
    public function displayRecommendedModules()
    {
        $cacheDir = dirname(__file__) . '/../../cache/'.$this->name.'/';
        $cacheFile = $cacheDir.'module-list.xml';
        $cacheLifeTime = 24;
        $cacheTime = (int)Configuration::getGlobalValue('ETS_MOD_CACHE_'.$this->name);
        $profileLinks = array(
            'en' => 'https://addons.prestashop.com/en/207_ets-soft',
            'fr' => 'https://addons.prestashop.com/fr/207_ets-soft',
            'it' => 'https://addons.prestashop.com/it/207_ets-soft',
            'es' => 'https://addons.prestashop.com/es/207_ets-soft',
        );
        if(!is_dir($cacheDir))
        {
            @mkdir($cacheDir, 0755,true);
            if ( @file_exists(dirname(__file__).'/index.php')){
                @copy(dirname(__file__).'/index.php', $cacheDir.'index.php');
            }
        }
        if(!file_exists($cacheFile) || !$cacheTime || time()-$cacheTime > $cacheLifeTime * 60 * 60)
        {
            if(file_exists($cacheFile))
                @unlink($cacheFile);
            if($xml = self::file_get_contents($this->shortlink.'ml.xml'))
            {
                $xmlData = @simplexml_load_string($xml);
                if($xmlData && (!isset($xmlData->enable_cache) || (int)$xmlData->enable_cache))
                {
                    @file_put_contents($cacheFile,$xml);
                    Configuration::updateGlobalValue('ETS_MOD_CACHE_'.$this->name,time());
                }
            }
        }
        else
            $xml = Tools::file_get_contents($cacheFile);
        $modules = array();
        $categories = array();
        $categories[] = array('id'=>0,'title' => $this->l('All categories'));
        $enabled = true;
        $iso = Tools::strtolower($this->context->language->iso_code);
        $moduleName = $this->displayName;
        $contactUrl = '';
        if($xml && ($xmlData = @simplexml_load_string($xml)))
        {
            if(isset($xmlData->modules->item) && $xmlData->modules->item)
            {
                foreach($xmlData->modules->item as $arg)
                {
                    if($arg)
                    {
                        if(isset($arg->module_id) && (string)$arg->module_id==$this->name && isset($arg->{'title'.($iso=='en' ? '' : '_'.$iso)}) && (string)$arg->{'title'.($iso=='en' ? '' : '_'.$iso)})
                            $moduleName = (string)$arg->{'title'.($iso=='en' ? '' : '_'.$iso)};
                        if(isset($arg->module_id) && (string)$arg->module_id==$this->name && isset($arg->contact_url) && (string)$arg->contact_url)
                            $contactUrl = $iso!='en' ? str_replace('/en/','/'.$iso.'/',(string)$arg->contact_url) : (string)$arg->contact_url;
                        $temp = array();
                        foreach($arg as $key=>$val)
                        {
                            if($key=='price' || $key=='download')
                                $temp[$key] = (int)$val;
                            elseif($key=='rating')
                            {
                                $rating = (float)$val;
                                if($rating > 0)
                                {
                                    $ratingInt = (int)$rating;
                                    $ratingDec = $rating-$ratingInt;
                                    $startClass = $ratingDec >= 0.5 ? ceil($rating) : ($ratingDec > 0 ? $ratingInt.'5' : $ratingInt);
                                    $temp['ratingClass'] = 'mod-start-'.$startClass;
                                }
                                else
                                    $temp['ratingClass'] = '';
                            }
                            elseif($key=='rating_count')
                                $temp[$key] = (int)$val;
                            else
                                $temp[$key] = (string)strip_tags($val);
                        }
                        if($iso)
                        {
                            if(isset($temp['link_'.$iso]) && isset($temp['link_'.$iso]))
                                $temp['link'] = $temp['link_'.$iso];
                            if(isset($temp['title_'.$iso]) && isset($temp['title_'.$iso]))
                                $temp['title'] = $temp['title_'.$iso];
                            if(isset($temp['desc_'.$iso]) && isset($temp['desc_'.$iso]))
                                $temp['desc'] = $temp['desc_'.$iso];
                        }
                        $modules[] = $temp;
                    }
                }
            }
            if(isset($xmlData->categories->item) && $xmlData->categories->item)
            {
                foreach($xmlData->categories->item as $arg)
                {
                    if($arg)
                    {
                        $temp = array();
                        foreach($arg as $key=>$val)
                        {
                            $temp[$key] = (string)strip_tags($val);
                        }
                        if(isset($temp['title_'.$iso]) && $temp['title_'.$iso])
                                $temp['title'] = $temp['title_'.$iso];
                        $categories[] = $temp;
                    }
                }
            }
        }
        if(isset($xmlData->{'intro_'.$iso}))
            $intro = $xmlData->{'intro_'.$iso};
        else
            $intro = isset($xmlData->intro_en) ? $xmlData->intro_en : false;
        $this->smarty->assign(array(
            'modules' => $modules,
            'enabled' => $enabled,
            'module_name' => $moduleName,
            'categories' => $categories,
            'img_dir' => $this->_path . 'views/img/',
            'intro' => $intro,
            'shortlink' => $this->shortlink,
            'ets_profile_url' => isset($profileLinks[$iso]) ? $profileLinks[$iso] : $profileLinks['en'],
            'trans' => array(
                'txt_must_have' => $this->l('Must-Have'),
                'txt_downloads' => $this->l('Downloads!'),
                'txt_view_all' => $this->l('View all our modules'),
                'txt_fav' => $this->l('Prestashop\'s favourite'),
                'txt_elected' => $this->l('Elected by merchants'),
                'txt_superhero' => $this->l('Superhero Seller'),
                'txt_partner' => $this->l('Module Partner Creator'),
                'txt_contact' => $this->l('Contact us'),
                'txt_close' => $this->l('Close'),
            ),
            'contactUrl' => $contactUrl,
         ));
         echo $this->display(__FILE__, 'module-list.tpl');
         die;
    }
    public static function file_get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 60)
    {
        if ($stream_context == null && preg_match('/^https?:\/\//', $url)) {
            $stream_context = stream_context_create(array(
                "http" => array(
                    "timeout" => $curl_timeout,
                    "max_redirects" => 101,
                    "header" => 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
                ),
                "ssl"=>array(
                    "allow_self_signed"=>true,
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ));
        }
        if (function_exists('curl_init')) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => html_entity_decode($url),
                CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => $curl_timeout,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_FOLLOWLOCATION => true,
            ));
            $content = curl_exec($curl);
            curl_close($curl);
            return $content;
        } elseif (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url)) {
            return Tools::file_get_contents($url, $use_include_path, $stream_context);
        } else {
            return false;
        }
    }
    public static function getBaseModLink()
    {
        $context = Context::getContext();
        return (Configuration::get('PS_SSL_ENABLED_EVERYWHERE')?'https://':'http://').$context->shop->domain.$context->shop->getBaseURI();
    }
    public function getBreadCrumb()
    {
        $title_page = Configuration::get('YBC_BLOCKSEARCH_TITLE_PAGE', $this->context->language->id);
        $nodes = array();
        $nodes[] = array(
            'title' => $this->l('Home'),
            'url' => $this->context->link->getPageLink('index', true),
        );
        $nodes[] = array(
            'title' => $title_page ? (Tools::getValue('search_query') ? $title_page.' "'.Tools::getValue('search_query').'"' : $title_page.' ""') : (Tools::getValue('search_query') ? $this->l('Search result for').' "'.Tools::getValue('search_query').'"' : $this->l('Search result')),
            'url' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"
        );
        if ($this->is17)
            return array('links' => $nodes, 'count' => count($nodes));
        return $this->displayBreadcrumb($nodes);
    }
    public function displayBreadcrumb($nodes)
    {
        $this->smarty->assign('nodes', $nodes);
        return $this->display(__FILE__, 'breadcrumb.tpl');
    }

    public function hookDisplayReviews($params){
        if (!$params['id_product']){
            return '';
        }

        $sql = 'SELECT SUM(pc.grade) / COUNT(pc.grade) AS averageGrade 
                FROM `'._DB_PREFIX_.'product_comment` pc 
                WHERE 1 AND pc.id_product = '.(int)$params['id_product'].' 
                        AND pc.deleted = 0 
                        '.(Configuration::get('PRODUCT_COMMENTS_MODERATE') ? ' AND pc.validate=1' : '').' ';

        $averageGrade =  (float)DB::getInstance()->getValue($sql);
        $this->context->smarty->assign(
            array(
                'averageGrade' =>$averageGrade
            )
        );
        return $this->display(__FILE__,'rating.tpl');
    }

    public function updateConfigCurrent(){
        Configuration::updateValue('YBC_BLOCKSEARCH_CURENT_ACTIVE','keyword');
    }
}
