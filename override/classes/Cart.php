<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class Cart extends CartCore
{

	public function getProducts($refresh = false, $id_product = false, $id_country = null)
	{
		$this->products = parent::getProducts($refresh, $id_product, $id_country);

		if (Module::isEnabled('ProductBundlesPro'))
		{
			include_once(_PS_MODULE_DIR_.'/productbundlespro/lib/bootstrap.php');
			$pbp_cart_controller = new PBPCartController();
			$this->products = $pbp_cart_controller->addDiscountFlagToCartProducts($this->products);
		}
		return $this->products;
	}

}