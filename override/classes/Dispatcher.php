<?php
/**
 *  2016 ModuleFactory.co
 *
 *  @author    ModuleFactory.co <info@modulefactory.co>
 *  @copyright 2016 ModuleFactory.co
 *  @license   ModuleFactory.co Commercial License
 */
class Dispatcher extends DispatcherCore
{
    /*
    * module: fsadvancedurl
    * date: 2020-03-31 11:37:37
    * version: 1.2.1
    */
    public function getRoutes()
    {
        return $this->routes;
    }
    /*
    * module: fsadvancedurl
    * date: 2020-03-31 11:37:37
    * version: 1.2.1
    */
    public function getRequestUri()
    {
        return $this->request_uri;
    }
    /*
    * module: fsadvancedurl
    * date: 2020-03-31 11:37:37
    * version: 1.2.1
    */
    protected function loadRoutes($id_shop = null)
    {
        parent::loadRoutes($id_shop);
        
        if (Module::isEnabled('fsadvancedurl')) {
            $fsadvancedurl = Module::getInstanceByName('fsadvancedurl');
            $this->routes = $fsadvancedurl->dispatcherLoadRoutes($this->routes);
        }
    }
}
