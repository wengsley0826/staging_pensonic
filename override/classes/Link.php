<?php
/**
 *  2016 ModuleFactory.co
 *
 *  @author    ModuleFactory.co <info@modulefactory.co>
 *  @copyright 2016 ModuleFactory.co
 *  @license   ModuleFactory.co Commercial License
 */
class Link extends LinkCore
{
    /*
    * module: fsadvancedurl
    * date: 2020-03-31 11:23:33
    * version: 1.2.1
    */
    public function getCategoryLink(
        $category,
        $alias = null,
        $id_lang = null,
        $selected_filters = null,
        $id_shop = null,
        $relative_protocol = false
    ) {
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        if (version_compare(_PS_VERSION_, '1.5.6.0', '>=')) {
            $url = $this->getBaseLink($id_shop, null, $relative_protocol).$this->getLangLink($id_lang, null, $id_shop);
        } elseif (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
            if ($id_shop === null) {
                $shop = Context::getContext()->shop;
            } else {
                $shop = new Shop($id_shop);
            }
            $url = 'http://'.$shop->domain.$shop->getBaseURI().$this->getLangLink($id_lang, null, $id_shop);
        } else {
            $url = _PS_BASE_URL_.__PS_BASE_URI__.$this->getLangLink($id_lang);
        }
        if (!is_object($category)) {
            $category = new Category($category, $id_lang);
        }
        $params = array();
        $params['id'] = $category->id;
        $params['rewrite'] = (!$alias) ? $category->link_rewrite : $alias;
        $params['meta_keywords'] = Tools::str2url($category->getFieldByLang('meta_keywords'));
        $params['meta_title'] = Tools::str2url($category->getFieldByLang('meta_title'));
        $category_disable_rewrite = array(1, 2);
        if (version_compare(_PS_VERSION_, '1.5.4.0', '>=')) {
            $category_disable_rewrite = Link::$category_disable_rewrite;
        }
        $dispatcher = Dispatcher::getInstance();
        if ($dispatcher->hasKeyword('category_rule', $id_lang, 'categories', $id_shop)) {
            $cats = array();
            foreach ($category->getParentsCategories() as $cat) {
                if (!in_array($cat['id_category'], $category_disable_rewrite)) {
                    $cats[] = $cat['link_rewrite'];
                }
            }
            $cats = array_reverse($cats);
            array_pop($cats);
            $params['categories'] = implode('/', $cats);
        }
        $selected_filters = is_null($selected_filters) ? '' : $selected_filters;
        if (empty($selected_filters)) {
            $rule = 'category_rule';
        } else {
            $rule = 'layered_rule';
            $params['selected_filters'] = $selected_filters;
        }
        return $url.Dispatcher::getInstance()->createUrl($rule, $id_lang, $params, $this->allow, '', $id_shop);
    }
}
