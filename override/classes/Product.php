<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

include_once(_PS_MODULE_DIR_.'/productbundlespro/lib/bootstrap.php');

class Product extends ProductCore
{
	public static function priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency, $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, &$specific_price, $use_group_reduction, $id_customer = 0, $use_customer_price = true, $id_cart = 0, $real_quantity = 0)
	{
		$price = parent::priceCalculation(
			$id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
			$id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
			$id_customer, $use_customer_price, $id_cart, $real_quantity
		);

		// @Todo: Check if module is enabled, and condition include
		if (Module::isEnabled('ProductBundlesPro'))
			include_once(_PS_MODULE_DIR_.'/productbundlespro/lib/bootstrap.php');
		else
			return $price;

			if ($id_cart)
		{
            $price = Hook::exec('pbpPriceCalculation', array(
                'price' => $price,
                'id_product' => $id_product,
                'id_product_attribute' => $id_product_attribute,
                'quantity' => $quantity,
                'id_cart' => $id_cart,
                'id_shop' => $id_shop,
                'id_country' => $id_country,
                'id_state' => $id_state,
                'zipcode' => $zipcode,
                'use_tax' => $use_tax				
            ));
			return (float)$price;
		}
		else return $price;
	}
}