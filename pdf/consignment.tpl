<style>
	table, th, td {
		margin: 0!important;
		padding: 0!important;
		vertical-align: middle;
		font-size: 8pt;
		line-height: 11pt;
		white-space: nowrap;
	}
</style>

<table width="100%" id="body" border="0" cellpadding="0" cellspacing="0" style="margin:0;font-size:10pt;">
  {assign var=counter value=0}
  {assign var=counter2 value=0}
  {foreach $addresses as $addr}
  {if $counter%3 eq 0 && $counter gt 2}
      {assign var=counter value=0}
    {/if}
  {if $counter2 == 0}
    {assign var=linecnt value=14}
    {for $foo=1 to $linecnt}
    <tr>
      <td></td>
    </tr>
    {/for}
    {assign var=counter2 value=2}
  {/if}
    <tr>
			<td>Order #{$addr['id_order']}</td>
		</tr>
		<tr>
			<td>{strtoupper($addr['addr']->lastname)} {strtoupper($addr['addr']->firstname)}</td>
		</tr>
    {if $addr['addr']->company !=""}
    <tr>
      <td>{strtoupper($addr['addr']->company)}</td>
    </tr>
    {/if}
    <tr>
			<td>{strtoupper($addr['addr']->address1)}</td>
		</tr>
    {if $addr['addr']->address2 !=""}
		<tr>
			<td>{strtoupper($addr['addr']->address2)}</td>
		</tr>
    {/if}
		<tr>
			<td>{$addr['addr']->postcode} {strtoupper($addr['addr']->city)}, {strtoupper($addr['state']->name)}, {strtoupper($addr['addr']->country)}</td>
		</tr>
		<tr>
			<td>{if $addr['addr']->phone}{l s='Tel: ' pdf='true'}{$addr['addr']->phone}{/if}{if $addr['addr']->phone && $addr['addr']->phone_mobile}, {/if}{if $addr['addr']->phone_mobile}{l s='Mobile: ' pdf='true'}{$addr['addr']->phone_mobile}{/if}</td>
		</tr>
    {if $addr['addr']->address2==""}
    <tr>
			<td>&nbsp;</td>
		</tr>
  {/if}
  {if $addr['addr']->company==""}
  <tr>
    <td>&nbsp;</td>
  </tr>
  {/if}
  {if $counter== 0}
  {assign var=linecnt value=19}
  {elseif $counter == 1}
  {assign var=linecnt value=20}
  {else}
  {assign var=linecnt value=18}
  {/if}

  {for $foo=1 to $linecnt}
  <tr><td></td></tr>
		{/for}
		{assign var=counter value=$counter+1}
	{/foreach}
</table>
