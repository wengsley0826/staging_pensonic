<table id="addresses-tab" cellspacing="0" cellpadding="0">
	<tr>
		<td width="50%">
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr><td colspan='2'><b>{l s='Ship to:' pdf='true'}</b></td></tr>
				<tr>
					<td width="40%"><b>{l s='Customer Name:' pdf='true'}</b></td>
					<td width="60%">{strtoupper($delivery_address2->firstname)} {strtoupper($delivery_address2->lastname)}</td>
				</tr>
				<tr>
					<td><b>{l s='Delivery Address:' pdf='true'}</b></td>
					<td>
						{strtoupper($delivery_address2->address1)} {strtoupper($delivery_address2->address2)}<br />
						{strtoupper($delivery_address2->postcode)} {strtoupper($delivery_address2->city)}, {strtoupper($delivery_state->name)},<br />
						{strtoupper($delivery_address2->country)}	
					</td>
				</tr>
				<tr>
					<td><b>{l s='Telephone:' pdf='true'}</b></td>
					<td>{$delivery_address2->phone}</td>
				</tr>
				<tr>
					<td><b>{l s='Mobile:' pdf='true'}</b></td>
					<td>{$delivery_address2->phone_mobile}</td>
				</tr>
			</table>
		</td>
		<td width="50%">
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr>
				  <td width="2%"></td>
				  <td width="50%"><span class="bold">{l s='DO Number:' pdf='true'}</span></td>
				  <td width="2%"></td>
				  <td width="40%">{$title}</td>
				</tr>
        <tr>
          <td width="2%"></td>
          <td width="50%"><span class="bold">{l s='Order #:' pdf='true'}</span></td>
          <td width="2%"></td>
          <td width="40%">{$order->id}</td>
        </tr>
				{*
        <tr>
				  <td width="2%"></td>
				  <td width="50%"><span class="bold">{l s='Order Reference:' pdf='true'}</span></td>
				  <td width="2%"></td>
				  <td width="40%">{$order->getUniqReference()}</td>
				</tr>
				*}
        <tr>
				  <td width="2%"></td>
				  <td width="50%"><span class="bold">{l s='Order Date:' pdf='true'}</span></td>
				  <td width="2%"></td>
				  <td width="40%">{dateFormat date=$order->date_add full=0}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
