<table id="payment-tab" width="100%" cellpadding="4" cellspacing="0" border="1" style="font-size: 10pt;">
	<tr>
		<td width="25%" style="border: 1px solid #000;">
			<table>
				<tr><td>{l s='Goods Prepared By:' pdf='true'}</td></tr>
				<tr><td><br /><br /><br /><br /><br /></td></tr>
				<tr><td width="20%">{l s='Date:' pdf='true'}</td><td width="70%" style="border-bottom:1px solid #000;"></td></tr>
				<tr><td style="height:1px;line-height:1px;"></td></tr>
			</table>
		</td>
		<td width="25%" style="border: 1px solid #000;">
			<table>
				<tr><td>{l s='Goods Issued By:' pdf='true'}</td></tr>
				<tr><td><br /><br /><br /><br /><br /></td></tr>
				<tr><td width="20%">{l s='Date:' pdf='true'}</td><td width="70%" style="border-bottom:1px solid #000;"></td></tr>
				<tr><td style="height:1px;line-height:1px;"></td></tr>
			</table>
		</td>
		<td width="25%" style="border: 1px solid #000;">
			<table>
				<tr><td>{l s='Approved By:' pdf='true'}</td></tr>
				<tr><td><br /><br /><br /><br /><br /></td></tr>
				<tr><td width="20%">{l s='Date:' pdf='true'}</td><td width="70%" style="border-bottom:1px solid #000;"></td></tr>
				<tr><td style="height:1px;line-height:1px;"></td></tr>
			</table>
		</td>
		<td width="25%" style="border: 1px solid #000;">
			<table>
				<tr><td>{l s='Security Guard:' pdf='true'}</td></tr>
				<tr><td><br /><br /><br /><br /><br /></td></tr>
				<tr><td width="20%">{l s='Date:' pdf='true'}</td><td width="70%" style="border-bottom:1px solid #000;"></td></tr>
				<tr><td style="height:1px;line-height:1px;"></td></tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td width="25%" style="border: 1px solid #000;">
			<table>
				<tr><td>{l s='Truck/Driver:' pdf='true'}</td></tr>
				<tr><td><br /><br /><br /><br /><br /></td></tr>
				<tr><td width="20%">{l s='Date:' pdf='true'}</td><td width="70%" style="border-bottom:1px solid #000;"></td></tr>
				<tr><td style="height:1px;line-height:1px;"></td></tr>
			</table>
		</td>
		<td width="25%" style="border: 1px solid #000;">
			<table>
				<tr><td>{l s='Customer Received:' pdf='true'}</td></tr>
				<tr><td><br /><br /><br /><br /><br /></td></tr>
				<tr><td width="20%">{l s='Date:' pdf='true'}</td><td width="70%" style="border-bottom:1px solid #000;"></td></tr>
				<tr><td style="height:1px;line-height:1px;"></td></tr>
			</table>
		</td>
		<td colspan="2" width="50%" style="border: 1px solid #000;">
			{l s='Transportation Company:' pdf='true'}<br /><br />
			{l s='Truck Number:' pdf='true'}<br /><br />
			{l s='Driver ID:' pdf='true'}<br /><br />
			{l s='Driver Phone:' pdf='true'}
		</td>
	</tr>	
</table>
