{if !$available_in_your_account}
<table>
	<tr>
		<td width="60%" style=""></td>
		<td width="40%" style="border: 1px solid #000;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td width="4%"></td>
					<td width="92%" style="font-size: 8pt;line-height:15pt;">{l s="For and on behalf of:" pdf="true"}</td>
					<td width="4%"></td>
				</tr>
				<tr>
					<td></td>
					<td style="font-size: 8pt;text-align:center;line-height:8pt;">{$shop_info['shop_name']} ({$shop_info['shop_registration_no']})</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td valign="bottom" style="height:20px;font-size: 6pt;"></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td valign="bottom" style="border-bottom:1px solid #000;font-size: 6pt;">This is computer generated invoice. No signature is required</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td style="height:1px;"></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
{/if}

<br /><br />
<table style="width: 100%;">
	<tr>
		<td style="text-align: center; font-size: 6pt; color: #444;  width:100%;">
			{$shop_address|escape:'html':'UTF-8'}
			{if isset($shop_details)}
				({$shop_details|escape:'html':'UTF-8'})<br />
			{/if}

			{if !empty($shop_phone) OR !empty($shop_fax)}
				{l s='For more assistance, contact Support:' pdf='true'}<br />
				{if !empty($shop_phone)}
					{l s='Tel: %1s / %2s' sprintf=[$shop_phone|escape:'html':'UTF-8', $shop_phone_pdf|escape:'html':'UTF-8'] pdf='true'}
				{/if}

				{if !empty($shop_fax)}
					{l s='Fax: %s' sprintf=[$shop_fax|escape:'html':'UTF-8'] pdf='true'}
				{/if}
				{l s='Email: %s' sprintf=[$shop_email_pdf|escape:'html':'UTF-8'] pdf='true'}
			{/if}
			{if isset($free_text)}
				{$free_text|escape:'html':'UTF-8'}
			{/if}
		</td>
	</tr>
</table>

