<table style="width: 100%">
<tr>
	<td style="width: 28%">
		{if $logo_path}
			<img src="{$logo_path}" style="width:{$width_logo}px; height:{$height_logo}px;" />
		{/if}
	</td>
	<td style="width: 10%"></td>
	<td style="width: 70%; text-align: left;">
		<table style="width: 100%">
			<tr>
				<td><span style="font-weight: bold; font-size:13pt;">{$shop_info['shop_name']} </span><span style="font-weight: bold; font-size:10pt;">({$shop_info['shop_registration_no']})</span></td>
			</tr>
			<tr>
				<td style="font-size:10pt;">{$shop_info['shop_address1']}
          {if $shop_info['shop_address2']}
          <br />{$shop_info['shop_address2']}
          {/if}
        </td>
			</tr>
			<tr>
				<td style="font-size:10pt;">{$shop_info['shop_postcode']} {$shop_info['shop_city']}, {$shop_info['shop_state']} {$shop_info['shop_country']}</td>
			</tr>
      {if $shop_info['shop_phone'] || $shop_info['shop_fax']}
			<tr>
				<td style="font-size:10pt;">{if $shop_info['shop_phone']}{l s="Tel:"} {$shop_info['shop_phone']} {/if}{if $shop_info['shop_fax']}{l s="Fax:"} {$shop_info['shop_fax']}{/if}</td>
			</tr>
      {/if}
			<tr>
				<td style="font-size:10pt;">Website: www.pensonic.com</td>
			</tr>
      <tr>
        <td style="font-size:10pt;">{$shop_info['shop_misc']}</td>
      </tr>
		</table>
	</td>
	<td style="width: 20%;"></td>
</tr>
</table>
<br />
<br />
<table id="invoice-title" style="width: 100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<th valign="middle" style="font-size:14pt;text-transform:uppercase; text-align:center; width:100%; border:1px solid #000; background-color:#f0f0f0;">
			<b>{if isset($header)}{$header|escape:'html':'UTF-8'|upper}{/if}</b>
		</th>
	</tr>
</table>


