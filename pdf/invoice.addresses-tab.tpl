<table id="addresses-tab" cellspacing="0" cellpadding="0">
	<tr>
		<td width="60%" style="border:1px solid #000;">
			<table cellspacing="0" cellpadding="0" width="100%">
				{if $invoice_address}
					<tr>
						<td width="20%" style="text-align: right;"><span class="bold">{l s='Sold To:' pdf='true'}</span></td>
            <td width="2%"></td>
						<td class="upper" width="74%">{$invoice_address}<br /></td>
					</tr>
        {/if}
        {if $delivery_address}
        <tr>
          <td width="20%" style="text-align: right;">
            <span class="bold">{l s='Ship To:' pdf='true'}</span>
          </td>
          <td width="2%"></td>
          <td class="upper" width="74%">{$delivery_address}<br />
          </td>
        </tr>
        {/if}
      </table>
		</td>
		<td width="40%" style="border:1px solid #000;">
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr>
          <td width="2%"></td>
          <td width="50%"><span class="bold">{l s='Invoice No:' pdf='true'}</span></td>
          <td width="2%"></td>
          <td width="40%">{$title}</td>
				</tr>
				<tr>
          <td width="2%"></td>
          <td width="50%"><span class="bold">{l s='Invoice Date:' pdf='true'}</span></td>
          <td width="2%"></td>
          <td width="40%">{dateFormat date=$order->invoice_date full=0}</td>
				</tr>
        <tr><td colspan="4"><br /></td></tr>
        <tr>
          <td width="2%"></td>
          <td width="50%"><span class="bold">{l s='Order #:' pdf='true'}</span></td>
          <td width="2%"></td>
          <td width="40%">{Tools::brandsOrderNumber($order->id)}</td>
        </tr>
        {*
        <tr>
          <td width="2%"></td>
          <td width="50%"><span class="bold">{l s='Order Reference:' pdf='true'}</span></td>
					<td width="2%"></td>
          <td width="40%">{$order->getUniqReference()}</td>
				</tr>
				*}
        <tr>
          <td width="2%"></td>
					<td width="50%"><span class="bold">{l s='Order Date:' pdf='true'}</span></td>
          <td width="2%"></td>
          <td width="40%">{dateFormat date=$order->date_add full=0}</td>
				</tr>
        <tr><td colspan="4"><br /></td></tr>
				{*
        {if $direct_sale}
				<tr>
          <td width="2%"></td>
          <td width="50%"><span class="bold">{l s='Payment Term:' pdf='true'}</span></td>
          <td width="2%"></td>
          <td width="40%">{l s='30 days from Invoice Date' pdf='true'}</td>
				</tr>
				<tr>
          <td width="2%"></td>
          <td width="50%"><span class="bold">{l s='Due Date:' pdf='true'}</span></td>
          <td width="2%"></td>
          <td width="40%">{dateFormat date=$expiry_date full=0}</td>
				</tr>
				{/if}
        *}
			</table>
		</td>
	</tr>
</table>
