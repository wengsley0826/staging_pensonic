<table id="payment-tab" width="100%" border="1">
	<tr>
		<td class="payment center small grey bold" width="44%">{l s='Payment Method' pdf='true'}</td>
		<td class="payment left white" width="56%">
			<table width="100%" border="0">
				{foreach from=$order_invoice->getOrderPaymentCollection() item=payment}
					<tr>
						<td class="right small">{if strtolower($payment->payment_method) == "global payment"}{l s='Visa / Master' pdf='true'}{else}{$payment->payment_method}{/if}</td>
						<td class="right small">{displayPrice currency=$payment->id_currency price=$payment->amount}</td>
					</tr>
				{/foreach}
			</table>
		</td>
	</tr>
</table>
