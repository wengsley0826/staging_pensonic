<table id="total-tab" width="100%" border="0" style="border:none;">
	<tr>
		<td class="white" style="border-right:1px solid #000;" width="8.6%"></td>
		<td class="grey" style="border:1px solid #000;" width="53%">
			{l s='Total products' pdf='true'}
		</td>
		<td class="white" style="border:1px solid #000;" width="38.4%">
			{displayPrice currency=$order->id_currency price=$footer.products_before_discounts_tax_incl}
		</td>
	</tr>
  {if $footer.product_discounts_tax_incl > 0}
  <tr class="">
    <td class="white" style="border-right:1px solid #000;"></td>
    <td class="grey" style="border:1px solid #000;">
      {l s='Total discounts' pdf='true'}
    </td>
    <td class="white" style="border:1px solid #000;">
      - {displayPrice currency=$order->id_currency price=$footer.product_discounts_tax_incl}
    </td>
  </tr>
  {/if}
  {if !$order->isVirtual()}
  <tr>
		<td class="white" style="border-right:1px solid #000;"></td>
		<td class="grey" style="border:1px solid #000;">
			{l s='Shipping Cost' pdf='true'}
		</td>
		<td class="white" style="border:1px solid #000;">
			{if $footer.shipping_tax_incl > 0}
				{displayPrice currency=$order->id_currency price=$footer.shipping_tax_incl}
			{else}
				{l s='Free Shipping' pdf='true'}
			{/if}
		</td>
	</tr>
	{/if}

	{if $footer.wrapping_tax_incl > 0}
		<tr>
			<td class="white" style="border-right:1px solid #000;"></td>
			<td class="grey" style="border:1px solid #000;">
				{l s='Wrapping Cost' pdf='true'}
			</td>
			<td class="white" style="border:1px solid #000;">{displayPrice currency=$order->id_currency price=$footer.wrapping_tax_incl}</td>
		</tr>
	{/if}
	
  {*
	{if $footer.total_taxes > 0}
	<tr class="bold">
		<td class="white" style="border-right:1px solid #000;"></td>
		<td class="grey" style="border:1px solid #000;">
			{l s='Total (SST excl.)' pdf='true'}
		</td>
		<td class="white" style="border:1px solid #000;">
			{displayPrice currency=$order->id_currency price=$footer.total_paid_tax_excl}
		</td>
	</tr>
	<tr class="bold">
		<td class="white" style="border-right:1px solid #000;"></td>
		<td class="grey" style="border:1px solid #000;">
			{l s='Sales Tax' pdf='true'}
		</td>
		<td class="white" style="border:1px solid #000;">
			{displayPrice currency=$order->id_currency price=$footer.total_taxes}
		</td>
	</tr>
  {/if}
  *}
  <tr class="bold">
		<td class="white" style="border-right:1px solid #000;"></td>
		<td class="grey" style="border:1px solid #000;">
			{l s='Total (SST Incl.)' pdf='true'}
		</td>
		<td class="white" style="border:1px solid #000;">
			{displayPrice currency=$order->id_currency price=$footer.total_paid_tax_incl}
    </td>
	</tr>
</table>
