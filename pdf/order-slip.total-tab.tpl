<table id="total-tab" width="100%" border="0" style="border:none;">

	{if $order_slip->shipping_cost_amount > 0}
		<tr>
			<td class="white" style="border-right:1px solid #000;" width="8.6%"></td>
			<td class="grey" style="border:1px solid #000;" width="53%">{l s='Shipping' pdf='true'}</td>
			<td class="white" style="border:1px solid #000;" width="38.4%">
				- {displayPrice currency=$order->id_currency price=$order_slip->shipping_cost_amount}
			</td>
		</tr>
	{/if}

	{if isset($order_details) && count($order_details) > 0}
		{if (($order->total_paid_tax_incl - $order->total_paid_tax_excl) > 0)}
			<tr>
				<td class="white" style="border-right:1px solid #000;" width="8.6%"></td>
				<td class="grey" style="border:1px solid #000;" width="53%">
					{l s='Product Total (Tax Incl.)' pdf='true'}
				</td>
				<td class="white" style="border:1px solid #000;" width="38.4%">
					- {displayPrice currency=$order->id_currency price=$order->total_products_wt}
				</td>
			</tr>
		{else}
			<tr>
				<td class="white" style="border-right:1px solid #000;" width="8.6%"></td>
				<td class="grey" style="border:1px solid #000;" width="53%">
					{l s='Product Total' pdf='true'}
				</td>
				<td class="white" style="border:1px solid #000;" width="38.4%">
					- {displayPrice currency=$order->id_currency price=$order->total_products}
				</td>
			</tr>
		{/if}
	{/if}
	
	{if ($order->total_paid_tax_incl - $order->total_paid_tax_excl) > 0}
		<tr>
			<td class="white" style="border-right:1px solid #000;" width="8.6%"></td>
			<td class="grey" style="border:1px solid #000;" width="53%">
				{l s='Total Tax' pdf='true'}
			</td>
			<td class="white" style="border:1px solid #000;" width="38.4%">
				- {displayPrice currency=$order->id_currency price=($order->total_paid_tax_incl - $order->total_paid_tax_excl)}
			</td>
		</tr>
	{/if}
	
	<tr class="bold">
		<td class="white" style="border-right:1px solid #000;" width="8.6%"></td>
		<td class="grey" style="border:1px solid #000;" width="53%">
			{l s='Total' pdf='true'}
		</td>
		<td class="white" style="border:1px solid #000;" width="38.4%">
			{if $total_cart_rule}
				{assign var=total_paid value=0}
				{$total_paid = $order->total_paid_tax_incl - $total_cart_rule}
				- {displayPrice currency=$order->id_currency price=$total_paid}
			{else}
				- {displayPrice currency=$order->id_currency price=$order->total_paid_tax_incl}
			{/if}
		</td>
	</tr>

</table>
