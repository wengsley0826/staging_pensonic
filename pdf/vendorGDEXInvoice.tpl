
<br>
  <!-- BILLING COPY -->

<div style="font-size: 10pt; color: #444">

  <!-- ADDRESSES -->
  <table style="width: 100%" border="1">
    <tr>
	  <td rowspan="3"  style="width:5%;" nowrap><br><br><br><br>
{assign var=params value=TCPDF::serializeTCPDFtagParameters(array(90))}
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />Y<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />P<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />O<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />C<tcpdf method="StopTransform"/>

<br><br>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />G<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />N<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />I<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />L<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />L<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />I<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />B<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />.<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />1<tcpdf method="StopTransform"/>

	  </td>
      <td style="width: 70%;">
		{assign var=black value=[0,0,0]}{assign var=white value=[255,255,255]}
		{assign var=stuff value=['position'=>'S', 'border'=>false,  'padding'=>4, 'fgcolor'=>$black, 'bgcolor'=>$white, 'text'=>true, 'font'=>'helvetica', 'fontsize'=>12, 'stretchtext'=>4]}
		{assign var=params value=TCPDF::serializeTCPDFtagParameters(array({$gdexnumber}, 'C128', '', '', 80, 30, 0.4, $stuff, 'N'))}
		<tcpdf method="write1DBarcode" params="{$params}" />
	  </td><td><br><br>
	{if stripos($customer->lastname, 'dropship') !== false}
	{else}
		<img src="/var/www/html/www.shop.motherhood.com.my/img/motherhood-shop-1453195809.jpg" style="width:150px;" />
<br>
		<img src="/var/www/html/www.shop.motherhood.com.my/img/logo-gdex.png" style="width:90px;" />
	{/if}
	  </td>
	</tr>
    <tr>
      <td style="width: 70%;height:110px">
		<table style="width: 100%;">	
			<tr>
				<td style="width: 100%;font-weight:normal;">{$sellerinfo->ams_custom_html2[1]|replace:"<br /><br />":"<br />"}</td>
			</tr>
		</table>
	  </td>
      <td style="width: 30%;" rowspan="2"><br><br>
        <table style="width: 100%; font-weight:bold;">
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">TYPE</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{if $hasOnlyFlash}D{else}P{/if}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">PIECES</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$pieces}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">WEIGHT</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$weight}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">REF.</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$invoiceNo}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
        </table>
      </td>
	</tr>
  
    <tr>
      <td style="width: 70%;height:110px">
        <table style="width: 100%">
          <tr>
            <td style="width: 100%;font-weight:normal;"><b>RECEIVER:</b>{$delivery_address}</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- / ADDRESSES -->


</div>

<div style="border-top: 1px dashed #CFCFCF; padding: 2px; padding-bottom: -4px; text-align: center; border-bottom: none;">&nbsp;</div>

<!---- RECEIVER COPY ----->


<div style="font-size: 10pt; color: #444">

  <!-- ADDRESSES -->
  <table style="width: 100%" border="1">
    <tr>
	  <td rowspan="3"  style="width:5%;" nowrap><br><br><br><br>
{assign var=params value=TCPDF::serializeTCPDFtagParameters(array(90))}
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />Y<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />P<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />O<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />C<tcpdf method="StopTransform"/>

<br><br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />R<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />E<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />V<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />I<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />E<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />C<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />E<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />R<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />.<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />2<tcpdf method="StopTransform"/>

	  </td>
      <td style="width: 70%;">
		{assign var=black value=[0,0,0]}{assign var=white value=[255,255,255]}
		{assign var=stuff value=['position'=>'S', 'border'=>false,  'padding'=>4, 'fgcolor'=>$black, 'bgcolor'=>$white, 'text'=>true, 'font'=>'helvetica', 'fontsize'=>12, 'stretchtext'=>4]}
		{assign var=params value=TCPDF::serializeTCPDFtagParameters(array({$gdexnumber}, 'C128', '', '', 80, 30, 0.4, $stuff, 'N'))}
		<tcpdf method="write1DBarcode" params="{$params}" />
	  </td><td><br><br>
	{if stripos($customer->lastname, 'dropship') !== false}
	{else}
		<img src="/var/www/html/www.shop.motherhood.com.my/img/motherhood-shop-1453195809.jpg" style="width:150px;" />
<br>
		<img src="/var/www/html/www.shop.motherhood.com.my/img/logo-gdex.png" style="width:90px;" />


	{/if}
	  </td>
	</tr>
    <tr>
      <td style="width: 70%;height:110px">
		<table style="width: 100%;">	
			<tr>
				<td style="width: 100%;font-weight:normal;">{$sellerinfo->ams_custom_html2[1]|replace:"<br /><br />":"<br />"}</td>
			</tr>
		</table>
	  </td>
      <td style="width: 30%;" rowspan="2"><br><br>
        <table style="width: 100%; font-weight:bold;">
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">TYPE</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{if $hasOnlyFlash}D{else}P{/if}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">PIECES</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$pieces}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">WEIGHT</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$weight}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">REF.</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$invoiceNo}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
        </table>
      </td>
	</tr>
  
    <tr>
      <td style="width: 70%;height:110px">
        <table style="width: 100%">
          <tr>
            <td style="width: 100%;font-weight:normal;"><b>RECEIVER:</b>{$delivery_address}</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- / ADDRESSES -->

</div>

<br pagebreak="true">

  <!-- BILLING COPY -->



<div style="font-size: 10pt; color: #444">

  <!-- ADDRESSES -->
  <table style="width: 100%" border="1">
    <tr>
	  <td rowspan="3"  style="width:5%;" nowrap><br><br><br><br><br><br>
{assign var=params value=TCPDF::serializeTCPDFtagParameters(array(90))}
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />Y<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />P<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />O<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />C<tcpdf method="StopTransform"/>

<br><br>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />D<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />O<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />P<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />.<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />3<tcpdf method="StopTransform"/>

	  </td>
      <td style="width: 70%;">
		{assign var=black value=[0,0,0]}{assign var=white value=[255,255,255]}
		{assign var=stuff value=['position'=>'S', 'border'=>false,  'padding'=>4, 'fgcolor'=>$black, 'bgcolor'=>$white, 'text'=>true, 'font'=>'helvetica', 'fontsize'=>12, 'stretchtext'=>4]}
		{assign var=params value=TCPDF::serializeTCPDFtagParameters(array({$gdexnumber}, 'C128', '', '', 80, 30, 0.4, $stuff, 'N'))}
		<tcpdf method="write1DBarcode" params="{$params}" />
	  </td><td><br><br>
	{if stripos($customer->lastname, 'dropship') !== false}
	{else}
		<img src="/var/www/html/www.shop.motherhood.com.my/img/motherhood-shop-1453195809.jpg" style="width:150px;" />
<br>
		<img src="/var/www/html/www.shop.motherhood.com.my/img/logo-gdex.png" style="width:90px;" />

	{/if}
	  </td>
	</tr>
    <tr>
      <td style="width: 70%;height:110px">
		<table style="width: 100%;">
			<tr>
				<td style="width: 100%;font-weight:normal;">{$sellerinfo->ams_custom_html2[1]|replace:"<br /><br />":"<br />"}</td>
			</tr>
		</table>
	  </td>
      <td style="width: 30%;" rowspan="2"><br><br>
        <table style="width: 100%; font-weight:bold;">
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">TYPE</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{if $hasOnlyFlash}D{else}P{/if}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">PIECES</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$pieces}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">WEIGHT</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$weight}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">REF.</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$invoiceNo}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
		  <tr>
            <td colspan="3" style="width: 100%; text-align: left; vertical-align: top; font-size: 8pt;">RECEIVER NAME & SIGNATURE</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td colspan="3" style="width: 100%; text-align: left; vertical-align: top; font-size: 8pt;">_________________________</td>
          </tr>

        </table>
      </td>
	</tr>
  
    <tr>
      <td style="width: 70%;height:110px">
        <table style="width: 100%">
          <tr>
            <td style="width: 100%;font-weight:normal;"><b>RECEIVER:</b>{$delivery_address}</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- / ADDRESSES -->


</div>

<div style="border-top: 1px dashed #CFCFCF; padding: 2px; padding-bottom: -4px; text-align: center; border-bottom: none;">&nbsp;</div>
<!---- MERCHANT COPY ----->


<div style="font-size: 10pt; color: #444">

  <!-- ADDRESSES -->
  <table style="width: 100%" border="1">
    <tr>
	  <td rowspan="3"  style="width:5%;" nowrap><br><br><br><br><br><br>
{assign var=params value=TCPDF::serializeTCPDFtagParameters(array(90))}
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />Y<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />P<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />O<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />C<tcpdf method="StopTransform"/>

<br>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />R<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />E<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />D<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />N<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />E<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />S<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />.<tcpdf method="StopTransform"/>
<br><tcpdf method="StartTransform"/><tcpdf method="Rotate" params="{$params}" />4<tcpdf method="StopTransform"/>

	  </td>
      <td style="width: 70%;">
		{assign var=black value=[0,0,0]}{assign var=white value=[255,255,255]}
		{assign var=stuff value=['position'=>'S', 'border'=>false,  'padding'=>4, 'fgcolor'=>$black, 'bgcolor'=>$white, 'text'=>true, 'font'=>'helvetica', 'fontsize'=>12, 'stretchtext'=>4]}
		{assign var=params value=TCPDF::serializeTCPDFtagParameters(array({$gdexnumber}, 'C128', '', '', 80, 30, 0.4, $stuff, 'N'))}
		<tcpdf method="write1DBarcode" params="{$params}" />
	  </td><td><br><br>
	{if stripos($customer->lastname, 'dropship') !== false}
	{else}
		<img src="/var/www/html/www.shop.motherhood.com.my/img/motherhood-shop-1453195809.jpg" style="width:150px;" />
<br>
		<img src="/var/www/html/www.shop.motherhood.com.my/img/logo-gdex.png" style="width:90px;" />

	{/if}
	  </td>
	</tr>
    <tr>
      <td style="width: 70%;height:110px">
		<table style="width: 100%;">	
			<tr>
				<td style="width: 100%;font-weight:normal;">{$sellerinfo->ams_custom_html2[1]|replace:"<br /><br />":"<br />"}</td>
			</tr>
		</table>
	  </td>
      <td style="width: 30%;" rowspan="2"><br><br>
        <table style="width: 100%; font-weight:bold;">
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">TYPE</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{if $hasOnlyFlash}D{else}P{/if}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">PIECES</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$pieces}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">WEIGHT</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$weight}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">REF.</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;">{$invoiceNo}</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td colspan="3" style="width: 100%; text-align: left; vertical-align: top; font-size: 8pt;">RECEIVER NAME & SIGNATURE</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td colspan="3" style="width: 100%; text-align: left; vertical-align: top; font-size: 8pt;">_________________________</td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">NRIC</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;"></td>
          </tr>
		  <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td style="width: 33%; text-align: left; vertical-align: top; font-size: 8pt;">DATE & TIME</td>
            <td style="width: 7%; text-align: left; vertical-align: top; font-size: 8pt;">:</td>
            <td style="width: 60%; text-align: left; vertical-align: top; font-size: 8pt;"></td>
          </tr>
        </table>
      </td>
	</tr>
  
    <tr>
      <td style="width: 70%;height:110px">
        <table style="width: 100%">
          <tr>
            <td style="width: 100%;font-weight:normal;"><b>RECEIVER:</b>{$delivery_address}</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- / ADDRESSES -->

</div>






