[/{shop_url}] 

You�ve received the following order from  {firstname} {lastname},

ORDER: {order_name} (Placed on {date})	 

REFERENCE

PRODUCT

UNIT PRICE

QUANTITY

TOTAL PRICE

{products_txt} 

{discounts} 

PRODUCTS 

{total_products} 

DISCOUNTS 

{total_discounts} 

SHIPPING 

{total_shipping} 

TOTAL PAID 

{total_paid}

CARRIER: {carrier}

PAYMENT: {payment} 		 

{delivery_block_txt} 

{invoice_block_txt} 

Congratulations on the sale.

This email is computer generated, do not reply to this email.
