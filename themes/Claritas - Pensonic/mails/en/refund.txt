[/{shop_url}] 

Hi {firstname} {lastname}, 

We have processed your {shop_name} refund for order with the
reference {order_name}. 

You can review your order and download your invoice from the
"Order history" [/{history_url}] section of your customer account by
clicking "My account" [/{my_account_url}] on eStore. 


This email is computer generated, do not reply to this email.