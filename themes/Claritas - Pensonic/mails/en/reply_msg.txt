[/{shop_url}] 

Hi {firstname} {lastname}, 

{reply} 

Please do not reply directly to this email as we will not receive it. 

To reply, please use the following link: {link} [/{link}] 

This email is computer generated, do not reply to this email.