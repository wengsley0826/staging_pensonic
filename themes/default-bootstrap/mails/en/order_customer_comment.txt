[/{shop_url}] 

Hi {firstname} {lastname}, 

You have received a new message regarding order {order_name}.

First name: {firstname} 

Last name: {lastname}

Email: {email}

Product: {product_name}

Message:
{message} 		 

This email is computer generated, do not reply to this email.