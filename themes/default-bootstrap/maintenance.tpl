<!DOCTYPE html>
<html lang="{$language_code|escape:'html':'UTF-8'}">
<head>
	<meta charset="utf-8">
	<title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description)}
	<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}">
{/if}
{if isset($meta_keywords)}
	<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}">
{/if}
	<meta name="robots" content="{if isset($nobots)}no{/if}index,follow">
	<link rel="shortcut icon" href="{$favicon_url}">
       	<link href="{$css_dir}maintenance.css" rel="stylesheet">
       	<link href='//fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet'>

		<style>
p {
	font-family: "Open Sans",Helvetica,Arial,sans-serif;
	font-size: 14px;
}
.announcement {
    background-color: #fece5b;
    color: #333;
    display: block;
    margin: 7px 0 30px;
    padding: 10px 15px;
    text-align: center;
    text-decoration: none;
	font-family: "Open Sans",Helvetica,Arial,sans-serif;
	font-size: 14px;
}
</style>
</head>
<body>
	{*
    	<div class="container">
			<div id="maintenance">
				<div class="logo"><img src="{$logo_url}" {if $logo_image_width}width="{$logo_image_width}"{/if} {if $logo_image_height}height="{$logo_image_height}"{/if} alt="logo" /></div>
	        		{$HOOK_MAINTENANCE}
	        		<div id="message">
	             			<h1 class="maintenance-heading">{l s='We\'ll be back soon.'}</h1>
							{l s='We are currently updating our shop and will be back really soon.'}
							<br />
							{l s='Thanks for your patience.'}
					</div>
				</div>
	        </div>
		</div>
		*}
			<p align="center"><img src="/img/logo.jpg"></p>
	<div class="announcement">
	<strong>We are upgrading our site to serve you better!</strong>
	</div>
	<p align="center">We are sorry for the inconvenience caused! <br> If you would like to contact us, you may still do so at 1-800-881-770 <br></p>
</body>
</html>
