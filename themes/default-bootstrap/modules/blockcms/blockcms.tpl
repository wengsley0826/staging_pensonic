{if $block == 1}
<!-- Block CMS module -->
{foreach from=$cms_titles key=cms_key item=cms_title}
<section id="informations_block_left_{$cms_key}" class="block informations_block_left">
  <p class="title_block"> <a href="{$cms_title.category_link|escape:'html':'UTF-8'}"> {if !empty($cms_title.name)}{$cms_title.name}{else}{$cms_title.category_name}{/if} </a> </p>
  <div class="block_content list-block">
    <ul>
      {foreach from=$cms_title.categories item=cms_page}						{if isset($cms_page.link)}
      <li class="bullet"> <a href="{$cms_page.link|escape:'html':'UTF-8'}" title="{$cms_page.name|escape:'html':'UTF-8'}"> {$cms_page.name|escape:'html':'UTF-8'} </a> </li>
      {/if}					{/foreach}					{foreach from=$cms_title.cms item=cms_page}						{if isset($cms_page.link)}
      <li> <a href="{$cms_page.link|escape:'html':'UTF-8'}" title="{$cms_page.meta_title|escape:'html':'UTF-8'}"> {$cms_page.meta_title|escape:'html':'UTF-8'} </a> </li>
      {/if}					{/foreach}					{if $cms_title.display_store}
      <li> <a href="{$link->getPageLink('stores')|escape:'html':'UTF-8'}" title="{l s='Our stores' mod='blockcms'}"> {l s='Our stores' mod='blockcms'} </a> </li>
      {/if}
    </ul>
  </div>
</section>
{/foreach}
<!-- /Block CMS module -->
{else}
<!-- Block CMS module footer -->
<section class="footer-block corporate col-xs-12 col-sm-3" id="block_various_links_footer">
  <h4>{l s='About Pensonic' mod='blockcms'}</h4>
  <ul>
    {if isset($show_price_drop) && $show_price_drop && !$PS_CATALOG_MODE}
    <li class="item"> <a href="{$link->getPageLink('prices-drop')|escape:'html':'UTF-8'}" title="{l s='Specials' mod='blockcms'}"> {l s='Specials' mod='blockcms'} </a> </li>
    {/if}			{if isset($show_new_products) && $show_new_products}
    <li class="item"> <a href="{$link->getPageLink('new-products')|escape:'html':'UTF-8'}" title="{l s='New products' mod='blockcms'}"> {l s='New products' mod='blockcms'} </a> </li>
    {/if}			{if isset($show_best_sales) && $show_best_sales && !$PS_CATALOG_MODE}
    <li class="item"> <a href="{$link->getPageLink('best-sales')|escape:'html':'UTF-8'}" title="{l s='Top sellers' mod='blockcms'}"> {l s='Top sellers' mod='blockcms'} </a> </li>
    {/if}			{if isset($display_stores_footer) && $display_stores_footer}
    <li class="item"> <a href="{$link->getPageLink('stores')|escape:'html':'UTF-8'}" title="{l s='Our stores' mod='blockcms'}"> {l s='Our stores' mod='blockcms'} </a> </li>
    {/if}			{if isset($show_contact) && $show_contact}
    <li class="item"> <a href="{$link->getPageLink($contact_url, true)|escape:'html':'UTF-8'}" title="{l s='Contact us' mod='blockcms'}"> {l s='Contact us' mod='blockcms'} </a> </li>
    {/if}			{foreach from=$cmslinks item=cmslink}				{if $cmslink.meta_title != ''}					{if strtolower($cmslink.meta_title) == 'faq'}
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}" title="{l s='General FAQ' mod='blockcms'}"> {l s='General FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#order-faq" title="{l s='Order FAQ' mod='blockcms'}"> {l s='Order FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#delivery-faq" title="{l s='Delivery FAQ' mod='blockcms'}"> {l s='Delivery FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#payment-faq" title="{l s='Payment and Pricing FAQ' mod='blockcms'}"> {l s='Payment and Pricing FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#account-faq" title="{l s='Account FAQ' mod='blockcms'}"> {l s='Account FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#return-faq" title="{l s='Return and Cancellation FAQ' mod='blockcms'}"> {l s='Return and Cancellation FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#refund-faq" title="{l s='Refund FAQ' mod='blockcms'}"> {l s='Refund FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#membership-faq" title="{l s='Membership FAQ' mod='blockcms'}"> {l s='Membership FAQ' mod='blockcms'} </a> </li>
    {else}
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}" title="{$cmslink.meta_title|escape:'html':'UTF-8'}"> {$cmslink.meta_title|escape:'html':'UTF-8'} </a> </li>
    {/if}				{/if}			{/foreach}			{if isset($show_sitemap) && $show_sitemap}
    <li> <a href="{$link->getPageLink('sitemap')|escape:'html':'UTF-8'}" title="{l s='Sitemap' mod='blockcms'}"> {l s='Sitemap' mod='blockcms'} </a> </li>
    {/if}
  </ul>
  {if isset($show_review) && $show_review} <br />
  <div class="y-badges"> <span class="y-badges-review">{$review_count}</span>
    <div class="y-badge-stars"> {if $review_rate>=0.8}<i class="icon-star"></i>{elseif $review_rate>=0.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=1.8}<i class="icon-star"></i>{elseif $review_rate>=1.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=2.8}<i class="icon-star"></i>{elseif $review_rate>=2.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=3.8}<i class="icon-star"></i>{elseif $review_rate>=3.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=4.8}<i class="icon-star"></i>{elseif $review_rate>=4.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if} </div>
    <span class="y-badges-title">Certified reviews</span> </div>
  {/if} </section>
<!-- Block CMS module footer -->
<section class="footer-block customer-service col-xs-12 col-sm-2" id="block_various_links_footer">
  <h4>{l s='FAQ' mod='blockcms'}</h4>
  <ul>
    {if isset($show_price_drop2) && $show_price_drop2 && !$PS_CATALOG_MODE}
    <li class="item"> <a href="{$link->getPageLink('prices-drop')|escape:'html':'UTF-8'}" title="{l s='Specials' mod='blockcms'}"> {l s='Specials' mod='blockcms'} </a> </li>
    {/if}			{if isset($show_new_products2) && $show_new_products2}
    <li class="item"> <a href="{$link->getPageLink('new-products')|escape:'html':'UTF-8'}" title="{l s='New products' mod='blockcms'}"> {l s='New products' mod='blockcms'} </a> </li>
    {/if}			{if isset($show_best_sales2) && $show_best_sales2 && !$PS_CATALOG_MODE}
    <li class="item"> <a href="{$link->getPageLink('best-sales')|escape:'html':'UTF-8'}" title="{l s='Top sellers' mod='blockcms'}"> {l s='Top sellers' mod='blockcms'} </a> </li>
    {/if}			{if isset($display_stores_footer2) && $display_stores_footer2}
    <li class="item"> <a href="{$link->getPageLink('stores')|escape:'html':'UTF-8'}" title="{l s='Our stores' mod='blockcms'}"> {l s='Our stores' mod='blockcms'} </a> </li>
    {/if}			{if isset($show_contact2) && $show_contact2}
    <li class="item"> <a href="{$link->getPageLink($contact_url, true)|escape:'html':'UTF-8'}" title="{l s='Contact us' mod='blockcms'}"> {l s='Contact us' mod='blockcms'} </a> </li>
    {/if}			{foreach from=$cmslinks2 item=cmslink}				{if $cmslink.meta_title != ''}					{if strtolower($cmslink.meta_title) == 'faq'}
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}" title="{l s='General FAQ' mod='blockcms'}"> {l s='General FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#order-faq" title="{l s='Order FAQ' mod='blockcms'}"> {l s='Order FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#delivery-faq" title="{l s='Delivery FAQ' mod='blockcms'}"> {l s='Delivery FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#payment-faq" title="{l s='Payment and Pricing FAQ' mod='blockcms'}"> {l s='Payment and Pricing FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#account-faq" title="{l s='Account FAQ' mod='blockcms'}"> {l s='Account FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#return-faq" title="{l s='Return and Cancellation FAQ' mod='blockcms'}"> {l s='Return and Cancellation FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#refund-faq" title="{l s='Refund FAQ' mod='blockcms'}"> {l s='Refund FAQ' mod='blockcms'} </a> </li>
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}#membership-faq" title="{l s='Membership FAQ' mod='blockcms'}"> {l s='Membership FAQ' mod='blockcms'} </a> </li>
    {else}
    <li class="item"> <a href="{$cmslink.link|escape:'html':'UTF-8'}" title="{$cmslink.meta_title|escape:'html':'UTF-8'}"> {$cmslink.meta_title|escape:'html':'UTF-8'} </a> </li>
    {/if}				{/if}			{/foreach}			{if isset($show_sitemap2) && $show_sitemap2}
    <li> <a href="{$link->getPageLink('sitemap')|escape:'html':'UTF-8'}" title="{l s='Sitemap' mod='blockcms'}"> {l s='Sitemap' mod='blockcms'} </a> </li>
    {/if}
  </ul>
  {if isset($show_review2) && $show_review2} <br />
  <div class="y-badges"> <span class="y-badges-review">{$review_count}</span>
    <div class="y-badge-stars"> {if $review_rate>=0.8}<i class="icon-star"></i>{elseif $review_rate>=0.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=1.8}<i class="icon-star"></i>{elseif $review_rate>=1.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=2.8}<i class="icon-star"></i>{elseif $review_rate>=2.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=3.8}<i class="icon-star"></i>{elseif $review_rate>=3.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if}				{if $review_rate>=4.8}<i class="icon-star"></i>{elseif $review_rate>=4.3}<i class="icon-star-half-empty"></i>{else}<i class="icon-star-empty"></i>{/if} </div>
    <span class="y-badges-title">Certified reviews</span> </div>
  {/if} </section>
<!-- /Block CMS module footer -->
{/if} 