<!-- MODULE Block contact infos -->
<section id="block_contact_infos" class="footer-address col-xs-12 col-sm-4">
  <div>
    <h4>{l s='Address' mod='blockcontactinfos'}</h4>
    <ul class="company-add-info">      {if $blockcontactinfos_company != '' || $blockcontactinfos_address != ''}        <li>          {if $blockcontactinfos_company != ''}{$blockcontactinfos_company|escape:'html':'UTF-8'}<br />{/if}                      {if $blockcontactinfos_address != ''}{$blockcontactinfos_address nofilter}{/if}        </li>
        {if $blockcontactinfos_phone != ''}
        <li>{l s=' ' mod='blockcontactinfos'} <span>{$blockcontactinfos_phone|escape:'html':'UTF-8'}</span></li>
        {/if}
        {if $blockcontactinfos_email != ''}
        <li>{l s=' ' mod='blockcontactinfos'} <span>{mailto address=$blockcontactinfos_email|escape:'html':'UTF-8' encode="hex"}</span></li>
        {/if}
      {/if}
    </ul>
  </div>
</section>
<!-- /MODULE Block contact infos -->
