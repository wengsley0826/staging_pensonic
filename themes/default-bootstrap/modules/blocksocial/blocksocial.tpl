<section id="social-media" class="footer-social-media col-xs-12 col-sm-3">  <div class="footer-download-apps">    <h4>Downloads Pensonic Apps</h4>    <ul>      <li>        <a href="https://play.google.com/store/apps/details?id=com.pensonic.DCRM">          <img src="../../../img/google-play-store.jpg" alt="Google Play Store">
          </a>      </li>      <li>        <a href="https://apps.apple.com/us/app/pensonic-your-enjoyment/id1439098419">          <img src="../../../img/apple-app-store.jpg" alt="App Store">
          </a>      </li>      <li>        <a href="https://appgallery.huawei.com/#/app/C101386843">          <img src="../../../img/huawei-app-gallery.png" alt="Huawei App Gallery" />
          </a>      </li>    </ul>  </div>
  <div class="footer-socialmedia-link" style="padding: 30px 0;">
    <h4>Social</h4>
    <ul>

      {if isset($facebook_url) && $facebook_url != ''}

      <li class="facebook">

        <a class="_blank" href="{$facebook_url|escape:html:'UTF-8'}">

          <span>{l s='Facebook' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($facebook2_url) && $facebook2_url != ''}

      <li class="facebook facebook2">

        <a class="_blank" href="{$facebook2_url|escape:html:'UTF-8'}">

          <span>{l s='Facebook' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($twitter_url) && $twitter_url != ''}

      <li class="twitter">

        <a class="_blank" href="{$twitter_url|escape:html:'UTF-8'}">

          <span>{l s='Twitter' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($rss_url) && $rss_url != ''}

      <li class="rss">

        <a class="_blank" href="{$rss_url|escape:html:'UTF-8'}">

          <span>{l s='RSS' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($youtube_url) && $youtube_url != ''}

      <li class="youtube">

        <a class="_blank" href="{$youtube_url|escape:html:'UTF-8'}">

          <span>{l s='Youtube' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($youtube2_url) && $youtube2_url != ''}

      <li class="youtube youtube2">

        <a class="_blank" href="{$youtube2_url|escape:html:'UTF-8'}">

          <span>{l s='Youtube' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($google_plus_url) && $google_plus_url != ''}

      <li class="google-plus">

        <a class="_blank" href="{$google_plus_url|escape:html:'UTF-8'}" rel="publisher">

          <span>{l s='Google Plus' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($pinterest_url) && $pinterest_url != ''}

      <li class="pinterest">

        <a class="_blank" href="{$pinterest_url|escape:html:'UTF-8'}">

          <span>{l s='Pinterest' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($vimeo_url) && $vimeo_url != ''}

      <li class="vimeo">

        <a class="_blank" href="{$vimeo_url|escape:html:'UTF-8'}">

          <span>{l s='Vimeo' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

      {if isset($instagram_url) && $instagram_url != ''}

      <li class="instagram">

        <a class="_blank" href="{$instagram_url|escape:html:'UTF-8'}">

          <span>{l s='Instagram' mod='blocksocial'}</span>

        </a>

      </li>

      {/if}

    </ul>
  </div>
</section>
